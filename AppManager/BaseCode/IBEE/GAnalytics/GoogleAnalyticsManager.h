//
//  GoogleAnalyticsManager.h
//  GoogleAnalyticsTest
//
//  Created by Betrand Yella on 03/08/12.
//  Copyright (c) 2012 betrand@ibeesolutions.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConstantsEDM.h"

@interface GoogleAnalyticsManager : NSObject

+(void)logAppEntry;
+(void)logAppForegroundEntry;
+(void)logAppExit;

+(void)logWidgetVisit:(AppType)appType;
+(void)logWidgetDetailVisit:(AppType)appType title:(NSString*)title;

+(void)logAppResignActive;

@end
