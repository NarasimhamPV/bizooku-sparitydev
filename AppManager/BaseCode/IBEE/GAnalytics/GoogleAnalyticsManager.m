//
//  GoogleAnalyticsManager.m
//  GoogleAnalyticsTest
//
//  Created by Betrand Yella on 03/08/12.
//  Copyright (c) 2012 betrand@ibeesolutions.com. All rights reserved.
//

#import "GoogleAnalyticsManager.h"
#import "GANTracker.h"
#import "Utils.h"

#define kGANDispatchPeriodSec 1.0
#define kGAnalyticsAccountID @"UA-33307420-1"
#define ANALYTICS_ACTIVE 0
#define DEBUG_ANALYTICS 0

@implementation GoogleAnalyticsManager

+(void)logAppEntry{
#if ANALYTICS_ACTIVE
    // **************************************************************************
    // PLEASE REPLACE WITH YOUR ACCOUNT DETAILS.
    // **************************************************************************
    [[GANTracker sharedTracker] startTrackerWithAccountID:kGAnalyticsAccountID
                                           dispatchPeriod:kGANDispatchPeriodSec
                                                 delegate:nil];
    
    NSError *error;
    if (![[GANTracker sharedTracker] setCustomVariableAtIndex:1
                                                         name:@"iPhone1"
                                                        value:@"iv1"
                                                    withError:&error]) {
        // Handle error here
        NSLog(@"GANTracke Error 1: %@", error);
    }
    
    if (![[GANTracker sharedTracker] trackEvent:@"my_category"
                                         action:@"my_action"
                                          label:@"my_label"
                                          value:-1
                                      withError:&error]) {
        // Handle error here
        // Handle error here
        NSLog(@"GANTracke Error 2: %@", error);
    }
    
    if (![[GANTracker sharedTracker] trackPageview:@"/app_launched"
                                         withError:&error]) {
        // Handle error here
        // Handle error here
        NSLog(@"GANTracke Error 3: %@", error);
    }
#endif
}

+(void)logAppResignActive{
#if ANALYTICS_ACTIVE
    NSError *error = nil;
    if (![[GANTracker sharedTracker] trackPageview:@"/app_resign_active"
                                         withError:&error]) {
        // Handle error here
        // Handle error here
#if DEBUG_ANALYTICS
        NSLog(@"GANTracke Error 3: %@", error);
#endif
    }
#endif
}

+(void)logAppForegroundEntry{
#if ANALYTICS_ACTIVE
    NSError *error = nil;
    if (![[GANTracker sharedTracker] trackPageview:@"/app_foreground_entry"
                                         withError:&error]) {
        // Handle error here
        // Handle error here
#if DEBUG_ANALYTICS
        NSLog(@"GANTracke Error 3: %@", error);
#endif
    }
#endif
}

+(void)logAppExit{
#if ANALYTICS_ACTIVE
    NSError *error = nil;
    if (![[GANTracker sharedTracker] trackPageview:@"/app_exit"
                                         withError:&error]) {
        // Handle error here
        // Handle error here
#if DEBUG_ANALYTICS
        NSLog(@"GANTracke Error 3: %@", error);
#endif
    }
#endif
}

+(void)logWidgetVisit:(AppType)appType{
#if ANALYTICS_ACTIVE
    NSError *error = nil;
    if (![[GANTracker sharedTracker] trackPageview:[NSString stringWithFormat:@"/page_visit/%@", [Utils titleForAppType:appType]]  withError:&error]) {
        // Handle error here
        // Handle error here
#if DEBUG_ANALYTICS
        NSLog(@"GANTracke Error 3: %@", error);
#endif
    }
#endif
}

+(void)logWidgetDetailVisit:(AppType)appType title:(NSString*)title{
#if ANALYTICS_ACTIVE
    NSError *error = nil;
    if (![[GANTracker sharedTracker] trackPageview:[NSString stringWithFormat:@"/page_visit/%@/%@", [Utils titleForAppType:appType], title]  withError:&error]) {
        // Handle error here
        // Handle error here
#if DEBUG_ANALYTICS
        NSLog(@"GANTracke Error 3: %@", error);
#endif
    }
#endif
}

@end
