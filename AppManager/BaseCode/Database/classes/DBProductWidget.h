//
//  DBProductWidget.h
//  Exceeding
//
//  Created by Rajesh Kumar on 13/12/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBWidget;

@interface DBProductWidget : NSManagedObject

@property (nonatomic, retain) NSString * productEmail;
@property (nonatomic, retain) NSNumber * productId;
@property (nonatomic, retain) NSString * productImage;
@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSString * productPhone;
@property (nonatomic, retain) NSString * productPrice;
@property (nonatomic, retain) NSString * productPageTitle;


@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSString * categoryId;

@property (nonatomic, retain) NSSet *widget;
@end

@interface DBProductWidget (CoreDataGeneratedAccessors)

- (void)addWidgetObject:(DBWidget *)value;
- (void)removeWidgetObject:(DBWidget *)value;
- (void)addWidget:(NSSet *)values;
- (void)removeWidget:(NSSet *)values;

@end
