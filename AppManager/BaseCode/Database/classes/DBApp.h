//
//  DBApp.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBLayoutInfo;

@interface DBApp : NSManagedObject

@property (nonatomic, retain) NSString * backgroundimage;
@property (nonatomic, retain) NSNumber * brandid;
@property (nonatomic, retain) NSString * brandname;
@property (nonatomic, retain) NSString * brandnickname;

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * createdon;
@property (nonatomic, retain) NSString * descrip;
@property (nonatomic, retain) NSNumber * islayoutmodified;
@property (nonatomic, retain) NSNumber * isnewlayoutselected;
@property (nonatomic, retain) NSDate * lastmodified;
@property (nonatomic, retain) NSString * logo;
@property (nonatomic, retain) NSString * splashimage;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * userid;
@property (nonatomic, retain) NSString * appName;
@property (nonatomic, retain) NSString * keywords;
@property (nonatomic, retain) NSString * listWidgets;
@property (nonatomic, retain) NSString * logoContainerImg;
@property (nonatomic, retain) NSString * markettingURL;
@property (nonatomic, retain) NSString * privacyURL;
@property (nonatomic, retain) NSString * serverPath;
@property (nonatomic, retain) NSString * supportEmail;
@property (nonatomic, retain) NSString * supportURL;
@property (nonatomic, retain) NSString * supportNumber;
@property (nonatomic, retain) NSString * aboutURL;
@property (nonatomic, retain) NSString * aboutImage;

@property (nonatomic, retain) NSString *donateURL;
@property (nonatomic, retain) NSString * podcAudioURL;
@property (nonatomic, retain) NSString * podcVideoURL;
@property (nonatomic, retain) NSString * podcYouTubeURL;
@property (nonatomic, retain) NSString * podcYouTubeChannelName;

@property (nonatomic, retain) NSString * socFBAppID;
@property (nonatomic, retain) NSString * socFBPageID;
@property (nonatomic, retain) NSString * socTwitterID;

@property (nonatomic, retain) NSString * appstoreID;
@property (nonatomic, retain) NSString * appstoreURL;
@property (nonatomic, retain) NSString * appstoreShort;

@property (nonatomic, retain) NSString * androidID;
@property (nonatomic, retain) NSString * androidURL;
@property (nonatomic, retain) NSString * androidShort;

@property (nonatomic, retain) DBLayoutInfo *layoutInfo;

@property (nonatomic, retain) NSString *phoneNum;
@property (nonatomic, retain) NSString *socTwitterBrandName;

@property (nonatomic, assign) BOOL isDonationsEnabled;
@property (nonatomic, retain) NSNumber *isPrivateAppEnabled;
@property (nonatomic, strong) NSNumber *islocalRecordingEnabled;
@property (nonatomic, retain) NSString *codeLoginType;

@end
