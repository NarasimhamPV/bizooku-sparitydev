//
//  DBVideoWidget.h
//  Exceeding
//
//  Created by Veeru on 07/01/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBWidget;

@interface DBVideoWidget : NSManagedObject

@property (nonatomic, retain) NSString * videoBrandId;
@property (nonatomic, retain) NSString * videoCategoryId;
@property (nonatomic, retain) NSString * videoCategoryName;
@property (nonatomic, retain) NSString * videoIsActive;
@property (nonatomic, retain) NSSet *widget;
@end

@interface DBVideoWidget (CoreDataGeneratedAccessors)

- (void)addWidgetObject:(DBWidget *)value;
- (void)removeWidgetObject:(DBWidget *)value;
- (void)addWidget:(NSSet *)values;
- (void)removeWidget:(NSSet *)values;

@end
