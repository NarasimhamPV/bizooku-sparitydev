//
//  DBLayoutTypes.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBLayoutInfo;

@interface DBLayoutTypes : NSManagedObject

@property (nonatomic, retain) DBLayoutInfo *layoutInfo;

@end
