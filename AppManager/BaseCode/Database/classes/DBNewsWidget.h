//
//  DBNewsWidget.h
//  Exceeding
//
//  Created by Veeru on 26/08/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@class DBWidget;

@interface DBNewsWidget : NSManagedObject

@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSNumber * categoryID;
@property (nonatomic, retain) NSString * newsImage;
@property (nonatomic, retain) NSString * newsTitle;
@property (nonatomic, retain) NSNumber * newsID;

@property (nonatomic, retain) NSSet *widget;


@end


@interface DBNewsWidget (CoreDataGeneratedAccessors)

- (void)addWidgetObject:(DBWidget *)value;
- (void)removeWidgetObject:(DBWidget *)value;
- (void)addWidget:(NSSet *)values;
- (void)removeWidget:(NSSet *)values;

@end
