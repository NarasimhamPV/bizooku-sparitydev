//
//  DBCouponsWidget.m
//  Exceeding
//
//  Created by Veeru on 19/05/14.
//
//

#import "DBCouponsWidget.h"
#import "DBWidget.h"


@implementation DBCouponsWidget

@dynamic couponTitle;
@dynamic couponId;
@dynamic couponImage;
@dynamic couponCategoryName;
@dynamic couponCategoryId;
@dynamic widget;

@end
