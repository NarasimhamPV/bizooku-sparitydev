//
//  DBTilesInfo.h
//  Exceeding
//
//  Created by Rajesh Kumar on 19/02/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBLayoutInfo, DBTileSize, DBWidget;

@interface DBTilesInfo : NSManagedObject

@property (nonatomic, retain) NSString * bgcolor;
@property (nonatomic, retain) NSString * bgimage;
@property (nonatomic, retain) NSNumber * brandid;
@property (nonatomic, retain) NSNumber * brandTileId;
@property (nonatomic, retain) NSString * defaultBgColor;
@property (nonatomic, retain) NSString * defaultTitleColor;
@property (nonatomic, retain) NSNumber * isactive;
@property (nonatomic, retain) NSNumber * isconfigured;
@property (nonatomic, retain) NSNumber * isnewtile;
@property (nonatomic, retain) NSDate * lastmodifiedon;
@property (nonatomic, retain) NSNumber * layouttileid;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSString * tilecolor;
@property (nonatomic, retain) NSNumber * tileid;
@property (nonatomic, retain) NSString * tilename;
@property (nonatomic, retain) NSString * tiletype;
@property (nonatomic, retain) NSString * titleColor;
@property (nonatomic, retain) NSString * titleFont;
@property (nonatomic, retain) NSNumber * titleFontSize;
@property (nonatomic, retain) NSString * titlePosition;
@property (nonatomic, retain) NSNumber * webLinkInApp;
@property (nonatomic, retain) NSString * webLinkPageTitle;
@property (nonatomic, retain) NSString * webLinkURL;
@property (nonatomic, retain) NSNumber * x;
@property (nonatomic, retain) NSNumber * y;
@property (nonatomic, retain) NSString * webLinkTitle;
@property (nonatomic, retain) DBLayoutInfo *layoutInfo;
@property (nonatomic, retain) DBTileSize *tileSize;
@property (nonatomic, retain) DBWidget *widget;

@property (nonatomic, retain) NSString *imageType;
@property (nonatomic, retain) NSString *categoryListTitle;
@property (nonatomic, retain) NSString *eventViewType;

@property (nonatomic, retain) NSNumber *isUserFavorite;
@property (nonatomic,retain) NSString *singleVideoIdStr;



@end
