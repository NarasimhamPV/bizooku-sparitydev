//
//  DBProductWidget.m
//  Exceeding
//
//  Created by Rajesh Kumar on 13/12/13.
//
//

#import "DBProductWidget.h"
#import "DBWidget.h"


@implementation DBProductWidget

@dynamic productEmail;
@dynamic productId;
@dynamic productImage;
@dynamic productName;
@dynamic productPhone;
@dynamic productPrice;
@dynamic productPageTitle;
@dynamic widget;
@dynamic categoryName;
@dynamic categoryId;

@end
