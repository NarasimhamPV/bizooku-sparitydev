//
//  DBLayoutInfo.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DBLayoutInfo.h"
#import "DBApp.h"
#import "DBLayoutTypes.h"
#import "DBTilesInfo.h"


@implementation DBLayoutInfo

@dynamic action;
@dynamic createddate;
@dynamic descrip;
@dynamic hasLogoContainer;
@dynamic image;
@dynamic isactive;
@dynamic layoutid;
@dynamic layoutname;
@dynamic hasNavigationConatiner;
@dynamic createdBy;
@dynamic app;
@dynamic layoutType;
@dynamic tiles;

@end
