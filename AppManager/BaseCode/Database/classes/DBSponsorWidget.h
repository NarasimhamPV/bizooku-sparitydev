//
//  DBSponsorWidget.h
//  Exceeding
//
//  Created by Rajesh Kumar on 19/02/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBWidget;

@interface DBSponsorWidget : NSManagedObject

@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * sponsorId;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * aboutUs;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSString * tileImage;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString * webSite;
@property (nonatomic, retain) NSSet *widget;
@end

@interface DBSponsorWidget (CoreDataGeneratedAccessors)

- (void)addWidgetObject:(DBWidget *)value;
- (void)removeWidgetObject:(DBWidget *)value;
- (void)addWidget:(NSSet *)values;
- (void)removeWidget:(NSSet *)values;
@end
