//
//  DBEventsWidget.h
//  Exceeding
//
//  Created by Veeru on 26/08/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBWidget;


@interface DBEventsWidget : NSManagedObject

@property (nonatomic, retain) NSString * categeoryId;
@property (nonatomic, retain) NSString * categeoryName;
@property (nonatomic, retain) NSNumber * eventsId;
@property (nonatomic, retain) NSString * eventImg;
@property (nonatomic, retain) NSString * eventTitle;

@property (nonatomic, retain) NSSet *widget;

@end


@interface DBEventsWidget (CoreDataGeneratedAccessors)

- (void)addWidgetObject:(DBWidget *)value;
- (void)removeWidgetObject:(DBWidget *)value;
- (void)addWidget:(NSSet *)values;
- (void)removeWidget:(NSSet *)values;

@end