//
//  DBSponsorWidget.m
//  Exceeding
//
//  Created by Rajesh Kumar on 19/02/13.
//
//

#import "DBSponsorWidget.h"
#import "DBWidget.h"


@implementation DBSponsorWidget

@dynamic image;
@dynamic sponsorId;
@dynamic title;
@dynamic aboutUs;
@dynamic email;
@dynamic createdDate;
@dynamic startDate;
@dynamic endDate;
@dynamic tileImage;
@dynamic phone;
@dynamic webSite;
@dynamic widget;

@end
