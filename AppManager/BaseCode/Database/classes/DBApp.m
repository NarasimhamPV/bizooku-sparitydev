//
//  DBApp.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DBApp.h"
#import "DBLayoutInfo.h"


@implementation DBApp


@dynamic donateURL;
@dynamic backgroundimage;
@dynamic brandid;
@dynamic brandname;
@dynamic category;
@dynamic createdon;
@dynamic descrip;
@dynamic islayoutmodified;
@dynamic isnewlayoutselected;
@dynamic lastmodified;
@dynamic logo;
@dynamic splashimage;
@dynamic title;
@dynamic userid;
@dynamic appName;
@dynamic keywords;
@dynamic listWidgets;
@dynamic logoContainerImg;
@dynamic markettingURL;
@dynamic privacyURL;
@dynamic serverPath;
@dynamic supportEmail;
@dynamic supportURL;
@dynamic layoutInfo;
@dynamic aboutURL;
@dynamic podcAudioURL;
@dynamic podcVideoURL;
@dynamic podcYouTubeURL;
@dynamic supportNumber;
@dynamic podcYouTubeChannelName;
@dynamic phoneNum;
@dynamic brandnickname;
@dynamic appstoreID, appstoreURL, androidID, androidURL, appstoreShort, androidShort;
@synthesize socFBAppID, socFBPageID, socTwitterID;

@dynamic aboutImage;

@dynamic socTwitterBrandName;

@dynamic isDonationsEnabled;
@dynamic isPrivateAppEnabled;
@dynamic islocalRecordingEnabled;
@dynamic codeLoginType;
@end
