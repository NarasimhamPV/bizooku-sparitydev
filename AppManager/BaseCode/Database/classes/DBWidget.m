//
//  DBWidget.m
//  Exceeding
//
//  Created by Veeru on 07/01/15.
//
//

#import "DBWidget.h"
#import "DBCouponsWidget.h"
#import "DBEventsWidget.h"
#import "DBNewsWidget.h"
#import "DBProductWidget.h"
#import "DBSponsorWidget.h"
#import "DBTilesInfo.h"
#import "DBVideoWidget.h"


@implementation DBWidget

@dynamic brandWidgetID;
@dynamic createddate;
@dynamic descrip;
@dynamic image;
@dynamic isactive;
@dynamic isConfigurable;
@dynamic liveTileInfo;
@dynamic widgetConfigPage;
@dynamic widgetConfigTable;
@dynamic widgetID;
@dynamic widgetName;
@dynamic action;
@dynamic coupons;
@dynamic events;
@dynamic news;
@dynamic products;
@dynamic sponsor;
@dynamic tileInfo;
@dynamic videos;

@end
