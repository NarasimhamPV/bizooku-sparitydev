//
//  DBLayoutInfo.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBApp, DBLayoutTypes, DBTilesInfo;

@interface DBLayoutInfo : NSManagedObject

@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSDate * createddate;
@property (nonatomic, retain) NSString * descrip;
@property (nonatomic, retain) NSNumber * hasLogoContainer;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * isactive;
@property (nonatomic, retain) NSNumber * layoutid;
@property (nonatomic, retain) NSString * layoutname;
@property (nonatomic, retain) NSNumber * hasNavigationConatiner;
@property (nonatomic, retain) NSNumber * createdBy;
@property (nonatomic, retain) DBApp *app;
@property (nonatomic, retain) DBLayoutTypes *layoutType;
@property (nonatomic, retain) NSSet *tiles;
@end

@interface DBLayoutInfo (CoreDataGeneratedAccessors)

- (void)addTilesObject:(DBTilesInfo *)value;
- (void)removeTilesObject:(DBTilesInfo *)value;
- (void)addTiles:(NSSet *)values;
- (void)removeTiles:(NSSet *)values;

@end
