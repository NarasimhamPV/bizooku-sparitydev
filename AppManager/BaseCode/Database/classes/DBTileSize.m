//
//  DBTileSize.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DBTileSize.h"
#import "DBTilesInfo.h"


@implementation DBTileSize

@dynamic height;
@dynamic idStr;
@dynamic width;
@dynamic tileInfo;

@end
