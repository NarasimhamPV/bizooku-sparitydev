//
//  DBVideoWidget.m
//  Exceeding
//
//  Created by Veeru on 07/01/15.
//
//

#import "DBVideoWidget.h"
#import "DBWidget.h"


@implementation DBVideoWidget

@dynamic videoBrandId;
@dynamic videoCategoryId;
@dynamic videoCategoryName;
@dynamic videoIsActive;
@dynamic widget;

@end
