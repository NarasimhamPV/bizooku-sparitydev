//
//  DBCouponsWidget.h
//  Exceeding
//
//  Created by Veeru on 19/05/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@class DBWidget;

@interface DBCouponsWidget : NSManagedObject

@property (nonatomic, retain) NSString *couponTitle;
@property (nonatomic, retain) NSString *couponId;
@property (nonatomic, retain) NSString *couponImage;
@property (nonatomic, retain) NSString *couponCategoryName;
@property (nonatomic, retain) NSString *couponCategoryId;
@property (nonatomic, retain) NSSet *widget;



@end

@interface DBCouponsWidget (CoreDataGeneratedAccessors)

- (void)addWidgetObject:(DBWidget *)value;
- (void)removeWidgetObject:(DBWidget *)value;
- (void)addWidget:(NSSet *)values;
- (void)removeWidget:(NSSet *)values;

@end
