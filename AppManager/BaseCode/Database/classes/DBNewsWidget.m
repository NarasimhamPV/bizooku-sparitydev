//
//  DBNewsWidget.m
//  Exceeding
//
//  Created by Veeru on 26/08/14.
//
//

#import "DBNewsWidget.h"
#import "DBWidget.h"

@implementation DBNewsWidget

@dynamic categoryName;
@dynamic categoryID;
@dynamic newsImage;
@dynamic newsTitle;
@dynamic newsID;
@dynamic widget;

@end
