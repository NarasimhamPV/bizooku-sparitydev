//
//  DBWidget.h
//  Exceeding
//
//  Created by Veeru on 07/01/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBCouponsWidget, DBEventsWidget, DBNewsWidget, DBProductWidget, DBSponsorWidget, DBTilesInfo, DBVideoWidget;

@interface DBWidget : NSManagedObject

@property (nonatomic, retain) NSNumber * brandWidgetID;
@property (nonatomic, retain) NSDate * createddate;
@property (nonatomic, retain) NSString * descrip;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * isactive;
@property (nonatomic, retain) NSNumber * isConfigurable;
@property (nonatomic, retain) NSString * liveTileInfo;
@property (nonatomic, retain) NSString * widgetConfigPage;
@property (nonatomic, retain) NSString * widgetConfigTable;
@property (nonatomic, retain) NSNumber * widgetID;
@property (nonatomic, retain) NSString * widgetName;
@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSSet *coupons;
@property (nonatomic, retain) NSSet *events;
@property (nonatomic, retain) NSSet *news;
@property (nonatomic, retain) NSSet *products;
@property (nonatomic, retain) NSSet *sponsor;
@property (nonatomic, retain) DBTilesInfo *tileInfo;
@property (nonatomic, retain) NSSet *videos;
@end

@interface DBWidget (CoreDataGeneratedAccessors)

- (void)addCouponsObject:(DBCouponsWidget *)value;
- (void)removeCouponsObject:(DBCouponsWidget *)value;
- (void)addCoupons:(NSSet *)values;
- (void)removeCoupons:(NSSet *)values;

- (void)addEventsObject:(DBEventsWidget *)value;
- (void)removeEventsObject:(DBEventsWidget *)value;
- (void)addEvents:(NSSet *)values;
- (void)removeEvents:(NSSet *)values;

- (void)addNewsObject:(DBNewsWidget *)value;
- (void)removeNewsObject:(DBNewsWidget *)value;
- (void)addNews:(NSSet *)values;
- (void)removeNews:(NSSet *)values;

- (void)addProductsObject:(DBProductWidget *)value;
- (void)removeProductsObject:(DBProductWidget *)value;
- (void)addProducts:(NSSet *)values;
- (void)removeProducts:(NSSet *)values;

- (void)addSponsorObject:(DBSponsorWidget *)value;
- (void)removeSponsorObject:(DBSponsorWidget *)value;
- (void)addSponsor:(NSSet *)values;
- (void)removeSponsor:(NSSet *)values;

- (void)addVideosObject:(DBVideoWidget *)value;
- (void)removeVideosObject:(DBVideoWidget *)value;
- (void)addVideos:(NSSet *)values;
- (void)removeVideos:(NSSet *)values;

@end
