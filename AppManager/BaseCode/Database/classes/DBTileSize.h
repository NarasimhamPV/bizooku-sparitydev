//
//  DBTileSize.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DBTilesInfo;

@interface DBTileSize : NSManagedObject

@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSString * idStr;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSSet *tileInfo;
@end

@interface DBTileSize (CoreDataGeneratedAccessors)

- (void)addTileInfoObject:(DBTilesInfo *)value;
- (void)removeTileInfoObject:(DBTilesInfo *)value;
- (void)addTileInfo:(NSSet *)values;
- (void)removeTileInfo:(NSSet *)values;

@end
