//
//  DBTilesInfo.m
//  Exceeding
//
//  Created by Rajesh Kumar on 19/02/14.
//
//

#import "DBTilesInfo.h"
#import "DBLayoutInfo.h"
#import "DBTileSize.h"
#import "DBWidget.h"


@implementation DBTilesInfo

@dynamic bgcolor;
@dynamic bgimage;
@dynamic brandid;
@dynamic brandTileId;
@dynamic defaultBgColor;
@dynamic defaultTitleColor;
@dynamic isactive;
@dynamic isconfigured;
@dynamic isnewtile;
@dynamic lastmodifiedon;
@dynamic layouttileid;
@dynamic position;
@dynamic tilecolor;
@dynamic tileid;
@dynamic tilename;
@dynamic tiletype;
@dynamic titleColor;
@dynamic titleFont;
@dynamic titleFontSize;
@dynamic titlePosition;
@dynamic webLinkInApp;
@dynamic webLinkPageTitle;
@dynamic webLinkURL;
@dynamic x;
@dynamic y;
@dynamic webLinkTitle;
@dynamic layoutInfo;
@dynamic tileSize;
@dynamic widget;
@dynamic imageType;
@dynamic categoryListTitle;
@dynamic eventViewType;
@dynamic isUserFavorite;
@dynamic singleVideoIdStr;

@end
