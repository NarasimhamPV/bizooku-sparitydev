//
//  SDZEXAnalyticsService.m
//  Exceeding
//
//  Created by Rajesh Kumar on 29/01/13.
//
//

#import "SDZEXAnalyticsService.h"
#import "ConstantsWebService.h"

@implementation SDZEXAnalyticsService


- (id) init
{
    if(self = [super init])
    {
        // self.serviceUrl = [NSString stringWithFormat:@"%@%@",kWebServiceURL, @"Services/EXLayoutService.asmx"];
        //http://exceedings.ibeesolutions.com/Services/EXAnalyticsService.asmx
        
        
        self.serviceUrl = [NSString stringWithFormat:@"%@%@",kWebServiceURL, @"Services/EXAnalyticsService.asmx"];
        
        self.namespace = @"http://tempuri.org/";
        self.headers = nil;
        self.logging = NO;
    }
    return self;
}

- (id) initWithUsername: (NSString*) username andPassword: (NSString*) password {
    if(self = [super initWithUsername:username andPassword:password]) {
    }
    return self;
}

+ (SDZEXAnalyticsService*) service {
    return [SDZEXAnalyticsService serviceWithUsername:nil andPassword:nil];
}

+ (SDZEXAnalyticsService*) serviceWithUsername: (NSString*) username andPassword: (NSString*) password {
    return [[[SDZEXAnalyticsService alloc] initWithUsername:username andPassword:password] autorelease];
}



// Session action..


- (SoapRequest*) AddSessionAction: (id <SoapDelegate>) handler brandId: (long) brandId sessionInSec:(float)sec deviceType: (NSString *)deviceType
{
    return [self AddSessionAction:handler action:nil brandId:brandId sessionInSec:sec deviceType:deviceType];
}


- (SoapRequest*) AddSessionAction: (id)_target action: (SEL)_action brandId: (long) brandId sessionInSec:(float)sec deviceType: (NSString *)deviceType
{
    NSLog(@"THe AddSessionAction fired");
    
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithFloat:sec] forName: @"timeinsec"] autorelease]];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddSession" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    NSLog(@"The AddSession log : %@ , %@", _params, _envelope);
    
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addSession postData: _envelope deserializeTo: @"NSString"];
    [_request send];
    
    return _request;
}

// Entry Actions....

/*
- (SoapRequest*) AddAudioEntryAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    
    return [self AddAudioEntryAction:handler action:nil brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
    
}


- (SoapRequest*) AddAudioEntryAction: (id) target action: (SEL) action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    
    
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetId] forName: @"widgetId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetItemId] forName: @"widgetItemId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: widgetType forName: @"widgetType"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddEntryAction" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    NSLog(@"The AddEntryAction log : %@ , %@", _params, _envelope);
    
    SoapRequest* _request = [SoapRequest create: target action: action service: self soapAction: addEntryActiion postData: _envelope deserializeTo: @"NSString"];
    [_request send];
    return _request;
    
}

*/

- (SoapRequest*) AddEntryAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    return [self AddEntryAction:handler action:nil brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
}

- (SoapRequest*) AddEntryAction: (id)_target action: (SEL)_action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetId] forName: @"widgetId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetItemId] forName: @"widgetItemId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: widgetType forName: @"widgetType"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddEntryAction" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    NSLog(@"The AddEntryAction log : %@ , %@", _params, _envelope);
    
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addEntryActiion postData: _envelope deserializeTo: @"NSString"];
    [_request send];
    return _request;
}


// Exit Actions...


- (SoapRequest*) AddExitAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    return [self AddExitAction:handler action:nil brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
}

- (SoapRequest*) AddExitAction: (id)_target action: (SEL)_action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetId] forName: @"widgetId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetItemId] forName: @"widgetItemId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: widgetType forName: @"widgetType"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddExitAction" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addExitAction postData: _envelope deserializeTo: @"NSString"];
    
    
    NSLog(@"The AddExitAction log : %@ , %@", _params, _envelope);
    
    
    [_request send];
    return _request;
    
    
    
    
    
}



// Info Action...

- (SoapRequest*) AddInfoAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId action:(NSString *)action deviceType: (NSString *)deviceType
{
    return [self AddInfoAction:handler action:nil brandId:brandId widgetId:widgetId widgetItemId:widgetItemId action:action deviceType:deviceType];
}



- (SoapRequest*) AddInfoAction: (id)_target action: (SEL)_action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId action:(NSString *)action deviceType: (NSString *)deviceType
{
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetId] forName: @"widgetId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetItemId] forName: @"widgetItemId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: action forName: @"action"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddInfoAction" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    
    NSLog(@"The AddInfoAction log : %@ , %@", _params, _envelope);
    
    
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addInfoAction postData: _envelope deserializeTo: @"NSString"];
    [_request send];
    return _request;
}


// Media Action


- (SoapRequest*) AddMediaAction: (id <SoapDelegate>) handler brandId: (long) brandId tabName:(NSString *)tabName deviceType: (NSString *)deviceType
{
    return [self AddMediaAction:handler action:nil brandId:brandId tabName:tabName deviceType:deviceType];
}


- (SoapRequest*) AddMediaAction: (id)_target action: (SEL)_action brandId: (long) brandId tabName:(NSString *)tabName deviceType: (NSString *)deviceType
{
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: tabName forName: @"tabName"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddMediaAction" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    NSLog(@"The AddMediaAction log : %@ , %@", _params, _envelope);
    
    
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addMediaAction postData: _envelope deserializeTo: @"NSString"];
    [_request send];
    return _request;
}


// share action..

- (SoapRequest*) AddShareAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType
{
    return [self AddShareAction:handler action:nil brandId:brandId widgetId:widgetId widgetItemId:widgetItemId actionType:actionType action:action status:status deviceType:deviceType];
    
}


- (SoapRequest*) AddShareAction: (id)_target action: (SEL)_action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType
{
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetId] forName: @"widgetId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: widgetItemId] forName: @"widgetItemId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: actionType forName: @"actionType"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: action forName: @"action"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: status forName: @"status"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddShareAction" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    
    NSLog(@"The AddShareAction log : %@ , %@", _params, _envelope);
    
    
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addShareAction postData: _envelope deserializeTo: @"NSString"];
    [_request send];
    return _request;
}

- (SoapRequest*) AddVolunteerRegisterAction: (id <SoapDelegate>) handler brandId: (long) brandId volunteerId:(long)volunteerId status: (NSString *)status deviceType: (NSString *)deviceType
{
    return [self AddVolunteerRegisterAction:handler action:nil brandId:brandId volunteerId:volunteerId status:status deviceType:deviceType];
    
}


- (SoapRequest*) AddVolunteerRegisterAction: (id)_target action: (SEL)_action brandId: (long) brandId volunteerId:(long)volunteerId status: (NSString *)status deviceType: (NSString *)deviceType
{
    NSMutableArray* _params = [NSMutableArray array];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: brandId] forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: [NSNumber numberWithLong: volunteerId] forName: @"volunteerId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue: status forName: @"status"] autorelease]];

    [_params addObject: [[[SoapParameter alloc] initWithValue: deviceType forName: @"deviceType"] autorelease]];
    ;
    
    NSString* _envelope = [Soap createEnvelope: @"AddVolunteerRegisterAction" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    
    NSLog(@"The AddVolunteerRegisterAction log : %@ , %@", _params, _envelope);
    
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addVolunteerRegisterAction postData: _envelope deserializeTo: @"NSString"];
    [_request send];
    return _request;
}






@end
