//
//  SDZEXAnalyticsService.h
//  Exceeding
//
//  Created by Rajesh Kumar on 29/01/13.
//
//

#import "Soap.h"


/*
 <brandId>long</brandId>
 <widgetId>long</widgetId>
 <widgetItemId>long</widgetItemId>
 <actionType>string</actionType>
 <action>string</action>
 <status>string</status>
 <deviceType>string</deviceType>
 */



@interface SDZEXAnalyticsService : SoapService

+ (SDZEXAnalyticsService*) service;
+ (SDZEXAnalyticsService*) serviceWithUsername: (NSString*) username andPassword: (NSString*) password;



// Entry Action....


- (SoapRequest*) AddSessionAction: (id <SoapDelegate>) handler brandId: (long) brandId sessionInSec:(float)sec deviceType: (NSString *)deviceType;


- (SoapRequest*) AddSessionAction: (id) target action: (SEL) action brandId: (long) brandId sessionInSec:(float)sec deviceType: (NSString *)deviceType;


/* Returns NSString*.  */

// Entry Action....


- (SoapRequest*) AddEntryAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;


- (SoapRequest*) AddEntryAction: (id) target action: (SEL) action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;


//Audio entry action

- (SoapRequest*) AddAudioEntryAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;


- (SoapRequest*) AddAudioEntryAction: (id) target action: (SEL) action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;


// Exit Action....


- (SoapRequest*) AddExitAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;


- (SoapRequest*) AddExitAction: (id) target action: (SEL) action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;


//  AddInfo Action...

- (SoapRequest*) AddInfoAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId action:(NSString *)action deviceType: (NSString *)deviceType;


- (SoapRequest*) AddInfoAction: (id) target action: (SEL) action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId action:(NSString *)action deviceType: (NSString *)deviceType;








// Media Action....


- (SoapRequest*) AddMediaAction: (id <SoapDelegate>) handler brandId: (long) brandId tabName:(NSString *)tabName deviceType: (NSString *)deviceType;


- (SoapRequest*) AddMediaAction: (id) target action: (SEL) action brandId: (long) brandId tabName:(NSString *)tabName deviceType: (NSString *)deviceType;

// Share Action

- (SoapRequest*) AddShareAction: (id <SoapDelegate>) handler brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;


- (SoapRequest*) AddShareAction: (id) target action: (SEL) action brandId: (long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;

/*
 <brandId>long</brandId>
 <volunteerId>long</volunteerId>
 <status>string</status>
 <deviceType>string</deviceType>
 */

//

- (SoapRequest*) AddVolunteerRegisterAction: (id <SoapDelegate>) handler brandId: (long) brandId volunteerId:(long)volunteerId status: (NSString *)status deviceType: (NSString *)deviceType;


- (SoapRequest*) AddVolunteerRegisterAction: (id) target action: (SEL) action brandId: (long) brandId volunteerId:(long)volunteerId status: (NSString *)status deviceType: (NSString *)deviceType;

//Banners analytics

/*
- (SoapRequest*) AddBannerRegisterAction: (id <SoapDelegate>) handler brandId: (long) brandId bannerId:(long)bannerId status: (NSString *)status deviceType: (NSString *)deviceType;


- (SoapRequest*) AddBannerRegisterAction: (id) target action: (SEL) action brandId: (long) brandId bannerId:(long)bannerId status: (NSString *)status deviceType: (NSString *)deviceType;


- (SoapRequest*) AddBannerRegisterAction: (id <SoapDelegate>) handler brandId: (long) brandId bannerWidgetId: (long)bannerWidgetId bannerId: (long)bannerId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;


- (SoapRequest*) AddBannerRegisterAction: (id) target action: (SEL) action brandId: (long) brandId widgetId: (long)widgetId bannerId: (long)bannerId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;


- (SoapRequest*) AddBannerRegisterAction: (id <SoapDelegate>) handler brandId: (long) brandId bannerWidgetId:(long)bannerWidgetId status: (NSString *)status deviceType: (NSString *)deviceType;

- (SoapRequest*) AddBannerRegisterAction: (id)_target action: (SEL)_action brandId: (long) brandId bannerWidgetId:(long)bannerWidgetId status: (NSString *)status deviceType: (NSString *)deviceType;

 
 - (SoapRequest*) AddBannerRegisterAction: (id <SoapDelegate>) handler brandId: (long) brandId bannerWidgetId: (long)bannerWidgetId bannerId: (long)bannerId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;
 
 
 - (SoapRequest*) AddBannerRegisterAction: (id) target action: (SEL) action brandId: (long) brandId bannerWidgetId: (long)bannerWidgetId bannerId: (long)bannerId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;
*/

- (SoapRequest*) AddBannerRegisterAction: (id <SoapDelegate>) handler brandId: (long) brandId bannerWidgetId:(long)bannerWidgetId status: (NSString *)status deviceType: (NSString *)deviceType;

- (SoapRequest*) AddBannerRegisterAction: (id)_target action: (SEL)_action brandId: (long) brandId bannerWidgetId:(long)bannerWidgetId status: (NSString *)status deviceType: (NSString *)deviceType;

@end
