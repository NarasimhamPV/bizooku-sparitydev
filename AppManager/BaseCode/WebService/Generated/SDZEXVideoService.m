//
//  SDZEXVideoService.m
//  Exceeding
//
//  Created by Veeru on 10/11/14.
//
//

#import "SDZEXVideoService.h"
#import "ConstantsWebService.h"
#import "AppDelegate.h"
#import "Soap.h"
#import "SDZEXVideoService.h"


@implementation SDZEXVideoService

- (id) init
{
    if(self = [super init])
    {
        
        self.serviceUrl = [NSString stringWithFormat:@"%@%@",kWebServiceURL, @"Services/EXVideoService.asmx"];
        
        self.namespace = @"http://tempuri.org/";
        self.headers = nil;
        self.logging = NO;
    }
    return self;
}

- (id) initWithUsername: (NSString*) username andPassword: (NSString*) password {
    if(self = [super initWithUsername:username andPassword:password]) {
    }
    return self;
}

+ (SDZEXVideoService*) service {
    return [SDZEXVideoService serviceWithUsername:nil andPassword:nil];
}

+ (SDZEXVideoService*) serviceWithUsername: (NSString*) username andPassword: (NSString*) password {
    return [[[SDZEXVideoService alloc] initWithUsername:username andPassword:password] autorelease];
}





- (SoapRequest*) GetVideoCategories: (id <SoapDelegate>) handler BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry
{
    return [self GetVideoCategories:handler action:nil BrandTileId:BrandTileId isEntry:isEntry];
    
}

- (SoapRequest*) GetVideoCategories: (id) _target action: (SEL) _action BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry
{
    NSMutableArray* _params = [NSMutableArray array];
    NSString *brandID = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandId"];
    NSString *brandTileId = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandTileId"];
    NSLog(@"brandTileId %@",brandTileId);
    [_params addObject: [[[SoapParameter alloc] initWithValue:brandID forName: @"BrandId"] autorelease]];

    [_params addObject: [[[SoapParameter alloc] initWithValue:brandTileId forName: @"BrandTileId"] autorelease]];

    NSString* _envelope = [Soap createEnvelope: @"GetCategoriesByBrand" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: getCategoriesByBrand postData: _envelope deserializeTo: @"NSString"];
    
    [_request send];
    
    return _request;
    
}


- (SoapRequest*) GetVideoList: (id <SoapDelegate>) handler BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry
{
    return [self GetVideoList:handler action:nil BrandTileId:BrandTileId isEntry:isEntry];
}
- (SoapRequest*) GetVideoList: (id) _target action: (SEL) _action BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry
{
    NSMutableArray* _params = [NSMutableArray array];
    NSString *brandID = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandId"];
    NSLog(@"brandID %@",brandID);
    
    NSString *videoCategoryId = [[NSUserDefaults standardUserDefaults]objectForKey:@"VideoCategoryId"];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue:brandID forName: @"BrandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:videoCategoryId forName: @"CategoryId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:[AppDelegate shareddelegate].deviceID forName: @"DeviceId"] autorelease]];

    
    
    NSString* _envelope = [Soap createEnvelope: @"GetVideosByCategoryId" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction:getVideosByCategoryId postData: _envelope deserializeTo: @"NSString"];
    
    [_request send];
    
    return _request;
}


- (SoapRequest*) GetVideoDetails: (id <SoapDelegate>) handler videoID: (NSString *) vedioID isEntry: (BOOL) isEntry
{
    return [self GetVideoDetails:handler action:nil videoID:vedioID isEntry:isEntry];

}
- (SoapRequest*) GetVideoDetails: (id) _target action: (SEL) _action videoID: (NSString *) videoID isEntry: (BOOL) isEntry
{
    NSMutableArray* _params = [NSMutableArray array];
    
    
    
    
    [_params addObject: [[[SoapParameter alloc] initWithValue:videoID forName: @"videoId"] autorelease]];
    
    
    
    NSString* _envelope = [Soap createEnvelope: @"GetVideoById" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction:getVideoById postData: _envelope deserializeTo: @"NSString"];
    
    [_request send];
    
    return _request;
}




- (SoapRequest*) GetUserFavorite: (id <SoapDelegate>) handler  isEntry: (BOOL) isEntry
{
    return [self GetUserFavorite:handler action:nil isEntry:isEntry];

}
- (SoapRequest*) GetUserFavorite: (id) _target action: (SEL) _action  isEntry: (BOOL) isEntry
{
    NSMutableArray* _params = [NSMutableArray array];
    
    NSString *brandID = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandId"];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue:brandID forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:[AppDelegate shareddelegate].deviceID forName: @"deviceId"] autorelease]];
    
    
    NSString* _envelope = [Soap createEnvelope: @"GetUserFavorites" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: getUserFavorites postData: _envelope deserializeTo: @"NSString"];
    
    [_request send];
    
    return _request;
}


- (SoapRequest*) GetAddFavoriteOrder: (id <SoapDelegate>) handler FevOrderList: (NSString *) FevOrderList isEntry: (BOOL) isEntry
{
    return [self GetAddFavoriteOrder:handler FevOrderList:FevOrderList isEntry:isEntry];
}
- (SoapRequest*) GetAddFavoriteOrder: (id) _target action: (SEL) _action FevOrderList: (NSString *) FevOrderList isEntry: (BOOL) isEntry
{
    //UpdateVideoFavouritesOrder(long brandId, string deviceId,string VideoIds)
    
    NSMutableArray* _params = [NSMutableArray array];
    
    NSString *brandID = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandId"];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue:brandID forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:[AppDelegate shareddelegate].deviceID forName: @"deviceId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:FevOrderList forName: @"VideoIds"] autorelease]];

    
    NSString* _envelope = [Soap createEnvelope: @"UpdateVideoFavouritesOrder" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: updateVideoFavouritesOrder postData: _envelope deserializeTo: @"NSString"];
    
    [_request send];
    
    return _request;
}




//Add Fevoriates Functionality
- (SoapRequest*) GetAddFavorite: (id <SoapDelegate>) handler VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry
{
    return [self GetAddFavorite:handler action:nil VideoID:VideoID isEntry:isEntry];
}
- (SoapRequest*) GetAddFavorite: (id) _target action: (SEL) _action VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry
{
    NSMutableArray* _params = [NSMutableArray array];
    
    NSString *brandID = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandId"];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue:brandID forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:[AppDelegate shareddelegate].deviceID forName: @"deviceId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:VideoID forName: @"videoId"] autorelease]];
    
    //videoId
    
    NSString* _envelope = [Soap createEnvelope: @"AddFavorite" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: addFavorite postData: _envelope deserializeTo: @"NSString"];
    

    
    [_request send];
    
    return _request;
}




- (SoapRequest*) GetDeleteFavorite: (id <SoapDelegate>) handler VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry
{
    return [self GetDeleteFavorite:handler action:nil VideoID:VideoID isEntry:isEntry];

}
- (SoapRequest*) GetDeleteFavorite: (id) _target action: (SEL) _action VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry
{
    NSMutableArray* _params = [NSMutableArray array];
    
    NSString *brandID = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandId"];
    
    [_params addObject: [[[SoapParameter alloc] initWithValue:brandID forName: @"brandId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:[AppDelegate shareddelegate].deviceID forName: @"deviceId"] autorelease]];
    [_params addObject: [[[SoapParameter alloc] initWithValue:VideoID forName: @"videoId"] autorelease]];

    //videoId
    
    NSString* _envelope = [Soap createEnvelope: @"DeleteFavorite" forNamespace: self.namespace withParameters: _params withHeaders: self.headers];
    SoapRequest* _request = [SoapRequest create: _target action: _action service: self soapAction: deleteFavorite postData: _envelope deserializeTo: @"NSString"];
    
    [_request send];
    
    return _request;
}




@end
