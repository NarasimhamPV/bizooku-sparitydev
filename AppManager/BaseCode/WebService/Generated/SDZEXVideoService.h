//
//  SDZEXVideoService.h
//  Exceeding
//
//  Created by Veeru on 10/11/14.
//
//

#import <Foundation/Foundation.h>
#import "Soap.h"


@interface SDZEXVideoService : SoapService



+ (SDZEXVideoService*) service;
+ (SDZEXVideoService*) serviceWithUsername: (NSString*) username andPassword: (NSString*) password;

//Get Video Categories
- (SoapRequest*) GetVideoCategories: (id <SoapDelegate>) handler BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry;
- (SoapRequest*) GetVideoCategories: (id) _target action: (SEL) _action BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry;


//Get Video Categories
- (SoapRequest*) GetVideoList: (id <SoapDelegate>) handler BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry;
- (SoapRequest*) GetVideoList: (id) _target action: (SEL) _action BrandTileId: (long) BrandTileId isEntry: (BOOL) isEntry;


//Get AddUserFavorite
- (SoapRequest*) GetUserFavorite: (id <SoapDelegate>) handler  isEntry: (BOOL) isEntry;
- (SoapRequest*) GetUserFavorite: (id) _target action: (SEL) _action  isEntry: (BOOL) isEntry;


//Get DeleteFavorite
- (SoapRequest*) GetAddFavorite: (id <SoapDelegate>) handler VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry;
- (SoapRequest*) GetAddFavorite: (id) _target action: (SEL) _action VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry;


//Get DeleteFavorite
- (SoapRequest*) GetDeleteFavorite: (id <SoapDelegate>) handler VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry;
- (SoapRequest*) GetDeleteFavorite: (id) _target action: (SEL) _action VideoID: (NSString *) VideoID isEntry: (BOOL) isEntry;

//Get DeleteFavorite
- (SoapRequest*) GetAddFavoriteOrder: (id <SoapDelegate>) handler FevOrderList: (NSString *) FevOrderList isEntry: (BOOL) isEntry;
- (SoapRequest*) GetAddFavoriteOrder: (id) _target action: (SEL) _action FevOrderList: (NSString *) FevOrderList isEntry: (BOOL) isEntry;


- (SoapRequest*) GetVideoDetails: (id <SoapDelegate>) handler videoID: (NSString *) vedioID isEntry: (BOOL) isEntry;
- (SoapRequest*) GetVideoDetails: (id) _target action: (SEL) _action videoID: (NSString *) videoID isEntry: (BOOL) isEntry;


@end
