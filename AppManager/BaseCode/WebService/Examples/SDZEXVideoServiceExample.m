//
//  SDZEXVideoServiceExample.m
//  Exceeding
//
//  Created by Veeru on 10/11/14.
//
//

#import "SDZEXVideoServiceExample.h"
#import "SDZEXVideoService.h"

#import "AppDelegate.h"
#import "ViewController.h"


BOOL isloginEnabled = NO;

@implementation SDZEXVideoServiceExample


- (void)runVideoCategeory:(NSInteger)bransTileId
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXVideoService* service = [SDZEXVideoService service];
	service.logging = isloginEnabled;
    [service GetVideoCategories:self action:@selector(GetVideoCategeoryHandler:) BrandTileId:bransTileId isEntry:YES];
    
    [pool release];
}

-(void) GetVideoCategeoryHandler: (id) value
{
    NSLog(@"value %@",value);
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_VIDEO) {
            
            [AppDelegate shareddelegate].viewController.appTableVC.dataSourceDic = jsonResponse;
            
        }
    }
    
}


- (void)runVideoList:(NSInteger)bransTileId
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];

    SDZEXVideoService* service = [SDZEXVideoService service];
	service.logging = isloginEnabled;
    [service GetVideoList:self action:@selector(GetVideoListHandler:) BrandTileId:bransTileId isEntry:YES];
    
    [pool release];
}

-(void) GetVideoListHandler: (id) value
{
    NSLog(@"value %@",value);
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_VIDEO) {
            [AppDelegate shareddelegate].videoListDict = jsonResponse;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getVideoListNotification" object:nil];
            
        }
    }
    
}


- (void)runVideoDetail:(NSString *)videoID
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
    SDZEXVideoService* service = [SDZEXVideoService service];
	service.logging = isloginEnabled;
    [service GetVideoDetails:self action:@selector(GetVideoDetailHandler:) videoID:videoID isEntry:YES];
    
    [pool release];
}

-(void) GetVideoDetailHandler: (id) value
{
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_VIDEO) {
            [AppDelegate shareddelegate].videoDetailDict = jsonResponse;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getVideoDetailNotification" object:nil];
            
        }
    }
    
}



- (void)runUserFevorates
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
    SDZEXVideoService* service = [SDZEXVideoService service];
	service.logging = isloginEnabled;
    [service GetUserFavorite:self action:@selector(GetUserFevrotesHandle:) isEntry:YES];
    
    
    
    [pool release];
}
- (void)GetUserFevrotesHandle:(id)value
{
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_VIDEO) {
            [AppDelegate shareddelegate].userFevVideoDict = jsonResponse;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getUserFevVideoListNotification" object:nil];
            
        }
    }
}
- (void)runUserFevoratesOrderChanged:(NSString *)order
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
    SDZEXVideoService* service = [SDZEXVideoService service];
	service.logging = isloginEnabled;
    [service GetAddFavoriteOrder:self action:@selector(GetUserFevrotesOrderHandle:) FevOrderList:order isEntry:YES];
    
    
    
    [pool release];
}
- (void)GetUserFevrotesOrderHandle:(id)value
{
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    NSLog(@"jsonResponse %@",jsonResponse);
}

//Add Fevoriate Functionality

- (void)runAddFavorite:(NSString *)videoId
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
    SDZEXVideoService* service = [SDZEXVideoService service];
	service.logging = isloginEnabled;
    [service GetAddFavorite:self action:@selector(GetAddFavorite:) VideoID:videoId isEntry:YES];
    [pool release];
}
- (void)GetAddFavorite: (id) value
{
    [[AppDelegate shareddelegate] updateHUDActivity:nil show:NO];

    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_VIDEO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getAddFevNotification" object:nil];
            
        }
    }
}



//Delete fevorite Functionality
- (void)runDeleteFevorite:(NSString *)videoID
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
    SDZEXVideoService* service = [SDZEXVideoService service];
	service.logging = isloginEnabled;
    [service GetDeleteFavorite:self action:@selector(GetUpdateFevoriteHandler:) VideoID:videoID isEntry:YES];
    [pool release];
}


-(void) GetUpdateFevoriteHandler: (id) value
{
    NSLog(@"value %@",value);
    [[AppDelegate shareddelegate] updateHUDActivity:nil show:NO];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"getDeleteFevNotification" object:nil];

    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_VIDEO) {
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"getDeleteFevNotification" object:nil];

        }
    }
    
}


@end
