//
//  SDZEXVideoServiceExample.h
//  Exceeding
//
//  Created by Veeru on 10/11/14.
//
//

#import <Foundation/Foundation.h>

@interface SDZEXVideoServiceExample : NSObject

- (void)runVideoCategeory:(NSInteger)bransTileId;

- (void)runVideoList:(NSInteger)bransTileId;
- (void)runAddFavorite:(NSString *)videoId;
- (void)runUserFevorates;
- (void)runUserFevoratesOrderChanged:(NSString *)order;
- (void)runVideoDetail:(NSString *)videoID;


- (void)runDeleteFevorite:(NSString *)videoID;

@end
