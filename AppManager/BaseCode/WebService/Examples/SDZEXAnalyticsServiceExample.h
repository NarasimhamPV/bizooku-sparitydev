//
//  SDZEXAnalyticsServiceExample.h
//  Exceeding
//
//  Created by Rajesh Kumar on 29/01/13.
//
//



@interface SDZEXAnalyticsServiceExample : NSObject
- (void)runCoupons;
- (void)getRedeemData;
-(void)runCouponsList;
-(void) runcouponsDetails;


- (void)runAddEntryAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;

- (void)runAddExitAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType;

- (void)runAddInfoAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId action:(NSString *)action deviceType: (NSString *)deviceType;

- (void)runAddMediaAction:(long) brandId tabName:(NSString *)tabName deviceType: (NSString *)deviceType;

- (void)runAddShareAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;

- (void)runAddVolunteerRegisterAction: (long) brandId volunteerId:(long)volunteerId status: (NSString *)status deviceType: (NSString *)deviceType;


- (void)runAddSession:(long) brandId timeInSec:(float)sec deviceType: (NSString *)deviceType;

// Banner
//- (void)runAddBannerRegisterAction: (long) brandId bannerId:(long)bannerId status: (NSString *)status deviceType: (NSString *)deviceType;



//- (void)runAddBannerRegisterAction:(long) brandId widgetId: (long)widgetId bannerId: (long)bannerId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType;


- (void)runAddBannerRegisterAction: (long) brandId bannerWidgetId:(long)bannerWidgetId status: (NSString *)status deviceType: (NSString *)deviceType;

@end
