//
//  SDZEXAnalyticsServiceExample.m
//  Exceeding
//
//  Created by Rajesh Kumar on 29/01/13.
//
//

#import "SDZEXAnalyticsServiceExample.h"
#import "SDZEXAnalyticsService.h"


#import "AppDelegate.h"
#import "NSString+SBJSON.h"
#import "DashboardBuilder.h"
#import "ViewController.h"

BOOL loggingEnabled = NO;


@implementation SDZEXAnalyticsServiceExample

- (void)runAddSession:(long) brandId timeInSec:(float)sec deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    //Rajesh..
  //  [service AddEntryAction:self action:@selector(getAddEntryActionHandler:) brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
    [service AddSessionAction:self action:@selector(getAddSessionActionHandler:) brandId:brandId sessionInSec:sec deviceType:deviceType];
    
    
    [pool release];
}


- (void)runAddEntryAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    //Rajesh..
    [service AddEntryAction:self action:@selector(getAddEntryActionHandler:) brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
    
    [pool release];
}

- (void)runAddBannerEntryAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    //Rajesh..
    [service AddEntryAction:self action:@selector(getAddEntryActionHandler:) brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
    
    [pool release];
}


- (void)runAddExitAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    //Rajesh..
    
    [service AddExitAction:self action:@selector(getAddExitActionHandler:) brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
    
    [pool release];
}

- (void)runAddBannerExitAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId widgetType:(NSString *)widgetType bannerId: (long)bannerId deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    //Rajesh..
    
    [service AddExitAction:self action:@selector(getAddExitActionHandler:) brandId:brandId widgetId:widgetId widgetItemId:widgetItemId widgetType:widgetType deviceType:deviceType];
    
    [pool release];
}


- (void)runAddInfoAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId action:(NSString *)action deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    //Rajesh..
    
    [service AddInfoAction:self action:@selector(getAddInfoActionHandler:) brandId:brandId widgetId:widgetId widgetItemId:widgetItemId action:action deviceType:deviceType];
    
    [pool release];
}


- (void)runAddMediaAction:(long) brandId tabName:(NSString *)tabName deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    
    [service AddMediaAction:self action:@selector(getAddMediaActionHandler:) brandId:brandId tabName:tabName deviceType:deviceType];
    
    [pool release];
}

- (void)runAddShareAction:(long) brandId widgetId: (long)widgetId widgetItemId: (long)widgetItemId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    
    [service AddShareAction:self action:@selector(getAddShareActionHandler:) brandId:brandId widgetId:widgetId widgetItemId:widgetItemId actionType:actionType action:action status:status deviceType:deviceType];
    
    
    [pool release];
}



- (void)runAddVolunteerRegisterAction: (long) brandId volunteerId:(long)volunteerId status: (NSString *)status deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
	// service.username = @"username";
	// service.password = @"password";
	
    
	// Returns NSString*
    
    [service AddVolunteerRegisterAction:self action:@selector(getAddVolunteerRegisterActionHandler:) brandId:brandId volunteerId:volunteerId status:status deviceType:deviceType];
    
    
    [pool release];
}


#pragma conncetion delegates....

-(void)getAddVolunteerRegisterActionHandler: (id) value {
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        NSLog(@"getAddVolunteerRegisterActionHandler: %@", value);
        // if ([AppDelegate shareddelegate].viewController.appType == APP_VOLUNTEER)
        {
            NSString *str = (NSString*)value;
            if (str && [str isKindOfClass:[NSString class]]) {
                if ([str compare:@"Success" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                    
                    [[AppDelegate shareddelegate].viewController.appTableVC.appDetailVC reloadUsingWebService];
                }
            }
        }
    }
}

//Banner

- (void)runAddBannerRegisterAction:(long) brandId widgetId: (long)widgetId bannerId: (long)bannerId bannerWidgetId:(long)bannerWidgetId actionType:(NSString *)actionType action:(NSString *)action status:(NSString *)status deviceType: (NSString *)deviceType
{
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
    
    //    [service AddBannerRegisterAction:self action:@selector(getAddBannerRegisterActionHandler:) brandId:brandId bannerId:bannerId status:status deviceType:deviceType];
    
    
    [service AddBannerRegisterAction:self action:@selector(getAddBannerRegisterActionHandler:) brandId:brandId bannerWidgetId:bannerWidgetId status:status deviceType:deviceType];
    
    
    [pool release];
}




/*
- (void)runAddBannerRegisterAction: (long) brandId bannerId:(long)bannerId status: (NSString *)status deviceType: (NSString *)deviceType
{
    
    
    NSAutoreleasePool *pool = [[ NSAutoreleasePool alloc] init];
    
	// Create the service
	SDZEXAnalyticsService* service = [SDZEXAnalyticsService service];
	service.logging = loggingEnabled;
		
	// Returns NSString*
    
    [service AddBannerRegisterAction:self action:@selector(getAddBannerRegisterActionHandler:) brandId:brandId bannerId:bannerId status:status deviceType:deviceType];
    
    
    [pool release];
    
}
*/

- (void)getAddBannerRegisterActionHandler:(id)value
{
    
    
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        NSLog(@"getAddBannerRegisterActionHandler: %@", value);
        
        {
            NSString *str = (NSString*)value;
            if (str && [str isKindOfClass:[NSString class]]) {
                if ([str compare:@"Success" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                    
                    [[AppDelegate shareddelegate].viewController.appTableVC.appDetailVC reloadUsingWebService];
                }
            }
        }
    }

    
    
}




-(void)getAddShareActionHandler: (id) value {
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        NSLog(@"getAddShareActionHandler: %@", value);
        // if ([AppDelegate shareddelegate].viewController.appType == APP_VOLUNTEER)
        {
            NSString *str = (NSString*)value;
            if (str && [str isKindOfClass:[NSString class]]) {
                if ([str compare:@"Success" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                    
                    [[AppDelegate shareddelegate].viewController.appTableVC.appDetailVC reloadUsingWebService];
                }
            }
        }
    }
}

-(void)getAddExitActionHandler: (id) value {
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        NSLog(@"getAddExitActionHandler: %@", value);
        // if ([AppDelegate shareddelegate].viewController.appType == APP_VOLUNTEER)
        {
            NSString *str = (NSString*)value;
            if (str && [str isKindOfClass:[NSString class]]) {
                if ([str compare:@"Success" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                    
                   // [[AppDelegate shareddelegate].viewController.appTableVC.appDetailVC reloadUsingWebService];
                }
            }
        }
    }
}

-(void)getAddMediaActionHandler: (id) value {
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        NSLog(@"getAddMediaActionHandler: %@", value);
        // if ([AppDelegate shareddelegate].viewController.appType == APP_VOLUNTEER)
        {
            NSString *str = (NSString*)value;
            if (str && [str isKindOfClass:[NSString class]]) {
                if ([str compare:@"Success" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                    
                    [[AppDelegate shareddelegate].viewController.appTableVC.appDetailVC reloadUsingWebService];
                }
            }
        }
    }
}

-(void)getAddEntryActionHandler: (id) value {
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        //NSLog(@"getAddEntryActionHandler: %@", value);
       // if ([AppDelegate shareddelegate].viewController.appType == APP_VOLUNTEER)
        {
            NSString *str = (NSString*)value;
            if (str && [str isKindOfClass:[NSString class]]) {
                if ([str compare:@"Success" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                    
                    [[AppDelegate shareddelegate].viewController.appTableVC.appDetailVC reloadUsingWebService];
                }
            }
        }
    }
}

-(void)getAddSessionActionHandler: (id) value {
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        NSLog(@"getAddSessionActionHandler: %@", value);
        // if ([AppDelegate shareddelegate].viewController.appType == APP_VOLUNTEER)
        {
            
        }
    }
}


//Sparity
- (void)GetCouponsHandler:(id)value
{
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_COUPONS) {
            NSLog(@"GetCouponsHandler: %@", jsonResponse);
            
            [AppDelegate shareddelegate].couponsdict = jsonResponse;
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardMyBets]forKey:@"viewNumber"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getCouponsNotification" object:nil];
            
            
            //            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(balanceUpdated:) name:@"BalanceUpdated" object:nil];
            //self.couponsViewCntr = [[SPCouponsViewController alloc] initWithNibName:@"SPCouponsViewController" bundle:nil];
            //self.couponsViewCntr.couponDict = jsonResponse;
            NSLog(@"jsonResponse %@",jsonResponse);
        }
    }
    
}


// Sparity_Banners



- (void)GetCouponsListHandler:(id)value
{
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        if ([AppDelegate shareddelegate].viewController.appType == APP_COUPONSLIST) {
            //NSLog(@"GetCouponsHandler: %@", jsonResponse);
            
            [AppDelegate shareddelegate].couponsListDict = jsonResponse;
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardMyBets]forKey:@"viewNumber"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getCouponsListNotification" object:nil];
            
            
            NSLog(@"jsonResponse %@",jsonResponse);
        }
    }
    
}


- (void)GetCouponsDetailsHandler:(id)value
{
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:YES];
    if (jsonResponse) {
        [AppDelegate shareddelegate].couponsDetailDict = jsonResponse;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getCouponsDetailsNotification" object:nil];
        if ([AppDelegate shareddelegate].viewController.appType == APP_COUPONSDETAILS) {
            //NSLog(@"GetCouponsHandler: %@", jsonResponse);
            
            
        }
    }
    
}



-(void)getAddInfoActionHandler: (id) value {
    NSDictionary *jsonResponse = [AppDelegate isValidSoapResponse:value showAlert:NO];
    if (!jsonResponse && value) {
        NSLog(@"getAddInfoActionHandler: %@", value);
        // if ([AppDelegate shareddelegate].viewController.appType == APP_VOLUNTEER)
        {
            NSString *str = (NSString*)value;
            if (str && [str isKindOfClass:[NSString class]]) {
                if ([str compare:@"Success" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                    
                    [[AppDelegate shareddelegate].viewController.appTableVC.appDetailVC reloadUsingWebService];
                }
            }
        }
    }
}


@end
