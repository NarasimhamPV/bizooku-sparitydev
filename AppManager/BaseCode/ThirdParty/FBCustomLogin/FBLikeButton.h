//
//  FBLikeButton.h
//  jefftest
//
//  Created by Sourabh Verma on 12/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Facebook.h"

//FBLikeButton.h
#define FB_LIKE_BUTTON_LOGIN_NOTIFICATION @"FBLikeLoginNotification"

typedef enum {
    FBLikeButtonStyleStandard,
    FBLikeButtonStyleButtonCount,
    FBLikeButtonStyleBoxCount
} FBLikeButtonStyle;

typedef enum {
    FBLikeButtonColorLight,
    FBLikeButtonColorDark
} FBLikeButtonColor;

@interface FBLikeButton : UIView <UIWebViewDelegate /*, FBDialogDelegate*/> {
    
    UIWebView *webView_;
    
    NSString *textColor_;
    NSString *linkColor_;
    NSString *buttonColor_;
}

@property(retain) NSString *textColor;
@property(retain) NSString *linkColor;
@property(retain) NSString *buttonColor;

- (id)initWithFrame:(CGRect)frame andUrl:(NSString *)likePage andStyle:(FBLikeButtonStyle)style andColor:(FBLikeButtonColor)color;
- (id)initWithFrame:(CGRect)frame andUrl:(NSString *)likePage;

@end
