//
//  FBCustomLoginDialog.m
//  jefftest
//
//  Created by Sourabh Verma on 12/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FBCustomLoginDialog.h"

@implementation FBCustomLoginDialog

- (id)initWithFrame:(CGRect)frame
{
    //self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    NSURL* url = request.URL;
    if ([[url absoluteString] rangeOfString:@"login"].location==NSNotFound) {
       // [self dialogDidSucceed:url];
        return NO;
    }else if (url!=nil){
//        [_spinner startAnimating];
//        [_spinner setHidden:NO];
        return YES;
    }
    return NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
