//
//  ExFBLike.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 13/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ExFBLike.h"
#import "FBIbee.h"

#define kExFBLikeURL @"https://www.facebook.com/plugins/like.php?href=https://www.facebook.com/%@&send=false&layout=box_count&width=50&show_faces=false&action=like&colorscheme=light&font&height=60&appId=%@"


@interface ExFBLike ()
@property (nonatomic, retain) NSString *appID;
@property (nonatomic, retain) NSString *postID;

@end

@implementation ExFBLike

@synthesize appID = _appID;
@synthesize postID = _postID;

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
         self.delegate = self;
        [self listenForLogin];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.delegate = self;
    }
    return self;
}

-(void)loadWithAppID:(NSString*)appID posiID:(NSString*)postID{

    self.appID = appID;
    self.postID = postID;
    
    [[FBIbee sharedDelegate] logFBEvent:[NSString stringWithFormat:@"Load Like Page: postID: %@", postID] ];
    
    /*
    if (!postID || ![postID length]) {
        [[FBIbee sharedDelegate] logFBEvent:@"Hide Like view since post ID doesnt exist"];
        self.hidden = YES;
    }
    else{
        NSString *likeStr = [NSString stringWithFormat:kExFBLikeURL, postID, [FBIbee sharedDelegate].fbAppID];
        [self loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:likeStr]]];
    }
     */

}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    NSLog(@"Load: %@", [[request URL] absoluteString]);

    if ([[[request URL] absoluteString] isEqualToString:@"https://www.facebook.com/plugins/like/connect"] ) {
        [FBIbee sharedDelegate].postAfterLogin = NO;
        if (![[FBIbee sharedDelegate] loginFacebook:nil]) {
            [[FBIbee sharedDelegate] logFBEvent:@"FBLike Login: Deny loading"];
            return NO;
        }
    }
    
    return YES;
}

-(void)listenForLogin{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fbloggedIn:) name:@"fbDidLoginNotification" object:nil];
}

-(void)fbloggedIn:(id)sender{
    [[FBIbee sharedDelegate] logFBEvent:@"Load Like view again after login"];
    [self loadWithAppID:self.appID posiID:self.postID];
}
     
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
