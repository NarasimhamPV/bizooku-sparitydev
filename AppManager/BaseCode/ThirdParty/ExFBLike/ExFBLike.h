//
//  ExFBLike.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 13/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExFBLike : UIWebView <UIWebViewDelegate>

-(void)loadWithAppID:(NSString*)appID posiID:(NSString*)postID;

@end
