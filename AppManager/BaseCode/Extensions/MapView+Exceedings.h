//
//  MapView+Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 06/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (Exceedings)

-(void)zoomToPoint:(CGFloat)lat lonfitude:(CGFloat)longitude;

@end
