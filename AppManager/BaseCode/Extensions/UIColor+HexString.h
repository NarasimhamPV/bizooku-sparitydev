//
//  NSDateFormatter_Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 05/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UIColor (HexString)

+ (UIColor *) colorWithHexString: (NSString *) hexString defaultColor:(UIColor*)defColor;

@end
