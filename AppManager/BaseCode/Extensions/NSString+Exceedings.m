//
//  NSString+Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 02/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+Exceedings.h"
#import "NSDateFormatter+Exceedings.h"

@implementation NSString (Exceedings)


-(NSString*)millionsFormatDollarString{
    //long currency = [self longLongValue];
    long currency = [self longLongValue];
  
    NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setCurrencySymbol:@"$"];
    
    // Added these two lines for supporting the US Dollars format....
    
    NSLocale *locale = [[[NSLocale alloc] initWithLocaleIdentifier: @"en_US"] autorelease];
    [numberFormatter setLocale:locale];

    
    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithInt:currency]];
    
    if ([numberAsString length] - 3 > 0) {
        numberAsString = [numberAsString substringToIndex:[numberAsString length] - 3];
    }
    
    return numberAsString;
}
 



-(NSDate*)exceedingsDateFromServerNewsString{
    //Remove T
    NSString *cleanStr = [self stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    //Remove .xxx
    NSRange range = [cleanStr rangeOfString:@"."];
    if (range.location != NSNotFound) {
        cleanStr = [cleanStr substringToIndex:range.location];
    }
    
    //If format is yyyy/mm/dd. COnvert to yyyy-mm-dd
    cleanStr = [cleanStr stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    
    //Now format date
    NSDateFormatter *dateFormatter = [NSDateFormatter exceedingNewsServerDateFormat];
    
    return [dateFormatter dateFromString:cleanStr];
}


-(NSDate*)exceedingsDateFromLocalNewsString{
    //Remove T
    NSString *cleanStr = [self stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    //Remove .xxx
    NSRange range = [cleanStr rangeOfString:@"."];
    if (range.location != NSNotFound) {
        cleanStr = [cleanStr substringToIndex:range.location];
    }
    
    //If format is yyyy/mm/dd. COnvert to yyyy-mm-dd
    cleanStr = [cleanStr stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    
    //Now format date
    NSDateFormatter *dateFormatter = [NSDateFormatter exceedingNewsServerDateFormat];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"PST"]];
    
    return [dateFormatter dateFromString:cleanStr];
}


-(NSString*)convertToUSDateString{
    NSString *newStr = self;
    
    if (self.length > 10) {
        newStr = [self substringToIndex:10];
    }
    
    if (newStr.length == 10) {
        NSString *dt = [newStr substringFromIndex:8];
        NSString *mo = [[newStr substringFromIndex:5] substringToIndex:2];
        NSString *yr = [newStr substringToIndex:4];
        
        return [NSString stringWithFormat:@"%@/%@/%@", dt, mo, yr];
    }
    else{
        return nil;
    }
}

//Be sure to have format dd/mm/yyyy while calling this method. otherwise it will return same string

-(NSString*)addProperExtToStrDate{
    NSDateFormatter *dateFormatter = [NSDateFormatter exceedingEventsDateFormatter];
    NSDateFormatter *onlyDateFormatter = [NSDateFormatter onlyDateDateFormatter];
    
    NSDate *date = [onlyDateFormatter dateFromString:self];
    
    NSString *dateStr = [dateFormatter stringFromDate:date];
    
    //add appropriate ext in date like rd, st, th
    {
        NSDateFormatter *monthDayFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        [monthDayFormatter setDateFormat:@"d"]; 
        int date_day = [[monthDayFormatter stringFromDate:date] intValue];      
        NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
        NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
        NSString *suffix = [suffixes objectAtIndex:date_day]; 
        dateStr = [dateStr stringByAppendingString:suffix];
    }
    
    if (dateStr) {
        return dateStr;
    }
    else {
        return self;
    }
}


@end