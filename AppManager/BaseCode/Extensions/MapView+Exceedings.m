//
//  MapView+Exceedings.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 06/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MapView+Exceedings.h"

#define METERS_PER_MILE 1609.344

@implementation MKMapView (Exceedings)

-(void)zoomToPoint:(CGFloat)lat lonfitude:(CGFloat)longitude{
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = lat;
    zoomLocation.longitude= longitude;
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 4*METERS_PER_MILE, 4*METERS_PER_MILE);
    // 3
    MKCoordinateRegion adjustedRegion = [self regionThatFits:viewRegion];                
    // 4
    [self setRegion:adjustedRegion animated:YES];  
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
