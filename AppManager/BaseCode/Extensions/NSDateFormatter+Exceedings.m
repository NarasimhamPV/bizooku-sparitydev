//
//  NSDateFormatter_Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 05/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSDateFormatter+Exceedings.h"

@implementation NSDate (Exceedings)

-(NSString*)exceedingsDaysAgoString{
    
    NSTimeInterval timeInterval = [self timeIntervalSinceNow];
    if (timeInterval < 0) {
        timeInterval = -timeInterval;
       // timeInterval = timeInterval-54000;
    }
    else {
        //If its future. Keep it zero
        timeInterval = 0;
    }
    
    CGFloat SIXTY_MINUTES = 60*60;
    CGFloat TWENETY_FOUR_HOURS = 24*60*60;
    NSLog(@"TWENETY_FOUR_HOURS %f",TWENETY_FOUR_HOURS);
    NSLog(@"timeInterval %f",timeInterval);
    
    if(timeInterval < SIXTY_MINUTES)
    {
        NSLog(@"integer %d",(NSInteger)(timeInterval/SIXTY_MINUTES));
        return [NSString stringWithFormat:@"%d hours ago", (NSInteger)(timeInterval/SIXTY_MINUTES)];
    }
    else if(timeInterval < TWENETY_FOUR_HOURS)
    {
        int i = (NSInteger)(timeInterval/SIXTY_MINUTES);
        if(i <= 1)
        {
            return [NSString stringWithFormat:@"%d hour ago",i];
        }
        else{
            return [NSString stringWithFormat:@"%d hours ago",i];
        }
        
        //return [NSString stringWithFormat:@"%d hours ago", (NSInteger)(timeInterval/SIXTY_MINUTES)];
    }
    else
    {
        int i = (NSInteger)(timeInterval/TWENETY_FOUR_HOURS);
        NSLog(@"i %d",i);
        if(i <= 1)
        {
            NSLog(@"time %@",[NSString stringWithFormat:@"%d day ago", i]);
            return [NSString stringWithFormat:@"%d day ago", i];
        }
        else{
            return [NSString stringWithFormat:@"%d days ago", i];
        }
    }
    
}

-(NSString*)exceedingsNewsDaysAgoString{
    
    
    NSLog(@"****************The time zone stuff time interval since now: %d, timeoffset difference : %d", (NSInteger)[self timeIntervalSinceNow], [self fetchTheExactTimeOffset]);
    
    NSTimeInterval timeInterval = [self timeIntervalSinceNow]- [self fetchTheExactTimeOffset];

    if (timeInterval < 0) {
        timeInterval = -timeInterval;
    }
    else {
        //If its future. Keep it zero
        timeInterval = 0;
    }
    
    CGFloat SIXTY_MINUTES = 60*60;
    CGFloat TWENETY_FOUR_HOURS = 24*60*60;
    
    if (timeInterval < SIXTY_MINUTES) {
        return [NSString stringWithFormat:@"%d minutes ago", (NSInteger)(timeInterval/60)];
    }
    else if (timeInterval < TWENETY_FOUR_HOURS) {
        return [NSString stringWithFormat:@"%d hours ago", (NSInteger)(timeInterval/SIXTY_MINUTES)];
    }
    else {
        return [NSString stringWithFormat:@"%d days ago", (NSInteger)(timeInterval/TWENETY_FOUR_HOURS)];
    }
    
}


- (NSInteger)fetchTheExactTimeOffset
{
    NSTimeZone *currentTimeZone = [NSTimeZone systemTimeZone];
    NSTimeZone *pstTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PST"];
    
    NSInteger totalZoneOffset = [currentTimeZone secondsFromGMT] - [pstTimeZone secondsFromGMT];
    return totalZoneOffset;
}



-(NSString*)exceedingsStringWithSuffix{
    NSDateFormatter *dateFormatter = [NSDateFormatter exceedingEventsDateFormatter];
    
    NSDate *date = self;
    
    NSString *dateStr = [dateFormatter stringFromDate:date];
    
    //Remove zero EXCMS-140
    if ([dateStr length] > 4) {
        if ([[[dateStr substringFromIndex:[dateStr length] - 2] substringToIndex:1] isEqualToString:@"0"]) {
            dateStr = [dateStr stringByReplacingOccurrencesOfString:@"0" withString:@""];
        }
    }
    
    
    //add appropriate ext in date like rd, st, th
    {
        NSDateFormatter *monthDayFormatter = [[[NSDateFormatter alloc] init] autorelease];
        [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        [monthDayFormatter setDateFormat:@"d"]; 
        int date_day = [[monthDayFormatter stringFromDate:date] intValue];      
        NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
        NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
        NSString *suffix = [suffixes objectAtIndex:date_day]; 
        //dateStr = [dateStr stringByAppendingString:suffix];
    }
   // NSLog(@"!!datstr = %@", dateStr);

    if (dateStr) {
        return dateStr;
    }
    return nil;
}

@end
@implementation NSDateFormatter (Exceedings)

+(NSDateFormatter*)exceedingNewsServerDateFormat{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";//0001-01-01T00:00:00
    return dateFormatter;
}

+(NSDateFormatter*)exceedingDateFormat{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"yyyy-MM-ddTHH:mm:ss";//0001-01-01T00:00:00
    return dateFormatter;
}

+(NSDateFormatter*)exceedingEventsPushTimeFormatter
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"HH:mm";//0001-01-01T00:00:00
    return dateFormatter;
}

+(NSDateFormatter*)exceedingEventsPullTimeFormatter
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"hh:mm a";//0001-01-01T00:00:00
    return dateFormatter;
}


+(NSDateFormatter*)exceedingFullDateFormat{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm:ss:xxx";//0001-01-01T00:00:00:000
    return dateFormatter;
}

+(NSDateFormatter*)exceedingFundraisingDateFormat{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"yyyy-MM-ddTHH:mm:ss:xx";//0001-01-01T00:00:00:000
    return dateFormatter;
}


+(NSDateFormatter*)onlyDateDateFormatter{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"dd-MM-yyyy";//0001-01-01T00:00:00:000
    return dateFormatter;
}

+(NSDateFormatter*)onlyDateDateFormatterSlash{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"dd/MM/yyyy";//0001-01-01T00:00:00:000
    return dateFormatter;
}

+(NSDateFormatter*)exceedingEventsDateFormatter{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"MMM dd, yyyy";//0001-01-01T00:00:00:000
    return dateFormatter;
}

+(NSDateFormatter*)onlyTimeDateFormatter{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"HH:mm:ss";//0001-01-01T00:00:00:000
    return dateFormatter;
}

@end