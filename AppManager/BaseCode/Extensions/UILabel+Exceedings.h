//
//  NSDateFormatter_Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 05/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppMainDetailVC.h"

@interface UILabel (Exceedings)

- (void)alignTop;
- (void)alignBottom;
- (float)resizeToFit;
- (float)resizeToFitAbout;

- (void)realignTop:(NSString *)font;
- (void)realignBottom:(NSString *)font;

//Exceedings ext
- (void)sizeLabelForTitle;
- (void)sizeLabelForSubTitle;
- (void)sizeLabelForMid;
- (void)sizeLabelForDescription;
- (void)sizeLabelForAboutStatic;

- (void)colorLabelToLight;

- (void)sizeLabelForFundStatic;
- (void)sizeLabelForEventsDate;


- (void)realignBottom:(NSString *)font rect:(CGRect) rect;

@end
