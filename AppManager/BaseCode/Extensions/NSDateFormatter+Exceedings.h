//
//  NSDateFormatter_Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 05/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Exceedings)
-(NSString*)exceedingsStringWithSuffix;
-(NSString*)exceedingsDaysAgoString;
-(NSString*)exceedingsNewsDaysAgoString;
@end

@interface NSDateFormatter (Exceedings)

+(NSDateFormatter*)exceedingDateFormat;
+(NSDateFormatter*)exceedingFullDateFormat;

+(NSDateFormatter*)onlyDateDateFormatterSlash;
+(NSDateFormatter*)onlyDateDateFormatter;
+(NSDateFormatter*)onlyTimeDateFormatter;

+(NSDateFormatter*)exceedingEventsDateFormatter;

//
+(NSDateFormatter*)exceedingNewsServerDateFormat;

+(NSDateFormatter*)exceedingEventsPullTimeFormatter;
+(NSDateFormatter*)exceedingEventsPushTimeFormatter;

+(NSDateFormatter*)exceedingFundraisingDateFormat;


// checking the date issue....



@end
