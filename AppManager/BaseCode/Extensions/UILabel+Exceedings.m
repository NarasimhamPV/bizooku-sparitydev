//
//  NSDateFormatter_Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 05/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UILabel+Exceedings.h"

@implementation UILabel (Exceedings)

- (void)sizeLabelForTitle {
    [self setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:24]];
}

- (void)sizeLabelForSubTitle {
    self.font = [UIFont boldSystemFontOfSize:18];
}

- (void)colorLabelToLight{
    self.textColor = [UIColor darkGrayColor];
}

- (void)sizeLabelForMid{
    self.font = [UIFont systemFontOfSize:17];
}

- (void)sizeLabelForFundStatic{
    [self setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:16]];
}

- (void)sizeLabelForAboutStatic{
    [self setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:20]];
}

- (void)sizeLabelForDescription{
    [self setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];
}

- (void)sizeLabelForEventsDate{
    [self setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
}

- (void)alignTop {
    NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:16.0f]};

    CGSize fontSize = [self.text sizeWithAttributes:attributes];
//    CGSize fontSize = [self.text sizeWithAttributes:self.font];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, fontSize.height*self.numberOfLines);
    
}

- (void)alignBottom {
    NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:16.0f]};

    
    CGSize fontSize = [self.text sizeWithAttributes:attributes];
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y + self.frame.size.height - fontSize.height*self.numberOfLines, self.frame.size.width, fontSize.height*self.numberOfLines);
}


- (void)realignTop:(NSString *)font {
    NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:16.0f]};

    CGSize fontSize = [self.text sizeWithAttributes:attributes];
    
    //NSLog(@"THe realigntop : %@ , %f", self.text, fontSize.height);
    
    NSInteger labelIssue = 0;//fontSize.height - 21.0f;
    
    self.numberOfLines = [self numberOfLinesInLabel];
    if(fontSize.height == 23.0f)
    {
        labelIssue = 4.0f;
    }
    else{
        labelIssue = 4.0f;
    }
    if([font isEqualToString:@"Helvetica-Condensed"] || [font isEqualToString:@""] )
    {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, (fontSize.height+8)*self.numberOfLines);
    }
    else{
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y+labelIssue, self.frame.size.width, fontSize.height*self.numberOfLines);
    }
    
}





- (void)realignBottom:(NSString *)font {
    NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:16.0f]};

    
    CGSize fontSize = [self.text sizeWithAttributes:attributes];
    
    self.numberOfLines = [self numberOfLinesInLabel];
   NSInteger labelIssue = 4.0f;
    if([font isEqualToString:@"Helvetica-Condensed"] || [font isEqualToString:@""])
    {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y + self.frame.size.height - fontSize.height*self.numberOfLines, self.frame.size.width, fontSize.height*self.numberOfLines);
    }
    else{
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y + self.frame.size.height - fontSize.height*self.numberOfLines+labelIssue, self.frame.size.width, fontSize.height*self.numberOfLines);
    }
}


- (int) numberOfLinesInLabel
{
    self.numberOfLines = 0;
    self.lineBreakMode = NSLineBreakByWordWrapping;
    //self.text = @"Some really really long vdgfd Some really really long vdgfd ";
    //self.text = @"1234567890 123456789";
    CGRect frame = self.frame;
    frame.size = [self sizeThatFits:frame.size];
    //self.frame = frame;
    
    CGFloat lineHeight = self.font.leading;
    NSUInteger linesInLabel = floor(frame.size.height/lineHeight);
   // NSLog(@"************************************************************* Number of lines in label: %i", linesInLabel);
    return linesInLabel;
    
}

-(float)expectedHeight{
    [self setNumberOfLines:0];
    [self setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize maximumLabelSize = CGSizeMake(self.frame.size.width,CGFLOAT_MAX);
    
    CGSize expectedLabelSize = [self sizeThatFits:maximumLabelSize];
    
    
//        CGSize expectedLabelSize = [[self text] sizeWithFont:[self font]
//                                           constrainedToSize:maximumLabelSize
//                                               lineBreakMode:[self lineBreakMode]];
    return expectedLabelSize.height;
}
-(float)resizeToFit{
    
    float height = [self expectedHeight];
    CGRect newFrame = [self frame];
    newFrame.size.height = height;
    [self setFrame:newFrame];
    return newFrame.origin.y + newFrame.size.height;
}


- (float)expectedAboutHeight{
    [self setNumberOfLines:2];
    [self setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize maximumLabelSize = CGSizeMake(self.frame.size.width,CGFLOAT_MAX);
    
    CGSize expectedLabelSize = [self sizeThatFits:maximumLabelSize];
    
    //
    //    CGSize expectedLabelSize = [[self text] sizeWithFont:[self font]
    //                                       constrainedToSize:maximumLabelSize
    //                                           lineBreakMode:[self lineBreakMode]];
    return expectedLabelSize.height;
}
-(float)resizeToFitAbout{
    
    float height = [self expectedAboutHeight];
    CGRect newFrame = [self frame];
    newFrame.size.height = height;
    [self setFrame:newFrame];
    return newFrame.origin.y + newFrame.size.height;
}



@end