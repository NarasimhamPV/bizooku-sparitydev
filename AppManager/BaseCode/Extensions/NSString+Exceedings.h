//
//  NSString+Exceedings.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 02/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Exceedings)

-(NSString*)millionsFormatDollarString;
-(NSString*)convertToUSDateString;
-(NSString*)addProperExtToStrDate;

-(NSDate*)exceedingsDateFromServerNewsString;
-(NSDate*)exceedingsDateFromLocalNewsString;

@end
