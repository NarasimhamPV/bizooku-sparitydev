//
//  ConstantsEDM.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef Exceeding_ConstantsEDM_h
#define Exceeding_ConstantsEDM_h


typedef enum AppType{
    APP_NONE = 0,
    APP_NEWS = 1,
    APP_EVENTS,
    APP_ABOUT,
    APP_MEDIA,
    APP_DONATE,
    APP_LOCATIONS,
    APP_SOCIAL,
    APP_SHARE,
    APP_VOLUNTEER,
    APP_FUNDRAISING,
    APP_CONTRIBUTE,
    APP_SPONSOR,
    APP_COUPONS,
    APP_COUPONSLIST,
    APP_COUPONSDETAILS,
    APP_BANNERS,
    APP_PRODUCTS,
    APP_WEBLINK,
    APP_AUDIO,
    APP_FREE_AUDIO_List,
    APP_AUDIO_DETAIL,
    APP_AUDIO_PURCHASE,
    APP_VIDEO,

    APP_BIGBANNERS,

    APP_INAPP
}AppType;

typedef enum AppSubTab{
    TAB_NONE = 0,
    TAB_FACEBOOK,
    TAB_TWITTER,
    TAB_YOUTUBE,
    TAB_VIDEOS,
    TAB_AUDIO
}AppSubTab;


typedef enum TileType{
    TILE_TYPE_145X115,
    TILE_TYPE_145X240,
    TILE_TYPE_300X240,
    TILE_TYPE_300X115
} TileType;

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)


#define kURLDonate @"http://mobiledonation.exceedings.com/"



#define kURLDonation @"%@/%@/%@/%@/%@/login"

//Share widget conten

#define wAppEmailSubject @"Check out this amazing App from '%@'"
#define wAppEmailContent @"You can get it here \n\n App Store \nAndroid Market"

#define wAppSMSContent   @"I just found this amazing app from '%@'. iPhone: ‘iTunes URL’  / Android: ‘Droid App URL.’"
#define kexTwitterContent kexSMSContent




#endif
