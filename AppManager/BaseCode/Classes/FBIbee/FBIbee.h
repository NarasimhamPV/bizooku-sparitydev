//
//  FBIbee.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 24/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Facebook.h"
#import "ConstantsEDM.h"


// Analytics...

#import "DBTilesInfo.h"
#import "DBWidget.h"
#import "SDZEXAnalyticsServiceExample.h"


@interface FBIbee : NSObject <FBSessionDelegate, FBDialogDelegate, FBRequestDelegate, FBLoginDialogDelegate>
{
    
}

@property (nonatomic, retain) Facebook *facebookObj;
@property (nonatomic, retain) NSArray *permissions;
@property (nonatomic, readwrite) BOOL gavePostPermission;
@property (nonatomic, retain) NSString *textMsg;
@property (nonatomic, retain) NSString *fbAppID;
@property (nonatomic, readwrite) BOOL postAfterLogin, isCheckingPermissions;

@property (nonatomic, retain) id detailObject;

@property (nonatomic, readwrite) AppType appType;

@property (nonatomic, readwrite) BOOL choosePosting;
@property (nonatomic, retain) DBTilesInfo *selectedTileInfo;
@property (nonatomic, retain) NSNumber *selectedItemId;
@property (nonatomic, readwrite) BOOL isSharing;





-(BOOL)loginFacebook:(id)sender;

-(void)logFBEvent:(NSString*)string;

+(FBIbee*)sharedDelegate;

-(void)shareTextInFacebookApp:(NSString*)shareText;

-(BOOL)haveAccessToken;

- (void)postFacebookContentsByAppType:(UIViewController *)vControl type:(AppType)type;


- (void)registeringAnEvent;

@end
