//
//  FBIbee.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 24/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FBIbee.h"
#import "AppDelegate.h"
#import "NSString+SBJSON.h"
#import "NSString+Exceedings.h"

#import "PodcastItem.h"
#import "TableObject.h"
#import "SBJSON.h"
#import "NSDateFormatter+Exceedings.h"
#import "PhoneNumberFormatter.h"



static FBIbee* _fbShare = nil;


@implementation FBIbee

@synthesize facebookObj = _facebookObj;
@synthesize permissions = _permissions;
@synthesize textMsg = _textMsg;
@synthesize fbAppID;
@synthesize postAfterLogin, isCheckingPermissions;
@synthesize appType = _appType;
@synthesize gavePostPermission;
@synthesize detailObject = _detailObject;
@synthesize choosePosting = _choosePosting;
@synthesize isSharing = _isSharing;


//Analytics ....

@synthesize selectedItemId = _selectedItemId;
@synthesize selectedTileInfo = _selectedTileInfo;

#pragma mark - Logging

-(void)logEvent:(NSString*)myText{
    NSLog(@"Facebook: %@", myText);
}

-(void)logFBEvent:(NSString*)string{
    NSLog(@"Facebook Class: %@", string);
}


#pragma mark Registering an event...

- (void)registeringAnEvent
{
    isCheckingPermissions  = NO;
    [self logFBEvent:@"Post content called"];
    self.choosePosting = NO;
    postAfterLogin = YES;
    if (![self loginFacebook:nil]) {
        [self logFBEvent:@"Post content aborted. Going for login first"];
        return;
    }

    
    
    if([_detailObject isKindOfClass:[NSString class]])
    {
        
        if(_selectedItemId && _selectedTileInfo)
        {
            SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            [service runAddVolunteerRegisterAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] volunteerId:[_selectedItemId integerValue] status:@"Registered" deviceType:@"iOS"];
            
        }
        NSLog(@"the fb access token is : %@", [FBIbee sharedDelegate].facebookObj.accessToken);
        //115743305246565/attending?method=POST&access_token=AAADcvGuEoi8BAP9Epg9w6ZBI6i06Ieil6ZC4P2aFQrA8Viy3hdr2WCSWFOlIEWFuRe2Ln7ki08j19oRvNuzNO3Ud9EzxqoTPJZCxld3DQZDZD
        NSString *str = [NSString stringWithFormat:@"%@/attending?method=POST&access_token=%@",_detailObject,[FBIbee sharedDelegate].facebookObj.accessToken ];
        
        [_facebookObj requestWithGraphPath:str andDelegate:self];
    }
}




#pragma mark - Post call


-(void)graphMethod:(NSString*)appName image:(NSString*)image link:(NSString*)link caption:(NSString*)caption desc:(NSString*)desc msg:(NSString*)msg needFanpageURL:(BOOL)boolFanURL applink:(BOOL)boolURL sponsorDic:(NSDictionary *)sponsorProperties{
    //Prepare Links
    NSString *adnroid  = [AppDelegate shareddelegate].appReference.androidURL;
    NSString *iphone = [AppDelegate shareddelegate].appReference.appstoreURL;
    
   
    if(!adnroid || [[adnroid stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""])
    {
        adnroid = @"http://play.google.com";
    }
    
    if(!iphone || [[iphone stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""])
    {
        iphone = @"http://itunes.apple.com";
    }
   
    SBJSON *jsonWriter = [[SBJSON new] autorelease];
    NSDictionary *propertyiOS = [NSDictionary dictionaryWithObjectsAndKeys:@"Download iPhone Version", @"text", iphone, @"href", nil];
    NSDictionary *propertyAnd = [NSDictionary dictionaryWithObjectsAndKeys:@"Download Android Version", @"text", adnroid, @"href", nil];
    
    
    NSMutableDictionary *properties = nil;
    if(boolFanURL)
    {
        NSString *facebookLink = [NSString stringWithFormat:kFBFanPageURL,[AppDelegate shareddelegate].appReference.socFBPageID];
        NSString *twitterLink = [NSString stringWithFormat:kTwitterFanPage,[AppDelegate shareddelegate].appReference.socTwitterBrandName];
        NSDictionary *propertyfb = [NSDictionary dictionaryWithObjectsAndKeys:@"View Posts", @"text", facebookLink, @"href", nil];
        NSDictionary *propertyTweet = [NSDictionary dictionaryWithObjectsAndKeys:@"View Tweets", @"text", twitterLink, @"href", nil];
        
        properties = [NSMutableDictionary dictionaryWithObjectsAndKeys: propertyiOS,@"iPhone",propertyAnd, @"Android", propertyfb, @"Facebook",  propertyTweet,@"Twitter", nil];
        
    }
    else if(boolURL){
       
        if(sponsorProperties)
        {
            properties = [NSMutableDictionary dictionaryWithDictionary:sponsorProperties];
        }
        else{
             properties =  [NSMutableDictionary dictionaryWithObjectsAndKeys:propertyiOS, @"iPhone",propertyAnd, @"Android", nil];
        }
    }
    else{
        properties =  [NSMutableDictionary dictionaryWithObjectsAndKeys:propertyiOS, @"iPhone",propertyAnd, @"Android", nil];
    }
   // NSDictionary *properties = [NSArray arrayWithObjects:propertyiOS,propertyAnd, nil];
    
    NSString *finalproperties = [jsonWriter stringWithObject:properties];
    NSMutableDictionary* params = nil;
    
    if(caption && ![caption isEqualToString:@""])
    {
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"post", @"type",
                                       image, @"picture",
                                       link, @"link",
                                       appName, @"name",
                                       caption, @"caption",
                                       desc, @"description",
                                       msg, @"message",
                                       finalproperties, @"properties",
                                       nil];
    }
    else{
        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                  @"post", @"type",
                  image, @"picture",
                  link, @"link",
                  appName, @"name",
                  @" ", @"caption",
                  desc, @"description",
                  msg, @"message",
                  finalproperties, @"properties",
                  nil];
    }
    
    [self logFBEvent:[NSString stringWithFormat:@"Params Posting: %@", params]];
    
    [_facebookObj requestWithGraphPath:@"me/feed"
                          andParams:params
                      andHttpMethod:@"POST"
                        andDelegate:self];
}



- (void)postFacebookContentsByAppType:(UIViewController *)vControl type:(AppType)type
{
    self.appType = type;
    
    isCheckingPermissions  = NO;
    [self logFBEvent:@"Post content called"];
    self.choosePosting = YES;
    postAfterLogin = YES;
    
    AppDelegate *del = [AppDelegate shareddelegate];
    self.selectedItemId = [AppDelegate shareddelegate].selectedItemId;
    self.selectedTileInfo = [AppDelegate shareddelegate].selectedTileInfo;
    
    if (![self loginFacebook:nil]) {
        [self logFBEvent:@"Post content aborted. Going for login first"];
        return;
    }
    
    [self logFBEvent:@"User is logged in. Go and post stuff"];
    
    switch (type) {
            
        case APP_CONTRIBUTE:
        {
            if ([_detailObject isKindOfClass:[NSDictionary class]]) {
                [self graphMethod:del.appReference.brandname image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.logo] link:del.appReference.markettingURL caption:@"" desc:del.appReference.descrip msg:@"About:" needFanpageURL:NO applink:NO sponsorDic:nil];
            }
            break;
            
            
        case APP_COUPONS:
            
            {
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    TableObject *object = (TableObject*)_detailObject;
                    NSDate *endDt = object.endDate;
                    
                    NSDateFormatter *df = [[NSDateFormatter alloc]init];
                    [df setDateFormat:@"MMM dd, yyyy"];
                    
                    NSString *endString = [df stringFromDate:endDt];
                    NSString *startString = [df stringFromDate:object.startDate];
                    if(!endString)
                    {
                        endString = @" ";
                    }
                    if(!startString)
                    {
                        startString = @" ";
                    }
                    
                    NSString *img = nil;
                    if([object.image length] && (![object.image isEqualToString:kWebServiceURL]))
                    {
                        img = object.image;
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    //Check out "Audio Title" from "App Name"
                    
                    [self graphMethod:object.titleText
                                image:img
                                 link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID]
                              caption:[NSString stringWithFormat:@"%@  %@", startString, endString]
                                 desc:@""
                                  msg: [NSString stringWithFormat:@"Check out '%@' From %@.\n\n%@",object.titleText,del.appReference.brandname,object.desc] needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }
            
            break;
            
        case APP_COUPONSLIST:
            
            {
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    //                    TableObject *object = (TableObject*)_detailObject;
                    //                    NSLog(@"the coupons :%@ ", __func__);
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Coupons" message:@"There is no services" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            
            break;
            
        case APP_COUPONSDETAILS:
            {
                {
                    if ([_detailObject isKindOfClass:[TableObject class]]) {
                        //                        TableObject *object = (TableObject*)_detailObject;
                        //                    NSLog(@"the coupons :%@ ", __func__);
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Coupons" message:@"There is no services" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
                
            }
            break;
            

            
        case APP_EVENTS:
            {
                NSLog(@"The detail object is: %@", _detailObject);
                
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    TableObject *object = (TableObject*)_detailObject;
                    NSDate *startDt = object.startDate;
                    NSDate *endDt = object.endDate;
                    
                    NSLog(@"The detail object is: %@", object.startDate);
                    NSDateFormatter *df = [[[NSDateFormatter alloc]init] autorelease];
                    [df setDateFormat:@"MMMM dd, yyyy"];
                    
                    NSDateFormatter *dfTime = [[[NSDateFormatter alloc] init] autorelease];
                    [dfTime setDateFormat:@"HH:mm"];
                    
                    NSString *startString = [df stringFromDate:startDt];
                    NSString *endString = [df stringFromDate:endDt];
                    
                    NSString *strtTime = [dfTime stringFromDate:object.startTime];
                    NSString *endTime = [dfTime stringFromDate:object.endTime];
                    if(!strtTime)
                    {
                        strtTime = @"";
                    }
                    if(!endTime)
                    {
                        endTime = @"";
                    }
                    if(!startString)
                    {
                        startString = del.appReference.brandname;
                    }
                    
                    if(!endString)
                    {
                        endString = @" ";
                    }
                    
                    NSString *img = nil;
                    if([object.image length] && (![object.image isEqualToString:kWebServiceURL]))
                    {
                        img = object.image;
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    
                    [self graphMethod:object.titleText
                                image:img
                                 link:[NSString stringWithFormat:kFBFanPageURL, object.fbPostID]
                              caption:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@ | %@ to %@", [object.startDate exceedingsStringWithSuffix], strtTime, endTime]]
                                 desc:@"  "
                                  msg:[NSString stringWithFormat:@"I will be attending '%@'. \n\n %@",object.titleText, object.desc]  needFanpageURL:NO applink:NO sponsorDic:nil];
                    
                }else
                {
                    NSLog(@"The detail dictionary object : %@",_detailObject );
                    
                    NSString *fbPostTitle = nil;
                    NSString *fbPostThumbnail = nil;
                    NSString *fbPostMessage = nil;
                    NSString *fbPostLink = nil;
                    NSMutableDictionary *properties = nil;
                    
                    if([_detailObject isKindOfClass:[NSDictionary class]])
                    {
                        
                        //"SPONSOR NAME" is making a difference by supporting "BRAND NAME".
                        
                        if(_detailObject && [[_detailObject allKeys] containsObject:@"Title"])
                        {
                            fbPostTitle = [NSString stringWithFormat:@"Check this out from %@!", del.appReference.brandname];
                        }
                        else{
                            fbPostTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                        }
                        
                        if(_detailObject && [[_detailObject allKeys] containsObject:@"Image"] && ![[_detailObject valueForKey:@"Image"] isEqualToString:@""])
                        {
                            fbPostThumbnail = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"Image"]];
                        }
                        else{
                            fbPostThumbnail = [NSString stringWithFormat:@"%@", del.appReference.aboutImage];
                        }
                        
                        if(_detailObject && [[_detailObject allKeys] containsObject:@"Description"])
                        {
                            fbPostMessage = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"Description"]];
                        }
                        else{
                            fbPostMessage = [NSString stringWithFormat:@"%@", del.appReference.descrip];
                        }
                        
                        if(_detailObject && [[_detailObject allKeys] containsObject:@"ActionButtonURL"])
                        {
                            fbPostLink = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"ActionButtonURL"]];
                        }
                        else{
                            fbPostLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                        }
                        NSString *phoneNum = nil;
                        PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
                        phoneNum = /*[dic valueForKey:@"Phone"]; */ [phoneNumberFormatter format:[_detailObject objectForKey:@"Phone"] withLocale:@"us"];
                        
                        
                        if(_detailObject && [[_detailObject allKeys] containsObject:@"Email"])
                        {
                            
                            
                            // NSDictionary *propertyWebLink = [NSDictionary dictionaryWithObjectsAndKeys:[_detailObject objectForKey:@"Website"], @"text", [_detailObject objectForKey:@"Website"], @"href", nil];
                            NSDictionary *propertyEmail = [NSDictionary dictionaryWithObjectsAndKeys:[_detailObject objectForKey:@"Email"], @"text", [_detailObject objectForKey:@"Email"], @"href", nil];
                            NSDictionary *propertyPhNo = [NSDictionary dictionaryWithObjectsAndKeys:phoneNum, @"text", [_detailObject objectForKey:@"Website"], @"href", nil];
                            properties = [NSMutableDictionary dictionaryWithObjectsAndKeys: propertyEmail, @"Email", propertyPhNo, @"Phone Number", nil];
                        }
                        
                        
                    }
                    [self graphMethod:[_detailObject objectForKey:@"Title"]
                                image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,fbPostThumbnail]
                                 link:[NSString stringWithFormat:@"%@",fbPostLink] caption:@""
                                 desc:@" "
                                  msg:[NSString stringWithFormat:@"%@ \n Here is more about '%@': \n\n%@",fbPostTitle,[_detailObject objectForKey:@"Title"],fbPostMessage]  needFanpageURL:NO applink:YES sponsorDic:nil];
                }
            }
            break;
        case APP_FUNDRAISING:
            {
                NSLog(@"THe detsaill object : %@", _detailObject);
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    TableObject *object = (TableObject*)_detailObject;
                    
                    NSLog(@"THe FUndraising options: %@, %@, %@, %@,%@,%@,%@", object.startTimeStr, object.date, object.endTimeStr, object.address, object.phone, object.newsID, object.titleText);
                    NSDate *endDt = object.endDate;
                    NSDate *startDt = object.startDate;
                    
                    NSDateFormatter *df = [[[NSDateFormatter alloc]init] autorelease];
                    [df setDateFormat:@"MMMM dd, yyyy"];
                    
                    NSString *endString = [df stringFromDate:endDt];
                    NSString *startString = [df stringFromDate:startDt];
                    
                    if(!endString)
                    {
                        endString = @"  ";
                    }
                    
                    if(!startString)
                    {
                        startString = @"  ";
                    }
                    
                    NSString *img = nil;
                    if([object.image length] && (![object.image isEqualToString:kWebServiceURL]))
                    {
                        img = object.image;
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    
                    [self graphMethod:object.titleText
                                image:img
                                 link:del.appReference.markettingURL
                              caption:[NSString stringWithFormat:@"%@ %@", startString, endString]
                                 desc:@"  "
                                  msg:[NSString stringWithFormat:@"I’m supporting ‘%@’ by contributing to ‘%@’ and thought you might be interested in learning more.. \n\n %@",del.appReference.brandname, object.titleText, object.desc]  needFanpageURL:NO applink:NO sponsorDic:nil];
                }
                else if([_detailObject isKindOfClass:[NSDictionary class]])
                {
                
                    NSString *GoalString = nil;
                    if([[_detailObject allKeys] containsObject:@"Goal"])
                    {
                        GoalString = [NSString stringWithFormat:@"Goal: %@", [[NSString stringWithFormat:@"%@",[_detailObject objectForKey:@"Goal"]] millionsFormatDollarString]];
                    }
                    
                    NSString *amountCollected = [NSString stringWithFormat:@"%@", [_detailObject valueForKey:@"AmountCollected"]];
                    NSString *raised = [NSString stringWithFormat:@"Raised: %@", [amountCollected millionsFormatDollarString]];
                    NSString *img = nil;
                    if([[_detailObject allKeys] containsObject:@"Image"] && ![[_detailObject objectForKey:@"Image"] isEqualToString:@""])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,[_detailObject objectForKey:@"Image"]];
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    NSString *title = nil;
                    if([[_detailObject allKeys] containsObject:@"Name"])
                    {
                        title = [_detailObject objectForKey:@"Name"];
                    }
                    else{
                        title = @" ";
                    }
                    
                    NSString *desc = nil;
                    if([[_detailObject allKeys] containsObject:@"Description"])
                    {
                        desc = [_detailObject objectForKey:@"Description"];
                    }
                    else{
                        desc = @" ";
                    }
                    
                    
                    [self graphMethod:title
                                image:img
                                 link:del.appReference.markettingURL
                              caption:[NSString stringWithFormat:@"%@ - %@", raised,GoalString]
                                 desc:@"  "
                                  msg:desc  needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }
            break;
        case APP_LOCATIONS:
            {
                if ([_detailObject isKindOfClass:[NSDictionary class]]) {
                    [self graphMethod:del.appReference.appName image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.logo] link:del.appReference.markettingURL caption:del.appReference.brandname desc:@" " msg:del.appReference.descrip  needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }
            break;
        case APP_MEDIA:
            {
                NSLog(@"_detailObject: %@", _detailObject);
                if ([_detailObject isKindOfClass:[PodcastItem class]]) {
                    PodcastItem *item = (PodcastItem*)_detailObject;
                  [self graphMethod:item.titleText
                                image:([item.image length] && (![item.image isEqualToString:kWebServiceURL]))?item.image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage]
                                 link:item.url
                              caption:@" "
                                 desc:@"  "
                                  msg:[NSString stringWithFormat:@"Check out '%@'. \n\n %@",item.url, item.desc]  needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }
            break;
        
        case APP_WEBLINK:
            {
                NSLog(@"_detailObject: %@", _detailObject);
                
                NSString *strTitle = nil;
                NSString *strURL = nil;
                
                if(_selectedTileInfo.webLinkPageTitle && ![_selectedTileInfo.webLinkPageTitle isKindOfClass:[NSNull class]])
                {
                    strTitle = _selectedTileInfo.webLinkPageTitle;
                }
                else if(_selectedTileInfo.webLinkTitle && ![_selectedTileInfo.webLinkTitle isKindOfClass:[NSNull class]])
                {
                    strTitle = _selectedTileInfo.webLinkTitle;
                }
                else{
                    strTitle = del.appReference.brandname;
                }
                
                
                if(_selectedTileInfo.webLinkURL && ![_selectedTileInfo.webLinkURL isKindOfClass:[NSNull class]])
                {
                    strURL = _selectedTileInfo.webLinkURL;
                }
                else{
                    strURL = del.appReference.aboutURL;
                }
                
                    [self graphMethod:strTitle
                                image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage]
                                 link:strURL
                              caption:@" "
                                 desc:@"  "
                                  msg:[NSString stringWithFormat:@"Check out '%@'.",strTitle]  needFanpageURL:NO applink:YES sponsorDic:nil];
            }
            break;
            
            
        case APP_NEWS:
            {
                
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    TableObject *object = (TableObject*)_detailObject;
                    
                    NSDate *endDt = object.endDate;
                    
                    NSDateFormatter *df = [[NSDateFormatter alloc]init];
                    [df setDateFormat:@"MMM dd, yyyy"];
                    
                    NSString *endString = [df stringFromDate:endDt];
                    
                    if(!endString)
                    {
                        endString = del.appReference.brandname;
                    }
                    NSString *img = nil;
                    if([object.image length] && (![object.image isEqualToString:kWebServiceURL]))
                    {
                        img = object.image;
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    [self graphMethod:object.titleText
                                image:img
                                 link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID]
                              caption:endString
                                 desc:@"  "
                                  msg:[NSString stringWithFormat:@"A must read from '%@'.\n\n%@",del.appReference.brandname,object.desc]  needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }
            break;
        case APP_VOLUNTEER:
            {
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    TableObject *object = (TableObject*)_detailObject;
                    NSLog(@"The object content : %@ %@ %@ %@ %@", object.startDate, object.endDate, object.startTimeStr, object.endTimeStr, object.image);
                    NSDate *endDt = object.endDate;
                    
                    NSDateFormatter *df = [[NSDateFormatter alloc]init];
                    [df setDateFormat:@"MMM dd, yyyy"];
                    
                    NSString *endString = [df stringFromDate:endDt];
                    NSString *startString = [df stringFromDate:object.startDate];
                    if(!endString)
                    {
                        endString = @" ";
                    }
                    if(!startString)
                    {
                        startString = @" ";
                    }
                    
                    NSString *img = nil;
                    if([object.image length] && (![object.image isEqualToString:kWebServiceURL]))
                    {
                        img = object.image;
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    [self graphMethod:object.titleText
                                image:img
                                 link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID]
                              caption:[NSString stringWithFormat:@"%@  %@", startString, endString]
                                 desc:@"  "
                                  msg:[NSString stringWithFormat:@"I am volunteering for '%@'.\n\n%@",object.titleText,object.desc]  needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }
            break;
            
        case APP_AUDIO_DETAIL:
            {
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    TableObject *object = (TableObject*)_detailObject;
                    NSLog(@"The object content : %@ %@ %@ %@ %@", object.startDate, object.endDate, object.startTimeStr, object.endTimeStr, object.image);
                   
                    NSString *androidURL = nil;
                    {
                        androidURL = del.appReference.androidURL;
                    }
                    NSString *appStoreURL = nil;
                    {
                        appStoreURL = del.appReference.appstoreURL;
                        
                    }
                    
                    NSString *img = nil;
                    if([object.image length] && (![object.image isEqualToString:kWebServiceURL]))
                    {
                        img = object.image;
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    //Check out "Audio Title" from "App Name"
                    
                    [self graphMethod:object.titleText
                                image:img
                                 link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID]
                              caption:nil
                                 desc:@""
                                  msg: [NSString stringWithFormat:@"Check out '%@' from %@.\n\n%@\niPhone: %@\nAndroid: %@",object.titleText,del.appReference.brandname,object.desc,appStoreURL,androidURL] needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }

            
            
            break;
            
        case APP_AUDIO_PURCHASE:
            {
                if ([_detailObject isKindOfClass:[TableObject class]]) {
                    TableObject *object = (TableObject*)_detailObject;
                    NSLog(@"The object content : %@ %@ %@ %@ %@", object.startDate, object.endDate, object.startTimeStr, object.endTimeStr, object.image);
                    
                    NSString *androidURL = nil;
                    {
                        androidURL = del.appReference.androidURL;
                    }
                    NSString *appStoreURL = nil;
                    {
                        appStoreURL = del.appReference.appstoreURL;
                        
                    }
                    
                    NSString *img = nil;
                    if([object.image length] && (![object.image isEqualToString:kWebServiceURL]))
                    {
                        img = object.image;
                    }
                    else if([del.appReference.aboutImage length] && ![del.appReference.aboutImage isEqualToString:kWebServiceURL])
                    {
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    }
                    else{
                        img = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage];
                    }
                    //Check out "Audio Title" from "App Name"
                    
                    [self graphMethod:object.titleText
                                image:img
                                 link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID]
                              caption:nil
                                 desc:@""
                                  msg: [NSString stringWithFormat:@"Check out '%@' from %@.\n\n%@\niPhone: %@\nAndroid: %@",object.titleText,del.appReference.brandname,object.desc,appStoreURL,androidURL] needFanpageURL:NO applink:NO sponsorDic:nil];
                }
            }
            
            
            
            break;
            
        case APP_SOCIAL:
            {
                [self graphMethod:del.appReference.brandname
                            image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage]
                             link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID] caption:@""
                             desc:@"  "
                              msg:[NSString stringWithFormat:@"Check this out from '%@'.\n\n%@",del.appReference.brandname,del.appReference.descrip]  needFanpageURL:YES applink:NO sponsorDic:nil];
            }
            break;
        
        case APP_ABOUT:
            {
                NSLog(@"the detail object :%@", _detailObject);
                
                [self graphMethod:del.appReference.brandname
                            image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage]
                             link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID] caption:@""
                             desc:@"  "
                              msg:[NSString stringWithFormat:@"I just found this amazing App from '%@'. \n\n%@",del.appReference.brandname,del.appReference.descrip]  needFanpageURL:YES applink:NO sponsorDic:nil];
            }
            break;
        
        case APP_SPONSOR:
            {
                NSLog(@"The detail dictionary object : %@",_detailObject );
                
                NSString *fbPostTitle = nil;
                NSString *fbPostThumbnail = nil;
                NSString *fbPostDescription = nil;
                NSString *fbPostMessage = nil;
                NSString *fbPostLink = nil;
                NSMutableDictionary *properties = nil;
                
                if([_detailObject isKindOfClass:[NSDictionary class]])
                {
                    
                    //"SPONSOR NAME" is making a difference by supporting "BRAND NAME".
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"SponserName"])
                    {
                        fbPostTitle = [NSString stringWithFormat:@"'%@' is making a difference by supporting '%@'", [_detailObject objectForKey:@"SponserName"], del.appReference.brandname];
                    }
                    else{
                        fbPostTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                    }
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"SponserImage"] && ![[_detailObject valueForKey:@"SponserImage"] isEqualToString:@""])
                    {
                        fbPostThumbnail = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"SponserImage"]];
                    }
                    else{
                        fbPostThumbnail = [NSString stringWithFormat:@"%@", del.appReference.aboutImage];
                    }
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"AboutUs"])
                    {
                        fbPostMessage = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"AboutUs"]];
                    }
                    else{
                        fbPostMessage = [NSString stringWithFormat:@"%@", del.appReference.descrip];
                    }
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"Website"])
                    {
                        fbPostLink = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"Website"]];
                    }
                    else{
                        fbPostLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                    }
                    NSString *phoneNum = nil;
                    PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
                    phoneNum = /*[dic valueForKey:@"Phone"]; */ [phoneNumberFormatter format:[_detailObject objectForKey:@"Phone"] withLocale:@"us"];
                    
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"Website"])
                    {
                       
                        
                        NSDictionary *propertyWebLink = [NSDictionary dictionaryWithObjectsAndKeys:[_detailObject objectForKey:@"Website"], @"text", [_detailObject objectForKey:@"Website"], @"href", nil];
                        NSDictionary *propertyEmail = [NSDictionary dictionaryWithObjectsAndKeys:[_detailObject objectForKey:@"Email"], @"text", [_detailObject objectForKey:@"Website"], @"href", nil];
                        NSDictionary *propertyPhNo = [NSDictionary dictionaryWithObjectsAndKeys:phoneNum, @"text", [_detailObject objectForKey:@"Website"], @"href", nil];
                        properties = [NSMutableDictionary dictionaryWithObjectsAndKeys: propertyWebLink,@"Website",propertyEmail, @"Email", propertyPhNo, @"Phone Number", nil];
                    }
                    
                    
                }
                [self graphMethod:[_detailObject objectForKey:@"SponserName"]
                            image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,fbPostThumbnail]
                             link:[NSString stringWithFormat:@"%@",fbPostLink] caption:@""
                             desc:@" "
                              msg:[NSString stringWithFormat:@"%@ \n Here is more about '%@': \n\n%@",fbPostTitle,[_detailObject objectForKey:@"SponserName"],fbPostMessage]  needFanpageURL:NO applink:YES sponsorDic:properties];
            }
            break;
        case APP_PRODUCTS:
            {
                NSLog(@"The detail dictionary object : %@",_detailObject );
                
                NSString *fbPostTitle = nil;
                NSString *fbPostThumbnail = nil;
                NSString *fbPostDescription = nil;
                NSString *fbPostMessage = nil;
                NSString *fbPostLink = nil;
                NSMutableDictionary *properties = nil;
                
                if([_detailObject isKindOfClass:[NSDictionary class]])
                {
                    
                    //"SPONSOR NAME" is making a difference by supporting "BRAND NAME".
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"Title"])
                    {
                        fbPostTitle = [NSString stringWithFormat:@"Check this out from %@!", del.appReference.brandname];
                    }
                    else{
                        fbPostTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                    }
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"Image"] && ![[_detailObject valueForKey:@"Image"] isEqualToString:@""])
                    {
                        fbPostThumbnail = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"Image"]];
                    }
                    else{
                        fbPostThumbnail = [NSString stringWithFormat:@"%@", del.appReference.aboutImage];
                    }
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"Description"])
                    {
                        fbPostMessage = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"Description"]];
                    }
                    else{
                        fbPostMessage = [NSString stringWithFormat:@"%@", del.appReference.descrip];
                    }
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"ActionButtonURL"])
                    {
                        fbPostLink = [NSString stringWithFormat:@"%@", [_detailObject objectForKey:@"ActionButtonURL"]];
                    }
                    else{
                        fbPostLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                    }
                    NSString *phoneNum = nil;
                    PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
                    phoneNum = /*[dic valueForKey:@"Phone"]; */ [phoneNumberFormatter format:[_detailObject objectForKey:@"Phone"] withLocale:@"us"];
                    
                    
                    if(_detailObject && [[_detailObject allKeys] containsObject:@"Email"])
                    {
                        
                        
                       // NSDictionary *propertyWebLink = [NSDictionary dictionaryWithObjectsAndKeys:[_detailObject objectForKey:@"Website"], @"text", [_detailObject objectForKey:@"Website"], @"href", nil];
                        NSDictionary *propertyEmail = [NSDictionary dictionaryWithObjectsAndKeys:[_detailObject objectForKey:@"Email"], @"text", [_detailObject objectForKey:@"Email"], @"href", nil];
                        NSDictionary *propertyPhNo = [NSDictionary dictionaryWithObjectsAndKeys:phoneNum, @"text", [_detailObject objectForKey:@"Website"], @"href", nil];
                        properties = [NSMutableDictionary dictionaryWithObjectsAndKeys: propertyEmail, @"Email", propertyPhNo, @"Phone Number", nil];
                    }
                    
                    
                }
                [self graphMethod:[_detailObject objectForKey:@"Title"]
                            image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,fbPostThumbnail]
                             link:[NSString stringWithFormat:@"%@",fbPostLink] caption:@""
                             desc:@" "
                              msg:[NSString stringWithFormat:@"%@ \n Here is more about '%@': \n\n%@",fbPostTitle,[_detailObject objectForKey:@"Title"],fbPostMessage]  needFanpageURL:NO applink:YES sponsorDic:nil];
            }
            break;
        case APP_VIDEO:
            {
                NSLog(@"_detailObject: %@", _detailObject);
                
                NSString *strTitle = nil;
                NSString *strURL = nil;
                
                NSString *postThumbnail = nil;
                
                
                
                if (![[_detailObject valueForKey:@"Title"] isEqualToString:@""] &&![[_detailObject valueForKey:@"Title"] isKindOfClass:[NSNull class]]){
                    strTitle = [NSString stringWithFormat:@"%@",[_detailObject valueForKey:@"Title"]];
                    
                    
                }else{
                    strTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                    
                    
                }
                
                
                if (![[_detailObject valueForKey:@"Image"] isKindOfClass:[NSNull class]]&&![[_detailObject valueForKey:@"Image"] isEqualToString:@""]){
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL,[_detailObject valueForKey:@"Image"]];
                    
                    
                }else{
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    
                    
                }
                
                if (![[_detailObject valueForKey:@"FilePath"] isEqualToString:@""] &&![[_detailObject valueForKey:@"FilePath"] isKindOfClass:[NSNull class]]){
                    strURL = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailObject valueForKey:@"FilePath"]];
                    
                }else{
                    strURL = [NSString stringWithFormat:@"%@", del.appReference.aboutURL];
                    
                }
                
                if(_selectedTileInfo.webLinkPageTitle && ![_selectedTileInfo.webLinkPageTitle isKindOfClass:[NSNull class]])
                {
                    strTitle = _selectedTileInfo.webLinkPageTitle;
                }
                else if(_selectedTileInfo.webLinkTitle && ![_selectedTileInfo.webLinkTitle isKindOfClass:[NSNull class]])
                {
                    strTitle = _selectedTileInfo.webLinkTitle;
                }
                else{
                    strTitle = del.appReference.brandname;
                }
                
                
                if(_selectedTileInfo.webLinkURL && ![_selectedTileInfo.webLinkURL isKindOfClass:[NSNull class]])
                {
                    strURL = _selectedTileInfo.webLinkURL;
                }
                else{
                    strURL = del.appReference.aboutURL;
                }
                
                [self graphMethod:strTitle
                            image:postThumbnail
                             link:strURL
                          caption:@" "
                             desc:@"  "
                              msg:[NSString stringWithFormat:@"Check out '%@'.",strTitle]  needFanpageURL:NO applink:YES sponsorDic:nil];
            }
            break;
            

        default:
            {

                [self graphMethod:del.appReference.brandname
                            image:[NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.splashimage]
                             link:[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID] caption:@""
                             desc:@"  "
                              msg:[NSString stringWithFormat:@"I just found this amazing App from '%@'. \n\n%@",del.appReference.brandname,del.appReference.descrip]  needFanpageURL:YES applink:NO sponsorDic:nil];
            }
            break;
        }
            
    }
}

#pragma mark - Inits

-(BOOL)haveAccessToken{
    NSLog(@"The updated FB accesstoken: %@", [FBIbee sharedDelegate].facebookObj.accessToken);
    
    return ([FBIbee sharedDelegate].facebookObj.accessToken != nil);
}

-(void)initFacebookObjs{
    if(!_facebookObj)
    {
        self.fbAppID = [AppDelegate shareddelegate].appReference.socFBAppID;
        self.facebookObj = [[[Facebook alloc] initWithAppId:fbAppID andDelegate:self] autorelease];
        
        _facebookObj.sessionDelegate = self;
        self.permissions =  [NSArray arrayWithObjects:@"publish_actions", @"user_likes",@"rsvp_event", nil];
    }
}

-(id)init{
    if (self = [super init]) {
        //
        
        [self initFacebookObjs];
    }
    return self;
}

+(FBIbee*)sharedDelegate{
    
    if (!_fbShare) {
        _fbShare = [[FBIbee alloc] init];
        
        
    }
    _fbShare.choosePosting = YES;
    return _fbShare;
}


//Returns YES if already logged in
-(BOOL)loginFacebook:(id)sender
{
    isCheckingPermissions = NO;
    
    if (![_facebookObj isSessionValid]) {
        [_facebookObj authorize:_permissions];
        return NO;
    }
    else{
        if (!gavePostPermission) {
            [_facebookObj authorize:_permissions];
            return NO;
        }
    }
    
    return YES;
}


-(void)logoutFacebook:(id)sender{
    
    //Clear cookies
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook.com"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
    
    [_facebookObj logout:self];
}

-(void)shareTextInFacebookApp:(NSString*)shareText{
    if ([self loginFacebook:nil]) {
        //Post directly
    }
    else {
        //Wait for login
    }
}

#pragma mark - FBSessionDelegate

-(void)checkPermissions{
    isCheckingPermissions = YES;
    [_facebookObj requestWithGraphPath:@"me/permissions" 
                                                andDelegate:self];
    
}
/**
 * Called when the user successfully logged in.
 */
- (void)fbDidLogin{
    [self logEvent:@"fbDidLogin"];
    
    [self checkPermissions];
  
    [[NSNotificationCenter defaultCenter] postNotificationName:fbDidLoginNotification object:nil];
}

/**
 * Called when the user dismissed the dialog without logging in.
 */
- (void)fbDidNotLogin:(BOOL)cancelled{
    [self logEvent:@"fbDidNotLogin"];
}

/**
 * Called after the access token was extended. If your application has any
 * references to the previous access token (for example, if your application
 * stores the previous access token in persistent storage), your application
 * should overwrite the old access token with the new one in this method.
 * See extendAccessToken for more details.
 */
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt{
    [self logEvent:@"fbDidLogin"];
}

/**
 * Called when the user logged out.
 */
- (void)fbDidLogout{
    [self logEvent:@"fbDidLogout"];
}

/**
 * Called when the current session has expired. This might happen when:
 *  - the access token expired
 *  - the app has been disabled
 *  - the user revoked the app's permissions
 *  - the user changed his or her password
 */
- (void)fbSessionInvalidated{
    [self logEvent:@"fbSessionInvalidated"];
    [AppDelegate showAlert:@"Your Facebook session is expired."];
}

#pragma mark - FBDialogDelegate

/**
 * Called when the dialog succeeds and is about to be dismissed.
 */
- (void)dialogDidComplete:(FBDialog *)dialog{
    [self logEvent:@"dialogDidComplete"];
}

/**
 * Called when the dialog succeeds with a returning url.
 */
- (void)dialogCompleteWithUrl:(NSURL *)url{
    [self logEvent:@"dialogCompleteWithUrl"];
}

/**
 * Called when the dialog get canceled by the user.
 */
- (void)dialogDidNotCompleteWithUrl:(NSURL *)url{
    [self logEvent:@"dialogDidNotCompleteWithUrl"];
}

/**
 * Called when the dialog is cancelled and is about to be dismissed.
 */
- (void)dialogDidNotComplete:(FBDialog *)dialog{
    [self logEvent:@"dialogDidNotComplete"];
}

/**
 * Called when dialog failed to load due to an error.
 */
- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error{
    [self logEvent:[NSString stringWithFormat:@"dialog: didFailWithError: %@ Full:%@", [error localizedDescription], error]];

    [AppDelegate showErrorAlert:error];
}

/**
 * Asks if a link touched by a user should be opened in an external browser.
 *
 * If a user touches a link, the default behavior is to open the link in the Safari browser,
 * which will cause your app to quit.  You may want to prevent this from happening, open the link
 * in your own internal browser, or perhaps warn the user that they are about to leave your app.
 * If so, implement this method on your delegate and return NO.  If you warn the user, you
 * should hold onto the URL and once you have received their acknowledgement open the URL yourself
 * using [[UIApplication sharedApplication] openURL:].
 */
- (BOOL)dialog:(FBDialog*)dialog shouldOpenURLInExternalBrowser:(NSURL *)url{
    [self logEvent:@"dialog: shouldOpenURLInExternalBrowser"];
    return YES;
}

#pragma mark - FBRequest Delegate

/**
 * Called just before the request is sent to the server.
 */
- (void)requestLoading:(FBRequest *)request{
    [self logEvent:@"requestLoading"];
}

/**
 * Called when the Facebook API request has returned a response.
 *
 * This callback gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response{
    [self logEvent:@"request: didReceiveResponse"];
}

/**
 * Called when an error prevents the request from completing successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error{
    [self logEvent:[NSString stringWithFormat:@"request: didFailWithError: %@ Full:%@", [error localizedDescription], error]];
    
    int widgetId;
    
    if(!_selectedTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_selectedTileInfo.widget.widgetID intValue];
    }
    
    
    if(_selectedItemId && _selectedTileInfo)
    {
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
     
        if(_isSharing)
        {
            [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Facebook" action:@"" status:@"Canceled" deviceType:@"iOS"];
        }
        else{
            
        }
    }
    
    
    [AppDelegate showErrorAlert:error];
}

/**
 * Called when a request returns and its response has been parsed into
 * an object.
 *
 * The resulting object may be a dictionary, an array or a string, depending
 * on the format of the API response. If you need access to the raw response,
 * use:
 *
 * (void)request:(FBRequest *)request
 *      didReceiveResponse:(NSURLResponse *)response
 */


- (void)request:(FBRequest *)request didLoad:(id)result{
    [self logEvent:[NSString stringWithFormat:@"request: didLoad: %@", result]];
    
    if (isCheckingPermissions) {
        isCheckingPermissions = NO;
        
        if ([result isKindOfClass:[NSDictionary class]]) {
            NSDictionary *data = [result valueForKey:@"data"];
            NSArray *per = [data valueForKey:@"user_likes"];
            NSArray *perPost = [data valueForKey:@"publish_actions"];
            
            NSLog(@"data: %@ per: %@, perPost:%@", data, per, perPost);
            
            if ([perPost count]) {
                NSLog(@"Post permission responded");
                NSString *obj = [perPost objectAtIndex:0];
                NSLog(@"obj: %@", obj);
                
                if ([obj isKindOfClass:[NSNumber class]]) {
                    if ([obj integerValue] == 1) {
                        gavePostPermission = YES;
                        NSLog(@"Post permission granted");
                        if(_choosePosting)
                        {
                            [self postFacebookContentsByAppType:nil type:_appType];
                        }
                        else{
                            [self registeringAnEvent];
                        }
                        return;
                    }
                }
            
                //Request permissions again
                [self logFBEvent:@"Request post permission again"];
                [_facebookObj authorize:_permissions];
            
            }
            else
            {
                return;
            }
        }
    }
    else {
        if ([result isKindOfClass:[NSDictionary class]]) {
            
            
            NSString *strTitle = nil;
            if([AppDelegate shareddelegate].appReference.brandname)
            {
                strTitle = [AppDelegate shareddelegate].appReference.brandname;
            }
            else{
                strTitle =  [AppDelegate shareddelegate].appTitle;
            }
            
            SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            
            if ([result objectForKey:@"id"]) {
               
                if(_choosePosting)
                {
                    if(!_selectedItemId)
                    {
                        self.selectedItemId = [NSNumber numberWithInt:0];
                    }
                    if(_selectedItemId && _selectedTileInfo)
                    {
                        int widgetId;
                        
                        if(!_selectedTileInfo.widget)
                        {
                            NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
                            widgetId = [[std objectForKey:kWidgetId] intValue];
                        }
                        else{
                            widgetId = [_selectedTileInfo.widget.widgetID intValue];
                        }
                        
                        
                        [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Facebook" action:@"" status:@"Shared" deviceType:@"iOS"];
                        
                        
                        //self.selectedTileInfo = nil;
                       // self.selectedItemId = nil;
                    }
                    
                    [AppDelegate showAlert:@"Thank you for sharing on Facebook!" withTitle:strTitle];
                }
            }
            else if([result objectForKey:@"result"] && [[result objectForKey:@"result"] isEqualToString:@"true"])
            {
                if(_selectedItemId && _selectedTileInfo)
                {
                    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
                    [service runAddVolunteerRegisterAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] volunteerId:[_selectedItemId integerValue] status:@"Registered" deviceType:@"iOS"];
                }
                [AppDelegate showAlert:@"Thanks for registering!" withTitle:strTitle];
            }
        }
        
    }
    
    
}

/// graph.facebook.com/455454544454/invited/7899323654

/**
 * Called when a request returns a response.
 *
 * The result object is the raw response from the server of type NSData
 */
- (void)request:(FBRequest *)request didLoadRawResponse:(NSData *)data{
    [self logEvent:@"request: didLoadRawResponse"];
}


#pragma mark - FBLoginDialog delegate

- (void)fbDialogLogin:(NSString*)token expirationDate:(NSDate*)expirationDate{
    [self logEvent:@"fb DialogLogin"];
    //[self postFacebookContentsByAppType:nil type:_appType];
}

- (void)fbDialogNotLogin:(BOOL)cancelled{
    [self logEvent:@"fbDialogNotLog"];
}

@end
