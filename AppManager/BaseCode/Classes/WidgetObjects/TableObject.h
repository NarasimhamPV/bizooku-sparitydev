//
//  TableObject.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 30/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface TableObject : NSObject

@property (nonatomic, retain) NSString *website;
@property (nonatomic, retain) NSString *titleText;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *fax;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *primaryEmail;
@property (nonatomic, retain) NSString *state;
@property (nonatomic, retain) NSString *zip;
@property (nonatomic, retain) NSString *additonalInfo;
@property (nonatomic, retain) NSString *newsID;
@property (nonatomic, retain) NSString *eventID;
@property (nonatomic, retain) NSString *locId;
@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *price;

@property (nonatomic, retain) NSString *fbPostID;

@property (nonatomic, retain) NSDate *startDate;
@property (nonatomic, retain) NSDate *endDate;

@property (nonatomic, retain) NSString *startTimeStr;
@property (nonatomic, retain) NSString *endTimeStr;

@property (nonatomic, retain) NSDate *startTime;
@property (nonatomic, retain) NSDate *endTime;

@property (nonatomic, retain) NSString *image;
@property (nonatomic, retain) NSString *name;

@property (nonatomic, retain) NSString *locationName;
@property (nonatomic, retain) NSString *desc;

@property (nonatomic, retain) NSString *lattitude;
@property (nonatomic, retain) NSString *longitude;

@property (nonatomic, retain) CLLocation *location;
@property (nonatomic, readwrite) CLLocationDistance distance;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *eventCategoryName;
@property (nonatomic, retain) NSString *eventCategoryId;

@property (nonatomic, retain) NSString *postDuration;





@end
