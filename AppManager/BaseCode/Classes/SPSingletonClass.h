//
//  SPSingletonClass.h
//  Exceeding
//
//  Created by Veeru on 19/05/14.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>

@interface SPSingletonClass : NSObject<SKProductsRequestDelegate,SKRequestDelegate>
{
    NSArray *myProductArry;

}

+ (id)sharedInstance;

@property (nonatomic,strong) NSString *couponTitleStr;

@property (nonatomic, assign) BOOL isBackgroundAudioButtonPressed;
@property (nonatomic,assign) BOOL isAudioDetailsView;
@property (nonatomic,strong) NSString *screenNameStr;
@property (nonatomic,strong) NSArray *myProductArry;
@property (nonatomic,strong) NSString *backGroundAudioScreen;
@property (nonatomic,strong) NSString *eventDcHaderTitle;
@property (nonatomic,strong) NSString *isMainViewCntr;

@property (nonatomic,strong) NSString *eventViewTypeStr;
@property (nonatomic,strong) NSDate *seletedDate;

@property (nonatomic, assign) BOOL isFirstTimeSevenDigitCode;

@property (nonatomic,strong) id callerID;
@property (nonatomic,assign) int getAppType;
@property (nonatomic,readwrite) BOOL isViewDetailLoaded;

@property (nonatomic,strong) NSString *repeatDateStr;
@property (nonatomic,assign) int fevVideoIndexPath;
@property (nonatomic,strong) NSString *appCategoryName;

@property (nonatomic,retain) AVAudioPlayer *audioPlayer;


- (void)requestProductData;
- (void)inAppPurchaseRequest;
- (void)setAudioPlayerStart;
- (void)setAudioPlayerStop;

@end
