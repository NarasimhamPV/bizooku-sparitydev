//
//  BigBanners.m
//  Exceeding
//
//  Created by Veeru on 02/01/15.
//
//

#import "BigBanners.h"
#import "EventsListVC.h"
#import "AppDelegate.h"
#import "SPSingletonClass.h"
#import "SPCouponsListViewController.h"
#import "NewListVC.h"
#import "AppTableVC.h"
#import "ProductsListVC.h"
#import "AppSubWebVC.h"
#import "FreeAudiosListViewController.h"
#import "ViewController.h"
#import "NewsDetailVC.h"
#import "FundVolDetailVC.h"
#import "EventDetailVC.h"
#import "SPCouponDetailsViewController.h"
#import "ProductsVC.h"
#import "AudioDetailViewController.h"
#import "SPVideoListVC.h"
#import "SPVideoDetailVC.h"

@implementation BigBanners
@synthesize appTableVC;

- (void)getBannerDictionary:(NSDictionary *)bannersDict
{
    
    //    NSLog(@"bannersDict %@",bannersDict);
    
    self.tileBodyVC = [[UIViewController alloc] init];
    if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeEvents]){
        if ( [[bannersDict valueForKey:@"ItemId"]boolValue] == 0){
            EventsListVC *appNewsListVC = [[EventsListVC alloc]initWithNibName:@"EventsListVC" bundle:nil];//CategoryId
            [[NSUserDefaults standardUserDefaults] setValue:[bannersDict valueForKey:@"CategoryId"] forKey:@"EventsCategoryId"];
            appNewsListVC.boolNoListing = NO;
            appNewsListVC.appType = APP_EVENTS;
            self.appType =APP_EVENTS;
            
            self.tileBodyVC = appNewsListVC;
            
        }else{
            AppMainVC *mainVC = [[AppMainVC alloc]init];
            mainVC.appDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil] ;
            mainVC.appDetailVC.appType = self.appType;
            mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            [[SPSingletonClass sharedInstance] setEventDcHaderTitle:[bannersDict valueForKey:@"CategoryName"]];
            self.tileBodyVC = mainVC.appDetailVC;
            mainVC.appDetailVC.detailDataDictionary = bannersDict;
            
        }
        
        [[SPSingletonClass sharedInstance] setGetAppType:APP_EVENTS];
        
        
    }else if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeNews]){
        
        if ( [[bannersDict valueForKey:@"ItemId"]integerValue] == 0){
            NewListVC *appNewsListVC = [[NewListVC alloc]initWithNibName:@"NewListVC" bundle:nil];
            appNewsListVC.boolNoListing = NO;
            [[NSUserDefaults standardUserDefaults] setObject:[bannersDict valueForKey:@"CategoryId"] forKey:@"NewsCategeoryId"];
            self.tileBodyVC = appNewsListVC;
            
        }else{
            AppMainVC *mainVC = [[AppMainVC alloc]init];
            
            mainVC.appDetailVC = [[NewsDetailVC alloc]initWithNibName:@"NewsDetailVC" bundle:nil] ;
            mainVC.appDetailVC.appType = self.appType;
            self.tileBodyVC = mainVC.appDetailVC;
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            //mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
            mainVC.appDetailVC.detailDataDictionary = bannersDict;
        }
        
        
        [[SPSingletonClass sharedInstance] setGetAppType:APP_NEWS];
        
        
    }else if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeCoupons]){
        if ( [[bannersDict valueForKey:@"ItemId"]integerValue]==0){
            SPCouponsListViewController *appCouponsListVC = [[[SPCouponsListViewController alloc]initWithNibName:@"SPCouponsListViewController" bundle:nil]autorelease];
            appCouponsListVC.boolNoListing = NO;
            
            [[NSUserDefaults standardUserDefaults] setValue:[bannersDict valueForKey:@"CategoryId"] forKey:@"CategoryId"];
            [[SPSingletonClass sharedInstance] setGetAppType:APP_COUPONSLIST];
            self.tileBodyVC = appCouponsListVC;
        }else{
            SPCouponDetailsViewController *couponDetailViewCntr = [[SPCouponDetailsViewController alloc] initWithNibName:@"SPCouponDetailsViewController" bundle:nil];
            couponDetailViewCntr.couponsDetailsDict = bannersDict;
            couponDetailViewCntr.appType = APP_COUPONSDETAILS;
            [[SPSingletonClass sharedInstance] setGetAppType:APP_COUPONSDETAILS];
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            
            self.tileBodyVC = couponDetailViewCntr;
            
        }
        
    }else if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeProducts]){
        if ( [[bannersDict valueForKey:@"ItemId"]integerValue]==0){
            ProductsListVC *productsList = [[ProductsListVC alloc] initWithNibName:@"ProductsListVC" bundle:nil];
            productsList.boolNoListing = NO;
            
            [[NSUserDefaults standardUserDefaults] setValue:[bannersDict valueForKey:@"CategoryId"] forKey:@"ProductionCategoryId"];
            
            [[SPSingletonClass sharedInstance] setGetAppType:APP_PRODUCTS];
            
            self.tileBodyVC = productsList;
        }else{
            AppMainVC *mainVC = [[AppMainVC alloc]init];
            mainVC.appDetailVC = [[ProductsVC alloc]initWithNibName:@"ProductsVC" bundle:nil];
            mainVC.appDetailVC.appType = APP_PRODUCTS;
            mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
            [[SPSingletonClass sharedInstance] setGetAppType:APP_PRODUCTS];
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            
            self.tileBodyVC = mainVC.appDetailVC;
            mainVC.appDetailVC.detailDataDictionary = bannersDict;
            
        }
        
        
    }else if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeAudio]){
        if ( [[bannersDict valueForKey:@"ItemId"]integerValue] == 0){
            FreeAudiosListViewController *audioListVC = [[[FreeAudiosListViewController alloc]initWithNibName:@"FreeAudiosListViewController" bundle:nil] autorelease];
            audioListVC.appType = APP_AUDIO;
            audioListVC.widgetConstantType = @"Detail";
            self.tileBodyVC = audioListVC;
            [[SPSingletonClass sharedInstance] setGetAppType:APP_AUDIO];
        }else{
            AudioDetailViewController *audioDetailVC = [[AudioDetailViewController alloc] initWithNibName:@"AudioDetailViewController" bundle:nil];
            audioDetailVC.appType = APP_AUDIO_DETAIL;
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            
            [[SPSingletonClass sharedInstance] setGetAppType:APP_AUDIO_DETAIL];
            
            audioDetailVC.audioDetailDictionary = bannersDict;
            self.tileBodyVC = audioDetailVC;
            
        }
        
        
    }else if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeFundraising]){
        if ( [[bannersDict valueForKey:@"ItemId"]integerValue]==0){
            appTableVC = [[[AppTableVC alloc]initWithNibName:@"AppTableVC" bundle:nil] autorelease];
            
            appTableVC.widgetConstantType = @"Listing";
            appTableVC.noDataAvailableLbl.hidden = YES;
            appTableVC.appType = APP_FUNDRAISING;
            self.tileBodyVC = appTableVC;
            
            
            [[SPSingletonClass sharedInstance] setGetAppType:APP_FUNDRAISING];
        }else{
            FundVolDetailVC *appDetailVC = [[[FundVolDetailVC alloc]initWithNibName:@"FundVolDetailVC" bundle:nil] autorelease];
            appDetailVC.appType = APP_FUNDRAISING;
            [[SPSingletonClass sharedInstance] setGetAppType:APP_FUNDRAISING];
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            
            appDetailVC.detailDataDictionary = bannersDict;
            self.tileBodyVC = appDetailVC;
            
            
        }
        
        
        
        
    }else if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeVideo]){
        
        
        if ( [[bannersDict valueForKey:@"ItemId"]integerValue] == 0){
            SPVideoListVC *appVideoListVC = [[SPVideoListVC alloc]initWithNibName:@"SPVideoListVC" bundle:nil];
            appVideoListVC.boolNoListing = NO;
            [[SPSingletonClass sharedInstance] setAppCategoryName:[bannersDict valueForKey:@"CategoryName"]];
            
            appVideoListVC.appType = APP_VIDEO;
            appVideoListVC.detailDataDictionary = bannersDict;
            [[NSUserDefaults standardUserDefaults] setValue:[bannersDict valueForKey:@"CategoryId"] forKey:@"VideoCategoryId"];
            self.tileBodyVC = appVideoListVC;
            
        }else{
            
            
            AppMainVC *mainVC = [[AppMainVC alloc]init];
            mainVC.appDetailVC = [[SPVideoDetailVC alloc]initWithNibName:@"SPVideoDetailVC" bundle:nil] ;
            mainVC.appDetailVC.appType = APP_VIDEO;
            mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
            mainVC.appDetailVC.detailDataDictionary = bannersDict;
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            [[SPSingletonClass sharedInstance] setAppCategoryName:[bannersDict valueForKey:@"CategoryName"]];
            
            self.tileBodyVC = mainVC.appDetailVC;
            
            
        }
        [[SPSingletonClass sharedInstance] setGetAppType:APP_VIDEO];
        
    }else if ([[bannersDict valueForKey:@"WidgName"]isEqualToString:kTypeVolunteer]){
        
        
        if ( [[bannersDict valueForKey:@"ItemId"]integerValue]==0){
            appTableVC  = [[[AppTableVC alloc]initWithNibName:@"AppTableVC" bundle:nil] autorelease];
            
            appTableVC.widgetConstantType = @"Listing";
            appTableVC.widgetTileInfo = self.loadedTileInfo;
            appTableVC.appType = APP_VOLUNTEER;
            [[SPSingletonClass sharedInstance] setGetAppType:APP_VOLUNTEER];
            appTableVC.noDataAvailableLbl.hidden = YES;
            self.tileBodyVC = appTableVC;
            
        }else{
            FundVolDetailVC *appDetailVC = [[[FundVolDetailVC alloc]initWithNibName:@"FundVolDetailVC" bundle:nil] autorelease];
            appDetailVC.appType = APP_VOLUNTEER;
            [[SPSingletonClass sharedInstance] setGetAppType:APP_VOLUNTEER];
            [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:YES];
            
            appDetailVC.detailDataDictionary = bannersDict;
            self.tileBodyVC = appDetailVC;
            
            
        }
    }
    
    else {
        [AppDelegate showAlert:[NSString stringWithFormat:@"Tile not configured!"] withTitle:[AppDelegate shareddelegate].appReference.brandname];
        return;
        
    }
    
    
    
    [self.bigBannerDelegate didChangeViewCntr:self selectedCntrl:self.tileBodyVC];
}
@end
