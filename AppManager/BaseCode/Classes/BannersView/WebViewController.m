//
//  WebViewController.m
//  Exceeding
//
//  Created by Veeru on 04/06/14.
//
//

#import "WebViewController.h"
#import "AppDelegate.h"


@interface WebViewController ()

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (strong,nonatomic) IBOutlet UIButton *backBtn;

- (IBAction)backToView:(id)sender;

@end

@implementation WebViewController
@synthesize aUrlStr;
@synthesize bannerDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view bringSubviewToFront:self.activity];

    [self.activity startAnimating];
    //[self entryAction];

    [self.webView setOpaque:NO];
    [self.view bringSubviewToFront:self.backBtn];
    
    self.headerObject.headerLabel.text = [bannerDict valueForKey:@"BannerName"];
    self.headerObject.headerLeftBtn.hidden = YES;
    [self.view bringSubviewToFront:self.headerObject.btnShare];
    

    self.headerObject.iconSeperator.hidden = YES;
    NSString *urlLinkStr = [bannerDict valueForKey:@"LinkIphone"];

    NSURL *url = [NSURL URLWithString:urlLinkStr];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    self.webView.scalesPageToFit=YES;
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark --
#pragma mark UIWebView Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activity stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activity stopAnimating];
}

- (IBAction)backToView:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
-(void)entryAction
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    self.itemId = [bannerDict valueForKey:@"BannerId"];
    
    //    self.itemId = [self.audioDetailDictionary objectForKey:@"AudioID"];
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        // [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    else{
        //  [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    
    
}
*/
@end
