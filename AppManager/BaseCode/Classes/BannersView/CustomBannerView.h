//
//  CustomBannerView.h
//  Exceeding
//
//  Created by Veeru on 03/06/14.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AppMainVC.h"
@class ViewController;

@interface CustomBannerView : UIView<UIGestureRecognizerDelegate>
{
    CGFloat axisY;
    CGFloat heightView;
    CGRect bannerFrame;
    NSString *screenNameStr;

}
@property (nonatomic, strong) UITapGestureRecognizer *bannerTapRecognizer;
@property (nonatomic, strong) NSMutableArray *imagesArray;
@property (nonatomic,strong) IBOutlet UIImageView *bannerImageView;
@property (nonatomic,strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic,strong) IBOutlet UIButton *cancelBtn;
@property (nonatomic, strong) NSMutableDictionary *bannersDict;
@property (nonatomic, strong) NSMutableArray *totalBannersArray;
@property (nonatomic,strong) id delegate;

@property (nonatomic, assign) BOOL isCancelClicked;

@property (nonatomic,strong) ViewController *mainViewCntr;

- (void)startShowingBanners:(NSMutableArray *)bannerUrlArray screenDetails:(NSString *)screenName;


- (IBAction)cencleBannersClicked;

- (IBAction)bannerBtnTap:(UITapGestureRecognizer*)recognizer;
- (void)bannerCountService:(NSString *)bannerId BrandId:(NSString *)brandId;
















@end
