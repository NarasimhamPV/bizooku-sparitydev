//
//  WebViewController.h
//  Exceeding
//
//  Created by Veeru on 04/06/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "AppMainVC.h"
#import "SDZEXWidgetServiceExample.h"



@interface WebViewController : AppMainDetailVC <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic) NSString *aUrlStr;
@property (strong,nonatomic) NSDictionary *bannerDict;

@end
