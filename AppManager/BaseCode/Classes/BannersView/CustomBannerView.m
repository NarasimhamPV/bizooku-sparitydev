//
//  CustomBannerView.m
//  Exceeding
//
//  Created by Veeru on 03/06/14.
//
//

#import "CustomBannerView.h"
#import "ConstantsEDM.h"
#import "AsyncImageDownloader.h"
#import "WebViewController.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppMainVC.h"
#import "AppMainDetailVC.h"
#import "EventsListVC.h"
#import "ViewController.h"


@implementation CustomBannerView

@synthesize bannerImageView;
@synthesize bannerTapRecognizer;
@synthesize bannersDict;
@synthesize totalBannersArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUserInteractionEnabled:YES];
        
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)startShowingBanners:(NSMutableArray *)bannerUrlArray screenDetails:(NSString *)screenName
{
    
    NSLog(@"totalBannersArray %@ screenName %@",totalBannersArray,screenName);
    
    screenNameStr = screenName;
    self.mainViewCntr = [ViewController alloc];
    
    if (self.imagesArray) {
        self.imagesArray = nil;
        self.imagesArray = [NSMutableArray array];
    }
    else {
        self.imagesArray = [NSMutableArray array];
    }
    
    bannerImageView.userInteractionEnabled = YES;
    
    
    [self sendSubviewToBack:bannerImageView];
    [self bringSubviewToFront:self.cancelBtn];
    
    NSString *urlStr = nil;
    if ([totalBannersArray count] > 0) {
        urlStr = [[totalBannersArray objectAtIndex:0] objectForKey:@"BannerImage"];
//        NSLog(@"IsCancel %@",[[totalBannersArray objectAtIndex:0] objectForKey:@"IsCancel"]);
        
        if ([[[totalBannersArray objectAtIndex:0] objectForKey:@"BannerType"] isEqualToString:@"Large"]){
            self.cancelBtn.hidden = NO;

            if (IS_IPHONE5) {
                axisY = 288;
                heightView = 280;
                
            }
            else {
                axisY = 200;
                heightView = 280;
                
            }
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isFirstLargeBanner"]boolValue] == YES){
                
                [self setFrame:CGRectMake(0,axisY, 320, 280)];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];

            }else{
                
                [self setFrame:CGRectMake(0,568, 320, 280)];


            }
            
            bannerFrame = CGRectMake(0,axisY, 320, heightView);
            
        }else{
            if ([[[totalBannersArray objectAtIndex:0] objectForKey:@"IsCancel"]boolValue] == YES){
                self.cancelBtn.hidden = NO;
            }else{
                self.cancelBtn.hidden = YES;

            }
            
            if (IS_IPHONE5) {
                axisY = 508;
                heightView = 60;
                
                
            }
            else {
                axisY = 420;
                heightView = 60;
                
            }
            [self setFrame:CGRectMake(0,568, 320, heightView)];
            
            bannerFrame = CGRectMake(0,axisY, 320, heightView);
        }
        
    }
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", kWebServiceURL, urlStr]];
    
    NSData *image = [[NSData alloc] initWithContentsOfURL:imageURL];
    bannerImageView.image =[UIImage imageWithData:image];
    
    self.userInteractionEnabled = YES;
    self.bannerImageView.userInteractionEnabled = YES;
    self.cancelBtn.userInteractionEnabled = YES;
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isFirstLargeBanner"]boolValue] == NO){
        [UIView animateWithDuration:2.0f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             [self setFrame:bannerFrame];
                             
                         }
                         completion:^(BOOL finished){
                             [self downAnimationAfterDealay];
                         }
         ];
    }
    
    //TapGestureRecognizer
    bannerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerBtnTap:)];
    [bannerTapRecognizer setNumberOfTouchesRequired:1];
    bannerTapRecognizer.delegate = self;
    bannerTapRecognizer.enabled = YES;
    [bannerImageView addGestureRecognizer:self.bannerTapRecognizer];
    
    
}

- (void)downAnimationAfterDealay
{
    if ([totalBannersArray count]>0)
    {
        
        NSString *isAlways = [NSString stringWithFormat:@"%@", [[totalBannersArray objectAtIndex:0] objectForKey:@"IsAlways"]];
        
        BOOL isAlwaysEnabled = NO;
        
        if ([isAlways isEqualToString:@"1"]) {
            isAlwaysEnabled = YES;
        }
        else {
            isAlwaysEnabled = NO;
        }
        
        float downAnimationDelay = 0.0f;
        float durationInMilliSeconds = [[[totalBannersArray objectAtIndex:0] objectForKey:@"Duration"] floatValue];
        
        downAnimationDelay = durationInMilliSeconds / 1000.0;
        
        if (isAlwaysEnabled) {
            downAnimationDelay = 99.0f;
        }
        
        self.userInteractionEnabled = YES;
        self.bannerImageView.userInteractionEnabled = YES;
        self.cancelBtn.userInteractionEnabled = YES;
        
        [self performSelector:@selector(animateDownAfterTimer) withObject:nil afterDelay:downAnimationDelay];
    }
}

- (void)animateDownAfterTimer
{
    
    if (self.isCancelClicked) {
        self.isCancelClicked = NO;
        return;
    }
    [UIView animateWithDuration:2.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self setFrame:CGRectMake(0.0f, axisY+heightView, 320.0f,heightView)];
                         
                     }
                     completion:^(BOOL finished){
                         [self nextUpAnimationAfterDealay];
                     }
     ];
    
    
}

- (void)nextUpAnimationAfterDealay
{
    
    if ([totalBannersArray count]) {
        [totalBannersArray removeObjectAtIndex:0];
    }
    
    if ([totalBannersArray count]) {
        [self startShowingBanners:totalBannersArray screenDetails:screenNameStr];
        //        [self performSelector:@selector(startShowingBanners:screenDetails:) withObject:nil afterDelay:0.0f];
        //[self performSelector:@selector(startShowingBanners:) withObject:nil afterDelay:0.0f];
    }
    else {
        return;
    }
}


- (IBAction)cencleBannersClicked
{
    NSLog(@"Closed Banners");
    NSLog(@"screenNameStr %@",screenNameStr);
    
    
    self.isCancelClicked = YES;
    
    [UIView animateWithDuration:0.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self setFrame:CGRectMake(0.0f, axisY+heightView, 320.0f,heightView)];
                         
                     }
                     completion:^(BOOL finished){
                         [self nextUpAnimationAfterDealay];
                     }
     ];
    
    /*
     [UIView beginAnimations:nil context:NULL];
     //    [UIView setAnimationDuration:2.0f];
     [UIView setAnimationDelegate:self];
     [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
     [UIView setAnimationDelay:0.0f];
     [UIView setAnimationDidStopSelector:@selector(nextUpAnimationAfterDealay)];
     [self setFrame:CGRectMake(0.0f, axisY+heightView, 320.0f,heightView)];
     [UIView commitAnimations];
     */
    
    [self exiteAction];
    
}

- (void)exiteAction
{
    //NSLog(@"screenNameStr %@",screenNameStr);
    
    
    NSString * itemID  = [[totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:screenNameStr deviceType:@"iOS"];
    
    
}

- (void)bannerCountService:(NSString *)bannerId BrandId:(NSString *)brandId
{
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    
    //- (void)runAddBannerRegisterAction: (NSString *) brandId bannerId:(NSString *)bannerId

    [example1 runAddBannerRegisterAction:brandId bannerId:bannerId];
    //[example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:screenNameStr deviceType:@"iOS"];
    
 
}

- (void)bannerBtnTap:(UITapGestureRecognizer*)recognizer
{
    NSLog(@"bannerBtnTap");
//    NSLog(@"Temp %@",self.superview.)
    NSDictionary *bannerDic = [totalBannersArray objectAtIndex:0];
    [self bannerCountService:[bannerDic valueForKey:@"BannerId"] BrandId:[bannerDic valueForKey:@"BrandId"]];
    //runAddBannerRegisterAction
    NSString *linkUrl = [[totalBannersArray objectAtIndex:0]valueForKey:@"LinkIphone"];
    NSString *LaunchType = [[totalBannersArray objectAtIndex:0]valueForKey:@"LaunchType"];
    NSString *linkType = [[totalBannersArray objectAtIndex:0]valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        
        [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];

    
        SEL callbackSelector = NSSelectorFromString(@"customBannerCallbackMethod:");
        NSLog(@"self.delegate %@",self.delegate);
        if ([self.delegate respondsToSelector:callbackSelector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.delegate performSelector:callbackSelector withObject:bannerDic];
#pragma clang diagnostic pop
        }

    }else {
        if ([LaunchType isEqualToString:@"Open in App"]){
            
            
            SEL callbackSelector = NSSelectorFromString(@"customBannerCallbackMethod:");
            
            if ([self.delegate respondsToSelector:callbackSelector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.delegate performSelector:callbackSelector withObject:bannerDic];
                
#pragma clang diagnostic pop
            }
            
        }
        else {
            
            NSURL *url = [NSURL URLWithString:linkUrl];
            
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
    }

    
    
}
/*
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    NSLog(@"tag=%@ touchview ===== %@ ++++++ subviews = %@", [NSString stringWithFormat:@"%@", touch], [touch view], [[touch view] subviews]);
    if ([touch view] == self)
    {
        CGPoint location = [touch locationInView:touch.view];
        if (CGRectContainsPoint(CGRectMake(0.0f, 10, 290.0f, 50.0f), location)) {
            NSLog(@"bannerImageView Touched");
            [self bannerBtnTap:nil];
            
        }
        
        if (CGRectContainsPoint(CGRectMake(291.0f, -3.0f, 32.0f, 32.0f), location)) {
            NSLog(@"cancelButton Touched");
            [self cencleBannersClicked];
            
        }
    }
    
}
*/

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//
////    UIView *hitView = [super hitTest:point withEvent:event];
//
//    NSSet *sTouches = [event touchesForView:nil];
//
//    NSLog(@"Touches %d", [sTouches count] );
//
//    return self;
//}

//- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
//
//    if(CGRectContainsPoint(self.bannerImageView.frame, point)) {
//        NSLog(@"touched");
//        return YES; // touched button!
//
//    }
//
//    return NO;
//}

@end
