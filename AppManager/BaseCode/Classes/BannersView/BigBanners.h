//
//  BigBanners.h
//  Exceeding
//
//  Created by Veeru on 02/01/15.
//
//

#import <Foundation/Foundation.h>
#import "ConstantsEDM.h"
#import "DBTilesInfo.h"

@class BigBanners;
@class AppTableVC;
@protocol BigBannersDelegate

- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;

@end

@interface BigBanners : NSObject

@property (assign, nonatomic) UIViewController  *tileBodyVC;
@property (nonatomic,assign) id<BigBannersDelegate> bigBannerDelegate;
@property (nonatomic,strong) AppTableVC *appTableVC;
- (void)getBannerDictionary:(NSDictionary *)bannersDict;
@property (nonatomic, readwrite) AppType appType;

@property (retain, nonatomic) DBTilesInfo *loadedTileInfo;

@end
