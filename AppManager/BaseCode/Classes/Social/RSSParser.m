//
//  Parser.m
//  RssReader
//
//  Created by Oscar Del Ben on 6/11/10.
//  Copyright 2010 DibiStore. All rights reserved.
//

#import "RSSParser.h"


@implementation RSSParser

@synthesize items, responseData;
@synthesize currentTitle;
@synthesize currentDate;
@synthesize currentSummary;
@synthesize currentLink;
@synthesize currentPodcastLink;

- (void)parseRssFeed:(NSString *)url withDelegate:(id)aDelegate {
	[self setDelegate:aDelegate];

	responseData = [[NSMutableData data] retain];
	NSURL *baseURL = [[NSURL URLWithString:url] retain];
	
	
	NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
	
	[[[NSURLConnection alloc] initWithRequest:request delegate:self] autorelease];
}


#pragma mark rssParser methods
// Called when the HTTP socket gets a response.
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"didReceiveResponse");
    [self.responseData setLength:0];
}

// Called when the HTTP socket received data.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)value {
    NSLog(@"didReceiveData");
    [self.responseData appendData:value];
}

// Called when the HTTP request fails.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
	self.responseData  = nil;
    
    if (_delegate && [_delegate respondsToSelector:@selector(podcastFeedsFailed:)]) {
        [_delegate performSelector:@selector(podcastFeedsFailed:) withObject:error];
    }
}



// Called when the connection has finished loading.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    self.items = [[[NSMutableArray alloc] init] autorelease];
	
	NSXMLParser *rssParser = [[NSXMLParser alloc] initWithData:self.responseData];
	[rssParser setDelegate:self];
	[rssParser parse];
    
}

#pragma mark - Feeds Parser

- (void)parserDidStartDocument:(NSXMLParser *)parser {
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
	currentElement = [elementName copy];
	
    if ([elementName isEqualToString:@"item"]) {
        item = [[PodcastItem alloc] init];
        self.currentTitle = [[NSMutableString alloc] init];
        self.currentDate = [[NSMutableString alloc] init];
        self.currentSummary = [[NSMutableString alloc] init];
        self.currentLink = [[NSMutableString alloc] init];		self.currentPodcastLink = [[NSMutableString alloc] init];
    }
	
	// podcast url is an attribute of the element enclosure
	if ([currentElement isEqualToString:@"enclosure"]) {
		[currentPodcastLink appendString:[attributeDict objectForKey:@"url"]];
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    if ([elementName isEqualToString:@"item"]) {
        item.titleText = currentTitle;
        item.link = currentLink;
        item.desc = currentSummary;
        item.url = currentPodcastLink;
		
		// Parse date here
		//NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
		
		//[dateFormatter setDateFormat:@"E, d LLL yyyy HH:mm:ss Z"]; // Thu, 18 Jun 2010 04:48:09 -0700
		NSDate *date = [NSDate date];//[dateFormatter dateFromString:self.currentDate];
		
        item.date = date;
		
        [items addObject:item];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if ([currentElement isEqualToString:@"title"]) {
        [self.currentTitle appendString:string];
    } else if ([currentElement isEqualToString:@"link"]) {
        [self.currentLink appendString:string];
    } else if ([currentElement isEqualToString:@"description"]) {
        [self.currentSummary appendString:string];
    } else if ([currentElement isEqualToString:@"pubDate"]) {
		[self.currentDate appendString:string];
		//NSCharacterSet* charsToTrim = [NSCharacterSet characterSetWithCharactersInString:@" \n"];
		//[self.currentDate setString: [self.currentDate stringByTrimmingCharactersInSet: charsToTrim]];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if (_delegate && [_delegate respondsToSelector:@selector(podcaseFeedsRecieved:)]) {
        [_delegate performSelector:@selector(podcaseFeedsRecieved:) withObject:[NSArray arrayWithArray:self.items]];
    }
    
    self.responseData = nil; 
}

#pragma mark Delegate methods

- (id)delegate {
	return _delegate;
}

- (void)setDelegate:(id)new_delegate {
	_delegate = new_delegate;
}

- (void)dealloc {
	[items release];
	[responseData release];
	[super dealloc];
}

@end
