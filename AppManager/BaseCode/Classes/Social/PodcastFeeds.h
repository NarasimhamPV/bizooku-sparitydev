//
//  PodcastFeeds.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PodcastItem.h"


@protocol PodcastFeedsDelegate;

@interface PodcastFeeds : NSObject <NSURLConnectionDelegate, NSXMLParserDelegate>
{
    //NSString *currentElement;
    //PodcastItem *item;
    PodcastItem *itemAudio;
    PodcastItem *itemVideo;
    
    NSString *currentAudioElement;
    NSString *currentVideoElement;
}


+(PodcastFeeds*)sharedInstance;

-(void)refreshFeedsWithDelegate:(id)delegate type:(AppSubTab)appSubType;

//Moderations for avoid main thread hang
-(void)refreshAudioFeedsWithDelegate:(id)localDelegate;
-(void)refreshVideoFeedsWithDelegate:(id)localDelegate;
-(void)refreshYoutubeFeedsWithDelegate:(id)localDelegate;

@property (nonatomic, assign) id <PodcastFeedsDelegate> delegate;
@property (nonatomic, retain) NSMutableDictionary *videoDictionary;

@end


@protocol PodcastFeedsDelegate <NSObject>

-(void)audioPodcastFeedsRecieved:(NSArray*)array;
-(void)videoPodcastFeedsRecieved:(NSArray*)array;
-(void)youtubePodcastFeedsRecieved:(NSArray*)array;

-(void)podcaseFeedsRecieved:(NSArray*)array;
-(void)podcastFeedsFailed:(NSError*)error;

@end
