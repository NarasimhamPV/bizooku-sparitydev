//
//  PodcastItem.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 25/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConstantsEDM.h"



@interface PodcastItem : NSObject

@property (nonatomic, retain) NSString *titleText;
@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *subTitle;
@property (nonatomic, retain) NSString *summary;
@property (nonatomic, retain) NSString *enclosure;
@property (nonatomic, retain) NSString *link;
@property (nonatomic, retain) NSString *guid;
@property (nonatomic, retain) NSString *pubDate;
@property (nonatomic, retain) NSString *category;
//@property (nonatomic, retain) NSString *explicit;
@property (nonatomic, retain) NSString *duration;
@property (nonatomic, retain) NSString *keywords;
@property (nonatomic, retain) NSString *containts;
@property (nonatomic, retain) NSString *thumbnail;
@property (nonatomic, retain) NSString *image;
@property (nonatomic, retain) NSString *creator;
@property (nonatomic, retain) NSString *feedburnerOrigLink;
@property (nonatomic, retain) NSString *url;

@end

