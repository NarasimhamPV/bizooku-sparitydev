//
//  PodcastFeeds.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 21/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PodcastFeeds.h"
#import "CXMLDocument.h"
#import "CXMLElement.h"
#import "NSString+SBJSON.h"
#import "AppDelegate.h"

static PodcastFeeds *_pdfsInsance = nil;

@interface PodcastFeeds ()

@property (nonatomic, readwrite) AppSubTab appSubType;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSMutableArray *feedArray;
@property (nonatomic, retain) PodcastItem *podcastItem;
@property (nonatomic, retain) NSMutableString *tempAudioString;
@property (nonatomic, retain) NSMutableString *tempVideoString;

//From Jeff
@property (nonatomic, retain) NSMutableArray *itemsAudio;
@property (nonatomic, retain) NSMutableArray *itemsVideo;

@property (retain, nonatomic) NSMutableString *audioTitle;
@property (retain, nonatomic) NSMutableString *audioDate;
@property (retain, nonatomic) NSMutableString *audioSummary;
@property (retain, nonatomic) NSMutableString *audioLink;
@property (retain, nonatomic) NSMutableString *audioPodcastLink;

@property (retain, nonatomic) NSMutableString *videoTitle;
@property (retain, nonatomic) NSMutableString *videoDate;
@property (retain, nonatomic) NSMutableString *videoSummary;
@property (retain, nonatomic) NSMutableString *videoLink;
@property (retain, nonatomic) NSMutableString *videoPodcastLink;

//Parsers
@property (retain, nonatomic) NSMutableString *audioThumbnail;
@property (retain, nonatomic) NSMutableString *videoThumbnail;


@property (nonatomic, assign) NSXMLParser *audioParser;
@property (nonatomic, assign) NSXMLParser *videoParser;

@end


@implementation PodcastFeeds

@synthesize delegate = _delegate;
@synthesize receivedData = _receivedData;
@synthesize videoDictionary = _videoDictionary;
@synthesize appSubType = _appSubType;
@synthesize feedArray = _feedArray;
@synthesize podcastItem = _podcastItem;
@synthesize tempAudioString = _tempAudioString;
@synthesize tempVideoString = _tempVideoString;

//From Jeff
@synthesize audioTitle, videoTitle;
@synthesize audioDate, videoDate;
@synthesize audioSummary, videoSummary;
@synthesize audioLink, videoLink;
@synthesize audioPodcastLink, videoPodcastLink;
@synthesize itemsAudio, itemsVideo;
@synthesize audioThumbnail, videoThumbnail;


@synthesize audioParser, videoParser;

-(id)init{
    if (self = [super init]) {
        //
    }
    
    return self;
}

+(PodcastFeeds*)sharedInstance{

    if (_pdfsInsance == nil) {
        _pdfsInsance = [[PodcastFeeds alloc] init];
    }
    return _pdfsInsance;
}

-(void)refreshAudioFeedsWithDelegate:(id)localDelegate{
    // NSString *channelString = @"http://feeds2.feedburner.com/TedTalks_audio";
    NSString *channelString = [AppDelegate shareddelegate].appReference.podcAudioURL;
    
    if(channelString && ![channelString isEqualToString:@""])
    {
        self.feedArray = [NSMutableArray array];
        
        NSURL *baseURL = [[NSURL URLWithString:channelString] retain];
        NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSLog(@"#refreshAudioFeedsWithDelegate: Completed");
            if (error) {
                if (localDelegate && [localDelegate respondsToSelector:@selector(podcastFeedsFailed:)]) {
                    [localDelegate performSelector:@selector(podcastFeedsFailed:) withObject:nil];
                }
            }
            else {
                self.itemsAudio = [NSMutableArray array];

                /// Checking ....
                NSString *strContent = [[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding] autorelease];
                NSLog(@"#refreshAudioFeedsWithDelegate: Completed %@", strContent);
                
                
                audioParser = [[[NSXMLParser alloc] initWithData:data] autorelease];
                [audioParser setDelegate:self];
                [audioParser parse];
                
                audioParser = nil;
                if (localDelegate && [localDelegate respondsToSelector:@selector(audioPodcastFeedsRecieved:)]) {
                    //[localDelegate performSelector:@selector(audioPodcastFeedsRecieved:) withObject:[NSArray arrayWithArray:self.itemsAudio]];
                    [localDelegate performSelectorOnMainThread:@selector(audioPodcastFeedsRecieved:) withObject:[NSArray arrayWithArray:self.itemsAudio] waitUntilDone:NO];
                }
                else{
                    NSLog(@"Audio delegate not responding");
                }
            }
        }];
    }
    else{
        if (localDelegate && [localDelegate respondsToSelector:@selector(audioPodcastFeedsRecieved:)]) {
            //[localDelegate performSelector:@selector(audioPodcastFeedsRecieved:) withObject:[NSArray arrayWithArray:self.itemsAudio]];
            [localDelegate performSelectorOnMainThread:@selector(audioPodcastFeedsRecieved:) withObject:[NSArray arrayWithArray:nil] waitUntilDone:NO];
        }
    }
}

-(void)refreshVideoFeedsWithDelegate:(id)localDelegate{
    // NSString *channelString = @"http://feeds2.feedburner.com/TedTalks_video";
    NSString *channelString = [AppDelegate shareddelegate].appReference.podcVideoURL;
    
    if(channelString && ![channelString isEqualToString:@""])
    {
        self.feedArray = [NSMutableArray array];
        
        NSURL *baseURL = [[NSURL URLWithString:channelString] retain];
        NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSLog(@"#refreshVideoFeedsWithDelegate: Completed: %@", localDelegate);
            if (error) {
                if (localDelegate && [localDelegate respondsToSelector:@selector(podcastFeedsFailed:)]) {
                    [localDelegate performSelector:@selector(podcastFeedsFailed:) withObject:nil];
                }
            }
            else {
                self.itemsVideo = [NSMutableArray array];
                
                // NSString *strCheckVideoResp = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
                //NSLog(@"The video content is : %@", strCheckVideoResp);
                
                videoParser = [[[NSXMLParser alloc] initWithData:data] autorelease];
                [videoParser setDelegate:self];
                [videoParser parse];
                
                videoParser = nil;
                if (localDelegate && [localDelegate respondsToSelector:@selector(videoPodcastFeedsRecieved:)]) {
                    [localDelegate performSelectorOnMainThread:@selector(videoPodcastFeedsRecieved:) withObject:[NSArray arrayWithArray:self.itemsVideo] waitUntilDone:NO];
                }
                else{
                    NSLog(@"Video delegate not responding");
                }
            }
        }];
    }
    else{
        if (localDelegate && [localDelegate respondsToSelector:@selector(videoPodcastFeedsRecieved:)]) {
            [localDelegate performSelectorOnMainThread:@selector(videoPodcastFeedsRecieved:) withObject:[NSArray arrayWithArray:nil] waitUntilDone:NO];
        }
    }
}

//bryanadams


-(void)refreshYoutubeFeedsWithDelegate:(id)localDelegate{
    
    NSString *channelString = [AppDelegate shareddelegate].appReference.podcYouTubeChannelName;
    
    if(channelString && ![channelString isEqualToString:@""])
    {
    NSString *channelString = [NSString stringWithFormat:@"http://gdata.youtube.com/feeds/api/users/%@/uploads?v=2&alt=json", [AppDelegate shareddelegate].appReference.podcYouTubeChannelName];
    
    self.feedArray = [NSMutableArray array];
    
    NSURL *baseURL = [[NSURL URLWithString:channelString] retain];
    NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSLog(@"#refreshYouTubeFeedsWithDelegate: Completed");
        if (error) {
            if (localDelegate && [localDelegate respondsToSelector:@selector(podcastFeedsFailed:)]) {
                [localDelegate performSelector:@selector(podcastFeedsFailed:) withObject:nil];
            }
        }
        else {
            NSString *str = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
            
            NSDictionary *dic = [str JSONValue];
            
            NSArray *entries = [[dic objectForKey:@"feed"] objectForKey:@"entry"];
            
            if ([entries isKindOfClass:[NSArray class]]) {
                for (NSDictionary *feedString in entries) {
                    if ([feedString isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *mediaGroup = [feedString objectForKey:@"media$group"];
                        NSDictionary *mediaTitle = [mediaGroup objectForKey:@"media$title"];
                        NSDictionary *mediaDesc = [mediaGroup objectForKey:@"media$description"];
                        NSArray *thumbs = [mediaGroup objectForKey:@"media$thumbnail"];
                        NSDictionary *dicThumb = nil;
                        if ([thumbs count] > 4) {
                            dicThumb = [thumbs objectAtIndex:1];
                        }
                        
                        NSDictionary *urlDic = [mediaGroup objectForKey:@"media$player"];
                        
                        //NSLog(@"media group: %@ mediaDesc:%@", mediaGroup, mediaDesc);
                        
                        PodcastItem *anItem = [[[PodcastItem alloc] init] autorelease];
                        anItem.titleText = [mediaTitle objectForKey:@"$t"];
                        anItem.desc = [mediaDesc objectForKey:@"$t"];
                        anItem.url = [urlDic objectForKey:@"url"];
                        anItem.image = [dicThumb objectForKey:@"url"];
                        [_feedArray addObject:anItem];
                    }
                    
                }
                if (localDelegate && [localDelegate respondsToSelector:@selector(youtubePodcastFeedsRecieved:)]) {
                    //[_delegate performSelector:@selector(youtubePodcastFeedsRecieved:) withObject:_feedArray];
                    [localDelegate performSelectorOnMainThread:@selector(youtubePodcastFeedsRecieved:) withObject:_feedArray waitUntilDone:NO];
                }
                else{
                    NSLog(@"Youtube delegate not responding");
                }
            }
            else
            {
                if (localDelegate && [localDelegate respondsToSelector:@selector(youtubePodcastFeedsRecieved:)]) {
                    //[_delegate performSelector:@selector(youtubePodcastFeedsRecieved:) withObject:_feedArray];
                    [localDelegate performSelectorOnMainThread:@selector(youtubePodcastFeedsRecieved:) withObject:nil waitUntilDone:NO];
                }
            }
        }
        
    }];
    }
    else{
        
        if (localDelegate && [localDelegate respondsToSelector:@selector(youtubePodcastFeedsRecieved:)]) {
            //[_delegate performSelector:@selector(youtubePodcastFeedsRecieved:) withObject:_feedArray];
            [localDelegate performSelectorOnMainThread:@selector(youtubePodcastFeedsRecieved:) withObject:nil waitUntilDone:NO];
        }
        
    }
    
}



-(void)refreshFeedsWithDelegate:(id)delegate type:(AppSubTab)appSubType{
    self.delegate = delegate;
    self.appSubType = appSubType;
    
    NSString *channelString = nil;
    
    if (appSubType == TAB_AUDIO) {
        //channelString = @"http://feeds2.feedburner.com/TedTalks_audio";
        channelString = [AppDelegate shareddelegate].appReference.podcAudioURL;
    }
    else if (appSubType == TAB_VIDEOS) {
       // channelString = @"http://feeds2.feedburner.com/TedTalks_video";
        channelString = [AppDelegate shareddelegate].appReference.podcVideoURL;
    }
    else{
        channelString = [NSString stringWithFormat:@"http://gdata.youtube.com/feeds/api/users/%@/uploads?v=2&alt=json", [AppDelegate shareddelegate].appReference.podcYouTubeChannelName];
    }
    
    self.feedArray = [NSMutableArray array];
    
    self.receivedData = [NSMutableData data];
    NSURL *baseURL = [[NSURL URLWithString:channelString] retain];
    NSURLRequest *request = [NSURLRequest requestWithURL:baseURL];
    NSURLConnection *connection = [[[NSURLConnection alloc] initWithRequest:request delegate:self] autorelease];
    
    if (!connection) {
        if (_delegate && [_delegate respondsToSelector:@selector(podcastFeedsFailed:)]) {
            [_delegate performSelector:@selector(podcastFeedsFailed:) withObject:nil];
        }
    }
}


// Called when the HTTP socket gets a response.
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"didReceiveResponse");
    [self.receivedData setLength:0];
}

// Called when the HTTP socket received data.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)value {
    NSLog(@"didReceiveData");
    [self.receivedData appendData:value];
}

// Called when the HTTP request fails.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
	self.receivedData  = nil;
    
    if (_delegate && [_delegate respondsToSelector:@selector(podcastFeedsFailed:)]) {
        [_delegate performSelector:@selector(podcastFeedsFailed:) withObject:error];
    }
}



// Called when the connection has finished loading.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if (self.appSubType == TAB_YOUTUBE) {
        
        NSString *str = [[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *dic = [str JSONValue];
        
       // NSLog(@"Podcast feed response : %@", dic);
        
        NSArray *entries = [[dic objectForKey:@"feed"] objectForKey:@"entry"];
        
        if ([entries isKindOfClass:[NSArray class]]) {
            for (NSDictionary *feedString in entries) {
                if ([feedString isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *mediaGroup = [feedString objectForKey:@"media$group"];
                    NSDictionary *mediaTitle = [mediaGroup objectForKey:@"media$title"];
                    NSDictionary *mediaDesc = [mediaGroup objectForKey:@"media$description"];
                    NSArray *thumbs = [mediaGroup objectForKey:@"media$thumbnail"];
                    NSDictionary *dicThumb = nil;
                    if ([thumbs count] > 4) {
                        dicThumb = [thumbs objectAtIndex:1];
                    }
                    
                    NSDictionary *urlDic = [mediaGroup objectForKey:@"media$player"];
                    
                    NSLog(@"media group: %@ mediaDesc:%@", mediaGroup, mediaDesc);
                    
                    PodcastItem *anItem = [[[PodcastItem alloc] init] autorelease];
                    anItem.titleText = [mediaTitle objectForKey:@"$t"];
                    anItem.desc = [mediaDesc objectForKey:@"$t"];
                    anItem.url = [urlDic objectForKey:@"url"];
                    anItem.image = [dicThumb objectForKey:@"url"];
                    [_feedArray addObject:anItem];
                }
                
            }
            if (_delegate && [_delegate respondsToSelector:@selector(podcaseFeedsRecieved:)]) {
                [_delegate performSelector:@selector(podcaseFeedsRecieved:) withObject:_feedArray];
            }
        }
    }
    else {
        /*
        self.items = [[[NSMutableArray alloc] init] autorelease];
        
        NSXMLParser *rssParser = [[NSXMLParser alloc] initWithData:_receivedData];
        [rssParser setDelegate:self];
        [rssParser parse];
         */
    }
    
    self.receivedData = nil;
}

#pragma mark - Feeds Parser

#pragma mark rssParser methods


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    if (parser == audioParser) {
        currentAudioElement = [elementName copy];
        
        if ([elementName isEqualToString:@"item"]) {
            itemAudio = [[PodcastItem alloc] init];
            self.audioTitle = [[NSMutableString alloc] init];
            self.audioDate = [[NSMutableString alloc] init];
            self.audioSummary = [[NSMutableString alloc] init];
            self.audioLink = [[NSMutableString alloc] init];		
             self.audioThumbnail = [[NSMutableString alloc]init];
            self.audioPodcastLink = [[NSMutableString alloc] init];
        }
        
        if([elementName isEqualToString:@"media:thumbnail"])
        {
            [audioThumbnail appendString:[attributeDict objectForKey:@"url"]];
        }

        // podcast url is an attribute of the element enclosure
        if ([currentAudioElement isEqualToString:@"enclosure"]) {
            [audioPodcastLink appendString:[attributeDict objectForKey:@"url"]];
        }
    }
    else if (parser == videoParser) {
        currentVideoElement = [elementName copy];
        
        if ([elementName isEqualToString:@"item"]) {
            itemVideo = [[PodcastItem alloc] init];
            self.videoTitle = [[NSMutableString alloc] init];
            self.videoDate = [[NSMutableString alloc] init];
            self.videoSummary = [[NSMutableString alloc] init];
            self.videoLink = [[NSMutableString alloc] init];
            self.videoPodcastLink = [[NSMutableString alloc] init];
            self.videoThumbnail = [[NSMutableString alloc]init];
            
        }
        
        if([elementName isEqualToString:@"media:thumbnail"])
        {
             [videoThumbnail appendString:[attributeDict objectForKey:@"url"]];
        }
        
        // podcast url is an attribute of the element enclosure
        if ([currentVideoElement isEqualToString:@"enclosure"]) {
            [videoPodcastLink appendString:[attributeDict objectForKey:@"url"]];
        }
    }
    
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
	
    if (parser == audioParser) {
        PodcastItem *item = itemAudio;
        
        if ([elementName isEqualToString:@"item"]) {
            item.titleText = audioTitle;
            item.link = audioLink;
            item.desc = audioSummary;
            item.url = audioPodcastLink;
            item.image = audioThumbnail;
            NSDate *date = [NSDate date];
            
            item.date = date;
            
            [itemsAudio addObject:item];
            
        }
    }
    else if (parser == videoParser) {
        PodcastItem *item = itemVideo;
        
        
        
        if ([elementName isEqualToString:@"item"]) {
            item.titleText = videoTitle;
            item.link = videoLink;
            item.desc = videoSummary;
            item.url = videoPodcastLink;
            item.image = videoThumbnail;
            NSDate *date = [NSDate date];
            
            item.date = date;
            
            [itemsVideo addObject:item];
        }
    }
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if (parser == audioParser) {
        if ([currentAudioElement isEqualToString:@"title"]) {
            [self.audioTitle appendString:string];
        } else if ([currentAudioElement isEqualToString:@"link"]) {
            [self.audioLink appendString:string];
        } else if ([currentAudioElement isEqualToString:@"description"]) {
            [self.audioSummary appendString:string];
        } else if ([currentAudioElement isEqualToString:@"pubDate"]) {
            [self.audioDate appendString:string];
        }
    }
    else if (parser == videoParser) {
        if ([currentVideoElement isEqualToString:@"title"]) {
            [self.videoTitle appendString:string];
        } else if ([currentVideoElement isEqualToString:@"link"]) {
            [self.videoLink appendString:string];
        } else if ([currentVideoElement isEqualToString:@"description"]) {
            [self.videoSummary appendString:string];
        } else if ([currentVideoElement isEqualToString:@"pubDate"]) {
            [self.videoDate appendString:string];
        }
    }
}

#pragma mark - Parser Errors

// this gives the delegate an opportunity to resolve an external entity itself and reply with the resulting data.

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSLog(@"#!#!#!# parseErrorOccurred: %@", parseError);
    [AppDelegate showErrorAlert:parseError];
    
}
// ...and this reports a fatal error to the delegate. The parser will stop parsing.

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
    [AppDelegate showErrorAlert:validationError];
}
// If validation is on, this will report a fatal validation error to the delegate. The parser will stop parsing.

@end
