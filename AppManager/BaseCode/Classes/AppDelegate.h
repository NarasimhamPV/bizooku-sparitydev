//
//  AppDelegate.h
//  jefftest
//
//  Created by Rajesh Kumar Yandamuri on 07/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

/*

 Bizooku:  com.exceedings.Bizooku
 
 #define kWebServiceURL @"http://exceedings.com/cms/"
 #define kAccessKey @"685de1c6-52a2-4b40-bbc7-fc5ee209ece4"

 */
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <CoreData/CoreData.h>

#import "MBProgressHUD.h"
#import "DBApp.h"

#import <Twitter/Twitter.h>
#import <CoreLocation/CoreLocation.h>

#import "MYCLController.h"
#import "ConstantsEDM.h"
#import "ConstantsShare.h"

#import "DBTilesInfo.h"
#import "DBWidget.h"


@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, MBProgressHUDDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, MyCLControllerDelegate, UIAlertViewDelegate>
{
    MBProgressHUD *HUD;
   // MyCLController *locationController;
    BOOL boolUpdate;
    BOOL forceShowSplash;
}

@property (assign) BOOL isOrientation;

@property (nonatomic, retain) NSString *deviceID;
@property (nonatomic, retain) DBApp *appReference;
@property (nonatomic, retain) MyCLController *locationController;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSString *appTitle;

@property (nonatomic, strong) NSArray *couponsArray;
@property (nonatomic, strong) NSDictionary *couponsdict;
@property (nonatomic, strong) NSString *redeemResponce;

@property (nonatomic, strong) NSArray *couponsListArray;
@property (nonatomic, strong)NSDictionary *couponsListDict;
@property (nonatomic, strong) NSArray *couponsDetailsArray;
@property (nonatomic, strong) NSDictionary *couponsDetailDict;

@property (nonatomic, strong) NSDictionary *bannersDict;
@property (retain, nonatomic) NSArray *bannersArray;


@property (strong, nonatomic) UIImageView *appBGImage;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic, retain) CLLocation *currentLocation;

@property (nonatomic, retain) DBTilesInfo *selectedTileInfo;
@property (nonatomic, retain) NSNumber *selectedItemId;
@property (nonatomic, readwrite) BOOL isFirstTime;

@property (nonatomic, strong) NSDictionary *videoListDict;
@property (nonatomic, strong) NSDictionary *videoDetailDict;
@property (nonatomic, strong) NSDictionary *userFevVideoDict;




//AUDIO
@property (nonatomic, strong) NSDictionary *audioListDictionary;
@property (nonatomic, strong) NSDictionary *audioDetailDictionary;
@property (nonatomic, strong) NSDictionary *backgroundAudiosDictionary;

@property (nonatomic, strong) NSMutableArray *dashboardAudiosArray;
//List.News
@property (nonatomic, strong) NSDictionary *productDict;
@property (strong, nonatomic) NSDictionary *newsListDict;
@property (nonatomic, strong) NSDictionary *eventsListDict;
@property (nonatomic, strong) NSDictionary *eventsDetailsDict;
@property (nonatomic, strong) NSDictionary *eventsCalendarDict;
@property (nonatomic, strong) NSDictionary *volunterDict;

@property (nonatomic, strong) UINavigationController *navController;


+(void)showErrorAlert:(NSError *)error;
+ (void) showAlert:(NSString *)msg withTitle:(NSString *)title;
+(BOOL) validateEmail: (NSString *) inputText;
+ (NSString *) getAccesskey;


- (void)showOfflineConnectionAlert;


-(NSString*)imageBGPath;
+(void)showAlert:(NSString *)msg;
+(NSDictionary*)isValidSoapResponse:(id)value showAlert:(BOOL)showAlert;

+ (AppDelegate*)shareddelegate;
- (IBAction)updateHUDActivity:(NSString*)hudText show:(BOOL)show;

- (void)updateGraphics;

- (void)postMail:(UIViewController*)vControl subject:(NSString *)subj msgBody:(NSString *)body;
-(void)postSMS:(UIViewController*)vControl smsContent:(NSString *)msg;
- (void)postTweet:(UIViewController*)vControl msgBody:(NSString *)str imagePath:(NSString *)imgPath link:(NSString *)link;

+ (UITextView *)resizeTheTextView:(UITextView *)txtView;
+ (UITextView *)resizeCalenderTextView:(UITextView *)txtView;

//vlpeetla: Dummy Commit & Push - Ignore. 

@end
