//
//  StartUpGuideVC.h
//  Exceeding
//
//  Created by Rajesh Kumar on 02/02/13.
//
//

#import <UIKit/UIKit.h>

@interface StartUpGuideVC : UIViewController
{
    IBOutlet UIImageView *splashImg;
    IBOutlet UIImageView *contentImage;
    
    
}

@property (retain, nonatomic) IBOutlet UIButton *btnDismiss;

- (IBAction)dismissStartUp:(id)sender;

@end
