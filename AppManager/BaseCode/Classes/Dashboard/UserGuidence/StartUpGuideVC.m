//
//  StartUpGuideVC.m
//  Exceeding
//
//  Created by Rajesh Kumar on 02/02/13.
//
//

#import "StartUpGuideVC.h"
#import "AppDelegate.h"

@interface StartUpGuideVC ()

@end

@implementation StartUpGuideVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*
    
    if([AppDelegate shareddelegate].window.frame.size.height > 480)
    {
        contentImage.image = [UIImage imageNamed:@"TipPage-iPhone5.png"];
    }
    else{
        contentImage.image = [UIImage imageNamed:@"TipPage-iPhone.png"];
    }
     */
    //splashImg.image = [UIImage imageWithContentsOfFile:[[AppDelegate shareddelegate] imageBGPath]];
    
    
    
    
}



-(NSString*)splashImagePath{
    
    DBApp* _appReference = [AppDelegate shareddelegate].appReference;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *splashPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"splash_%@", [_appReference.splashimage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    
    NSLog(@"splashPath: %@", splashPath);
    return splashPath;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [_btnDismiss release];
    [splashImg release];
    
    [super dealloc];
}
- (void)viewDidUnload {
    
    [self setBtnDismiss:nil];
    [super viewDidUnload];
}
- (IBAction)dismissStartUp:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^(void){ [AppDelegate shareddelegate].isFirstTime = NO;  }];
    
    
}
@end
