//
//  SPCouponsViewController.h
//  Exceeding
//
//  Created by SREELAKSHMI on 19/02/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainVC.h"
#import "BigBanners.h"

@interface SPCouponsViewController : AppMainVC<UISearchBarDelegate,BigBannersDelegate>


@property (strong,nonatomic) NSArray *couponsArray;
@property (strong ,nonatomic) NSDictionary *couponDict;
@property (readwrite, nonatomic) AppType appType;
@property (strong,nonatomic) IBOutlet UIButton *searchBtn;
@property (retain, nonatomic) IBOutlet UISearchBar *couponsSearchBar;
@property (retain, nonatomic) IBOutlet UIView *bannerView;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (nonatomic, strong) NSDictionary *couponBannerDict;

- (IBAction)searchBtnAction:(id)sender;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
