//
//  DashboardBuilder.m
//  jefftest
//
//  Created by Rajesh Kumar Yandamuri on 12/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DashboardBuilder.h"
#import "AppDelegate.h"
#import "ViewController.h"

#import "DBApp.h"
#import "DBLayoutInfo.h"
#import "DBLayoutTypes.h"
#import "DBTilesInfo.h"
#import "DBTileSize.h"
#import "DBWidget.h"
#import "DBSponsorWidget.h"
#import "DBProductWidget.h"
#import "DBCouponsWidget.h"
#import "DBNewsWidget.h"
#import "DBEventsWidget.h"


#import "NSManagedObjectContext-EasyFetch.h"
#import "NSDateFormatter+Exceedings.h"
#import "DBVideoWidget.h"

static DashboardBuilder *_dashInstance = nil;


@implementation DashboardBuilder

@synthesize wsdlService = _wsdlService;
@synthesize refreshMode = _refreshMode;

#pragma mark - Init

-(id)init{
    if (self = [super init]) {
        //
        
        NSString *path = [[NSBundle mainBundle] pathForResource:kPlistName ofType:kPlistType];
        appPrefDic = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    return self;
}

+(DashboardBuilder*)sharedDelegate{
    if (!_dashInstance) {
        _dashInstance = [[DashboardBuilder alloc] init];
    
    }
    return _dashInstance;
}

-(void)callWebServiceInSecondaryThread{
    
}

-(void)updateUsingWebService{
    
    NSLog(@"updateUsingWebService ..");
    //ViewController *mainVC = [AppDelegate shareddelegate].viewController;
    
    //[mainVC.view bringSubviewToFront:mainVC.splashImg];
    
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
    
    self.wsdlService = [[[SDZEXLayoutServiceExample alloc] init] autorelease];
    [NSThread detachNewThreadSelector:@selector(runMe:) toTarget:_wsdlService withObject:_refreshMode];
}

#pragma mark - Core Data

- (void) deleteAllObjects: (NSString *) entityDescription  managedObjectContext:(NSManagedObjectContext *)context{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    
    
    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
        // NSLog(@"%@ object deleted",entityDescription);
    }
    if (![context save:&error]) {
        //  NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}

#pragma mark App Native Stuff update starts here....


-(void)updateCoreData:(NSDictionary*)dictionary{
    
    
    if (dictionary) {
        NSManagedObjectContext *context = [AppDelegate shareddelegate].managedObjectContext;
        NSDateFormatter *dateFormatter = [NSDateFormatter exceedingDateFormat];
        //Delete old data
        [self deleteAllObjects:kDBApp managedObjectContext:context];
        [self deleteAllObjects:kDBLayoutInfo managedObjectContext:context];
        [self deleteAllObjects:kDBLayoutTypes managedObjectContext:context];
        [self deleteAllObjects:kDBTilesInfo managedObjectContext:context];
        [self deleteAllObjects:kDBTileSize managedObjectContext:context];
        [self deleteAllObjects:kDBSponsorWidget managedObjectContext:context];
        [self deleteAllObjects:kDBProductWidget managedObjectContext:context];

        
        //Sparity
        [self deleteAllObjects:kDBCouponsWidget managedObjectContext:context];
        [self deleteAllObjects:kDBNewsWidget managedObjectContext:context];
        [self deleteAllObjects:kDBEventsWidget managedObjectContext:context];
        [self deleteAllObjects:kDBVideoWidget managedObjectContext:context];


        
        //Create default tile sizes
        {
            DBTileSize *SMTile = [NSEntityDescription insertNewObjectForEntityForName:kDBTileSize inManagedObjectContext:context];
            SMTile.idStr = @"SM";
            SMTile.width = [NSNumber numberWithInt:145];
            SMTile.height = [NSNumber numberWithInt:115];
            
            DBTileSize *HLTile = [NSEntityDescription insertNewObjectForEntityForName:kDBTileSize inManagedObjectContext:context];
            HLTile.idStr = @"HL";
            HLTile.width = [NSNumber numberWithInt:300];
            HLTile.height = [NSNumber numberWithInt:115];
            
            DBTileSize *VLTile = [NSEntityDescription insertNewObjectForEntityForName:kDBTileSize inManagedObjectContext:context];
            VLTile.idStr = @"VL";
            VLTile.width = [NSNumber numberWithInt:145];
            VLTile.height = [NSNumber numberWithInt:240];
            
            DBTileSize *HVLTile = [NSEntityDescription insertNewObjectForEntityForName:kDBTileSize inManagedObjectContext:context];
            HVLTile.idStr = @"HVL";
            HVLTile.width = [NSNumber numberWithInt:300];
            HVLTile.height = [NSNumber numberWithInt:240];
        }
        
        
        
        //Store new data
        //Step 1: Create the app
        DBApp *app = [NSEntityDescription insertNewObjectForEntityForName:kDBApp inManagedObjectContext:context];
        {
            id object = [dictionary valueForKey:@"AppName"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.appName = object;
            }
        }
        
        {
            id object = [dictionary valueForKey:@"AboutImage"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.aboutImage = object;
            }
        }
        
        
        //AppStore
        {
            id object = [dictionary valueForKey:@"AppStore"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.appstoreID = object;
                app.appstoreURL = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", object];
            }
            else{
                app.appstoreURL = @"http://itunes.apple.com";
            }
        }
        {
            id object = [dictionary valueForKey:@"AppStoreShort"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.appstoreShort = object;
            }
            else{
                app.appstoreShort = @"http://itunes.apple.com";
            }
        }
        
        //Android
        {
            id object = [dictionary valueForKey:@"Android"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.androidID = object;
                app.androidURL = [NSString stringWithFormat:@"https://market.android.com/details?id=%@", object];
            }
            else{
                app.androidURL = @"http://play.google.com";
            }
        }
        {
            id object = [dictionary valueForKey:@"AndroidShort"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.androidShort = object;
            }
            else{
                app.androidShort = @"http://play.google.com";
            }
        }
        
        {
            if([AppDelegate shareddelegate].window.frame.size.height == 568)
            {
                id object = [dictionary valueForKey:@"BackgroundImage5"];
                if (object && ![object isKindOfClass:[NSNull class]]) {
                    app.backgroundimage = object;
                }
            }
            else{
                id object = [dictionary valueForKey:@"BackgroundImage"];
                if (object && ![object isKindOfClass:[NSNull class]]) {
                    app.backgroundimage = object;
                }
            }
        }
        {
            id object = [dictionary valueForKey:@"BrandId"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.brandid = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"BrandName"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.brandname = object;
            }
        }
        
        {
            id object = [dictionary valueForKey:@"Category"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.category = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"CreatedOn"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.createdon = [dateFormatter dateFromString:object];
            }
        }
        
        
        {
            id object = [dictionary valueForKey:@"Description"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.descrip = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"IsLayoutModified"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.islayoutmodified = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"IsNewLayoutSelected"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.isnewlayoutselected = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"IsPrivateAppEnabled"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                
                app.isPrivateAppEnabled = object;

            }
        }
        {
            id object = [dictionary valueForKey:@"CodeLoginType"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                
                app.codeLoginType = object;
                
            }
        }
        {
            id object = [dictionary valueForKey:@"IsLocalRecordingEnabled"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.islocalRecordingEnabled = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"Keywords"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.keywords = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"LastModified"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.lastmodified = [dateFormatter dateFromString:object];
            }
        }
        {
            id object = [dictionary valueForKey:@"ListWidgets"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.listWidgets = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"Logo"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.logo = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"LogoContainerImg"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.logoContainerImg = object;
            }
        }
        
        {
            id object = [dictionary valueForKey:@"MarketingUrl"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.markettingURL = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"PhoneNumber"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.phoneNum = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"PrivacyUrl"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.privacyURL = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"ServerPath"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.serverPath = object;
            }
        }
        
        {
            id object = [dictionary valueForKey:@"ServerPath"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.serverPath = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"SocialMediaSettings"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
               
                    {
                        id subobject = [object valueForKey:@"AppleStoreLink"];
                        if (subobject && ![subobject isKindOfClass:[NSNull class]]) {
                           // app.appstoreID = subobject;
                            app.appstoreURL = subobject; //[NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", object];
                        }
                        else{
                            app.appstoreURL = @"http://itunes.apple.com";
                        }
                    }
                    
                    {
                        id subobject = [object valueForKey:@"AndroidStoreLink"];
                        if (subobject && ![subobject isKindOfClass:[NSNull class]]) {
                          //  app.androidID = subobject;
                            app.androidURL = subobject; //[NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", object];
                            
                        }
                        else{
                            app.androidURL = @"http://play.google.com";
                        }
                    }
                    
                    {
                        id subobject = [object valueForKey:@"DonationSiteURL"];
                        if (subobject && ![subobject isKindOfClass:[NSNull class]]) {
                            app.donateURL = subobject;
                        }
                        else{
                            app.donateURL = kURLDonate;
                        }
                    }
                {
                    BOOL checkDonations = [[object objectForKey:@"IsDonationEnabled"] boolValue];
                    app.isDonationsEnabled = checkDonations;
                    
                }
                
                {
                    id subObject = [object valueForKey:@"FacebookAppID"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        
                        app.socFBAppID = subObject;
                    }
                }
                
                
                {
                    id subObject = [object valueForKey:@"FacebookPageId"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.socFBPageID = subObject;
                    }
                }
                {
                    id subObject = [object valueForKey:@"TwitterAppID"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.socTwitterID = subObject;
                    }
                }
                {
                    id subObject = [object valueForKey:@"PodcastAudioURL"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.podcAudioURL = subObject;
                    }
                }
                {
                    id subObject = [object valueForKey:@"PodcastVideoURL"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.podcVideoURL = subObject;
                    }
                }
                {
                    id subObject = [object valueForKey:@"TwitterBrandPage"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.socTwitterBrandName = subObject;
                    }
                }
                {
                    id subObject = [object valueForKey:@"YouTubeURL"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.podcYouTubeURL = subObject;
                    }
                }
                {
                    id subObject = [object valueForKey:@"YoutubeChannelUsername"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.podcYouTubeChannelName = subObject;
                    }
                }
                
                
                
                {
                    id subObject = [object valueForKey:@"BrandNickName"];
                    if (subObject && ![subObject isKindOfClass:[NSNull class]]) {
                        app.brandnickname = subObject;
                    }
                }
                
                
            }
        }
        {
            if([AppDelegate shareddelegate].window.frame.size.height == 568)
            {
                id object = [dictionary valueForKey:@"SplashImage5"];
                if (object && ![object isKindOfClass:[NSNull class]]) {
                    app.splashimage = object;
                }
            }
            else{
                id object = [dictionary valueForKey:@"SplashImage"];
                if (object && ![object isKindOfClass:[NSNull class]]) {
                    app.splashimage = object;
                }
            }
        }
        {
            id object = [dictionary valueForKey:@"SupportEmail"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.supportEmail = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"SupportUrl"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.supportURL = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"Title"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.title = object;
            }
        }
        {
            id object = [dictionary valueForKey:@"UserId"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                app.userid = object;
            }
        }
        
        
        
        //[app printMe];
        
        [AppDelegate shareddelegate].appReference = app;
        
        //Step 2: Create layout info
        NSDictionary *dictionaryLayout = [dictionary objectForKey:@"LayoutInfo"];
        
        DBLayoutInfo *layoutInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBLayoutInfo inManagedObjectContext:context];
        {
            id object = [dictionaryLayout valueForKey:@"Action"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.action = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"CreatedBy"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.createdBy = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"CreatedDate"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.createddate = [dateFormatter dateFromString:object];
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"Description"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.descrip = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"HasLogoContainer"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.hasLogoContainer = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"HasNavigationConatiner"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.hasNavigationConatiner = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"Image"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.image = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"IsActive"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.isactive = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"LayoutId"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.layoutid = object;
            }
        }
        {
            id object = [dictionaryLayout valueForKey:@"LayoutName"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                layoutInfo.layoutname = object;
            }
        }
       
        {
            id object = [dictionaryLayout valueForKey:@"LayoutType"];
            if (object && ![object isKindOfClass:[NSNull class]]) {
                //layoutInfo.layoutType = object;
            }
        }
        
        layoutInfo.app = app;
        //layoutInfo.layoutType = [dictionaryLayout valueForKey:@"LayoutName"];
        
        //Step 3: Create Tiles info
        
        if(dictionaryLayout && [dictionaryLayout isKindOfClass:[NSDictionary class]] && [[dictionaryLayout allKeys] containsObject:@"ListTiles"])
        {
            NSArray *tilesLayout = [dictionaryLayout objectForKey:@"ListTiles"];
            
            if(tilesLayout && [tilesLayout isKindOfClass:[NSArray class]])
            {
                for (NSDictionary *tileDictionary in tilesLayout) {
                    
                    DBTilesInfo *tileInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBTilesInfo inManagedObjectContext:context];
//                    NSLog(@"tileInfo %@",tileInfo);
                    {
                        id object = [tileDictionary valueForKey:@"BgColor"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.bgcolor = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"BgImage"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.bgimage = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"ImageType"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.imageType = object;
                        }
                    }
                    //Video Fev
                    {
                        id object = [tileDictionary valueForKey:@"IsUserFavorite"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.isUserFavorite = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"VideoId"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.singleVideoIdStr = object;
                        }
                    }
                    
                    {
                        id object = [tileDictionary valueForKey:@"CategoryListTitle"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.categoryListTitle = object;
                        }
                    }
                    //
                    
                    {
                        id object = [tileDictionary valueForKey:@"BrandId"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.brandid = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"BrandTileId"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.brandTileId = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"DefaultBgColor"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.defaultBgColor = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"DefaultTitleColor"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.defaultTitleColor = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"IsActive"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.isactive = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"IsNewTile"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.isnewtile = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"IsTileConfigured"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.isconfigured = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"LastModifiedOn"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.lastmodifiedon = [dateFormatter dateFromString:object];
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"LayoutTileId"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.layouttileid = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"Position"];
//                        NSLog(@"object: %@", object);
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.position = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"TileId"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.tileid = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"TileName"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.tilename = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"TileType"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.tiletype = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"TitleColor"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.titleColor = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"WeblinkUrl"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.webLinkURL = object;
                        }
                    }
                    
                    {
                        id object = [tileDictionary valueForKey:@"WeblinkInApp"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.webLinkInApp = object;
                        }
                    }
                    
                    {
                        id object = [tileDictionary valueForKey:@"WeblinkName"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.webLinkTitle = object;
                        }
                    }
                    
                    {
                        id object = [tileDictionary valueForKey:@"WeblinkPagetitle"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.webLinkPageTitle = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"EventViewType"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.eventViewType = object;
                        }
                    }
                    
                    
                    {
                        id object = [tileDictionary valueForKey:@"TitleFont"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            if ([object isEqualToString:@"HelveticaCondensed"]) {
                                tileInfo.titleFont = @"Helvetica-Condensed";
                            }
                            else if ([object isEqualToString:@"HelveticaNeueMedium"]) {
                                tileInfo.titleFont = @"HelveticaNeueLTCom-MdCn";
                            }
                            
                            else if ([object isEqualToString:@"ITCAvantGardeStd"]) {
                                tileInfo.titleFont = @"ITCAvantGardeStd-BkCn";
                            }
                            else if ([object isEqualToString:@"ITCAvantGardeStdDemi"]) {
                                tileInfo.titleFont = @"ITCAvantGardeStd-DemiCn";
                            }
                            else{
                                tileInfo.titleFont = object;
                            }
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"TitleFontSize"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.titleFontSize = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"TitlePosition"];
                        if (object && ![object isKindOfClass:[NSNull class]]) {
                            tileInfo.titlePosition = object;
                        }
                    }
                    {
                        id object = [tileDictionary valueForKey:@"Widget"];
                        if (object && [object isKindOfClass:[NSDictionary class]]) {
                            NSDictionary *widgetDic = (NSDictionary*)object;
                            DBWidget *widgetInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBWidget inManagedObjectContext:context];
                            tileInfo.widget = widgetInfo;
                            {
                                id object = [widgetDic valueForKey:@"Action"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.action = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"BrandWidgetId"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.brandWidgetID = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"CreatedDate"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.createddate = [dateFormatter dateFromString:object];
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"Description"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.descrip = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"Image"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.image = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"IsActive"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.isactive = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"IsConfigurable"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.isConfigurable = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"LiveTileInfo"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.liveTileInfo = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"WidgetConfigPage"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.widgetConfigPage = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"WidgetConfigTable"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.widgetConfigTable = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"WidgetId"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.widgetID = object;
                                }
                            }
                            {
                                id object = [widgetDic valueForKey:@"WidgetName"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    widgetInfo.widgetName = object;
                                }
                            }
                            
//#warning checking the sponsorers functionality....
                            {
                                id object = [widgetDic valueForKey:@"ListSponsors"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    
                                    NSArray *arr = (NSArray *)object;
                                    
                                    for(NSDictionary *dicSponsor in arr)
                                    {
                                        
                                        DBSponsorWidget *sponsorInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBSponsorWidget inManagedObjectContext:context];
                                        {
                                            id object = [dicSponsor valueForKey:@"SponserID"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.sponsorId = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"SponserName"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.title = object;
                                            }
                                        }
                                        
                                        {
                                            id object = [dicSponsor valueForKey:@"Email"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.email = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"AboutUs"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.aboutUs = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"Phone"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.phone = object;
                                            }
                                        }
                                        NSString *tileRefSize = [tileDictionary valueForKey:@"Size"];
                                        {
                                            id object = [dicSponsor valueForKey:[self imageKeyByTileType:tileRefSize]];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.tileImage = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"SponsorImage"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.image = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"Website"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.webSite = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"CreatedDate"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                //   sponsorInfo.createdDate = [dateFormatter dateFromString:object];
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"StartDate"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                // sponsorInfo.startDate = [dateFormatter dateFromString:object];
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"EndDate"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                //  sponsorInfo.endDate = [dateFormatter dateFromString:object];
                                            }
                                        }
                                        
                                        
                                        [widgetInfo addSponsorObject:sponsorInfo];
                                        
                                    }
                                }
                            }
                            //ListProducts
                            
                            // News
                            
                            {
                                id object = [widgetDic valueForKey:@"ListNewsCategories"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    
                                    NSArray *arr = (NSArray *)object;
                                    
                                    for(NSDictionary *dicSponsor in arr)
                                    {
                                        
                                        DBNewsWidget *newsInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBNewsWidget inManagedObjectContext:context];
                                    
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryId"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                newsInfo.categoryID = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryName"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                newsInfo.categoryName = object;
                                            }
                                        }
                                        
                                        
                                        [widgetInfo addNewsObject:newsInfo];
                                        
                                    }
                                }
                            }
                            //Events
                            
                            {
                                id object = [widgetDic valueForKey:@"ListEventCategories"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    
                                    NSArray *arr = (NSArray *)object;
                                    
                                    for(NSDictionary *dicSponsor in arr)
                                    {
                                        
                                        DBEventsWidget *eventsInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBEventsWidget inManagedObjectContext:context];
                                        
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryId"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                eventsInfo.categeoryId = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryName"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                eventsInfo.categeoryName = object;
                                            }
                                        }
                                        
                                        
                                        [widgetInfo addEventsObject:eventsInfo];
                                        
                                    }
                                }
                            }

                            
                            
                            
                            {
                                id object = [widgetDic valueForKey:@"ListProducts"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    
                                    NSArray *arr = (NSArray *)object;
                                    
                                    for(NSDictionary *dicSponsor in arr)
                                    {
                                        
                                        DBProductWidget *sponsorInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBProductWidget inManagedObjectContext:context];
                                        {
                                            id object = [dicSponsor valueForKey:@"ProductID"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.productId = object;
                                            }
                                        }
                                        
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryName"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.categoryName = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryId"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                
                                                sponsorInfo.categoryId = object;
                                            }
                                        }
                                        
                                        {
                                            id object = [dicSponsor valueForKey:@"ProductName"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.productName = object;
                                            }
                                        }
                                        
                                        {
                                            id object = [dicSponsor valueForKey:@"Email"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.productEmail = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"AboutUs"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                // sponsorInfo.aboutUs = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"Phone"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.productPhone = object;
                                            }
                                        }
                                        NSString *tileRefSize = [tileDictionary valueForKey:@"Size"];
                                        {
                                            id object = [dicSponsor valueForKey:[self imageKeyByTileType:tileRefSize]];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.productImage = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"ProductImage"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                // sponsorInfo.productImage = object;
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"PageTitle"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                sponsorInfo.productPageTitle = object;
                                            }
                                        }
                                        /* {
                                         id object = [dicSponsor valueForKey:@"Website"];
                                         if (object && ![object isKindOfClass:[NSNull class]]) {
                                         // sponsorInfo.webSite = object;
                                         }
                                         }
                                         {
                                         id object = [dicSponsor valueForKey:@"CreatedDate"];
                                         if (object && ![object isKindOfClass:[NSNull class]]) {
                                         //   sponsorInfo.createdDate = [dateFormatter dateFromString:object];
                                         }
                                         }
                                         {
                                         id object = [dicSponsor valueForKey:@"StartDate"];
                                         if (object && ![object isKindOfClass:[NSNull class]]) {
                                         // sponsorInfo.startDate = [dateFormatter dateFromString:object];
                                         }
                                         }
                                         {
                                         id object = [dicSponsor valueForKey:@"EndDate"];
                                         if (object && ![object isKindOfClass:[NSNull class]]) {
                                         //  sponsorInfo.endDate = [dateFormatter dateFromString:object];
                                         }
                                         }*/
                                        
                                        [widgetInfo addProductsObject:sponsorInfo];
                                        
                                    }
                                }
                            }
                            //Sparity
                            
                            {
                                //tileInfo.titleFontSize = object;
                                id object = [widgetDic valueForKey:@"ListCoupons"];
                                
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    NSArray *arr = (NSArray *)object;
                                    
                                    for(NSDictionary *dicSponsor in arr)
                                    {
                                        DBCouponsWidget *couponsInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBCouponsWidget inManagedObjectContext:context];
                                        {
                                            id object = [dicSponsor valueForKey:@"CouponID"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                couponsInfo.couponId = [NSString stringWithFormat:@"%@", object];
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryName"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                couponsInfo.couponCategoryName = object;
                                            }
                                        }
                                        
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryId"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                couponsInfo.couponCategoryId = [NSString stringWithFormat:@"%@", object];
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"Title"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                 couponsInfo.couponTitle = object;
                                            }
                                        }
                                        NSString *tileRefSize = [tileDictionary valueForKey:@"Size"];
                                        {
                                            id object = [dicSponsor valueForKey:[self imageKeyByTileType:tileRefSize]];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                couponsInfo.couponImage = object;
                                            }
                                        }
                                    
                                        
                                        [widgetInfo addCouponsObject:couponsInfo];

                                    }
                                    
                                }
                            }
                            
                            //Video
                            
                            {
                                id object = [widgetDic valueForKey:@"ListVideoCategories"];
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    
                                    NSArray *arr = (NSArray *)object;
                                    
                                    for(NSDictionary *dicSponsor in arr)
                                    {
                                        DBVideoWidget *videoWidget = [NSEntityDescription insertNewObjectForEntityForName:kDBVideoWidget inManagedObjectContext:context];
                                        {
                                            id object = [dicSponsor valueForKey:@"BrandId"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                videoWidget.videoBrandId = [NSString stringWithFormat:@"%@", object];
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryName"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                videoWidget.videoCategoryName = object;
                                            }
                                        }
                                        
                                        {
                                            id object = [dicSponsor valueForKey:@"CategoryId"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                videoWidget.videoCategoryId = [NSString stringWithFormat:@"%@", object];
                                            }
                                        }
                                        {
                                            id object = [dicSponsor valueForKey:@"IsActive"];
                                            if (object && ![object isKindOfClass:[NSNull class]]) {
                                                videoWidget.videoIsActive = [NSString stringWithFormat:@"%@", object];
                                            }
                                        }
                                        
                                        [widgetInfo addVideosObject:videoWidget];
                                        //                                        [widgetInfo addVideoObject:videoWidget];
                                        
                                        
                                        
                                    }
                                }
                            }
                            
                            //AUDIO
                            {
                                //tileInfo.titleFontSize = object;
                                id object = [widgetDic valueForKey:@"ListAudios"];
                                
                                if (object && ![object isKindOfClass:[NSNull class]]) {
                                    NSDictionary *dictAudio = (NSDictionary *)object;
                                    
                                    NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
                                    
                                    
                                    {
                                        id object = [dictAudio valueForKey:@"AndroidProductID"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:[NSString stringWithFormat:@"%@", object] forKey:@"AndroidProductID"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"AudioID"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            
                                            [mutDict setObject:object forKey:@"AudioID"];
                                        }
                                    }
                                    
                                    {
                                        id object = [dictAudio valueForKey:@"AudioType"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"AudioType"];

                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"BrandId"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"BrandId"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"Description"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"Description"];
                                        }
                                    }

                                    {
                                        id object = [dictAudio valueForKey:@"Fee"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"Fee"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"FileName"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"FileName"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"FilePath"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"FilePath"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"Image"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"Image"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"IphoneProductID"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"IphoneProductID"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"IsActive"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"IsActive"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"PreviewInterval"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"PreviewInterval"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"Price"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"Price"];
                                        }
                                    }
                                    {
                                        id object = [dictAudio valueForKey:@"Title"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"Title"];
                                        }
                                    }
                                    
                                    {
                                        id object = [widgetDic valueForKey:@"WidgetId"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"WidgetId"];
                                        }
                                    }
                                    {
                                        id object = [tileDictionary valueForKey:@"BrandTileId"];
                                        if (object && ![object isKindOfClass:[NSNull class]]) {
                                            [mutDict setObject:object forKey:@"BrandTileId"];
                                        }
                                    }
                                    
                                    
                                    [[AppDelegate shareddelegate].dashboardAudiosArray addObject:mutDict];
                                    
                                }
                            }
                            /*{
                             for(int i=0; i<=5; i++)
                             {
                             DBSponsorWidget *sponsorInfo = [NSEntityDescription insertNewObjectForEntityForName:kDBSponsorWidget inManagedObjectContext:context];
                             if(i%2==0)
                             {
                             sponsorInfo.image = @"http://cdn.ttgtmedia.com/rms/onlineImages/elearningtools_current_145X115.jpg";
                             }
                             else{
                             sponsorInfo.image = @"http://cdn.ttgtmedia.com/rms/onlineImages/infomanagement_current_145X115.jpg";
                             }
                             sponsorInfo.sponsorId = [NSNumber numberWithInt:i];
                             sponsorInfo.title = [NSString stringWithFormat:@"Sponsorer: %d", i];
                             
                             [widgetInfo addSponsorObject:sponsorInfo];
                             }
                             }
                             */
                            
                            
//#warning end
                            
                        }
                    }
                    
                    
                    tileInfo.layoutInfo = layoutInfo;
                    
                    NSString* sizeForTile = [tileDictionary valueForKey:@"Size"];
                    
                    NSArray *sizeObjects = [context fetchObjectsForEntityName:kDBTileSize predicateWithFormat:@"idStr == %@", sizeForTile];
                    
                    if ([sizeObjects count]) {
                        tileInfo.tileSize = [sizeObjects objectAtIndex:0];
                    }
                }
            }
        }
        
        //Calculate x, y based on position
        {
            NSArray *array =[context fetchObjectsForEntityName:kDBTilesInfo sortByKey:@"position" ascending:YES] ;
            
            int topLeftAvailableRow = 0, topRightAvailableRow = 0;
            CGFloat spacing = 10.0;
            
            for(DBTilesInfo *tile in array){
                
                NSString *tileSizeStr = tile.tileSize.idStr;
                int whoIsHigh = MAX(topLeftAvailableRow, topRightAvailableRow);
                
                int rowsRequired = ([tileSizeStr isEqualToString:@"SM"] || [tileSizeStr isEqualToString:@"HL"])?1:2;
                
                //Case 1
                if([tileSizeStr isEqualToString:@"SM"] || [tileSizeStr isEqualToString:@"VL"])
                {
                    if(whoIsHigh == topRightAvailableRow || topLeftAvailableRow == topRightAvailableRow){
                        //Calculate X and Y
                        tile.x = [NSNumber numberWithFloat:spacing];
                        tile.y = [NSNumber numberWithFloat:spacing + (115.0 + spacing)*(topLeftAvailableRow)] ;
                        
                        topLeftAvailableRow += rowsRequired;
                    }
                    else{
                        //Calculate X
                        tile.x = [NSNumber numberWithFloat:spacing*2 + 145.0 ];
                        tile.y = [NSNumber numberWithFloat:spacing + (115.0 + spacing)*(topRightAvailableRow)] ;
                        
                        topRightAvailableRow += rowsRequired;
                        
                        
                    }
                }
                else{
                    if(whoIsHigh == topLeftAvailableRow || topLeftAvailableRow == topRightAvailableRow){
                        //Calculate X and Y
                        tile.x = [NSNumber numberWithFloat:spacing];
                        tile.y = [NSNumber numberWithFloat:spacing + (115.0 + spacing)*(topLeftAvailableRow)] ;
                        
                    }
                    else {
                        //Calculate X
                        tile.x = [NSNumber numberWithFloat:spacing];
                        tile.y = [NSNumber numberWithFloat:spacing + (115.0 + spacing)*(topLeftAvailableRow)] ;
                        
                    }
                    
                    //Increase the next available left and right row counter
                    topLeftAvailableRow = topRightAvailableRow = whoIsHigh + rowsRequired;
                }
            }
        }
        
        [context save:nil];
        
        
        [[AppDelegate shareddelegate].viewController performSelectorOnMainThread:@selector(downlaodImagesInBG) withObject:nil waitUntilDone:NO];
        
//#warning multi calls issue fixed after commenting these lines....
       
        // [[AppDelegate shareddelegate].viewController performSelectorOnMainThread:@selector(updateAllDashboardGraphics) withObject:nil waitUntilDone:NO];
        
    }
}

- (void) downlaodImagesInBG
{
    [[AppDelegate shareddelegate].viewController downlaodImagesInBG];
}

#pragma mark - Utils


- (NSString *)imageKeyByTileType:(NSString *)tileType
{
    if([tileType isEqualToString:@"SM"])
    {
        return @"Image1";
    }
    else if([tileType isEqualToString:@"HL"])
    {
        return @"Image2";
    }
    else if([tileType isEqualToString:@"VL"])
    {
        return @"Image3";
    }
    else if([tileType isEqualToString:@"HVL"])
    {
        return @"Image4";
    }
    return nil;
}
-(CGSize)appSizeForSizeID:(NSString*)sizeID{
    if (sizeID) {
        NSDictionary *iconSizes = [appPrefDic valueForKey:sizeID];
        NSNumber *width = (NSNumber*)[iconSizes valueForKey:kIconWidth];
        NSNumber *height = (NSNumber*)[iconSizes valueForKey:kIconHeight];
        return CGSizeMake([width integerValue], [height integerValue]);
    }
    else{
//        NSLog(@"#warning- Nil size ID Requested: %@", sizeID);
    }
    
//    NSLog(@"#warning- Returning zero size for ID : %@", sizeID);
    return CGSizeZero;
}


-(NSArray *)fetchAllAppItems
{
    return [appPrefDic objectForKey:kApps];
}
            
            
-(NSString *)appTitle:(NSString *)type
{
    NSDictionary *dicBuffer = [[appPrefDic objectForKey:kAppTypes] objectForKey:type];
    if(dicBuffer)
    {
        return [dicBuffer objectForKey:kAppTitle];
    }
  return nil;
}


-(NSString *)appURL:(NSString *)type
{
    NSDictionary *dicBuffer = [[appPrefDic objectForKey:kAppTypes] objectForKey:type];
    if(dicBuffer)
    {
        return [dicBuffer objectForKey:kAppURL];
    }
    return nil;
}


-(NSString *)appDescription:(NSString *)type
{
    NSDictionary *dicBuffer = [[appPrefDic objectForKey:kAppTypes] objectForKey:type];
    if(dicBuffer)
    {
        return [dicBuffer objectForKey:kAppDescription];
    }
    return nil;
}


-(CGSize)fetchIconSizes:(NSString *)sizeType
{
    NSDictionary *dicSize = [[appPrefDic objectForKey:kIconSize] objectForKey:sizeType]; 
    float width = [[dicSize objectForKey:kIconWidth] floatValue];
    float height =  [[dicSize objectForKey:kIconHeight] floatValue];
    CGSize size = CGSizeMake(width, height);
    return size; 
}

-(NSString *)fetchURLForAppType:(NSString *)type
{
    NSArray *arr = [self fetchAllAppItems];

    for(NSDictionary *appDic in arr)
    {
        if([[appDic objectForKey:kAppType] isEqualToString:type])
        {
            NSString *urlString = [appDic objectForKey:@"url"];
            return urlString;
        }
    }
    return nil;
}
@end
