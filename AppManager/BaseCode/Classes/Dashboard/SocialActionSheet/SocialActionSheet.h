//
//  SocialActionSheet.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "Facebook.h"


#import "DBTilesInfo.h"
#import "DBWidget.h"
#import "SDZEXAnalyticsServiceExample.h"

typedef enum ActionType{
    ACTION_SMS = 0,
    ACTION_MAIL = 1,
    ACTION_FACEBOOK,
    ACTION_TWITTER,
    ACTION_POPUP,
    ACTION_NO,
}ActionType;


@protocol SocialActionDelegate;

@interface SocialActionSheet : UIView
{
    BOOL adsremoved;
}

- (IBAction)socialActMessage:(id)sender;
- (IBAction)socialActEmail:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
- (IBAction)bannerCancelBtn:(id)sender;
- (IBAction)socialBannerWebBtn:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
- (IBAction)socialActFacebook:(id)sender;
@property (retain, nonatomic) IBOutlet UIView *bannerView;
- (IBAction)socialActTwitter:(id)sender;
- (IBAction)cancelAction:(id)sender;
@property (nonatomic, strong)NSDictionary *productBannersDict;

@property (nonatomic, strong) NSMutableArray *bannerURLS;
- (void)startShowingBanners:(NSString *)bannerURL;

- (id)initWithDelegate:(id)delegate;

-(void)presentSocialActionSheet:(BOOL)animated;
-(void)dismissSocialActionSheet:(BOOL)animated;

@property (retain, nonatomic) IBOutlet UIView *mainView;
@property (retain, nonatomic) IBOutlet NSObject<SocialActionDelegate> *delegate;
@property (nonatomic, assign) AppType appType;
@property (nonatomic, retain) NSDictionary *detailDictionary;
@property (nonatomic, retain) id detailDataObject;
@property (nonatomic, retain) NSNumber *itemId;

@property (nonatomic, retain) DBTilesInfo *widgetInfo;

@end


@protocol SocialActionDelegate
-(void)socialActionSheetDidDismiss:(SocialActionSheet*)actionSheet;
-(void)socialActionSheet:(SocialActionSheet*)actionSheet cancelAction:(id)sender;
@end
