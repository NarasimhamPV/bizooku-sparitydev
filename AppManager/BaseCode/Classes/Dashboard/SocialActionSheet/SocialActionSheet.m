//
//  SocialActionSheet.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SocialActionSheet.h"
#import "AppDelegate.h"
#import "SBJSON.h"
#import <QuartzCore/QuartzCore.h>
#import "FBIbee.h"
#import "NSString+Exceedings.h"
#import "PodcastItem.h"
#import "PhoneNumberFormatter.h"


@implementation SocialActionSheet

@synthesize mainView;
@synthesize delegate = _delegate;
@synthesize appType = _appType;
@synthesize detailDictionary = _detailDictionary;
@synthesize detailDataObject = _detailDataObject;
@synthesize widgetInfo = _widgetInfo;
@synthesize itemId = _itemId;

- (id)initWithDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        // Initialization code
        self.delegate = delegate;
        
        [[NSBundle mainBundle] loadNibNamed:@"SocialActionSheet" owner:self options:nil];
    }
    
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */




-(void)presentSocialActionSheet:(BOOL)animated{
    
    if(_widgetInfo && _itemId)
    {
        [self addActionInAnalyticsByActionType:@"Entry" action:ACTION_POPUP status:@""];
    }else if(_widgetInfo && !(_itemId))
    {
        self.itemId = 0;
        [self addActionInAnalyticsByActionType:@"Entry" action:ACTION_POPUP status:@""];
    }
    UIViewController *vc = (UIViewController*)_delegate;
    [vc.view addSubview:mainView];
    
    NSLog(@"The widget details : %@ name : %@", _widgetInfo.widget.widgetID, _widgetInfo.widget.widgetName);
    
    mainView.alpha = 0.0;
    
    if([AppDelegate shareddelegate].window.frame.size.height == 568.0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            mainView.transform = CGAffineTransformMakeTranslation(0, -200);
            mainView.alpha = 1.0;
        } completion:^(BOOL finished) {
            //
        }];
    }
    else{
        [UIView animateWithDuration:0.3 animations:^{
            mainView.transform = CGAffineTransformMakeTranslation(0, -282);
            mainView.alpha = 1.0;
        } completion:^(BOOL finished) {
            //
        }];
    }
    
    
}


-(void)dismissSocialActionSheet:(BOOL)animated{
    
    if(_widgetInfo && _itemId)
    {
        [self addActionInAnalyticsByActionType:@"Exit" action:ACTION_POPUP status:@""];
    }else if(_widgetInfo && !(_itemId))
    {
        self.itemId = 0;
        [self addActionInAnalyticsByActionType:@"Exit" action:ACTION_POPUP status:@""];
    }
    
    
    UIViewController *vc = (UIViewController*)_delegate;
    [vc.view addSubview:mainView];
    
    
    if(self.window.frame.size.height == 568.0)
    {
        [UIView animateWithDuration:(animated)?0.3:0.0 animations:^{
            mainView.transform = CGAffineTransformMakeTranslation(0,200);
            mainView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [_delegate performSelectorOnMainThread:@selector(socialActionSheetDidDismiss:) withObject:self waitUntilDone:NO];
        }];
    }
    else{
        [UIView animateWithDuration:(animated)?0.3:0.0 animations:^{
            mainView.transform = CGAffineTransformMakeTranslation(0,282);
            mainView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [_delegate performSelectorOnMainThread:@selector(socialActionSheetDidDismiss:) withObject:self waitUntilDone:NO];
        }];
        
    }
}

- (IBAction)socialMessage:(id)sender
{
    //[self dismissSocialActionSheet:NO];
    
}

- (IBAction)socialActMessage:(id)sender {
    [self dismissSocialActionSheet:NO];
    [self addActionInAnalyticsByActionType:@"Entry" action:ACTION_SMS status:@""];
    [self postSMSContentsByAppType:(UIViewController*)_delegate type:_appType];
}

- (IBAction)socialActEmail:(id)sender {
    
    [self addActionInAnalyticsByActionType:@"Entry" action:ACTION_MAIL status:@""];
    
    [self dismissSocialActionSheet:NO];
    
    [self postMailContentsByAppType:(UIViewController*)_delegate type:_appType];
    
}

- (IBAction)socialActFacebook:(id)sender
{
    NSLog(@"detailDataObject %@",self.detailDataObject);
    if (self.detailDataObject) {
        [FBIbee sharedDelegate].detailObject = self.detailDataObject;
    }
    else{
        [FBIbee sharedDelegate].detailObject = self.detailDictionary;
    }
    
    [AppDelegate shareddelegate].selectedItemId = _itemId;
    if(_widgetInfo && ![_widgetInfo isKindOfClass:[NSNull class]])
    {
        [AppDelegate shareddelegate].selectedTileInfo = _widgetInfo;
    }
    
    [self addActionInAnalyticsByActionType:@"Entry" action:ACTION_FACEBOOK status:@""];
    
    [FBIbee sharedDelegate].isSharing = YES;
    [[FBIbee sharedDelegate] postFacebookContentsByAppType:(UIViewController*)_delegate type:_appType];
    
    [self dismissSocialActionSheet:NO];
}

- (IBAction)socialActTwitter:(id)sender {
    [self dismissSocialActionSheet:NO];
    [self addActionInAnalyticsByActionType:@"Entry" action:ACTION_TWITTER status:@""];
    [self postTwitterContentsByAppType:(UIViewController*)_delegate type:_appType];
}

- (IBAction)cancelAction:(id)sender {
    [self dismissSocialActionSheet:YES];
    [self addActionInAnalyticsByActionType:@"" action:ACTION_POPUP status:@"Canceled"];
    //[_delegate socialActionSheet:self cancelAction:nil];
}


-(NSString *)defaultMailSubject
{
    AppDelegate *del = [AppDelegate shareddelegate];
    NSString *subject = [NSString stringWithFormat:kApplicationMailSubj, del.appReference.brandname];
    return subject;
}

-(NSString *)defaultMailBody
{
    AppDelegate *del = [AppDelegate shareddelegate];
    NSString *msgBodyExt = [NSString stringWithFormat:kApplicationMailContent,del.appReference.descrip];
    NSLog(@"msgBodyExt %@",msgBodyExt);
    NSString *mailBody = [NSString stringWithFormat:[self mailHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],del.appReference.title, msgBodyExt, del.appReference.appstoreURL,del.appReference.androidURL];
    NSLog(@"mailBody %@",mailBody);
    
    return mailBody;
}


- (NSString *)evaluateTheLocationString:(NSDictionary *)dic
{
    NSMutableString *str = [NSMutableString string];
    
    if(dic && [[_detailDictionary objectForKey:@"Location"] isKindOfClass:[NSDictionary class]])
    {
        [NSString stringWithFormat:@"%@, %@",[[_detailDictionary objectForKey:@"Location"] objectForKey:@"City"], [[_detailDictionary objectForKey:@"Location"] objectForKey:@"State"]];
        if(![[[_detailDictionary objectForKey:@"Location"] objectForKey:@"Address"] isEqualToString:@""])
        {
            [str appendString:[[_detailDictionary objectForKey:@"Location"] objectForKey:@"Address"]];
            [str appendString:@", "];
        }
        
        if(![[[_detailDictionary objectForKey:@"Location"] objectForKey:@"City"] isEqualToString:@""])
        {
            [str appendString:[[_detailDictionary objectForKey:@"Location"] objectForKey:@"City"]];
            [str appendString:@", "];
        }
        
        if(![[[_detailDictionary objectForKey:@"Location"] objectForKey:@"State"] isEqualToString:@""])
        {
            [str appendString:[[_detailDictionary objectForKey:@"Location"] objectForKey:@"State"]];
            
        }
        return str;
        
    }
    return str;
}

#pragma Post Mail Dynamic Content

- (void)addActionInAnalyticsByActionType :(NSString *)actionType action:(ActionType)action status:(NSString *)status
{
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    NSString *strAction = nil;
    
    switch (action) {
        case ACTION_FACEBOOK:
        {
            strAction = @"Facebook";
        }
            break;
        case ACTION_TWITTER:
        {
            strAction = @"Twitter";
        }
            break;
        case ACTION_MAIL:
        {
            strAction = @"Email";
        }
            break;
        case ACTION_SMS:
        {
            strAction = @"SMS";
        }
            break;
        case ACTION_NO:
        {
            strAction = @"";
        }
            break;
        case ACTION_POPUP:
        {
            strAction = @"Popup";
        }
            break;
            
        default:
            break;
    }
    
    int widgetId;
    
    if(!_widgetInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_widgetInfo.widget.widgetID intValue];
    }
    NSLog(@"brandid %d %d %d %@ %@ %@",[[AppDelegate shareddelegate].appReference.brandid integerValue],widgetId,[_itemId integerValue],strAction,actionType,status);
    [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId integerValue] actionType:strAction action:actionType status:status deviceType:@"iOS"];
    
}


- (IBAction)share {
    NSString *postText = @"some text";
    UIImage *postImage = [UIImage imageNamed:@"myImage"];
    NSURL *postURL = [NSURL URLWithString:@"myUrl"];
    NSArray *activityItems = @[postText, postImage, postURL];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityController.excludedActivityTypes =
    @[
      UIActivityTypePrint,
      UIActivityTypeCopyToPasteboard,
      UIActivityTypeAssignToContact,
      UIActivityTypeSaveToCameraRoll,
      UIActivityTypeCopyToPasteboard,
      UIActivityTypeMail,
      UIActivityTypeMessage,
      UIActivityTypePostToWeibo,
      ];
    [activityController setCompletionHandler:^(NSString *activityType, BOOL completed) {
        if ([activityType isEqualToString:UIActivityTypePostToFacebook]) {
            if (completed){
                
            }
            //[self doSomethingForFB];
        } else if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
            if (completed){
                
            }
            //[self doSomethingForTwitter];
        }
    }];
    //[self presentViewController:activityController animated:YES completion:nil];
}


- (void)postMailContentsByAppType:(UIViewController *)vControl type:(AppType)type
{
    
    
    
    NSString *subject = nil;
    NSString *mailBody = nil;
    AppDelegate *del = [AppDelegate shareddelegate];
    NSLog(@"type %d",type);
    switch (type) {
            
        case APP_NONE:
        {
            subject = [self defaultMailSubject];
            mailBody = [self defaultMailBody];
            
        }
            break;
        case APP_ABOUT:
        {
            subject = [self defaultMailSubject];
            AppDelegate *del = [AppDelegate shareddelegate];
            NSString *msgBodyExt = [NSString stringWithFormat:kApplicationMailContent,del.appReference.descrip];
            mailBody = [NSString stringWithFormat:[self mailHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],del.appReference.title, msgBodyExt, del.appReference.appstoreURL,del.appReference.androidURL];
        }
            break;
        case APP_CONTRIBUTE:
        {
            subject = [self defaultMailSubject];
            mailBody = [self defaultMailBody];
        }
            break;
        case APP_DONATE:
        {
            subject = [self defaultMailSubject];
            mailBody = [self defaultMailBody];
        }
            break;
        case APP_SPONSOR:
        {
            NSLog(@"THe sposnsor ---> postMailContentsByAppType %@", _detailDictionary);
            
            
            if(_detailDictionary)
            {
                NSString *postTitle = nil;
                NSString *postThumbnail = nil;
                NSString *postDescription = nil;
                NSString *postLink = nil;
                NSString *postSubject = nil;
                NSString *links = nil;
                
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"SponserName"])
                {
                    postSubject = [NSString stringWithFormat:@"%@ is making a difference by supporting %@", [_detailDictionary objectForKey:@"SponserName"], del.appReference.brandname];
                }
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"SponserName"])
                {
                    postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"SponserName"]];
                }
                else{
                    postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                }
                
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"SponserImage"] && ![[_detailDictionary valueForKey:@"SponserImage"] isEqualToString:@""])
                {
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [_detailDictionary objectForKey:@"SponserImage"]];
                }
                else{
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                }
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"AboutUs"])
                {
                    NSString *phoneNum = nil;
                    
                    PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
                    phoneNum = /*[dic valueForKey:@"Phone"]; */ [phoneNumberFormatter format:[_detailDictionary objectForKey:@"Phone"] withLocale:@"us"];
                    
                    if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Website"])
                    {
                        links = [NSString stringWithFormat:@"Website: %@,<br/>Email: %@,<br/>Phone Number: %@",[_detailDictionary objectForKey:@"Website"],[_detailDictionary objectForKey:@"Email"],phoneNum];
                    }
                    
                    postDescription = [NSString stringWithFormat:@"I am telling every one about %@ because they support %@, which is an organization I believe in and support too. Here is more info on them.", [_detailDictionary objectForKey:@"SponserName"], del.appReference.brandname];
                }
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Website"])
                {
                    postLink = [NSString stringWithFormat:@"%@",[_detailDictionary objectForKey:@"Website"]];
                }
                else{
                    postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                }
                NSString *tempBody = [NSString stringWithFormat:@"%@",postDescription];
                subject = [NSString stringWithFormat:@"%@",postSubject];
                mailBody = [NSString stringWithFormat:[self mailSponsorHTMLContent],postThumbnail,postLink,postTitle,  tempBody,[_detailDictionary objectForKey:@"AboutUs"], links, postTitle,del.appReference.appstoreURL,del.appReference.androidURL];
            }
            else{
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
            
            
            
        }
            break;
        case APP_PRODUCTS:
        {
            NSLog(@"THe Products ---> postMailContentsByAppType %@", _detailDictionary);
            NSString *titleId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBrandTileId"]];
            
            NSLog(@"nick name %@",del.appReference.brandnickname);
            if(_detailDictionary)
            {
                NSString *postTitle = nil;
                NSString *postThumbnail = nil;
                NSString *postDescription = nil;
                NSString *postLink = nil;
                NSString *postSubject = nil;
                NSString *links = nil;
                
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Title"])
                {
                    postSubject = [NSString stringWithFormat:@"Check this out! %@ from %@", [_detailDictionary objectForKey:@"Title"], del.appReference.brandname];
                }
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Title"])
                {
                    postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"Title"]];
                }
                else{
                    postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                }
                
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Image"] && ![[_detailDictionary valueForKey:@"Image"] isEqualToString:@""])
                {
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [_detailDictionary objectForKey:@"Image"]];
                }
                else{
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                }
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Title"])
                {
                    NSString *phoneNum = nil;
                    
                    PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
                    phoneNum = /*[dic valueForKey:@"Phone"]; */ [phoneNumberFormatter format:[_detailDictionary objectForKey:@"Phone"] withLocale:@"us"];
                    
                    if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Price"])
                    {
                        links = [NSString stringWithFormat:@"Price: %@,<br/>Email: %@,<br/>Phone Number: %@",[_detailDictionary objectForKey:@"Price"],[_detailDictionary objectForKey:@"Email"],phoneNum];
                    }
                    postDescription = [_detailDictionary objectForKey:@"Description"]; // [NSString stringWithFormat:@"Check this out from %@",  del.appReference.brandname];
                }
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"ProductID"])
                {
                    // postLink = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"Website"]];
                    postLink = [NSString stringWithFormat:kAppProductWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"ProductID"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId];//
                }
                else{
                    postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                }
                NSString *tempBody = [NSString stringWithFormat:@"%@",postDescription];
                subject = [NSString stringWithFormat:@"%@",postSubject];
                mailBody = [NSString stringWithFormat:[self mailProductsHTMLContent],postThumbnail,postLink,postTitle,tempBody,@" ",del.appReference.appstoreURL,del.appReference.androidURL];
            }
            else{
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
            
            
            
        }
            break;
            
            
        case APP_WEBLINK:
        {
            NSLog(@"THe Products ---> APP_WEBLINK %@", _detailDictionary);
            
            
            if(_widgetInfo)
            {
                NSString *postTitle = nil;
                NSString *postThumbnail = nil;
                NSString *postLink = nil;
                NSString *postSubject = nil;
                
                
                if(_widgetInfo.webLinkPageTitle && ![_widgetInfo.webLinkPageTitle isKindOfClass:[NSNull class]])
                {
                    postSubject = [NSString stringWithFormat:@"Check this %@ from %@",_widgetInfo.webLinkPageTitle, del.appReference.brandname];
                }
                else{
                    postSubject = [NSString stringWithFormat:@"Check this out! %@", del.appReference.brandname];
                }
                
                if(_widgetInfo.webLinkTitle && ![_widgetInfo.webLinkTitle isKindOfClass:[NSNull class]])
                {
                    postTitle = [NSString stringWithFormat:@"%@",_widgetInfo.webLinkTitle];
                }
                else{
                    postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                }
                
                if(_widgetInfo.webLinkURL && ![_widgetInfo.webLinkURL isKindOfClass:[NSNull class]])
                {
                    postLink = [NSString stringWithFormat:@"%@",_widgetInfo.webLinkURL];
                }
                else{
                    postLink = [NSString stringWithFormat:@"%@", del.appReference.aboutURL];
                }
                
                {
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                }
                
                NSString *tempBody = [NSString stringWithFormat:@"Check this out from %@",del.appReference.brandname];
                subject = [NSString stringWithFormat:@"%@",postSubject];
                mailBody = [NSString stringWithFormat:[self mailWebLinkHTML],postThumbnail,postLink,postTitle,tempBody,del.appReference.appstoreURL,del.appReference.androidURL];
            }
            else{
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
            
            
            
        }
            break;
            
            
        case APP_EVENTS:
        {
            NSLog(@"_detailDictionary %@",_detailDictionary);
            if(_detailDictionary)
            {
                NSString *androidURL = nil;
                
                {
                    androidURL = del.appReference.androidURL;
                    
                }
                
                NSString *appStoreURL = nil;
                
                {
                    appStoreURL = del.appReference.appstoreURL;
                    
                }
                
                NSString *img = nil;
                //  img = [_detailDictionary objectForKey:@"Image"];
                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    img = [_detailDictionary objectForKey:@"Image"];
                }
                else if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    img = del.appReference.aboutImage;
                }
                else{
                    img = del.appReference.splashimage;
                }
                NSDate *startDt = [[_detailDictionary objectForKey:@"StartDate"] exceedingsDateFromServerNewsString];
                
                NSDateFormatter *df = [[[NSDateFormatter alloc]init] autorelease];
                [df setDateFormat:@"MMM dd, yyyy"];
                
                NSString *startString = [df stringFromDate:startDt];
                
                subject = [NSString stringWithFormat:kEventsMailSubj, [_detailDictionary objectForKey:@"Title"], del.appReference.brandname];
                
                NSString *addre = [self evaluateTheLocationString:_detailDictionary];
                NSLog(@"addre %@",addre);
                
                if (!addre.length){
                    addre = @"--";
                }
                
                NSString *tempBody = [NSString stringWithFormat:kEventsMailContent,del.appReference.brandname, startString,[_detailDictionary objectForKey:@"StartTime"],[_detailDictionary objectForKey:@"EndTime"],addre,[_detailDictionary objectForKey:@"Description"]];
                NSString *titleId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBrandTileId"]];


                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    mailBody = [NSString stringWithFormat:[self mailEventHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,img],[NSString stringWithFormat:kAppEventWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"EventId"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL, androidURL];
                    
                    //mailBody = [NSString stringWithFormat:[self mailEventHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,img],[NSString stringWithFormat:kFBEventPageURL, [_detailDictionary objectForKey:@"FBPostId"]],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL, androidURL];
                }
                else if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    mailBody = [NSString stringWithFormat:[self mailEventHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,img],[NSString stringWithFormat:kAppEventWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"EventId"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL, androidURL];

                    //mailBody = [NSString stringWithFormat:[self mailEventHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,img],[NSString stringWithFormat:kFBEventPageURL, [_detailDictionary objectForKey:@"FBPostId"]],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL, androidURL];
                }
                else {
                    mailBody = [NSString stringWithFormat:[self mailEventHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,img],[NSString stringWithFormat:kAppEventWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"EventId"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL, androidURL];

                   // mailBody = [NSString stringWithFormat:[self mailEventHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,img],[NSString stringWithFormat:kFBEventPageURL, [_detailDictionary objectForKey:@"FBPostId"]],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL, androidURL];
                }
                NSLog(@"mailBody %@",mailBody);
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
        }
            break;
            
            
        case APP_FUNDRAISING:
        {
            if(_detailDictionary)
            {
                NSString *androidURL = nil;
                
                {
                    androidURL = del.appReference.androidURL;
                    
                    /* id object = [_detailDictionary valueForKey:@"AndroidStoreLink"];
                     if (object && ![object isKindOfClass:[NSNull class]]) {
                     androidURL = [_detailDictionary objectForKey:@"AndroidStoreLink"];
                     }
                     else{
                     androidURL = del.appReference.androidURL;
                     }
                     */
                    
                }
                
                NSString *appStoreURL = nil;
                
                {
                    appStoreURL = del.appReference.appstoreURL;
                    /*id object = [_detailDictionary valueForKey:@"AppleStoreLink"];
                     if (object && ![object isKindOfClass:[NSNull class]]) {
                     appStoreURL = [_detailDictionary objectForKey:@"AppleStoreLink"];
                     }
                     else{
                     appStoreURL = del.appReference.appstoreURL;
                     }
                     */
                }
                
                
                
                subject = [NSString stringWithFormat:kFundraisingEmailSubj, del.appReference.brandname];
                NSDate *startDt = [[_detailDictionary objectForKey:@"StartDate"] exceedingsDateFromServerNewsString];
                NSDate *endDt = [[_detailDictionary objectForKey:@"EndDate"] exceedingsDateFromServerNewsString];
                
                NSDateFormatter *df = [[[NSDateFormatter alloc]init] autorelease];
                [df setDateFormat:@"MMM dd, yyyy"];
                
                NSString *startString = [df stringFromDate:startDt];
                NSString *endString = [df stringFromDate:endDt];
                
                NSString *amountCollected = [NSString stringWithFormat:@"%@", [_detailDictionary valueForKey:@"AmountCollected"]];
                NSString *raise = [NSString stringWithFormat:@"%@", [amountCollected millionsFormatDollarString]];
                
                
                NSString *goal = [NSString stringWithFormat:@"%@",[_detailDictionary objectForKey:@"Goal"]];
                NSString *goalVal = [NSString stringWithFormat:@"%@", [goal millionsFormatDollarString]];
                
                NSString *tempBody = [NSString stringWithFormat:kFundraisingEmailContent,del.appReference.brandname,[_detailDictionary objectForKey:@"Name"], startString, endString, goalVal,raise,[_detailDictionary objectForKey:@"Description"]];
                
                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                else if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.aboutImage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                else {
                    mailBody = [NSString stringWithFormat:[self mailHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
                
            }
        }
            break;
        case APP_LOCATIONS:
        {
            subject = [self defaultMailSubject];
            mailBody = [self defaultMailBody];
        }
            break;
        case APP_MEDIA:
        {
            if(_detailDataObject)
            {
                PodcastItem *item = (PodcastItem *)_detailDataObject;
                
                NSString *tempBody = [NSString stringWithFormat:@"%@",item.desc];
                subject = [NSString stringWithFormat:kMediaMailSubj, del.appReference.brandname];
                mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],item.image,item.url,item.titleText,  tempBody,del.appReference.appstoreURL,del.appReference.androidURL];
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
        }
            break;
        case APP_NEWS:
        {
            if(_detailDictionary)
            {
                NSString *androidURL = nil;
                
                {
                    androidURL = del.appReference.androidURL;
                    /* id object = [_detailDictionary valueForKey:@"AndroidStoreLink"];
                     if (object && ![object isKindOfClass:[NSNull class]]) {
                     androidURL = [_detailDictionary objectForKey:@"AndroidStoreLink"];
                     }
                     else{
                     androidURL = del.appReference.androidURL;
                     }*/
                }
                
                NSString *appStoreURL = nil;
                
                {
                    /* id object = [_detailDictionary valueForKey:@"AppleStoreLink"];
                     if (object && ![object isKindOfClass:[NSNull class]]) {
                     appStoreURL = [_detailDictionary objectForKey:@"AppleStoreLink"];
                     }
                     else{
                     appStoreURL = del.appReference.appstoreURL;
                     }
                     */
                    appStoreURL = del.appReference.appstoreURL;
                }
                
                
                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    
                    NSString *tempBody = [NSString stringWithFormat:kNewsMailContent,[_detailDictionary objectForKey:@"Description"]];
                    subject = [NSString stringWithFormat:kNewsMailSubj, del.appReference.brandname];
                    mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]],[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL,androidURL];
                }
                else  if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    
                    NSString *tempBody = [NSString stringWithFormat:kNewsMailContent,[_detailDictionary objectForKey:@"Description"]];
                    subject = [NSString stringWithFormat:kNewsMailSubj, del.appReference.brandname];
                    mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.aboutImage],[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL,androidURL];
                }
                else{
                    NSString *tempBody = [NSString stringWithFormat:kNewsMailContent,[_detailDictionary objectForKey:@"Description"]];
                    subject = [NSString stringWithFormat:kNewsMailSubj, del.appReference.brandname];
                    mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Title"], tempBody, appStoreURL,androidURL];
                }
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
        }
            break;
        case APP_SHARE:
        {
            subject = [self defaultMailSubject];
            mailBody = [self defaultMailBody];
        }
            break;
        case APP_SOCIAL:
        {
            AppDelegate *del = [AppDelegate shareddelegate];
            subject = [NSString stringWithFormat:kSocialMailSubj, del.appReference.brandname];
            NSString *msgBodyExt = [NSString stringWithFormat:kSocialMailContent,[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID], [NSString stringWithFormat:kTwitterFanPage, del.appReference.socTwitterBrandName]];
            mailBody = [NSString stringWithFormat:[self mailHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:@"%@",del.appReference.markettingURL],del.appReference.title, msgBodyExt, del.appReference.appstoreURL,del.appReference.androidURL];
        }
            break;
            
        case APP_AUDIO_DETAIL:
        {
            if(_detailDictionary)
            {
                NSLog(@"_detailDictionary %@",_detailDictionary);
                NSString *androidURL = nil;
                {
                    androidURL = del.appReference.androidURL;
                }
                NSString *appStoreURL = nil;
                {
                    appStoreURL = del.appReference.appstoreURL;
                    
                }
                
                NSString *tempBody = [_detailDictionary objectForKey:@"Description"];
                //kAudioEmailSubj
                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    subject = [NSString stringWithFormat:@"Check out %@ from %@",[_detailDictionary objectForKey:@"Title"],del.appReference.brandname];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"AudioType"], tempBody, appStoreURL,androidURL];
                }
                else if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    subject = [_detailDictionary objectForKey:@"Title"];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.aboutImage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                else {
                    subject = [_detailDictionary objectForKey:@"Title"];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
        }
            break;
            
        case APP_AUDIO_PURCHASE:
        {
            if(_detailDictionary)
            {
                NSLog(@"_detailDictionary %@",_detailDictionary);
                NSString *androidURL = nil;
                {
                    androidURL = del.appReference.androidURL;
                }
                NSString *appStoreURL = nil;
                {
                    appStoreURL = del.appReference.appstoreURL;
                    
                }
                
                NSString *tempBody = [_detailDictionary objectForKey:@"Description"];
                //kAudioEmailSubj
                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    subject = [NSString stringWithFormat:@"Check out %@ from %@",[_detailDictionary objectForKey:@"Title"],del.appReference.brandname];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"AudioType"], tempBody, appStoreURL,androidURL];
                }
                else if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    subject = [_detailDictionary objectForKey:@"Title"];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.aboutImage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                else {
                    subject = [_detailDictionary objectForKey:@"Title"];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
        }
            break;
            
            
        case APP_VOLUNTEER:
        {
            if(_detailDictionary)
            {
                NSString *androidURL = nil;
                
                {
                    androidURL = del.appReference.androidURL;
                    /* id object = [_detailDictionary valueForKey:@"AndroidStoreLink"];
                     if (object && ![object isKindOfClass:[NSNull class]]) {
                     androidURL = [_detailDictionary objectForKey:@"AndroidStoreLink"];
                     }
                     else{
                     androidURL = del.appReference.androidURL;
                     }
                     */
                }
                
                NSString *appStoreURL = nil;
                
                {
                    appStoreURL = del.appReference.appstoreURL;
                    /*  id object = [_detailDictionary valueForKey:@"AppleStoreLink"];
                     if (object && ![object isKindOfClass:[NSNull class]]) {
                     appStoreURL = [_detailDictionary objectForKey:@"AppleStoreLink"];
                     }
                     else{
                     appStoreURL = del.appReference.appstoreURL;
                     }
                     */
                    
                }
                
                
                
                NSDate *startDt = [[_detailDictionary objectForKey:@"StartDate"] exceedingsDateFromServerNewsString];
                NSDate *endDt = [[_detailDictionary objectForKey:@"EndDate"] exceedingsDateFromServerNewsString];
                
                NSDateFormatter *df = [[[NSDateFormatter alloc]init] autorelease];
                [df setDateFormat:@"MMM dd, yyyy"];
                
                NSString *startString = [df stringFromDate:startDt];
                NSString *endString = [df stringFromDate:endDt];
                
                NSString *joined = [NSString stringWithFormat:@"%@", [_detailDictionary valueForKey:@"Joined"]];
                NSString *needed = [NSString stringWithFormat:@"%@",[_detailDictionary objectForKey:@"Need"]];
                NSString *tempBody = [NSString stringWithFormat:kVolunteerEmailContent,del.appReference.brandname, [_detailDictionary objectForKey:@"Name"], startString, endString,needed,joined, [_detailDictionary objectForKey:@"Description"]];
                //NSString *img = nil;
                
                
                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    
                    subject = [NSString stringWithFormat:kVolunteerEmailSubj];
                    mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                else if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    subject = [NSString stringWithFormat:kVolunteerEmailSubj];
                    mailBody = [NSString stringWithFormat:[self mailMediaHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.aboutImage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                else {
                    subject = [NSString stringWithFormat:kVolunteerEmailSubj];
                    mailBody = [NSString stringWithFormat:[self mailHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
        }
            break;
            
        case APP_COUPONS:
        {
            if(_detailDictionary)
            {
                NSLog(@"_detailDictionary %@",_detailDictionary);
                NSString *androidURL = nil;
                {
                    androidURL = del.appReference.androidURL;
                }
                NSString *appStoreURL = nil;
                {
                    appStoreURL = del.appReference.appstoreURL;
                    
                }
                
                NSString *tempBody = [_detailDictionary objectForKey:@"Description"];
                //kAudioEmailSubj
                if([_detailDictionary objectForKey:@"Image"] && ![[_detailDictionary objectForKey:@"Image"] isEqualToString:@""])
                {
                    subject = [NSString stringWithFormat:@"Check out %@ From %@",[_detailDictionary objectForKey:@"CategoryName"],del.appReference.brandname];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"CategoryName"], tempBody, appStoreURL,androidURL];
                }
                else if(del.appReference.aboutImage && ![del.appReference.aboutImage isEqualToString:@""])
                {
                    subject = [_detailDictionary objectForKey:@"Title"];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.aboutImage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                else {
                    subject = [_detailDictionary objectForKey:@"Title"];
                    mailBody = [NSString stringWithFormat:[self mailAudioHTMLContent],[NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.splashimage],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID],[_detailDictionary objectForKey:@"Name"], tempBody, appStoreURL,androidURL];
                }
                
            }
            else {
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
        }
            break;
            
        case APP_VIDEO:
        {
            NSLog(@"THe Products ---> APP_Video %@", _detailDictionary);
            
            
            if(_detailDictionary)
            {
                NSString *postTitle = nil;
                NSString *postThumbnail = nil;
                NSString *postLink = nil;
                NSString *postSubject = nil;
                
                
                
                postSubject = [NSString stringWithFormat:@"Check this out from %@", del.appReference.brandname];
                
                
                if (![[_detailDictionary valueForKey:@"Title"] isEqualToString:@""] &&![[_detailDictionary valueForKey:@"Title"] isKindOfClass:[NSNull class]]){
                    postTitle = [NSString stringWithFormat:@"%@!",[_detailDictionary valueForKey:@"Title"]];
                    
                    
                }else{
                    postTitle = [NSString stringWithFormat:@"%@!", del.appReference.brandname];
                    
                }
                
                if (![[_detailDictionary valueForKey:@"Image"] isKindOfClass:[NSNull class]]&&![[_detailDictionary valueForKey:@"Image"] isEqualToString:@""]){
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL,[_detailDictionary valueForKey:@"Image"]];
                    
                    
                }else{
                    postThumbnail = [NSString stringWithFormat:@"%@%@", kWebServiceURL,del.appReference.aboutImage];
                    
                }
                
                if (![[_detailDictionary valueForKey:@"FilePath"] isEqualToString:@""] &&![[_detailDictionary valueForKey:@"FilePath"] isKindOfClass:[NSNull class]]){
                    postLink = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary valueForKey:@"FilePath"]];
                    
                }else{
                    postLink = [NSString stringWithFormat:@"%@", del.appReference.aboutURL];
                    
                }
                
                
                NSString *tempBody = [NSString stringWithFormat:@"Check this out from %@",del.appReference.brandname];
                subject = [NSString stringWithFormat:@"%@",postSubject];
                mailBody = [NSString stringWithFormat:[self mailWebLinkHTML],postThumbnail,postLink,postTitle,postLink,del.appReference.appstoreURL,del.appReference.androidURL];
            }
            else{
                subject = [self defaultMailSubject];
                mailBody = [self defaultMailBody];
            }
            
        }
            
            break;
            
            
        default:
        {
            subject = [self defaultMailSubject];
            mailBody = [self defaultMailBody];
        }
            break;
    }
    
    [AppDelegate shareddelegate].selectedItemId = _itemId;
    if(_widgetInfo && ![_widgetInfo isKindOfClass:[NSNull class]] && !_appType == APP_WEBLINK)
    {
        [AppDelegate shareddelegate].selectedTileInfo = _widgetInfo;
    }
    
    [[AppDelegate shareddelegate] postMail:vControl subject:subject msgBody:mailBody];
}

#pragma mark Post SMS.....


-(NSString *)smsString
{
    AppDelegate *del = [AppDelegate shareddelegate];
    NSString *msg = [NSString stringWithFormat:kSMSDefaultText,del.appReference.brandname,del.appReference.appstoreURL, del.appReference.androidURL];
    return msg;
}

- (void)postSMSContentsByAppType:(UIViewController *)vControl type:(AppType)type
{
    
    
    AppDelegate *del = [AppDelegate shareddelegate];
    NSString *msg = nil;
    switch (type) {
            
        case APP_NONE:
        {
            msg = [self smsString];
        }
            break;
        case APP_ABOUT:
        {
            msg = [self smsString];
        }
            break;
        case APP_SPONSOR:
        {
            NSString *postTitle = nil;
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"SponserName"])
            {
                postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"SponserName"]];
            }
            else{
                postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
            }
            
            NSString *postLink = nil;
            
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Website"])
            {
                postLink = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"Website"]];
            }
            else{
                postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
            }
            msg = [NSString stringWithFormat:@"I am telling everyone about '%@' because they support '%@', which is an organization I believe in and support too. %@", postTitle,del.appReference.brandname,postLink]; //[self smsString];
        }
            break;
            
        case APP_PRODUCTS:
        {
            NSString *postTitle = nil;
            
            NSString *titleId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBrandTileId"]];
            
            
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Title"])
            {
                postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"ProductName"]];
            }
            else{
                postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
            }
            
            NSString *postLink = nil;
            
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"ProductID"])
            {
                //postLink = [NSString stringWithFormat:kAppProductWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"ProductID"] intValue], [AppDelegate shareddelegate].appReference.brandname];
                
                postLink = [NSString stringWithFormat:kAppProductWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"ProductID"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId];
            }
            else{
                postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
            }
            msg = [NSString stringWithFormat:@"Check this out!%@", /*postTitle,del.appReference.brandname,*/postLink]; //[self smsString];
        }
            break;
        case APP_WEBLINK:
        {
            
            NSString *postTitle = nil;
            if(_widgetInfo && ![_widgetInfo.webLinkPageTitle isKindOfClass:[NSNull class]])
            {
                postTitle = [NSString stringWithFormat:@"%@", _widgetInfo.webLinkPageTitle];
            }
            else{
                postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
            }
            
            NSString *postLink = nil;
            
            if(_widgetInfo && ![_widgetInfo.webLinkPageTitle isKindOfClass:[NSNull class]])
            {
                postLink = [NSString stringWithFormat:@"%@", _widgetInfo.webLinkURL];
            }
            else{
                postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
            }
            msg = [NSString stringWithFormat:@"Check this out! %@", /*postTitle, del.appReference.brandname,*/ postLink];
        }
            break;
            
        case APP_COUPONS:   //SL
        {
            
            NSString *androidURL = nil;
            {
                androidURL = del.appReference.androidURL;
            }
            NSString *appStoreURL = nil;
            {
                appStoreURL = del.appReference.appstoreURL;
                
            }
            if(_detailDictionary)
            {
                
                
                msg = [NSString stringWithFormat:@"Check out '%@' From %@. \niPhone: %@ \nAndroid: %@",[_detailDictionary objectForKey:@"Title"],del.appReference.brandname,appStoreURL,androidURL];
            }
            else {
                msg = [self smsString];
            }
        }
            
            
            
            
            break;
            
        case APP_CONTRIBUTE:
        {
            msg = [self smsString];
            
        }
            break;
        case APP_DONATE:
        {
            msg = [self smsString];
        }
            break;
        case APP_EVENTS:
        {
            
            {
                NSString *titleId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBrandTileId"]];
                NSString *postTitle = nil;
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Title"])
                {
                    postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"CategoryName"]];
                }
                else{
                    postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                }
                
                NSString *postLink = nil;
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"EventId"])
                {
                    //postLink = [NSString stringWithFormat:kAppEventWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"EventId"] intValue],[[_detailDictionary objectForKey:@"CategoryId"] intValue], [AppDelegate shareddelegate].appReference.brandname];
                    postLink = [NSString stringWithFormat:kAppEventWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"EventId"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId];

                }
                else{
                    postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                }
                msg = [NSString stringWithFormat:@"Check this out!%@", /*postTitle,del.appReference.brandname,*/postLink]; //[self smsString];
            }
            
            /*
             if(_detailDictionary)
             {
             NSString *fbId = nil;
             (![[_detailDictionary objectForKey:@"FBPostId"] isEqualToString:@""])?(fbId = [_detailDictionary objectForKey:@"FBPostId"]):(fbId = del.appReference.socFBPageID);
             
             msg = [NSString stringWithFormat:kEventsTextContent,[NSString stringWithFormat:kFBFanPageURL, fbId]];
             }
             else {
             msg = [self smsString];
             }*/
        }
            break;
        case APP_FUNDRAISING:
        {
            if(_detailDictionary)
            {
                msg = [NSString stringWithFormat:kFundraisingText,[_detailDictionary objectForKey:@"Name"],[NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID] ];
            }
            else {
                msg = [self smsString];
            }
        }
            break;
        case APP_LOCATIONS:
        {
            msg = [self smsString];
        }
            break;
        case APP_MEDIA:
        {
            if(_detailDataObject)
            {
                PodcastItem *item = (PodcastItem *)_detailDataObject;
                msg = [NSString stringWithFormat:@"Check out '%@'",item.url];
            }
            else {
                msg = [self smsString];
            }
        }
            break;
        case APP_NEWS:
        {
            if(_detailDictionary)
            {
                NSString *fbId = nil;
                //(![[_detailDictionary objectForKey:@"FBPostId"] isEqualToString:@""])?(fbId = [_detailDictionary objectForKey:@"FBPostId"]):();
                fbId = del.appReference.socFBPageID;
                
                msg = [NSString stringWithFormat:kNewsTextContent,[_detailDictionary objectForKey:@"Title"], [NSString stringWithFormat:kFBFanPageURL,fbId]];
            }
            else {
                msg = [self smsString];
            }
        }
            break;
        case APP_SHARE:
        {
            msg = [self smsString];
        }
            break;
        case APP_SOCIAL:
        {
            msg = [NSString stringWithFormat:@"Check this out from '%@'! %@, %@",del.appReference.brandname,[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID], [NSString stringWithFormat:kTwitterFanPage,del.appReference.socTwitterBrandName]];
            // imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
        }
            break;
        case APP_VOLUNTEER:
        {
            if(_detailDictionary)
            {
                msg = [NSString stringWithFormat:kVolunteerText,[_detailDictionary objectForKey:@"Name"],[NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
            }
            else {
                msg = [self smsString];
            }
        }
            break;
            
        case APP_AUDIO_DETAIL:
        {
            
            NSString *androidURL = nil;
            {
                androidURL = del.appReference.androidURL;
            }
            NSString *appStoreURL = nil;
            {
                appStoreURL = del.appReference.appstoreURL;
                
            }
            if(_detailDictionary)
            {
                
                
                msg = [NSString stringWithFormat:@"Check out '%@' From %@. \niPhone: %@ \nAndroid: %@",[_detailDictionary objectForKey:@"Title"],del.appReference.brandname,appStoreURL,androidURL];
            }
            else {
                msg = [self smsString];
            }
        }
            break;
            
            
        case APP_AUDIO_PURCHASE:
        {
            
            NSString *androidURL = nil;
            {
                androidURL = del.appReference.androidURL;
            }
            NSString *appStoreURL = nil;
            {
                appStoreURL = del.appReference.appstoreURL;
                
            }
            if(_detailDictionary)
            {
                
                
                msg = [NSString stringWithFormat:@"Check out '%@' From %@. \niPhone: %@ \nAndroid: %@",[_detailDictionary objectForKey:@"Title"],del.appReference.brandname,appStoreURL,androidURL];
            }
            else {
                msg = [self smsString];
            }
        }
            break;
            
            
        case APP_VIDEO:
        {
            
            NSString *postTitle = nil;
            
            NSString *postLink = nil;
            
            
            
            //postSubject = [NSString stringWithFormat:@"Check this %@ from %@",[_detailDictionary valueForKey:@"Title"], del.appReference.brandname];
            
            
            if ([_detailDictionary valueForKey:@"Title"] &&![[_detailDictionary valueForKey:@"Title"] isKindOfClass:[NSNull class]]){
                postTitle = [NSString stringWithFormat:@"%@",[_detailDictionary valueForKey:@"Title"]];
                
                
            }else{
                postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                
                
            }
            
            
            
            if (![[_detailDictionary valueForKey:@"FilePath"] isEqualToString:@""] &&![[_detailDictionary valueForKey:@"FilePath"] isKindOfClass:[NSNull class]]){
                postLink = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary valueForKey:@"FilePath"]];
                
            }else{
                postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                
            }
            
            
            msg = [NSString stringWithFormat:@"Check this out! %@", /*postTitle, del.appReference.brandname,*/ postLink];
        }
            break;
            
            
  
            
            
            
        default:
            
        {
            msg = [self smsString];
        }
            break;
    }
    [AppDelegate shareddelegate].selectedItemId = _itemId;
    
    if(_widgetInfo && ![_widgetInfo isKindOfClass:[NSNull class]] && !_appType == APP_WEBLINK)
    {
        [AppDelegate shareddelegate].selectedTileInfo = _widgetInfo;
    }
    
    [[AppDelegate shareddelegate] postSMS:vControl smsContent:msg];
}


#pragma mark Twitter Dynamic Content


#pragma mark Dynamic Content

- (void)postTwitterContentsByAppType:(UIViewController *)vControl type:(AppType)type
{
    
    NSString *msg = nil;
    NSString *imgURLString = nil;
    NSString *link = nil;
    
    NSLog(@"The detail dictionary object : %@", _detailDictionary);
    
    
    AppDelegate *del = [AppDelegate shareddelegate];
    switch (type) {
            
        case APP_NONE:
        {
            msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = del.appReference.androidURL;
        }
            break;
        case APP_SPONSOR:
        {
            NSString *postTitle = nil;
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"SponserName"])
            {
                postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"SponserName"]];
            }
            else{
                postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
            }
            
            NSString *postLink = nil;
            
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Website"])
            {
                postLink = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"Website"]];
            }
            else{
                postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
            }
            msg = [NSString stringWithFormat:@"'%@' is supporting '%@', which is an organization I support too. %@", postTitle, del.appReference.brandname, postLink];
            
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = del.appReference.androidURL;
        }
            break;
            
        case APP_PRODUCTS:
        {
            NSString *postTitle = nil;
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Title"])
            {
                postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"Title"]];
            }
            else{
                postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
            }
            
            NSString *postLink = nil;
            
            if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"ProductID"])
            {
                //postLink = [NSString stringWithFormat:kAppProductWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"ProductID"] intValue], [AppDelegate shareddelegate].appReference.brandname];
                NSString *titleId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBrandTileId"]];
                postLink = [NSString stringWithFormat:kAppProductWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"ProductID"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId];
            }
            else{
                postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
            }
            msg = [NSString stringWithFormat:@"Check this out! %@", postLink];
            NSLog(@"msg %@",msg);
            
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link =@""; //postLink;
        }
            break;
            
        case APP_COUPONSDETAILS: //SL
        {
            if(_detailDictionary)
            {
                
                NSLog(@"_detailDictionary %@",_detailDictionary);
                NSString *androidURL = nil;
                {
                    androidURL = del.appReference.androidURL;
                }
                NSString *appStoreURL = nil;
                {
                    appStoreURL = del.appReference.appstoreURL;
                    
                }
                
                msg = [NSString stringWithFormat:@"Check out %@ From %@.\niPhone:%@ \nAndroid:%@",[_detailDictionary objectForKey:@"CategoryName"],del.appReference.brandname,appStoreURL,androidURL];
                imgURLString = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [_detailDictionary objectForKey:@"Image"]];
                
                link = @"";
                
            }
            else {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
        }
            // SL
            break;
            
            
            
        case APP_WEBLINK:
        {
            NSString *postTitle = nil;
            if(_widgetInfo && ![_widgetInfo.webLinkPageTitle isKindOfClass:[NSNull class]])
            {
                postTitle = [NSString stringWithFormat:@"%@", _widgetInfo.webLinkPageTitle];
            }
            else
            {
                postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
            }
            
            NSString *postLink = nil;
            
            if(_widgetInfo.webLinkURL && ![_widgetInfo.webLinkURL isKindOfClass:[NSNull class]])
            {
                postLink = [NSString stringWithFormat:@"%@", _widgetInfo.webLinkURL];
            }
            else{
                postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
            }
            msg = [NSString stringWithFormat:@"Check this out! %@", /*postTitle, del.appReference.brandname,*/ postLink];
            
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.aboutImage];
            link = postLink;
        }
            break;
            
        case APP_ABOUT:
        {
            msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = del.appReference.androidURL;
        }
            break;
        case APP_CONTRIBUTE:
        {
            msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = del.appReference.androidURL;
        }
            break;
            
            
            
        case APP_DONATE:
        {
            msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = del.appReference.androidURL;
        }
            break;
        case APP_EVENTS:
        {
            if(_detailDictionary)
            {
                NSString *titleId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProductBrandTileId"]];
                
                NSString *postTitle = nil;
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"Title"])
                {
                    postTitle = [NSString stringWithFormat:@"%@", [_detailDictionary objectForKey:@"CategoryName"]];
                }
                else{
                    postTitle = [NSString stringWithFormat:@"%@", del.appReference.brandname];
                }
                
                NSString *postLink = nil;
                
                if(_detailDictionary && [[_detailDictionary allKeys] containsObject:@"EventId"])
                {
                    //postLink = [NSString stringWithFormat:kAppEventWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"EventId"] intValue],[[_detailDictionary objectForKey:@"CategoryId"] intValue], [AppDelegate shareddelegate].appReference.brandname];
                    postLink = [NSString stringWithFormat:kAppEventWebSiteURL, kWebServiceURL, [[_detailDictionary objectForKey:@"EventId"] intValue], [[_detailDictionary objectForKey:@"CategoryId"] intValue],[AppDelegate shareddelegate].appReference.brandnickname,titleId];

                }
                else{
                    postLink = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:kFBFanPageURL,del.appReference.socFBPageID]];
                }
                msg = [NSString stringWithFormat:@"Check this out!%@", /*postTitle,del.appReference.brandname,*/postLink];
                /*
                 msg = [NSString stringWithFormat:kEventsTwitterContent, [NSString stringWithFormat:@"www.facebook.com/events/%@", [_detailDictionary objectForKey:@"FBPostId"]]];
                 imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]];
                 link = del.appReference.appstoreURL;
                 */
            }
            else {
                
                
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
        }
            break;
        case APP_FUNDRAISING:
        {
            if(_detailDictionary)
            {
                msg = [NSString stringWithFormat:kFundraisingTweet, [_detailDictionary objectForKey:@"Name"]];
                link = [NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]];
            }
            else {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;            }
        }
            break;
        case APP_LOCATIONS:
        {
            if(_detailDictionary)
            {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
            else {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
        }
            break;
        case APP_MEDIA:
        {
            if(_detailDataObject)
            {
                PodcastItem *item = (PodcastItem *)_detailDataObject;
                msg = [NSString stringWithFormat:@"Check out '%@'", item.url];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,item.image];
                link = item.url;
            }
            else{
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
            
        }
            break;
        case APP_NEWS:
        {
            if(_detailDictionary)
            {
                NSLog(@"The detail dic: %@", _detailDictionary);
                msg = [NSString stringWithFormat:kNewsTwitterContent, [_detailDictionary objectForKey:@"Title"]];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]];
                link = [NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID];//[_detailDictionary objectForKey:@"FBPostId"]];
            }
            else {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;            }
        }
            break;
        case APP_SHARE:
        {
            msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = del.appReference.androidURL;
        }
            break;
        case APP_SOCIAL:
        {
            msg = [NSString stringWithFormat:kSocialMailSubj, del.appReference.brandname];
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = [NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID];
        }
            break;
        case APP_VOLUNTEER:
        {
            if(_detailDictionary)
            {
                msg = [NSString stringWithFormat:kVolunteerTweet, [_detailDictionary objectForKey:@"Name"]];
                imgURLString = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [_detailDictionary objectForKey:@"Image"]];
                link = [NSString stringWithFormat:kFBFanPageURL, del.appReference.socFBPageID];
            }
            else {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
        }
            break;
        case APP_AUDIO_DETAIL:
        {
            if(_detailDictionary)
            {
                
                NSLog(@"_detailDictionary %@",_detailDictionary);
                NSString *androidURL = nil;
                {
                    androidURL = del.appReference.androidURL;
                }
                NSString *appStoreURL = nil;
                {
                    appStoreURL = del.appReference.appstoreURL;
                    
                }
                
                msg = [NSString stringWithFormat:@"Check out %@ from %@.\niPhone:%@ \nAndroid:%@",[_detailDictionary objectForKey:@"Title"],del.appReference.brandname,appStoreURL,androidURL];
                imgURLString = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [_detailDictionary objectForKey:@"Image"]];
                
                link = @"";
                
            }
            else {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
        }
            break;
            
            
        case APP_AUDIO_PURCHASE:
        {
            if(_detailDictionary)
            {
                
                NSLog(@"_detailDictionary %@",_detailDictionary);
                NSString *androidURL = nil;
                {
                    androidURL = del.appReference.androidURL;
                }
                NSString *appStoreURL = nil;
                {
                    appStoreURL = del.appReference.appstoreURL;
                    
                }
                
                msg = [NSString stringWithFormat:@"Check out %@ from %@.\niPhone:%@ \nAndroid:%@",[_detailDictionary objectForKey:@"Title"],del.appReference.brandname,appStoreURL,androidURL];
                imgURLString = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [_detailDictionary objectForKey:@"Image"]];
                
                link = @"";
                
            }
            else {
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
        }
            break;
            
        case APP_VIDEO:
        {
            
            if(_detailDictionary)
            {
                NSString *postLink = nil;
                
                
                
               
                
                
                
                postLink = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary valueForKey:@"FilePath"]];

                msg = [NSString stringWithFormat:@"Check this out! %@", /*postTitle, del.appReference.brandname,*/ postLink];
                
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[_detailDictionary objectForKey:@"Image"]];
                link = @"";
            }else{
                msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
                imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
                link = del.appReference.androidURL;
            }
            
            
           
        }
            break;
            
        default:
        {
            msg = [NSString stringWithFormat:kApplicationTweetContent, del.appReference.brandname, del.appReference.appstoreURL];
            imgURLString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,del.appReference.logo];
            link = del.appReference.androidURL;
        }
            break;
    }
    
    //
    
    [AppDelegate shareddelegate].selectedItemId = _itemId;
    if(_widgetInfo && ![_widgetInfo isKindOfClass:[NSNull class]] && !_appType == APP_WEBLINK)
    {
        [AppDelegate shareddelegate].selectedTileInfo = _widgetInfo;
    }
    NSLog(@"imgURLString %@",imgURLString);
    [[AppDelegate shareddelegate] postTweet:vControl msgBody:msg imagePath:imgURLString link:link];
}


- (NSString *)mailHTMLContent
{
    NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [arrPaths objectAtIndex:0];
    //  NSString *path = [docsDir stringByAppendingPathComponent:@"exeedings-email.txt"];
    NSString *path = [docsDir stringByAppendingPathComponent:@"NewsSample.txt"];
    
    NSFileManager *mangr = [[[NSFileManager alloc]init] autorelease];
    if(![mangr fileExistsAtPath:path])
    {
        //        NSString *str = [[NSBundle mainBundle] pathForResource:@"exeedings-email" ofType:@"txt"];
        NSString *str = [[NSBundle mainBundle] pathForResource:@"NewsSample" ofType:@"txt"];
        [mangr copyItemAtPath:str toPath:path error:nil];
    }
    NSLog(@"The file path is : %@", path);
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    //NSLog(@"The file content is : %@", string);
    return string;
}


-(NSString *)mailProductsHTMLContent
{
    NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [arrPaths objectAtIndex:0];
    //  NSString *path = [docsDir stringByAppendingPathComponent:@"exeedings-email.txt"];
    NSString *path = [docsDir stringByAppendingPathComponent:@"ProductsMail.txt"];
    
    NSFileManager *mangr = [[[NSFileManager alloc]init] autorelease];
    if(![mangr fileExistsAtPath:path])
    {
        //        NSString *str = [[NSBundle mainBundle] pathForResource:@"exeedings-email" ofType:@"txt"];
        NSString *str = [[NSBundle mainBundle] pathForResource:@"ProductsMail" ofType:@"txt"];
        [mangr copyItemAtPath:str toPath:path error:nil];
    }
    NSLog(@"The file path is : %@", path);
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    //NSLog(@"The file content is : %@", string);
    return string;
}

-(NSString *)mailWebLinkHTML
{
    NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [arrPaths objectAtIndex:0];
    //  NSString *path = [docsDir stringByAppendingPathComponent:@"exeedings-email.txt"];
    NSString *path = [docsDir stringByAppendingPathComponent:@"Weblink.txt"];
    
    NSFileManager *mangr = [[[NSFileManager alloc]init] autorelease];
    if(![mangr fileExistsAtPath:path])
    {
        //        NSString *str = [[NSBundle mainBundle] pathForResource:@"exeedings-email" ofType:@"txt"];
        NSString *str = [[NSBundle mainBundle] pathForResource:@"Weblink" ofType:@"txt"];
        [mangr copyItemAtPath:str toPath:path error:nil];
    }
    NSLog(@"The file path is : %@", path);
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    //NSLog(@"The file content is : %@", string);
    return string;
}

-(NSString *)mailSponsorHTMLContent
{
    NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [arrPaths objectAtIndex:0];
    //  NSString *path = [docsDir stringByAppendingPathComponent:@"exeedings-email.txt"];
    NSString *path = [docsDir stringByAppendingPathComponent:@"SponsorsMail.txt"];
    
    NSFileManager *mangr = [[[NSFileManager alloc]init] autorelease];
    if(![mangr fileExistsAtPath:path])
    {
        //        NSString *str = [[NSBundle mainBundle] pathForResource:@"exeedings-email" ofType:@"txt"];
        NSString *str = [[NSBundle mainBundle] pathForResource:@"SponsorsMail" ofType:@"txt"];
        [mangr copyItemAtPath:str toPath:path error:nil];
    }
    NSLog(@"The file path is : %@", path);
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    //NSLog(@"The file content is : %@", string);
    return string;
}

-(NSString *)mailMediaHTMLContent
{
    NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [arrPaths objectAtIndex:0];
    //  NSString *path = [docsDir stringByAppendingPathComponent:@"exeedings-email.txt"];
    NSString *path = [docsDir stringByAppendingPathComponent:@"MediaMail.txt"];
    
    NSFileManager *mangr = [[[NSFileManager alloc]init] autorelease];
    if(![mangr fileExistsAtPath:path])
    {
        //        NSString *str = [[NSBundle mainBundle] pathForResource:@"exeedings-email" ofType:@"txt"];
        NSString *str = [[NSBundle mainBundle] pathForResource:@"MediaMail" ofType:@"txt"];
        [mangr copyItemAtPath:str toPath:path error:nil];
    }
    NSLog(@"The file path is : %@", path);
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"The file content is : %@", string);
    return string;
}

-(NSString *)mailEventHTMLContent
{
    NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [arrPaths objectAtIndex:0];
    //  NSString *path = [docsDir stringByAppendingPathComponent:@"exeedings-email.txt"];
    NSString *path = [docsDir stringByAppendingPathComponent:@"Event.txt"];
    
    NSFileManager *mangr = [[[NSFileManager alloc]init] autorelease];
    if(![mangr fileExistsAtPath:path])
    {
        //        NSString *str = [[NSBundle mainBundle] pathForResource:@"exeedings-email" ofType:@"txt"];
        NSString *str = [[NSBundle mainBundle] pathForResource:@"Event" ofType:@"txt"];
        [mangr copyItemAtPath:str toPath:path error:nil];
    }
    NSLog(@"The file path is : %@", path);
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"The file content is : %@", string);
    return string;
}




-(NSString *)mailAudioHTMLContent
{
    NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [arrPaths objectAtIndex:0];
    //  NSString *path = [docsDir stringByAppendingPathComponent:@"exeedings-email.txt"];
    NSString *path = [docsDir stringByAppendingPathComponent:@"Audio.txt"];
    
    NSFileManager *mangr = [[[NSFileManager alloc]init] autorelease];
    if(![mangr fileExistsAtPath:path])
    {
        //        NSString *str = [[NSBundle mainBundle] pathForResource:@"exeedings-email" ofType:@"txt"];
        NSString *str = [[NSBundle mainBundle] pathForResource:@"Audio" ofType:@"txt"];
        [mangr copyItemAtPath:str toPath:path error:nil];
    }
    NSLog(@"The file path is : %@", path);
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"The file content is : %@", string);
    return string;
}


#pragma mark FB Delegates


- (void)showAlert:(NSString *)message
           result:(id)result
            error:(NSError *)error {
    
    NSString *alertMsg;
    NSString *alertTitle;
    if (error) {
        alertMsg = error.localizedDescription;
        alertTitle = @"Error";
    } else {
        NSDictionary *resultDict = (NSDictionary *)result;
        alertMsg = [NSString stringWithFormat:@"Successfully posted '%@'.\nPost ID: %@",
                    message, [resultDict valueForKey:@"id"]];
        alertTitle = @"Success";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle
                                                        message:alertMsg
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}


-(void)sendPostToFacebook:(id)sender
{
#if 0
    if([FBSession activeSession].isOpen)
    {
        DBApp *app = [AppDelegate shareddelegate].appReference;
        
        if(app)
        {
            SBJSON *jsonWriter = [[SBJSON new] autorelease];
            
#warning FB Share Content App links
            
            NSDictionary *propertyvalue = [NSDictionary dictionaryWithObjectsAndKeys:@"On iTunes AppStore", @"text", @"http://itunes.apple.com/us/app/edmtonight/id511225702?mt=8", @"href", nil];
            NSDictionary *propertyvalue1 = [NSDictionary dictionaryWithObjectsAndKeys:@"On Android Store", @"text", @"http://itunes.apple.com/in/app/delhi-daredevils/id512135778?mt=8", @"href", nil];
            
            
            NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:propertyvalue, @"Download it free",propertyvalue1, @"Download it", nil];
            
            NSDictionary *actions = [NSDictionary dictionaryWithObjectsAndKeys:@"Download it free", @"name",@"http://itunes.apple.com/us/app/myApp/id12345?mt=8", @"link", nil];
            
            NSString *finalproperties = [jsonWriter stringWithObject:properties];
            
            NSString *finalactions = [jsonWriter stringWithObject:actions];
            
            NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           @"Exceedings", @"to",
                                           app.markettingURL, @"link",
                                           app.splashimage, @"picture",
                                           app.appName, @"name",
                                           app.brandname, @"caption",
                                           app.descrip, @"description",
                                           finalproperties, @"properties",
                                           finalactions, @"actions",
                                           nil];
            NSLog(@"Facebook Parameters: %@", params);
            
            FBRequest *postReq = [FBRequest requestWithGraphPath:@"me/feed" parameters:params HTTPMethod:@"POST"];
            
            [postReq startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                [self showAlert:@"Post Message" result:result error:error];
                //[self statusUpdate:nil];
            }];
        }
    }
    else {
        [self login:sender];
    }
#endif
}

//// PopUp Animation stuff.....

- (void) attachPopUpAnimation: (id)sender
{
    UIView *view = (UIView *)sender;
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
    CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
    CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale1],
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale3],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    [animation setValues:frameValues];
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.9],
                           [NSNumber numberWithFloat:1.0],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.duration = .2;
    
    [view.layer addAnimation:animation forKey:@"popup"];
}


- (void)dealloc {
    [mainView release];
    self.detailDictionary = nil;
    [_bannerView release];
    [_bannerImg release];
    [_bannerCancelBtn release];
    [super dealloc];
}
- (IBAction)bannerCancelBtn:(id)sender {
    
    [self.bannerView setAlpha:0];
    adsremoved = YES;
    self.bannerCancelBtn.hidden = YES;
    self.bannerCancelBtn.enabled = NO;
    [[NSUserDefaults standardUserDefaults] setBool:adsremoved forKey:@"adsremoved"];
    //use NSUserDefaults so that you can load wether or not they bought it
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)socialBannerWebBtn:(id)sender {
    
    
    for (NSDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"News"]) {
            self.productBannersDict = dict;
            break;

        }
        
    }
    
    self.bannerURLS = [NSMutableArray arrayWithCapacity:0];
    
    if (self.productBannersDict) {
        
        for (int i = 0; i < [[self.productBannersDict objectForKey:@"BannersList"] count]; i++) {
            
            [self.bannerURLS addObject:[[[self.productBannersDict objectForKey:@"BannersList"] objectAtIndex:i] objectForKey:@"LinkIphone"]];
            
            
            NSString *urlStr = [self.bannerURLS objectAtIndex:i];
            NSLog(@"urlStr %@",urlStr);
            NSURL *url = [NSURL URLWithString:urlStr];
            
            
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
        
    }
    
    [self startShowingBanners:[self.bannerURLS objectAtIndex:0]];
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"NetWork" message:@"Website are not available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    
    
}
@end
