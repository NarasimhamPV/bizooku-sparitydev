//
//  AppMainVC.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppMainDetailVC.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Utils.h"
#import "TableObject.h"
#import "SDZEXAnalyticsServiceExample.h"
#import "SPSingletonClass.h"

@interface AppMainDetailVC ()

@end

@implementation AppMainDetailVC

@synthesize headerObject = _headerObject;
//@synthesize dataSourceDic, dataSourceArray;
@synthesize appType;

@synthesize subTabOptions;
@synthesize selectedTab = _selectedTab;
@synthesize subTabBtnArray = _subTabBtnArray;
@synthesize detailDataDictionary = _detailDataDictionary;
@synthesize socialASheet = _socialASheet;
@synthesize detailDataObject = _detailDataObject;
@synthesize itemId = _itemId;

@synthesize tileInfo = _tileInfo;


- (void)reloadUsingWebService{
    
}

#pragma mark - App Header Delegate
- (IBAction)headerSecondBtnAction:(UIButton *)sender
{
    if (sender == self.headerObject.btnShare) {
        self.socialASheet = [[[SocialActionSheet alloc] initWithDelegate:self] autorelease];
        _socialASheet.appType = appType;
        _socialASheet.detailDictionary = _detailDataDictionary;
        _socialASheet.detailDataObject = _detailDataObject;
        
        NSLog(@"The tile information: %@", _tileInfo);
        
        if(_tileInfo && ![_tileInfo isKindOfClass:[NSNull class]])
        {
            _socialASheet.widgetInfo = _tileInfo;
        }
        else if([AppDelegate shareddelegate].selectedTileInfo && ![[AppDelegate shareddelegate].selectedTileInfo isKindOfClass:[NSNull class]] ){
            _socialASheet.widgetInfo = [AppDelegate shareddelegate].selectedTileInfo;
            
        }
        NSLog(@"The tile information: %@", [AppDelegate shareddelegate].selectedTileInfo);
        _socialASheet.itemId = _itemId;
        
        NSLog(@"The tile info : %@", _socialASheet.widgetInfo);
        [_socialASheet presentSocialActionSheet:YES];
    }
}

-(IBAction)headerBtnAction:(UIButton*)sender
{
    
    if (sender == self.headerObject.btnShare) {
        
        
        if(appType == APP_WEBLINK)
        {
            if(self.socialASheet == nil)
            {
                self.socialASheet = [[[SocialActionSheet alloc] initWithDelegate:self] autorelease];
                _socialASheet.appType = appType;
                _socialASheet.detailDictionary = _detailDataDictionary;
                _socialASheet.detailDataObject = _detailDataObject;
                
                NSLog(@"The tile information: %@", _tileInfo);
                
                if(_tileInfo && ![_tileInfo isKindOfClass:[NSNull class]])
                {
                    _socialASheet.widgetInfo = _tileInfo;
                }
                else if([AppDelegate shareddelegate].selectedTileInfo && ![[AppDelegate shareddelegate].selectedTileInfo isKindOfClass:[NSNull class]] ){
                    _socialASheet.widgetInfo = [AppDelegate shareddelegate].selectedTileInfo;
                    
                }
                NSLog(@"The tile information: %@", [AppDelegate shareddelegate].selectedTileInfo);
                _socialASheet.itemId = _itemId;
            }
            
            NSLog(@"The tile info : %@", _socialASheet.widgetInfo);
            [_socialASheet presentSocialActionSheet:YES];
        }
        else{
            self.socialASheet = [[[SocialActionSheet alloc] initWithDelegate:self] autorelease];
            _socialASheet.appType = appType;
            _socialASheet.detailDictionary = _detailDataDictionary;
            _socialASheet.detailDataObject = _detailDataObject;
            
            NSLog(@"The tile information: %@", _tileInfo);
            
            if(_tileInfo && ![_tileInfo isKindOfClass:[NSNull class]])
            {
                _socialASheet.widgetInfo = _tileInfo;
            }
            else if([AppDelegate shareddelegate].selectedTileInfo && ![[AppDelegate shareddelegate].selectedTileInfo isKindOfClass:[NSNull class]] ){
                _socialASheet.widgetInfo = [AppDelegate shareddelegate].selectedTileInfo;
                
            }
            NSLog(@"The tile information: %@", [AppDelegate shareddelegate].selectedTileInfo);
            _socialASheet.itemId = _itemId;
            
            NSLog(@"The tile info : %@", _socialASheet.widgetInfo);
            [_socialASheet presentSocialActionSheet:YES];
        }
        
    }

}



-(void)viewDidAppear:(BOOL)animated{
    
    if (_detailDataDictionary && [_detailDataDictionary isKindOfClass:[NSDictionary class]]) {
       // [GoogleAnalyticsManager logWidgetDetailVisit:self.appType title:[_detailDataDictionary objectForKey:@"Title"]];
    }
    else if (_detailDataDictionary && [_detailDataDictionary isKindOfClass:[PodcastItem class]]) {
      //  PodcastItem *pdItem = (PodcastItem*)_detailDataDictionary;
      //  [GoogleAnalyticsManager logWidgetDetailVisit:self.appType title:pdItem.titleText];
    }
    else if (_detailDataDictionary && [_detailDataDictionary isKindOfClass:[TableObject class]]) {
        // TableObject *pdItem = (TableObject*)_detailDataDictionary;
     //   [GoogleAnalyticsManager logWidgetDetailVisit:self.appType title:pdItem.titleText];
    }
    else if (_detailDataDictionary) {
      //  [GoogleAnalyticsManager logWidgetDetailVisit:self.appType title:@"Unknown"];
    }
    else {
     //   [GoogleAnalyticsManager logWidgetDetailVisit:self.appType title:@"UnknownDetails"];
    }
    
}

-(void)socialActionSheetDidDismiss:(SocialActionSheet*)actionSheet{
    [self.socialASheet removeFromSuperview];
    
    if(!appType == APP_WEBLINK)
    {
        self.socialASheet = nil;
    }
}

#pragma mark - Custom

-(void)setDetailDataDictionary:(NSDictionary *)detailDataDictionary{
    if (self.detailDataDictionary) {
        [_detailDataDictionary release];
    }
    
    _detailDataDictionary = [detailDataDictionary retain];
    
    /*
    if (self.dataSourceDic && [self.dataSourceDic isKindOfClass:[NSDictionary class]]) {
        self.dataSourceArray = [self.dataSourceDic allValues];
    }
    else if (self.dataSourceDic && [self.dataSourceDic isKindOfClass:[NSArray class]]) {
        self.dataSourceArray = (NSArray*)self.dataSourceDic;
    }
     */
}


-(void)setTileInfo:(DBTilesInfo *)tileInfo{
    if (self.tileInfo) {
        [_tileInfo release];
    }
    
    _tileInfo = [tileInfo retain];
    
    /*
     if (self.dataSourceDic && [self.dataSourceDic isKindOfClass:[NSDictionary class]]) {
     self.dataSourceArray = [self.dataSourceDic allValues];
     }
     else if (self.dataSourceDic && [self.dataSourceDic isKindOfClass:[NSArray class]]) {
     self.dataSourceArray = (NSArray*)self.dataSourceDic;
     }
     */
}




#pragma mark - View Life
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.navigationController.navigationBarHidden = YES;
    
    //Load Header View
    self.headerObject = [[AppHeaderBar alloc] initWithDelegate:self appType:appType];
    [self.view addSubview:_headerObject.mainHeader];
    
    NSInteger adjustFac = 2;//Call it 2 words
    
    NSInteger charCount = 0, charUsed = 0;
    
    //Set subTabs
     if (!subTabOptions) {
         switch (appType) {
             case APP_MEDIA:
             {
                 self.subTabOptions = nil;
             }
                 break;
             case APP_SOCIAL:
             {
                 self.subTabOptions = nil;
             }
                 break;
             case APP_ABOUT:
             {
                 self.subTabOptions = nil;
             }
                 break;  
             default:
                 break;
         }
     }
   
    if(subTabOptions){
        for (NSString *str in subTabOptions) {
            charCount += ([str length] + adjustFac);
        }
        
        self.subTabBtnArray = [NSMutableArray array];
        
        for (NSInteger i=0; i< [subTabOptions count]; i++ ) {
            NSString *optStr = [subTabOptions objectAtIndex:i];
            NSInteger optCharCount = ([optStr length] + adjustFac);
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.tag = i;
            
            [btn addTarget:self action:@selector(subTabOptionClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [btn setBackgroundImage:[UIImage imageNamed:@"subTabBG_normal"] forState:UIControlStateNormal];
            if (appType == APP_SOCIAL) {
                [btn setBackgroundImage:[UIImage imageNamed:@"subTabBG_sel"] forState:UIControlStateSelected];
            }
            else{
                [btn setBackgroundImage:[UIImage imageNamed:@"subTabBG2_sel"] forState:UIControlStateSelected];
            }
            
            
            [btn setFrame:CGRectMake((charUsed*1.0/charCount) *_headerObject.subTabsView.frame.size.width, 0, (optCharCount*1.0/charCount) *_headerObject.subTabsView.frame.size.width, kSubBarHeight)];
            
            {
                //Button Label custom
                UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, btn.frame.size.width, btn.frame.size.height)] autorelease];
                [label setFont:[UIFont fontWithName:kDefaultFontName size:18]];
                label.textAlignment = NSTextAlignmentCenter;
                label.textColor = [UIColor whiteColor];
                label.text = optStr;
                label.backgroundColor = [UIColor clearColor];
                [btn addSubview:label];
                
            }
            [_headerObject.subTabsView addSubview:btn];
            
            charUsed += optCharCount;
            
            
            [_subTabBtnArray addObject:btn];
            
        }
        
        [self.view addSubview:_headerObject.subTabsView];
        
    }
}

-(IBAction)subTabOptionClicked:(UIButton*)sender{
    
    //Deselect old Tab
    [_selectedTab setSelected:NO];
    
    //Select New Tab
    self.selectedTab = sender;
    [_selectedTab setSelected:YES];

}


- (void)viewDidUnload
{   
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)leftBtnAction:(id)sender {
    
    NSLog(@"Navigation Views MainDeatails %@",[self.navigationController viewControllers]);

    int widgetId;
      NSString *analyticsKey =@"Detail";
    
    if(!_tileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_tileInfo.widget.widgetID intValue];
    }
    
    
    
    if(appType == APP_PRODUCTS)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    NSLog(@"appType %d",appType);
    if (appType == APP_AUDIO_DETAIL) {
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AudioDetailScreenExitNotification" object:nil];

        
        if (![[[SPSingletonClass sharedInstance] backGroundAudioScreen] isEqualToString:@"YES"]){
            if(_boolNoListing)
            {
                [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
                
            }
            else{
                NSLog(@"Navigatincntr %@",[self.navigationController viewControllers]);

                if ([[[SPSingletonClass sharedInstance] isMainViewCntr] isEqualToString:@"YES"]){

                    NSLog(@"List");
                    [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
                   // [self.navigationController popViewControllerAnimated:YES];

                }else{
                    NSLog(@"Purchage");
                    [self.navigationController popToViewController:self.navigationController.viewControllers[0] animated:YES];

                }
                
                
                
            }

        }
            
        
    }

    
    if(appType == APP_AUDIO_PURCHASE)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AudioPurchaseScreenExitNotification" object:nil];

        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
        }
        else{
            
                [self.navigationController popViewControllerAnimated:YES];
                
            
        }
        
    }
    if(appType == APP_VIDEO)
    {
        
        NSLog(@"Navigatincntr %@",[self.navigationController viewControllers]);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"exitWindowVideoDetailPlayerStop" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"exitWindowFevPlayerStop" object:nil];
        
        
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        
    }
    if(appType == APP_BIGBANNERS)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    if(appType == APP_COUPONS)
    {
        analyticsKey = @"Listing";
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
        }
        else{
            NSLog(@"navigation root %@",self.navigationController);
            [self.navigationController popViewControllerAnimated:YES];
            //[[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        
    }
    
    if(appType == APP_COUPONSDETAILS)
    {
        analyticsKey = @"Detail";
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    NSLog(@"appType1 %d",appType);
    
    if(appType == APP_COUPONSLIST)
    {
        analyticsKey = @"Listing";
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    if(appType == APP_FUNDRAISING)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    if(appType == APP_VOLUNTEER)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    if(appType == APP_EVENTS)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    if(appType == APP_NEWS)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    if(appType == APP_MEDIA)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    
    if(appType == APP_LOCATIONS)
    {
        if(_boolNoListing)
        {
            [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }

    
    if(appType == APP_WEBLINK)
    {
        [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
    }
    else {
        //[self.navigationController popViewControllerAnimated:YES];
    }
    //APP_NONE
    
    if (appType == APP_NONE){
        [self.navigationController popViewControllerAnimated:YES];

    }
    
    
    
    
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId integerValue] widgetType:analyticsKey deviceType:@"iOS"];
    
}




-(AppType)appTypeForView:(id)sender{
    return appType;
}

- (void)dealloc {
    [super dealloc];
}
@end
