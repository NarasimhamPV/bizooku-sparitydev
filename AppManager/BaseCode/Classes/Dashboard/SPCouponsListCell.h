//
//  SPCouponsListCell.h
//  Exceeding
//
//  Created by VENKATALAKSHMI on 24/04/14.
//
//

#import <UIKit/UIKit.h>

@interface SPCouponsListCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *couponListImage;
@property (retain, nonatomic) IBOutlet UIImageView *redeemImageView;

@end
