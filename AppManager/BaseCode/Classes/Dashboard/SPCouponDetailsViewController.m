//
//  SPCouponDetailsViewController.m
//  Exceeding
//
//  Created by SREELAKSHMI on 21/02/14.
//
//

#import "SPCouponDetailsViewController.h"
#import "DBApp.h"
#import "AppDelegate.h"
#import "SDZEXWidgetServiceExample.h"
#import "NSData+Base64.h"
#import "SPTermsAndConditionsVC.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"

@interface SPCouponDetailsViewController ()  <UIActionSheetDelegate, UIScrollViewDelegate>
{
    BOOL isRedeemed;
    
}
@property (retain, nonatomic) IBOutlet UIView *subview;
@property (retain, nonatomic) IBOutlet UIImageView *qrCodeImageView;
@property (nonatomic ,strong) IBOutlet UIImageView *logoImageView;
@property (nonatomic,strong) IBOutlet UILabel *disLabel;
@property (nonatomic,strong) IBOutlet UILabel *enddateLabel;
@property (retain, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (assign) CGPoint rememberContentOffset;
@property (nonatomic,strong) IBOutlet UILabel *couponCode;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;



//Banner
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


- (IBAction)redeemButton:(id)sender;
- (IBAction)termsandConditionBtnClick:(id)sender;

@end

@implementation SPCouponDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //        self.couponDeatailDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view from its nib.
    isRedeemed = NO;
    
    [self.view bringSubviewToFront:self.infoBtn];
    self.headerObject.iconSeperator.hidden = YES;
    
    
    
    self.couponsDetailsArray = [[NSArray alloc]init];
    if ([[SPSingletonClass sharedInstance] isViewDetailLoaded])
    {
        [[NSUserDefaults standardUserDefaults] setObject:[self.couponsDetailsDict objectForKey:@"ItemId"] forKey:@"CouponID"];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[self.couponsDetailsDict objectForKey:@"CouponID"] forKey:@"CouponID"];

    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self couponsDetailsService];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCouponsDetailsNotification:)name:@"getCouponsDetailsNotification"object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedRedeemNotification:)name:@"getRedeemNotification"object:nil];
    CGFloat spacing = 10;
    
    
    self.myScrollView.contentSize = CGSizeMake(self.myScrollView.frame.size.width, self.descriptionLabel.frame.origin.y + self.descriptionLabel.frame.size.height + spacing );
    
    [self entryActionAnalytics];

    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    self.headerObject.headerLabel.text = [self.couponsDetailsDict valueForKey:@"CategoryName"];
    
    if ([[self.couponsDetailsDict valueForKey:@"Redeemptions"]boolValue]){
        self.redeemImageView.image = [UIImage imageNamed:@"redeem.png"];
    }
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


- (void)couponsDetailsService
{
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runcouponsDetails];
}


- (void)receiveCouponsDetailsNotification:(NSNotification *)notification
{
    
    self.couponsDetailsArray = [[AppDelegate shareddelegate].couponsDetailDict mutableCopy];
    self.detailDataDictionary = [[AppDelegate shareddelegate] couponsDetailDict];
    [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:NO];

    // Facebook shared data
    TableObject *obj = [[[TableObject alloc] init] autorelease];
    
    NSString *imageStr = [[[AppDelegate shareddelegate] couponsDetailDict] valueForKey:@"Image"];//
    
    NSString *descriptionStr = [[[AppDelegate shareddelegate] couponsDetailDict] valueForKey:@"Description"];
    NSString *titleStr = [[[AppDelegate shareddelegate] couponsDetailDict] valueForKey:@"CategoryName"];
    
    obj.image = [NSString stringWithFormat:@"%@%@",kWebServiceURL,imageStr];
    obj.titleText = titleStr;
    obj.desc =  descriptionStr;
    [super setDetailDataObject:obj];
    
    [self dataLoading];
}


-(void)updateGraphics
{
    
    self.logoImageView.image = [UIImage imageWithContentsOfFile:[self imageLogoPath]];
}

-(NSString*)imageLogoPath{
    DBApp* _appReference = [AppDelegate shareddelegate].appReference;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logoPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"logo_%@", [_appReference.logoContainerImg stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    return logoPath;
}

- (void)dataLoading
{
    
    NSString *string=[[AppDelegate shareddelegate].couponsDetailDict valueForKey:@"EndDate"];
    NSArray *subStrings = [string componentsSeparatedByString:@"T"]; //or rather @" - "
    
    NSString *dateString = [subStrings objectAtIndex:0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString= [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yy"];
    NSString *dateAsString = [formatter stringFromDate:dateFromString];
    
    [self.couponDetailTitleLabel setText:[NSString stringWithFormat:@"Valid Until: %@",dateAsString]];
    [self.couponDetailTitleLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
    
    self.descriptionLabel.textColor = [UIColor darkGrayColor];
    [self.descriptionLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];
    
    self.descriptionLabel.text = [[AppDelegate shareddelegate].couponsDetailDict valueForKey:@"Description"];
    [self.descriptionLabel resizeToFit];
    
    [self.couponCode setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:12]];
    
    
    self.couponCode.text = [[AppDelegate shareddelegate].couponsDetailDict valueForKey:@"CouponCode"];
    [self.couponDetailText sizeToFit];
    [self.couponDetailText layoutIfNeeded];
    
    
    
    //[NSString stringWithFormat:@"%@/%@", kWebServiceURL,imageData]
    NSString *imageStr = [self.couponsDetailsDict valueForKey:@"Image"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,imageStr];
    
    NSLog(@"urlString %@",urlString);
    
    // spawn a new thread to load the image in the background, from the network
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
        NSURLResponse* response = nil;
        NSError* error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            // update the image and sent notification on the main thread
            if (imageStr.length >0) {
                self.couponDetailImage.image =[UIImage imageWithData:data];
                
            }else{
                self.couponDetailImage.image = [UIImage imageNamed:@"no-image.png"];
                
            }
        });
    });
    
    self.barcodeClickReedemLbl.text = @"Click To Redeem";
    [self.barcodeClickReedemLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
    [self.clickReedemLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
    
    if ([[self.couponsDetailsDict valueForKey:@"Redeemptions"]boolValue]) {
        self.redeemImageView.image = [UIImage imageNamed:@"redeem.png"];
        
    }
    
    if ([[[AppDelegate shareddelegate].couponsDetailDict valueForKey:@"BarCode"] length]) {
        
        self.clickReedemLbl.hidden = YES;
        self.clickReedemLbl.textColor = [UIColor clearColor];
        self.clickReedemLbl.text = @"Click To Redeem";
        [self.barcodeLineImg setBackgroundColor:[UIColor grayColor]];
        
        [self.qrlineImg setBackgroundColor:[UIColor clearColor]];
        self.qrlineImg.hidden = YES;
        
        self.redeemBtn.frame = CGRectMake(82.0f, 299.0f, 175.0f, 78.0f);
        self.descriptionLabel.frame = CGRectMake(29, CGRectGetMinY(self.barcodeLineImg.frame)+6, 304, self.descriptionLabel.frame.size.height);
        NSData *imageData = [NSData dataFromBase64String:[[AppDelegate shareddelegate].
                                                          couponsDetailDict valueForKey:@"BarCode"]];
        UIImage *image1 = [UIImage imageWithData:imageData];
        self.barcodeImg.image=image1;
        
        
        
    }else{
        
        [self.qrlineImg setBackgroundColor:[UIColor grayColor]];
        self.clickReedemLbl.text = @"Click To Redeem";
        self.barcodeClickReedemLbl.hidden = YES;
        self.barcodeClickReedemLbl.textColor = [UIColor clearColor];
        [self.barcodeLineImg setBackgroundColor:[UIColor clearColor]];
        self.barcodeLineImg.hidden = YES;
        self.couponCode.hidden = YES;
        self.redeemBtn.frame = CGRectMake(64.0f, 282.0f, 205.0f, 167.0f);
        
        
        
        NSData *imageData = [NSData dataFromBase64String:[[AppDelegate shareddelegate].couponsDetailDict valueForKey:@"QrCode"]];
        UIImage *image1 = [UIImage imageWithData:imageData];
        self.qrCodeImageView.image=image1;
    }
    
    [self.myScrollView setContentSize:CGSizeMake(320, self.descriptionLabel.frame.origin.y + self.descriptionLabel.frame.size.height + 10)];

}
-(void)qrCodeImage
{
    
    NSData *imageData = [NSData dataFromBase64String:[[AppDelegate shareddelegate].couponsDetailDict valueForKey:@"QrCode"]];
    UIImage *image1 = [UIImage imageWithData:imageData];
    self.qrCodeImageView.image=image1;
}


- (void)receivedRedeemNotification:(NSNotification *)notification
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Redeem" message:[AppDelegate shareddelegate].redeemResponce delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)redeemButton:(id)sender
{
    {
        if ([[self.couponsDetailsDict valueForKey:@"Redeemptions"]boolValue]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Redeem" message:@"Already Redeemed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            
        }else{
            if (isRedeemed == YES) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Redeem" message:@"Already Redeemed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                return;
            }
            UIActionSheet *redeemaction = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Redeem", nil];
            
            redeemaction.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
            redeemaction.tag = 1000;
            [redeemaction showInView:self.view];
            
        }
    }
}
#pragma mark----
#pragma mark TermsAndConditions
- (IBAction)termsandConditionBtnClick:(id)sender
{
    SPTermsAndConditionsVC *termsandConditionsVC = [[SPTermsAndConditionsVC alloc] initWithNibName:@"SPTermsAndConditionsVC" bundle:nil];
    NSString *termsStr = [[AppDelegate shareddelegate].couponsDetailDict objectForKey:@"TermsAndConditions"];
    termsandConditionsVC.termsandConditionsStr =termsStr;
    [self.navigationController pushViewController:termsandConditionsVC animated:YES];
    //TermsAndConditions
}



- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    
    [[actionSheet layer] setBackgroundColor:[UIColor blackColor].CGColor];
    for (UIView *subview in actionSheet.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            button.backgroundColor = [UIColor grayColor];
            button.titleLabel.textColor = [UIColor whiteColor];
        }
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1000) {
        
        if(buttonIndex == 0)
        {
            
            [self redeemActionSheet];
            
        }
        else if(buttonIndex == 1)
        {
            
        }
        
    }
    else if (actionSheet.tag == 2000){
        
        if(buttonIndex == 0)
        {
            //            NSLog(@"%s", __FUNCTION__);
            SDZEXWidgetServiceExample *example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            [example1 getRedeemData];
        }
        else if(buttonIndex == 1)
        {
            
        }
        
    }
}
- (void)redeemActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Please Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Yes, I want to Redeem this Coupon", nil];
    actionSheet.tag = 2000;
    [actionSheet showInView:self.view];
}

- (void)dealloc {
    [_myScrollView release];
    [_couponDetailText release];
    [_bannerImg release];
    [_bannerCancelBtn release];
    [_barcodeImg release];
    [_clickReedemLbl release];
    [_barcodeClickReedemLbl release];
    [_qrlineImg release];
    [_barcodeLineImg release];
    [_subview release];
    [_descriptionLabel release];
    [super dealloc];
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Coupons"]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
        
        
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    [self entryAction];

    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
    NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}

-(void)entryActionAnalytics
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    self.itemId = [self.couponsDetailsDict objectForKey:@"CouponID"];
    
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    else{
        
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.redeemImageView.image = [UIImage imageNamed:@"redeem.png"];
    isRedeemed = YES;
    
}

@end
