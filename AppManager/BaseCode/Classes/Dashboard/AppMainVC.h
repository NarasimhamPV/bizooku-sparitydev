//
//  AppMainVC.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppHeaderBar.h"
#import "AppMainDetailVC.h"
#import "SocialActionSheet.h"

#import "DBTilesInfo.h"
#import "DBWidget.h"


#import "SDZEXAnalyticsServiceExample.h"



@interface AppMainVC : UIViewController <AppHeaderDelegate>
{
    
    
}


@property (nonatomic, retain) IBOutlet AppHeaderBar *headerObject;
@property (nonatomic, retain) NSString *pageTitle;

@property (nonatomic, retain) NSDictionary *dataSourceDic;
@property (nonatomic, retain) NSArray *dataSourceArray;

@property (nonatomic, retain) NSMutableArray *subTabBtnArray;
@property (nonatomic, retain) NSArray *subTabOptions;//Sub tab option must be set before [super viewDidLoad] call is made to AppMainVC from base class otherwise default options will appear

@property (nonatomic, readwrite) AppSubTab tabType;
@property (nonatomic, readwrite) AppType appType;

@property (retain, nonatomic) UIButton *selectedTab;

@property (retain, nonatomic) AppMainDetailVC *appDetailVC;
@property (retain, nonatomic) SocialActionSheet *socialASheet;
@property (retain, nonatomic) NSMutableArray *searchDataSourceArray;

@property (retain, nonatomic) DBTilesInfo *widgetTileInfo;
@property (retain, nonatomic) NSString *widgetConstantType;
@property (nonatomic, retain) NSNumber *itemId;
@property (nonatomic, readwrite) BOOL isLocationSortEnabled;
-(IBAction)subTabOptionClicked:(UIButton*)sender;

@end
