//
//  SPTermsAndConditionsVC.m
//  Exceeding
//
//  Created by Veeru on 20/05/14.
//
//

#import "SPTermsAndConditionsVC.h"

@interface SPTermsAndConditionsVC ()

@property (nonatomic,strong) IBOutlet UITextView *termsAndConTextView;
@property (retain, nonatomic) IBOutlet UIView *bannerView;
- (IBAction)backViewContr:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
- (IBAction)bannerCnacelBtn:(id)sender;
- (IBAction)bannerWebBtn:(id)sender;

@end

@implementation SPTermsAndConditionsVC
@synthesize termsandConditionsStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"termsandConditionsStr %@",termsandConditionsStr);
//    self.termsAndConTextView.layer.borderWidth = 0.5;
    if (termsandConditionsStr.length!=0){
        self.termsAndConTextView.text = termsandConditionsStr;
    }else{
        self.termsAndConTextView.text = @"No data available";
    }
    
   
    for (NSDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Coupons"]) {
            self.termsBannersDict = dict;
            break;

        }
        
        
        
        
    }
    
    self.bannerURLS = [NSMutableArray arrayWithCapacity:0];
    
    if (self.termsBannersDict) {
        
        for (int i = 0; i < [[self.termsBannersDict objectForKey:@"BannersList"] count]; i++) {
            
            [self.bannerURLS addObject:[[[self.termsBannersDict objectForKey:@"BannersList"] objectAtIndex:i] objectForKey:@"BannerImage"]];
            
        }
    }
    
//    [self startShowingBanners:[self.bannerURLS objectAtIndex:0]];
    
}



- (void)startShowingBanners:(NSString *)bannerURL
{
    
    
    //get a dispatch queue
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
        
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", kWebServiceURL, bannerURL]];
        
        NSLog(@"image URL = %@", imageURL);
        NSData *image = [[NSData alloc] initWithContentsOfURL:imageURL];
        
        //this will set the image when loading is finished
        dispatch_async(dispatch_get_main_queue(), ^{
            self.bannerImg.image =[UIImage imageWithData:image];
            [self.view bringSubviewToFront:self.bannerView];

            
            //animation 1
            [UIView animateWithDuration:5 delay:1 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction animations:^{
                self.bannerView.frame = CGRectMake(0.0f, 430, 320.0f,48.0f);
            } completion:^(BOOL finished){
                
                //animation 2
                [UIView animateWithDuration:5 delay:1 options:0 animations:^{
                    self.bannerView.frame = CGRectMake(0.0f, 500, 320.0f,48.0f);
                } completion:nil];
                
            }];
            
            
            
        });
    });
    
    
    [self.bannerURLS removeObject:bannerURL];
    
    if ([self.bannerURLS count]) {
        
        [self performSelector:@selector(startShowingBanners:) withObject:[self.bannerURLS objectAtIndex:0] afterDelay:10];
        [self startShowingBanners:[self.bannerURLS objectAtIndex:0]];
    }
    else{
        return;
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backViewContr:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [_bannerView release];
    [_bannerImg release];
    [_bannerCancelBtn release];
    [super dealloc];
}
- (IBAction)bannerCnacelBtn:(id)sender {
    
    [self.bannerView setAlpha:0];
    adsremoved = YES;
    self.bannerCancelBtn.hidden = YES;
    self.bannerCancelBtn.enabled = NO;
    [[NSUserDefaults standardUserDefaults] setBool:adsremoved forKey:@"adsremoved"];
    //use NSUserDefaults so that you can load wether or not they bought it
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (IBAction)bannerWebBtn:(id)sender {
    
    
    for (NSDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"News"]) {
            self.termsBannersDict = dict;
            break;

        }
        
    }
    
    self.bannerURLS = [NSMutableArray arrayWithCapacity:0];
    
    if (self.termsBannersDict) {
        
        for (int i = 0; i < [[self.termsBannersDict objectForKey:@"BannersList"] count]; i++) {
            
            [self.bannerURLS addObject:[[[self.termsBannersDict objectForKey:@"BannersList"] objectAtIndex:i] objectForKey:@"LinkIphone"]];
            
            
            NSString *urlStr = [self.bannerURLS objectAtIndex:i];
            NSLog(@"urlStr %@",urlStr);
            NSURL *url = [NSURL URLWithString:urlStr];
            
            
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
        
    }
    
    [self startShowingBanners:[self.bannerURLS objectAtIndex:0]];
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"NetWork" message:@"Website are not available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    

}
@end
