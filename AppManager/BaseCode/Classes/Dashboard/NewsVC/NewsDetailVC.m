//
//  NewsDetailVC.m
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "NewsDetailVC.h"
#import "DashboardBuilder.h"
#import "UIImageView+WebCache.h"
#import "SDZEXWidgetServiceExample.h"
#import "UILabel+Exceedings.h"
#import "NSDateFormatter+Exceedings.h"
#import "NSString+Exceedings.h"
#import "AppDelegate.h"
#import "DBApp.h"

#import "CustomBannerView.h"
#import "WebViewController.h"
#import "NSString+Exceedings.h"
#import "SPSingletonClass.h"

@interface NewsDetailVC ()
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation NewsDetailVC

@synthesize newsObject  = _newsObject;
//@synthesize detailDataDictionary = _detailDataDictionary;
- (void)receiveNewsDetailsNotification:(NSNotification *) notification
{
    //self.headerObject.headerLabel.text = [self.newsListArray valueForKey:@"CategoryName"];
    //isViewLoaded = YES;
    [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:NO];

    [self setDetailDataDictionary:[[AppDelegate shareddelegate].volunterDict mutableCopy]];
    
}

- (void)setDetailDataDictionary:(NSDictionary *)dataSourceDic{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNewsDetailsNotification:) name:@"getNewsDetailNotification"
                                               object:nil];
    
    

    [super setDetailDataDictionary:dataSourceDic];
    
    NSLog(@"LoadData: %@", dataSourceDic);
    
    [self viewDidLoad];

    
    if ([[SPSingletonClass sharedInstance]isViewDetailLoaded]) {
        [super setDetailDataObject:(TableObject*)dataSourceDic];
        self.newsObject = (TableObject*)dataSourceDic;
        NSLog(@"self.newsObject %@",self.newsObject);
        
        NSString *newsID = [NSString stringWithFormat:@"%@",[_newsObject valueForKey:@"ItemId"]];
        NSLog(@"newsID %lu",(unsigned long)newsID.length);
        self.itemId = [NSNumber numberWithInt:[newsID intValue]];
        
        if (newsID && newsID.length) {
            SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            [example1 runNewsByID:[newsID integerValue]];
        }
    }
    else{

        self.headerObject.headerLabel.text = [dataSourceDic valueForKey:@"CategoryName"];
        [self hideEverything:NO];
        
        CGFloat spacing = 10;
        CGFloat allWidth = myScrollView.frame.size.width - 2*spacing;
        
        ///Step 1: Set text///
        
        self.itemId = [NSNumber numberWithInt:[[dataSourceDic valueForKey:@"NewsId"] intValue]];
        
        //News Image
        NSString *img = [dataSourceDic valueForKey:@"Image"];
        
        
        
        BOOL haveImage = (img != nil && img.length != 0);
        
        //News Title
        NSString *titleText = [dataSourceDic valueForKey:@"Title"];
        newsTitle.text = titleText;
        [newsTitle resizeToFit];
        
        //News Date & time
        
        NSString *cleanStartDate = [NSString stringWithFormat:@"%@", [dataSourceDic valueForKey:@"StartDate"]];
        
        [dateLabel resizeToFit];
       // NSDate *date = [cleanStartDate exceedingsDateFromServerNewsString];
        dateLabel.text = [dataSourceDic valueForKey:@"Duration"];//[date exceedingsDaysAgoString];
        NSLog(@"dateText: %@", dateLabel.text);
        
        
        //News Details
        NSString *descText = [dataSourceDic valueForKey:@"Description"];
     //   newsDetails.text = descText;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
        {
            NSMutableParagraphStyle *style =  [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
            [style setAlignment: NSTextAlignmentLeft];
            
            NSAttributedString * subText1 = [[NSAttributedString alloc] initWithString:descText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Condensed" size:16.0f],
                                                        NSParagraphStyleAttributeName : style
                                             }];
            newsDetails.attributedText = subText1;
        }
        else{
            newsDetails.text = descText;
        }
        
        //[newsDetails resizeToFit];
        newsDetails = [AppDelegate resizeTheTextView:newsDetails];
        
        ///Step 2: Align All///
        CGRect lastRect = CGRectZero;
        CGFloat lastHighHeight = 0.0;
        
        //Image
        {
            lastRect = CGRectMake(spacing, 0, allWidth, 0);
            
            if (!haveImage) {
                lastRect.size.height = 0.0;
                lastHighHeight = 10.0;
                newsImg.frame = lastRect;
            }
            else {
                [newsImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
                lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, newsImg.frame.size.height);
                lastHighHeight = newsImgHeight + 10;
                
                newsImg.frame = CGRectMake(0, 0, 320, newsImgHeight);
            }
            

        }
        
        //News Title
        {
            //1
            if (titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, newsTitle.frame.size.height);
                lastHighHeight = lastRect.size.height;
            }
            newsTitle.frame = lastRect;
        }
        
        //News Date & Time
        {
            if (cleanStartDate == nil || cleanStartDate.length < 10) {
                lastRect.size.height = 0.0;
            }
            else{
                lastRect = CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, dateLabel.frame.size.height);
                lastHighHeight = lastRect.size.height;
            }
            dateLabel.frame = lastRect;
        }
        
        //Seperator
        {
            if (titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect = CGRectMake(spacing, lastRect.origin.y + lastHighHeight + spacing, allWidth, imgSeperator.frame.size.height);
            }
            imgSeperator.frame = lastRect;
        }
        
        
        //Organization text
        {
            lastRect = imgSeperator.frame;
            lastHighHeight = lastRect.size.height;
            
            if (descText == nil) {
                lastRect.size.height = 0.0;
            }
            else{
                lastRect = CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight + spacing, allWidth, newsDetails.frame.size.height);
            }
            
            newsDetails.frame = lastRect;
        }
        
        
        //Finally set scrolling
        myScrollView.contentSize = CGSizeMake(myScrollView.frame.size.width, newsDetails.frame.origin.y + newsDetails.frame.size.height + spacing );
        
        
        
        //Configure FB Like View
        //FB Like floating right
        {
            NSString *postID = [dataSourceDic valueForKey:@"FBPostId"];
            
            [myFbLike loadWithAppID:[AppDelegate shareddelegate].appReference.socFBAppID posiID:postID];
            
            myFbLike.frame = CGRectMake(myFbLike.frame.origin.x , newsTitle.frame.origin.y + newsTitle.frame.size.height + spacing, myFbLike.frame.size.width , myFbLike.frame.size.height );
            myFbLike.backgroundColor = [UIColor clearColor];
            myFbLike.opaque = NO;
        }
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        newsImgHeight = 240;
    }
    return self;
}

-(void)hideEverything:(BOOL)shouldHide{
    //Hide all
    newsImg.hidden = shouldHide;
    newsTitle.hidden = shouldHide;
    dateLabel.hidden = shouldHide;
    imgSeperator.hidden = shouldHide;
    newsDetails.hidden = shouldHide;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self hideEverything:YES];
    
    [newsTitle sizeLabelForTitle];
    [dateLabel sizeLabelForEventsDate];
    
    newsDetails.textColor = [UIColor darkGrayColor];
    
    [newsDetails setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}
- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}



#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"News"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
//        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}

- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}



- (void)viewDidUnload
{
    [newsImg release];
    newsImg = nil;
    [newsTitle release];
    newsTitle = nil;
    [imgSeperator release];
    imgSeperator = nil;
    [newsDetails release];
    newsDetails = nil;
    [myScrollView release];
    myScrollView = nil;
    [dateLabel release];
    dateLabel = nil;
    [myFbLike release];
    myFbLike = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [newsImg release];
    [newsTitle release];
    [imgSeperator release];
    [newsDetails release];
    [myScrollView release];
    [dateLabel release];
    [myFbLike release];
    [super dealloc];
}
@end
