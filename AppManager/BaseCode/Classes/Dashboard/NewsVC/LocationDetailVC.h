//
//  NewsDetailVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AppMainDetailVC.h"
#import "TableObject.h"
#import "CustomBannerView.h"

#import "LocationDetailTableViewCell.h"

#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 300.0f
#define CELL_CONTENT_MARGIN 10.0f

@interface LocationDetailVC : AppMainDetailVC <UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate>
{
    IBOutlet MKMapView *locationMap;
    
    BOOL findRoute;
    
    BOOL showMap;
    IBOutlet UITableViewCell *locationDetailCell;
    
    IBOutlet UIScrollView *myScrollView;
    IBOutlet UIView *bannerView;
    
    IBOutlet UIButton *bannerCancelBtn;
    IBOutlet UIImageView *bannerImg;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    BOOL adsremoved;
    IBOutlet UILabel *lbTitle;
    IBOutlet UILabel *lbDistance;
    IBOutlet UILabel *lbAddress;
    IBOutlet UILabel *lbCity;
    
    IBOutlet UIButton *btnWebSite;
    IBOutlet UIButton *btnPhone;
    
    IBOutlet UITableView *mapDetailTableView;
    

}
- (IBAction)actPhone:(id)sender;
- (IBAction)actWebsite:(id)sender;
- (IBAction)actDirections:(id)sender;

@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (retain, nonatomic) TableObject *locationObject;

@property (nonatomic, retain) IBOutlet UIView *viCallus;
@property (nonatomic, retain) IBOutlet UIView *viWebsite;
@property (nonatomic, retain) IBOutlet UIView *viDirections;
@property (nonatomic, strong) NSDictionary *locationBannersDict;
//Banner
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;

@property (strong,nonatomic) NSMutableArray *mapTableArray;
@property (strong,nonatomic) NSMutableArray *mapTableTitleArray;

@end
