//
//  ProductsWebView.m
//  Exceeding
//
//  Created by Rajesh Kumar on 12/12/13.
//
//

#import "ProductsWebView.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"



@interface ProductsWebView ()



@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation ProductsWebView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    /*
    for (NSDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Media"]) {
            self.productBannersDict = dict;
        }
        
        
        
    }
    
    self.bannerURLS = [NSMutableArray arrayWithCapacity:0];
    
    if (self.productBannersDict) {
        
        for (int i = 0; i < [[self.productBannersDict objectForKey:@"BannersList"] count]; i++) {
            
            [self.bannerURLS addObject:[[[self.productBannersDict objectForKey:@"BannersList"] objectAtIndex:i] objectForKey:@"BannerImage"]];
            
        }
    }
    
    [self startShowingBanners:[self.bannerURLS objectAtIndex:0]];
*/
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Media"]) {
            self.aboutBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aboutBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aboutBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aboutBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
//        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}

- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryAction
{
    
//    NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
    
}



/*
- (void)startShowingBanners:(NSString *)bannerURL
{
    
    
    //get a dispatch queue
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
        
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", kWebServiceURL, bannerURL]];
        
        NSLog(@"image URL = %@", imageURL);
        NSData *image = [[NSData alloc] initWithContentsOfURL:imageURL];
        
        //this will set the image when loading is finished
        dispatch_async(dispatch_get_main_queue(), ^{
            self.bannerImg.image =[UIImage imageWithData:image];
            
            [self.view bringSubviewToFront:self.bannerView];

            //animation 1
            [UIView animateWithDuration:5 delay:1 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction animations:^{
                self.bannerView.frame = CGRectMake(0.0f, 430, 320.0f,46.0f);
            } completion:^(BOOL finished){
                
                //animation 2
                [UIView animateWithDuration:5 delay:1 options:0 animations:^{
                    self.bannerView.frame = CGRectMake(0.0f, 600, 320.0f,46.0f);
                } completion:nil];
                
            }];
            
            
            
        });
    });
    
    
    [self.bannerURLS removeObject:bannerURL];
    
    if ([self.bannerURLS count]) {
        
        [self performSelector:@selector(startShowingBanners:) withObject:[self.bannerURLS objectAtIndex:0] afterDelay:10];
        [self startShowingBanners:[self.bannerURLS objectAtIndex:0]];
    }
    else{
        return;
    }
    
}
*/


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_bannerView release];
    [_bannerImg release];
    [_bannerCancelBtn release];
    [super dealloc];
}
/*
- (IBAction)bannerCancelBtn:(id)sender {
    
    }
- (IBAction)productWebBannerBtn:(id)sender {
    
    
    for (NSDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"News"]) {
            self.productBannersDict = dict;
        }
        
    }
    
    self.bannerURLS = [NSMutableArray arrayWithCapacity:0];
    
    if (self.productBannersDict) {
        
        for (int i = 0; i < [[self.productBannersDict objectForKey:@"BannersList"] count]; i++) {
            
            [self.bannerURLS addObject:[[[self.productBannersDict objectForKey:@"BannersList"] objectAtIndex:i] objectForKey:@"LinkIphone"]];
            
            
            NSString *urlStr = [self.bannerURLS objectAtIndex:i];
            NSLog(@"urlStr %@",urlStr);
            NSURL *url = [NSURL URLWithString:urlStr];
            
            
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
        
    }
    
    [self startShowingBanners:[self.bannerURLS objectAtIndex:0]];
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"NetWork" message:@"Website are not available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    

    
}
 */
@end
