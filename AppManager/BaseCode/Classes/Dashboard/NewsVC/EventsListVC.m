//
//  EventsListVC.m
//  Exceeding
//
//  Created by Veeru on 27/08/14.
//
//

#import "EventsListVC.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "NewsListCell.h"
#import "PhoneNumberFormatter.h"
#import "NSDateFormatter+Exceedings.h"
#import "NSString+Exceedings.h"
#import "EventDetailVC.h"
#import "SPSingletonClass.h"
#import "CustomBannerView.h"
#import "WebViewController.h"

static NSString *const kNewsListCell = @"NewsListCellIdentfier";
static NSString *const kNewsListCellNib = @"NewsListCell";

@interface EventsListVC ()
@property (retain, nonatomic) IBOutlet UITableView *eventsListTable;
@property (strong, nonatomic) NSArray *eventsListArray;
@property (strong, nonatomic) AppMainVC *appmainVC;
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation EventsListVC
@synthesize dateStr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.eventsListArray = [[NSMutableArray alloc] init];
    [self.eventsListTable registerNib:[UINib nibWithNibName:kNewsListCellNib bundle:nil] forCellReuseIdentifier:kNewsListCell];

    self.headerObject.rightToolbarItem = nil;
    self.headerObject.btnShare.hidden = YES;
    self.headerObject.btnLocation.hidden = NO;
    self.headerObject.headerLabel.text = @"";

    self.eventsListTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveEventsListsNotification:) name:@"getEventsListNotification"object:nil];
    
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    
    if (self.isEventCalendar == YES){
        //getEventsByDate
        [example1 getEventsByDate:self.dateStr];
        
        
    }else{
        [example1 runEventsList];
        
    }
}

- (void)receiveEventsListsNotification:(NSNotification *) notification
{
    self.eventsListArray = [[AppDelegate shareddelegate].eventsListDict mutableCopy];
    [self reloadTable];
    
}
- (void)reloadTable
{
    [self.eventsListTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.eventsListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NewsListCell *newsListCell = [tableView dequeueReusableCellWithIdentifier:kNewsListCell];
    newsListCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.headerObject.headerLabel.text = [[self.eventsListArray objectAtIndex:0]valueForKey:@"CategoryName"];

    newsListCell.titleLabel.text = [[self.eventsListArray objectAtIndex:indexPath.row]objectForKey:@"Title"];
    
    NSString *startDate =[[self.eventsListArray objectAtIndex:indexPath.row]objectForKey:@"StartDate"];
    NSDate *date = [startDate exceedingsDateFromServerNewsString];

    NSString *startTim =[[self.eventsListArray objectAtIndex:indexPath.row]objectForKey:@"StartTime"];
    NSString *endTim =[[self.eventsListArray objectAtIndex:indexPath.row]objectForKey:@"EndTime"];
    NSDateFormatter *format = [NSDateFormatter exceedingEventsPullTimeFormatter];
    
    NSDate *startTime = [format dateFromString:startTim];
    
    
    NSDate *endTime = [format dateFromString:endTim];

    
    
    newsListCell.descriptionLabel.text = [NSString stringWithFormat:@"%@ | %@ to %@", [date exceedingsStringWithSuffix], [format stringFromDate:startTime], [format stringFromDate:endTime]];
    

    
    NSDictionary *tempDict = [self.eventsListArray objectAtIndex:indexPath.row];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[tempDict objectForKey:@"Image"]];
    if ([urlString length]!=0) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
            NSURLResponse* response = nil;
            NSError* error = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                // update the image and sent notification on the main thread
                if ([[tempDict valueForKey:@"Image"] length]) {
                    newsListCell.listImage.image = [UIImage imageWithData:data];

                    
                }else{
                    newsListCell.listImage.image = [UIImage imageNamed:@"eventsdefault"];
                    
                }
                
            });
        });
        
        
    }else{
        newsListCell.listImage.image = [UIImage imageNamed:@"eventsdefault"];

    }
    return newsListCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


-  (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tempDict = [self.eventsListArray objectAtIndex:indexPath.row];
    
    AppMainVC *mainVC = [[AppMainVC alloc]init];
    mainVC.appDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil] ;
    mainVC.appDetailVC.appType = self.appType;
    mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
    mainVC.appDetailVC.detailDataDictionary = tempDict;
    [[SPSingletonClass sharedInstance] setEventDcHaderTitle:[tempDict valueForKey:@"CategoryName"]];
    [self.navigationController pushViewController:mainVC.appDetailVC animated:YES];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    //    NSLog(@"[AppDelegate shareddelegate].bannersDict %@",[AppDelegate shareddelegate].bannersDict);
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Events"]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //    NSLog(@"self.bannerUrlsArray %@",self.aBannerDict);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }

    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
    
}


@end
