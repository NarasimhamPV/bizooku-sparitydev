//
//  ProductsWebView.h
//  Exceeding
//
//  Created by Rajesh Kumar on 12/12/13.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "AppDelegate.h"
#import "BigBanners.h"
@interface ProductsWebView : AppMainDetailVC<BigBannersDelegate>
{
    BOOL adsremoved;
}
@property (retain, nonatomic) IBOutlet UIView *bannerView;
- (IBAction)productWebBannerBtn:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;

@property (nonatomic, strong) NSMutableArray *bannerURLS;
- (void)startShowingBanners:(NSString *)bannerURL;
@property (strong, nonatomic) NSDictionary *productBannersDict;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
