//
//  FundVolDetailVC.m
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "FundVolDetailVC.h"
#import "DashboardBuilder.h"
#import "UIImageView+WebCache.h"
#import "SDZEXWidgetServiceExample.h"
#import "UILabel+Exceedings.h"
#import "InfoCollectorVC.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

#import "CustomBannerView.h"
#import "WebViewController.h"

#import "NSString+Exceedings.h"

#import "TableObject.h"
#import "SPSingletonClass.h"

@interface FundVolDetailVC ()
@property (nonatomic, retain) InfoCollectorVC *infoCollVC;

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation FundVolDetailVC

@synthesize infoCollVC = _infoCollVC;

//@synthesize detailDataDictionary = _detailDataDictionary;

-(void)reloadUsingWebService{
    
    [self.view bringSubviewToFront:actIndicator];
    [actIndicator startAnimating];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveVolunterDetailNotification:)
                                                 name:@"getVolunterDetailNotification"
                                               object:nil];
    if (self.appType == APP_VOLUNTEER) {
        NSNumber *myID = [self.detailDataDictionary valueForKey:@"ItemId"];
        self.itemId = myID;
        if (myID) {
            SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            [example1 runVolunteerInfoByCampaignID:[myID integerValue]];
        }
    }
    else if (self.appType == APP_FUNDRAISING) {
        NSNumber *myID = [self.detailDataDictionary valueForKey:@"ItemId"];
        self.itemId = myID;
        if (myID) {
            SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            [example1 runFundraisingByCampaignID:[myID integerValue]];
        }
    }
}
- (void)receiveVolunterDetailNotification:(NSNotification *) notification
{
    [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:NO];
    [self setDetailDataDictionary:[[AppDelegate shareddelegate].volunterDict mutableCopy]];
    
    
    
}


-(void)setDetailDataDictionary:(NSDictionary *)dataSourceDic
{
    
    [super setDetailDataDictionary:dataSourceDic];
    [actIndicator stopAnimating];
    
    [super viewDidLoad];
   // NSLog(@"LoadData: ID:%@ Img:%@ title:%@ Dic:%@", [self.detailDataDictionary valueForKey:@"NewsId"], [self.detailDataDictionary valueForKey:@"Image"], [self.detailDataDictionary valueForKey:@"Title"], self.detailDataDictionary);
    
    if ([[SPSingletonClass sharedInstance] isViewDetailLoaded]) {
        // Do any additional setup after loading the view from its nib.
        
        [self reloadUsingWebService];
    }
    else{
        
        if(dataSourceDic && [[dataSourceDic allKeys] containsObject:@"CampaignId"])
        {
            self.itemId = [NSNumber numberWithInt:[[dataSourceDic valueForKey:@"CampaignId"] intValue]];
        }
    
        [self hideEverything:NO];
        
        CGFloat spacing = 10;
        CGFloat allWidth = myScrollView.frame.size.width - 2*spacing;
        
        //Set texts first so that we can decide sizes
        NSString *img = [dataSourceDic valueForKey:@"Image"];
        [newsImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
        
        NSString *titleText = [dataSourceDic valueForKey:@"Name"];
        newsTitle.text = titleText;
        [newsTitle resizeToFit];
        
        
        NSString *descText = [dataSourceDic valueForKey:@"Description"];
        
        
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
        {
            NSMutableParagraphStyle *style =  [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
            [style setAlignment: NSTextAlignmentLeft];
            
            NSAttributedString * subText1 = [[NSAttributedString alloc] initWithString:descText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Condensed" size:16.0f],
                                                        NSParagraphStyleAttributeName : style
                                             }];
            newsDetails.attributedText = subText1;
        }
        else{
            newsDetails.text = descText;
        }
        
        
        //Raised & Target
        if (self.appType == APP_VOLUNTEER) {
            NSString *raisedText = [dataSourceDic valueForKey:@"Joined"];
            labelRaised.text = [NSString stringWithFormat:@"%@", raisedText] ;
            
            NSString *requiredText = [dataSourceDic valueForKey:@"Need"];
            labelTarget.text = [NSString stringWithFormat:@"%@", requiredText] ;
            
            myProgressBar.progress = [[dataSourceDic valueForKey:@"Joined"] doubleValue]/[[dataSourceDic valueForKey:@"Need"] doubleValue];
        }
        else{
            //Amount Collected
            NSString *amountCollected = [NSString stringWithFormat:@"%@", [dataSourceDic valueForKey:@"AmountCollected"]];
            labelRaised.text = [NSString stringWithFormat:@"%@", [amountCollected millionsFormatDollarString]];
            
            //Goal
            NSString *goal = [NSString stringWithFormat:@"%@", [dataSourceDic valueForKey:@"Goal"]];
            labelTarget.text = [NSString stringWithFormat:@"%@", [goal millionsFormatDollarString]];
            
            myProgressBar.progress = [[dataSourceDic valueForKey:@"AmountCollected"] doubleValue]/[[dataSourceDic valueForKey:@"Goal"] doubleValue];
        }
        
        
        
        //Fit text for sizes & reset width to fit full width
        
        newsTitle.frame = CGRectMake(spacing, newsTitle.frame.origin.y, allWidth, newsTitle.frame.size.height);
        
        [AppDelegate resizeTheTextView:newsDetails];
        
       // [newsDetails resizeToFit];
        newsDetails.frame = CGRectMake(spacing, newsDetails.frame.origin.y, allWidth, newsDetails.frame.size.height);
        
        //Now align all
        CGRect lastRect = CGRectZero;
        CGFloat lastHighHeight = 0.0;
        
        //Image
        {
            lastRect = CGRectMake(spacing, 0, allWidth, 0);
            
            if (img == nil || [img length] == 0) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect =  CGRectMake(0, 0, 320, fundVolImgHeight);
            }
            newsImg.frame = lastRect;

        }
        
        //Title View
        {
            lastRect = CGRectMake(spacing, 0, allWidth, 0);
            lastHighHeight = 0.0;
            
            //1
            if (titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight +spacing, allWidth, newsTitle.frame.size.height);
                lastHighHeight = lastRect.size.height;
            }
            newsTitle.frame = lastRect;
            
            //2
            lastRect = CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight - 2, allWidth, giveView.frame.size.height);
            lastHighHeight = lastRect.size.height;
            
            giveView.frame = lastRect;
            
            //Now set the view size
            if (img == nil || [img length] == 0) {
                mainTitleView.frame = CGRectMake(0, 0, allWidth + 2*spacing, lastRect.origin.y + lastHighHeight + spacing);
            }
            else{
                mainTitleView.frame = CGRectMake(0, fundVolImgHeight, allWidth + 2*spacing, lastRect.origin.y + lastHighHeight + spacing);
            }
            
        }
        
        NSLog(@"giveView: %@, newsTitle:%@ mainTitleView:%@", giveView, newsTitle, mainTitleView );
        
        //Seperator
        {
            lastRect = mainTitleView.frame;
            lastHighHeight = lastRect.size.height;
            
            if (titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect = CGRectMake(spacing, lastRect.origin.y + lastHighHeight + spacing/2, allWidth, imgSeperator.frame.size.height);
            }
            imgSeperator.frame = lastRect;
        }
        
        
        //Organization text
        {
            lastRect = imgSeperator.frame;
            lastHighHeight = lastRect.size.height;
            
            if (descText == nil) {
                lastRect.size.height = 0.0;
            }
            else{
                lastRect = CGRectMake(5, lastRect.origin.y + lastHighHeight + spacing, allWidth+10, newsDetails.frame.size.height);
            }
            newsDetails.frame = lastRect;
        }
        
        TableObject *obj = [[[TableObject alloc] init] autorelease];
        obj.image = [NSString stringWithFormat:@"%@%@",kWebServiceURL,img];
        obj.titleText = titleText;
        obj.desc = descText;
        
        [super setDetailDataObject:obj];
        
        //Finally set scrolling
        myScrollView.contentSize = CGSizeMake(myScrollView.frame.size.width, newsDetails.frame.origin.y + newsDetails.frame.size.height + spacing );
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        fundVolImgHeight = 240;
    }
    return self;
}

-(void)hideEverything:(BOOL)shouldHide{
    //Hide all
    newsImg.hidden = shouldHide;
    newsTitle.hidden = shouldHide;
    mainTitleView.hidden = shouldHide;
    imgSeperator.hidden = shouldHide;
    newsDetails.hidden = shouldHide;
}

- (void) viewWillAppear:(BOOL)animated
{
    if(!self.tileInfo)
    {
        self.tileInfo = (DBTilesInfo *)[[NSUserDefaults standardUserDefaults] objectForKey:@"TileInfo"];
    }
    [super viewWillAppear:animated];
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;
    
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    [self addBannerData];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isViewLoaded = YES;
    
    //Fonts setuup
    [newsTitle sizeLabelForTitle];
   // [newsDetails sizeLabelForDescription];
    [newsDetails setTextColor:[UIColor darkGrayColor]];
    
    [raisedStatic sizeLabelForFundStatic];
    [targetStatic sizeLabelForFundStatic];
    
    [labelRaised sizeLabelForDescription];
    [labelRaised setTextColor:[UIColor lightGrayColor]];
    
    [labelTarget sizeLabelForDescription];
    [labelTarget setTextColor:[UIColor lightGrayColor]];
    
    [giveJoinBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    if (self.appType == APP_VOLUNTEER) {
        [raisedStatic setText:@"Joined"];
        [targetStatic setText:@"Needed"];
        [giveJoinBtn setTitle:@"Register" forState:UIControlStateNormal];
    }
    else if (self.appType == APP_FUNDRAISING) {
        [raisedStatic setText:@"Raised"];
        [targetStatic setText:@"Goal"];
        [giveJoinBtn setTitle:@"GIVE" forState:UIControlStateNormal];
    }
    [self hideEverything:YES];
    
    
    
    
}




#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Fundraising"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
//        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        //[self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}

- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryAction
{
    
    
//    NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
    
}




- (void)viewDidUnload
{
    [newsImg release];
    newsImg = nil;
    [newsTitle release];
    newsTitle = nil;
    [imgSeperator release];
    imgSeperator = nil;
    [newsDetails release];
    newsDetails = nil;
    [myScrollView release];
    myScrollView = nil;
    [mainTitleView release];
    mainTitleView = nil;
    [giveView release];
    giveView = nil;
    [labelRaised release];
    labelRaised = nil;
    [labelTarget release];
    labelTarget = nil;
    [myProgressBar release];
    myProgressBar = nil;
    [actIndicator release];
    actIndicator = nil;
    [giveJoinBtn release];
    giveJoinBtn = nil;
    [raisedStatic release];
    raisedStatic = nil;
    [targetStatic release];
    targetStatic = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [newsImg release];
    [newsTitle release];
    [imgSeperator release];
    [newsDetails release];
    [myScrollView release];
    [mainTitleView release];
    [giveView release];
    [labelRaised release];
    [labelTarget release];
    [myProgressBar release];
    [actIndicator release];
    [giveJoinBtn release];
    [raisedStatic release];
    [targetStatic release];
    [_bannerCancelBtn release];
    [super dealloc];
}
- (IBAction)giveAction:(id)sender {
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    NSString *str = nil;
    if(self.appType == APP_FUNDRAISING)
    {
        if([AppDelegate shareddelegate].appReference.isDonationsEnabled)
        {
            UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:@"This Will Take You Out Of The App And Forward You To Our Contribution Website!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil] autorelease];
            alert.tag = 122;
            [alert show];
        }
        else{
            [AppDelegate showAlert:@"Donations not available."];
        }
        str = @"Fundraising-Give";
    }
    else if(self.appType == APP_VOLUNTEER){
        [self insertInfoVCAnimated];  //fix : 146
        str = @"Volunteer-Register";
        
    }
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[df objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:str deviceType:@"iOS"];
    }
    else{
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId intValue] widgetType:str deviceType:@"iOS"];
    }
}

#pragma mark Alert View Delegate

#pragma mark Alert View Delegate

-(void)visitFundraisingSite{
    NSString *urlString = nil;
    
    NSArray *arrDonateURL = [[AppDelegate shareddelegate].appReference.donateURL componentsSeparatedByString:@"exceedings"];
    if([AppDelegate shareddelegate].appReference.donateURL && ![[AppDelegate shareddelegate].appReference.donateURL isEqualToString:@""] && [arrDonateURL count] >= 2)
    {
        urlString = [NSString stringWithFormat:kURLDonation,[AppDelegate shareddelegate].appReference.donateURL, [AppDelegate shareddelegate].appReference.brandid,[AppDelegate shareddelegate].appReference.brandname, @"FundCampaign", [self.detailDataDictionary valueForKey:@"CampaignId"]];
    }
    else{
        urlString = [AppDelegate shareddelegate].appReference.donateURL;
    }
    
    NSString *url = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSLog(@"The URL string is: %@", url);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 122) {
        if (buttonIndex == 1) {
            [self visitFundraisingSite];
            SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            if(!self.tileInfo.widget)
            {
                NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
                [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[df objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Fundraising-Give-Ok" deviceType:@"iOS"];
            }
            else{
                [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Fundraising-Give-Ok" deviceType:@"iOS"];
            }
        }
        else if(buttonIndex == 0)
        {
            SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            if(!self.tileInfo.widget)
            {
                NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
                [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[df objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Fundraising-Give" deviceType:@"iOS"];
            }
            else{
                [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Fundraising-Give" deviceType:@"iOS"];
            }
        }
    }
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    [_infoCollVC.view removeFromSuperview];
    self.infoCollVC = nil;
}

#pragma mark - InfoCollector Delegate

-(void)insertInfoVCAnimated{
    self.infoCollVC = [[[InfoCollectorVC alloc] initWithNibName:@"InfoCollectorVC" bundle:nil] autorelease];
    _infoCollVC.delegate = self;
    [self.view addSubview:_infoCollVC.view];
    
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.2],
                             [NSNumber numberWithFloat:1.2],
                             [NSNumber numberWithFloat:.9],
                             [NSNumber numberWithFloat:1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.5],
                               [NSNumber numberWithFloat:0.7],
                               [NSNumber numberWithFloat:1.0], 
                               nil];    
    //popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"transform.scale"];
}

-(void)removeInfoVCAnimated{
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:1.0],
                             [NSNumber numberWithFloat:0.1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:1.0], 
                               nil];    
    popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"transform.scale"];
}

-(void)infoCancel:(id)sender{

    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
        [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[df objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Volunteer-Register" deviceType:@"iOS"];
    }
    else{
        [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Volunteer-Register" deviceType:@"iOS"];
    }
    [self removeInfoVCAnimated];
    
}

-(void)infoProceed:(id)sender firstName:(NSString*)firstName lastName:(NSString*)lastName gender:(NSString*)gender mobile:(NSString*)mobile  email:(NSString*)email{
    [self removeInfoVCAnimated];
    
//    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
//    [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Entry" deviceType:@"iOS"];
    
    
    if (self.appType == APP_VOLUNTEER) {
        NSNumber *myID = [self.detailDataDictionary valueForKey:@"CampaignId"];
        
        if (myID) {
            SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            NSString *brandId = [NSString stringWithFormat:@"%@", [AppDelegate shareddelegate].appReference.brandid];
            
          //Rajesh  [example1 runAddVolunteer:[myID longValue] firstName:firstName lastName:lastName gender:gender mobile:mobile email:email];
            [example1 runAddVolunteer:[myID longValue] firstName:firstName lastName:lastName gender:gender mobile:mobile email:email brandId:brandId];
        }
    }
    else {
        NSNumber *myID = [self.detailDataDictionary valueForKey:@"CampaignId"];
        
        if (myID) {
            SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            [example1 runAddFundraiser:[myID longValue] firstName:firstName lastName:lastName gender:gender mobile:mobile amount:10.0 email:email];
        }
    }
}



@end
