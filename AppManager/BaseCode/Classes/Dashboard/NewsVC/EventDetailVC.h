//
//  EventDetailVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "ExFBLike.h"
#import "TableObject.h"
#import <EventKit/EventKit.h>

#import  <EventKitUI/EventKitUI.h>
#import "BigBanners.h"
@interface EventDetailVC : AppMainDetailVC <EKEventEditViewDelegate, UIWebViewDelegate, UIActionSheetDelegate,BigBannersDelegate>
{
    //Options View
    IBOutlet UIView *callLocWebView;
    
    IBOutlet UIButton *bannerCancelBtn;
    IBOutlet UIImageView *bannerImage;
    IBOutlet UIView *bannerView;
    //First Image
    IBOutlet UIImageView *newsImg;
    
    //Next title
    
    IBOutlet UILabel *newsTitle;
    
    IBOutlet UILabel *lbAddress;
    IBOutlet UILabel *lbCity;
    IBOutlet UILabel *lbLocationName;
    
    IBOutlet UILabel *dateLabel;
    
    //Next seperator
    IBOutlet UIImageView *imgSeperator;
    
    //FInally details
    IBOutlet UITextView *newsDetails;
    
    //Container of all elements
    IBOutlet UIScrollView *myScrollView;
    IBOutlet UIButton *btnRegister;
    //Decide to call webserview after viewDidAppear.
    BOOL isViewLoaded;
    
    NSInteger eventImgHeight;
    
    CGFloat latitude, longitude;
    BOOL adsremoved;
    NSString *strPhoneNumber;
    
}

@property (retain, nonatomic) IBOutlet UIButton *btnInfo;

@property (nonatomic, strong) NSDictionary *eventsBannersDict;
@property (nonatomic,strong) NSString *headerTitle;

@property (nonatomic,strong) UIColor *viewBgColor;
@property (nonatomic, assign) TableObject *eventObject;
@property (nonatomic, strong) BigBanners *bigBanner;

- (IBAction)actLocation:(id)sender;
- (IBAction)actCall:(id)sender;
- (IBAction)actAttendEvent:(id)sender;
- (IBAction)actInfo:(id)sender;

@end
