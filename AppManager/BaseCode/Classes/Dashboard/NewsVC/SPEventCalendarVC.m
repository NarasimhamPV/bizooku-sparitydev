//
//  SPEventCalendarVC.m
//  Exceeding
//
//  Created by Veeru on 09/10/14.
//
//

#import "SPEventCalendarVC.h"
#import "CKCalendarView.h"
#import "EventsListVC.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "SPSingletonClass.h"
#import "EventDetailVC.h"
#import "CustomBannerView.h"
#import "WebViewController.h"

@interface SPEventCalendarVC ()<CKCalendarDelegate>

@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *datesToMark;
@property (strong, nonatomic) NSMutableArray *datesToMarkData;
@property (strong, nonatomic) UIView *customCalendarView;
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation SPEventCalendarVC
@synthesize headerTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //GetEventDatesByCategoryId
    
    //GetEventCalenderNotification
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCalendarNotification:) name:@"GetEventCalenderNotification"
                                               object:nil];
    
    
    // Do any additional setup after loading the view from its nib.
    self.headerObject.headerLabel.text = headerTitle;
    self.headerObject.btnShare.hidden = YES;
    self.headerObject.iconSeperator.hidden = YES;
    
    self.customCalendarView = [[UIView alloc]initWithFrame:CGRectMake(0, 44, 320, CGRectGetHeight(self.view.bounds))];

    self.calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
    self.calendar = self.calendar;
    self.calendar.delegate = self;
    
    [self.calendar reloadData];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
    [self.dateFormatter setLocale:[NSLocale currentLocale]];
    //    [self.dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    
    self.calendar.onlyShowCurrentMonth = NO;
    self.calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    [self.customCalendarView addSubview:self.calendar];
    [self.view addSubview:self.customCalendarView];
    self.customCalendarView.backgroundColor = [UIColor colorWithRed:(250.0/255.0) green:(250.0/255.0) blue:(250.0/255.0) alpha:(1.0)];
    
    self.view.backgroundColor = [UIColor colorWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:(1.0)];
    //    [self.customCalendarView setHidden:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
    
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 geteventsCalendar];
    
}
- (void)receiveCalendarNotification:(NSNotification *) notification
{
    self.datesToMarkData = [[AppDelegate shareddelegate].eventsCalendarDict mutableCopy];
    
    self.datesToMark = [self datesToMark];
    self.calendar.dateToMark = self.datesToMark;
    
    [self.calendar reloadData];
    if ([self.datesToMarkData count]==0){
        [AppDelegate showAlert:[NSString stringWithFormat:@"No events available"]];
        
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    //    NSLog(@"[AppDelegate shareddelegate].bannersDict %@",[AppDelegate shareddelegate].bannersDict);
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Events"]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //    NSLog(@"self.bannerUrlsArray %@",self.aBannerDict);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSArray *)datesToMark
{
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    
    if (!_datesToMark) {
        
        //        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        //        NSDateComponents *todayComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
        
        NSMutableArray *datesToMark = [[NSMutableArray alloc] initWithCapacity:0];
        
        
        [self.datesToMarkData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDate *date = [dateFormatter1 dateFromString:[obj valueForKey:@"EventDate"]];
            [datesToMark addObject:date];
            
        }];
        
        
        
        _datesToMark = [datesToMark copy];
    }
    return _datesToMark;
}

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date
{
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];

    NSString *newString = [self.dateFormatter stringFromDate:date];
    
    
    [[NSUserDefaults standardUserDefaults] setValue:date forKey:@"SelectedDate"];
    [[NSUserDefaults standardUserDefaults] setValue:newString forKey:@"DateNewString"];

    
    if ([self.datesToMark containsObject:date]) {
        
        NSMutableArray *eventsAray = [self.datesToMarkData[[self.datesToMark indexOfObject:date]] valueForKey:@"EventIds"];
        
        if ([eventsAray count] == 1){
            AppMainVC *mainVC = [[AppMainVC alloc]init];
            mainVC.appDetailVC = [[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil] ;
            mainVC.appDetailVC.appType = self.appType;
            mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
            NSDictionary *tempDict = @{@"EventId":[eventsAray objectAtIndex:0]};
            mainVC.appDetailVC.detailDataDictionary = tempDict;
            [[SPSingletonClass sharedInstance] setEventDcHaderTitle:[tempDict valueForKey:@"CategoryName"]];
            
            [self.navigationController pushViewController:mainVC.appDetailVC animated:YES];
        }else{
            EventsListVC *eventListVC = [[EventsListVC alloc]initWithNibName:@"EventsListVC" bundle:nil];
            eventListVC.dateStr = newString;
            eventListVC.appType = self.appType;

            eventListVC.isEventCalendar = YES;
            [self.navigationController pushViewController:eventListVC animated:YES];
        }
        
    }
    
}



@end
