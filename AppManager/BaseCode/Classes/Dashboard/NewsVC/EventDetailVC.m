//
//  NewsDetailVC.m
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//





#import "EventDetailVC.h"
#import "DashboardBuilder.h"
#import "UIImageView+WebCache.h"
#import "SDZEXWidgetServiceExample.h"
#import "UILabel+Exceedings.h"
#import "NSDateFormatter+Exceedings.h"
#import "NSString+Exceedings.h"
#import "AppDelegate.h"
#import "DBApp.h"
#import "FBIbee.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "PhoneNumberFormatter.h"
#import "NSDateFormatter+Exceedings.h"
#import "SPSingletonClass.h"

@interface EventDetailVC ()
//Banner
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableArray *eventsListArray;
@property (nonatomic, strong) NSString *titleText;
@property (nonatomic, strong) NSString *eventID;
@property (nonatomic,strong) IBOutlet UIView *reminderView;
@property (nonatomic,strong) IBOutlet UILabel *selectedDate;
@property (nonatomic,strong) IBOutlet UILabel *dateStr;

@property (nonatomic,strong) IBOutlet UIButton *reminderBtn;
@property (nonatomic,strong) IBOutlet UILabel *reminderLabel;
@property (nonatomic,strong) IBOutlet UILabel *thLabel;
@property (nonatomic,strong) NSString *eventType;
@property (assign) BOOL isEventAddtoCalender;

@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation EventDetailVC

@synthesize btnInfo,headerTitle,viewBgColor;
@synthesize eventObject = _eventObject;

-(void)setDetailDataDictionary:(NSDictionary *)dataSourceDic{
    
    //[self viewDidLoad];

    if ([self.eventType isEqualToString:@"Calender"]){
        [myScrollView setBackgroundColor:[UIColor colorWithRed:114.0/255.0 green:173.0/255.0 blue:141.0/255.0 alpha:1.0]];
        
    }else
    {
        [myScrollView setBackgroundColor:[UIColor whiteColor]];
        
    }
    
    [super setDetailDataDictionary:dataSourceDic];
    
    NSLog(@"LoadData: %@", dataSourceDic);
    
    
    
    if ([[SPSingletonClass sharedInstance]isViewDetailLoaded]||!isViewLoaded) {
        
        [super setDetailDataObject:(TableObject*)dataSourceDic];
        TableObject *pdcItem = (TableObject*)dataSourceDic;
        self.eventObject = pdcItem;
        
        //EventId  ItemId
        //self.eventID = [self.eventObject valueForKey:@"EventId"];
        if([[SPSingletonClass sharedInstance] isViewDetailLoaded]){
            self.eventID = [self.eventObject valueForKey:@"ItemId"];
            
            
        }else{
            self.eventID = [self.eventObject valueForKey:@"EventId"];
            
        }
        
        if (self.eventID) {
            self.itemId = [NSNumber numberWithInt:[self.eventID intValue]];
            SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            [example1 runEventByID:[self.eventID integerValue]];
        }
        else {
            [AppDelegate showAlert:@"Event not found"];
        }
    }
    else{
        [super setDetailDataObject:(TableObject*)dataSourceDic];
        //        TableObject *pdcItem = (TableObject*)dataSourceDic;
        //Event Title
        self.titleText = [dataSourceDic valueForKey:@"Title"];
        
        
        //self.headerObject.headerLabel.text = self.titleText;
        
        self.headerObject.headerLabel.text = [dataSourceDic valueForKey:@"CategoryName"];
        
        
        [self hideEverything:NO];
        
        CGFloat spacing = 10;
        CGFloat allWidth = myScrollView.frame.size.width - 2*spacing;
        
        ///Step 1: Set text///
        //Event Image
        NSString *img = [dataSourceDic valueForKey:@"Image"];
        BOOL haveImage = (img != nil && img.length != 0);
        
        
        //Event Date & time
        NSString *cleanStartDate = [NSString stringWithFormat:@"%@", [dataSourceDic valueForKey:@"StartDate"]];
        [dateLabel resizeToFit];
        NSDate *date = [cleanStartDate exceedingsDateFromServerNewsString];
        
        
        
        NSDateFormatter *format = [NSDateFormatter exceedingEventsPullTimeFormatter];
        
        NSDate *startTime = [format dateFromString:[dataSourceDic valueForKey:@"StartTime"]];
        
        NSDate *endTime = [format dateFromString:[dataSourceDic valueForKey:@"EndTime"]];
        
        NSDateFormatter *format2 = [NSDateFormatter exceedingEventsPullTimeFormatter];
        
        dateLabel.text  = [NSString stringWithFormat:@"%@ | %@ to %@", [date exceedingsStringWithSuffix], [format2 stringFromDate:startTime], [format2 stringFromDate:endTime]];
        
        
        
        //News Details
        NSString *descText = [dataSourceDic valueForKey:@"Description"];
        
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
        {
            
            
            NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            [style setAlignment: NSTextAlignmentLeft];
            NSAttributedString * subText1;
            
            if ([self.eventType isEqualToString:@"Calender"]){
                newsDetails.textColor = [UIColor whiteColor];
                
                subText1  = [[NSAttributedString alloc] initWithString:descText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Avenir-Light" size:17.0f],
                                                                                             NSParagraphStyleAttributeName : style
                                                                                             }];
                
            }else{
                newsDetails.textColor = [UIColor blackColor];
                subText1  = [[NSAttributedString alloc] initWithString:descText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Condensed" size:17.0f],
                                                                                             NSParagraphStyleAttributeName : style
                                                                                             }];
            }
            newsDetails.attributedText = subText1;
        }
        else{
            newsDetails.text = descText;
            newsDetails = [AppDelegate resizeTheTextView:newsDetails];

        }
        
        NSString *addLocationName = nil;
        
        TableObject *obj = [self getModelObjectFromDictionary:(NSMutableDictionary *)dataSourceDic];
        _eventObject = obj;
        NSDictionary *locDic = [dataSourceDic objectForKey:@"Location"];
        NSString *addFullText;
        NSString *addCityText;
        if (locDic && [locDic isKindOfClass:[NSDictionary class]]) {
            latitude = [[locDic objectForKey:@"Lattitude"] doubleValue];
            longitude = [[locDic objectForKey:@"Longitude"] doubleValue];
            
            if([[locDic allKeys] containsObject:@"Phone"])
            {
                strPhoneNumber = [locDic valueForKey:@"Phone"];
            }else{
                strPhoneNumber = @"";
            }
            
            
            if([[locDic allKeys] containsObject:@"LocationName"])
            {
                addLocationName = [locDic objectForKey:@"LocationName"];
            }
            
            [lbLocationName sizeLabelForDescription];
            [lbLocationName setTextColor:[UIColor darkGrayColor]];
            lbLocationName.text = addLocationName;
            [lbLocationName resizeToFit];
            
            
            // New requirement for event details....
            
            addFullText = [self evaluateAddressString:locDic];
            //[lbAddress sizeLabelForMid];
            [lbAddress sizeLabelForDescription];
            [lbAddress setTextColor:[UIColor darkGrayColor]];
            lbAddress.text = addFullText;
            [lbAddress resizeToFit];
            
            addCityText = [self evaluateCityAndStateString:locDic];
            //[lbAddress sizeLabelForMid];
            [lbCity sizeLabelForDescription];
            [lbCity setTextColor:[UIColor darkGrayColor]];
            lbCity.text = addCityText;
            [lbCity resizeToFit];
            
            
        }
        
        
        ///Step 2: Align All///
        CGRect lastRect = CGRectZero;
        CGFloat lastHighHeight = 0.0;
        
        //Image
        {
            lastRect = CGRectMake(spacing, 0, allWidth, 0);
            
            
            if ([self.eventType isEqualToString:@"Calender"]){
                btnInfo.hidden = YES;
                self.reminderView.hidden = NO;
                
                [myScrollView setBackgroundColor:[UIColor colorWithRed:114.0/255.0 green:173.0/255.0 blue:141.0/255.0 alpha:1.0]];
                newsTitle.font =  [UIFont fontWithName:@"AvenirNext-Bold" size:24.0f];
                
                if (!haveImage) {
                    self.selectedDate.hidden = NO;
                    self.dateStr.hidden = NO;
                    self.thLabel.hidden = NO;
                    
                    
                    [newsImg setBackgroundColor:[UIColor colorWithRed:127.0/255.0 green:169.0/255.0 blue:143.0/255.0 alpha:1.0]];
                    lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, eventImgHeight);
                    lastHighHeight = eventImgHeight + 10;
                    newsImg.frame = CGRectMake(0, 0, 320, eventImgHeight);
                }
                else {
                    self.selectedDate.hidden = YES;
                    self.dateStr.hidden = YES;
                    self.thLabel.hidden = YES;
                    
                    
                    [newsImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
                    lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, eventImgHeight);
                    lastHighHeight = eventImgHeight + 10;
                    newsImg.frame = CGRectMake(0, 0, 320, eventImgHeight);
                }
                addLocationName = nil;
                addFullText = nil;
                addCityText = nil;
                cleanStartDate = nil;
                imgSeperator.hidden= YES;
                
            }else{
                
                btnInfo.hidden = NO;
                
                self.reminderView.hidden = YES;
                self.selectedDate.hidden = YES;
                self.dateStr.hidden = YES;
                self.thLabel.hidden = YES;
                
                self.headerObject.iconSeperator.hidden = NO;
                lbAddress.hidden = NO;
                lbCity.hidden = NO;
                lbLocationName.hidden = NO;
                
                dateLabel.hidden = NO;
                imgSeperator.hidden= NO;
                
                if (!haveImage) {
                    lastRect.size.height = 0.0;
                    lastHighHeight = 10.0;
                    newsImg.frame = lastRect;
                }
                else {
                    [newsImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
                    lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, eventImgHeight);
                    lastHighHeight = eventImgHeight + 10;
                    newsImg.frame = CGRectMake(0, 0, 320, eventImgHeight);
                }
            }
            
            
            
            
        }
        newsTitle.text = self.titleText;
        [newsTitle resizeToFit];
        CGFloat lastRectCalender;
        
        
        //News Title
        {
            
            //1
            if ([self.eventType isEqualToString:@"Calender"]){
                newsTitle.textColor = [UIColor whiteColor];
                lastRectCalender = lastRect.origin.y+105;
                
                newsTitle.textAlignment = NSTextAlignmentCenter;
                
            }else{
                lastRectCalender = lastRect.origin.y;
                
                
            }
            
            if (self.titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect =  CGRectMake(lastRect.origin.x, lastRectCalender + lastHighHeight, newsTitle.frame.size.width, newsTitle.frame.size.height);
                lastHighHeight = lastRect.size.height;
            }
            newsTitle.frame = lastRect;
        }
        //        NSLog(@"The newsTitle rect : %f, %f", lastRect.origin.y, lastRect.size.height);
        
        
        
        //3
        if (addLocationName == nil) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, lbLocationName.frame.size.width, lbLocationName.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lbLocationName.frame = lastRect;
        //         NSLog(@"The lbLocationName rect : %f, %f", lastRect.origin.y, lastRect.size.height);
        
        
        //4
        if (addFullText == nil) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, lbAddress.frame.size.width, lbAddress.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lbAddress.frame = lastRect;
        //         NSLog(@"The lbAddress rect : %f, %f", lastRect.origin.y, lastRect.size.height);
        
        
        //5
        if (addCityText == nil) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, lbCity.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lbCity.frame = lastRect;
        //         NSLog(@"The lbCity rect : %f, %f", lastRect.origin.y, lastRect.size.height);
        
        
        
        //News Date & Time
        {
            if (cleanStartDate == nil || cleanStartDate.length < 10) {
                lastRect.size.height = 0.0;
            }
            else{
                lastRect = CGRectMake(dateLabel.frame.origin.x, lastRect.origin.y + lastHighHeight, allWidth, dateLabel.frame.size.height);
                lastHighHeight = lastRect.size.height;
            }
            dateLabel.frame = lastRect;
        }
        //        NSLog(@"The dateLabel rect : %f, %f", lastRect.origin.y, lastRect.size.height);
        //Seperator
        {
            if (self.titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect = CGRectMake(spacing, lastRect.origin.y + lastHighHeight, allWidth, imgSeperator.frame.size.height);
            }
            imgSeperator.frame = lastRect;
        }
        
        // btn
        
        //dataSourceDic
        if([_eventObject.fbPostID length] > 0)
        {
            btnRegister.hidden = NO;
            btnRegister.frame = CGRectMake(btnRegister.frame.origin.x, lastRect.origin.y-10.0-65, btnRegister.frame.size.width, 65);
        }
        else{
            btnRegister.hidden = YES;
        }
        
        
        //Organization text
        {
            
            lastRect = imgSeperator.frame;
            lastHighHeight = lastRect.size.height;
            
            if ([self.eventType isEqualToString:@"Calender"]){
                btnRegister.hidden = YES;

                newsDetails = [AppDelegate resizeCalenderTextView:newsDetails];
                newsDetails.textColor = [UIColor whiteColor];
                
            }else{
                newsDetails = [AppDelegate resizeTheTextView:newsDetails];
                newsDetails.textColor = [UIColor blackColor];
                
            }
            
            
            if (descText == nil) {
                lastRect.size.height = 0.0;
            }
            else{
                lastRect = CGRectMake(lastRect.origin.x-8, lastRect.origin.y + lastHighHeight + spacing, allWidth+16, newsDetails.frame.size.height);
            }
            
            newsDetails.frame = lastRect;
        }
        
        
        //Finally set scrolling
        myScrollView.contentSize = CGSizeMake(myScrollView.frame.size.width, newsDetails.frame.origin.y + newsDetails.frame.size.height + spacing );
        
        
        
        //Configure FB Like View
        //Call, location floating right
        {
            callLocWebView.frame = CGRectMake(callLocWebView.frame.origin.x , newsTitle.frame.origin.y + newsTitle.frame.size.height, callLocWebView.frame.size.width , callLocWebView.frame.size.height );
        }
    }
    
    
    
}


- (TableObject *)getModelObjectFromDictionary:(NSMutableDictionary *)dic
{
    TableObject *obj = [[[TableObject alloc] init] autorelease];
    obj.name = [dic valueForKey:@"Name"];
    obj.locationName = [dic valueForKey:@"LocationName"];
    obj.desc = [dic valueForKey:@"Description"];
    obj.zip = [dic valueForKey:@"Zip"];
    obj.state = [dic valueForKey:@"State"];
    obj.eventCategoryName = [dic valueForKey:@"CategoryName"];//
    obj.eventCategoryId = [dic valueForKey:@"CategoryId"];
    obj.titleText = [dic valueForKey:@"Title"];
    obj.newsID = [NSString stringWithFormat:@"%@", [dic valueForKey:@"NewsId"]] ;
    obj.eventID = [NSString stringWithFormat:@"%@", [dic valueForKey:@"EventId"]] ;
    obj.locId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"LocationId"]];
    obj.productId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"ProductId"]];
    NSDateFormatter *format = [NSDateFormatter exceedingEventsPullTimeFormatter];
    
    obj.startTime = [format dateFromString:[dic valueForKey:@"StartTime"]];
    
    
    obj.endTime = [format dateFromString:[dic valueForKey:@"EndTime"]];
    
    obj.startTimeStr = [[[[dic valueForKey:@"StartTime"]
                          stringByReplacingOccurrencesOfString:@" PM" withString:@"pm"]
                         stringByReplacingOccurrencesOfString:@" AM" withString:@"am"]
                        stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    obj.endTimeStr = [[[[dic valueForKey:@"EndTime"]
                        stringByReplacingOccurrencesOfString:@" PM" withString:@"pm"]
                       stringByReplacingOccurrencesOfString:@" AM" withString:@"am"]
                      stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    obj.fbPostID = [dic valueForKey:@"FBPostId"];
    
    NSString *image = [dic valueForKey:@"Image"];
    
    if (image && image.length) {
        obj.image = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [dic valueForKey:@"Image"]];
    }
    
    obj.location =  [[[CLLocation alloc] initWithLatitude:[[dic valueForKey:@"Lattitude"] doubleValue] longitude:[[dic valueForKey:@"Longitude"] doubleValue]] autorelease];
    obj.lattitude = [dic valueForKey:@"Lattitude"];
    obj.longitude = [dic valueForKey:@"Longitude"];
    
    obj.address = [dic valueForKey:@"Address"];
    
    obj.city = [dic valueForKey:@"City"];
    obj.fax = [dic valueForKey:@"Fax"];
    obj.primaryEmail = [dic valueForKey:@"PrimaryEmailAddress"];
    
    PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
    obj.phone = [phoneNumberFormatter format:[dic valueForKey:@"Phone"] withLocale:@"us"];
    obj.additonalInfo = [dic valueForKey:@"AdditonalInfo"];
    obj.website = [dic valueForKey:@"Website"];
    
    
    NSString *startDate = [dic valueForKey:@"StartDate"];
    obj.startDate = [startDate exceedingsDateFromServerNewsString];
    
    NSString *endDate = [dic valueForKey:@"EndDate"];
    obj.endDate = [endDate exceedingsDateFromServerNewsString];
    
    NSString *date = [dic valueForKey:@"CreatedDate"];
    if(date)
    {
        obj.date = [date exceedingsDateFromServerNewsString];
    }
    //self.headerObject.headerLabel.text =tempStr;
    self.eventObject = (TableObject*)dic;
    
    
    return obj;
}


-(void)resizeToFit:(UITextView *)txtView
{
    
    /* float height = [self expectedHeight:txtView];
     CGRect newFrame = [txtView frame];
     newFrame.size.height = height;
     [txtView setFrame:newFrame];
     return newFrame.origin.y + newFrame.size.height;*/
    
    CGRect frame = txtView.frame;
    frame.size.height = txtView.contentSize.height;
    txtView.frame = frame;
}


-(NSString*)evaluateAddressString:(NSDictionary *)locationDic{
    NSMutableString *addStr = [NSMutableString string];
    
    if(locationDic && [[locationDic allKeys] containsObject:@"Address"])
    {
        NSString *keyObject = [locationDic objectForKey:@"Address"];
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@"%@", keyObject];
        }
    }
    return [NSString stringWithString:addStr];
}

- (NSString *)evaluateCityAndStateString:(NSDictionary *)locationDic
{
    NSMutableString *addStr = [NSMutableString string];
    if(locationDic && [[locationDic allKeys] containsObject:@"City"])
    {
        NSString *keyObject = [locationDic objectForKey:@"City"];
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@"%@", keyObject];
        }
    }
    
    if(locationDic && [[locationDic allKeys] containsObject:@"State"])
    {
        NSString *keyObject = [locationDic objectForKey:@"State"];
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@", %@", keyObject];
        }
    }
    
    if(locationDic && [[locationDic allKeys] containsObject:@"Zip"])
    {
        NSString *keyObject = [locationDic objectForKey:@"Zip"];
        
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@" - %@", keyObject];
        }
    }
    
    return [NSString stringWithString:addStr];
    
}
- (void)fetchEventLocationString:(NSDictionary *)dataSourceDic
{
    if(dataSourceDic)
    {
        if([[dataSourceDic allKeys] containsObject:@"Location"])
        {
            NSDictionary *locDic = [dataSourceDic objectForKey:@"Location"];
            if(locDic)
            {
                
            }
        }
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        eventImgHeight = 240;
    }
    return self;
}


-(void)hideEverything:(BOOL)shouldHide
{
    
    //Hide all
    btnInfo.hidden = shouldHide;
    callLocWebView.hidden = YES;
    btnRegister.hidden = shouldHide;
    newsImg.hidden = shouldHide;
    newsTitle.hidden = shouldHide;
    lbLocationName.hidden = shouldHide;
    lbAddress.hidden = shouldHide;
    lbCity.hidden = shouldHide;
    
    dateLabel.hidden = shouldHide;
    imgSeperator.hidden = shouldHide;
    newsDetails.hidden = shouldHide;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.headerObject.headerLabel.text = @"";
    self.headerObject.iconSeperator.hidden = YES;
    
    self.eventType = [[SPSingletonClass sharedInstance] eventViewTypeStr];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEventsDetailsNotification:)
                                                 name:@"getEventsDetailsNotification"
                                               object:nil];
    
    
    
    if ([self.eventType isEqualToString:@"Calender"]){
        [myScrollView setBackgroundColor:[UIColor colorWithRed:114.0/255.0 green:173.0/255.0 blue:141.0/255.0 alpha:1.0]];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"cccc-MMMM--dd"];
        NSString *dateString = [dateFormat stringFromDate:[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"] ];
        NSMutableString *tempDate = [[NSMutableString alloc]initWithString:dateString];
        int day = [[tempDate substringFromIndex:[tempDate length]-2] intValue];
        switch (day) {
            case 1:
            case 21:
            case 31:
                [tempDate appendString:@" st"];
                break;
            case 2:
            case 22:
                [tempDate appendString:@" nd"];
                break;
            case 3:
            case 23:
                [tempDate appendString:@" rd"];
                break;
                
                
            default:
                [tempDate appendString:@" th"];
                break;
        }
        
        
        NSString *date = [NSString stringWithFormat:@"%d",day];
        
        
        
        
        
        //        if (date.length == 1){
        //
        //            self.thLabel.frame = CGRectMake(self.thLabel.frame.origin.x-26, self.thLabel.frame.origin.y, self.thLabel.frame.size.width, self.thLabel.frame.size.height);
        //
        //        }
        
        NSArray *dateStrArray = [tempDate componentsSeparatedByString:@"-"];
        
        self.selectedDate.font = [UIFont fontWithName:@"AvenirNext-Bold" size:40.0f];
        self.dateStr.font = [UIFont fontWithName:@"AvenirNext-Bold" size:90.0f];//Helvetica-Condensed
        self.selectedDate.adjustsFontSizeToFitWidth = YES;
        self.reminderBtn.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:24.0f];
        self.reminderLabel.font = [UIFont fontWithName:@"Avenir-Book" size:24.0f];
        self.thLabel.font = [UIFont fontWithName:@"AvenirNext-Bold" size:35.0];
        
        NSMutableString *dayStr = [dateStrArray objectAtIndex:3];
        NSArray *dayArray = [dayStr componentsSeparatedByString:@" "];
        self.selectedDate.hidden = YES;
        self.dateStr.hidden = YES;
        self.thLabel.hidden = YES;
        self.selectedDate.text = [NSString stringWithFormat:@"%@,\n%@",[dateStrArray objectAtIndex:0],[dateStrArray objectAtIndex:1]];
        self.dateStr.text = date;
        self.thLabel.text = [NSString stringWithFormat:@"%@",[dayArray objectAtIndex:1]];
        
        
        CGSize expectedSize = [self.dateStr sizeThatFits:CGSizeMake(CGRectGetWidth(self.dateStr.frame), CGFLOAT_MAX)];
        
        [self.dateStr setFrame:CGRectMake((278-expectedSize.width)/2, CGRectGetMinY(self.dateStr.frame), expectedSize.width, CGRectGetHeight(self.dateStr.frame))];
        
        [self.thLabel setFrame:CGRectMake(CGRectGetMaxX(self.dateStr.frame), self.thLabel.frame.origin.y, self.thLabel.frame.size.width, self.thLabel.frame.size.height)];
        
        
        if ([self eventExisted]){
            [self.reminderBtn setTitle: @"Reminder Set!" forState: UIControlStateNormal];
            self.reminderBtn.userInteractionEnabled = NO;
            
        }else{
            [self.reminderBtn setTitle: @"Not Yet! Set one." forState: UIControlStateNormal];
            self.reminderBtn.userInteractionEnabled = YES;
        }
        
        
    }else{
        [myScrollView setBackgroundColor:[UIColor whiteColor]];
        
    }
    
    
    self.eventsListArray = [[NSMutableArray alloc] init];
    
    isViewLoaded = YES;
    
    [self hideEverything:YES];
    [self.view bringSubviewToFront:btnInfo];
    
    [newsDetails setTextColor:[UIColor darkGrayColor]];
    
    [newsTitle sizeLabelForTitle];
    [dateLabel sizeLabelForEventsDate];
    
    for (NSDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Events"]) {
            self.eventsBannersDict = dict;
            break;

        }
    }
    
}
-(void)receiveEventsDetailsNotification:(NSNotification *)notification
{
    [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:NO];

    self.eventsListArray = [[AppDelegate shareddelegate].eventsDetailsDict mutableCopy];
    [self setDetailDataDictionary:[[AppDelegate shareddelegate].eventsDetailsDict mutableCopy]];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}
- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (IBAction)eventAddtoCalendar:(id)sender
{
    [self addEventToCalendar];
    
    /*
     EKEventStore *store = [[EKEventStore alloc] init];
     [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
     if (!granted) {
     return;
     }
     EKEvent *event = [EKEvent eventWithEventStore:store];
     event.title = self.titleText;
     event.startDate = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"];//[NSDate date]; //today
     event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
     [event setCalendar:[store defaultCalendarForNewEvents]];
     NSError *err = nil;
     [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
     NSString *savedEventId = event.eventIdentifier;  //this is so you can access this event later
     NSLog(@"savedEventId %@",savedEventId);
     
     }];*/
    
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    //    NSLog(@"[AppDelegate shareddelegate].bannersDict %@",[AppDelegate shareddelegate].bannersDict);
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Events"]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //    NSLog(@"self.bannerUrlsArray %@",self.aBannerDict);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}

- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

- (void)viewDidUnload
{
    [newsImg release];
    newsImg = nil;
    [newsTitle release];
    newsTitle = nil;
    [imgSeperator release];
    imgSeperator = nil;
    [newsDetails release];
    newsDetails = nil;
    [myScrollView release];
    myScrollView = nil;
    [dateLabel release];
    dateLabel = nil;
    
    [dateLabel release];
    dateLabel = nil;
    
    [lbLocationName release];
    lbLocationName = nil;
    
    [lbAddress release];
    lbAddress = nil;
    
    [lbCity release];
    lbCity = nil;
    
    [callLocWebView release];
    callLocWebView = nil;
    [self setBtnInfo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (IBAction)actLocation:(id)sender {
    
    [self getDirections];
    
}

//- (IBAction)actCall:(id)sender {
//    NSString *number = [AppDelegate shareddelegate].appReference.phoneNum;
//    NSURL *callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", number]];
//
//    if ([[UIApplication sharedApplication] canOpenURL:callURL]) {
//        [[UIApplication sharedApplication] openURL:callURL];
//    }
//    else{
//        [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to dial the number. Please dial the number '%@' manually.", number ]];
//    }
//}

- (NSString *)checkPhoneNumber: (NSString *)number
{
    NSString *str = [[[[number stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}


- (IBAction)actCall:(id)sender {
    NSString *number = [self checkPhoneNumber:strPhoneNumber];
    
    if(number && ![number isEqualToString:@""])
    {
        NSURL *callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", number]];
        //        NSLog(@"The number is : %@",[self checkPhoneNumber:number]);
        if ([[UIApplication sharedApplication] canOpenURL:callURL]) {
            [[UIApplication sharedApplication] openURL:callURL];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to dial the number. Please dial the number '%@' manually.", strPhoneNumber ]];
        }
    }
    else{
        [AppDelegate showAlert:@"Unable to dial the number."];
    }
}
- (IBAction)actAttendEvent:(id)sender {
    
    if(_eventObject.fbPostID)
    {
        [FBIbee sharedDelegate].detailObject = _eventObject.fbPostID;
        
        [[FBIbee sharedDelegate] registeringAnEvent];
    }
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        // [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Event-Join" deviceType:@"iOS"];
    }
    else{
        //  [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Event-Join" deviceType:@"iOS"];
    }
    
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
    
}







#pragma mark - UIActionSheet Delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *tempString = nil;
    switch (buttonIndex) {
        case 0:
        {
            [self actAttendEvent:nil];
            tempString = @"Join Event to Facebook";
        }
            break;
        case 1:
        {
            [self addEventToCalendar];
            tempString = @"Add to Calendar";
        }
            break;
        case 2:
        {
            [self actLocation:nil];
            tempString = @"Directions";
        }
            break;
        case 3:
        {
            tempString = @"Call";
            [self actCall:nil];
            break;
            
        }
        case 4:
        {
            tempString = @"Canceled";
        }
            break;
        default:
            break;
    }
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] action:tempString deviceType:@"iOS"];
    }
    else{
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:tempString deviceType:@"iOS"];
    }
}


#pragma mark - UIActionSheet Delegates

- (IBAction)actInfo:(id)sender {
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] action:@"Entry" deviceType:@"iOS"];
    }
    else{
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Entry" deviceType:@"iOS"];
    }
    
    UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Event" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Join Event on Facebook", @"Add to Calendar", @"Directions",  @"Call", nil] autorelease];
    [actinsheet showInView:self.view];
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIView *_currentView in actionSheet.subviews) {
        if ([_currentView isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel *)_currentView;
            
            [lbl setFrame:CGRectMake(0, 5, 320, 40)];
            [lbl setTextAlignment:NSTextAlignmentCenter];
            
            [((UILabel *)_currentView) setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:22]];
        }
    }
}

#pragma mark Events Adding to Native Calendar:

- (void)addEventToCalendar
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Set Reminder?" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    [alertView show];
    /*
     if(!self.eventExisted)
     {
     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Set Reminder?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
     [alertView show];
     }
     else {
     [AppDelegate showAlert:@"Event already existed in your calendar"];
     }*/
}
#pragma mark Fetching Events from the Native Calendar

- (BOOL)eventExisted
{
    
    EKEventStore *eventStore = [[[EKEventStore alloc] init] autorelease];
    
    // Get the appropriate calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    
    // Create the start date components
    NSDateComponents *oneDayAgoComponents = [[NSDateComponents alloc] init];
    oneDayAgoComponents.day = -1;
    // NSDate *oneDayAgo = [calendar dateByAddingComponents:oneDayAgoComponents toDate:[NSDate date] options:0];
    
    NSDate *oneDayAgo = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"];
    
    //    NSDate *oneDayAgo = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"];
    NSLog(@"oneDayAgo %@",oneDayAgo);
    // Create the end date components
    NSDateComponents *oneYearFromNowComponents = [[NSDateComponents alloc] init];
    oneYearFromNowComponents.year = 1;
    NSDate *oneYearFromNow = [calendar dateByAddingComponents:oneYearFromNowComponents
                                                       toDate:oneDayAgo
                                                      options:0];
    
    // Create the predicate from the event store's instance method
    NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:oneDayAgo
                                                                 endDate:oneYearFromNow
                                                               calendars:nil];
    
    NSLog(@"The predicate string is : %@", predicate);
    
    // Fetch all events that match the predicate
    NSArray *events = [eventStore eventsMatchingPredicate:predicate];
    
    for(EKEvent *item in events)
    {
        NSMutableString *checkString = [NSMutableString string];
        
        //        NSLog(@"item.notes %@",item.notes);
        //        NSLog(@"self.eventID %@",self.eventID);
        
        
        switch ([item.startDate compare:oneDayAgo]){
            case NSOrderedAscending:
                NSLog(@"NSOrderedAscending");
                break;
            case NSOrderedSame:
            {
                if(item.notes)
                {
                    [checkString appendString:item.notes];
                    
                    NSArray *arr = [checkString componentsSeparatedByString:@"//&"];
                    
                    if(arr && [arr count]>=2)
                    {
                        NSString *strTempId = [arr lastObject];
                        if([strTempId isEqualToString:[NSString stringWithFormat:@"%@", self.eventID]])
                        {
                            return YES;
                        }
                    }
                }
                
            }
                NSLog(@"NSOrderedSame");
                break;
            case NSOrderedDescending:
                NSLog(@"NSOrderedDescending");
                break;
        }
        
        
    }
    
    return NO;
}

- (NSDate *)fetchTheExactDate:(NSString *)time date:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setTimeZone:[NSTimeZone localTimeZone]];
    [timeFormat setDateFormat:@"dd/MM/yyyy hha"];
    
    
    NSDate *resultDate = nil;
    
    if(date && time)
    {
        resultDate = [timeFormat dateFromString:[NSString stringWithFormat:@"%@ %@", [dateFormat stringFromDate:date], time]];
    }
    
    return resultDate;
}
- (void)addEventToEventStore
{
    EKEventStore *eventStore = [[EKEventStore alloc] init]  ;
    
    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        // iOS 6 and later
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            
            
            if (granted){
                //---- codes here when user allow your app to access theirs' calendar.
                //  [self performCalendarActivity:eventStore];
                [self performSelectorOnMainThread: @selector(performCalendarActivity:) withObject:eventStore waitUntilDone:NO];
                
            }else
            {
                //----- codes here when user NOT allow your app to access the calendar.
                [self performSelectorOnMainThread: @selector(alertForNoAccess:) withObject:@"Enable access for calendars in Privacy Settings" waitUntilDone:NO];
                
            }
        }];
    }
    else
    {
        [self performSelectorOnMainThread: @selector(performCalendarActivity:) withObject:eventStore waitUntilDone:NO];
    }
    
}


- (void)alertForNoAccess:(NSString *)msg
{
    UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
}

- (void)performCalendarActivity:(EKEventStore *)eventStore
{
    
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    event.title = [self.eventsListArray valueForKey:@"Title"];
    
    event.startDate = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"];//[self fetchTheExactDate:[self.eventsListArray valueForKey:@"StartTime"] date:[self.eventsListArray valueForKey:@"StartDate"]];
    //EndDate
    event.endDate = [event.startDate dateByAddingTimeInterval:60*60];//[self fetchTheExactDate:[self.eventsListArray valueForKey:@"EndTime"] date:[self.eventsListArray valueForKey:@"EndDate"]];
    
    NSString *eventStr  = [NSString stringWithFormat:@"%@",[self.eventsListArray valueForKey:@"EventId"]];
    
    event.location = [self.eventsListArray valueForKey:@"LocationName"];
    event.allDay = NO;
    //event.URL = [NSURL URLWithString:_eventObject.website];
    
    NSMutableString *stringTemp = [NSMutableString string];
    [stringTemp appendString:[self.eventsListArray valueForKey:@"Description"]];
    [stringTemp appendString:@"//&"];
    [stringTemp appendString:eventStr];
    
    
    event.notes = stringTemp;
    
    [event setCalendar:[eventStore defaultCalendarForNewEvents]];
    
    NSError *err = nil;
    [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
    if(err){
        [AppDelegate showAlert:[err localizedDescription]];
    }
    else {
        self.reminderBtn.userInteractionEnabled = NO;
        
        [self.reminderBtn setTitle: @"Reminder Set!" forState: UIControlStateNormal];
        [AppDelegate showAlert:@"Event Added to Your Calendar"];
    }
    
    
}

- (void)presentEventEditViewControllerWithEventStore:(EKEventStore*)eventStore
{
    
    
    EKEventEditViewController* eventEditVC = [[EKEventEditViewController alloc] init];
    eventEditVC.eventStore = eventStore;
    
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    event.title = _eventObject.titleText;
    
    
    event.startDate = [[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"];
    event.endDate = [self fetchTheExactDate:_eventObject.endTimeStr date:_eventObject.endDate];
    
    event.location = _eventObject.locationName;
    event.allDay = NO;
    event.URL = [NSURL URLWithString:_eventObject.website];
    NSMutableString *stringTemp = [NSMutableString string];
    [stringTemp appendString:_eventObject.desc];
    [stringTemp appendString:@"//&"];
    [stringTemp appendString:_eventObject.eventID];
    
    event.notes = stringTemp;
    
    eventEditVC.event = event;
    
    eventEditVC.editViewDelegate = self;
    
    [self presentViewController:eventEditVC animated:YES completion:nil];
    
}



- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark Get Directions Stuff

- (void)getDirections
{
    CLLocationCoordinate2D origin = [AppDelegate shareddelegate].currentLocation.coordinate;
    //CLLocationCoordinate2D destination = locationObject.location.coordinate;
    
    //float latitude = _eventObject.location.coordinate.latitude;
    //float longtitude = _eventObject.location.coordinate.longitude;
    
    
    CLLocationCoordinate2D destination = { latitude, longitude };
    
    NSString *appleMapsURLStr = [NSString stringWithFormat:@"maps://q=&saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f&view=map", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    NSURL *appleURL = [NSURL URLWithString:appleMapsURLStr];
    appleURL = nil;
    
    //    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    
    NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%1.6f,%1.6f",destination.latitude, destination.longitude];
    NSURL *appleMapUrl = [NSURL URLWithString:urlString];
    
    //    NSURL *url = [NSURL URLWithString:googleMapsURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:appleURL]) {
        [[UIApplication sharedApplication] openURL:appleURL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:appleMapUrl]) {
        [[UIApplication sharedApplication] openURL:appleMapUrl];
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to find directions"]];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self addEventToEventStore];
        
    }
    
}

@end
