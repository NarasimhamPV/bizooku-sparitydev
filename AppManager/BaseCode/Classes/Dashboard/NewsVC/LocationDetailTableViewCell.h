//
//  LocationDetailTableViewCell.h
//  Exceeding
//
//  Created by Sparity on 8/12/14.
//
//

#import <UIKit/UIKit.h>

@interface LocationDetailTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (retain, nonatomic) IBOutlet UILabel *titleLbl;

@end
