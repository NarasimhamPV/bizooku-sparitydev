//
//  NewsDetailVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "ExFBLike.h"
#import "TableObject.h"
#import "BigBanners.h"
@interface NewsDetailVC : AppMainDetailVC <UIWebViewDelegate,BigBannersDelegate>
{
    //FB Like View
    IBOutlet ExFBLike *myFbLike;
    
    //First Image
    IBOutlet UIImageView *newsImg;
    //Next title
    IBOutlet UILabel *newsTitle;
    IBOutlet UILabel *dateLabel;
    BOOL adsremoved;
    //Next seperator
    IBOutlet UIImageView *imgSeperator;
    //FInally details
    IBOutlet UITextView *newsDetails;
    //Container of all elements
    IBOutlet UIScrollView *myScrollView;
    
    //Decide to call webserview after viewDidAppear.
    BOOL isViewLoaded;
    NSInteger newsImgHeight;
}
@property (retain, nonatomic) NSString *websiteURL;
@property (nonatomic, assign) TableObject *newsObject;
@property (nonatomic, strong) NSDictionary *newsbannersDict;
@property (nonatomic, strong) NSMutableArray *bannerURLS;
@property (nonatomic, strong) BigBanners *bigBanner;

- (void)startShowingBanners:(NSString *)bannerURL;


@end
