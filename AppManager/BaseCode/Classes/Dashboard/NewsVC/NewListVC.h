//
//  NewListVC.h
//  Exceeding
//
//  Created by Veeru on 26/08/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "BigBanners.h"
@interface NewListVC : AppMainDetailVC<UITableViewDataSource,UITableViewDelegate,BigBannersDelegate>
@property (nonatomic, strong) BigBanners *bigBanner;



@end
