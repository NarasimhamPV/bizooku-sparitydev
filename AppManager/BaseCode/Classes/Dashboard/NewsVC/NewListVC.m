//
//  NewListVC.m
//  Exceeding
//
//  Created by Veeru on 26/08/14.
//
//

#import "NewListVC.h"
#import "AppMainVC.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "NewsListCell.h"
#import "NewsDetailVC.h"

#import "NSString+Exceedings.h"
#import "UILabel+Exceedings.h"
#import "NSDateFormatter+Exceedings.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"


static NSString *const kNewsListCell = @"NewsListCellIdentfier";
static NSString *const kNewsListCellNib = @"NewsListCell";
@interface NewListVC ()

@property (retain, nonatomic) IBOutlet UITableView *newsListTable;
@property (strong, nonatomic) NSArray *newsListArray;
@property (strong, nonatomic) AppMainVC *appmainVC;
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation NewListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.appType = APP_EVENTS;
    [self.newsListTable registerNib:[UINib nibWithNibName:kNewsListCellNib bundle:nil] forCellReuseIdentifier:kNewsListCell];
    self.newsListTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    self.headerObject.rightToolbarItem = nil;
    self.headerObject.headerLabel.text =@"";
    self.headerObject.btnShare.hidden = YES;
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNewsListsNotification:)
                                                 name:@"getNewsListNotification"
                                               object:nil];
    
    
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    
    [example1 runNewsList];
    

}

- (void)receiveNewsListsNotification:(NSNotification *) notification
{
    self.newsListArray = [[AppDelegate shareddelegate].newsListDict mutableCopy];
    //self.headerObject.headerLabel.text = [self.newsListArray valueForKey:@"CategoryName"];

    [self reloadTable];
    
}
- (void)reloadTable
{
    [self.newsListTable reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.newsListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NewsListCell *newsListCell = [tableView dequeueReusableCellWithIdentifier:kNewsListCell];
    newsListCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *tempDict1 =[self.newsListArray objectAtIndex:indexPath.row];
    
    self.headerObject.headerLabel.text = [tempDict1 valueForKey:@"CategoryName"];
    newsListCell.titleLabel.text = [tempDict1 objectForKey:@"Title"];
    
    
    NSString *cleanStartDate = [tempDict1 valueForKey:@"StartDate"];
    NSLog(@"cleanStartDate %@",cleanStartDate);
    
    [newsListCell.titleLabel resizeToFit];
    //NSDate *date = [cleanStartDate exceedingsDateFromServerNewsString];
    newsListCell.descriptionLabel.text = [tempDict1 valueForKey:@"Duration"];//[date exceedingsDaysAgoString];

    
    NSDictionary *tempDict = [self.newsListArray objectAtIndex:indexPath.row];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[tempDict objectForKey:@"Image"]];
    if ([urlString length]!=0) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
            NSURLResponse* response = nil;
            NSError* error = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                // update the image and sent notification on the main thread
                NSLog(@"veeru %@",[tempDict valueForKey:@"Image"]);
                
                        
                if ([[tempDict valueForKey:@"Image"] length]) {
                    newsListCell.listImage.image = [UIImage imageWithData:data];
                   // newsListCell.listImage.frame = CGRectMake(0, 2, 65, 52);

                }else{
                    newsListCell.listImage.image = [UIImage imageNamed:@"eventsdefault"];
                    //newsListCell.listImage.frame = CGRectMake(0, 2, 65, 52);

                }
                
            });
        });
        
        
    }else{
        newsListCell.listImage.image = [UIImage imageNamed:@"eventsdefault"];

    }
    return newsListCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


-  (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tempDict = [self.newsListArray objectAtIndex:indexPath.row];
    
    AppMainVC *mainVC = [[AppMainVC alloc]init];
    
    mainVC.appDetailVC = [[NewsDetailVC alloc]initWithNibName:@"NewsDetailVC" bundle:nil] ;
    mainVC.appDetailVC.appType = self.appType;
    mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
    mainVC.appDetailVC.detailDataDictionary = tempDict;
    [self.navigationController pushViewController:mainVC.appDetailVC animated:YES];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"News"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}
- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


@end
