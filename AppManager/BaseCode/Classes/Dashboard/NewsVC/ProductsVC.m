//
//  ProductsVC.m
//  Exceeding
//
//  Created by Rajesh Kumar on 12/12/13.
//
//

#import "ProductsVC.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "ProductsWebView.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"


@interface ProductsVC ()

@property (nonatomic, retain) InfoCollectorVC *infoCollVC;

//Banner
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation ProductsVC


@synthesize btnInfo;
@synthesize prodObject = _prodObject;
@synthesize isActionURL = _isActionURL;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(void)setDetailDataDictionary:(NSDictionary *)dataSourceDic{
    [super setDetailDataDictionary:dataSourceDic];
    
    NSLog(@"LoadData: %@", dataSourceDic);
    [self viewDidLoad];
    self.headerObject.headerLabel.text = [dataSourceDic valueForKey:@"CategoryName"];

    
    if ([[SPSingletonClass sharedInstance]isViewDetailLoaded]) {
        
        [super setDetailDataObject:(TableObject*)dataSourceDic];
        
        // self.prodObject = (TableObject*)dataSourceDic;

       // NSLog(@"The Data Source dic product details : %@, %@", _prodObject.name, _prodObject.price);
        
      //  NSLog(@"The Data Source dic event end time : %@, %@", _eventObject.endDate, _eventObject.startTimeStr);
        
        int prodID = [[dataSourceDic valueForKey:@"ItemId"] intValue]; //_prodObject.productId;
        self.itemId = [dataSourceDic valueForKey:@"ItemId"];
        
        if (prodID != 0) {
            self.itemId = [NSNumber numberWithInt:prodID];
            SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
            if(self.boolNoListing)
            {
                [example1 runDirectProductByID:prodID];
            }
            else{
                [example1 runProductByID:prodID];
            }
        }
        else {
            [AppDelegate showAlert:@"Product not found"];
        }
    }
    else{
        [self hideEverything:NO];
        
        CGFloat spacing = 10;
        CGFloat allWidth = myScrollView.frame.size.width - 2*spacing;
        
        NSLog(@"dataSourceDic %@",dataSourceDic);
        
        if ([[dataSourceDic valueForKey:@"CategoryName"] isEqualToString:@"Default"]){
            self.headerObject.headerLabel.text = [dataSourceDic valueForKey:@"PageTitle"];
        }
       
        ///Step 1: Set text///
        //Event Image
        NSString *img = [dataSourceDic valueForKey:@"Image"];
        BOOL haveImage = (img != nil && ![img isEqualToString:@""]);
        
        //Event Title
        NSString *titleText = [dataSourceDic valueForKey:@"Title"];
        lblTitle.text = titleText;
        [lblTitle resizeToFit];
        
        
        //News Details
        NSString *descText = [dataSourceDic valueForKey:@"Description"];
        
        //  newsDetails.text = descText;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
        {
            NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            [style setAlignment: NSTextAlignmentLeft];
            
            NSAttributedString * subText1 = [[NSAttributedString alloc] initWithString:descText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Condensed" size:16.0f],
                                                                                                             NSParagraphStyleAttributeName : style
                                                                                                             }];
            txtDescription.attributedText = subText1;
        }
        else{
            txtDescription.text = descText;
        }
        
        
        // [newsDetails resizeToFit];
        txtDescription = [AppDelegate resizeTheTextView:txtDescription];
        
        float price = 0.0;
        
        
        if([[dataSourceDic allKeys] containsObject:@"Phone"])
        {
            self.strPhoneNumber = [dataSourceDic valueForKey:@"Phone"];
        }
        else{
            self.strPhoneNumber = @"";
        }
        
        if([[dataSourceDic allKeys] containsObject:@"Email"])
        {
            self.strEmailId = [dataSourceDic valueForKey:@"Email"];
        }
        else{
            self.strEmailId = @"";
        }
        
        if(![_strEmailId isEqualToString:@""] || ![_strPhoneNumber isEqualToString:@""])
        {
            btnInfo.hidden = NO;
            [self.view bringSubviewToFront:btnInfo];
        }
        else{
            btnInfo.hidden = YES;
        }
        
        NSString *priceStr = [NSString stringWithFormat:@"%.2f", [[dataSourceDic objectForKey:@"Price"] floatValue]];
        
        NSLog(@"priceStr %@",priceStr);
/*
        if([[dataSourceDic allKeys] containsObject:@"Price"])
        {
            
            
            if ([priceStr isEqualToString:@"0.00"]){
                lblPrice.hidden = YES;

            }else{
                lblPrice.hidden = NO;
                
            }

        }
        else{
            price = 0.0;
            lblPrice.hidden = YES;
        }
        
        [lblPrice sizeLabelForTitle];
        [lblPrice setTextColor:[UIColor darkGrayColor]];
        lblPrice.text = priceStr;
        
        [lblPrice resizeToFit];
        [lblPrice setTextAlignment:NSTextAlignmentCenter];
        */
        
        if([[dataSourceDic allKeys] containsObject:@"Price"])
        {
            price = [[dataSourceDic objectForKey:@"Price"] intValue];
            lblPrice.hidden = NO;
            if(price == 0)
            {
                lblPrice.hidden = YES;
            }
            
        }
        else{
            price = 0.0;
            lblPrice.hidden = YES;
        }
        
        [lblPrice sizeLabelForTitle];
        [lblPrice setTextColor:[UIColor darkGrayColor]];
        lblPrice.text = [NSString stringWithFormat:@"$%.2f",price];
        [lblPrice resizeToFit];
        [lblPrice setTextAlignment:NSTextAlignmentCenter];

        
        ///Step 2: Align All///
        CGRect lastRect = CGRectZero;
        CGFloat lastHighHeight = 0.0;
        CGFloat eventImgHeight = 240.0;
        //Image
        {
            lastRect = CGRectMake(spacing, 0, allWidth, 0);
            
            if (!haveImage) {
                lastRect.size.height = 0.0;
                lastHighHeight = 10.0;
                productImg.frame = lastRect;
            }
            else {
                [productImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
                lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, eventImgHeight);
                lastHighHeight = eventImgHeight + 10;
                
                productImg.frame = CGRectMake(0, 0, 320, eventImgHeight);
            }
            
        }
        
        //News Title
        {
            //1
            if (titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y+lastHighHeight, lblTitle.frame.size.width, lblTitle.frame.size.height);
                lastHighHeight = lastRect.size.height;
            }
            lblTitle.frame = lastRect;
        }
        
        NSLog(@"The produc Title rect : %f, %f", lastRect.origin.y, lastRect.size.height);
        
        
        
        //3
        if (price == 0) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight+spacing, lblPrice.frame.size.width, lblPrice.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lblPrice.frame = lastRect;
        [lblPrice setTextColor:[UIColor colorWithRed:0.067 green:0.625 blue:1.000 alpha:1.000]];
        
        NSLog(@"The product price rect : %f, %f", lastRect.origin.y, lastRect.size.height);
        
        if([[dataSourceDic allKeys] containsObject:@"ActionButtonURL"])
        {
            self.strActionURL = [dataSourceDic valueForKey:@"ActionButtonURL"];
        }
        else{
            self.strActionURL = @"";
            
        }
        
        if([[dataSourceDic allKeys] containsObject:@"ActionButtonEmail"])
        {
            self.strActionEmailId = [dataSourceDic valueForKey:@"ActionButtonEmail"];
        }
        else{
            self.strActionEmailId = @"";
            
        }
        
        if(_strActionEmailId && ![_strActionEmailId isEqualToString:@""])
        {
            self.isActionURL = NO;
        }
        else{
            self.isActionURL = YES;
        }
        
        
        if([[dataSourceDic allKeys] containsObject:@"ActionButton"])
        {
            NSString *str = [dataSourceDic valueForKey:@"ActionButton"];
            if(str && [str isEqualToString:@"Custom"] )
            {
                if([[dataSourceDic allKeys] containsObject:@"CustomActionButtonTitle"])
                {
                    NSString *strActionButton = [dataSourceDic valueForKey:@"CustomActionButtonTitle"];
                    if(strActionButton && ![strActionButton isEqualToString:@""])
                    {
                        [btnAction setTitle:strActionButton forState:UIControlStateNormal];
                    }
                    else{
                        [btnAction setTitle:str forState:UIControlStateNormal];
                    }
                }
                else{
                    [btnAction setTitle:str forState:UIControlStateNormal];
                }
                btnAction.hidden = NO;
                btnAction.frame = CGRectMake(btnAction.frame.origin.x, lastRect.origin.y+lastHighHeight+spacing, btnAction.frame.size.width, 40);
            }
            else if(str && ![str isEqualToString:@""]){
                [btnAction setTitle:str forState:UIControlStateNormal];
                btnAction.hidden = NO;
                btnAction.frame = CGRectMake(btnAction.frame.origin.x, lastRect.origin.y+lastHighHeight+spacing, btnAction.frame.size.width, 40);
            }
            else{
                [btnAction setHidden:YES];
            }
        }
        [btnAction addTarget:self action:@selector(actionButton:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //3
        if (btnAction.hidden) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(btnAction.frame.origin.x, lastRect.origin.y+lastHighHeight+spacing, btnAction.frame.size.width, 40);
            lastHighHeight = lastRect.size.height;
        }
        btnAction.frame = lastRect;
        
        
        {
            if (titleText == nil) {
                lastRect.size.height = 0.0;
            }
            else {
                lastRect = CGRectMake(spacing, lastRect.origin.y + lastHighHeight+2* spacing, allWidth, imgSeperator.frame.size.height);
            }
            imgSeperator.frame = lastRect;
        }
        
        
        //Organization text
        {
            lastRect = imgSeperator.frame;
            lastHighHeight = lastRect.size.height;
            
            if (descText == nil) {
                lastRect.size.height = 0.0;
            }
            else{
                lastRect = CGRectMake(10, lastRect.origin.y + lastHighHeight + spacing, 300, txtDescription.frame.size.height);
            }
            
            txtDescription.frame = lastRect;
        }
        
        
        //Finally set scrolling
        myScrollView.contentSize = CGSizeMake(myScrollView.frame.size.width, txtDescription.frame.origin.y + txtDescription.frame.size.height + spacing );
        
        
        
        //Configure FB Like View
        //Call, location floating right
        {
            //callLocWebView.frame = CGRectMake(callLocWebView.frame.origin.x , newsTitle.frame.origin.y + newsTitle.frame.size.height, callLocWebView.frame.size.width , callLocWebView.frame.size.height );
        }
    }
}

- (void) actionButton:(id) sender
{
    if(_strActionEmailId && ![_strActionEmailId isEqualToString:@""])
    {
        [self actMailSending1:nil];
    }
    else if(_strActionURL && ![_strActionURL isEqualToString:@""])
    {
        if(![_strActionURL isEqualToString:@""])
        {
            // NSString *urlStr = [@"http://" stringByAppendingString:_websiteURL];
            
            NSURL *url = [NSURL URLWithString:_strActionURL];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
            else{
                [AppDelegate showAlert:[NSString stringWithFormat:@"Website not available"]];
            }
            
            
           /* ProductsWebView *webView = [[[ProductsWebView alloc] initWithNibName:@"ProductsWebView" bundle:nil] autorelease];
            [self.navigationController pushViewController:webView animated:YES];*/
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"website not configured"]];
        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    isViewLoaded = YES;
    
    self.appType = APP_PRODUCTS;
    
    [self hideEverything:YES];
    [self.view bringSubviewToFront:btnInfo];
    
    [txtDescription setTextColor:[UIColor darkGrayColor]];
    
    [lblTitle sizeLabelForTitle];
    


}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveProductDetailsNotification:) name:@"getProductsDetailNotification"
                                               object:nil];

    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}
- (void)receiveProductDetailsNotification:(NSNotification *) notification
{
    //self.headerObject.headerLabel.text = [self.newsListArray valueForKey:@"CategoryName"];
    //isViewLoaded = YES;
    [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:NO];
    
    [self setDetailDataDictionary:[[AppDelegate shareddelegate].volunterDict mutableCopy]];
    
}

- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void) hideEverything:(BOOL) boolCheck
{
    productImg.hidden = boolCheck;
    lblTitle.hidden = boolCheck;
//    lblPrice.hidden = boolCheck;
    btnAction.hidden = boolCheck;
    imgSeperator.hidden = boolCheck;
    txtDescription.hidden = boolCheck;
    btnInfo.hidden = boolCheck;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)resizeToFit:(UITextView *)txtView
{
    
    /* float height = [self expectedHeight:txtView];
     CGRect newFrame = [txtView frame];
     newFrame.size.height = height;
     [txtView setFrame:newFrame];
     return newFrame.origin.y + newFrame.size.height;*/
    
    CGRect frame = txtView.frame;
    frame.size.height = txtView.contentSize.height;
    txtView.frame = frame;
}


#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Lists"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
//        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
//    NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}



- (IBAction)actInfo:(id)sender
{
    
    if(_strEmailId && ![_strEmailId isEqualToString:@""] && _strPhoneNumber && ![_strPhoneNumber isEqualToString:@""])
    {
        UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Contact" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Contact Us",@"Call Us",nil] autorelease];
        [actinsheet showInView:self.view];
    } else if(_strEmailId && ![_strEmailId isEqualToString:@""])
    {
        UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Contact" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Contact Us",nil] autorelease];
        [actinsheet showInView:self.view];
    } else if(_strPhoneNumber && ![_strPhoneNumber isEqualToString:@""])
    {
        UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Contact" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call Us",nil] autorelease];
        [actinsheet showInView:self.view];
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *strAction = nil;
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    
    if(_strEmailId && ![_strEmailId isEqualToString:@""] && _strPhoneNumber && ![_strPhoneNumber isEqualToString:@""])
    {
        switch (buttonIndex) {
                
            case 0:
            {
                strAction = @"ContactUs";
                [self sendMail:nil];
            }
                break;
            case 1:
            {
                strAction = @"Call";
                [self actCall:nil];
            }
                break;
            case 2:
            {
                strAction = @"Canceled";
            }
                break;
            default:
                break;
        }
    } else if(_strEmailId && ![_strEmailId isEqualToString:@""])
    {
        switch (buttonIndex) {
                
            case 0:
            {
                strAction = @"ContactUs";
                [self sendMail:nil];
            }
                break;
                
            case 1:
            {
                strAction = @"Canceled";
            }
                break;
            default:
                break;
        }
    } else if(_strPhoneNumber && ![_strPhoneNumber isEqualToString:@""])
    {
        switch (buttonIndex) {
                
                
            case 0:
            {
                strAction = @"Call";
                [self actCall:nil];
            }
                break;
            case 1:
            {
                strAction = @"Canceled";
            }
                break;
            default:
                break;
        }
    }
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [service runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId longValue] action:strAction deviceType:@"iOS"];
    }
    else {
        [service runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId longValue] action:strAction deviceType:@"iOS"];
    }
    
}


- (IBAction)actCall:(id)sender
{
    NSString *number = [self checkPhoneNumber:_strPhoneNumber];
    //_eventObject.phone;
    
    if(number && ![number isEqualToString:@""])
    {
        NSURL *callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", number]];
        NSLog(@"The number is : %@",[self checkPhoneNumber:number]);
        if ([[UIApplication sharedApplication] canOpenURL:callURL]) {
            [[UIApplication sharedApplication] openURL:callURL];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to dial the number. Please dial the number '%@' manually.", _strPhoneNumber ]];
        }
    }
    else{
        [AppDelegate showAlert:@"Unable to dial the number."];
    }
}

- (NSString *)checkPhoneNumber: (NSString *)number
{
    NSString *str = [[[[number stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

- (void) sendMail:(id)sender
{
    [self mailSending:nil withMail:_strEmailId];
    
}
- (IBAction)actMailSending1:(id)sender
{
    int widgetId;
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [self.tileInfo.widget.widgetID intValue];
    }
    
     [self insertInfoVCAnimated];
    
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.tileInfo.widget)
    {
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:[self.itemId longValue] widgetType:@"Product-ContactUs" deviceType:@"iOS"];
    }
    else{
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId longValue] widgetType:@"Product-ContactUs" deviceType:@"iOS"];
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    [_infoCollVC.view removeFromSuperview];
    self.infoCollVC = nil;
}

-(void)infoCancel:(id)sender{
    //    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    //    [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Cancelled" deviceType:@"iOS"];
    
    
    int widgetId;
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [self.tileInfo.widget.widgetID intValue];
    }
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.tileInfo.widget)
    {
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:[self.itemId longValue] widgetType:@"Product-ContactUs" deviceType:@"iOS"];
    }
    else{
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId longValue] widgetType:@"Product-ContactUs" deviceType:@"iOS"];
    }
    [self removeInfoVCAnimated];
    
}

-(void)infoProceed:(id)sender firstName:(NSString*)firstName lastName:(NSString*)lastName gender:(NSString*)gender mobile:(NSString*)mobile  email:(NSString*)email{
    [self removeInfoVCAnimated];
    NSString *body = [self selfBodyContent:firstName lastname:lastName email:email mobile:mobile];
    [self mailSending:body withMail:_strActionEmailId];
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.tileInfo.widget)
    {
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:[self.itemId longValue] widgetType:@"Product-ContactUs-Continue" deviceType:@"iOS"];
    }
    else{
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId longValue] widgetType:@"Product-ContactUs-Continue" deviceType:@"iOS"];
    }
}

- (NSString *) selfBodyContent:(NSString *)firstName lastname:(NSString *)lastName email:(NSString *)email mobile:(NSString *)mobile
{
    NSMutableString *strContent = nil;
    strContent = [NSMutableString stringWithFormat:@"%@",@"Please contact me at your convenience. Here is my contact information:<br/>"];
    //if(email)
    {
        [strContent appendFormat:@"Name: %@ %@<br/>",firstName, lastName];
        [strContent appendFormat:@"Phone: %@<br/>", mobile];
        [strContent appendFormat:@"email: %@<br/>", email];
    }
    return strContent;
    
}

- (void) mailSending:(NSString *)body withMail:(NSString *)email
{
    if(email)
    {
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
            mfViewController.mailComposeDelegate = self;
            [[mfViewController navigationBar] setTintColor:[UIColor blackColor]];
            
            [mfViewController setSubject:[NSString stringWithFormat:@"Please contact me."]];
            [mfViewController setMessageBody:body isHTML:YES];
            [mfViewController setToRecipients:[NSArray arrayWithObject:email]];
            mfViewController.navigationItem.title = @"Mail";
//            [self presentModalViewController:mfViewController animated:YES];
            [self presentViewController:mfViewController animated:YES completion:nil];
            [mfViewController release];
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:@"Your device is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
            [alert release];
        }
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:@"email not available" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
}

#pragma mark MFMailComposeViewControllerDelegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    int widgetId;
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [self.tileInfo.widget.widgetID intValue];
    }
    
    
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message Status" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	if(self.itemId && self.tileInfo)
    {
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        
        switch (result) {
            case MFMailComposeResultCancelled:{
                //alert.message = @"Canceled";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[self.itemId longValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultSaved:
            {
                //alert.message = @"Saved";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[self.itemId longValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultSent:
            {
                //alert.message = @"Sent";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[self.itemId longValue]  actionType:@"Email" action:@"" status:@"Shared" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultFailed:
            {
                //alert.message = @"Message Failed";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[self.itemId longValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            default:
                //alert.message = @"Message Not Sent";
                break;
        }
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}



-(void)insertInfoVCAnimated{
    self.infoCollVC = [[[InfoCollectorVC alloc] initWithNibName:@"InfoCollectorVC" bundle:nil] autorelease];
    _infoCollVC.delegate = self;
    [self.view addSubview:_infoCollVC.view];
    
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.2],
                             [NSNumber numberWithFloat:1.2],
                             [NSNumber numberWithFloat:.9],
                             [NSNumber numberWithFloat:1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.5],
                               [NSNumber numberWithFloat:0.7],
                               [NSNumber numberWithFloat:1.0],
                               nil];
    //popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"transform.scale"];
}

-(void)removeInfoVCAnimated{
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:1.0],
                             [NSNumber numberWithFloat:0.1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:1.0],
                               nil];
    popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"transform.scale"];
}
- (void)dealloc {
    [_bannerImg release];
    [_bannerCancelBtn release];
    [super dealloc];
}

@end
