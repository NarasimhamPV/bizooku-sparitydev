//
//  ProductsVC.h
//  Exceeding
//
//  Created by Rajesh Kumar on 12/12/13.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "TableObject.h"
#import "UILabel+Exceedings.h"
#import "InfoCollectorVC.h"
#import <MessageUI/MessageUI.h>
#import "BigBanners.h"


@interface ProductsVC : AppMainDetailVC<UIActionSheetDelegate, InfoCollectorDelegate, MFMailComposeViewControllerDelegate,BigBannersDelegate>
{
    IBOutlet UIImageView *productImg;
    IBOutlet UIImageView *imgSeperator;
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblPrice;
    IBOutlet UIButton *btnAction;
    IBOutlet UITextView *txtDescription;
    IBOutlet UIScrollView *myScrollView;
    //Decide to call webserview after viewDidAppear.
    BOOL isViewLoaded;
    BOOL adsremoved;
    
    
    
}


@property (retain, nonatomic) IBOutlet UIButton *btnInfo;
@property (nonatomic, assign) TableObject *prodObject;
@property (nonatomic, retain) NSString *strPhoneNumber;
@property (nonatomic, retain) NSString *strEmailId;

@property (nonatomic, retain) NSString *strActionEmailId;
@property (nonatomic, retain) NSString *strActionURL;

@property (nonatomic, readwrite) BOOL isActionURL;
@property (nonatomic, strong) NSDictionary *listBannersDict;



@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (nonatomic, strong) BigBanners *bigBanner;

- (IBAction)actInfo:(id)sender;
- (IBAction)actCall:(id)sender;
- (IBAction)actMailSending:(id)sender;


@end
