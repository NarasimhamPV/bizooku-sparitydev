//
//  NewsListCell.h
//  Exceeding
//
//  Created by VENKATALAKSHMI on 31/07/14.
//
//

#import <UIKit/UIKit.h>

@interface NewsListCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UIImageView *listImage;

@end
