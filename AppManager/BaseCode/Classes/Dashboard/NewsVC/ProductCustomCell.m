//
//  ProductCustomCell.m
//  Exceeding
//
//  Created by VENKATALAKSHMI on 01/08/14.
//
//

#import "ProductCustomCell.h"

@implementation ProductCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaCondensed" size:18];
    self.desLbl.font = [UIFont fontWithName:@"HelveticaCondensed" size:18];
    self.seeDetailsLbl.font = [UIFont fontWithName:@"HelveticaCondensed" size:18];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_titleLabel release];
    [_productImg release];
    [_desLbl release];
    [super dealloc];
}
@end
