//
//  NewsDetailVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "InfoCollectorVC.h"
#import "BigBanners.h"
@interface FundVolDetailVC : AppMainDetailVC <UIWebViewDelegate, InfoCollectorDelegate, UIAlertViewDelegate,BigBannersDelegate>
{
    IBOutlet UIImageView *newsImg;
    IBOutlet UILabel *newsTitle;
    IBOutlet UIImageView *imgSeperator;
    
    IBOutlet UITextView *newsDetails;
    IBOutlet UIScrollView *myScrollView;
    
    IBOutlet UIProgressView *myProgressBar;
    IBOutlet UILabel *labelTarget;
    IBOutlet UILabel *labelRaised;
    IBOutlet UIView *giveView;
    IBOutlet UIView *mainTitleView;
    BOOL isViewLoaded;
    IBOutlet UIActivityIndicatorView *actIndicator;
    IBOutlet UIButton *giveJoinBtn;
    IBOutlet UILabel *raisedStatic;
    IBOutlet UILabel *targetStatic;
    BOOL adsremoved;
    NSInteger fundVolImgHeight;
}
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
@property (nonatomic, strong) NSMutableArray *bannerURLS;
//@property (retain, nonatomic) IBOutlet UIView *bannerView;
//@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (strong, nonatomic) NSDictionary *fundBannersDict;
@property (nonatomic, strong) BigBanners *bigBanner;
- (void)startShowingBanners:(NSString *)bannerURL;
- (void)reloadUsingWebService;

- (IBAction)bannerCancelBtn:(id)sender;
- (IBAction)fundBannerWebBtn:(id)sender;
- (IBAction)giveAction:(id)sender;

@end
