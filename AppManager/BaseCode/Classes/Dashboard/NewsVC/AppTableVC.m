//
//  AppTableVC.m
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

//AD8A2E0A-8412-41B1-B5F6-10D3520AF408


#import "AppTableVC.h"
#import "NewsDetailVC.h"
#import "EventDetailVC.h"
#import "FundVolDetailVC.h"
#import "SPCouponDetailsViewController.h"

#import "SDZEXWidgetServiceExample.h"
#import "UIImageView+WebCache.h"
#import "Utils.h"
#import "LocationDetailVC.h"
#import "AppDelegate.h"
#import "SocialActionSheet.h"
#import "AppHeaderBar.h"
#import "PodcastItem.h"
#import <MediaPlayer/MediaPlayer.h>
#import "YouTubeVC.h"
#import "TableObject.h"
#import "NSDateFormatter+Exceedings.h"
#import "NSString+Exceedings.h"
#import "RSSParser.h"
#import "PhoneNumberFormatter.h"
#import "ProductsVC.h"

#import "SPCouponsListViewController.h"

#import "SDZEXAnalyticsServiceExample.h"

#import "CustomBannerView.h"
#import "WebViewController.h"
#import "ProductsListVC.h"
#import "NewListVC.h"
#import "EventsListVC.h"
#import "SPSingletonClass.h"
#import "SPEventCalendarVC.h"
#import "SPVideoListVC.h"
#import "SDZEXVideoServiceExample.h"

@interface NSArray (Exceedings)

-(NSArray*)tableObjects;
-(NSArray*)sortedArrayFromLocation:(CLLocation*)location ascending:(BOOL)ascending;



@end


@implementation NSArray (Exceedings)




-(NSArray*)tableObjects{
    
    NSMutableArray *mutableArray = [NSMutableArray array];
    for (NSDictionary *dic in self) {
        
        
        TableObject *obj = [[[TableObject alloc] init] autorelease];
        if([[dic allKeys] containsObject:@"Name"])
        {
            obj.name = [dic valueForKey:@"Name"];
        }
        else if([[dic allKeys] containsObject:@"PageTitle"]){
            obj.name = [dic valueForKey:@"PageTitle"];
        }
        
        obj.locationName = [dic valueForKey:@"LocationName"];
        obj.desc = [dic valueForKey:@"Description"];
        obj.zip = [dic valueForKey:@"Zip"];
        obj.state = [dic valueForKey:@"State"];
        obj.titleText = [dic valueForKey:@"Title"];
        obj.newsID = [NSString stringWithFormat:@"%@", [dic valueForKey:@"NewsId"]] ;
        obj.eventID = [NSString stringWithFormat:@"%@", [dic valueForKey:@"EventId"]] ;
        // rajesh..
        obj.productId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"ProductID"]];
        
        obj.locId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"LocationId"]];
        NSDateFormatter *format = [NSDateFormatter exceedingEventsPullTimeFormatter];
        
        obj.startTime = [format dateFromString:[dic valueForKey:@"StartTime"]];
        obj.price = [dic valueForKey:@"Price"];
        NSLog(@"The current date is: %@", [format stringFromDate:[NSDate date]]);
        
        obj.endTime = [format dateFromString:[dic valueForKey:@"EndTime"]];
        
        obj.startTimeStr = [[[[dic valueForKey:@"StartTime"]
                              stringByReplacingOccurrencesOfString:@" PM" withString:@"pm"]
                             stringByReplacingOccurrencesOfString:@" AM" withString:@"am"]
                            stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        obj.endTimeStr = [[[[dic valueForKey:@"EndTime"]
                            stringByReplacingOccurrencesOfString:@" PM" withString:@"pm"]
                           stringByReplacingOccurrencesOfString:@" AM" withString:@"am"]
                          stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        obj.fbPostID = [dic valueForKey:@"FBPostId"];
        
        NSString *image = [dic valueForKey:@"Image"];
        
        if (image && image.length) {
            obj.image = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [dic valueForKey:@"Image"]];
        }
        
        obj.location =  [[[CLLocation alloc] initWithLatitude:[[dic valueForKey:@"Lattitude"] doubleValue] longitude:[[dic valueForKey:@"Longitude"] doubleValue]] autorelease];
        obj.lattitude = [dic valueForKey:@"Lattitude"];
        obj.longitude = [dic valueForKey:@"Longitude"];
        
        obj.address = [dic valueForKey:@"Address"];
        
        obj.city = [dic valueForKey:@"City"];
        obj.fax = [dic valueForKey:@"Fax"];
        obj.primaryEmail = [dic valueForKey:@"PrimaryEmailAddress"];
        
        PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
        obj.phone = /*[dic valueForKey:@"Phone"]; */ [phoneNumberFormatter format:[dic valueForKey:@"Phone"] withLocale:@"us"];
        obj.additonalInfo = [dic valueForKey:@"AdditonalInfo"];
        obj.website = [dic valueForKey:@"Website"];
        
        
        NSString *startDate = [dic valueForKey:@"StartDate"];
        obj.startDate = [startDate exceedingsDateFromServerNewsString];
        
        NSString *endDate = [dic valueForKey:@"EndDate"];
        obj.endDate = [endDate exceedingsDateFromServerNewsString];
        
        NSString *date = [dic valueForKey:@"CreatedDate"];
        if(date)
        {
            obj.date = [date exceedingsDateFromServerNewsString];
        }
        [mutableArray addObject:obj];
    }
    
    return [NSArray arrayWithArray:mutableArray];
}

-(NSArray*)sortedArrayFromLocation:(CLLocation*)location ascending:(BOOL)ascending{
    //NSMutableArray *newArray = [NSMutableArray arrayWithArray:self];
    
    for (TableObject *obj in self) {
        obj.distance = [obj.location distanceFromLocation:location];
        
    }
    
    NSSortDescriptor * sortByFrequency = [[[NSSortDescriptor alloc] initWithKey:@"distance" ascending:ascending] autorelease];
    return [self sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortByFrequency]];
    return [NSArray arrayWithArray:self] ;
}

-(NSArray*)sortedArrayForEventsFromLocation:(CLLocation*)location ascending:(BOOL)ascending{
    //NSMutableArray *newArray = [NSMutableArray arrayWithArray:self];
    
    for (TableObject *obj in self) {
        CLLocation *locEvent = [[CLLocation alloc] initWithLatitude:[obj.lattitude doubleValue] longitude:[obj.longitude doubleValue]];
        obj.distance = [locEvent distanceFromLocation:location];
        
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
    NSSortDescriptor *sortTimeDesc = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES];
    
    NSSortDescriptor * sortByFrequency = [[[NSSortDescriptor alloc] initWithKey:@"distance" ascending:ascending] autorelease];
    
    
    NSArray *arrSorted = [self sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortByFrequency,sortTimeDesc, nil]];
    
    
    //  self = [NSArray arrayWithArray:arrSorted];
    
    return [arrSorted sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    return [NSArray arrayWithArray:self] ;
}


@end



@interface AppTableVC ()
@property (retain, nonatomic) UIWebView *myWebView;

@property (retain, nonatomic) NSMutableArray *videoArray;
@property (retain, nonatomic) NSMutableArray *audioArray;
@property (retain, nonatomic) NSMutableArray *youtubeArray;

@property (readwrite, nonatomic) AppSubTab appSubType;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation AppTableVC
@synthesize activityIndicator;
@synthesize searchBar = _searchBar;

@synthesize myWebView;
@synthesize videoArray = _videoArray;
@synthesize audioArray = _audioArray;
@synthesize youtubeArray = _youtubeArray;
@synthesize appSubType = _appSubType;
@synthesize haveMediaContent = _haveMediaContent;

@synthesize pullToReloadHeaderView = _pullToReloadHeaderView;

@synthesize clController = _clController;


#pragma mark - Activity

-(void)startFrontActivity{
    [self.view bringSubviewToFront:activityIndicator];
    [activityIndicator startAnimating];
}


#pragma mark - Data reciever
-(void)podcaseFeedsRecieved:(NSArray*)array{
    NSLog(@"podcaseFeedsRecieved");
    self.dataSourceDic = (NSDictionary*)array;//Fake it
    //[tabNews reloadData];
}

-(void)podcastFeedsFailed:(NSError*)error{
    //self.dataSourceDic = nil;
}

#pragma mark - Btn action
-(IBAction)headerBtnAction:(UIButton*)sender{
    [super headerBtnAction:sender];
    
    
    if (sender == self.headerObject.btnAdd) {
        //
    }
    else if (sender == self.headerObject.btnClose) {
        //
    }
    else if (sender == self.headerObject.btnOptions) {
        //
    }
    else if (sender == self.headerObject.btnSearch) {
        [self.headerObject.mainHeader addSubview:_searchBar];
        [_searchBar becomeFirstResponder];
        tabNews.frame = CGRectMake(tabNews.frame.origin.x, tabNews.frame.origin.y, tabNews.frame.size.width, 200);
        
        
    }
    else if(sender == self.headerObject.btnLocation)
    {
        // [AppDelegate showAlert:@"Loation sorting"];
        
        
        if(!self.isLocationSortEnabled)
        {
            [self.headerObject.btnLocation setSelected:YES];
            boolUpdate = NO;
            self.isLocationSortEnabled = YES;
            if(_clController)
            {
                self.clController = nil;
            }
            self.clController = [[MyCLController alloc] init];
            
            _clController.delegate = self;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                [_clController.locationManager requestWhenInUseAuthorization];
            }
            
            if ([_clController.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_clController.locationManager requestAlwaysAuthorization];
            }
            
            [_clController.locationManager startUpdatingLocation];
        }
        else{
            self.isLocationSortEnabled = NO;
            [self.headerObject.btnLocation setSelected:NO];
            
            NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
            NSSortDescriptor *sortTimeDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES];
            self.searchDataSourceArray = [NSMutableArray arrayWithArray:[self.searchDataSourceArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor, sortTimeDescriptor,nil]]];
            [tabNews reloadData];
        }
    }
}





#pragma mark - Btn action
-(IBAction)headerSecondBtnAction:(UIButton *)sender{
    // [super headerSecondBtnAction:sender];
    
    
    if (sender == self.headerObject.btnAdd) {
        //
    }
    else if (sender == self.headerObject.btnClose) {
        //
    }
    else if (sender == self.headerObject.btnOptions) {
        //
    }
    else if (sender == self.headerObject.btnSearch) {
        [self.headerObject.mainHeader addSubview:_searchBar];
        [_searchBar becomeFirstResponder];
        tabNews.frame = CGRectMake(tabNews.frame.origin.x, tabNews.frame.origin.y, tabNews.frame.size.width, self.view.frame.size.height - 150-44.0);
    }
    else if(sender == self.headerObject.btnLocation)
    {
        //[AppDelegate showAlert:@"The thing"];
    }
}
#pragma mark - Search Bar

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    @synchronized(self){
        NSLog(@"self.dataSourceArray %@",self.dataSourceArray);
        NSMutableArray *filteredArray = [NSMutableArray array];
        NSString *text = searchText;
        for (NSDictionary *dictionary in self.dataSourceArray) {
            if ([dictionary isKindOfClass:[NSDictionary class]]) {
                NSString *name = [dictionary valueForKey:@"Name"];
                NSString *locName = [dictionary valueForKey:@"LocationName"];
                NSString *desc = [dictionary valueForKey:@"Description"];
                NSString *titleText = [dictionary valueForKey:@"Title"];
                if (self.appType == APP_PRODUCTS||self.appType == APP_VIDEO) {
                    NSString *categoryName = [dictionary valueForKey:@"CategoryName"];
                    
                    if (categoryName && ([categoryName rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                        [filteredArray addObject:dictionary];
                    }
                }
                
                //CategoryName
                
                if (name && ([name rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                
                else if (locName && ([locName rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                else if (titleText && ([titleText rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                else if (desc && ([desc rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
            }
            else if ([dictionary isKindOfClass:[TableObject class]]) {
                TableObject *item = (TableObject*)dictionary;
                if (item.titleText && ([item.titleText rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                else if (item.desc && ([item.desc rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                else if (item.name && ([item.name rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                else if (item.locationName && ([item.locationName rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                else if (item.eventCategoryName && ([item.eventCategoryName rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
            }
            else if ([dictionary isKindOfClass:[PodcastItem class]]) {
                PodcastItem *item = (PodcastItem*)dictionary;
                if (item.titleText && ([item.titleText rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
                else if (item.desc && ([item.desc rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
            }
        }
        self.searchDataSourceArray = filteredArray;
        
        if ([searchText length] == 0) {
            self.searchDataSourceArray = [NSMutableArray arrayWithArray:self.dataSourceArray];
        }
        
        [tabNews reloadData];
    }
    
}


#pragma mark Pull Down....

-(void)pulltoReleaseViewOverView:(UIView*)view
{
    UIScrollView *tempScroll = (UIScrollView*)view;
    tempScroll.delegate = self;
    
    if (_pullToReloadHeaderView)
    {
        [_pullToReloadHeaderView removeFromSuperview],_pullToReloadHeaderView = nil;
        
        _pullToReloadHeaderView = [[UIPullToReloadHeaderView alloc] initWithFrame: CGRectMake(0.0, 0.0f - view.bounds.size.height, view.frame.size.width, view.bounds.size.height)];
        [_pullToReloadHeaderView setBackgroundColor:[UIColor whiteColor]];
        [view addSubview:_pullToReloadHeaderView];
    }
    else
    {
        _pullToReloadHeaderView = [[UIPullToReloadHeaderView alloc] initWithFrame: CGRectMake(0.0, 0.0f - view.bounds.size.height, view.frame.size.width, view.bounds.size.height)];
        [_pullToReloadHeaderView setBackgroundColor:[UIColor clearColor]];
        [view addSubview:_pullToReloadHeaderView];
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    [searchBar resignFirstResponder];
    [searchBar removeFromSuperview];
    
    tabNews.frame = CGRectMake(tabNews.frame.origin.x, tabNews.frame.origin.y, tabNews.frame.size.width, self.view.frame.size.height-44);
    self.searchDataSourceArray = [NSMutableArray arrayWithArray:self.dataSourceArray];
    
    
    [tabNews reloadData];
}


#pragma mark UIScrollViewDelegates starts

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(self.appType == APP_LOCATIONS)
    {
        if ([_pullToReloadHeaderView status] == kPullStatusLoading) return;
        checkForRefresh = YES;  //  only check offset when dragging
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(self.appType == APP_LOCATIONS)
    {
        if ([_pullToReloadHeaderView status] == kPullStatusLoading) return;
        
        if (checkForRefresh) {
            if (scrollView.contentOffset.y > -kPullDownToReloadToggleHeight && scrollView.contentOffset.y < 0.0f) {
                [_pullToReloadHeaderView setStatus:kPullStatusPullDownToReload animated:YES];
                
                
            } else if (scrollView.contentOffset.y < -kPullDownToReloadToggleHeight) {
                [_pullToReloadHeaderView setStatus:kPullStatusReleaseToReload animated:YES];
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(self.appType == APP_LOCATIONS)
    {
        if ([_pullToReloadHeaderView status] == kPullStatusLoading) return;
        
        if ([_pullToReloadHeaderView status]==kPullStatusReleaseToReload) {
            
            /*if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
             {
             
             }
             else
             {
             [_pullToReloadHeaderView startReloading:self.tableMain animated:YES];
             }*/
            [self.searchDataSourceArray removeAllObjects];
            page = 1;
            [self reloadUsingWebService];
            //[_pullToReloadHeaderView startReloading:tabNews animated:YES];
            [self pullDownToReloadAction];
        }
        checkForRefresh = NO;
    }
}


#pragma mark UIScrollViewDelegates ends

-(void) pullDownToReloadActionFinished
{
    // add one data on top
    [self.pullToReloadHeaderView setLastUpdatedDate: [NSDate date]];
    [self.pullToReloadHeaderView finishReloading:tabNews animated:YES];
    
}

-(void) pullDownToReloadAction
{
    // finish reload after three seconds
    [self performSelector:@selector(pullDownToReloadActionFinished) withObject:nil afterDelay: 1.0f];
}

#pragma mark Location delegate methods

#pragma mark Current Location Delegates

- (void)locationError:(NSError *)error {
    NSLog(@"locationError :::  description is: %@",error.localizedDescription);
    if(self.appType == APP_EVENTS)
    {
        self.isLocationSortEnabled = NO;
        [self.headerObject.btnLocation setSelected:NO];
        
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
        NSSortDescriptor *sortTimeDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES];
        self.searchDataSourceArray = [NSMutableArray arrayWithArray:[self.searchDataSourceArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortTimeDescriptor,nil]]];
        [tabNews reloadData];
        [AppDelegate showAlert:@"Please enable Location services in your device settings."];
    }
    
    
}

- (void)locationUpdate:(CLLocation *)location {
    if(!boolUpdate)
    {
        [AppDelegate shareddelegate].currentLocation = location;
        
        boolUpdate = YES;
        [_clController.locationManager stopUpdatingLocation];
        [super setDataSourceArray:self.searchDataSourceArray];
        
        self.searchDataSourceArray = [NSMutableArray arrayWithArray:[self.searchDataSourceArray sortedArrayForEventsFromLocation:location ascending:YES]];
        
        [tabNews reloadData];
    }
    
}





#pragma mark - Table Cell


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    //Import component
    UIImageView *imgShow = (UIImageView *)[cell viewWithTag:1];
    
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:2];
    
    UILabel *lblDescrip = (UILabel *)[cell viewWithTag:3];
    
    UILabel *lblDescrip2 = (UILabel *)[cell viewWithTag:4];
    
    UIProgressView *progBar = (UIProgressView *)[cell viewWithTag:5];
    
    UIImage *placeholderImage = nil;
    if ([self.searchDataSourceArray count] ){
    NSDictionary *dictionary = [self.searchDataSourceArray objectAtIndex:indexPath.row];
    //NSLog(@"Veeru dictionary %@",dictionary);
    
    if (dictionary && [dictionary isKindOfClass:[NSDictionary class]]) {
        
        if (self.appType == APP_LOCATIONS) {
            
            NSMutableString *locationStr = [[[NSMutableString alloc] initWithString:[dictionary valueForKey:@"State"]] autorelease];
            NSString *zipCode = [dictionary valueForKey:@"Zip"];
            if (zipCode && [zipCode length]) {
                [locationStr appendFormat:@" - %@", zipCode];
            }
            lblTitle.text = [dictionary valueForKey:@"LocationName"];
            lblDescrip.text = [NSString stringWithFormat:@"%d kms", indexPath.row*10 ] ;
        }
        else if (self.appType == APP_FUNDRAISING) {
            lblTitle.text = [dictionary valueForKey:@"Name"];
            
            //Amount Collected
            NSString *amountCollected = [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"AmountCollected"]];
            lblDescrip.text = [NSString stringWithFormat:@"%@", [amountCollected millionsFormatDollarString]];
            
            //Goal
            NSString *goal = [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"Goal"]];
            lblDescrip2.text = [NSString stringWithFormat:@"%@", [goal millionsFormatDollarString]];
            
            //Progress
            progBar.progress = [[dictionary valueForKey:@"AmountCollected"] doubleValue]/[[dictionary valueForKey:@"Goal"] doubleValue];
            
            placeholderImage = [UIImage imageNamed:@"fundDefault"];
        }
        
        
        
        else if (self.appType == APP_VOLUNTEER) {
            NSLog(@"dictionary %@",dictionary);
            
            lblTitle.text = [dictionary valueForKey:@"Name"];
            lblDescrip.text = [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"Joined"]];
            lblDescrip2.text = [NSString stringWithFormat:@"%@", [dictionary valueForKey:@"Need"]];
            progBar.progress = [[dictionary valueForKey:@"Joined"] doubleValue]/[[dictionary valueForKey:@"Need"] doubleValue];
            
            placeholderImage = [UIImage imageNamed:@"volDefault"];
        }
        else if (self.appType == APP_NEWS) {
            self.headerObject.headerLabel.text = @"News";
            
            
            lblTitle.text = [dictionary valueForKey:@"CategoryName"];
            lblTitle.frame = CGRectMake(10, 10, 190, 25);
            
            if([[dictionary valueForKey:@"Price"] intValue] == 0)
            {
                lblDescrip.hidden = YES;
            }
            else{
                lblDescrip.hidden = NO;
            }
            lblDescrip.text = [NSString stringWithFormat:@"$%@",[dictionary valueForKey:@"Price"]];
        }
        else if (self.appType == APP_EVENTS) {
            
            placeholderImage = [UIImage imageNamed:@"eventsdefault"];
            
            NSString *strDate = [dictionary valueForKey:@"CreatedDate"];
            strDate = [strDate convertToUSDateString];
            
            lblTitle.text = [dictionary valueForKey:@"Title"];
            lblDescrip.text = [NSString stringWithFormat:@"%@ at %@", strDate, [dictionary valueForKey:@"StartTime"]];
            
            lblDescrip2.text = [dictionary valueForKey:@"LocationName"];
        }else if (self.appType == APP_VIDEO) {
            
            NSLog(@"dictionary %@",dictionary);
            
            lblTitle.frame = CGRectMake(10, 10, 190, 25);
            self.headerObject.headerLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"CategoryListTitle"];
            
            
            lblTitle.text = [dictionary valueForKey:@"CategoryName"];
        }
        else if (self.appType == APP_PRODUCTS) {
            placeholderImage = [UIImage imageNamed:@"productDefault"];
            self.headerObject.headerLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"CategoryListTitle"];
            
            lblTitle.text = [dictionary valueForKey:@"CategoryName"];
            
            if([[dictionary valueForKey:@"Price"] intValue] == 0)
            {
                lblDescrip.hidden = YES;
            }
            else{
                lblDescrip.hidden = NO;
            }
            lblDescrip.text = [NSString stringWithFormat:@"$%@",[dictionary valueForKey:@"Price"]];
            
        }
        
        else{
            lblTitle.text = [dictionary valueForKey:@"Title"];
            lblDescrip.text = [dictionary valueForKey:@"Description"];
        }
        
        NSString *strImgSource = ([[dictionary objectForKey:@"Image"] length])?[NSString stringWithFormat:@"%@%@", kWebServiceURL, [dictionary valueForKey:@"Image"]]:nil;
        
        [imgShow setImageWithURL:[NSURL URLWithString:strImgSource] placeholderImage:placeholderImage];
    }
    else if (dictionary && [dictionary isKindOfClass:[TableObject class]]) {
        TableObject *pdcItem = (TableObject*)dictionary;
        //        NSLog(@"Test %@",pdcItem);
        
        if (self.appType == APP_LOCATIONS) {
            
            lblTitle.text = pdcItem.locationName;
            lblDescrip.text = [NSString stringWithFormat:@"%.f Miles", pdcItem.distance/kMeterToMilesFactor];
        }
        else if (self.appType == APP_NEWS) {
            NSLog(@"Test %@",pdcItem);
            //            placeholderImage = [UIImage imageNamed:@"newsdefault"];
            
            //            lblTitle.text = pdcItem.titleText;
            lblDescrip.text = [pdcItem.startDate exceedingsDaysAgoString];
            NSLog(@"The news detail start Date : %@", pdcItem.startDate);
        }
        else if (self.appType == APP_EVENTS) {
            //self.headerObject.iconSeperator.hidden = YES;
            
            NSLog(@"pdcItem %@",pdcItem);
            //placeholderImage = [UIImage imageNamed:@"eventsdefault"];
            
            lblTitle.text = pdcItem.eventCategoryName;
            lblTitle.frame = CGRectMake(10, 10, 190, 25);
            
        }
        else if (self.appType == APP_PRODUCTS) {
            placeholderImage = [UIImage imageNamed:@"eventsdefault"];
            
            lblTitle.text = pdcItem.titleText;
            self.headerObject.headerLabel.text = @"Products";
            
            // NSDateFormatter *format = [NSDateFormatter exceedingEventsPullTimeFormatter];
            
            if([pdcItem.price intValue] == 0)
            {
                lblDescrip.hidden = YES;
            }
            else{
                lblDescrip.hidden = NO;
            }
            
            lblDescrip.text = [NSString stringWithFormat:@"$%@", pdcItem.price];
        }
        
        [imgShow setImageWithURL:[NSURL URLWithString:pdcItem.image] placeholderImage:placeholderImage];
        
    }
    else if (dictionary && [dictionary isKindOfClass:[PodcastItem class]]) {
        PodcastItem *pdcItem = (PodcastItem*)dictionary;
        
        lblTitle.text = pdcItem.titleText;
        lblDescrip.text = pdcItem.desc;
        lblDescrip2.text = [pdcItem.pubDate convertToUSDateString];
        
        UIImage *placeHolderimg = [UIImage imageNamed:@"cellPodcast"];
        if (self.selectedTab.tag == 1) {
            placeHolderimg = [UIImage imageNamed:@"cellVideo"];
        }
        else if (self.selectedTab.tag == 2) {
            placeHolderimg = [UIImage imageNamed:@"cellAudio"];
        }
        else if (self.selectedTab.tag == 0) {
            placeHolderimg = [UIImage imageNamed:@"cellYouTube"];
        }
        
        [imgShow setImageWithURL:[NSURL URLWithString:pdcItem.image] placeholderImage:placeHolderimg];
    }
    }
}



#pragma mark TableView Data Source and Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return [[self.fetchedResultsController sections] count];
    
    if(self.appType == APP_LOCATIONS)
    {
        [self pulltoReleaseViewOverView:tabNews];
    }
    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.appType == APP_FUNDRAISING || self.appType == APP_VOLUNTEER || self.appType == APP_COUPONS || self.appType == APP_COUPONSLIST || self.appType == APP_COUPONSDETAILS) {
        return 80;
    }
    else if(self.appType == APP_PRODUCTS)
    {
        return 60.0f;
    }
    
    return 50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // self.noDataAvailableLbl.hidden = YES;
    
    if(!_haveMediaContent && self.appType == APP_MEDIA)
    {
        if([self.subTabOptions containsObject:kAppNoContent])
        {
            return 0;
        }
        else{
            return 1;
        }
    }else if (self.appType == APP_LOCATIONS){
        return [self.searchDataSourceArray count]+1;
        
    }
    else{
        return [self.searchDataSourceArray count];
        
        
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Veeru %d row %d",[self.searchDataSourceArray count],[indexPath row]);
    if (self.appType == APP_LOCATIONS){
        if ([indexPath row] == [self.searchDataSourceArray count] ) {
            //loadMore cell loading in tableview
            static NSString *CellIdentifier1 = @"loadMore";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
                activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(84, 9, 32, 32)];
                activityIndicator.tag = 1002;
                [cell.contentView addSubview:activityIndicator];
                
                loadMoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(1, 10, 304, 30)];
                loadMoreLabel.tag = 1001;
                [cell.contentView setBackgroundColor:[UIColor colorWithRed:(153.0/255.0) green:(153.0/255.0) blue:(153.0/255.0) alpha:0]];
                
                [loadMoreLabel setTextColor:[UIColor grayColor]];
                [loadMoreLabel setHighlightedTextColor:[UIColor lightGrayColor]];
                [loadMoreLabel setTextAlignment:NSTextAlignmentCenter];
                [loadMoreLabel setBackgroundColor:[UIColor colorWithRed:(153.0/255.0) green:(153.0/255.0) blue:(153.0/255.0) alpha:0]];
                [loadMoreLabel setFont:[UIFont fontWithName:@"Gotham-Black" size:15]];
                
                [cell.contentView addSubview:loadMoreLabel];
                UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
                [v setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.5]];
                [cell.contentView addSubview:v];
                
            }
            activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:1002];
            [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            loadMoreLabel = (UILabel *)[cell.contentView viewWithTag:1001];
            loadMoreLabel.text = @"Loading...";
            [activityIndicator startAnimating];
//            if ([self.searchDataSourceArray count]>0)
//            {
//                loadMoreLabel.text = @"Loading...";
//                [activityIndicator startAnimating];
//            }
            
            
            if ([self.searchDataSourceArray count] == indexPath.row) {
                
                cell.userInteractionEnabled = NO;
            }
            if ([self.searchDataSourceArray count]){
                [self loadMore:indexPath];
            }
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
    }
    NSString *cellIdentifier = @"CustomerCellgaU";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        if (self.appType == APP_FUNDRAISING || self.appType == APP_VOLUNTEER) {
            [[NSBundle mainBundle] loadNibNamed:@"TableFundCell" owner:self options:nil];
        }
        else if (self.appType == APP_LOCATIONS) {
            [[NSBundle mainBundle] loadNibNamed:@"TableLocationCell" owner:self options:nil];
        }
        
        else if (self.appType == APP_PRODUCTS) {
            [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:self options:nil];
        }
        else {
            [[NSBundle mainBundle] loadNibNamed:@"TableCell" owner:self options:nil];
        }
        
        cell = feedCell;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellWhiteBg.png"]] autorelease];
    }
    
    if(_haveMediaContent && self.appType == APP_MEDIA)
    {
        [self configureCell:cell atIndexPath:indexPath];
    }
    else if(self.appType == APP_MEDIA)
    {
        UIImageView *imgView = (UIImageView *)[cell viewWithTag:5];
        
        [cell.textLabel setFont:[UIFont systemFontOfSize:16.0f]];
        //cell.textLabel.text = @"Currently there is no content available.";
        imgView.hidden = YES;
        
    }
    else
    {
        [self configureCell:cell atIndexPath:indexPath];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)moviePlaybackComplete:(NSNotification *)notification
{
    MPMoviePlayerController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayerController];
    
    [moviePlayerController.view removeFromSuperview];
    [moviePlayerController release];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.appType == APP_MEDIA){
        
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        
        PodcastItem *item = [self.searchDataSourceArray objectAtIndex:indexPath.row];
        if (item.url) {
            
            NSURL *fileURL    =   [NSURL URLWithString:item.url];
            
            if (self.appSubType == TAB_YOUTUBE) {
                
                YouTubeVC *yuTube = [[[YouTubeVC alloc]initWithNibName:@"YouTubeVC" bundle:nil] autorelease];
                yuTube.youtubeURL = fileURL;
                self.appDetailVC = yuTube;
                self.appDetailVC.appType = self.appType;//Set app type before detail data dictionary
                self.appDetailVC.detailDataDictionary = self.appDetailVC.detailDataObject = [self.searchDataSourceArray objectAtIndex:indexPath.row];
                self.appDetailVC.tileInfo = self.widgetTileInfo;
                if(!self.widgetTileInfo.widget)
                {
                    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:0 widgetType:@"Detail" deviceType:@"iOS"];
                }
                else{
                    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Detail" deviceType:@"iOS"];
                }
                [self.navigationController pushViewController:self.appDetailVC animated:YES];
                
            }
            else if (self.appSubType == TAB_AUDIO){
                MPMoviePlayerController *moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(moviePlaybackComplete:)
                                                             name:MPMoviePlayerPlaybackDidFinishNotification
                                                           object:moviePlayerController];
                
                [self.view addSubview:moviePlayerController.view];
                moviePlayerController.fullscreen = YES;
                [moviePlayerController play];
                //                [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Audio-Detail" deviceType:@"iOS"];
            }
            
            else if (self.appSubType == TAB_VIDEOS){
                MPMoviePlayerController *moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(moviePlaybackComplete:)
                                                             name:MPMoviePlayerPlaybackDidFinishNotification
                                                           object:moviePlayerController];
                
                [self.view addSubview:moviePlayerController.view];
                moviePlayerController.fullscreen = YES;
                [moviePlayerController play];
                if(!self.widgetTileInfo.widget)
                {
                    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId ] integerValue] widgetItemId:0 widgetType:@"Detail" deviceType:@"iOS"];
                }
                else{
                    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Detail" deviceType:@"iOS"];
                }
            }
            
            
        }
        else {
            [AppDelegate showAlert:@"Podcast URL not found"];
        }
    }
    else if (self.appType == APP_LOCATIONS) {
        
        LocationDetailVC *locDetail = [[[LocationDetailVC alloc]initWithNibName:@"LocationDetailVC" bundle:nil] autorelease];
        if ([self.searchDataSourceArray count]){
            NSLog(@"Count Array %@",[self.searchDataSourceArray objectAtIndex:indexPath.row]);
            locDetail.locationObject = [self.searchDataSourceArray objectAtIndex:indexPath.row];

        }

        
        self.appDetailVC = locDetail;
        self.appDetailVC.tileInfo = self.widgetTileInfo;
        self.appDetailVC.appType = self.appType;//Set app type before detail data dictionary
        [self.navigationController pushViewController:self.appDetailVC animated:YES];
    }
    else if (self.appType == APP_FUNDRAISING || self.appType == APP_VOLUNTEER) {
        self.appDetailVC = [[[FundVolDetailVC alloc]initWithNibName:@"FundVolDetailVC" bundle:nil] autorelease];
        self.appDetailVC.appType = self.appType;//Set app type before detail data dictionary
        self.appDetailVC.tileInfo = self.widgetTileInfo;
        self.appDetailVC.detailDataDictionary = [self.searchDataSourceArray objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:self.appDetailVC animated:YES];
    }
    else if (self.appType == APP_EVENTS){
        
        TableObject *pdcItem = (TableObject*)[self.searchDataSourceArray objectAtIndex:indexPath.row];
        
        [[NSUserDefaults standardUserDefaults] setValue:pdcItem.eventCategoryId forKey:@"EventsCategoryId"];
        NSString *eventType = [[SPSingletonClass sharedInstance] eventViewTypeStr];
        if ([eventType isEqualToString:@"Calender"]){
            SPEventCalendarVC *eventCalendarVC = [[SPEventCalendarVC alloc] initWithNibName:@"SPEventCalendarVC" bundle:nil];
            eventCalendarVC.appType = self.appType;
            
            eventCalendarVC.headerTitle = pdcItem.eventCategoryName;
            [self.navigationController pushViewController:eventCalendarVC animated:YES];
        }else{
            
            self.appDetailVC = [[EventsListVC alloc]initWithNibName:@"EventsListVC" bundle:nil];
            self.appDetailVC.appType = self.appType;
            self.appDetailVC.tileInfo = self.widgetTileInfo;
            self.appDetailVC.detailDataDictionary = [self.searchDataSourceArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:self.appDetailVC animated:YES];
        }
        
        
        
        
        
        
    }
    else if (self.appType == APP_NEWS){
        self.appDetailVC = [[[NewListVC alloc]initWithNibName:@"NewListVC" bundle:nil] autorelease];
        NSString *eventsId= [[self.searchDataSourceArray objectAtIndex:indexPath.row] valueForKey:@"CategoryId"];
        //[self.searchDataSourceArray objectAtIndex:indexPath.row]
        //TableObject *pdcItem = (TableObject*)[self.searchDataSourceArray objectAtIndex:indexPath.row];
        
        [[NSUserDefaults standardUserDefaults] setObject:eventsId forKey:@"NewsCategeoryId"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
        self.appDetailVC.appType = self.appType;
        self.appDetailVC.tileInfo = self.widgetTileInfo;
        self.appDetailVC.detailDataDictionary = [self.searchDataSourceArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:self.appDetailVC animated:YES];
    }
    else if (self.appType == APP_PRODUCTS){
        
        
        ProductsListVC *prod = [[ProductsListVC alloc] initWithNibName:@"ProductsListVC" bundle:nil];
        [[NSUserDefaults standardUserDefaults] setValue:[[self.searchDataSourceArray objectAtIndex:indexPath.row] valueForKey:@"CategoryId"] forKey:@"ProductionCategoryId"];
        
        prod.boolNoListing = NO;
        self.appDetailVC = prod;
        
        self.appDetailVC.appType = self.appType;
        self.appDetailVC.tileInfo = self.widgetTileInfo;
        self.appDetailVC.detailDataDictionary = [self.searchDataSourceArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:self.appDetailVC animated:YES];
    }else if (self.appType == APP_VIDEO){
        SPVideoListVC *appVideoListVC = [[SPVideoListVC alloc]initWithNibName:@"SPVideoListVC" bundle:nil];
        appVideoListVC.boolNoListing = NO;
        self.appDetailVC = appVideoListVC;
        [[SPSingletonClass sharedInstance] setAppCategoryName:[[self.searchDataSourceArray objectAtIndex:indexPath.row] valueForKey:@"CategoryName"]];
        
        self.appDetailVC.appType = self.appType;
        self.appDetailVC.tileInfo = self.widgetTileInfo;
        self.appDetailVC.detailDataDictionary = [self.searchDataSourceArray objectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults] setValue:[[self.searchDataSourceArray objectAtIndex:indexPath.row] valueForKey:@"CategoryId"] forKey:@"VideoCategoryId"];
        [self.navigationController pushViewController:self.appDetailVC animated:YES];
        
        
        
        
    }
    
    
    
    
    
    //  SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    // [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Sponsor-Detail" deviceType:@"iOS"];
    
    //Present it finally
    
    
}

#pragma mark - Data Source

-(void)setDataSourceDic:(NSDictionary *)dataSourceDic
{
    self.noDataAvailableLbl.hidden = YES;
    NSLog(@"dataSourceDic %@",dataSourceDic);
    if (dataSourceDic.count==0){
        
        [self.noDataAvailableLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
        
        self.noDataAvailableLbl.hidden = NO;
        self.noDataAvailableLbl.text = @"Currently there is no content available";
        
    }else{
        self.noDataAvailableLbl.hidden = YES;
        
    }
    
    /*
     NSString *str;
     objectArray = (NSMutableArray *)dataSourceDic;
     
     if (objectArray.count>0){
     str = [NSString stringWithFormat:@"%@",[objectArray objectAtIndex:0]];
     
     }
     if ([str isEqualToString:@"<null>"]||[objectArray count]==0){
     [activityIndicator stopAnimating];
     loadMoreLabel.text =@"No More Activity";
     return ;
     
     }else{
     //[self.accountActivityArry addObjectsFromArray:objectArray];
     //[self.walletTableView reloadData];
     }
     */
    
    
    [super setDataSourceDic:dataSourceDic];
    [activityIndicator stopAnimating];
    
    // NSLog(@"DataSourceDic %@",self.DataSourceDic);
    
    /*
     if (self.appType == APP_MEDIA) {
     if (self.selectedTab.tag == 0) {
     self.videoArray = (NSMutableArray*)dataSourceDic;
     }
     if (self.selectedTab.tag == 1) {
     self.audioArray = (NSMutableArray*)dataSourceDic;
     }
     if (self.selectedTab.tag == 2) {
     self.youtubeArray = (NSMutableArray*)dataSourceDic;
     }
     }
     */
    [self tableReloadData];
    
    
}

- (void)tableReloadData
{
    NSLog(@"tabNews %@",tabNews);
    tabNews.dataSource = self;
    tabNews.delegate = self;
    
    [tabNews reloadData];
    
}

#pragma mark - Audio/Video/Youtube Feeds Delegate

-(void)videoPodcastFeedsRecieved:(NSArray*)array{
    NSLog(@"Table: videoPodcastFeedsRecieved");
    
    if(!array || ([array count] == 0))
    {
        self.haveMediaContent = NO;
    }
    else{
        self.haveMediaContent = YES;
    }
    
    self.videoArray = (NSMutableArray*)array;
    if (self.selectedTab.tag == 1) {
        self.dataSourceDic = (NSDictionary*)array;
    }
    
    
}


-(void)audioPodcastFeedsRecieved:(NSArray*)array{
    NSLog(@"Table: audioPodcastFeedsRecieved");
    self.audioArray = (NSMutableArray*)array;
    if(!array || ([array count] == 0))
    {
        self.haveMediaContent = NO;
    }
    else{
        self.haveMediaContent = YES;
    }
    if (self.selectedTab.tag == 2) {
        self.dataSourceDic = (NSDictionary*)array;
    }
    
    
}


-(void)youtubePodcastFeedsRecieved:(NSArray*)array{
    NSLog(@"Table: youtubePodcastFeedsRecieved");
    self.youtubeArray = (NSMutableArray*)array;
    if(!array || ([array count] == 0))
    {
        self.haveMediaContent = NO;
    }
    else{
        self.haveMediaContent = YES;
    }
    
    if (self.selectedTab.tag == 0) {
        self.dataSourceDic = (NSDictionary*)array;
    }
    
    
}


#pragma mark - Override

-(void)loadNewSubTabAfterSomeTime:(UIButton*)sender{
    
    if (myWebView) {
        myWebView.delegate = nil;
        [myWebView stopLoading];
        [myWebView removeFromSuperview];
    }
    
    switch (self.appType) {
        case APP_MEDIA:
        {
            self.dataSourceDic = nil;
            
            //Setting to nil will stop activity and its best to start it again.
            [self startFrontActivity];
            
            switch (sender.tag) {
                case 1:
                {
                    self.appSubType = TAB_VIDEOS;
                    if (!_videoArray) {
                        [[PodcastFeeds sharedInstance] refreshVideoFeedsWithDelegate:self];
                        //[NSThread detachNewThreadSelector:@selector(refreshVideoFeedsWithDelegate:) toTarget:[PodcastFeeds sharedInstance] withObject:self];
                        //[[PodcastFeeds sharedInstance] refreshFeedsWithDelegate:self type:_appSubType];
                    }
                    else{
                        
                        if([_videoArray count]==0)
                        {
                            self.haveMediaContent = NO;
                        }
                        else{
                            self.haveMediaContent = YES;
                        }
                        self.dataSourceDic = (NSDictionary*)_videoArray;
                    }
                }
                    break;
                case 2:
                {
                    self.appSubType = TAB_AUDIO;
                    if (!_audioArray) {
                        [self startFrontActivity];
                        [[PodcastFeeds sharedInstance] refreshAudioFeedsWithDelegate:self];
                        //[NSThread detachNewThreadSelector:@selector(refreshAudioFeedsWithDelegate:) toTarget:[PodcastFeeds sharedInstance] withObject:self];
                        //[[PodcastFeeds sharedInstance] refreshFeedsWithDelegate:self type:_appSubType];
                    }
                    else{
                        
                        if([_audioArray count]==0)
                        {
                            self.haveMediaContent = NO;
                        }
                        else{
                            self.haveMediaContent = YES;
                        }
                        self.dataSourceDic = (NSDictionary*)_audioArray;
                    }
                }
                    break;
                case 0:
                {
                    self.appSubType = TAB_YOUTUBE;
                    if (!_youtubeArray) {
                        [[PodcastFeeds sharedInstance] refreshYoutubeFeedsWithDelegate:self];
                        //[NSThread detachNewThreadSelector:@selector(refreshYoutubeFeedsWithDelegate:) toTarget:[PodcastFeeds sharedInstance] withObject:self];
                        //[[PodcastFeeds sharedInstance] refreshFeedsWithDelegate:self type:_appSubType];
                    }
                    else{
                        
                        if([_youtubeArray count]==0)
                        {
                            self.haveMediaContent = NO;
                        }
                        else{
                            self.haveMediaContent = YES;
                        }
                        self.dataSourceDic = (NSDictionary*)_youtubeArray;
                    }
                    
                }
                    break;
                case 3:
                {
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
        default:
            break;
    }
    
    self.view.userInteractionEnabled = YES;
    [activityIndicator stopAnimating];
}

-(IBAction)subTabOptionClicked:(UIButton*)sender{
    [super subTabOptionClicked:sender];
    self.selectedTab = sender;
    [self startFrontActivity];
    
    self.view.userInteractionEnabled = NO;
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    NSString *action = nil;
    switch (sender.tag) {
        case 0:
        {
            action = @"YouTube";
        }
            break;
        case 1:
        {
            action = @"Video";
        }
            break;
        case 2:
        {
            action = @"Audio";
        }
            break;
            
        default:
            break;
    }
    [service runAddMediaAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] tabName:action deviceType:@"iOS"];
    
    [self performSelector:@selector(loadNewSubTabAfterSomeTime:) withObject:sender afterDelay:0.1];
}


#pragma mark - UIWebView
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"webView: shouldStartLoadWithRequest");
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSLog(@"webView: webViewDidStartLoad");
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"webView: webViewDidFinishLoad");
    [activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"webView: didFailLoadWithError: %@", error);
    [activityIndicator stopAnimating];
}


#pragma mark - Youtube RSS

-(void)rssFeedsRecieved:(NSArray*)array{
    NSLog(@"array: %@", array);
    //self.dataSourceDic = dictionary;
}

-(void)rssFeedsFailed:(NSError*)error{
    [AppDelegate showAlert:[NSString stringWithFormat:@"Error %d: %@", [error code], [error localizedDescription]]];
}

#pragma Life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)animateActivityAndBringForward{
    [self.view bringSubviewToFront:activityIndicator];
    [self startFrontActivity];
}

-(void)reloadUsingWebService{
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    
    
    switch (self.appType) {
        case APP_NONE:
        {
            //[example1 runNews];
        }
            break;
        case APP_ABOUT:
        {
            //[example1 runEvents];
        }
            break;
        case APP_CONTRIBUTE:
        {
            //[example1 runNews];
        }
            break;
        case APP_EVENTS:
        {
            if (![self.dataSourceArray count]) {
                
                widgetName = @"Events";
                widgetName = @"Lists";
                [self animateActivityAndBringForward];
                [example1 runEventsCategeory];
            }
        }
            break;
        case APP_LOCATIONS:
        {
            
            boolUpdate = NO;
            self.clController = [[MyCLController alloc] init];
            
            _clController.delegate = self;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                [_clController.locationManager requestWhenInUseAuthorization];
            }
            
            if ([_clController.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_clController.locationManager requestAlwaysAuthorization];
            }
            
            
            [_clController.locationManager startUpdatingLocation];
            
            widgetName = @"Locations";
            [self pageNumber];
            
            //[self animateActivityAndBringForward];
            [example1 runLocations:[NSString stringWithFormat:@"%d",page] recoderNo:@"50"];
            
        }
            break;
        case APP_MEDIA:
        {
            widgetName = @"Media";
            
            [self performSelectorOnMainThread:@selector(subTabOptionClicked:) withObject:[self.subTabBtnArray objectAtIndex:0] waitUntilDone:NO];
        }
            break;
        case APP_NEWS:
        {
            
            if (![self.dataSourceArray count]) {
                widgetName = @"News";
                [self animateActivityAndBringForward];
                
                
                NSString *brandTileId = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandTileId"];
                self.myId = brandTileId;
                [example1 runNewsCategeory:[brandTileId integerValue]];
                
            }
            
        }
            break;
        case APP_PRODUCTS:
        {
            // if (![self.dataSourceArray count])
            {
                widgetName = @"Lists";
                [self animateActivityAndBringForward];
                [example1 runProductCategeory];
                
            }
        }
            break;
            
        case APP_COUPONS:
        {
            
            [self animateActivityAndBringForward];
        }
            break;
        case APP_COUPONSDETAILS:
        {
            [self animateActivityAndBringForward];
            [example1 runcouponsDetails];
        }
            
            
            
            break;
        case APP_VIDEO:
        {
            
            if (![self.dataSourceArray count]) {
                SDZEXVideoServiceExample *videoService = [[[SDZEXVideoServiceExample alloc] init] autorelease];
                
                widgetName = @"Video";
                [self animateActivityAndBringForward];
                
                NSString *brandTileId = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandTileId"];
                self.myId = brandTileId;
                [videoService runVideoCategeory:[brandTileId integerValue]];
                
            }
            
        }
            break;
        case APP_FUNDRAISING:
        {
            // Fixed the Fundraising refreshing issue.
            
            //  if (![self.dataSourceArray count]) {
            widgetName = @"Fundraising";
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveVolunterListsNotification:) name:@"getVolunterListNotification"
                                                       object:nil];
            [self animateActivityAndBringForward];
            [example1 runFundraisingByBrand];
            // }
        }
            break;
        case APP_VOLUNTEER:
        {
            //if (![self.dataSourceArray count]) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveVolunterListsNotification:) name:@"getVolunterListNotification"
                                                       object:nil];
            widgetName = @"Volunteer";
            
            [self animateActivityAndBringForward];
            [example1 runVolunteerInfoByBrand];
            // }
        }
            break;
        case APP_SOCIAL:
        {
            [self performSelectorOnMainThread:@selector(subTabOptionClicked:) withObject:[self.subTabBtnArray objectAtIndex:0] waitUntilDone:NO];
        }
            break;
            
        default:
            break;
    }
}

// First 20 objects loading here and next 20 objects server hits
- (void)loadMore:(NSIndexPath *)indexPath
{
    //NSLog(@"Load Array %@",[AppDelegate shareddelegate].volunterDict);
    if ([[AppDelegate shareddelegate].volunterDict count] < 50 ) {
        loadMoreLabel.text =@"No More Locations";
        [activityIndicator stopAnimating];
        
        return;
    }
    
    //you just increment page every time ,, page  =  page +1
    //indexPage = page*20;
    //NSLog(@"Page index is %d",indexPage);
    page = page + 1;
    NSLog(@"Page index is incremented %d",page);
    [self reloadUsingWebService];
    
}

// if page count is equal to 1 then to remove all objects in array
- (void)pageNumber
{
    if (page == 1) {
        [self.searchDataSourceArray removeAllObjects];
        return;
    }
}



- (void)receiveVolunterListsNotification:(NSNotification *) notification
{
    
    [self setDataSourceDic:[[AppDelegate shareddelegate].volunterDict mutableCopy]];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //[[DashboardBuilder sharedDelegate] updateUsingWebService];
    
    
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad
{
    self.noDataAvailableLbl.hidden = YES;
    self.searchDataSourceArray = [[NSMutableArray alloc] initWithCapacity:0];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.appType == APP_PRODUCTS||self.appType ==  APP_VIDEO){
        self.headerObject.headerLabel.text = @"";
    }
    NSString *tempStr = [NSString stringWithFormat:@"%@", self.widgetTileInfo.brandid];
    NSString *brandTileID = [NSString stringWithFormat:@"%@", self.widgetTileInfo.brandTileId];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:tempStr forKey:@"BrandId"];
    [[NSUserDefaults standardUserDefaults] setObject:brandTileID forKey:@"BrandTileId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    isViewLoaded = YES;
    
    if (self.subTabOptions) {
        tabNews.frame = CGRectMake(tabNews.frame.origin.x, tabNews.frame.origin.y + kSubBarHeight, tabNews.frame.size.width, tabNews.frame.size.height-kSubBarHeight);
    }
    
}




-(void)viewWillAppear:(BOOL)animated
{
    //Load More Functionality
    
    
    if(self.appType == APP_LOCATIONS)
    {
        pageSize = 50;
        indexPage = 0;
        page = 1;
        [self.searchDataSourceArray removeAllObjects];

    }
    
    if (self.appSubType != TAB_YOUTUBE) {
        [self reloadUsingWebService];
    }
    
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;
    
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    self.haveMediaContent = YES;
    self.navigationController.navigationBar.hidden = NO;
}


- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
    [self.searchDataSourceArray removeAllObjects];
    self.dataSourceArray = nil;

    if(self.appType == APP_LOCATIONS)
    {
        [self.searchDataSourceArray removeAllObjects];

    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewDidUnload
{
    [self setActivityIndicator:nil];
    self.searchBar = nil;
    self.pullToReloadHeaderView = nil;
    self.clController = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)entryAction
{
    
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Listing" deviceType:@"iOS"];
    
}




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [activityIndicator release];
    self.searchBar = nil;
    self.pullToReloadHeaderView = nil;
    self.clController = nil;
    
    [_bannerView release];
    [_bannerImg release];
    [_bannerCancel release];
    [super dealloc];
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    NSLog(@"widgetName %@",widgetName);
    
    
    //    NSLog(@"[AppDelegate shareddelegate].bannersDict %@",[AppDelegate shareddelegate].bannersDict);
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:widgetName]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;
        }
        
    }
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Listing"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


@end
