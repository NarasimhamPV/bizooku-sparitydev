//
//  SPEventCalendarVC.h
//  Exceeding
//
//  Created by Veeru on 09/10/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "BigBanners.h"

@interface SPEventCalendarVC : AppMainDetailVC<BigBannersDelegate>
@property (nonatomic,strong) NSString *headerTitle;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
