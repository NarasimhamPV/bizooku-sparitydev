//
//  ProductsListVC.h
//  Exceeding
//
//  Created by Veeru on 25/08/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "BigBanners.h"
@interface ProductsListVC : AppMainDetailVC<BigBannersDelegate>
@property (nonatomic, strong) BigBanners *bigBanner;

@end
