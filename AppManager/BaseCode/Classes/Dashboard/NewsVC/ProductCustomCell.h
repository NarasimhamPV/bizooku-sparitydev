//
//  ProductCustomCell.h
//  Exceeding
//
//  Created by VENKATALAKSHMI on 01/08/14.
//
//

#import <UIKit/UIKit.h>

@interface ProductCustomCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *productImg;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *desLbl;
@property (retain, nonatomic) IBOutlet UILabel *seeDetailsLbl;

@end
