//
//  NewsDetailVC.m
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "LocationDetailVC.h"
#import "DashboardBuilder.h"
#import "UILabel+Exceedings.h"
#import "AppDelegate.h"

#import "MapView+Exceedings.h"
#import "CustomBannerView.h"
#import "WebViewController.h"

#import "LocationDetailTableViewCell.h"


#define kGoogleKey @""



@interface AddressAnnotation : NSObject<MKAnnotation> {
    CLLocationCoordinate2D coordinate;
}

@property (nonatomic, retain) NSString *mTitle;
@property (nonatomic, retain) NSString *mSubTitle;



@end

@interface AddressAnnotation ()

@property (nonatomic, retain) AddressAnnotation *addAnnotation;

@end

@implementation AddressAnnotation

@synthesize coordinate;
@synthesize mTitle = _mTitle;
@synthesize mSubTitle = _mSubTitle;
@synthesize addAnnotation;



- (NSString *)subtitle{
    return _mSubTitle;
}

- (NSString *)title{
    return _mTitle;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c{
    coordinate=c;
    NSLog(@"%f,%f",c.latitude,c.longitude);
    return self;
}
@end



@interface LocationDetailVC ()
@property (nonatomic, retain) NSMutableArray *detailDataSortedKeys;
@property (retain, nonatomic) NSString *googleAddress;
@property (retain, nonatomic) NSString *displayAddress;
@property (retain, nonatomic) NSMutableArray *detailDataKeysObject;
@property (nonatomic,strong) NSString *addCityText;
@property (nonatomic,strong) NSString *distanceText;
- (IBAction) showAddress;

@end

@implementation LocationDetailVC

@synthesize mainTable;
@synthesize detailDataDictionary = _detailDataDictionary;
@synthesize detailDataKeysObject = _detailDataKeysObject;
@synthesize detailDataSortedKeys = _detailDataSortedKeys;
@synthesize googleAddress = _googleAddress;
@synthesize displayAddress = _displayAddress;
@synthesize locationObject;

@synthesize viCallus = _viCallus;
@synthesize viDirections = _viDirections;
@synthesize viWebsite = _viWebsite;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}
- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
}


#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Locations"]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
//        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    [self entryAction];
    NSLog(@"withObject %@",withObject);
    WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
    webVc.bannerDict = withObject;
    [self.navigationController pushViewController:webVc animated:YES];
    
    
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
    
}




-(NSString*)cleanNiltext:(NSString*)string{
    if ([string length] == 0) {
        return nil;
    }
    return string;
}



-(NSString*)evaluateAddressString{
    NSMutableString *addStr = [NSMutableString string];
    
    {
        NSString *keyObject = locationObject.address;
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@"%@", keyObject];
        }
    }
    return [NSString stringWithString:addStr];
}

- (NSString *)evaluateCityAndStateString
{
     NSMutableString *addStr = [NSMutableString string];
    {
        NSString *keyObject = locationObject.city;
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@"%@", keyObject];
        }
    }
    
    {
        NSString *keyObject = locationObject.state;
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@", %@", keyObject];
        }
    }
    
    {
        NSString *keyObject = locationObject.zip;
        if (keyObject && [keyObject length]) {
            [addStr appendFormat:@" - %@", keyObject];
        }
    }
    
    return [NSString stringWithString:addStr];

}

-(void)buildTableDataSource{
    NSString *addStr = [self evaluateAddressString];
    
    self.detailDataSortedKeys = [NSMutableArray array];
    self.detailDataKeysObject = [NSMutableArray array];
    
    CGFloat spacing = 10;
    CGFloat allWidth = myScrollView.frame.size.width - 2*spacing;
    
    
    //Set texts first so that we can decide sizes
    NSString *locationText = locationObject.locationName;
    locationText = ([self cleanNiltext:locationText] != nil)?[@"" stringByAppendingString:locationText]:nil;
    [lbTitle sizeLabelForTitle];
    // [lbTitle sizeLabelForDescription];
    [lbTitle setTextColor:[UIColor blackColor]];
    lbTitle.text = locationText;
    [lbTitle resizeToFit];
    [lbTitle setTextAlignment:NSTextAlignmentCenter];
    
    //Google search address
    self.googleAddress = [locationText stringByAppendingString:addStr];
     /*
    self.distanceText = [[NSString stringWithFormat:@"%.f", locationObject.distance/kMeterToMilesFactor] stringByAppendingString:@" m"];
    [lbDistance sizeLabelForDescription];
    [lbDistance setTextColor:[UIColor grayColor]];
    [lbDistance resizeToFit];*/
    
    NSString *addFullText = [self evaluateAddressString];
    //[lbAddress sizeLabelForMid];
    lbAddress.text = addFullText;
    [lbAddress sizeLabelForDescription];
    [lbAddress setTextColor:[UIColor darkGrayColor]];
    
    [lbAddress resizeToFit];
    
    self.addCityText = [self evaluateCityAndStateString];
    //[lbAddress sizeLabelForMid];
    [lbCity sizeLabelForDescription];
    [lbCity setTextColor:[UIColor darkGrayColor]];
//    lbCity.text = addCityText;
    [lbCity resizeToFit];
    
   // [btnPhone setTitle:[@"Phone    : " stringByAppendingString:(locationObject.phone)?locationObject.phone:@""] forState:UIControlStateNormal];
  //  [btnWebSite setTitle:[@"Website : " stringByAppendingString:(locationObject.website)?locationObject.website:@""] forState:UIControlStateNormal];
    
    //Now align all
    CGRect lastRect = CGRectZero;
    CGFloat lastHighHeight = 0.0;
    
    
    //Title View
    {
        lastRect = CGRectMake(spacing, 0, allWidth, 0);
        
        if(showMap)
        {
            lastHighHeight = 240.0;
        }
        else{
            lastHighHeight = 0.0;
        }
        
        //1
        if (locationText == nil) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y+5 + lastHighHeight, allWidth, lbTitle.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lbTitle.frame = lastRect;
        
        //3
        if (self.distanceText == nil) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, lbDistance.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lbDistance.frame = lastRect;
        
        //4
        if (addFullText == nil) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, lbAddress.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lbAddress.frame = lastRect;
        
        //5
        if (self.addCityText == nil) {
            lastRect.size.height = 0.0;
        }
        else {
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, lbCity.frame.size.height);
            lastHighHeight = lastRect.size.height;
        }
        lbCity.frame = lastRect;
        
    }
    
    {
        lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight + spacing, allWidth, _viCallus.frame.size.height);
        lastHighHeight = lastRect.size.height;
        _viCallus.frame = lastRect;
    }
    
    {
        lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, _viWebsite.frame.size.height);
        lastHighHeight = lastRect.size.height;
        _viWebsite.frame = lastRect;
    }
    
    {
        lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, _viDirections.frame.size.height);
        lastHighHeight = lastRect.origin.y+lastRect.size.height;
        _viDirections.frame = lastRect;
    }
    
    //Finally set scrolling
    
    if (showMap) {
        myScrollView.contentSize = CGSizeMake(myScrollView.frame.size.width,  lastHighHeight+250);
    }
    else{
        myScrollView.frame = CGRectMake(myScrollView.frame.origin.x, kBarHeight - 3, myScrollView.frame.size.width, 200);
    }
    
    
    NSLog(@"myScrollView: %@ lastRect:%@ scroll view height : %f", myScrollView, NSStringFromCGRect(lastRect), myScrollView.contentSize.height);
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [activityIndicator stopAnimating];
    self.view.userInteractionEnabled = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mapTableArray =[[NSMutableArray alloc]init];
    
    [self.mapTableArray addObject:locationObject.phone];
    [self.mapTableArray addObject:locationObject.website];
    [self.mapTableArray addObject:locationObject.address];
    [self.mapTableArray addObject:@""];
    
    
    self.mapTableTitleArray =[[NSMutableArray alloc]init];
    
    [self.mapTableTitleArray addObject:@"Phone"];
    [self.mapTableTitleArray addObject:@"Website"];
    [self.mapTableTitleArray addObject:@"Address"];
    [self.mapTableTitleArray addObject:@"Directions to here"];
    
   // NSLog(@"The location info : %@  %@  %@", locationObject.newsID, locationObject.eventID, locationObject.locId);
    
    if(locationObject.locId)
    {
        self.itemId = [NSNumber numberWithInt:[locationObject.locId intValue]];
    }
    //self.itemId = locationObject.
    showMap = (locationObject.location.coordinate.latitude != 0.0 && locationObject.location.coordinate.longitude != 0.0);
    
    [lbTitle sizeLabelForTitle];
    [lbAddress sizeLabelForDescription];
    
    findRoute = NO;
    
    // Do any additional setup after loading the view from its nib.
    
    [activityIndicator startAnimating];
    
    if (showMap) {
        [self showAddress];
        [locationMap zoomToPoint:locationObject.location.coordinate.latitude lonfitude:locationObject.location.coordinate.longitude];
    }
    else{
        locationMap.hidden = YES;
    }
    
    [self buildTableDataSource];
    
    self.view.userInteractionEnabled = NO;
    
     [lbTitle setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18.0f]];
    [lbTitle setFont:[UIFont boldSystemFontOfSize:18.0f]];  
     [lbAddress setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18.0f]];
     [lbDistance setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18.0f]];

       [mapDetailTableView registerNib:[UINib nibWithNibName:@"LocationDetailTableViewCell" bundle:nil]forCellReuseIdentifier:@"LocationDetailTableViewCell"];
}




- (void)viewDidUnload
{
    
    [locationMap release];
    locationMap = nil;
 
    [self setMainTable:nil];
    [locationDetailCell release];
    locationDetailCell = nil;
    [myScrollView release];
    myScrollView = nil;
    [lbTitle release];
    lbTitle = nil;
    [lbDistance release];
    lbDistance = nil;
    [lbAddress release];
    lbAddress = nil;
    
    
    [activityIndicator release];
    activityIndicator = nil;
    [btnPhone release];
    btnPhone = nil;
    [btnWebSite release];
    btnWebSite = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table Cell

-(NSString*)labelTitleForKey:(NSString*)key{
    if ([key isEqualToString:@"Address"]) {
        return key;
    }
    if ([key isEqualToString:@"LocationName"]) {
        return @"Location";
    }
    if ([key isEqualToString:@"City"]) {
        return key;
    }
    if ([key isEqualToString:@"Fax"]) {
        return key;
    }
    if ([key isEqualToString:@"Phone"]) {
        return key;
    }
    if ([key isEqualToString:@"PrimaryEmailAddress"]) {
        return @"Email Address";
    }
    if ([key isEqualToString:@"State"]) {
        return key;
    }
    if ([key isEqualToString:@"Zip"]) {
        return key;
    }
    return nil;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    //Import component
    
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
    [lblTitle setFont:[UIFont fontWithName:kDefaultFontName size:20]];
    
    NSString *key = [_detailDataSortedKeys objectAtIndex:indexPath.row];
    NSString *keyObj = [_detailDataKeysObject objectAtIndex:indexPath.row];
    
    if (key && [key length]) {
        lblTitle.text = [NSString stringWithFormat:@"%@ : %@", [self labelTitleForKey:key], keyObj];
    }
}

#pragma mark TableView Data Source and Delegates

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mapDetailTableView) {
        
        NSString *text = [self.mapTableArray objectAtIndex:indexPath.row];
        NSLog(@"self.mapTableArray %@",self.mapTableArray);
        NSLog(@"text %@",text);
        
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        
        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
        
        CGFloat height = MAX(size.height, 44.0f);
        NSLog(@"height %f",height + (CELL_CONTENT_MARGIN * 2));
        if (indexPath.row == 2){
            return 84;
            
        }
        return height + (CELL_CONTENT_MARGIN * 2);
        
        
        
    }
    
    return 40;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == mapDetailTableView) {
        return 4;
    }
    else{
        return [_detailDataSortedKeys count];
    }

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == mapDetailTableView) {
        
        NSString *cellIdentifier = @"LocationDetailTableViewCell";
        
        LocationDetailTableViewCell *cell = (LocationDetailTableViewCell*)[mapDetailTableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.descriptionLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        
        
        [cell.descriptionLbl  setNumberOfLines:0];
        [cell.descriptionLbl  setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [cell.descriptionLbl  setTag:11];
        [[cell contentView] addSubview: cell.descriptionLbl];
        
        [cell.titleLbl  setNumberOfLines:0];
        [cell.titleLbl  setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [cell.titleLbl  setTag:22];
        [[cell contentView] addSubview: cell.titleLbl];
        
        
        NSString *text = [self.mapTableArray objectAtIndex:indexPath.row];
        NSString *titleText = [self.mapTableTitleArray objectAtIndex:indexPath.row];
        
        
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        
        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
        
        if (!cell.descriptionLbl)
            cell.descriptionLbl = (UILabel*)[cell viewWithTag:11];
        
        [cell.descriptionLbl  setText:text];
        [cell.descriptionLbl  setFrame:CGRectMake(CELL_CONTENT_MARGIN+10, 15, CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), MAX(size.height, 44.0f))];
        
        cell.titleLbl = (UILabel*)[cell viewWithTag:22];
        
        [ cell.titleLbl  setText:titleText];
        [ cell.titleLbl  setFrame:CGRectMake(CELL_CONTENT_MARGIN , CELL_CONTENT_MARGIN, CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), MAX(size.height, 44.0f))];
        
        if (indexPath.row == 2) {
            
            //NSString *addText =[NSString stringWithFormat:@"%@\n%@ \n%@",self.distanceText,text,self.addCityText];
            NSString *addText =[NSString stringWithFormat:@"%@ \n%@",text,self.addCityText];

            [cell.descriptionLbl setFrame:CGRectMake(20, 18, 279, 60)];
            
            [ cell.descriptionLbl  setText:addText];
            
//            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);

            
        }
/*      if (indexPath.row == 3){
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);

        }*/

        //self.addCityText;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
        
    }
    else
        
    {
        NSString *cellIdentifier = @"CustomerCellgaU";
        UITableViewCell *TableCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (TableCell == nil) {
            
            [[NSBundle mainBundle] loadNibNamed:@"LocationDetailCell" owner:self options:nil];
            TableCell = locationDetailCell;
            
            TableCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            //TableCell.backgroundView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cellWhiteBg.png"]] autorelease];
        }
        [self configureCell:TableCell atIndexPath:indexPath];
        
        
        return TableCell;
        
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == mapDetailTableView) {
        
        
        if (indexPath.row == 0) {
            
            UIDevice *device = [UIDevice currentDevice];
            if ([[device model] isEqualToString:@"iPhone"] ) {
                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"make a call or not" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
                [callAlert show];
                
            } else {
                UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [Notpermitted show];
            }
            
            
        }
        if (indexPath.row == 1) {
            
            NSLog(@"The website : %@", locationObject.website);
            
            NSURL *url = [NSURL URLWithString:locationObject.website];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
            else{
                [AppDelegate showAlert:[NSString stringWithFormat:@"Website is not available"]];
            }
            
            
        }
        if (indexPath.row == 2) {
            
        }
        if (indexPath.row == 3) {
            
            CLLocationCoordinate2D origin = [AppDelegate shareddelegate].currentLocation.coordinate;
            CLLocationCoordinate2D destination = locationObject.location.coordinate;
            
            NSString *appleMapsURLStr = [NSString stringWithFormat:@"maps://q=&saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f&view=map", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
            NSURL *appleURL = [NSURL URLWithString:appleMapsURLStr];
            appleURL = nil;
            
            //    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
            
          NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%1.6f,%1.6f",destination.latitude, destination.longitude];
            
        //NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f",origin.latitude, origin.longitude,destination.latitude, destination.longitude];
            NSURL *appleMapUrl = [NSURL URLWithString:urlString];
            
            if ([[UIApplication sharedApplication] canOpenURL:appleURL]) {
                [[UIApplication sharedApplication] openURL:appleURL];
            }
            else if ([[UIApplication sharedApplication] canOpenURL:appleMapUrl]) {
                [[UIApplication sharedApplication] openURL:appleMapUrl];
            }
            else{
                [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to find directions"]];
            }
            
        }
        
    }

}

#pragma mark - Location

-(CLLocationCoordinate2D) addressLocation {
    NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@&output=csv",[_googleAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSUTF8StringEncoding error:nil];
    NSArray *listItems = [locationString componentsSeparatedByString:@","];
    
    double latitude = 0.0;
    double longitude = 0.0;
    
    if([listItems count] >= 4 && [[listItems objectAtIndex:0] isEqualToString:@"200"]) {
        latitude = [[listItems objectAtIndex:2] doubleValue];
        longitude = [[listItems objectAtIndex:3] doubleValue];
    }
    else {
        //Show error
    }
    CLLocationCoordinate2D location;
    location.latitude = latitude;
    location.longitude = longitude;
    return location;
}


- (IBAction) showAddress {
    //Hide the keypad
    
    CLLocationCoordinate2D location = locationObject.location.coordinate;

    AddressAnnotation *addAnnotation = [[[AddressAnnotation alloc] initWithCoordinate:location] autorelease];
    addAnnotation.mTitle = locationObject.locationName;
    addAnnotation.mSubTitle = [NSString stringWithFormat:@"Email: %@", locationObject.primaryEmail];
    [locationMap addAnnotation:addAnnotation];
    
}

/*

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
    MKPinAnnotationView *annView =[[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"] autorelease];
    annView.pinColor = MKPinAnnotationColorGreen;
    annView.animatesDrop=TRUE;
    annView.canShowCallout = YES;
    annView.calloutOffset = CGPointMake(-5, 5);
    NSLog(@"The Users LOcation : %f, %f",mapView.userLocation.location.coordinate.latitude, mapView.userLocation.location.coordinate.longitude);
    
    return annView;
}
*/



- (IBAction)actPhone:(id)sender {
    
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"make a call or not" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
        [callAlert show];
        
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
    }
    
}

- (IBAction)actWebsite:(id)sender {
    

    NSURL *url = [NSURL URLWithString:locationObject.website];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Website is not available"]];
    }
    
}

- (IBAction)actDirections:(id)sender {
    CLLocationCoordinate2D origin = [AppDelegate shareddelegate].currentLocation.coordinate; 
    CLLocationCoordinate2D destination = locationObject.location.coordinate;   
    
    NSString *appleMapsURLStr = [NSString stringWithFormat:@"maps://q=&saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f&view=map", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    NSURL *appleURL = [NSURL URLWithString:appleMapsURLStr];
    appleURL = nil;
    
//    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    
//        NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%1.6f,%1.6f",destination.latitude, destination.longitude];
    
    NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f",origin.latitude, origin.longitude,destination.latitude, destination.longitude];
    NSURL *appleMapUrl = [NSURL URLWithString:urlString];

    
    if ([[UIApplication sharedApplication] canOpenURL:appleURL]) {
        [[UIApplication sharedApplication] openURL:appleURL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:appleMapUrl]) {
                [[UIApplication sharedApplication] openURL:appleMapUrl];
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to find directions"]];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if(buttonIndex == 0)//Cancle button pressed
    {
        
        
    }
    else if(buttonIndex == 1)//Call button pressed.
    {
        
        NSString *phoneNumber = [locationObject.phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSString *phoNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
        NSString *number = [phoNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *pno = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSURL *callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", pno]];
        NSLog(@"The phone number : %@", pno);
        NSLog(@"pno %@",pno);
        if ([[UIApplication sharedApplication] canOpenURL:callURL]) {
            [[UIApplication sharedApplication] openURL:callURL];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to dial the number"]];
        }
    }
}

@end
