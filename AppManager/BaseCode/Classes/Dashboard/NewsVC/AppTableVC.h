//
//  AppTableVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainVC.h"
#import "PodcastFeeds.h"
#import "SBJSON.h"
#import "NSString+SBJSON.h"
#import "DBTilesInfo.h"
#import "DBWidget.h"
#import "AppMainDetailVC.h"

#import "UIPullToReloadHeaderView.h"
#import "MyCLController.h"
#import "CustomBannerView.h"
#import "BigBanners.h"


@interface AppTableVC : AppMainVC<UITableViewDataSource, UITableViewDelegate, PodcastFeedsDelegate, UIWebViewDelegate, UISearchBarDelegate, MyCLControllerDelegate,BigBannersDelegate>
{
    IBOutlet UITableView *tabNews;
    IBOutlet UITableViewCell *feedCell;
    BOOL checkForRefresh;
    BOOL isViewLoaded;
    BOOL boolUpdate;
    
    NSString *widgetName;
    
    //Load More
    int pageSize;
    int indexPage;
    NSMutableArray *objectArray;
    
    UIActivityIndicatorView *activityIndicator;
    UILabel *loadMoreLabel;
    int page;
    
    
}
@property (retain, nonatomic) IBOutlet UIButton *bannerCancel;
@property (retain, nonatomic) IBOutlet UIButton *appTableBannerWebBtn;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (nonatomic,retain)  IBOutlet UILabel *noDataAvailableLbl;
@property (retain, nonatomic) IBOutlet UIView *bannerView;
@property (nonatomic, strong) NSMutableArray *bannerURLS;
@property (nonatomic, strong) NSDictionary *appTablebannerDict;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;
@property (readwrite, nonatomic) BOOL haveMediaContent;

@property (nonatomic, retain) UIPullToReloadHeaderView *pullToReloadHeaderView;
@property (nonatomic, retain) MyCLController *clController;
@property (strong, nonatomic) NSString *myId;



@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
