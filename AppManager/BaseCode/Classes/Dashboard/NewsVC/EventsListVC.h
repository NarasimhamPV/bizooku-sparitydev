//
//  EventsListVC.h
//  Exceeding
//
//  Created by Veeru on 27/08/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "BigBanners.h"

@interface EventsListVC :AppMainDetailVC<UITableViewDataSource,UITableViewDelegate,BigBannersDelegate>
@property (strong, nonatomic) NSString *dateStr;
@property (assign,nonatomic) BOOL isEventCalendar;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
