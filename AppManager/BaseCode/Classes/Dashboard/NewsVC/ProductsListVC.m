//
//  ProductsListVC.m
//  Exceeding
//
//  Created by Veeru on 25/08/14.
//
//

#import "ProductsListVC.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "ProductCustomCell.h"
#import "ProductsVC.h"

#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"

static NSString *const kProductCustomCell = @"ProductCustomCellIdentifier";
static NSString *const kProductCustomCellNib = @"ProductCustomCell";

@interface ProductsListVC ()<UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UILabel *errorLbl;
@property (retain, nonatomic) IBOutlet UITableView *productTableView;
//@property (strong, nonatomic) AppMainVC *mainVC;
@property (strong, nonatomic) NSArray *productArray;
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation ProductsListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.productTableView registerNib:[UINib nibWithNibName:kProductCustomCellNib bundle:nil] forCellReuseIdentifier:kProductCustomCell];
    self.productTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.headerObject.rightToolbarItem = nil;
    self.headerObject.btnShare.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveProductsNotification:)
                                                 name:@"getProductListsNotification"
                                               object:nil];
    
    SDZEXWidgetServiceExample *service = [[[SDZEXWidgetServiceExample alloc]init]autorelease];
    [service runProductsList];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}
- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)receiveProductsNotification:(NSNotification *)notification
{
    
    self.productArray = [[AppDelegate shareddelegate].productDict mutableCopy];
    
    if(self.productArray.count > 0)
    {
        [self reloadTable];
        
    }else{
        self.errorLbl.text = @"Data are not availabele";
    }
    
}
- (void)reloadTable
{
    [self.productTableView reloadData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [self.productArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.headerObject.headerLabel.text = [[self.productArray objectAtIndex:indexPath.row]valueForKey:@"CategoryName"];
    
    ProductCustomCell *productListCell = [tableView dequeueReusableCellWithIdentifier:kProductCustomCell];
    productListCell.selectionStyle =UITableViewCellSelectionStyleNone;
    
    productListCell.titleLabel.text = [[self.productArray objectAtIndex:indexPath.row]objectForKey:@"Title"];
    NSString *priceStr = [NSString stringWithFormat:@"%.2f", [[[self.productArray objectAtIndex:indexPath.row]objectForKey:@"Price"] floatValue]];
    
    if ([priceStr isEqualToString:@"0.00"]){
        productListCell.desLbl.hidden = YES;
    }else{
        productListCell.desLbl.hidden = NO;
        productListCell.desLbl.text = [NSString stringWithFormat:@"$%@", priceStr];


    }
    NSDictionary *tempDict = [self.productArray objectAtIndex:indexPath.row];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[tempDict objectForKey:@"Image"]];
    if ([urlString length]!=0) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
            NSURLResponse* response = nil;
            NSError* error = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                // update the image and sent notification on the main thread
                if ([[tempDict valueForKey:@"Image"] length]) {
                    productListCell.productImg.image = [UIImage imageWithData:data];
                }else{
                    productListCell.productImg.image = [UIImage imageNamed:@"no-image.png"];
                }
            });
        });

    }else{
        productListCell.productImg.image = [UIImage imageNamed:@"no-image.png"];

    }
    return productListCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110.0f;
}

-  (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tempDict = [self.productArray objectAtIndex:indexPath.row];
    AppMainVC *mainVC = [[AppMainVC alloc]init];
    mainVC.appDetailVC = [[ProductsVC alloc]initWithNibName:@"ProductsVC" bundle:nil];
    mainVC.appDetailVC.appType = self.appType;
    mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
    mainVC.appDetailVC.detailDataDictionary = tempDict;
    [self.navigationController pushViewController:mainVC.appDetailVC animated:YES];
    
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"News"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = APP_NEWS;//[[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}



@end
