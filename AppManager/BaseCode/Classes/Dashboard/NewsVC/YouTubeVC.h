//
//  AboutUsVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainVC.h"

@interface YouTubeVC : AppMainDetailVC <UIWebViewDelegate, UIScrollViewDelegate>

@property (retain, nonatomic) NSURL *youtubeURL;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UIWebView *myWebView;
- (IBAction)backWebAction:(id)sender;

@end
