//
//  AppHeaderBar.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"



@protocol AppHeaderDelegate

-(IBAction)headerBtnAction:(UIButton*)sender;
-(IBAction)headerSecondBtnAction:(UIButton*)sender;

-(AppType)appTypeForView:(id)sender;
- (IBAction)leftBtnAction:(id)sender;

@end

@interface AppHeaderBar : NSObject


//Header & Subheader
@property (nonatomic, retain) IBOutlet UIView *mainHeader;
@property (retain, nonatomic) IBOutlet UIView *subTabsView;


//Buttons
@property (nonatomic, retain) IBOutlet UIButton *btnClose;
@property (nonatomic, retain) IBOutlet UIButton *btnLocation;
@property (nonatomic, retain) IBOutlet UIButton *btnOptions;
@property (nonatomic, retain) IBOutlet UIButton *btnSearch;
@property (nonatomic, retain) IBOutlet UIView *rightToolBar;
@property (nonatomic, retain) IBOutlet UIButton *btnShare;
@property (nonatomic, retain)  IBOutlet UIButton *btnAdd;


//
@property (nonatomic, retain) IBOutlet UILabel *headerLabel;
@property (nonatomic, retain) IBOutlet UIImageView *iconSeperator;
@property (nonatomic, retain) IBOutlet UIButton *headerLeftBtn;

@property (nonatomic, retain) NSArray *rightToolbarItem;
@property (nonatomic, assign) NSObject<AppHeaderDelegate> *delegate;




- (id)initWithDelegate:(id)delegate appType:(AppType)appType;
- (IBAction)leftBtnAction:(id)sender;
@end
