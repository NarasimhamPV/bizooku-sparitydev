//
//  SponsorVC.m
//  Exceeding
//
//  Created by Rajesh Kumar on 19/02/13.
//
//

#import "SponsorVC.h"
#import "UIImageView+WebCache.h"
#import "SDZEXWidgetServiceExample.h"
#import "UILabel+Exceedings.h"
#import "AppDelegate.h"
#import "PhoneNumberFormatter.h"
#import "SDZEXAnalyticsServiceExample.h"

#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"

@interface SponsorVC ()

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation SponsorVC

@synthesize viewSponsorDesc;
@synthesize lblSponsorTitle;
@synthesize mySponsorImg;
@synthesize txtDesc;
@synthesize myScrollView;
@synthesize btnInfo;
@synthesize websiteURL = _websiteURL;

@synthesize btnSponsors = _btnSponsors;
@synthesize itemId = _itemId;


- (void)hideContent:(BOOL) boolVal
{
    mySponsorImg.hidden = boolVal;
    myScrollView.hidden = boolVal;
    viewSponsorDesc.hidden = boolVal;
    txtDesc.hidden = boolVal;
    lblSponsorTitle.hidden = boolVal;
}

- (IBAction)contactUs:(id)sender
{
    
}
-(void)setDataSourceDic:(NSDictionary *)dataSourceDic{
    [super setDataSourceDic:dataSourceDic];
    
   //  self.dataSourceDic = dataSourceDic;
    
    self.socialASheet.detailDictionary = dataSourceDic;
    
    
    [self hideContent:NO];
    
    NSLog(@"The dictionary values are : %@", dataSourceDic);
    
    NSLog(@"The scroll view frame : %f, %f", myScrollView.frame.size.width, myScrollView.frame.size.height);
    
    
    
    CGFloat spacing = 10;
    CGFloat allWidth = myScrollView.frame.size.width - 2*spacing;
    
    CGRect lastRect = CGRectZero;
    CGFloat lastHighHeight = 0.0;
    
    if(dataSourceDic && [[dataSourceDic allKeys] containsObject:@"Email"])
    {
        mail = [dataSourceDic objectForKey:@"Email"];
    }
    
    if(dataSourceDic && [[dataSourceDic allKeys] containsObject:@"Phone"])
    {
        phoneNumber = [dataSourceDic objectForKey:@"Phone"];
    }
    
    if(dataSourceDic && [[dataSourceDic allKeys] containsObject:@"Website"])
    {
        self.websiteURL = [dataSourceDic objectForKey:@"Website"];
    }
    
    if(dataSourceDic && [[dataSourceDic allKeys] containsObject:@"SponserID"])
    {
        self.itemId = [NSNumber numberWithInt:[[dataSourceDic valueForKey:@"SponserID"] intValue]];
    }
    
    if(dataSourceDic && [[dataSourceDic allKeys] containsObject:@"ProductID"])
    {
        self.itemId = [NSNumber numberWithInt:[[dataSourceDic valueForKey:@"ProductID"] intValue]];
    }
    
    
    
    NSString *img = nil;
    if(self.appType == APP_SPONSOR)
    {
        img = [dataSourceDic valueForKey:@"SponserImage"];
       // self.headerObject.headerLabel.text = @"Sponsor";
    }
    else if(self.appType == APP_PRODUCTS)
    {
        img = [dataSourceDic valueForKey:@"ProductImage"];
       // self.headerObject.headerLabel.text = @"Product";
        
    }
    BOOL haveImage = (img != nil && img.length != 0);
    
    //Image
    
    {
        lastRect = CGRectMake(spacing, 0, allWidth, 0);
        
        if (!haveImage) {
            lastRect.size.height = 0.0;
            lastHighHeight = 10.0;
            mySponsorImg.frame = lastRect;
        }
        else {
            [mySponsorImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, mySponsorImg.frame.size.height);
            lastHighHeight = 240;
            
            mySponsorImg.frame = CGRectMake(0, 0, 320, aboutImgHeight);
        }
    }
    
    // Sponsor name:
    NSString *titleText = nil;
    
    if(self.appType == APP_PRODUCTS)
    {
        titleText = [dataSourceDic valueForKey:@"ProductName"];
        sponsorName = [dataSourceDic valueForKey:@"ProductName"];
    }
    else if(self.appType == APP_SPONSOR)
    {
        titleText = [dataSourceDic valueForKey:@"SponserName"];
        sponsorName = [dataSourceDic valueForKey:@"SponserName"];
    }
    
    
    lblSponsorTitle.text = titleText;
    [lblSponsorTitle resizeToFit];
   
    if(titleText)
    {
        lblSponsorTitle.frame = CGRectMake(spacing, mySponsorImg.frame.size.height + spacing, lblSponsorTitle.frame.size.width, lblSponsorTitle.frame.size.height);
        lastHighHeight = mySponsorImg.frame.size.height+lblSponsorTitle.frame.size.height;
        
        lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, mySponsorImg.frame.size.height+lblSponsorTitle.frame.size.height );
    }
    
    
    if(self.appType == APP_PRODUCTS)
    {
        if([[dataSourceDic allKeys] containsObject:@"Price"])
        {
            _lblPrice.text = [NSString stringWithFormat:@"%d",[[dataSourceDic valueForKey:@"Price"] intValue]];
            if([_lblPrice.text isEqualToString:@"0"])
            {
                _lblPrice.text = @"";
            }
            else{
                _lblPrice.text = [NSString stringWithFormat:@"$ %d",[[dataSourceDic valueForKey:@"Price"] intValue]];
                
            }
        }
        
        
        [_lblPrice resizeToFit];
        
        _lblPrice.frame = CGRectMake(spacing, lastHighHeight + spacing, _lblPrice.frame.size.width, _lblPrice.frame.size.height);
        lastHighHeight = lastHighHeight+_lblPrice.frame.size.height;
        
        lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, lastHighHeight);
        
        if([[dataSourceDic allKeys] containsObject:@"ActionButton"])
        {
            if([[dataSourceDic valueForKey:@"ActionButton"] isEqualToString:@"Custom"])
            {
                if([[dataSourceDic allKeys] containsObject:@"CustomActionButtonTitle"])
                {
                    [_btnSponsors setTitle:[dataSourceDic valueForKey:@"CustomActionButtonTitle"] forState:UIControlStateNormal];
                }
            }
            else{
                [_btnSponsors setTitle:[dataSourceDic valueForKey:@"ActionButton"] forState:UIControlStateNormal];
            }
        }
        
        if([mail isEqualToString:@""] && [phoneNumber isEqualToString:@""])
        {
            btnInfo.hidden = YES;
            self.headerObject.iconSeperator.hidden = YES;
        }
        else{
            btnInfo.hidden = NO;
            self.headerObject.iconSeperator.hidden = NO;
        }
        
        [_btnSponsors addTarget:self action:@selector(purchaseAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        _lblPrice.hidden = YES;
        [_btnSponsors setTitle:@"Contact Us" forState:UIControlStateNormal];
        [_btnSponsors addTarget:self action:@selector(actMailSending:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    // Sponsor Contact US button....
    
    
    {
        _btnSponsors.frame = CGRectMake(40, lastRect.size.height + spacing, 240.0, _btnSponsors.frame.size.height);
        lastHighHeight = mySponsorImg.frame.size.height+lblSponsorTitle.frame.size.height+ _btnSponsors.frame.size.height+spacing;
        
        lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, mySponsorImg.frame.size.height+lblSponsorTitle.frame.size.height+_btnSponsors.frame.size.height+spacing);
    }
    
    
    //Organization text
    CGRect orgRect = mySponsorImg.frame;
    
    NSString *orgText = nil;
    
    if(self.appType == APP_SPONSOR)
    {
        orgText = [dataSourceDic valueForKey:@"AboutUs"];
    }
    else if(self.appType == APP_PRODUCTS)
    {
        orgText = [dataSourceDic valueForKey:@"Description"];
    }
  //  txtDesc.text = orgText;

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        NSMutableParagraphStyle *style =  [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
        [style setAlignment: NSTextAlignmentLeft];
        
        NSAttributedString * subText1 = [[NSAttributedString alloc] initWithString:orgText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Condensed" size:16.0f],
                                                    NSParagraphStyleAttributeName : style
                                         }];
        txtDesc.attributedText = subText1;
    }
    else{
        txtDesc.text = orgText;
    }
    
     txtDesc = [AppDelegate resizeTheTextView:txtDesc];

    txtDesc.hidden = (orgText == nil);
    if (orgText) {
        viewSponsorDesc.frame = CGRectMake(spacing, lastRect.size.height + 3, viewSponsorDesc.frame.size.width, viewSponsorDesc.frame.size.height);
    }
    else {
        viewSponsorDesc.frame = CGRectMake(spacing, orgRect.origin.y, viewSponsorDesc.frame.size.width, viewSponsorDesc.frame.size.height);
        lastHighHeight = mySponsorImg.frame.size.height+lblSponsorTitle.frame.size.height + viewSponsorDesc.frame.size.height+spacing;
        
        lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, lastHighHeight);
    }
    
    myScrollView.contentSize = CGSizeMake(myScrollView.frame.size.width, mySponsorImg.frame.size.height + spacing + lblSponsorTitle.frame.size.height + spacing + txtDesc.frame.size.height + 2* spacing + _btnSponsors.frame.size.height + spacing);
     
}


- (void) purchaseAction:(id)sender
{
    [AppDelegate showAlert:@"Yet to be implement"];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        aboutImgHeight = 240.0f;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Do any additional setup after loading the view from its nib.
    
    [self hideContent:YES];
    
    [lblSponsorTitle sizeLabelForTitle];
    
    // [aboutLabel sizeLabelForDescription];
    [txtDesc setTextColor:[UIColor darkGrayColor]];
    
    // [visionLabel sizeLabelForDescription];
  //  [visionLabel setTextColor:[UIColor darkGrayColor]];
    
    [aboutStaticTitle sizeLabelForAboutStatic];
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    NSLog(@"The sponsor Id:  %@",_itemId);
    if(self.appType == APP_SPONSOR)
    {
        [example1 runSponsorByID:[_itemId integerValue]];
    }
    else if(self.appType == APP_PRODUCTS)
    {
        [example1 runProductByID:[_itemId integerValue]];
    }
    
    [self.view bringSubviewToFront:btnInfo];
    
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}

- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
}






#pragma mark Button Info Action....

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    NSString *strAction = nil;
    
    if(self.appType == APP_SPONSOR)
    {
        switch (buttonIndex) {
            case 0:
            {
                strAction = @"Website";
                [self actWeb:nil];
            }
                break;
            case 1:
            {
                strAction = @"ContactUs";
                [self actMailSending:nil];
            }
                break;
            case 2:
            {
                strAction = @"Call";
                [self actCall:nil];
            }
                break;
            case 3:
            {
                strAction = @"Canceled";
            }
                break;
            default:
                break;
        }
    }
    else  if(self.appType == APP_PRODUCTS)
    {
        
        if(mail && ![mail isEqualToString:@""] && phoneNumber && ![phoneNumber isEqualToString:@""])
        {
            switch (buttonIndex) {
                    
                case 0:
                {
                    strAction = @"ContactUs";
                    [self actMailSending:nil];
                }
                    break;
                case 1:
                {
                    strAction = @"Call";
                    [self actCall:nil];
                }
                    break;
                case 2:
                {
                    strAction = @"Canceled";
                }
                    break;
                default:
                    break;
            }
        } else if(mail && ![mail isEqualToString:@""])
        {
            switch (buttonIndex) {
                    
                case 0:
                {
                    strAction = @"ContactUs";
                    [self actMailSending:nil];
                }
                    break;
               
                case 1:
                {
                    strAction = @"Canceled";
                }
                    break;
                default:
                    break;
            }
        } else if(phoneNumber && ![phoneNumber isEqualToString:@""])
        {
            switch (buttonIndex) {
                    
                
                case 0:
                {
                    strAction = @"Call";
                    [self actCall:nil];
                }
                    break;
                case 1:
                {
                    strAction = @"Canceled";
                }
                    break;
                default:
                    break;
            }
        }
        
        
        
    }
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [service runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[_itemId longValue] action:strAction deviceType:@"iOS"];
    }
    else {
        [service runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:[_itemId longValue] action:strAction deviceType:@"iOS"];
    }
    
}



- (NSString *)checkPhoneNumber: (NSString *)number
{
    NSString *str = [[[[number stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

- (IBAction)actCall:(id)sender {
    PhoneNumberFormatter *formatter = [[[PhoneNumberFormatter alloc] init] autorelease];
    NSString *number = [formatter format:phoneNumber withLocale:@"us"];
    
    NSLog(@"The phone number : %@", number);
    
    NSURL *callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [self checkPhoneNumber:number]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:callURL]) {
        [[UIApplication sharedApplication] openURL:callURL];
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to dial the number. Please dial the number '%@' manually.", number ]];
    }
}

- (NSString *) selfBodyContent:(NSString *)firstName lastname:(NSString *)lastName email:(NSString *)email mobile:(NSString *)mobile
{
    NSMutableString *strContent = nil;
    strContent = [NSMutableString stringWithFormat:@"%@",@"Please contact me at your convenience. Here is my contact information:<br/>"];
    //if(email)
    {
        [strContent appendFormat:@"Name: %@ %@<br/>",firstName, lastName];
        [strContent appendFormat:@"Phone: %@<br/>", mobile];
        [strContent appendFormat:@"email: %@<br/>", email];
    }
    return strContent;
    
}
- (IBAction)actMailSending:(id)sender
{
    [self insertInfoVCAnimated];
    
    int widgetId;
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [self.widgetTileInfo.widget.widgetID intValue];
    }

    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.widgetTileInfo.widget)
    {
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:[_itemId longValue] widgetType:@"Sponsor-ContactUs" deviceType:@"iOS"];
    }
    else{
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:[_itemId longValue] widgetType:@"Sponsor-ContactUs" deviceType:@"iOS"];
    }
}

- (void) mailSending:(NSString *)body
{
    if(mail)
    {
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
            mfViewController.mailComposeDelegate = self;
            [[mfViewController navigationBar] setTintColor:[UIColor blackColor]];
            [mfViewController setSubject:[NSString stringWithFormat:@"I saw your information in the '%@'", [AppDelegate shareddelegate].appReference.brandname]];
            [mfViewController setMessageBody:body isHTML:YES];
            [mfViewController setToRecipients:[NSArray arrayWithObject:mail]];
            mfViewController.navigationItem.title = @"Mail";
//            [self presentModalViewController:mfViewController animated:YES];
            [self presentViewController:mfViewController animated:YES completion:nil];
            [mfViewController release];
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:@"Your device is not currently configured to send mail." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            
            [alert show];
            [alert release];
        }
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:@"email not available" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }

}

#pragma mark MFMailComposeViewControllerDelegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    int widgetId;
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [self.widgetTileInfo.widget.widgetID intValue];
    }
    
    
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message Status" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	if(_itemId && self.widgetTileInfo)
    {
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        
        switch (result) {
            case MFMailComposeResultCancelled:{
                //alert.message = @"Canceled";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId longValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultSaved:
            {
                //alert.message = @"Saved";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId longValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultSent:
            {
                //alert.message = @"Sent";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId longValue]  actionType:@"Email" action:@"" status:@"Shared" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultFailed:
            {
                //alert.message = @"Message Failed";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId longValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            default:
                //alert.message = @"Message Not Sent";
                break;
        }
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}


/*- (IBAction)actDirections:(id)sender {
    CLLocationCoordinate2D origin = [AppDelegate shareddelegate].currentLocation.coordinate;
    CLLocationCoordinate2D destination = { latitude, longitude };
    
    NSString *appleMapsURLStr = [NSString stringWithFormat:@"maps://q=&saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f&view=map", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    NSURL *appleURL = [NSURL URLWithString:appleMapsURLStr];
    
    appleURL = nil;
    
    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    
    NSURL *url = [NSURL URLWithString:googleMapsURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:appleURL]) {
        [[UIApplication sharedApplication] openURL:appleURL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to find directions"]];
    }
}
 
 */



- (IBAction)actWeb:(id)sender {

    
    if(_websiteURL && ![_websiteURL isEqualToString:@""])
    {
        // NSString *urlStr = [@"http://" stringByAppendingString:_websiteURL];
        
        NSURL *url = [NSURL URLWithString:_websiteURL];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"Website not available"]];
        }
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"website not configured"]];
    }
   
}


- (IBAction)actInfo:(id)sender {
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[_itemId longValue] action:@"Entry" deviceType:@"iOS"];
    }
    else{
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.widgetTileInfo.widget.widgetID intValue] widgetItemId:[_itemId longValue] action:@"Entry" deviceType:@"iOS"];
    }
    
    
    if(self.appType == APP_SPONSOR)
    {
        UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Contact" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Our Website", @"Contact Us", @"Call Us",nil] autorelease];
        [actinsheet showInView:self.view];
    }
    else if(self.appType == APP_PRODUCTS)
    {
      //  NSMutableArray *arr = [NSMutableArray array];
        
        
        if(mail && ![mail isEqualToString:@""] && phoneNumber && ![phoneNumber isEqualToString:@""])
        {
            UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Contact" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Contact Us",@"Call Us",nil] autorelease];
            [actinsheet showInView:self.view];
        } else if(mail && ![mail isEqualToString:@""])
        {
            UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Contact" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Contact Us",nil] autorelease];
            [actinsheet showInView:self.view];
        } else if(phoneNumber && ![phoneNumber isEqualToString:@""])
        {
            UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Contact" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call Us",nil] autorelease];
            [actinsheet showInView:self.view];
        }
        
    }
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIView *_currentView in actionSheet.subviews) {
        if ([_currentView isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel *)_currentView;
            
            [lbl setFrame:CGRectMake(0, 5, 320, 40)];
            [lbl setTextAlignment:NSTextAlignmentCenter];
            
            [((UILabel *)_currentView) setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:22]];
        }
        else if([_currentView isKindOfClass:[UIButton class]])
        {
           // [((UIButton *)_currentView).titleLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:24]];
            
        }
    }
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    [_infoCollVC.view removeFromSuperview];
    self.infoCollVC = nil;
}

#pragma mark - InfoCollector Delegate

-(void)insertInfoVCAnimated{
    self.infoCollVC = [[[InfoCollectorVC alloc] initWithNibName:@"InfoCollectorVC" bundle:nil] autorelease];
    _infoCollVC.delegate = self;
    [self.view addSubview:_infoCollVC.view];
    
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.2],
                             [NSNumber numberWithFloat:1.2],
                             [NSNumber numberWithFloat:.9],
                             [NSNumber numberWithFloat:1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.5],
                               [NSNumber numberWithFloat:0.7],
                               [NSNumber numberWithFloat:1.0],
                               nil];
    //popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"transform.scale"];
}

-(void)removeInfoVCAnimated
{
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:1.0],
                             [NSNumber numberWithFloat:0.1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:1.0],
                               nil];
    popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"transform.scale"];
}

-(void)infoCancel:(id)sender{
    //    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    //    [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Cancelled" deviceType:@"iOS"];
    
    
    int widgetId;
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [self.widgetTileInfo.widget.widgetID intValue];
    }
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.widgetTileInfo.widget)
    {
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:[_itemId longValue] widgetType:@"Sponsor-ContactUs" deviceType:@"iOS"];
    }
    else{
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:[_itemId longValue] widgetType:@"Sponsor-ContactUs" deviceType:@"iOS"];
    }
    [self removeInfoVCAnimated];
    
}

-(void)infoProceed:(id)sender firstName:(NSString*)firstName lastName:(NSString*)lastName gender:(NSString*)gender mobile:(NSString*)mobile  email:(NSString*)email{
    [self removeInfoVCAnimated];
    NSString *body = [self selfBodyContent:firstName lastname:lastName email:email mobile:mobile];
    [self mailSending:body];
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.widgetTileInfo.widget)
    {
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:[_itemId longValue] widgetType:@"Sponsor-ContactUs-Continue" deviceType:@"iOS"];
    }
    else{
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:[_itemId longValue] widgetType:@"Sponsor-ContactUs-Continue" deviceType:@"iOS"];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)dealloc
{
    [txtDesc release];
    [viewSponsorDesc release];
    [myScrollView release];
    [mySponsorImg release];
    [viewSponsorDesc release];
    [btnInfo release];
    
    [_bannerView release];
    [_bannerImg release];
    [_bannerCancelBtn release];
    [super dealloc];
}


- (void)viewDidUnload {
    
    [self setMyScrollView:nil];
    [self setMySponsorImg:nil];
    [self setLblSponsorTitle:nil];
    [self setTxtDesc:nil];
    [self setViewSponsorDesc:nil];
    [self setBtnInfo:nil];
    
    [super viewDidUnload];
}

- (IBAction)bannerCancelBtn:(id)sender {
    
    
    
    [self.bannerView setAlpha:0];
    adsremoved = YES;
    self.bannerCancelBtn.hidden = YES;
    self.bannerCancelBtn.enabled = NO;
    [[NSUserDefaults standardUserDefaults] setBool:adsremoved forKey:@"adsremoved"];
    //use NSUserDefaults so that you can load wether or not they bought it
    [[NSUserDefaults standardUserDefaults] synchronize];
    

}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
//    NSLog(@"[AppDelegate shareddelegate].bannersDict %@",[AppDelegate shareddelegate].bannersDict);
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Sponsors"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
//    NSLog(@"self.bannerUrlsArray %@",self.aBannerDict);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
//        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryActionBanners];
}

- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryActionBanners
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Listing" deviceType:@"iOS"];
    
    
}




@end
