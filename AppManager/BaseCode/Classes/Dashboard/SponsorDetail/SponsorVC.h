//
//  SponsorVC.h
//  Exceeding
//
//  Created by Rajesh Kumar on 19/02/13.
//
//

#import "AppMainVC.h"
#import <MessageUI/MessageUI.h>
#import "InfoCollectorVC.h"
#import "BigBanners.h"



@interface SponsorVC : AppMainVC<UIWebViewDelegate, UIScrollViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, InfoCollectorDelegate,BigBannersDelegate>
{
    NSInteger aboutImgHeight;
    IBOutlet UILabel *aboutStaticTitle;
    NSString *phoneNumber;
    NSString *mail, *sponsorName;
    BOOL adsremoved;
    
}


@property (nonatomic, strong) NSMutableArray *bannerURLS;


@property (strong, nonatomic)NSDictionary *sponserBannerDict;

@property (retain, nonatomic) IBOutlet UIImageView *mySponsorImg;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
@property (nonatomic, retain) InfoCollectorVC *infoCollVC;
@property (retain, nonatomic) IBOutlet UIView *bannerView;


@property (nonatomic, retain) IBOutlet UIView *viewSponsorDesc;
@property (nonatomic, retain) IBOutlet UITextView *txtDesc;
@property (nonatomic, retain) IBOutlet UILabel *lblSponsorTitle;
@property (nonatomic, retain) IBOutlet UILabel *lblPrice;
@property (nonatomic, retain) IBOutlet UIScrollView *myScrollView;
@property (nonatomic, retain) IBOutlet UIButton *btnSponsors;
@property (nonatomic, retain) NSNumber *itemId;
@property (retain, nonatomic) IBOutlet UIButton *btnInfo;

@property (nonatomic, retain) NSString *websiteURL;
@property (nonatomic, strong) BigBanners *bigBanner;

- (IBAction)actInfo:(id)sender;
- (IBAction)contactUs:(id)sender;

@end
