//
//  AppWebVC.m
//  Exceedings
//
//  Created by Rajesh Kumar Yandamuri on 19/07/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "AppWebVC.h"
#import "DashboardBuilder.h"
#import "Utils.h"
#import "SDZEXWidgetServiceExample.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "DBApp.h"
#import "SDZEXAnalyticsServiceExample.h"


@interface AppWebVC ()
@property (retain, nonatomic) IBOutlet UIWebView *webFacebook;
@property (retain, nonatomic) IBOutlet UIWebView *webTwitter;
@end

@implementation AppWebVC
@synthesize activityIndicator;
@synthesize myWebView;
@synthesize webFacebook, webTwitter;

#pragma mark - Data Source

-(void)setDataSourceDic:(NSDictionary *)dataSourceDic{
    [super setDataSourceDic:dataSourceDic];
    
}

#pragma mark - Override superclass

-(void)createNewWebView{
    self.myWebView = [[[UIWebView alloc] initWithFrame:self.view.frame] autorelease];
    myWebView.delegate = self;
}

-(IBAction)subTabOptionClicked:(UIButton*)sender{
    [super subTabOptionClicked:sender];
    
    switch (self.appType) {
        case APP_SOCIAL:
        {
            if (myWebView) {
                [myWebView removeFromSuperview];
            }
            
            
            switch (sender.tag) {
                case 0:
                {
                    self.tabType = TAB_FACEBOOK;
                    
                    if (!webFacebook) {
                        [self createNewWebView];
                        self.webFacebook = myWebView;
                        
                        myWebView.frame = CGRectMake(0, 2*kBarHeight, self.view.frame.size.width, self.view.frame.size.height - 2*kBarHeight);
                    }
                    else{
                        self.myWebView = webFacebook;
                    }
//#warning  facebook fanpage fixed
                    [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:kFBFanPageURL, [AppDelegate shareddelegate].appReference.socFBPageID]]]];
                    
                    //Realign webview
                    [self.view addSubview:myWebView];
                    [self.view sendSubviewToBack:myWebView];
                    
                    //
                    
                }
                    break;
                case 1:
                {
                    self.tabType = TAB_TWITTER;
                    
                    if (!webTwitter) {
                        [self createNewWebView];
                        self.webTwitter = myWebView;
                        
                        self.myWebView.frame = CGRectMake(0, 2*kBarHeight, self.view.frame.size.width, self.view.frame.size.height - kBarHeight);
                    }
                    else{
                        self.myWebView = webTwitter;
                    }

//#warning Twitter string appending ...
                    
                    [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@", [AppDelegate shareddelegate].appReference.socTwitterBrandName] ]]];
                    
                    //Realign webview
                    [self.view addSubview:myWebView];
                    [self.view sendSubviewToBack:myWebView];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case APP_MEDIA:
        {
            
        }
            break;  
        case APP_ABOUT:
        {
            [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[AppDelegate shareddelegate].appReference.supportURL]]];
        }
            break;
        case APP_WEBLINK:
        {
            [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.widgetTileInfo.webLinkURL]]];
        }
            break;
        default:
            break;
    }
}

#pragma Life cycle


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view from its nib.
    //SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    //[example1 runAboutWidget];
    
    if (self.appType == APP_MEDIA || self.appType == APP_SOCIAL) {
        
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        NSString *appType = nil;
        if(self.appType == APP_MEDIA)
        {
            appType = @"Listing";
        }
        else if(self.appType == APP_SOCIAL)
        {
            appType = @"Detail";
        }
        else if(self.appType == APP_SOCIAL)
        {
            appType = @"Detail";
        }
        if(!self.widgetTileInfo.widget)
        {
            [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] integerValue] widgetItemId:0 widgetType:appType deviceType:@"iOS"];
        }
        else{
            [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:appType deviceType:@"iOS"];
        }
        
        [self performSelectorOnMainThread:@selector(subTabOptionClicked:) withObject:[self.subTabBtnArray objectAtIndex:0] waitUntilDone:NO];
    }
    
}

-(void)bailOut{
    myWebView.delegate = nil;
    self.myWebView = nil;
    
    webFacebook.delegate = nil;
    self.webFacebook = nil;
    
    webTwitter.delegate = nil;
    self.webTwitter = nil;
}

- (void)viewDidUnload
{
    [self bailOut];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)dealloc {
    [self bailOut];
    [activityIndicator release];
    [_bannerView release];
    [_bannerImg release];
    [_bannerCancelBtn release];
    [super dealloc];
}

#pragma mark - UIWebView
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"webView: shouldStartLoadWithRequest");
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSLog(@"webView: webViewDidStartLoad");
    
    [self.view bringSubviewToFront:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"webView: webViewDidFinishLoad");
    
    [activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"webView: didFailLoadWithError: %@", error);
    
    [activityIndicator stopAnimating];
}



- (IBAction)backWebAction:(id)sender {
    [myWebView goBack];
}


- (IBAction)bannerCancelBtn:(id)sender {
}
- (IBAction)youTubeBannerWebBtn:(id)sender {
}
@end
