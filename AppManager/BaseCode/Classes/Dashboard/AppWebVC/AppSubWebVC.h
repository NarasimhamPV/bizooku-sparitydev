//
//  AppSubWebVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainDetailVC.h"
#import "DBTilesInfo.h"
#import "BigBanners.h"


@interface AppSubWebVC : AppMainDetailVC <UIWebViewDelegate, UIScrollViewDelegate,BigBannersDelegate>

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UIWebView *myWebView;
@property (strong, nonatomic) DBTilesInfo *tileInfo;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
