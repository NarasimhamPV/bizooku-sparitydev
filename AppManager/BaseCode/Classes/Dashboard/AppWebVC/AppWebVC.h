//
//  AboutUsVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainVC.h"

@interface AppWebVC : AppMainVC <UIWebViewDelegate, UIScrollViewDelegate>

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)youTubeBannerWebBtn:(id)sender;
@property (retain, nonatomic) IBOutlet UIWebView *myWebView;
@property (retain, nonatomic) IBOutlet UIView *bannerView;
- (IBAction)backWebAction:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
- (IBAction)bannerCancelBtn:(id)sender;

@end
