//
//  AppWebVC.m
//  Exceedings
//
//  Created by Rajesh Kumar Yandamuri on 19/07/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "AppSubWebVC.h"
#import "DashboardBuilder.h"
#import "Utils.h"
#import "SDZEXWidgetServiceExample.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "DBApp.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"

@interface AppSubWebVC ()

//Banner
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation AppSubWebVC
@synthesize activityIndicator;
@synthesize myWebView;
@synthesize tileInfo = _tileInfo;


#pragma mark - Override superclass
       
#pragma Life cycle


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //[myWebView stringByEvaluatingJavaScriptFromString:@"document.characterSet='utf-8';"];

    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}
- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    

    [AppDelegate shareddelegate].selectedTileInfo = _tileInfo;
    
    if (self.appType == APP_ABOUT) {
       
        
        NSString *urlStr = [@"http://" stringByAppendingString:[AppDelegate shareddelegate].appReference.supportURL];
        
         [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
    }
    else if(self.appType == APP_WEBLINK)
    {
        self.navigationItem.title = _tileInfo.webLinkPageTitle;
        self.headerObject.headerLabel.text = _tileInfo.webLinkPageTitle;
        
        if(_tileInfo && ![_tileInfo isKindOfClass:[NSNull class]])
        {
            //NSString *urlStr = [NSString stringWithFormat:@"%@", _tileInfo.webLinkURL];
            
            NSString *correctString = [NSString stringWithCString:[_tileInfo.webLinkURL cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
            /*
            NSData *urlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"]]];
            [myWebView loadData:urlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:nil];
*/
            
            
            [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[correctString stringByReplacingOccurrencesOfString:@" " withString:@"%20"]]]];
            
            
        }
    }
}

-(void)bailOut{
    myWebView.delegate = nil;
    self.myWebView = nil;
    
}

- (void)viewDidUnload
{
    [self bailOut];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Weblink"]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;
        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];

    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


- (void)dealloc {
    [self bailOut];
    [activityIndicator release];
    [super dealloc];
}

#pragma mark - UIWebView
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"webView: shouldStartLoadWithRequest");
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSLog(@"webView: webViewDidStartLoad");
    
    [self.view bringSubviewToFront:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"webView: webViewDidFinishLoad");
    
    [activityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"webView: didFailLoadWithError: %@", error);
    
    [activityIndicator stopAnimating];
}


@end
