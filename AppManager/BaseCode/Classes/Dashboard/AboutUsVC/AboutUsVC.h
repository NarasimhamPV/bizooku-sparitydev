//
//  AboutUsVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainVC.h"
#import "BigBanners.h"



@interface AboutUsVC : AppMainVC <UIWebViewDelegate, UIScrollViewDelegate, UIActionSheetDelegate, UIGestureRecognizerDelegate,BigBannersDelegate>
{
    IBOutlet UIButton *btnCall;
    IBOutlet UIButton *btnWeb;
    IBOutlet UIButton *btnDirection;
    NSString *phoneNumber;
    NSInteger aboutImgHeight;
    CGFloat latitude, longitude;
    
}


@property (retain, nonatomic) NSArray *bannersArray;
@property (retain, nonatomic) NSDictionary *bannersDict;
@property (retain, nonatomic) NSString *websiteURL;
@property (retain, nonatomic) IBOutlet UIWebView *myWebView;
@property (retain, nonatomic) IBOutlet UIImageView *myAboutImg;
- (IBAction)aboutBannerCancelBtn:(id)sender;

@property (retain, nonatomic) IBOutlet UIView *bannerView;
@property (retain, nonatomic) IBOutlet UIButton *aboutBannerCancel;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (retain, nonatomic) IBOutlet UITextView *aboutLabel;
@property (retain, nonatomic) IBOutlet UILabel *orgNameLabel;
@property (retain, nonatomic) IBOutlet UITextView *visionLabel;

@property (retain, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (retain, nonatomic) IBOutlet UIView *aboutView;
@property (retain, nonatomic) IBOutlet UIView *visionView;

@property (retain, nonatomic) IBOutlet UILabel *aboutStaticTitle;
@property (retain, nonatomic) IBOutlet UILabel *visionStaticTitle;
@property (retain, nonatomic) IBOutlet UIImageView *visionLineImageView;
@property (retain, nonatomic) IBOutlet UIButton *btnInfo;
@property (retain, nonatomic) IBOutlet UILabel *deviceTokenLabel;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;
@property (nonatomic, strong) BigBanners *bigBanner;



- (IBAction)actCall:(id)sender;
- (IBAction)actDirections:(id)sender;
- (IBAction)actWeb:(id)sender;
- (IBAction)actInfo:(id)sender;

@end
