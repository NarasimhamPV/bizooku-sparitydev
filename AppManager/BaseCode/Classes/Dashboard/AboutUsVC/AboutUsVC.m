//
//  AboutUsVC.m
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import "AboutUsVC.h"
#import "DashboardBuilder.h"
#import "Utils.h"
#import "SDZEXWidgetServiceExample.h"
#import "UIImageView+WebCache.h"
#import "UILabel+Exceedings.h"
#import "AppWebVC.h"
#import "AppSubWebVC.h"
#import "AppDelegate.h"
#import "PhoneNumberFormatter.h"

#import "SDZEXAnalyticsServiceExample.h"
#import "DBTilesInfo.h"
#import "DBWidget.h"

#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"
#import "AppDelegate.h"

#import "AppTableVC.h"


@interface AboutUsVC ()

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;

@property (nonatomic, strong) NSMutableArray *actionSheetTitleArray;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation AboutUsVC


@synthesize myWebView, bannersDict, bannersArray;
@synthesize myAboutImg;
@synthesize aboutLabel;
@synthesize orgNameLabel;
@synthesize visionLabel;
@synthesize myScrollView;
@synthesize aboutView;
@synthesize visionView;
@synthesize aboutStaticTitle;
@synthesize visionStaticTitle;
@synthesize btnInfo;
@synthesize websiteURL = _websiteURL;


#pragma mark - Data Source



-(void)setDataSourceDic:(NSDictionary *)dataSourceDic{
    [super setDataSourceDic:dataSourceDic];
//    NSLog(@"dataSourceDic %@",dataSourceDic);
    
    CGFloat spacing = 10;
    CGFloat allWidth = myScrollView.frame.size.width - 2*spacing;
    
    ///Step 1: Set text///
    //News Image
    NSString *img = [dataSourceDic valueForKey:@"Image"];
    BOOL haveImage = (img != nil && img.length != 0);
    
    if(haveImage)
    {
        [myAboutImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
        
        [[NSUserDefaults standardUserDefaults]setObject:img forKey:@"aboutImage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSString *orgText = [dataSourceDic valueForKey:@"Title"];
    
    orgNameLabel.text = orgText;

    NSString *aboutText = [dataSourceDic valueForKey:@"About"];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        NSMutableParagraphStyle *style =  [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
        [style setAlignment: NSTextAlignmentLeft];
        
        NSAttributedString * subText1 = [[[NSAttributedString alloc] initWithString:aboutText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Condensed" size:16.0f],
                                                                                                           NSParagraphStyleAttributeName : style
                                                                                                           }] autorelease];
        aboutLabel.attributedText = subText1;
    }
    else{
        aboutLabel.text = aboutText;
    }
    
    
    
    
    // [aboutLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:30]];
    
    //aboutLabel.text = aboutText;
    
    
    self.websiteURL = [dataSourceDic objectForKey:@"Website"];
    
    
    NSString *visionText = [dataSourceDic valueForKey:@"Vision"];
    
    if ([visionText length]) {
        
        self.visionStaticTitle.hidden = NO;
        self.visionLineImageView.hidden = NO;
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0"))
    {
        NSMutableParagraphStyle *style =  [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
        [style setAlignment: NSTextAlignmentLeft];
        
        NSAttributedString * subText1 = [[[NSAttributedString alloc] initWithString:visionText attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica-Condensed" size:16.0f],
                                                                                                            NSParagraphStyleAttributeName : style
                                                                                                            }] autorelease];
        
        visionLabel.attributedText = subText1;
    }
    else{
        visionLabel.text = visionText;
    }
    
    
    
    phoneNumber = [dataSourceDic valueForKey:@"Phone"];
    
    NSDictionary *locDic = [dataSourceDic objectForKey:@"Location"];
    if (locDic && ![locDic isKindOfClass:[NSNull class]]) {
        latitude = [[locDic valueForKey:@"Lattitude"] floatValue];
        longitude = [[locDic valueForKey:@"Longitude"] floatValue];
    }
    //Fit text for sizes
    [orgNameLabel resizeToFitAbout];

    orgNameLabel.frame = CGRectMake(spacing, aboutImgHeight + 10, myScrollView.frame.size.width - 2*spacing, orgNameLabel.frame.size.height);
    
    
    aboutLabel = [AppDelegate resizeTheTextView:aboutLabel];
    visionLabel = [AppDelegate resizeTheTextView:visionLabel];
    
    
    //Resize views according to these sizes
    aboutView.frame = CGRectMake(0, 0, aboutView.frame.size.width, aboutLabel.frame.origin.y + aboutLabel.frame.size.height + 10);
    visionView.frame = CGRectMake(0, 0, visionView.frame.size.width, visionLabel.frame.origin.y + visionLabel.frame.size.height + 10);
    
    //Step 2: Now align all
    
    CGRect lastRect = CGRectZero;
    CGFloat lastHighHeight = 0.0;
    
    //Image
    {
        lastRect = CGRectMake(spacing, 0, allWidth, 0);
        
        if (!haveImage) {
            lastRect.size.height = 0.0;
            lastHighHeight = 10.0;
            myAboutImg.frame = lastRect;
        }
        else {
            [myAboutImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL,img]]];
            lastRect =  CGRectMake(lastRect.origin.x, lastRect.origin.y + lastHighHeight, allWidth, myAboutImg.frame.size.height);
            lastHighHeight = 180;
            
            myAboutImg.frame = CGRectMake(0, 0, 320, aboutImgHeight);
        }
    }
    
    //Organization text
    CGRect orgRect = orgNameLabel.frame;
    orgNameLabel.hidden = (orgText == nil);
    if (orgText) {
        aboutView.frame = CGRectMake(spacing, orgRect.origin.y + orgRect.size.height + 3, aboutView.frame.size.width, aboutView.frame.size.height);
    }
    else {
        aboutView.frame = CGRectMake(spacing, orgRect.origin.y, aboutView.frame.size.width, aboutView.frame.size.height);
    }
    
    //About text
    CGRect aboutRect = aboutView.frame;
    aboutView.hidden = (aboutText == nil);
    if (aboutText) {
        visionView.frame = CGRectMake(spacing, spacing*2 + aboutRect.origin.y + aboutRect.size.height, visionView.frame.size.width, visionView.frame.size.height);
    }
    else {
        visionView.frame = CGRectMake(spacing, spacing*2 + aboutRect.origin.y, visionView.frame.size.width, visionView.frame.size.height);
    }
    
    //Vision Text
    visionView.hidden = (visionText == nil);
    
    //Finally set scrolling
    myScrollView.contentSize = CGSizeMake(myScrollView.frame.size.width, visionView.frame.origin.y + visionView.frame.size.height + spacing );
    
    //Action Sheet
    
    if (![self.websiteURL isEqualToString:@""]){
        [self.actionSheetTitleArray addObject:@"Website"];
        
    }
    if (![phoneNumber isEqualToString:@"--"]){
        [self.actionSheetTitleArray addObject:@"Call"];
        
    }
    if (latitude){
        [self.actionSheetTitleArray addObject:@"Directions"];
        
    }
    if ([self.actionSheetTitleArray count] == 0){
        self.btnInfo.hidden = YES;
    }else{
        self.btnInfo.hidden = NO;

    }
}


#pragma Life cycle


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        aboutImgHeight = 240;
    }
    return self;
}


- (void) viewWillAppear:(BOOL)animated
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.selectedBannerDict = [[NSMutableDictionary alloc] init];

    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"PushToken"];
    self.headerObject.iconSeperator.hidden = YES;
    self.btnInfo.hidden = YES;

    self.actionSheetTitleArray = [[NSMutableArray alloc] init];
    
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    self.deviceTokenLabel.text = deviceToken;
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,568, 320, 60);
    [self.view addSubview:self.addBannerView];

    [self.view bringSubviewToFront:self.addBannerView];
    [self addBannerData];
    
    
    orgNameLabel.hidden = YES;
    aboutView.hidden = YES;
    visionView.hidden = YES;
    
    [orgNameLabel sizeLabelForTitle];
    
    [aboutLabel setTextColor:[UIColor darkGrayColor]];
    
    [visionLabel setTextColor:[UIColor darkGrayColor]];
    
    [aboutStaticTitle sizeLabelForAboutStatic];
    [visionStaticTitle sizeLabelForAboutStatic];
    
    // Do any additional setup after loading the view from its nib.
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runAboutWidget];
    
    [self.view bringSubviewToFront:btnInfo];
}

- (void)addBannerData
{
    NSMutableArray *localTempArry = [NSMutableArray array];
    NSMutableArray *timearr = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"About"]) {
            self.aboutBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aboutBannerDict objectForKey:@"BannersList"]];
            
            [timearr addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"Duration"]];
            break;
        }
        
    }
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aboutBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aboutBannerDict;
        
        
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
    
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)createNewAppTableType{
    
//    NSLog(@"_loadedTileInfo %@",_loadedTileInfo);
    
    AppTableVC *appTableVC = [[[AppTableVC alloc]initWithNibName:@"AppTableVC" bundle:nil] autorelease];
//    tileBodyVC = appTableVC;
    
    appTableVC.widgetConstantType = @"Listing";
    //appTableVC.widgetTileInfo = _loadedTileInfo;
    appTableVC.appType = APP_VOLUNTEER;
    appTableVC.noDataAvailableLbl.hidden = YES;
    [self.navigationController pushViewController:appTableVC animated:YES];
//    return YES;
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}


- (IBAction)actInfo:(id)sender
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:0 action:@"Entry" deviceType:@"iOS"];
    }
    else{
        [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:0 action:@"Entry" deviceType:@"iOS"];
    }
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Information"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    // ObjC Fast Enumeration
    for (NSString *title in self.actionSheetTitleArray) {
        [actionSheet addButtonWithTitle:title];
    }
    
    [actionSheet setCancelButtonIndex: [actionSheet addButtonWithTitle:@"Cancel"]];

    
    
    [actionSheet showInView:self.view];
    
    /*
    UIActionSheet *actinsheet = [[[UIActionSheet alloc] initWithTitle:@"Information" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Website", @"Directions", @"Call", nil] autorelease];
    
    [actinsheet showInView:self.view];
     */
}



#pragma mark - UIActionSheet Delegates


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    NSString *strAction = nil;
    
    switch (buttonIndex)
    {
        case 0:
        case 1:
        case 2:
        {
            if ([self.actionSheetTitleArray count] > buttonIndex){
                NSString *tempStr = [self.actionSheetTitleArray objectAtIndex:buttonIndex];
                if ([tempStr isEqualToString:@"Call"]){
                    strAction = @"Call";
                    
                    [self actCall:nil];
                    
                }else if ([tempStr isEqualToString:@"Website"]){
                    strAction = @"Website";
                    
                    [self actWeb:nil];
                    
                }
                else if ([tempStr isEqualToString:@"Directions"]){
                    strAction = @"Directions";
                    
                    [self actDirections:nil];
                    
                }
                
            }else{
                strAction = @"Canceled";
                
            }
            
        }
            break;
            
        default:
            break;
    }
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [service runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:0 action:strAction deviceType:@"iOS"];
    }
    else{
        [service runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.widgetTileInfo.widget.widgetID integerValue] widgetItemId:0 action:strAction deviceType:@"iOS"];
    }
    
    
}


#pragma mark - Btn Actions



- (NSString *)checkPhoneNumber: (NSString *)number
{
    NSString *str = [[[[number stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

- (IBAction)actCall:(id)sender
{
    PhoneNumberFormatter *formatter = [[[PhoneNumberFormatter alloc] init] autorelease];
    NSString *number = [formatter format:phoneNumber withLocale:@"us"];
    
    NSURL *callURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [self checkPhoneNumber:number]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:callURL]) {
        [[UIApplication sharedApplication] openURL:callURL];
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to dial the number. Please dial the number '%@' manually.", number ]];
    }
    
}

- (IBAction)actDirections:(id)sender
{
    CLLocationCoordinate2D origin = [AppDelegate shareddelegate].currentLocation.coordinate;
    CLLocationCoordinate2D destination = { latitude, longitude };
    
    NSString *appleMapsURLStr = [NSString stringWithFormat:@"maps://q=&saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f&view=map", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    NSURL *appleURL = [NSURL URLWithString:appleMapsURLStr];
    
    appleURL = nil;
    
    //    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f", origin.latitude, origin.longitude, destination.latitude, destination.longitude];
    
    
    NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%1.6f,%1.6f",destination.latitude, destination.longitude];
    NSURL *appleMapUrl = [NSURL URLWithString:urlString];
    
    
    //    NSURL *url = [NSURL URLWithString:googleMapsURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:appleURL]) {
        [[UIApplication sharedApplication] openURL:appleURL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:appleMapUrl]) {
        [[UIApplication sharedApplication] openURL:appleMapUrl];
        
        
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Unable to find directions"]];
    }
}

- (IBAction)actWeb:(id)sender {
    
    /* AppSubWebVC *web = [[[AppSubWebVC alloc] initWithNibName:@"AppSubWebVC" bundle:nil] autorelease];
     web.appType = APP_ABOUT;
     [self.navigationController pushViewController:web animated:YES];*/
    
    if(_websiteURL && ![_websiteURL isEqualToString:@""])
    {
        // NSString *urlStr = [@"http://" stringByAppendingString:_websiteURL];
        
        NSURL *url = [NSURL URLWithString:_websiteURL];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"Website not available"]];
        }
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"website not configured"]];
    }
}



- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIView *_currentView in actionSheet.subviews) {
        if ([_currentView isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel *)_currentView;
            
            [lbl setFrame:CGRectMake(0, 5, 320, 40)];
            [lbl setTextAlignment:NSTextAlignmentCenter];
            
            [((UILabel *)_currentView) setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:22]];
        }
    }
}



- (void)viewDidUnload
{
    [self setMyWebView:nil];
    [self setMyAboutImg:nil];
    [self setAboutLabel:nil];
    [self setMyScrollView:nil];
    [self setOrgNameLabel:nil];
    [self setVisionLabel:nil];
    [self setAboutView:nil];
    [self setVisionView:nil];
    [btnCall release];
    btnCall = nil;
    [btnDirection release];
    btnDirection = nil;
    [btnWeb release];
    btnWeb = nil;
    [self setAboutStaticTitle:nil];
    [self setVisionStaticTitle:nil];
    [self setBtnInfo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

/*
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
 {
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)dealloc {
    [myWebView release];
    [myAboutImg release];
    [aboutLabel release];
    [myScrollView release];
    [orgNameLabel release];
    [visionLabel release];
    [aboutView release];
    [visionView release];
    [btnCall release];
    [btnDirection release];
    [btnWeb release];
    [aboutStaticTitle release];
    [visionStaticTitle release];
    [btnInfo release];
    [_bannerView release];
    [_bannerImg release];
    [_aboutBannerCancel release];
    [super dealloc];
}


@end
