//
//  ContributeVC.h
//  Exceedings
//
//  Created by Betrand Yella on 13/06/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainVC.h"

@interface ContributeVC : AppMainVC <UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;
}



@end
