//
//  AppHeaderBar.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppHeaderBar.h"
#import "Utils.h"
#import "AppMainDetailVC.h"
#import "SPSingletonClass.h"

@implementation AppHeaderBar

//Header and subheader
@synthesize mainHeader = _mainHeader;
@synthesize subTabsView = _subTabsView;
//
@synthesize headerLabel, headerLeftBtn; 
@synthesize rightToolbarItem = _rightToolbarItem;
@synthesize delegate = _delegate;
@synthesize btnClose, btnOptions, btnSearch, rightToolBar, btnShare, btnAdd, btnLocation;
@synthesize iconSeperator = _iconSeperator;




-(void)setRightToolbarItem:(NSArray *)rightToolbarItem
{
    _rightToolbarItem = [rightToolbarItem retain];
    
    NSInteger itemSpacing = 10;
    NSInteger itemSize = 40;
    
    NSInteger toolbarWidth = (rightToolbarItem)?(4 + (itemSpacing+itemSize)*[_rightToolbarItem count]):0;
    
    //Set Frame
    rightToolBar.frame = CGRectMake(_mainHeader.frame.size.width - toolbarWidth, 0, toolbarWidth, kBarHeight);
    
    
    //Place items
    for (NSInteger i = 0; i<[_rightToolbarItem count]; i++) {
        UIButton *btn = [_rightToolbarItem objectAtIndex:i];
        btn.frame = CGRectMake(itemSpacing + i*(itemSpacing + itemSize), 0, itemSize, kBarHeight);
        if(i == 0)
        {
            [btn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        if(i == 1)
        {
            _iconSeperator.frame = CGRectMake((itemSpacing + itemSize)+itemSpacing/2, _iconSeperator.frame.origin.y, _iconSeperator.frame.size.width, _iconSeperator.frame.size.height);
            [btn addTarget:self action:@selector(rightSecondBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        [rightToolBar addSubview:btn];
    }
    
    [headerLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:22]];
}


- (id)initWithDelegate:(id)delegate appType:(AppType)appType
{
    self = [super init];
    if (self) {
        // Initialization code
        
        headerLabel = [[UILabel alloc] init];
        self.delegate = delegate;
        
        [[NSBundle mainBundle] loadNibNamed:@"AppHeader" owner:self options:nil];
        
        //Lets change color to clear color since in xib its black color for visibility
        
        btnShare.backgroundColor = btnSearch.backgroundColor = btnOptions.backgroundColor = btnAdd.backgroundColor = [UIColor clearColor];
        NSLog(@"Title %d",appType);
        
        if ([_delegate isKindOfClass:[AppMainDetailVC class]]) {
            headerLabel.text = [Utils detailTitleForAppType:appType];
            
            if (appType == APP_ABOUT) {
                headerLabel.text = @"Web";
            }
            
            switch (appType) {
                case APP_NEWS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                    //self.rightToolbarItem = nil;
                }
                    break;
                case APP_LOCATIONS:
                {
                    self.rightToolbarItem = nil;
                    //self.rightToolbarItem = [NSArray arrayWithObjects:btnShare,nil];
                }
                    break;
                case APP_MEDIA:
                {
                    //self.rightToolbarItem = nil;
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                 
                //AUDIO
                case APP_AUDIO:
                {
                    self.rightToolbarItem = nil;
                }
                    break;
                case APP_FREE_AUDIO_List:
                {
                    self.rightToolbarItem = nil;
                }
                    break;
                case APP_AUDIO_DETAIL:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects: btnOptions, btnShare, nil];
                }
                    
//                    case APP_AUDIO_DETAIL:
//                {
//                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
//                    
//                }
                    break;
                case APP_AUDIO_PURCHASE:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects: btnOptions, btnShare, nil];
                }
                    break;
                case APP_VIDEO:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                    
                }
                    break;
                    //SL
                case APP_COUPONS:
                    
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                    headerLabel.text = [[SPSingletonClass sharedInstance] couponTitleStr];
                    
                }
                    
                    break;
                    
                case APP_COUPONSLIST:
                {
                    self.rightToolbarItem = nil;
                    headerLabel.text = [[SPSingletonClass sharedInstance] couponTitleStr];

                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    
                    break;
                 
                    case APP_COUPONSDETAILS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                  

                    
                case APP_EVENTS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];

                }
                    break;
                case APP_SOCIAL:
                {
                    self.rightToolbarItem = nil;
                    //self.rightToolbarItem = [NSArray arrayWithObjects:btnAdd, nil];
                }
                    break;
                case APP_ABOUT:
                {
                    self.rightToolbarItem = nil;
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];

                }
                
                    break;
                case APP_CONTRIBUTE:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_FUNDRAISING:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_VOLUNTEER:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_SPONSOR:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    
                    break;
                case APP_PRODUCTS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_WEBLINK:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                    
                case APP_INAPP:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                    
                    

                default:
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];

                    break;
            }
        }
        else{
            
            if(appType != APP_PRODUCTS)
            {
                headerLabel.text = [Utils titleForAppType:appType];
            }
            NSLog(@"appType %d",appType);
            
            if(appType != APP_COUPONSLIST)
            {
                headerLabel.text = [Utils titleForAppType:appType];
            }
            switch (appType) {
                case APP_NEWS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                }
                    break;
                case APP_LOCATIONS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                }
                    break;
                case APP_MEDIA:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                }
                    break;
                case APP_EVENTS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                  //  headerLabel.text =@"";
                }
                    break;
                case APP_SOCIAL:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_ABOUT:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                //AUDIO
                case APP_AUDIO:
                {
                    self.rightToolbarItem = nil;
                }
                    break;
                case APP_FREE_AUDIO_List:
                {
                    self.rightToolbarItem = nil;
                }
                    break;
                case APP_AUDIO_DETAIL:
                {
                    
                    self.rightToolbarItem = [NSArray arrayWithObjects: btnOptions, btnShare, nil];
                }
                    break;
                case APP_AUDIO_PURCHASE:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects: btnOptions, btnShare, nil];
                }
                    break;

                case APP_COUPONSDETAILS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_CONTRIBUTE:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_FUNDRAISING:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                }
                    break;
                case APP_VOLUNTEER:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                }
                    break;
                case APP_SPONSOR:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_PRODUCTS:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];

                }
                    break;
                case APP_COUPONS: //SL
                {

                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                }
                    break;
                    case APP_COUPONSLIST:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                }
                    break;
            
                case APP_WEBLINK:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                    
                case APP_INAPP:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnShare, nil];
                }
                    break;
                case APP_VIDEO:
                {
                    self.rightToolbarItem = [NSArray arrayWithObjects:btnSearch, nil];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
        
        
    }
    return self;
}

- (IBAction)leftBtnAction:(id)sender {
    [_delegate leftBtnAction:sender];
}

- (IBAction)rightBtnAction:(id)sender {
    [_delegate headerBtnAction:sender];
}


- (IBAction) rightSecondBtnAction:(id)sender
{
    [_delegate headerSecondBtnAction:sender];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)dealloc{
    [rightToolBar release];
    rightToolBar = nil;
    [btnSearch release];
    btnSearch = nil;
    [btnOptions release];
    btnOptions = nil;
    [btnAdd release];
    btnAdd = nil;
    [btnShare release];
    btnShare = nil;
    [btnClose release];
    btnClose = nil;
    
    [super dealloc];
}

@end
