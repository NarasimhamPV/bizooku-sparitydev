//
//  AudioDetailViewController.m
//  Exceeding
//
//  Created by NarasimhamPV on 31/05/14.
//
//

#import "AudioDetailViewController.h"
#import "AppDelegate.h"
#import "SDZEXWidgetServiceExample.h"
#import "SPSingletonClass.h"
#import "AudioCollectionViewCell.h"
#import "AudioPurchaseViewController.h"

#import "UILabel+Exceedings.h"

#import "CustomBannerView.h"
#import "WebViewController.h"

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

static NSString *const kcustomAudioCollectionCellIdentifier = @"audiosCellIdentifier";
static NSString *const kcustomAudioCollectionNib = @"AudioCollectionViewCell";


@interface AudioDetailViewController () <AVAudioPlayerDelegate, AVPlayerItemOutputPullDelegate,NSURLSessionDelegate,NSURLSessionDownloadDelegate,AVAudioPlayerDelegate>

@property (retain, nonatomic) IBOutlet UILabel *descriptionLbl;

@property (retain, nonatomic) IBOutlet UIImageView *audioImageView;
@property (retain, nonatomic) IBOutlet UILabel *audioTitleLable;
@property (retain, nonatomic) IBOutlet UISlider *mScrubber;
@property (retain, nonatomic) IBOutlet UILabel *audioMaxTimeLabel;
@property (retain, nonatomic) IBOutlet UIButton *audioPlayButton;
@property (retain, nonatomic) IBOutlet UISlider *volumeSlider;
@property (retain, nonatomic) IBOutlet UIButton *volumeButton;
@property (retain, nonatomic) IBOutlet UIButton *fullVolumeButton;
@property (retain, nonatomic) IBOutlet UITextView *audionDescriptionView;
@property (retain, nonatomic) IBOutlet UILabel *audioCurrentSeekLable;

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;

@property (retain, nonatomic) IBOutlet UIScrollView *detailScrollView;

@property (retain, nonatomic) IBOutlet UIButton *repeatButton;

@property (nonatomic, assign) BOOL isVolumeMuted;
@property (nonatomic, assign) BOOL isBGVolumeMuted;
@property (nonatomic, strong) NSMutableArray *totalBGAudiosArray;

@property (nonatomic, assign) BOOL isReplayButtonPressed;
@property (nonatomic, assign) BOOL isRepeatSelected;
@property (nonatomic,strong) IBOutlet UIButton *backGroundMusicBtn;

@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

- (IBAction)audioSeekValueChanged:(id)sender;
- (IBAction)playButtonAction:(id)sender;
- (IBAction)volumeChanged:(id)sender;


- (IBAction)repeatButtonAction:(id)sender;

- (IBAction)volumeButtonAction:(id)sender;
- (IBAction)fullVolumeButtonAction:(id)sender;

- (IBAction)moveToBgAudios:(id)sender;
- (IBAction)showAudioInfo:(id)sender;

@end

@implementation AudioDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self entryAction];
    
    self.headerObject.headerLabel.text = @"Audio";
    self.headerObject.iconSeperator.hidden = YES;
    [[self.headerObject.rightToolbarItem objectAtIndex:0] setHidden:YES];

    [self.audioCurrentSeekLable setAdjustsFontSizeToFitWidth:YES];
    [self.audioMaxTimeLabel setAdjustsFontSizeToFitWidth:YES];

    self.detailDataDictionary = self.audioDetailDictionary;
    
    self.backGroundMusicBtn.hidden = YES;
    
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    
    
    if ([[SPSingletonClass sharedInstance] isViewDetailLoaded])
    {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveAudioDetailNotification:)
                                                     name:@"getAudioDetailtNotification"
                                                   object:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:[self.audioDetailDictionary objectForKey:@"ItemId"] forKey:@"AudioId"];
        [example1 getAudioDetailsByAudioID];

    }else{
        [self loadData];
//        [[NSUserDefaults standardUserDefaults] setObject:[self.audioDetailDictionary objectForKey:@"CouponID"] forKey:@"AudioId"];
        
    }
    
    [example1 getBackgroundAudiosByBrandID];

    //getAudioDetailsByAudioID
    // Facebook shared data
    
    
    //self.detailDataObject = self.widgetTileInfo;
    //self.tileInfo = self.widgetTileInfo;
    self.bigBanner = [[BigBanners alloc] init];

    self.bigBanner.bigBannerDelegate = self;

    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,568, 320, 60);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    [self addBannerData];

    

    
 
    self.totalBGAudiosArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    
    
    
    self.isBackgroundAudioPlaying = NO;
    


    
/////////////////////////////////////BACKGROUND AUDIO SLIDE VIEW INITIATION////////////////////////////

    self.backgroundAudioView = [[[NSBundle mainBundle] loadNibNamed:@"BackgroundAudiosView" owner:self options:nil]objectAtIndex:0];
    
    float height = 0.0f;
    
    if (IS_IPHONE5) {
        height = 524;
    }else {
        height = 436;
    }
    
    self.backgroundAudioView.frame = CGRectMake(325.0f, 44.0f, 285.0f, height);
    
    [self.view addSubview:self.backgroundAudioView];
    
    [self.view bringSubviewToFront:self.backgroundAudioView];

    //Background Audio Service call and update data
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveBackgroundAudiosNotification:)
                                                 name:@"getBackgroundAudiosNotification"
                                               object:nil];

    
    [self.backgroundAudioView.backgroundAudiosCollectionView registerNib:[UINib nibWithNibName:kcustomAudioCollectionNib bundle:nil] forCellWithReuseIdentifier:kcustomAudioCollectionCellIdentifier];

    self.backgroundAudioView.bgVolumeSlider.value = 10.0f;

    [self.backgroundAudioView.bgVolumeButton addTarget:self action:@selector(bgVolumeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.backgroundAudioView.bgFullVolumeButton addTarget:self action:@selector(bgFullVolumeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.backgroundAudioView.bgVolumeSlider addTarget:self action:@selector(bgVolumeSliderAction:) forControlEvents:UIControlEventValueChanged];
    self.backgroundAudioView.bgVolumeSlider.enabled = NO;
    
    
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideBackgroundMusicScreen:)];
    [self.detailScrollView addGestureRecognizer:gestureRecognizer];
    [gestureRecognizer release];
    
    //NSLog(@"%s subViews = %@", __FUNCTION__, [self.view subviews]);
    
    self.isRepeatSelected = NO;
    
    
    //Airplay Button
    self.airplayButton.showsVolumeSlider = NO;
    self.airplayButton.showsRouteButton = YES;
    
    
}
- (void)loadData
{
    TableObject *obj = [[[TableObject alloc] init] autorelease];
    
    NSString *imageStr = [self.audioDetailDictionary valueForKey:@"Image"];
    
    NSString *descriptionStr = [self.audioDetailDictionary valueForKey:@"Description"];
    NSString *titleStr = [self.audioDetailDictionary valueForKey:@"Title"];
    
    obj.image = [NSString stringWithFormat:@"%@%@",kWebServiceURL,imageStr];
    obj.titleText = titleStr;
    obj.desc =  descriptionStr;
    
    [super setDetailDataObject:obj];
    
    
    //get a dispatch queue
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
        
        if ([[self.audioDetailDictionary objectForKey:@"Image"] length]) {
            
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL, [self.audioDetailDictionary objectForKey:@"Image"]]];
            NSData *image = [[NSData alloc] initWithContentsOfURL:imageURL];
            
            //this will set the image when loading is finished
            dispatch_async(dispatch_get_main_queue(), ^{
                self.audioImageView.image = [UIImage imageWithData:image];
            });
            
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.audioImageView.image = [UIImage imageNamed:@"no-image.png"];
            });
        }
        
    });
    
    self.descriptionLbl.textColor = [UIColor darkGrayColor];
    [self.descriptionLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];
    
    self.audioCurrentSeekLable.textColor = [UIColor darkGrayColor];
    [self.audioCurrentSeekLable setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];
    self.audioMaxTimeLabel.textColor = [UIColor darkGrayColor];
    [self.audioMaxTimeLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];
    
    self.audioTitleLable.text = [self.audioDetailDictionary objectForKey:@"Title"];
    
    self.descriptionLbl.text = [self.audioDetailDictionary objectForKey:@"Description"];
    [self.descriptionLbl resizeToFit];
    
    [self.detailScrollView setContentSize:CGSizeMake(320, self.descriptionLbl.frame.origin.y + self.descriptionLbl.frame.size.height+3)];
    
    [self.fullVolumeButton addTarget:self action:@selector(fullVolumeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitAudioDetailScreen:) name:@"AudioDetailScreenExitNotification" object:nil];


    self.backgroundAudioView.backgroundAudiosCollectionView.delegate = self;
    self.backgroundAudioView.backgroundAudiosCollectionView.dataSource = self;
    [self.backgroundAudioView.backgroundAudiosCollectionView reloadData];
}
- (void)myTask {
	// Do something usefull in here instead of sleeping ...
	sleep(1);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
- (void)dealloc {

    [_audioImageView release];
    [_audioTitleLable release];
    [_mScrubber release];
    [_audioMaxTimeLabel release];
    [_audioPlayButton release];
    [_volumeSlider release];
    [_volumeButton release];
    [_audionDescriptionView release];
    [_detailScrollView release];
    [_audioCurrentSeekLable release];
    [_fullVolumeButton release];
    [_descriptionLbl release];
    [_repeatButton release];
    [_airplayButton release];
    [super dealloc];
}

- (void)exitAudioDetailScreen:(NSNotification *)notification
{
    if (self.audioPlayer) {
        [self.bgAudioPlayer pause];
        self.bgAudioPlayer = nil;
//        self.bgAudioPlayerItem = nil;
        
       // [self.audioPlayer pause];
       // self.audioPlayer = nil;
       // self.audioPlayer = nil;

    }

    //[self hideBackgroundMusicScreen:nil];
    [self backAudioGroundView];
    
}


- (void)backAudioGroundView
{
    
    if ([[SPSingletonClass sharedInstance] isBackgroundAudioButtonPressed]){
        [[SPSingletonClass sharedInstance] setIsBackgroundAudioButtonPressed:NO];

        [[SPSingletonClass sharedInstance] setBackGroundAudioScreen:@"YES"];
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             float height = 0.0f;
                             
                             if (IS_IPHONE5) {
                                 height = 524;
                             }else {
                                 height = 436;
                             }
                             
                             self.backgroundAudioView.frame = CGRectMake(325.0f, 44.0f, 285.0f, height);   // last position
                         }
                         completion:nil];
    }else{
        [[SPSingletonClass sharedInstance] setBackGroundAudioScreen:@"NO"];

        if (self.audioPlayer) {
            
            
            [self.audioPlayer pause];
            self.audioPlayer = nil;
        }

        if (self.player) {
            [self.player pause];
            //self.player = nil;
            //self.playerItem = nil;
        }
    }
}

////////////

//- (void)hardwareVolumeChanged:(NSNotification *)notification
//{
//    
//    NSLog(@"%s", __FUNCTION__);
//    
//    
//    float volume = [[[notification userInfo] objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"] floatValue];
//    NSLog(@"%.2f", volume);
//
//    
////    self.volumeSlider.value = volume * 10.0f;
////    self.backgroundAudioView.bgVolumeSlider.value = volume * 10.0f;
//    
//}

#pragma mark - Play Audio -


- (IBAction)playButtonAction:(id)sender
{
    
//    self.isReplayButtonPressed = NO;
    
    if (self.player) {
        if (self.isPlaying) {
            
            [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
            // Music is currently playing
            [self.player pause];
            self.isPlaying = NO;
        }
        else {
            
            [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
            // Music is currenty paused/stopped
            [self.player play];
            self.isPlaying = YES;
            
        }
        
    }
    else {
        [self playAudio:self.audioDetailDictionary];
    }
    
    
    
}


- (void)playAudio:(NSDictionary *)detailsDictionary
{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    
    HUD.delegate = self;
    //    HUD.labelText = @"Loading";
    //    HUD.detailsLabelText = @"Streaming audio data";
    HUD.square = YES;
    
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    
    
    //NSLog(@"%s %@", __FUNCTION__, self.audioDetailDictionary);
    
    self.volumeSlider.value = 10.0f;
    
    self.isPlaying = YES;
    [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];

    
    NSString *fileNameStr = [detailsDictionary valueForKey:@"FileName"];
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileNameStr];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    if (fileExists) {
        

        [self playFourground:filePath DownloadedFile:@"Yes"];
        
    }
    else{
        NSString *urlStr = [NSString stringWithFormat:@"http://%@", [self.audioDetailDictionary objectForKey:@"FilePath"]];
        [self playFourground:urlStr DownloadedFile:@"No"];

    }
    
    
    
}

- (void)playFourground:(NSString *)filePath DownloadedFile:(NSString *)isFileDownloaded
{
    NSURL *fileURL;
    if ([isFileDownloaded isEqualToString:@"Yes"]){
        fileURL = [[NSURL alloc] initFileURLWithPath: filePath];

    }else{
        fileURL = [NSURL URLWithString:filePath];
 
    }
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
    NSString *tracksKey = @"tracks";
    
    [asset loadValuesAsynchronouslyForKeys:@[tracksKey] completionHandler:
     ^{
         // The completion block goes here.
         
         static const NSString *ItemStatusContext;
         
         // Completion handler block.
         dispatch_async(dispatch_get_main_queue(),
                        ^{
                            NSError *error;
                            AVKeyValueStatus status = [asset statusOfValueForKey:tracksKey error:&error];
                            
                            if (status == AVKeyValueStatusLoaded) {
                                
                                self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
                                // ensure that this is done before the playerItem is associated with the player
                                [self.playerItem addObserver:self forKeyPath:@"status"
                                                     options:NSKeyValueObservingOptionInitial context:&ItemStatusContext];
                                [[NSNotificationCenter defaultCenter] addObserver:self
                                                                         selector:@selector(playerItemDidReachEnd:)
                                                                             name:AVPlayerItemDidPlayToEndTimeNotification
                                                                           object:self.playerItem];
                                self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
                                self.player.rate = 0.0000001f;
                                //                                if (self.isReplayButtonPressed && self.isRepeatSelected) {
                                self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                                //                                }
                            }
                            else {
                                
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:[NSString stringWithFormat:@"The asset's tracks were not loaded:\n%@", [error localizedDescription]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                [alert show];
                                // You should deal with the error appropriately.
                                //NSLog(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
                            }
                        });
     }];
    

}

- (IBAction)repeatButtonAction:(id)sender {
    
    self.isReplayButtonPressed = YES;
    
    if (self.isRepeatSelected) {
        self.isRepeatSelected = NO;
        [self.repeatButton setImage:[UIImage imageNamed:@"ic_rep_un.png"] forState:UIControlStateNormal];
    }
    else {
        self.isRepeatSelected = YES;
        [self.repeatButton setImage:[UIImage imageNamed:@"ic_rep_sec.png"] forState:UIControlStateNormal];
    }
    
//    if (self.player) {
//        return;
//    }
//
//    if (self.isRepeatSelected) {
//        [self playAudio:self.audioDetailDictionary];
//    }

//    if (self.player) {
//        if (self.isPlaying) {
//            
//            [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
//            // Music is currently playing
//            [self.player pause];
//            self.isPlaying = NO;
//        }
//        else {
//            
//            [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
//            // Music is currenty paused/stopped
//            [self.player play];
//            self.isPlaying = YES;
//            
//        }
//        
//    }
//    else {
//        [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
//
//        [self playAudio:self.audioDetailDictionary];
//    }
    
    
    
}

- (IBAction)volumeButtonAction:(id)sender
{
    
//    for(UIView *v in [self.volumeView subviews]) {
//        if([v isKindOfClass:[UISlider class]]) {
//            ((UISlider *)v).value = 0;
//        }
//    }
    
    
//    [self.volumeButton setImage:[UIImage imageNamed:@"icon_mute.png"] forState:UIControlStateNormal];
    self.volumeSlider.value = 0.0f;
    self.player.volume = 0.0;
    
    //    if (self.isVolumeMuted) {
    //
    //        self.isVolumeMuted = NO;
    //        [self.volumeButton setImage:[UIImage imageNamed:@"audio_volume_high.png"] forState:UIControlStateNormal];
    //        self.volumeSlider.value = 10.0f;
    //        self.player.volume = 1.0;
    //    }
    //    else {
    //
    //        self.isVolumeMuted = YES;
    //        [self.volumeButton setImage:[UIImage imageNamed:@"audio_volume_muted.png"] forState:UIControlStateNormal];
    //        self.volumeSlider.value = 0.0f;
    //        self.player.volume = 0.0;
    //    }
}

- (IBAction)fullVolumeButtonAction:(id)sender
{
    
//    for(UIView *v in [self.volumeView subviews]) {
//        if([v isKindOfClass:[UISlider class]]) {
//            ((UISlider *)v).value = 1.0;
//        }
//    }
    
    
    self.volumeSlider.value = 10.0f;
    self.player.volume = 1.0;
    
}



/////
#pragma mark - Key Value Observer for PlayerItem -

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{

    if (object == self.playerItem && [keyPath isEqualToString:@"status"]) {
        if (self.player.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
        } else if (self.player.status == AVPlayerStatusReadyToPlay) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //            CMTime playerDuration = [[[[[self.playerItem tracks] objectAtIndex:0] assetTrack] asset] duration];
                //            if (CMTIME_IS_INVALID(playerDuration)) {
                //                return;
                //            }
                //
                //            double duration = CMTimeGetSeconds(playerDuration);
                //
                //            NSLog(@"AVPlayer Ready to Play, %f", duration);
                
                Float64 interval = 0.0;
                
                CMTime playerDuration = [self playerItemDuration]; // return player duration.
                if (CMTIME_IS_INVALID(playerDuration))
                {
                    return;
                }
                double duration = CMTimeGetSeconds(playerDuration);
                
                self.audioMaxTimeLabel.text = [NSString stringWithFormat:@"%d:%02d", (int)duration/60, (int)duration%60];
                
                
                if (isfinite(duration))
                {
                    CGFloat width = CGRectGetWidth([self.mScrubber bounds]);
                    interval = 0.5f * duration / width;
                }
                
                /* Update the scrubber during normal playback. */
                self.timeObserver = [[self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                               queue:NULL
                                                                          usingBlock:
                                      ^(CMTime time)
                                      {
                                          [self syncScrubber];
                                      }] retain];
                
                [HUD hide:YES];

                [self.player play];

            });
            

            
        } else if (self.player.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
        }
    }
    else if (object == self.bgAudioPlayerItem && [keyPath isEqualToString:@"status"]) {
        if (self.bgAudioPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"BG AVPlayer Failed");
        } else if (self.bgAudioPlayer.status == AVPlayerStatusReadyToPlay) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"BG AVPlayer Ready To Play");
                
                
                self.backgroundAudioView.bgVolumeSlider.enabled = YES;
                
                [HUD hide:YES];
                [self.bgAudioPlayer play];

                
                
            });
            

        } else if (self.bgAudioPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"BG AVPlayer Unknown");
        }
    }
}

////

#pragma mark - PlayerItemDidReachEnd Notification -
////

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    //NSLog(@"%s", __FUNCTION__);
    self.isPlaying = NO;

    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    
    if (self.isRepeatSelected == NO) {
        
        [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
        [self.player seekToTime:kCMTimeZero];
        
        [self syncScrubber];
        
        if (self.player) {
            [self.player pause];
            //self.player = nil;
            //self.playerItem = nil;
        }

    }

}

/////

#pragma mark -  Syncing scrubber -

////

- (void)syncScrubber
{
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration)) {
        self.mScrubber.minimumValue = 0.0;
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration) && (duration > 0))
    {
        float minValue = [ self.mScrubber minimumValue];
        float maxValue = [ self.mScrubber maximumValue];
        double time = CMTimeGetSeconds([self.player currentTime]);
        [self.mScrubber setValue:(maxValue - minValue) * time / duration + minValue];
        
        self.audioCurrentSeekLable.text = [NSString stringWithFormat:@"%d:%02d", (int)time/60, (int)time%60];

    }
    

}

#pragma mark - Player Item Duration -
- (CMTime)playerItemDuration
{
    AVPlayerItem *thePlayerItem = [self.player currentItem];
    if (thePlayerItem.status == AVPlayerItemStatusReadyToPlay)
    {
        
        return([self.playerItem duration]);
    }
    
    return(kCMTimeInvalid);
}

#pragma mark - AudioSeek (scrubber) slider action -

- (IBAction)audioSeekValueChanged:(id)sender
{
    if ([sender isKindOfClass:[UISlider class]])
	{
		UISlider* slider = sender;
		
		CMTime playerDuration = [self.playerItem duration];
		if (CMTIME_IS_INVALID(playerDuration)) {
			return;
		}
		
		double duration = CMTimeGetSeconds(playerDuration);
		if (isfinite(duration))
		{
			float minValue = [slider minimumValue];
			float maxValue = [slider maximumValue];
			float value = [slider value];
			
			double time = duration * (value - minValue) / (maxValue - minValue);
			
			[self.player seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC)];
		}
	}

}

- (IBAction)volumeChanged:(UISlider *)slider
{
    
    self.player.volume = slider.value / 10.0;
    
//    if (self.player.volume == 0.0) {
//        [self.volumeButton setImage:[UIImage imageNamed:@"icon_mute.png"] forState:UIControlStateNormal];
//    }
//    else {
//        [self.volumeButton setImage:[UIImage imageNamed:@"icon_volume.png"] forState:UIControlStateNormal];
//    }

}


-(UIImage*)resizeImage:(UIImage *)image imageSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width,size.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    //here is the scaled image which has been changed to the size specified
    UIGraphicsEndImageContext();
    return newImage;
    
}

- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize withChosenImage:(UIImage *)chosenImage
{
    UIImage *sourceImage = chosenImage;
    UIImage *newImage = nil;
    
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        scaledWidth  = width * scaleFactor*1;
        scaledHeight = height * scaleFactor;
        
        // center the image
        
        if (widthFactor < heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor > heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage ;
}

#pragma mark - UICollectionViewDataSource -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.totalBGAudiosArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AudioCollectionViewCell *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:kcustomAudioCollectionCellIdentifier forIndexPath:indexPath];
    
    
    NSDictionary *tempDict = [self.totalBGAudiosArray objectAtIndex:indexPath.row];
    NSLog(@"tempDict %@",tempDict);
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL, [tempDict valueForKey:@"Image"]];
    // spawn a new thread to load the image in the background, from the network
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
        NSURLResponse* response = nil;
        NSError* error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            // update the image and sent notification on the main thread
            
            
            if ([[tempDict valueForKey:@"Image"] length]) {
                UIImage *tileImage = [self imageByScalingProportionallyToSize:CGSizeMake(126.0f, 94.0f) withChosenImage:[UIImage imageWithData:data]];
                
                collectionViewCell.backgroundImageView.image = tileImage;
                
                if ([[[self.totalBGAudiosArray objectAtIndex:indexPath.row] objectForKey:@"Fee"]isEqualToString:@"Free"]) {
                    collectionViewCell.staticImg.hidden = YES;
                }
                else {
                    NSString *productIDStr = [[self.totalBGAudiosArray objectAtIndex:indexPath.row] valueForKey:@"IphoneProductID"];
                    
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:productIDStr] == 1){
                        collectionViewCell.staticImg.hidden = YES;
                        
                    }else{
                        collectionViewCell.staticImg.hidden = NO;
                        collectionViewCell.staticImg.image = [UIImage imageNamed:@"ic_buy.png"];
                    }
                    
                }

            }else{
                collectionViewCell.backgroundImageView.image = [UIImage imageNamed:@"no-image.png"];
                if ([[[self.totalBGAudiosArray objectAtIndex:indexPath.row] objectForKey:@"Fee"]isEqualToString:@"Free"]) {
                    collectionViewCell.staticImg.hidden = YES;
                }
                else {
                    NSString *productIDStr = [[self.totalBGAudiosArray objectAtIndex:indexPath.row] valueForKey:@"IphoneProductID"];
                    
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:productIDStr] == 1){
                        collectionViewCell.staticImg.hidden = YES;
                        
                    }else{
                        collectionViewCell.staticImg.hidden = NO;
                        collectionViewCell.staticImg.image = [UIImage imageNamed:@"ic_buy.png"];
                    }
                    
                }
                
            }
            
            collectionViewCell.titleLabel.text = [tempDict objectForKey:@"Title"];
            
        });
    });
        
    
    return collectionViewCell;
    
}

#pragma mark - UICollectionViewDelegates -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    AudioCollectionViewCell  *previousSelectedCell = (AudioCollectionViewCell *)[collectionView cellForItemAtIndexPath:_previousSelectionCellIndex];
    if (previousSelectedCell.isSelected) {
        
        previousSelectedCell.isSelected = NO;
        [UIView animateWithDuration:0.8
                              delay:0
                            options:(UIViewAnimationOptionAllowUserInteraction)
                         animations:^{
                             previousSelectedCell.layer.borderColor = [UIColor clearColor].CGColor;

                         }
                         completion:^(BOOL finished){
                         }
         ];
        
        if (_previousSelectionCellIndex == indexPath) {

            [self.audioPlayer pause];
            self.audioPlayer = nil;

            return;
        }
        
    }

    if (![[[self.totalBGAudiosArray objectAtIndex:indexPath.row] objectForKey:@"Fee"]isEqualToString:@"Free"]) {
        
        
        
        NSString *productIDStr = [[self.totalBGAudiosArray objectAtIndex:indexPath.row] valueForKey:@"IphoneProductID"];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:productIDStr] == 1){
            
        }else{

            
            if (self.audioPlayer||self.player) {
                [self.audioPlayButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];

                [self.player pause];
                //self.player = nil;
                //self.playerItem = nil;
                
                [self.audioPlayer pause];
                self.audioPlayer = nil;
                self.audioPlayer = nil;

                
            }
            
            AudioPurchaseViewController *purchaseVC = [[AudioPurchaseViewController alloc] initWithNibName:@"AudioPurchaseViewController" bundle:nil];
            purchaseVC.purchasingAudioDetailDictionary = [self.totalBGAudiosArray objectAtIndex:indexPath.row];
            purchaseVC.appType = APP_AUDIO_PURCHASE;
            purchaseVC.isBackgroundPurchasedAudio = YES;
            _previousSelectionCellIndex = nil;
            [self.navigationController pushViewController:purchaseVC animated:YES];
            return;
        }
        
        
        
    }


    // animate the cell user tapped on
    AudioCollectionViewCell  *cell = (AudioCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if (cell) {
        
        if (self.bgAudioPlayer) {
            [self.audioPlayer pause];
            self.audioPlayer = nil;


            [self.bgAudioPlayer pause];
            self.bgAudioPlayer = nil;
        }
        
        cell.isSelected = YES;
        [UIView animateWithDuration:0.8
                              delay:0
                            options:(UIViewAnimationOptionAllowUserInteraction)
                         animations:^{
                             cell.layer.borderColor = [UIColor colorWithRed:158.0f/255.0f green:142.0f/255.0f blue:94.0f/255.0f alpha:1.0].CGColor;

                         }
                         completion:^(BOOL finished){
                         }
         ];

        _previousSelectionCellIndex = indexPath;
        
        if (self.bgAudioPlayer) {
            [self.audioPlayer play];

            //[self.bgAudioPlayer play];
        }
        else {
            
            
            //detailsDictionary
            
            [self backgroundAudioFileDownloading:[self.totalBGAudiosArray objectAtIndex:indexPath.row]];
            //[self playBackgroundAudio:[self.totalBGAudiosArray objectAtIndex:indexPath.row]];

        }

    }
    
    
}
- (void)backgroundAudioFileDownloading:(NSDictionary *)detailsDictionary
{
    
    self.audioPlayer.volume = 1.0f;
    
    [[LeveyHUD sharedHUD] appearWithText:@"Loading..."];
    
    NSString *fileNameStr = [detailsDictionary valueForKey:@"FileName"];
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString* filePath = [documentsPath stringByAppendingPathComponent:fileNameStr];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    if (fileExists) {
        
        [self playAudioAtPath:filePath];
        
    }
    else{
        [self.audioPlayer stop];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", [detailsDictionary objectForKey:@"FilePath"]]];
        //NSLog(@"fileURL %@",url);
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDownloadTask * downloadTask = [ defaultSession downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error)
   {
       if(error == nil)
       {
           // NSLog(@"Temporary file =%@",location);
           NSError *err = nil;
           NSFileManager *fileManager = [NSFileManager defaultManager];
           
           NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
           
           NSURL *docsDirURL = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:fileNameStr]];
           
           if ([fileManager moveItemAtURL:location toURL:docsDirURL error: &err])
           {
               NSLog(@"File is saved to =%@",docsDir);
               
               
           }
           else
           {
               NSLog(@"failed to move: %@",[err userInfo]);
           }
           
           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
           NSString *documentsDirectory = [paths objectAtIndex:0];
           NSString *path = [documentsDirectory stringByAppendingPathComponent:fileNameStr];
           [self playAudioAtPath:path];
       }
   }];
        
        [downloadTask resume];
        
        
    }
    //  [self oldFileRemoved];
    
    
}

- (void)playAudioAtPath:(NSString*)filePath
{
    NSURL *url1 = [[NSURL alloc] initFileURLWithPath:filePath];
    NSLog(@"url1 %@",url1);
    [self.audioPlayer stop];
    NSError *error = nil;
    AVAudioPlayer *player1 = [[AVAudioPlayer alloc] initWithContentsOfURL:url1 error:&error];
    if (player1) {
        [[LeveyHUD sharedHUD] disappear];
        [player1 setNumberOfLoops:-1];
        player1.delegate=self;
        [player1 prepareToPlay];
        [player1 play];
        self.backgroundAudioView.bgVolumeSlider.enabled = YES;
        
        self.audioPlayer = player1;
    } else {
        NSLog(@"Error create audio player: %@", error);
        [[LeveyHUD sharedHUD] disappear];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:[NSString stringWithFormat:@"The asset's tracks were not loaded:\n%@", [error localizedDescription]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
    
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"%s successfully=%@", __PRETTY_FUNCTION__, flag ? @"YES"  : @"NO");
    
    // self.player.currentTime = 0;
    // _player.numberOfLoops = -1;
    
    [self.audioPlayer play];//self.audioPlayerNew
    //[self.audioPlayerNew play];
    
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"%s error=%@", __PRETTY_FUNCTION__, error);
    
}


#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize retval = CGSizeMake(126.0f, 94.0f);
    return retval;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(3, 9, 3, 13);
}

///

- (void)receiveBackgroundAudiosNotification:(NSNotification *)notification
{

    [self.totalBGAudiosArray addObjectsFromArray:[[AppDelegate shareddelegate].backgroundAudiosDictionary objectForKey:@"FreeAudios"]];
    [self.totalBGAudiosArray addObjectsFromArray:[[AppDelegate shareddelegate].backgroundAudiosDictionary objectForKey:@"PaidAudios"]];
    
    
    if ([self.totalBGAudiosArray count] == 0) {
        self.backGroundMusicBtn.hidden = YES;
    }else{
        self.backGroundMusicBtn.hidden = NO;

    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.backgroundAudioView.backgroundAudiosCollectionView reloadData];
    });
    
}

- (void)receiveAudioDetailNotification:(NSNotification *)notification
{
    [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:NO];
    self.audioDetailDictionary =[[AppDelegate shareddelegate].audioDetailDictionary mutableCopy];
    NSLog(@"Dict %@",self.audioDetailDictionary);
    [self loadData];

    //[AppDelegate shareddelegate].audioDetailDictionary
}
#pragma mark - Play Background Audios -

- (void)bgPlayerItemDidReachEnd1:(NSNotification *)notification
{
    //NSLog(@"%s", __FUNCTION__);
    //set our delegate and begin playback
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    
}

- (void)bgVolumeSliderAction:(UISlider *)bgVlmSlider
{
    //NSLog(@"%s", __FUNCTION__);
    self.audioPlayer.volume = bgVlmSlider.value / 10.0;
    


}


- (void)bgFullVolumeButtonAction:(id)sender
{
    self.backgroundAudioView.bgVolumeSlider.value = 10.0f;
    self.audioPlayer.volume = 1.0;
    return;
    
    
}
- (void)bgVolumeButtonAction:(id)sender
{
    self.backgroundAudioView.bgVolumeSlider.value = 0.0f;
    self.audioPlayer.volume = 0.0;
    return;
    
    
    if (self.isBGVolumeMuted) {
        [self.backgroundAudioView.bgVolumeButton setImage:[UIImage imageNamed:@"ic_unmute.png"] forState:UIControlStateNormal];

        self.isBGVolumeMuted = NO;
        self.backgroundAudioView.bgVolumeSlider.value = 10.0f;
        self.audioPlayer.volume = 1.0;
    }
    else {
        [self.backgroundAudioView.bgVolumeButton setImage:[UIImage imageNamed:@"ic_mute.png"] forState:UIControlStateNormal];

        self.isBGVolumeMuted = YES;
        self.backgroundAudioView.bgVolumeSlider.value = 0.0f;
        self.audioPlayer.volume = 0.0;
    }

}

- (void)hideBackgroundMusicScreen:(UITapGestureRecognizer *)gestureRecogniser
{
    
    
    CGPoint point = [gestureRecogniser locationInView:self.view];
    
    if(CGRectContainsPoint([self.backgroundAudioView frame], point)) {
        NSLog(@"Exists in background");
    }
    
    
    if (gestureRecogniser.view == self.backgroundAudioView) {
        
        NSLog(@"BackgroundView");
        return;
        
    }
    //isBackGrdView
    
    if ([[SPSingletonClass sharedInstance] isBackgroundAudioButtonPressed]) {
        NSLog(@"ssssssssssssssss");
        

        [[SPSingletonClass sharedInstance] setIsBackgroundAudioButtonPressed:NO];

        //[[SPSingletonClass sharedInstance] setIsAudioDetailsView:NO];

        
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             float height = 0.0f;
                             
                             if (IS_IPHONE5) {
                                 height = 524;
                             }else {
                                 height = 436;
                             }

                             self.backgroundAudioView.frame = CGRectMake(325.0f, 44.0f, 285.0f, height);   // last position
                         }
                         completion:nil];
    }

}


#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	[HUD release];
	HUD = nil;
}



#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Audio"]) {
            self.aboutBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aboutBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aboutBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aboutBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];

        //[self.addBannerView startShowingBanners:self.bannerUrlsArray];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryActionBanners];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryAction
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    self.itemId = [self.audioDetailDictionary objectForKey:@"AudioID"];
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        // [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    else{
        //  [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    

}

-(void)entryActionBanners
{
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
}


- (IBAction)moveToBgAudios:(id)sender {
    
    if ([self.totalBGAudiosArray count] == 0) {
        self.backGroundMusicBtn.hidden = YES;
        
    }
    else {
        self.backGroundMusicBtn.hidden = NO;

        [[SPSingletonClass sharedInstance] setIsBackgroundAudioButtonPressed:YES];

//        [[SPSingletonClass sharedInstance] setIsAudioDetailsView:YES];
        

        //isAudioDetailsView
        
//        NSLog(@"value ==== %d", [[SPSingletonClass sharedInstance] isBackgroundAudioButtonPressed]);
        
        if (![[SPSingletonClass sharedInstance] isBackgroundAudioButtonPressed]) {
            
            
            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 
                                 float height = 0.0f;
                                 
                                 if (IS_IPHONE5) {
                                     height = 524;
                                 }else {
                                     height = 436;
                                 }
                                 
                                 self.backgroundAudioView.frame = CGRectMake(325.0f, 44.0f, 285.0f, height);   // last position
                             }
                             completion:nil];
        }
        else {
            
            [UIView animateWithDuration:0.5f
                                  delay:0.0f
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 float height = 0.0f;
                                 
                                 if (IS_IPHONE5) {
                                     height = 524;
                                 }else {
                                     height = 436;
                                 }
                                 
                                 self.backgroundAudioView.frame = CGRectMake(40.0f, 44.0f, 285.0f, height);   // last position
                             }
                             completion:nil];
            
        }

    }


}

- (IBAction)showAudioInfo:(id)sender
{
    
//    NSLog(@"%s", __FUNCTION__);
    int deslebelHeight;
    if (IS_IPHONE5){
        deslebelHeight = 98;
    }else{
        deslebelHeight = 20;
    }
    
    if (self.descriptionLbl.frame.size.height>deslebelHeight){
        self.detailScrollView.contentOffset = CGPointMake(0, 160);
    }

    
}
@end
