//
//  FreeAudiosListViewController.m
//  Exceeding
//
//  Created by NarasimhamPV on 03/06/14.
//
//

#import "FreeAudiosListViewController.h"
#import "AudioDetailViewController.h"
#import "FreeAudioTableViewCell.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "AudioPurchaseViewController.h"

#import "CustomBannerView.h"
#import "WebViewController.h"
#import "AsyncImageDownloader.h"
#import "SPSingletonClass.h"

static NSString *const kcustomFreeAudioTableViewCellIdentifier = @"freeAudioTableViewCellIdentifier";
static NSString *const kcustomFreeAudioTableViewCellNib = @"FreeAudioTableViewCell";


@interface FreeAudiosListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *freeAudioListTableView;

@property (nonatomic, strong) NSMutableArray *totalAudiosListArray;

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;
@property (nonatomic,strong)  NSMutableArray *audiosObjectsArray;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@end

@implementation FreeAudiosListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self entryAction];
    
    self.totalAudiosListArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    self.audiosObjectsArray = [[NSMutableArray alloc] init];
    
    [self.freeAudioListTableView registerNib:[UINib nibWithNibName:kcustomFreeAudioTableViewCellNib bundle:nil] forCellReuseIdentifier:kcustomFreeAudioTableViewCellIdentifier];
    self.freeAudioListTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.freeAudioListTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveAudioListNotification:)
                                                 name:@"getAudioListNotification"
                                               object:nil];
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    
    [example1 getAudioListByBrandID];
    
    
    
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[SPSingletonClass sharedInstance] setBackGroundAudioScreen:@"NO"];
    [self.freeAudioListTableView reloadData];
    
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,568, 320, 60);
    [self.view addSubview:self.addBannerView];
    [self.view bringSubviewToFront:self.addBannerView];
    [self addBannerData];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.addBannerView removeFromSuperview];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)receiveAudioListNotification:(NSNotification *)notification
{
    
    [self.totalAudiosListArray addObjectsFromArray:[[AppDelegate shareddelegate].audioListDictionary objectForKey:@"FreeAudios"]];
    [self.totalAudiosListArray addObjectsFromArray:[[AppDelegate shareddelegate].audioListDictionary objectForKey:@"PaidAudios"]];
    
    [self.freeAudioListTableView reloadData];
    
}

#pragma mark - UITableViewDataSources -


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.totalAudiosListArray count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FreeAudioTableViewCell *cell = [self.freeAudioListTableView dequeueReusableCellWithIdentifier:kcustomFreeAudioTableViewCellIdentifier forIndexPath:indexPath];
    
    
    
    NSDictionary *tempDict = [self.totalAudiosListArray objectAtIndex:indexPath.row];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL, [tempDict valueForKey:@"Image"]];
    // spawn a new thread to load the image in the background, from the network
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
        NSURLResponse* response = nil;
        NSError* error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            // update the image and sent notification on the main thread
            if ([[tempDict valueForKey:@"Image"] length]) {
                cell.cellImageView.image = [UIImage imageWithData:data];
                
            }else{
                cell.cellImageView.image = [UIImage imageNamed:@"no-image.png"];
                
            }
            if ([[tempDict valueForKey:@"Fee"]isEqualToString:@"Free"]) {
                cell.staticImageView.hidden = YES;
            }
            else {
                NSString *productIDStr = [tempDict valueForKey:@"IphoneProductID"];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:productIDStr] == 1){
                    cell.staticImageView.hidden = YES;
                    
                }else{
                    cell.staticImageView.hidden = NO;
                    cell.staticImageView.image = [UIImage imageNamed:@"ic_buy.png"];
                }
                
            }
            
        });
    });
    
    cell.titleLable.text = [tempDict objectForKey:@"Title"];
    cell.descriptionLabel.text = [tempDict valueForKey:@"Description"];
    
    return cell;
}


#pragma mark - UITableViewDelegates -


// Called after the user changes the selection.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __FUNCTION__);
    
    if ([[[self.totalAudiosListArray objectAtIndex:indexPath.row] objectForKey:@"Fee"]isEqualToString:@"Free"]) {
        
        
        AudioDetailViewController *audioDetailVC = [[AudioDetailViewController alloc] initWithNibName:@"AudioDetailViewController" bundle:nil];
        audioDetailVC.appType = APP_AUDIO_DETAIL;
        audioDetailVC.audioDetailDictionary = [self.totalAudiosListArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:audioDetailVC animated:YES];
    }
    else {
        
        NSString *productIDStr = [[self.totalAudiosListArray objectAtIndex:indexPath.row] valueForKey:@"IphoneProductID"];
        NSLog(@"IphoneProductID %@",productIDStr);
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:productIDStr] == 1){
            
            AudioDetailViewController *audioDetailVC = [[AudioDetailViewController alloc] initWithNibName:@"AudioDetailViewController" bundle:nil];
            audioDetailVC.appType = APP_AUDIO_DETAIL;
            audioDetailVC.audioDetailDictionary = [self.totalAudiosListArray objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:audioDetailVC animated:YES];
            
            
        }else{
            AudioPurchaseViewController *purchaseVC = [[AudioPurchaseViewController alloc] initWithNibName:@"AudioPurchaseViewController" bundle:nil];
            purchaseVC.purchasingAudioDetailDictionary = [self.totalAudiosListArray objectAtIndex:indexPath.row];
            purchaseVC.appType = APP_AUDIO_PURCHASE;
            [self.navigationController pushViewController:purchaseVC animated:YES];
        }
        
        
        
        
        
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 80.0f;
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Audio"]) {
            self.aboutBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aboutBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aboutBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aboutBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Listing"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryActionBanner];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    self.itemId = [self.totalAudiosListArray objectForKey:@"AudioID"];//[[self.totalAudiosListArray objectAtIndex:indexPath.row] objectForKey:@"Title"];
    
    //    self.itemId = [[self.totalAudiosListArray objectAtIndex:indexPath.row] objectForKey:@"AudioID"];
    NSDictionary *audiodict = [AppDelegate shareddelegate].audioListDictionary;
    
    
    self.itemId = [audiodict objectForKey:@"AudioID"];
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        // [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    else{
        //  [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.widgetTileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    
}


-(void)entryActionBanner
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Listing" deviceType:@"iOS"];
}



@end
