//
//  AudioPurchaseViewController.h
//  Exceeding
//
//  Created by NarasimhamPV on 06/06/14.
//
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainDetailVC.h"
#import "DACircularProgressView.h"
#import "IAPShare.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
#import "MBProgressHUD.h"
#import "TableObject.h"
#import <StoreKit/StoreKit.h>
#import "BigBanners.h"

@interface AudioPurchaseViewController : AppMainDetailVC <MBProgressHUDDelegate,BigBannersDelegate>
{
    MBProgressHUD *HUD;
    NSArray *myProductArry;
    
}

@property (strong, nonatomic) IAPHelper *iapHelper;

@property (nonatomic, strong) NSDictionary *purchasingAudioDetailDictionary;
@property (strong, nonatomic) DACircularProgressView *progressView;

@property (nonatomic, strong) AVPlayerItem *bgAudioPlayerItem;
@property (nonatomic, strong) AVPlayer *bgAudioPlayer;
@property (strong) id timeObserver;
@property (nonatomic) BOOL isBackgroundAudioPlaying;
@property (strong, nonatomic) NSString *aProductIdentifierStr;
@property (nonatomic, assign) BOOL isBackgroundPurchasedAudio;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
