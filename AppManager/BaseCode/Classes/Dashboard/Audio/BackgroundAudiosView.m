//
//  BackgroundAudiosView.m
//  Exceeding
//
//  Created by NarasimhamPV on 05/06/14.
//
//

#import "BackgroundAudiosView.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "UIView+Shadow.h"


@implementation BackgroundAudiosView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    [self.layer setCornerRadius:0.0f];
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 1.0f;
    [self.layer setMasksToBounds:NO];

    self.layer.shadowOpacity = 1.0f;
    self.layer.shadowRadius = 6.0f;
    self.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
//    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.frame.origin.x, self.frame.size.height, self.frame.size.width, 15)].CGPath;
    self.layer.shouldRasterize = YES;
    
//(self.frame.origin.x, self.frame.size.height, self.frame.size.width, 15)
    
    
    UIView *sampleView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 1, self.frame.size.width, 15)];
    [sampleView2 makeInsetShadowWithRadius:6.0f Color:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7] Directions:[NSArray arrayWithObjects:@"top", nil]];
    [self addSubview:sampleView2];
    
    
}


- (void)dealloc {
    [_backgroundAudiosCollectionView release];
    [super dealloc];
}


@end
