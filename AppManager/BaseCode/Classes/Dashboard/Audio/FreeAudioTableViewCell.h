//
//  FreeAudioTableViewCell.h
//  Exceeding
//
//  Created by NarasimhamPV on 05/06/14.
//
//

#import <UIKit/UIKit.h>

@interface FreeAudioTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *cellImageView;
@property (retain, nonatomic) IBOutlet UILabel *titleLable;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UIImageView *staticImageView;
@end
