//
//  AudioDetailViewController.h
//  Exceeding
//
//  Created by NarasimhamPV on 31/05/14.
//
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainDetailVC.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
#import "BackgroundAudiosView.h"
#import "MBProgressHUD.h"
#import <MediaPlayer/MediaPlayer.h>
#import "LeveyHUD.h"
#import "TableObject.h"
#import "BigBanners.h"
@interface AudioDetailViewController : AppMainDetailVC <UICollectionViewDelegate, UICollectionViewDataSource, MBProgressHUDDelegate, AVAudioPlayerDelegate,BigBannersDelegate>
{
    
	MBProgressHUD *HUD;

}

@property (nonatomic, strong) NSDictionary *audioDetailDictionary;

@property (nonatomic) BOOL isPlaying;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayer *player;

@property (strong) id timeObserver;
@property (nonatomic, retain) BackgroundAudiosView *backgroundAudioView;

@property (nonatomic) BOOL isBackgroundAudioPlaying;
@property (nonatomic, strong) AVPlayerItem *bgAudioPlayerItem;
@property (nonatomic, strong) AVPlayer *bgAudioPlayer;

@property (nonatomic, assign) NSIndexPath *previousSelectionCellIndex;
@property (nonatomic, strong) NSString *widgetTileInfo;
@property (strong, nonatomic) TableObject *tableobject;

@property (retain, nonatomic) IBOutlet MPVolumeView *airplayButton;

@property (nonatomic, strong) AVAudioPlayer* audioPlayer;
@property (nonatomic, strong) AVAudioPlayer* audioPlayerNew;

@property (nonatomic, strong) BigBanners *bigBanner;

//@property (nonatomic, strong) MPVolumeView *volumeView;

@end
