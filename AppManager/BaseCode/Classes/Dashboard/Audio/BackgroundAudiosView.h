//
//  BackgroundAudiosView.h
//  Exceeding
//
//  Created by NarasimhamPV on 05/06/14.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>

#import "CustomBannerView.h"
#import "WebViewController.h"

@interface BackgroundAudiosView : UIView

@property (retain, nonatomic) IBOutlet UICollectionView *backgroundAudiosCollectionView;
@property (retain, nonatomic) IBOutlet UISlider *bgVolumeSlider;
@property (retain, nonatomic) IBOutlet UIButton *bgVolumeButton;
@property (retain, nonatomic) IBOutlet UIButton *bgFullVolumeButton;


@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;

//@property (nonatomic, strong) MPVolumeView *bgVolumeView;

@end
