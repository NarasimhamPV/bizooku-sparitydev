//
//  AudioCollectionViewCell.m
//  Exceeding
//
//  Created by NarasimhamPV on 05/06/14.
//
//

#import "AudioCollectionViewCell.h"

@implementation AudioCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.isSelected = NO;
        
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.backgroundImageView.clipsToBounds = YES;

    [self.layer setCornerRadius:1.0f];
    self.layer.borderColor = [UIColor clearColor].CGColor;
    self.layer.borderWidth = 4.0f;
    [self.layer setMasksToBounds:YES];
    

}


@end
