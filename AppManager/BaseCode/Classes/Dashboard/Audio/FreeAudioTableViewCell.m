//
//  FreeAudioTableViewCell.m
//  Exceeding
//
//  Created by NarasimhamPV on 05/06/14.
//
//

#import "FreeAudioTableViewCell.h"

@implementation FreeAudioTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
//    self.titleLable.textColor = [UIColor darkTextColor];
//    [self.titleLable setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:20]];
    
    self.descriptionLabel.textColor = [UIColor darkGrayColor];
    [self.descriptionLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_cellImageView release];
    [_titleLable release];
    [_descriptionLabel release];
    [super dealloc];
}
@end
