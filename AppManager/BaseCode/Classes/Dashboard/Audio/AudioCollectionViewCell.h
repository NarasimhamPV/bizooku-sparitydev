//
//  AudioCollectionViewCell.h
//  Exceeding
//
//  Created by NarasimhamPV on 05/06/14.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface AudioCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic,assign) IBOutlet UIImageView *staticImg;

@end
