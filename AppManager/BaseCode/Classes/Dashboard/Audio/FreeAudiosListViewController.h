//
//  FreeAudiosListViewController.h
//  Exceeding
//
//  Created by NarasimhamPV on 03/06/14.
//
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainVC.h"
#import "BigBanners.h"

@interface FreeAudiosListViewController : AppMainVC<BigBannersDelegate>
@property (nonatomic, strong) BigBanners *bigBanner;

@end
