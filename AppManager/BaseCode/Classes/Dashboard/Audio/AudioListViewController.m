//
//  AudioListViewController.m
//  Exceeding
//
//  Created by NarasimhamPV on 29/05/14.
//
//

#import "AudioListViewController.h"
#import "AppDelegate.h"

#import "SDZEXWidgetServiceExample.h"

#import "SDZEXWidgetServiceExample.h"
#import "AudioDetailViewController.h"
#import "FreeAudiosListViewController.h"
#import "AudioCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "CustomBannerView.h"
#import "WebViewController.h"


#import "AppSubWebVC.h"
#import "DBTilesInfo.h"

#import "AudioPurchaseViewController.h"
#import "SPSingletonClass.h"

static NSString *const kcustomAudioCollectionCellIdentifier = @"audiosCellIdentifier";
static NSString *const kcustomAudioCollectionNib = @"AudioCollectionViewCell";

@interface AudioListViewController () <UICollectionViewDataSource, UICollectionViewDelegate>


@property (strong, nonatomic)  DBTilesInfo *tileInfo;
@property (retain, nonatomic) IBOutlet UIButton *freeAudiosButton;
@property (nonatomic, strong) NSArray *freeAudiosArray;
@property (nonatomic, strong) NSArray *paidAudiosArray;
@property (retain, nonatomic) IBOutlet UICollectionView *audiosCollectionView;

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

- (IBAction)freeAudiosButtonAction:(id)sender;

@end

@implementation AudioListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
   
    
    // Do any additional setup after loading the view from its nib.
    //self.appDetailVC.detailDataDictionary =
    self.freeAudiosArray = [[NSArray alloc] init];
    self.paidAudiosArray = [[NSArray alloc] init];
    [self entryAction];

    
    self.bigBanner = [[BigBanners alloc] init];

    self.bigBanner.bigBannerDelegate = self;

    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,568, 320, 60);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    [self addBannerData];
    
    
    [self.audiosCollectionView registerNib:[UINib nibWithNibName:kcustomAudioCollectionNib bundle:nil] forCellWithReuseIdentifier:kcustomAudioCollectionCellIdentifier];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveAudioListNotification:)
                                                 name:@"getAudioListNotification"
                                               object:nil];
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    
    [example1 getAudioListByBrandID];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)receiveAudioListNotification:(NSNotification *)notification
{
    
    self.freeAudiosArray = [[AppDelegate shareddelegate].audioListDictionary objectForKey:@"FreeAudios"];
    
    self.paidAudiosArray = [[AppDelegate shareddelegate].audioListDictionary objectForKey:@"PaidAudios"];

    NSLog(@"%s paidAudiosArray = %@ count = %d", __FUNCTION__, self.paidAudiosArray, [self.paidAudiosArray count]);
    
    [self.audiosCollectionView reloadData];
    
}

- (void)dealloc {
    [_freeAudiosButton release];
    [_audiosCollectionView release];
    [super dealloc];
}



#pragma mark - UICollectionViewDataSource -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.paidAudiosArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    AudioCollectionViewCell *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:kcustomAudioCollectionCellIdentifier forIndexPath:indexPath];
    
    
    
    //get a dispatch queue
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
        
        if ([[[self.paidAudiosArray objectAtIndex:indexPath.row] objectForKey:@"Image"] length]) {
            
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL, [[self.paidAudiosArray objectAtIndex:indexPath.row] objectForKey:@"Image"]]];
            
            
            NSData *image = [[NSData alloc] initWithContentsOfURL:imageURL];
            
            //this will set the image when loading is finished
            dispatch_async(dispatch_get_main_queue(), ^{
                
                collectionViewCell.backgroundImageView.image = [UIImage imageWithData:image];
                
            });
            
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                collectionViewCell.backgroundImageView.image = [UIImage imageNamed:@"no-image.png"];
                
            });

        }
        
    });

    collectionViewCell.titleLabel.text = [[self.paidAudiosArray objectAtIndex:indexPath.row] objectForKey:@"Title"];

    return collectionViewCell;

}

#pragma mark - UICollectionViewDelegates -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __FUNCTION__);
    
    /*
    
    int widgetId;
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [self.tileInfo.widget.widgetID intValue];
    }
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(!self.tileInfo.widget)
    {
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[[NSUserDefaults standardUserDefaults] objectForKey:kWidgetId] intValue] widgetItemId:[self.itemId longValue] widgetType:@"Audio-List" deviceType:@"iOS"];
    }
    else{
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[self.tileInfo.widget.widgetID integerValue] widgetItemId:[self.itemId longValue] widgetType:@"Audio-List" deviceType:@"iOS"];
    }

    */
    
    
    
    if (indexPath.row == 1) {
        
        AudioPurchaseViewController *audioPurchaseVC = [[AudioPurchaseViewController alloc]initWithNibName:@"AudioPurchaseViewController" bundle:nil];
        audioPurchaseVC.appType = APP_AUDIO_PURCHASE;
        audioPurchaseVC.purchasingAudioDetailDictionary = [self.paidAudiosArray objectAtIndex:indexPath.row];
        audioPurchaseVC.isBackgroundPurchasedAudio = NO;
        [self.navigationController pushViewController:audioPurchaseVC animated:YES];
        
    }
    else {
        
        AudioDetailViewController *audioDetailVC = [[AudioDetailViewController alloc] initWithNibName:@"AudioDetailViewController" bundle:nil];
        audioDetailVC.appType = APP_AUDIO_DETAIL;
        audioDetailVC.audioDetailDictionary = [self.paidAudiosArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:audioDetailVC animated:YES];

    }
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize retval = CGSizeMake(140, 140);
    return retval;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}


- (IBAction)freeAudiosButtonAction:(id)sender
{
    
    FreeAudiosListViewController *freeAudiosListVC = [[FreeAudiosListViewController alloc] initWithNibName:@"FreeAudiosListViewController" bundle:nil];
    freeAudiosListVC.appType = APP_FREE_AUDIO_List;
    [self.navigationController pushViewController:freeAudiosListVC animated:YES];
    
}

//Main Menu, Audio
#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Audio"]) {
            self.aboutBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aboutBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aboutBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aboutBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //[self.addBannerView startShowingBanners:self.bannerUrlsArray];
        
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Listing"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryActionBanners];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryAction
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        // [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    else{
        //  [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Join Event to Facebook" deviceType:@"iOS"];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.widgetTileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }

}

-(void)entryActionBanners
{
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Listing" deviceType:@"iOS"];
    
}



@end
