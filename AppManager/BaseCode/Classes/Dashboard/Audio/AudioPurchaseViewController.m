//
//  AudioPurchaseViewController.m
//  Exceeding
//
//  Created by NarasimhamPV on 06/06/14.
//
//

#import "AudioPurchaseViewController.h"

#import "UILabel+Exceedings.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"
#import "LeveyHUD.h"
#import "AudioDetailViewController.h"


@interface AudioPurchaseViewController () <UIAlertViewDelegate,NSURLSessionDelegate>

@property (retain, nonatomic) IBOutlet UILabel *audioTitleLabel;
@property (retain, nonatomic) IBOutlet UIImageView *audioImageView;

@property (retain, nonatomic) IBOutlet UIButton *purchaseButton;
@property (retain, nonatomic) IBOutlet UITextView *audioDescription;
@property (retain, nonatomic) IBOutlet UIScrollView *purchaseScrollView;
@property (retain, nonatomic) IBOutlet UILabel *priceLabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic, strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;
@property (retain, nonatomic) IBOutlet UIButton *previewButton;
@property (strong, nonatomic) NSTimer *timer;
@property (retain, nonatomic) NSMutableArray *productIdsArray;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;

@property (strong, nonatomic) NSMutableArray *productsArray;

- (IBAction)previewButtonAction:(id)sender;
- (IBAction)purchaseButtonAction:(id)sender;


@end

@implementation AudioPurchaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    
    self.productIdsArray = [[NSMutableArray alloc] init];
    self.productsArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    self.detailDataDictionary = self.purchasingAudioDetailDictionary;
    // Facebook shared data
       TableObject *obj = [[[TableObject alloc] init] autorelease];
     
     NSString *imageStr = [self.purchasingAudioDetailDictionary valueForKey:@"Image"];//
     
     NSString *descriptionStr = [self.purchasingAudioDetailDictionary valueForKey:@"Description"];
     NSString *titleStr = [self.purchasingAudioDetailDictionary valueForKey:@"Title"];
     
     obj.image = [NSString stringWithFormat:@"%@%@",kWebServiceURL,imageStr];
     obj.titleText = titleStr;
     obj.desc =  descriptionStr;
     [super setDetailDataObject:obj];
    
        [self entryAction];
    
    CGFloat spacing = 10;
    
    
    self.purchaseScrollView.contentSize = CGSizeMake(self.purchaseScrollView.frame.size.width, self.descriptionLabel.frame.origin.y + self.descriptionLabel.frame.size.height + spacing );
    [self.purchaseScrollView setFrame:CGRectMake(0.0f, 44.0f, self.purchaseScrollView.frame.size.width, self.descriptionLabel.frame.origin.y + self.descriptionLabel.frame.size.height + spacing )];
    
    if (self.isBackgroundPurchasedAudio) {
        self.headerObject.headerLabel.text = @"Background Music";
        [[self.headerObject.rightToolbarItem objectAtIndex:1] setHidden:YES];
        
    }
    else {
        self.headerObject.headerLabel.text = @"Audio";
        [[self.headerObject.rightToolbarItem objectAtIndex:1] setHidden:NO];
        
    }
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,568, 320, 60);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    //  [self addBannerData];
    //exitAudioDetailScreen
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitAudioPurchaseScreen:) name:@"AudioPurchaseScreenExitNotification" object:nil];
    
    self.headerObject.iconSeperator.hidden = YES;
    [[self.headerObject.rightToolbarItem objectAtIndex:0] setHidden:YES];
    
    self.progressView = [[DACircularProgressView alloc] initWithFrame:CGRectMake(253.0f, 283.0f, 58.0f, 58.0f)];
    self.progressView.roundedCorners = YES;
    self.progressView.thicknessRatio = 0.2f;
    self.progressView.trackTintColor = [UIColor lightGrayColor];
    self.progressView.progressTintColor = [UIColor blueColor];
    [self.purchaseScrollView addSubview:self.progressView];
    self.progressView.hidden = YES;
    
    
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnProgressView:)];
    [self.progressView addGestureRecognizer:tapRecognizer];
    [tapRecognizer release];
    
    self.isBackgroundAudioPlaying = NO;
    
    //get a dispatch queue
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    //this will start the image loading in bg
    dispatch_async(concurrentQueue, ^{
        
        if ([[self.purchasingAudioDetailDictionary objectForKey:@"Image"] length]) {
            
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebServiceURL, [self.purchasingAudioDetailDictionary objectForKey:@"Image"]]];
            NSData *image = [[NSData alloc] initWithContentsOfURL:imageURL];
            
            //this will set the image when loading is finished
            dispatch_async(dispatch_get_main_queue(), ^{
                self.audioImageView.image = [UIImage imageWithData:image];
            });
            
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.audioImageView.image = [UIImage imageNamed:@"no-image.png"];
            });
        }
        
    });
    
    self.audioTitleLabel.text = [self.purchasingAudioDetailDictionary objectForKey:@"Title"];
    self.priceLabel.text = [NSString stringWithFormat:@"Price: $%.2f", [[self.purchasingAudioDetailDictionary objectForKey:@"Price"] floatValue]];
    self.priceLabel.textColor = [UIColor darkGrayColor];
    [self.priceLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
    [self.descriptionLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];

    self.descriptionLabel.text = [self.purchasingAudioDetailDictionary objectForKey:@"Description"];
    
    [self.descriptionLabel resizeToFit];
    
    [self.purchaseScrollView setContentSize:CGSizeMake(320, self.descriptionLabel.frame.origin.y + self.descriptionLabel.frame.size.height)];
    
    self.descriptionLabel.textColor = [UIColor darkGrayColor];
    
    
    self.aProductIdentifierStr = [self.purchasingAudioDetailDictionary objectForKey:@"IphoneProductID"];
    
    [[IAPHelper sharedInstance] initWithProductIdentifiers:[NSSet setWithObject:self.aProductIdentifierStr]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
   
    

}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.addBannerView removeFromSuperview];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)exitAudioPurchaseScreen:(NSNotification *)notification
{
    
    if (self.bgAudioPlayer) {
        [self.bgAudioPlayer pause];
        self.bgAudioPlayer = nil;
//        self.bgAudioPlayerItem = nil;
    }
}



- (void)handleTapOnProgressView:(UITapGestureRecognizer*)recognizer
{
    // Do Your thing.
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [self stopAnimation];
        [self.purchaseScrollView sendSubviewToBack:self.progressView];
        
        self.isBackgroundAudioPlaying = NO;
        [self.previewButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
        [self.bgAudioPlayer pause];
        self.bgAudioPlayer = nil;
        
    }
}

- (void)startAnimation
{
    
    CMTime playerDuration = [self playerItemDuration];
    
    if (CMTimeGetSeconds(playerDuration) < [[self.purchasingAudioDetailDictionary objectForKey:@"PreviewInterval"] floatValue]) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:CMTimeGetSeconds(playerDuration)/10.0f
                                                      target:self
                                                    selector:@selector(progressChange)
                                                    userInfo:nil
                                                     repeats:YES];
        
    }
    else {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:[[self.purchasingAudioDetailDictionary objectForKey:@"PreviewInterval"] floatValue]/10.0f
                                                      target:self
                                                    selector:@selector(progressChange)
                                                    userInfo:nil
                                                     repeats:YES];
        
    }
}

- (void)stopAnimation
{
    [self.progressView setProgress:0.f animated:YES];
    [self.timer invalidate];
    self.timer = nil;
}

- (void)progressChange
{
    CGFloat progress = self.progressView.progress + [[self.purchasingAudioDetailDictionary objectForKey:@"PreviewInterval"] floatValue]/([[self.purchasingAudioDetailDictionary objectForKey:@"PreviewInterval"] floatValue]*10);
    [self.progressView setProgress:progress animated:YES];
    
}


- (void)playBackgroundAudio:(NSDictionary *)detailsDictionary
{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    
    HUD.delegate = self;
    HUD.square = YES;
    
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    
    NSURL *fileURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", [detailsDictionary objectForKey:@"FilePath"]]];
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
    NSString *tracksKey = @"tracks";
    
    [asset loadValuesAsynchronouslyForKeys:@[tracksKey] completionHandler:
     ^{
         // The completion block goes here.
         
         static const NSString *bgItemStatusContext;
         
         // Completion handler block.
         dispatch_async(dispatch_get_main_queue(),
                        ^{
                            NSError *error;
                            AVKeyValueStatus status = [asset statusOfValueForKey:tracksKey error:&error];
                            
                            if (status == AVKeyValueStatusLoaded) {
                                self.bgAudioPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
                                // ensure that this is done before the playerItem is associated with the player
                                [self.bgAudioPlayerItem addObserver:self forKeyPath:@"status"
                                                            options:NSKeyValueObservingOptionInitial context:&bgItemStatusContext];
                                [[NSNotificationCenter defaultCenter] addObserver:self
                                                                         selector:@selector(bgPlayerItemDidReachEnd:)
                                                                             name:AVPlayerItemDidPlayToEndTimeNotification
                                                                           object:self.bgAudioPlayerItem];
                                self.bgAudioPlayer = [AVPlayer playerWithPlayerItem:self.bgAudioPlayerItem];
                            }
                            else {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:[NSString stringWithFormat:@"The asset's tracks were not loaded:\n%@", [error localizedDescription]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                [alert show];
                                
                                // You should deal with the error appropriately.
                                NSLog(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
                            }
                        });
     }];
    
    
}

- (void)bgPlayerItemDidReachEnd:(NSNotification *)notification
{
    
    [self.previewButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    
    self.bgAudioPlayer = nil;
    
    [self.bgAudioPlayer seekToTime:kCMTimeZero];
    
    
    
}

- (void)myTask {
	// Do something usefull in here instead of sleeping ...
	sleep(1);
}


#pragma mark - Key Value Observer for PlayerItem -

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if (object == self.bgAudioPlayerItem && [keyPath isEqualToString:@"status"]) {
        if (self.bgAudioPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"BG AVPlayer Failed");
        } else if (self.bgAudioPlayer.status == AVPlayerStatusReadyToPlay) {
            
            
            [self.view bringSubviewToFront:self.progressView];
            
            [self.progressView setProgress:0.01 animated:YES];
            
            [self startAnimation];
            
            [self.bgAudioPlayer play];
            
            self.isBackgroundAudioPlaying = YES;
            
            [HUD show:NO];
            
            Float64 interval = 0.1f;
            /* Update the scrubber during normal playback. */
            self.timeObserver = [[self.bgAudioPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                                  queue:NULL
                                                                             usingBlock:
                                  ^(CMTime time)
                                  {
                                      [self syncScrubber];
                                  }] retain];
            
            
            
        } else if (self.bgAudioPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"BG AVPlayer Unknown");
        }
    }
}

#pragma mark - Player Item Duration -
- (CMTime)playerItemDuration
{
    AVPlayerItem *thePlayerItem = [self.bgAudioPlayer currentItem];
    if (thePlayerItem.status == AVPlayerItemStatusReadyToPlay)
    {
        
        return([self.bgAudioPlayerItem duration]);
    }
    
    return(kCMTimeInvalid);
}


- (void)syncScrubber
{
    CMTime playerDuration = [self playerItemDuration];
    //    if (CMTIME_IS_INVALID(playerDuration)) {
    //        return;
    //    }
    
    if (CMTimeGetSeconds(playerDuration) < [[self.purchasingAudioDetailDictionary objectForKey:@"PreviewInterval"] floatValue]) {
        
        double duration = CMTimeGetSeconds(playerDuration);
        NSLog(@"duration %.2f",duration);
        
        
        
        if (isfinite(duration) && (duration > 0))
        {
            double time = CMTimeGetSeconds([self.bgAudioPlayer currentTime]);
            
            if (time >= CMTimeGetSeconds(playerDuration)) {
                
                self.progressView.hidden = YES;
                [self stopAnimation];
                
                self.isBackgroundAudioPlaying = NO;
                [self.previewButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
                [self.bgAudioPlayer pause];
                self.bgAudioPlayer = nil;
            }
        }
        
        
    }
    else {
        
        double duration = CMTimeGetSeconds(playerDuration);
        //        NSLog(@"duration %.2f",duration);
        
        
        
        if (isfinite(duration) && (duration > 0))
        {
            double time = CMTimeGetSeconds([self.bgAudioPlayer currentTime]);
            
            if (time >= [[self.purchasingAudioDetailDictionary objectForKey:@"PreviewInterval"] floatValue]) {
                
                self.progressView.hidden = YES;
                [self stopAnimation];
                
                self.isBackgroundAudioPlaying = NO;
                [self.previewButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
                [self.bgAudioPlayer pause];
                self.bgAudioPlayer = nil;
            }
        }
        
    }
    
    
    
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	[HUD release];
	HUD = nil;
}



#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Audio"]) {
            self.aboutBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aboutBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aboutBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aboutBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    [self entryActionBanner];

}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


- (IBAction)previewButtonAction:(id)sender
{
    
    if (self.isBackgroundAudioPlaying) {
        
        self.progressView.hidden = YES;
        [self.purchaseScrollView sendSubviewToBack:self.progressView];
        
        [self.previewButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
        
        // Music is currently playing
        [self.bgAudioPlayer pause];
        self.bgAudioPlayer = nil;
        self.isBackgroundAudioPlaying = NO;
    }
    else {
        
        self.progressView.hidden = NO;
        [self.purchaseScrollView bringSubviewToFront:self.progressView];
        // Music is currenty paused/stopped
        [self.previewButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
        [self playBackgroundAudio:self.purchasingAudioDetailDictionary];
        self.isBackgroundAudioPlaying = YES;
        
    }
    
}


- (void)audioFileDownLoading
{
    [[LeveyHUD sharedHUD] appearWithText:@"Downloading..."];
    
    NSString *fileNameStr = [self.purchasingAudioDetailDictionary valueForKey:@"FileName"];
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString* filePath = [documentsPath stringByAppendingPathComponent:fileNameStr];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    NSLog(@"filePath %@",filePath);
    if (fileExists) {
        [[LeveyHUD sharedHUD] disappear];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else{
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", [self.purchasingAudioDetailDictionary objectForKey:@"FilePath"]]];
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDownloadTask * downloadTask = [ defaultSession downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error)
                                                   {
                                                       if(error == nil)
                                                       {
                                                           NSError *err = nil;
                                                           NSFileManager *fileManager = [NSFileManager defaultManager];
                                                           
                                                           NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                                           
                                                           NSURL *docsDirURL = [NSURL fileURLWithPath:[docsDir stringByAppendingPathComponent:fileNameStr]];
                                                           [[LeveyHUD sharedHUD] disappear];
                                                           
                                                           
                                                           
                                                           
                                                           
                                                           if ([fileManager moveItemAtURL:location toURL:docsDirURL error: &err])
                                                           {
                                                               AudioDetailViewController *audioDetailVC = [[AudioDetailViewController alloc] initWithNibName:@"AudioDetailViewController" bundle:nil];
                                                               NSLog(@"File is saved to =%@",docsDir);
                                                               audioDetailVC.appType = APP_AUDIO_DETAIL;
                                                               audioDetailVC.audioDetailDictionary = self.purchasingAudioDetailDictionary;
                                                               
                                                               NSLog(@"BoolValue %d",self.boolNoListing);
                                                               if (self.boolNoListing == YES){
                                                                   [[SPSingletonClass sharedInstance] setIsMainViewCntr:@"YES"];
                                                                   //isMainViewCntr
                                                                   
                                                               }else{
                                                                   [[SPSingletonClass sharedInstance] setIsMainViewCntr:@"NO"];                                                                   
                                                               }
                                                               
                                                               [self.navigationController pushViewController:audioDetailVC animated:YES];
                                                               
                                                               
                                                           }
                                                           else
                                                           {
                                                               NSLog(@"failed to move: %@",[err userInfo]);
                                                               AudioDetailViewController *audioDetailVC = [[AudioDetailViewController alloc] initWithNibName:@"AudioDetailViewController" bundle:nil];
                                                               audioDetailVC.appType = APP_AUDIO_DETAIL;
                                                               audioDetailVC.audioDetailDictionary = self.purchasingAudioDetailDictionary;
                                                               
                                                               if (self.boolNoListing == YES){
                                                                   self.boolNoListing = YES;
                                                                   
                                                               }else{
                                                                   self.boolNoListing = NO;

                                                               }
                                                               [self.navigationController pushViewController:audioDetailVC animated:YES];
                                                               
                                                               
                                                               //                                                               UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information" message:@"An error has occurred while file downloading!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                               //                                                               [alertView show];
                                                               
                                                           }
                                                           
                                                           
                                                       }
                                                   }];
        
        [downloadTask resume];
        
    }
    
}


- (IBAction)purchaseButtonAction:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[self.purchasingAudioDetailDictionary valueForKey:@"Title"] message:[NSString stringWithFormat:@"Price: $%.2f", [[self.purchasingAudioDetailDictionary objectForKey:@"Price"] floatValue]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Buy",@"Restore Purchase",@"Cancel", nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0) {
        
        if([SKPaymentQueue canMakePayments])
        {
            [self buyProduct];
        }
        else {
            NSLog(@"IN-APP:can't make payments");
        }
    }
    if (buttonIndex == 1) {
        [[IAPHelper sharedInstance] restoreCompletedTransactions];
        [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            self.productsArray = (NSMutableArray *)products;
        }];
        
    }
}


#pragma mark -
#pragma mark In-App Purchase Delegate Methods

- (void)buyProduct
{
    //to send product identifiers request
    [[LeveyHUD sharedHUD] appearWithText:@"Loading..."];
    [[IAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        [[LeveyHUD sharedHUD] disappear];
        self.productsArray = (NSMutableArray *)products;

        if (success && [self.productsArray count] > 0) {
            [[IAPHelper sharedInstance] buyProduct:self.productsArray[0]];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Unable to purchase at this time, Please try after few minutes" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }];
    
}

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    
    
    
    [self.productsArray enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            [self audioFileDownLoading];
            *stop = YES;
        }
    }];
    
}


- (void)showAlert:(NSString *)message withMessage:(NSString *)cancelMsg
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}



-(void)entryAction
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    self.itemId  =  [self.purchasingAudioDetailDictionary objectForKey:@"AudioID"];
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    else{
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Detail" deviceType:@"iOS"];
    }
    
    
}

-(void)entryActionBanner
{
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}



@end
