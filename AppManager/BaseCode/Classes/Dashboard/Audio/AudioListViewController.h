//
//  AudioListViewController.h
//  Exceeding
//
//  Created by NarasimhamPV on 29/05/14.
//
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppMainVC.h"


#import "AppMainDetailVC.h"
#import "TableObject.h"
#import "UILabel+Exceedings.h"
#import "InfoCollectorVC.h"
#import <MessageUI/MessageUI.h>
#import "BigBanners.h"

@interface AudioListViewController : AppMainVC<BigBannersDelegate>
@property (nonatomic, strong) BigBanners *bigBanner;

@end
