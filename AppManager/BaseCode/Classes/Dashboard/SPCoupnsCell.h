//
//  SPCoupnsCell.h
//  Exceeding
//
//  Created by SREELAKSHMI on 20/02/14.
//
//

#import <UIKit/UIKit.h>

@interface SPCoupnsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *couponImageView;
@property (nonatomic, strong) IBOutlet UILabel *couponDescriptionLabel;
@property (nonatomic, strong) IBOutlet UILabel *validDateLabel;


@end
