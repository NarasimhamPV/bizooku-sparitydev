//
//  SPCouponsListViewController.m
//  Exceeding
//
//  Created by VENKATALAKSHMI on 24/04/14.
//
//

#import "SPCouponsListViewController.h"
#import "SPCouponsListCell.h"
#import "SPCouponDetailsViewController.h"
#import "AppDelegate.h"
#import "SDZEXWidgetServiceExample.h"
#import "SPSingletonClass.h"
#import "CustomBannerView.h"
#import "WebViewController.h"

static NSString *const kcustomCouponsCell = @"couponsListCellIdentifier";
static NSString *const kcouponsCellNib = @"SPCouponsListCell";

@interface SPCouponsListViewController () <UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UILabel *noDataLabel;
}
@property (retain, nonatomic) IBOutlet UIView *bannerView;
@property (retain, nonatomic) IBOutlet UIImageView *bannerimg;
//@property (strong,nonatomic) AppHeaderBar *headerBarView;
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
@property (retain, nonatomic) IBOutlet UITableView *couponsListTableView;
//Banner
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation SPCouponsListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self entryActionAnalytics];
    
    self.headerObject.rightToolbarItem = nil;
    self.headerObject.btnShare.hidden = YES;
    
    [self.couponsListTableView registerNib:[UINib nibWithNibName:kcouponsCellNib bundle:nil] forCellReuseIdentifier:kcustomCouponsCell];
    self.couponsListTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCouponsListNotification:)name:@"getCouponsListNotification" object:nil];
    
    self.couponListDict = [[NSDictionary alloc] init];

    self.couponsListArray = [[NSMutableArray alloc] init];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runCouponsList];
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
}

- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
}



- (void)receiveCouponsListNotification:(NSNotification *) notification
{
    self.couponsListArray = [[AppDelegate shareddelegate].couponsListDict mutableCopy];
    
    if ([self.couponsListArray count]==0){
        noDataLabel.hidden = NO;
        
    }else{
        noDataLabel.hidden = YES;
        [self.couponsListTableView reloadData];
        
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [self.couponsListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SPCouponsListCell *customCoupnsListCell = [tableView dequeueReusableCellWithIdentifier:kcustomCouponsCell];
    customCoupnsListCell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *temp = [self.couponsListArray objectAtIndex:indexPath.row];
    NSString *imageStr = [temp objectForKey:@"Image"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,imageStr];
    
    // spawn a new thread to load the image in the background, from the network
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
        NSURLResponse* response = nil;
        NSError* error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            // update the image and sent notification on the main thread
            customCoupnsListCell.redeemImageView.hidden = YES;
            if (imageStr.length >0) {
                customCoupnsListCell.couponListImage.image = [UIImage imageWithData:data];
                if ([[[self.couponsListArray objectAtIndex:indexPath.row] valueForKey:@"Redeemptions"] boolValue]) {
                    customCoupnsListCell.redeemImageView.hidden = NO;
                    customCoupnsListCell.redeemImageView.image =[UIImage imageNamed:@"redeem.png"];
                }
                
            }else{
                customCoupnsListCell.couponListImage.image = [UIImage imageNamed:@"no-image.png"];
                if ([[[self.couponsListArray objectAtIndex:indexPath.row] valueForKey:@"Redeemptions"] boolValue]) {
                    customCoupnsListCell.redeemImageView.hidden = NO;
                    customCoupnsListCell.redeemImageView.image =[UIImage imageNamed:@"redeem.png"];
                }
            }
            //sandy
            
        });
    });
    return customCoupnsListCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 240.0f;
}

#pragma mark --
#pragma mark -- Table view delegate

-  (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.couponsListArray count] > 0) {
        
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [tempDict setDictionary:[self.couponsListArray objectAtIndex:indexPath.row]];
        
        NSString *tempStr = [tempDict valueForKey:@"BrandId"];
        [[NSUserDefaults standardUserDefaults] setObject:tempStr forKey:@"BrandId"];

        SPCouponDetailsViewController *couponDetailViewCntr = [[SPCouponDetailsViewController alloc] initWithNibName:@"SPCouponDetailsViewController" bundle:nil];
        couponDetailViewCntr.couponsDetailsDict = tempDict;
        couponDetailViewCntr.appType = APP_COUPONSDETAILS;
        [self.navigationController pushViewController:couponDetailViewCntr animated:YES];
        
    }
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_couponsListTableView release];
    [_bannerCancelBtn release];
    [_bannerimg release];
    [_bannerView release];
    [super dealloc];
}

- (IBAction)backBtn:(id)sender
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Coupons"]) {
            self.aBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //[self.addBannerView startShowingBanners:self.bannerUrlsArray];
        
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Listing"];
        
        
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    [self entryAction];

    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryActionAnalytics
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    self.itemId = [self.couponListDict objectForKey:@"CategoryId"];
    
    
    if(!self.tileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    else{
        
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    
}


-(void)entryAction
{
    
    NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID integerValue] widgetType:@"Listing" deviceType:@"iOS"];
    
}

- (void)changeRedeemImage
{
     [self.couponsListTableView reloadData];
}

@end
