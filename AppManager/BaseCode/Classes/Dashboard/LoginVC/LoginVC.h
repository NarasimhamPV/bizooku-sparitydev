//
//  LoginVC.h
//  Exceeding
//
//  Created by Rajesh Kumar on 27/01/14.
//
//

#import <UIKit/UIKit.h>
#import "SDZEXLayoutServiceExample.h"

@interface LoginVC : UIViewController<UITextFieldDelegate, SDZEXLayoutServiceExampleDelegate>

@property (nonatomic, strong) IBOutlet UITextField *txtUsername, *txtPassword;
@property (nonatomic, strong) IBOutlet UIButton *btnStaySignedIn;

- (IBAction) staySignedInAction:(id)sender;
- (IBAction) signInAction:(id)sender;
- (IBAction) skipAction:(id)sender;


@end
