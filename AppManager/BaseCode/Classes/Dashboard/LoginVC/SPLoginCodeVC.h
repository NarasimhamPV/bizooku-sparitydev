//
//  SPLoginCodeVC.h
//  Exceeding
//
//  Created by Veeru on 21/10/14.
//
//

#import <UIKit/UIKit.h>
#import "SDZEXLayoutServiceExample.h"

@interface SPLoginCodeVC : UIViewController<SDZEXLayoutServiceExampleDelegate>
@property (strong, nonatomic) IBOutlet UITextField *codeTF;
@property	CGFloat shiftForKeyboard;

@end
