//
//  LoginVC.m
//  Exceeding
//
//  Created by Rajesh Kumar on 27/01/14.
//
//

#import "LoginVC.h"
#import "ViewController.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "JSON.h"




@interface LoginVC ()

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.hidden = YES;
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginViewCntr"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    BOOL isStaySignedIn = NO;
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:kStaySignedIn];
    
    if(str && ![str isKindOfClass:[NSNull class]])
    {
        if([str isEqualToString:@"YES"])
        {
            isStaySignedIn = YES;
            
            NSString *strUsername = [[NSUserDefaults standardUserDefaults] objectForKey:kStayUserName];
            if(strUsername && ![strUsername isKindOfClass:[NSNull class]])
            {
                _txtUsername.text = strUsername;
            }
            
            NSString *strPassword = [[NSUserDefaults standardUserDefaults] objectForKey:kStayPassword];
            if(strPassword && ![strPassword isKindOfClass:[NSNull class]])
            {
                _txtPassword.text = strPassword;
            }
        }
        else{
            isStaySignedIn = NO;
        }
    }
    
    if(isStaySignedIn)
    {
        _btnStaySignedIn.selected = YES;
    }
    else{
        _btnStaySignedIn.selected = NO;
    }
    
}


- (IBAction) staySignedInAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(btn.selected == NO)
    {
        _btnStaySignedIn.selected = YES;
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:kStaySignedIn];
        [[NSUserDefaults standardUserDefaults] setObject:_txtUsername.text forKey:kStayUserName];
        [[NSUserDefaults standardUserDefaults] setObject:_txtPassword.text forKey:kStayPassword];
        
        
    }
    else{
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kStaySignedIn];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kStayUserName];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kStayPassword];
        
        _btnStaySignedIn.selected = NO;
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


- (IBAction) signInAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoginViewCntr"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if(![_txtUsername.text isEqualToString:@""])
    {
        if(![_txtPassword.text isEqualToString:@""])
        {
            if(_btnStaySignedIn.selected)
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:kStaySignedIn];
                [[NSUserDefaults standardUserDefaults] setObject:_txtUsername.text forKey:kStayUserName];
                [[NSUserDefaults standardUserDefaults] setObject:_txtPassword.text forKey:kStayPassword];
            }
            else{
                [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kStaySignedIn];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kStayUserName];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kStayPassword];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [_txtPassword resignFirstResponder];
            
            SDZEXLayoutServiceExample *exmp = [[SDZEXLayoutServiceExample alloc] init];
            exmp.delegate = self;
            [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
            [exmp runMeWithUsername:_txtUsername.text password:_txtPassword.text];
            
        }
        else{
            [AppDelegate showAlert:@"Please enter correct password"];
        }
    }
    else{
        [AppDelegate showAlert:@"Please enter registered email id"];
    }
    
}

- (void) callSuccess:(id)sender
{
    //
    NSLog(@"Sender value : %@", sender);
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    NSString *responseStr = (NSString *)sender;
    id object = [responseStr JSONValue];
    if([object isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dic = (NSDictionary *)object;
        if([[dic allKeys] containsObject:@"ErrorMessage"])
        {
            //           [AppDelegate showAlert:[dic objectForKey:@"ErrorMessage"]];
            
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please Try Again" message:@"Invalid username or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        else if([[dic allKeys] containsObject:@"AccessKey"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:[dic objectForKey:@"AccessKey"] forKey:kBizookuAccessKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self loadScreen];
            [[AppDelegate shareddelegate] updateHUDActivity:@"Completing Setup..." show:YES];
            
            
        }
    }
}


- (void) loadScreen
{
    [AppDelegate shareddelegate].viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    UINavigationController *navController = [[[UINavigationController alloc]initWithRootViewController:[AppDelegate shareddelegate].viewController] autorelease];
    [AppDelegate shareddelegate].window.rootViewController = navController;
    [[AppDelegate shareddelegate].window makeKeyAndVisible];
    [[AppDelegate shareddelegate] updateHUDActivity:@"Loading..." show:YES];
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runDownloadInfoAddition];
}

- (IBAction) skipAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoginViewCntr"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kBizookuAccessKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self loadScreen];
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == _txtUsername)
    {
        [_txtPassword becomeFirstResponder];
    }
    else if(textField == _txtPassword)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}// called when 'return' key pressed. return
@end
