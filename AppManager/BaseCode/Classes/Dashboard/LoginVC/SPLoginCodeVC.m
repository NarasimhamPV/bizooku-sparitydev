//
//  SPLoginCodeVC.m
//  Exceeding
//
//  Created by Veeru on 21/10/14.
//
//

#import "SPLoginCodeVC.h"
#import "AppDelegate.h"
#import "SDZEXLayoutServiceExample.h"
#import "ViewController.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "JSON.h"


@interface SPLoginCodeVC ()
{
    int codeLenth;
}
@property (strong, nonatomic) IBOutlet UIView *customAlertView;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UIImageView *splashImg;
@property (strong, nonatomic) UITapGestureRecognizer *singleTapGestureRecognize;
@property (strong, nonatomic) NSString *codeLoginType;

@end

@implementation SPLoginCodeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate *del = [AppDelegate shareddelegate];
    self.codeLoginType = del.appReference.codeLoginType;
    NSLog(@"codeLoginType %@",self.codeLoginType);
    if ([self.codeLoginType isEqualToString:@"Multi Code"]){
        self.headerLbl.text =@"PLEASE ENTER ACCESS CODE";
        codeLenth = 11;
    }else{
        codeLenth = 8;

        self.headerLbl.text =@"PLEASE ENTER 7-DIGIT CODE";

    }
    self.navigationController.navigationBar.hidden = YES;
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"FirstTimeSevenDigitCode"];

    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SevenDigitViewCntr"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    self.splashImg.image = [UIImage imageWithContentsOfFile:[self splashImagePath]];
    
    [self.headerLbl setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:18.0f]];
    
    self.submitBtn.titleLabel.font =[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:22.0f];
    
    
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 50)];
    self.codeTF.leftView = paddingView1;
    self.codeTF.leftViewMode = UITextFieldViewModeAlways;
    self.codeTF.rightViewMode = UITextFieldViewModeAlways;
    self.codeTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.codeTF.layer.borderColor=[UIColor colorWithRed:190.0/255.0 green:190.0/255.0 blue:190.0/255.0 alpha:1.0].CGColor;
    [self.codeTF setTextColor:[UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1.0]];
    self.codeTF.font=[UIFont fontWithName:@"Helvetica-Condensed" size:22];
    
    self.singleTapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyBoard)];
    self.singleTapGestureRecognize.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:self.singleTapGestureRecognize];
    
    

    
    
}
- (void)resignKeyBoard
{
    [self.codeTF resignFirstResponder];
 
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)splashImagePath{
    
    DBApp* _appReference = [AppDelegate shareddelegate].appReference;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *splashPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"splash_%@", [_appReference.splashimage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
        return splashPath;
}

- (IBAction)signInApp:(id)sender
{
    
    [self.codeTF resignFirstResponder];
    SDZEXLayoutServiceExample *exmp = [[SDZEXLayoutServiceExample alloc] init];

    if ([self.codeLoginType isEqualToString:@"Multi Code"]){
        if(![self.codeTF.text isEqualToString:@""])
        {
            exmp.delegate = self;
            [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
            [exmp runMeWithNineDigitCode:[self.codeTF.text stringByReplacingOccurrencesOfString:@"-" withString:@""] ];
            
        }
        else{
            [AppDelegate showAlert:@"Please Enter Access Code"];
        }
    }else{
        if(![self.codeTF.text isEqualToString:@""])
        {
            exmp.delegate = self;
            [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
            [exmp runMeWithsevenDigitCode:[self.codeTF.text stringByReplacingOccurrencesOfString:@"-" withString:@""] ];
            
        }
        else{
            [AppDelegate showAlert:@"Please Enter 7 Digit Code"];
        }

    }
    
    
    
}
- (void)nineDigitCode
{
    SDZEXLayoutServiceExample *exmp = [[SDZEXLayoutServiceExample alloc] init];
    exmp.delegate = self;
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
    [exmp runMeWithNineDigitCode:[self.codeTF.text stringByReplacingOccurrencesOfString:@"-" withString:@""] ];
    

}


- (void) callSuccess:(id)sender
{
        
    NSString *responseStr = (NSString *)sender;
    
    if([sender isKindOfClass:[NSError class]]) {
        [[AppDelegate shareddelegate] updateHUDActivity:nil show:NO];

        [AppDelegate showAlert:kOfflineError];

        return;
    }
    
    id object = [responseStr JSONValue];
    if([object isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dic = (NSDictionary *)object;
        if([[dic allKeys] containsObject:@"ErrorMessage"])
        {
            
            self.codeTF.text = @"";
            [[AppDelegate shareddelegate] updateHUDActivity:nil show:NO];

            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please Try Again" message:[dic valueForKey:@"ErrorMessage"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        else if([[dic allKeys] containsObject:@"AccessKey"])
        {
            //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SevenDigitViewCntr"];
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"FirstTimeSevenDigitCode"];

            [[NSUserDefaults standardUserDefaults] setObject:[dic objectForKey:@"AccessKey"] forKey:kBizookuAccessKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Veeru %@",[[NSUserDefaults standardUserDefaults] valueForKey:kBizookuAccessKey]);
            
            [self loadScreen];
//            [[AppDelegate shareddelegate] updateHUDActivity:@"Completing Setup..." show:YES];

            
        }
    }
}


- (void)loadScreen
{
    [AppDelegate shareddelegate].viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    UINavigationController *navController = [[[UINavigationController alloc]initWithRootViewController:[AppDelegate shareddelegate].viewController] autorelease];
    [AppDelegate shareddelegate].window.rootViewController = navController;
    [[AppDelegate shareddelegate].window makeKeyAndVisible];
    [[AppDelegate shareddelegate] updateHUDActivity:@"Loading..." show:YES];
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runDownloadInfoAddition];
}


#pragma mark - UIText Field Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    
    
	CGRect textViewRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGFloat bottomEdge = textViewRect.origin.y + textViewRect.size.height;
	if (bottomEdge >= 140) {//250
        CGRect viewFrame = self.view.frame;
        
        if (IS_IPHONE5){
            self.shiftForKeyboard = bottomEdge - 250;//193
            
        }else{
            self.shiftForKeyboard = bottomEdge - 160;//193
        }
        viewFrame.origin.y -= self.shiftForKeyboard;
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationBeginsFromCurrentState:YES];
		[UIView setAnimationDuration:0.3];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];
		
	} else {
		self.shiftForKeyboard = 0.0f;
	}
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if (self.view.frame.origin.y == 0) {
        [textField resignFirstResponder];
    }
    else{
     	// Resign first responder
        [textField resignFirstResponder];
        
        
        // Make a CGRect for the view (which should be positioned at 0,0 and be 320px wide and 480px tall)
        CGRect viewFrame = self.view.frame;
        if(viewFrame.origin.y!=0)
        {
            // Adjust the origin back for the viewFrame CGRect
            viewFrame.origin.y += self.shiftForKeyboard;
            // Set the shift value back to zero
            self.shiftForKeyboard = 0.0f;
            
            // As above, the following animation setup just makes it look nice when shifting
            // Again, we don't really need the animation code, but we'll leave it in here
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:0.3];
            // Apply the new shifted vewFrame to the view
            [self.view setFrame:viewFrame];
            // More animation code
            [UIView commitAnimations];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    switch (textField.tag) {
        case 1: //Username Text field
        {
            // All digits entered
            if (range.location == codeLenth || newLength > codeLenth )
            {
                return NO;
            }
            
            // Reject appending non-digit characters
            if (range.length == 0 &&
                ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
                return NO;
            }
            if ([self.codeLoginType isEqualToString:@"Multi Code"]){
                // Auto-add hyphen before appending 3th or 7th
                if (range.length == 0 &&
                    (range.location == 3 || range.location == 7)) {
                    textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
                    return NO;
                }
                
                // Delete hyphen when deleting its trailing digit
                if (range.length == 1 &&
                    (range.location == 4 || range.location == 8))  {
                    range.location--;
                    range.length = 2;
                    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                
                // Auto-add hyphen before appending 3rd or 7th digit
                if (range.length == 0 &&
                    (range.location == 3)) {
                    textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
                    return NO;
                }
                // Delete hyphen when deleting its trailing digit
//                NSLog(@"range %lu %lu",(unsigned long)range.length,(unsigned long)range.location);
                
                if (range.length == 1 &&
                    (range.location == 4))  {
                    range.location--;
                    range.length = 2;
                    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            
            
            return YES;
        }
            break;
            
            
        default:
            
            return YES;
            
            break;
    }
    
    
}


@end
