//
//  SPTermsAndConditionsVC.h
//  Exceeding
//
//  Created by Veeru on 20/05/14.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SPTermsAndConditionsVC : UIViewController
{
    BOOL adsremoved;
}
@property (nonatomic, strong) NSMutableArray *bannerURLS;
- (void)startShowingBanners:(NSString *)bannerURL;

@property (nonatomic,strong) NSString *termsandConditionsStr;
@property (nonatomic, strong) NSDictionary *termsBannersDict;
@end
