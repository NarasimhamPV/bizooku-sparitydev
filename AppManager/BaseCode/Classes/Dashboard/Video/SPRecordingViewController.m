//
//  SPRecordingViewController.m
//  VideoRecordingView
//
//  Created by Sandeep on 06/11/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import "SPRecordingViewController.h"
#import "AppDelegate.h"

@interface SPRecordingViewController ()

{
    NSTimeInterval startTime;
}

//- (void)changeFlashMode:(id)sender;
//- (void)changeCamera:(id)sender;

- (void)createImagePicker;
//- (void)startRecording;
//- (void)stopRecording;
@property (nonatomic) UIImagePickerControllerCameraFlashMode flashMode;
@property (retain, nonatomic) IBOutlet UIView *topView;
@property (retain, nonatomic) IBOutlet UIButton *cameraSelectionButton;
@property (retain, nonatomic) IBOutlet UILabel *timeLabel;
@property (retain, nonatomic) IBOutlet UIButton *recordingBtn;
@property (retain, nonatomic) IBOutlet UIButton *flashModeButton;
@property (nonatomic, assign) NSTimer *timer;
@property (retain, nonatomic) IBOutlet UIButton *doneBtn;
- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
@end

@implementation SPRecordingViewController

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIApplication sharedApplication].statusBarHidden = YES;
    _cameraSelectionButton.alpha = 0.0;
    _flashModeButton.alpha = 0.0;
    
    self.doneBtn.hidden = YES;
    [self createImagePicker];
    
 }

- (void)viewWillAppear:(BOOL)animated
{
    
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    
    
    [_topView release];
    [_timeLabel release];
    [_recordingBtn release];
    [_cameraSelectionButton release];
    [_flashModeButton release];
    [_doneBtn release];
    [super dealloc];
}

- (void)createImagePicker
{
    
    imagePicker = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {

    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    imagePicker.mediaTypes = [NSArray arrayWithObject:@"public.movie"];
    imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;

    imagePicker.allowsEditing = NO;
    imagePicker.showsCameraControls = NO;
    // Device's screen size (ignoring rotation intentionally):
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    // iOS is going to calculate a size which constrains the 4:3 aspect ratio
    // to the screen size. We're basically mimicking that here to determine
    // what size the system will likely display the image at on screen.
    // NOTE: screenSize.width may seem odd in this calculation - but, remember,
    // the devices only take 4:3 images when they are oriented *sideways*.
    float cameraAspectRatio = 4.0 / 3.0;
    float imageWidth = floorf(screenSize.width * cameraAspectRatio);
    float scale = ceilf((screenSize.height / imageWidth) * 10.0) / 10.0;
    imagePicker.cameraViewTransform = CGAffineTransformMakeScale(scale, scale);
    imagePicker.cameraOverlayView = cameraOverlayView;
    
    // not all devices have two cameras or a flash so just check here
    if ( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceRear] ) {
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        if ( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront] ) {
            _cameraSelectionButton.alpha = 1.0;
            showCameraSelection = YES;
        }
    } else {
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    
    if ( [UIImagePickerController isFlashAvailableForCameraDevice:imagePicker.cameraDevice] ) {
        self.flashMode = UIImagePickerControllerCameraFlashModeOff;
        imagePicker.cameraFlashMode = self.flashMode;
        _flashModeButton.alpha = 1.0;
        showFlashMode = YES;
    }
    
    imagePicker.videoQuality = UIImagePickerControllerQualityType640x480;
    
    imagePicker.delegate = self;
    imagePicker.extendedLayoutIncludesOpaqueBars = YES;
    
    CGRect theRect = [imagePicker.view frame];
    [cameraOverlayView setFrame:theRect];
    
    [self presentViewController:imagePicker animated:YES completion:nil];
    imagePicker.cameraOverlayView = cameraOverlayView;
}


//- (void)toggleVideoRecording {
//    if (!recording) {
//        recording = YES;
//        [self startRecording];
//    } else {
//        recording = NO;
//        [self stopRecording];
//    }
//}

- (IBAction)flashBtnClicked:(id)sender

{
    
    if (self.flashMode == UIImagePickerControllerCameraFlashModeAuto) {
        //toggle your button to "on"
        self.flashMode = UIImagePickerControllerCameraFlashModeOn;
        [_flashModeButton setImage:[UIImage imageNamed:@"flash-on.png"] forState:UIControlStateNormal];

    }else if (self.flashMode == UIImagePickerControllerCameraFlashModeOn){
        //toggle your button to "Off"
        self.flashMode = UIImagePickerControllerCameraFlashModeOff;
        [_flashModeButton setImage:[UIImage imageNamed:@"flash-off.png"] forState:UIControlStateNormal];

    }else if (self.flashMode == UIImagePickerControllerCameraFlashModeOff){
        //toggle your button to "Auto"
        self.flashMode = UIImagePickerControllerCameraFlashModeAuto;
        [_flashModeButton setImage:[UIImage imageNamed:@"flash-auto.png"] forState:UIControlStateNormal];
    }
    
    imagePicker.cameraFlashMode = self.flashMode;

}

- (IBAction)cameraBtnClicked:(id)sender
{
    
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {

    if (imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceRear) {
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    } else {
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
    
    if ( ![UIImagePickerController isFlashAvailableForCameraDevice:imagePicker.cameraDevice] ) {
        [UIView animateWithDuration:0.3 animations:^(void) {_flashModeButton.alpha = 0;}];
        showFlashMode = NO;
    } else {
        [UIView animateWithDuration:0.3 animations:^(void) {_flashModeButton.alpha = 1.0;}];
        showFlashMode = YES;
    }
    }
}

#pragma mark - Timer Handler

- (void)timerHandler:(NSTimer *)timer {
    
    NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval recorded = current - startTime;
    
    NSUInteger seconds = ABS((int)recorded);
    NSUInteger minutes = seconds/60;
    NSUInteger hours = minutes/60;
    self.timeLabel.text = [NSString stringWithFormat:@"%02lu:%02u:%02u", (unsigned long)hours, minutes%60, seconds%60];
    
}


-(void) startFlashingbutton
{
    if (!isRecording) return;
    isRecording = YES;
    self.recordingBtn.alpha = 1.0f;
    
    
    [UIView animateWithDuration:0.12
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionRepeat |
     UIViewAnimationOptionAutoreverse |
     UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.recordingBtn.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         // Do nothing
                     }];
}

-(void) stopFlashingbutton
{
    if (isRecording) return;
    isRecording = NO;
    [UIView animateWithDuration:0.12
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.recordingBtn.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         // Do nothing
                     }];
}


- (IBAction)startRecording:(id)sender

{
    void (^hideControls)(void);
    hideControls = ^(void) {
        _cameraSelectionButton.alpha = 0;
        _flashModeButton.alpha = 0;
        
        
    };
    
    void (^recordMovie)(BOOL finished);
    recordMovie = ^(BOOL finished) {
        [imagePicker startVideoCapture];
        self.doneBtn.hidden = NO;
        isRecording = YES;
        startTime = [[NSDate date] timeIntervalSince1970];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                      target:self
                                                    selector:@selector(timerHandler:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        
        [self startFlashingbutton];
        
    };
    
    // Hide controls
    [UIView  animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:hideControls completion:recordMovie];
}

- (IBAction)doneBtnClicked:(id)sender
{
    
    [imagePicker stopVideoCapture];
    isRecording = NO;
    [self stopFlashingbutton];
    [self.timer invalidate];
    self.timer = nil;
    [[AppDelegate shareddelegate] updateHUDActivity:@"Saving..." show:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    
    NSURL *videoURL = [info valueForKey:UIImagePickerControllerMediaURL];
    NSString *pathToVideo = [videoURL path];
    BOOL okToSaveVideo = UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(pathToVideo);
    if (okToSaveVideo) {
        UISaveVideoAtPathToSavedPhotosAlbum(pathToVideo, self, @selector(video:didFinishSavingWithError:contextInfo:), NULL);
        [self.recordedVideosArray addObject:videoURL];
        [self saveVideosToDiskForVideo:self.videoDetailDictionary[@"Title"]];
    } else {
        [self video:pathToVideo didFinishSavingWithError:nil contextInfo:NULL];
    }
    
}

- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    void (^showControls)(void);
    showControls = ^(void) {
        if (showCameraSelection) _cameraSelectionButton.alpha = 1.0;
        if (showFlashMode) _flashModeButton.alpha = 1.0;
    };
    
    // Show controls
    [UIView  animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:showControls completion:NULL];
    
}


- (void)saveVideosToDiskForVideo:(NSString *)videoName
{
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
    NSString *documentsDirectory = [sysPaths objectAtIndex:0];
    NSURL *filePath =  [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"RecordedVideos.plist"]];
    
    NSLog(@"File Path: %@", filePath);
    
    NSMutableDictionary *plistDict; // needs to be mutable
    if ([[NSFileManager defaultManager] fileExistsAtPath:[filePath path]]) {
        
        NSData *data = [NSData dataWithContentsOfFile:[filePath path]];
        NSDictionary *jsonObject = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        plistDict = [[NSMutableDictionary alloc] initWithDictionary:jsonObject];
    } else {
        // Doesn't exist, start with an empty dictionary
        plistDict = [[NSMutableDictionary alloc] init];
        
    }
    
    NSLog(@"plist: %@", [plistDict description]);
    
    [plistDict setObject:self.recordedVideosArray forKey:self.videoDetailDictionary[@"Title"]];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:plistDict];
    BOOL didWriteToFile = [data writeToFile:[filePath path] atomically:YES];
    if (didWriteToFile) {
        NSLog(@"Write to file a SUCCESS!");
    } else {
        NSLog(@"Write to file a FAILURE!");
    }
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [imagePicker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelBtnClicked:(id)sender {
    
    imagePicker.delegate = nil;
    isRecording = NO;
    [imagePicker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}



@end
