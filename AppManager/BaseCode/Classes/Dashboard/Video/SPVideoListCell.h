//
//  SPVideoListCell.h
//  Exceeding
//
//  Created by Veeru on 11/11/14.
//
//

#import <UIKit/UIKit.h>
#import "SPVideoListVC.h"
#import "SPUserFavoriteVC.h"

@interface SPVideoListCell : UITableViewCell<UIGestureRecognizerDelegate>
{

}

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (strong, nonatomic) IBOutlet UIImageView *fevImageView;

@property (nonatomic, strong) IBOutlet UIImageView *videoImage;

@property (nonatomic, strong) IBOutlet UIButton *addToFavBtn;
@property (nonatomic, assign) BOOL isFromRemainder;


@property (strong, nonatomic) id <videoListDelegate> videoListViewDelegate;
@property (strong, nonatomic) id <userFevVideoDelegate> userFevVideoListViewDelegate;
@property (nonatomic,assign) BOOL isVideoListVC;

- (void)prepareView:(NSDictionary *)listVideoDic;

@end
