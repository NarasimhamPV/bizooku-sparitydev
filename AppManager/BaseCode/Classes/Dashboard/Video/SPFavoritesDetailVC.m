//
//  SPFavoritesDetailVC.m
//  Exceeding
//
//  Created by Veeru on 24/11/14.
//
//

#import "SPFavoritesDetailVC.h"
#import "AppDelegate.h"
#import "SPFavoritesCollectionCell.h"
#import "RemaindersCustomView.h"
#import "ReminderCustomTVCell.h"
#import "SPSingletonClass.h"
#import "SPRecordingViewController.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
@interface SPFavoritesDetailVC ()

@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UICollectionView *videosCollectionView;

@property (strong, nonatomic) SPFavoritesCollectionCell *videoCollectionViewCell;
@property (strong, nonatomic) NSURL *videoURL;
//@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (assign,nonatomic) BOOL isPageChanged;
@property(nonatomic) int PAGES;

@property (strong, nonatomic) RemaindersCustomView *remaindersCustomView;
@property (strong, nonatomic) ReminderCustomTVCell *remaindersCustomTableCell;
@property (nonatomic, strong) NSMutableArray *recordedVideosArray2;

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic,strong) CustomBannerView *addBannerView;

@end

@implementation SPFavoritesDetailVC

@synthesize detailsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.detailsArray = [[AppDelegate shareddelegate].userFevVideoDict mutableCopy];
    self.headerObject.headerLabel.text = @"";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoritesViewContrll:) name:@"getLocalLcheduleNotification" object:nil];

    
    [self.view bringSubviewToFront:self.pageControl];
    
    self.isPageChanged = YES;
    
    self.navigationController.navigationBarHidden = YES;
    
    UINib *cellNib = [UINib nibWithNibName:@"SPFavoritesCollectionCell" bundle:nil];
    [self.videosCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"CollectionCell"];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.pageControl.numberOfPages = [self.detailsArray count];
    
    
    NSIndexPath *iPath = [NSIndexPath indexPathForItem:[[SPSingletonClass sharedInstance] fevVideoIndexPath] inSection:0];
    [self.videosCollectionView scrollToItemAtIndexPath:iPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    self.isVideoFullScreenExited = NO;
    self.headerObject.btnShare.hidden = YES;
    self.headerObject.iconSeperator.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.videosCollectionView reloadData];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;
    
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];

    
}
#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Video"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
    //    [self.moviePlayer stop];
}
- (void)favoritesViewContrll:(NSNotification *) notification
{
    SPUserFavoriteVC *appUserFevVC = [[SPUserFavoriteVC alloc]initWithNibName:@"SPUserFavoriteVC" bundle:nil];
    appUserFevVC.appType = APP_VIDEO;
    appUserFevVC.boolNoListing = YES;
    [self.navigationController pushViewController:appUserFevVC animated:YES];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.detailsArray count];
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.videosCollectionView.frame.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"CollectionCell";
    
    SPFavoritesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!self.isVideoFullScreenExited) {
        cell.moviePlayer = [self moviePlayerInitialisation1] ;
        [cell videoPlayerInitializationWithIndexPath:indexPath];
    }
    else
    {
        self.isVideoFullScreenExited = NO;
    }
    NSDictionary *tempDict = [self.detailsArray objectAtIndex:indexPath.row];
    NSLog(@"tempDict %@",tempDict);
    if ([[tempDict valueForKey:@"FilePath"] isEqualToString:@""]){
        self.headerObject.btnShare.hidden = YES;
        self.headerObject.iconSeperator.hidden = YES;
    }else{
        self.headerObject.btnShare.hidden = NO;
        self.headerObject.iconSeperator.hidden = NO;
        
    }
    
    [cell setDetailVideoViewDelegate:self];
    self.pageControl.currentPage = indexPath.row;
    
    int pages = floor(self.videosCollectionView.contentSize.width / self.videosCollectionView.frame.size.width);
    [_pageControl setNumberOfPages:pages];
    self.detailDataDictionary = cell.detailDictionary;
    self.recordedVideosArray2 = cell.recordedVideosArray;
    
    
    if (self.isVideoFullScreenExited){
        self.isVideoFullScreenExited = NO;
    }
    
    return cell;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.videosCollectionView.frame.size.width;
    float currentPage = self.videosCollectionView.contentOffset.x / pageWidth;
    
    
    if (0.0f != fmodf(currentPage, 1.0f)) {
        _pageControl.currentPage = currentPage + 1;
        
        self.isPageChanged = YES;
        
    } else {
        _pageControl.currentPage = currentPage;
        [self.videosCollectionView reloadData];
    }
}

- (IBAction)pageControlClicked:(id)sender
{
    
    CGFloat pageWidth = self.videosCollectionView.frame.size.width;
    
    CGPoint scrollTo = CGPointMake(pageWidth * self.pageControl.currentPage, 0);
    [self.videosCollectionView setContentOffset:scrollTo animated:YES];
}



-(MPMoviePlayerController *)moviePlayerInitialisation1

{
    
    //create a player
    static MPMoviePlayerController *_sharedMoviePlayer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _sharedMoviePlayer =  [[MPMoviePlayerController alloc] initWithContentURL:nil];
        _sharedMoviePlayer.controlStyle = MPMovieControlStyleDefault;
        _sharedMoviePlayer.shouldAutoplay = NO;
        [_sharedMoviePlayer setFullscreen:YES animated:YES];
        _sharedMoviePlayer.scalingMode = YES;
        _sharedMoviePlayer.view.frame = CGRectMake(0, 0, 320, 240);
        
        
        
        //    [self.view addSubview:self.moviePlayer.view];
        
    });
    
    return  _sharedMoviePlayer;
}

- (void)gotoRecordingView
{
    
    SPRecordingViewController *recordingVC = [[SPRecordingViewController alloc]initWithNibName:@"SPRecordingViewController" bundle:nil];
    recordingVC.videoDetailDictionary = self.detailDataDictionary;
    recordingVC.recordedVideosArray = self.recordedVideosArray2;
    [self.navigationController pushViewController:recordingVC animated:YES];
    
}

- (void)goPopView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popToViewController:[self.navigationController viewControllers][1] animated:YES];
}


@end
