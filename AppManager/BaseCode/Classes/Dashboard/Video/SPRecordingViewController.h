//
//  SPRecordingViewController.h
//  VideoRecordingView
//
//  Created by Sandeep on 06/11/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <AVFoundation/AVFoundation.h>
#import "SPVideoDetailVC.h"

@interface SPRecordingViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

{

IBOutlet UIView *cameraOverlayView;

UIImagePickerController *imagePicker;
BOOL                     isRecording;
BOOL                     showCameraSelection;
BOOL                     showFlashMode;
}

@property (strong, nonatomic) NSMutableArray *recordedVideosArray;
@property (strong, nonatomic) NSDictionary *videoDetailDictionary;
@property (strong, nonatomic) NSMutableDictionary *recordedVideosDict;

@end
