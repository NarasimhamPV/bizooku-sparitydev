//
//  SPFavoritesDetailVC.h
//  Exceeding
//
//  Created by Veeru on 24/11/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import "BigBanners.h"



@protocol detailVideoViewProtocol <NSObject>

@optional

-(MPMoviePlayerController *)moviePlayerInitialisation1;
- (void)gotoRecordingView;
- (void)goPopView;

@end


@interface SPFavoritesDetailVC : AppMainDetailVC <detailVideoViewProtocol,BigBannersDelegate>

@property (nonatomic,strong) NSMutableArray *detailsArray;
@property (nonatomic, assign) BOOL isVideoFullScreenExited;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
