//
//  SPUserFavoriteVC.m
//  Exceeding
//
//  Created by Veeru on 20/11/14.
//
//

#import "SPUserFavoriteVC.h"
#import "SPVideoListCell.h"
#import "SDZEXVideoServiceExample.h"
#import "AppDelegate.h"
#import "AppMainVC.h"
#import "SPFavoritesDetailVC.h"

#import "RemaindersCustomView.h"
#import "ReminderCustomTVCell.h"
#import "DetailRemainderCustomView.h"
#import "EndRepeatCustomView.h"
#import "SPSingletonClass.h"

#import "CustomBannerView.h"
#import "WebViewController.h"

static NSString *const kVideoListCell = @"UserFevVideoCellIdentfier";
static NSString *const kVideoListCellNib = @"SPVideoListCell";



@interface SPUserFavoriteVC ()
{
    BOOL isSwipeRight;
}

@property (nonatomic,strong) IBOutlet UITableView *userFevVideoTableView;
@property (nonatomic,strong) NSMutableArray *usetFevVideoListArray;

@property (nonatomic,strong) IBOutlet UIButton *toggleBtn;
@property (nonatomic,strong) IBOutlet UIButton *alarmBtn;
@property (nonatomic,strong) UIButton *doneBtn;
@property (nonatomic,strong) UIButton *calendarDoneBtn;
@property (nonatomic,strong) NSIndexPath *swipedIndexPath;
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic,strong) CustomBannerView *addBannerView;



@property (strong, nonatomic) RemaindersCustomView *remaindersCustomView;
@property (strong, nonatomic) ReminderCustomTVCell *remaindersCustomTableCell;

@property (strong, nonatomic) DetailRemainderCustomView *detailRemainderCustomView;
@property (strong, nonatomic) EndRepeatCustomView *endRepeatCustomView;
@property (strong, nonatomic) IBOutlet UILabel *nodataLabel;
- (IBAction)alarmBtnAction:(id)sender;
- (IBAction)toggleBtnAction:(id)sender;

@end



@implementation SPUserFavoriteVC
@synthesize tileInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveUserFevVideoListsNotification:)
                                                 name:@"getUserFevVideoListNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveDeleteFevNotification:)
                                                 name:@"getDeleteFevNotification"
                                               object:nil];
    self.headerObject.iconSeperator.hidden = YES;

    [AppDelegate shareddelegate].selectedTileInfo = tileInfo;

    self.nodataLabel.hidden = YES;
    [self.nodataLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];

    self.headerObject.btnShare.hidden = YES;
    self.headerObject.headerLabel.text = @"Favorites";
    [self.view bringSubviewToFront:self.toggleBtn];
    [self.view bringSubviewToFront:self.alarmBtn];
    
    
    self.doneBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.doneBtn.frame = CGRectMake(257, 0, 60, 44);
    [self.doneBtn setBackgroundImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [self.view addSubview:self.doneBtn];
    [self.view bringSubviewToFront:self.doneBtn];
    
    self.toggleBtn.hidden = NO;
    self.alarmBtn.hidden = NO;
    self.doneBtn.hidden = YES;
    
    
    [self.userFevVideoTableView registerNib:[UINib nibWithNibName:kVideoListCellNib bundle:nil] forCellReuseIdentifier:kVideoListCell];
    self.userFevVideoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.usetFevVideoListArray =[[NSMutableArray alloc] initWithCapacity:0];
    
    
    [self videoFevService];
    
    
    

    
    self.swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    [self.swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.userFevVideoTableView addGestureRecognizer:self.swipeLeft];
    
    self.swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [self.swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.userFevVideoTableView addGestureRecognizer:self.swipeRight];
    /*
    if ([NSThread isMainThread]) {
        //[self loadData];
        //[self performSelector:@selector(loadData) withObject:nil afterDelay:0.1];

    }
    else {
        dispatch_sync(dispatch_get_main_queue(), ^{
            //[self loadData];

            //[self performSelector:@selector(loadData) withObject:nil afterDelay:0.1];

        });
    }
     */
    
   

    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    self.swipedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:1];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;
    
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];

    [super viewWillAppear:YES];
    
}
- (void)loadData
{
    self.remaindersCustomView = [[[NSBundle mainBundle] loadNibNamed:@"RemaindersCustomView" owner:self options:nil] objectAtIndex:0];
    [self.remaindersCustomView setFrame:CGRectMake(0, 44, 320, 524)];
    [self.view addSubview:self.remaindersCustomView];
    [self.remaindersCustomView setHidden:YES];
    
    self.detailRemainderCustomView = [[[NSBundle mainBundle] loadNibNamed:@"DetailRemainderCustomView" owner:self options:nil] objectAtIndex:0];
    [self.detailRemainderCustomView setFrame:CGRectMake(0, 44, 320, 524)];
    [self.view addSubview:self.detailRemainderCustomView];
    [self.detailRemainderCustomView setHidden:YES];
    
    
    self.endRepeatCustomView = [[[NSBundle mainBundle] loadNibNamed:@"EndRepeatCustomView" owner:self options:nil] objectAtIndex:0];
    [self.endRepeatCustomView setFrame:CGRectMake(0, 44, 320, 524)];
    [self.view addSubview:self.endRepeatCustomView];
    [self.endRepeatCustomView setHidden:YES];
    
}

- (void)videoFevService
{
    SDZEXVideoServiceExample *videoService = [[[SDZEXVideoServiceExample alloc] init] autorelease];
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
    [videoService runUserFevorates];
}

- (void)receiveUserFevVideoListsNotification:(NSNotification *)notification
{
//    NSLog(@"notification %@",notification);
    
    [self loadData];
    self.usetFevVideoListArray = nil;
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    self.usetFevVideoListArray = [[AppDelegate shareddelegate].userFevVideoDict mutableCopy];
    [self reloadTable];
    
    
    if ([self.usetFevVideoListArray count] == 0){
        self.nodataLabel.hidden = NO;
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKeyPath:@"isVideoFevEnable"];
    }else{
        self.nodataLabel.hidden = YES;

    }
    
}
- (void)receiveDeleteFevNotification:(NSNotification *)notification
{
    [self videoFevService];
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Video"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}

-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)reloadTable
{
    [self.userFevVideoTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.usetFevVideoListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SPVideoListCell *videoListCell = [tableView dequeueReusableCellWithIdentifier:kVideoListCell];
    videoListCell.selectionStyle = UITableViewCellSelectionStyleNone;
    videoListCell.isVideoListVC = NO;
    
    videoListCell.addToFavBtn.tag = indexPath.row;
    
    
    //NSLog(@"cellForRowAtIndexPath = %ld",(long)indexPath.row);
    //NSLog(@"selected = %ld",(long)self.swipedIndexPath.row);
    
    if(self.swipedIndexPath.row == indexPath.row)
    {
        NSLog(@"Matched");
        
         if (isSwipeRight != YES) {
            [UIView animateWithDuration:0.5f
                                  delay:0.1f
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 [videoListCell.addToFavBtn setFrame:CGRectMake(246, videoListCell.addToFavBtn.frame.origin.y, videoListCell.addToFavBtn.frame.size.width, videoListCell.addToFavBtn.frame.size.height)];
                                 
                             }
                             completion:^(BOOL finished){
                                 
                             }
             ];

         }else{
            [UIView animateWithDuration:0.5f
                                  delay:0.1f
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 [videoListCell.addToFavBtn setFrame:CGRectMake(320, videoListCell.addToFavBtn.frame.origin.y, videoListCell.addToFavBtn.frame.size.width, videoListCell.addToFavBtn.frame.size.height)];
                                 
                                 
                             }
                             completion:^(BOOL finished){
                                 
                             }
             ];

         }
        
        
    }
    else{
        [UIView animateWithDuration:0.5f
                              delay:0.1f
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             [videoListCell.addToFavBtn setFrame:CGRectMake(320, videoListCell.addToFavBtn.frame.origin.y, videoListCell.addToFavBtn.frame.size.width, videoListCell.addToFavBtn.frame.size.height)];
                             
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }
    
    
    
    NSDictionary *tempDict = [self.usetFevVideoListArray objectAtIndex:indexPath.row];
    [videoListCell prepareView:tempDict];
    videoListCell.userFevVideoListViewDelegate = self;
    videoListCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return videoListCell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AppMainVC *mainVC = [[AppMainVC alloc]init];
    mainVC.appDetailVC = [[SPFavoritesDetailVC alloc]initWithNibName:@"SPFavoritesDetailVC" bundle:nil] ;
    mainVC.appDetailVC.appType = self.appType;
    mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
    [[SPSingletonClass sharedInstance] setFevVideoIndexPath:indexPath.row];
    
    [self.navigationController pushViewController:mainVC.appDetailVC animated:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SPVideoListCell *cell = (SPVideoListCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    if (self.userFevVideoTableView.editing == YES) {
        
        cell.addToFavBtn.hidden = YES;
    }else{
        cell.addToFavBtn.hidden = NO;
        
    }
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}


- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (BOOL)tableView: (UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndex toIndexPath:(NSIndexPath *)destinationIndexPath
{
    
    NSInteger sourceRow = fromIndex.row;
    NSInteger destRow = destinationIndexPath.row;
    id object = [self.usetFevVideoListArray objectAtIndex:sourceRow];
    
    [self.usetFevVideoListArray removeObjectAtIndex:sourceRow];
    [self.usetFevVideoListArray insertObject:object atIndex:destRow];
    NSString *orderStr = [[self.usetFevVideoListArray valueForKey:@"VideoId"] componentsJoinedByString:@","];
    SDZEXVideoServiceExample *videoService = [[[SDZEXVideoServiceExample alloc] init] autorelease];
    [videoService runUserFevoratesOrderChanged:orderStr];
    
    return YES;
    
}



- (void)deleteFavoriteVideo:(id)sender
{
    
    self.swipedIndexPath = [NSIndexPath indexPathForRow:-1 inSection:1];
    
    
    NSDictionary *tempDict = [self.usetFevVideoListArray objectAtIndex:[sender tag]];
    SDZEXVideoServiceExample *videoService = [[[SDZEXVideoServiceExample alloc] init] autorelease];
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
    
    [videoService runDeleteFevorite:[tempDict valueForKey:@"VideoId"]];
    
}
- (IBAction)toggleBtnAction:(id)sender
{
    
    if (![sender isSelected]) {
        
        [sender setSelected:YES];
        self.toggleBtn.selected = YES;
        [self.userFevVideoTableView setEditing: YES animated: YES];
        
    } else {
        [sender setSelected:NO];
        [self.userFevVideoTableView setEditing: NO animated: NO];
    }
    
    
    
}
- (IBAction)alarmBtnAction:(id)sender
{
    [self.headerObject.headerLeftBtn setUserInteractionEnabled:NO];
    [self.headerObject.headerLeftBtn setEnabled:NO];

    [self.remaindersCustomView setHidden:NO];
    
    self.doneBtn.hidden = NO;
    self.headerObject.iconSeperator.hidden = YES;

    self.headerObject.headerLabel.text = @"Reminders";
    self.toggleBtn.hidden = YES;
    self.alarmBtn.hidden = YES;
    
    self.doneBtn.tag = 1;
    [self.doneBtn removeTarget:self.detailRemainderCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneBtn removeTarget:self action:@selector(remiderViewDone:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.doneBtn addTarget:self action:@selector(remiderViewDone:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.remaindersCustomView prepareView];
    [self.remaindersCustomView setRemaindersViewDelegate:self];
    
    
}

- (void)remiderViewDone:(id)sender
{
    if ([sender tag] == 1){
        self.doneBtn.hidden = YES;
        self.headerObject.headerLabel.text = @"Favorites";
        self.headerObject.iconSeperator.hidden = NO;
        self.toggleBtn.hidden = NO;
        self.alarmBtn.hidden = NO;
        [self.headerObject.headerLeftBtn setUserInteractionEnabled:YES];
        [self.headerObject.headerLeftBtn setEnabled:YES];
        [self.remaindersCustomView setHidden:YES];
    }else{
        [self.detailRemainderCustomView setHidden:YES];
        [self alarmBtnAction:self.alarmBtn];
    }
    
}

- (void)doneBtnPressedInDetailRemainderCustomViewWithSender:(UIButton *)sender
{
    
    [self.detailRemainderCustomView setHidden:YES];
    [self alarmBtnAction:self.alarmBtn];
}

#pragma mark -----
#pragma mark CustomCells Delegates

-(void)gotoDetailRemainder:(UIButton *)sender
{
    self.headerObject.headerLabel.text = @"Add Reminders";
    
    
    [self.remaindersCustomView setHidden:YES];
    [self.endRepeatCustomView setHidden:YES];
    
    [self.detailRemainderCustomView setHidden:NO];
    
    [self.detailRemainderCustomView prepareView];
    self.doneBtn.hidden = NO;
    
    [self.doneBtn removeTarget:self action:@selector(remiderViewDone:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneBtn removeTarget:self.endRepeatCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.doneBtn.tag = 2;
    [self.doneBtn addTarget:self.detailRemainderCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.endRepeatCustomView setEndRepeatCustomViewDelegate:self];
    [self.detailRemainderCustomView setEndRepeatCustomViewDelegate:self];
}

- (void)doneBtnPressedInEndRepeatCustomViewWithSender:(UIButton *)sender
{
    
    [self.remaindersCustomView setHidden:YES];
    [self.endRepeatCustomView setHidden:YES];
    
    [self.detailRemainderCustomView setHidden:NO];
    
    [self.detailRemainderCustomView prepareView];
    self.doneBtn.hidden = NO;
    
    [self.doneBtn removeTarget:self action:@selector(remiderViewDone:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneBtn removeTarget:self.endRepeatCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.doneBtn.tag = 2;
    [self.doneBtn addTarget:self.detailRemainderCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.endRepeatCustomView setEndRepeatCustomViewDelegate:self];
    [self.detailRemainderCustomView setEndRepeatCustomViewDelegate:self];
}

- (void)calenderViewDone:(id)sender
{
    [self.detailRemainderCustomView setHidden:YES];
    
}


- (void)gotoDetailRemainderWithInfo:(UIButton *)sender withDict:(NSMutableDictionary *)selectedDict
{
    self.headerObject.headerLabel.text = @"Add Reminders";
    
    [self.remaindersCustomView setHidden:YES];
    [self.endRepeatCustomView setHidden:YES];
    
    [self.detailRemainderCustomView setHidden:NO];
    
    [self.detailRemainderCustomView prepareView:selectedDict];
    self.doneBtn.hidden = NO;
    
    [self.doneBtn removeTarget:self action:@selector(remiderViewDone:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneBtn removeTarget:self.endRepeatCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.doneBtn.tag = 2;
    [self.doneBtn addTarget:self.detailRemainderCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.endRepeatCustomView setEndRepeatCustomViewDelegate:self];
    [self.detailRemainderCustomView setEndRepeatCustomViewDelegate:self];
    
    //self.detailRemainderCustomView = [[[NSBundle mainBundle] loadNibNamed:@"DetailRemainderCustomView" owner:self options:nil] objectAtIndex:0];
    //[self.detailRemainderCustomView prepareView];
    
    //[self.detailRemainderCustomView setEndRepeatCustomViewDelegate:self];
    //[self.view addSubview:self.detailRemainderCustomView];
    
}
-(void)gotoEndRepeatCustomView:(UIButton *)sender
{
    
    self.doneBtn.hidden = NO;
    [self.detailRemainderCustomView setHidden:YES];
    
    [self.endRepeatCustomView setHidden:NO];
    
    
    [self.endRepeatCustomView prepareView];
    
    [self.endRepeatCustomView setEndRepeatCustomViewDelegate:self];
    
    [self.doneBtn removeTarget:self action:@selector(remiderViewDone:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneBtn removeTarget:self.detailRemainderCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.doneBtn setTag:3];
    [self.doneBtn addTarget:self.endRepeatCustomView action:@selector(doneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)handleSwipeLeft:(UISwipeGestureRecognizer *)swipe
{
    
    if (swipe.state == UIGestureRecognizerStateEnded) {
        CGPoint swipeLocation = [swipe locationInView:self.userFevVideoTableView];
        NSIndexPath *swipedIndexPath1 = [self.userFevVideoTableView indexPathForRowAtPoint:swipeLocation];
        self.swipedIndexPath = swipedIndexPath1;
        isSwipeRight= NO;
//        NSLog(@"index = %ld",(long)self.swipedIndexPath.row);
        [self.userFevVideoTableView reloadData];
        
    }
    
}

- (void)handleSwipeRight:(UISwipeGestureRecognizer *)swipe
{
    
    if (swipe.state == UIGestureRecognizerStateEnded) {
        CGPoint swipeLocation = [swipe locationInView:self.userFevVideoTableView];
        NSIndexPath *swipedIndexPath1 = [self.userFevVideoTableView indexPathForRowAtPoint:swipeLocation];
        self.swipedIndexPath = swipedIndexPath1;
        
        isSwipeRight= YES;
        
        //        NSLog(@"index = %ld",(long)self.swipedIndexPath.row);
        [self.userFevVideoTableView reloadData];
        
    }
    
}


@end
