//
//  SPUserFavoriteVC.h
//  Exceeding
//
//  Created by Veeru on 20/11/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "DBTilesInfo.h"
#import "BigBanners.h"

@protocol userFevVideoDelegate <NSObject>

@required

-(void)deleteFavoriteVideo:(id)sender;
- (void)doneBtnPressedInEndRepeatCustomViewWithSender:(UIButton *)sender;
- (void)doneBtnPressedInDetailRemainderCustomViewWithSender:(UIButton *)sender;
-(void)gotoDetailRemainder:(UIButton *)sender;
-(void)gotoEndRepeatCustomView:(UIButton *)sender;
//-(void)gotoDetailRemainderWithInfo:(UIButton *)sender;
-(void)gotoDetailRemainderWithInfo:(UIButton *)sender withDict:(NSMutableDictionary *)selectedDict;

@end

@interface SPUserFavoriteVC : AppMainDetailVC<userFevVideoDelegate,BigBannersDelegate>
{

}
@property (nonatomic, strong) UISwipeGestureRecognizer *swipeLeft;
@property (nonatomic, strong) UISwipeGestureRecognizer *swipeRight;
@property (strong, nonatomic) DBTilesInfo *tileInfo;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
