//
//  SPVideoListVC.h
//  Exceeding
//
//  Created by Veeru on 10/11/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "BigBanners.h"

@protocol videoListDelegate <NSObject>

@required

-(void)addFavoriteVideo:(id)sender;

@end


@interface SPVideoListVC : AppMainDetailVC<videoListDelegate,BigBannersDelegate>

@property (nonatomic, strong) UISwipeGestureRecognizer *swipeLeft, *swipeRight;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
