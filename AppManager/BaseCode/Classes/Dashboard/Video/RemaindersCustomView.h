//
//  RemaindersCustomView.h
//  VideoWidget
//
//  Created by Sandeep on 21/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserFavoriteVC.h"



@protocol remainderCustomViewProtocol <NSObject>
@required
- (void)removeCellWithSender:(UIButton *)sender;
//- (void)goToInfoWithSender:(UIButton *)sender;

@end
@interface RemaindersCustomView : UIView<remainderCustomViewProtocol>
{
    NSInteger deleteIndex;
}

@property (strong, nonatomic) IBOutlet UITableView *remainderTableView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

@property (strong, nonatomic) id <userFevVideoDelegate> remaindersViewDelegate;
@property (strong, nonatomic) NSMutableArray *remainderArray;
-(void)prepareView;

@end
