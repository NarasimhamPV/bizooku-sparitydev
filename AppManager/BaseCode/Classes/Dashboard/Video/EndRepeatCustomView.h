//
//  EndRepeatCustomView.h
//  VideoWidget
//
//  Created by Sandeep on 24/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserFavoriteVC.h"


@interface EndRepeatCustomView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UIDatePicker *endRepeatPicker;
@property (strong, nonatomic) NSString *dateString;
@property (strong,nonatomic) IBOutlet UIButton *checkBoxBtn;
@property (strong, nonatomic) id <userFevVideoDelegate> endRepeatCustomViewDelegate;
@property (strong,nonatomic) IBOutlet UILabel *repeatLabel;
@property (strong, nonatomic) IBOutlet UIView *pickerDisableView;

-(void)prepareView;
- (void)doneButtonClicked:(UIButton *)sender;

@end
