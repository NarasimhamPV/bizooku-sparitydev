//
//  SPVideoListVC.m
//  Exceeding
//
//  Created by Veeru on 10/11/14.
//
//

#import "SPVideoListVC.h"
#import "SDZEXVideoServiceExample.h"
#import "AppDelegate.h"
#import "SPVideoListCell.h"
#import "AppMainVC.h"
#import "SPVideoDetailVC.h"
#import "SPSingletonClass.h"
#import "CustomBannerView.h"
#import "WebViewController.h"

static NSString *const kVideoListCell = @"VideoListCellIdentfier";
static NSString *const kVideoListCellNib = @"SPVideoListCell";

@interface SPVideoListVC ()
{
    BOOL isRightSwipe;
}

@property (nonatomic,strong) IBOutlet UITableView *videoTableView;
@property (nonatomic,strong) NSMutableArray *videoListArray;
@property (nonatomic,strong) IBOutlet UIButton *fevBtn;
@property (nonatomic, strong) NSIndexPath *swipedIndex;
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic,strong) CustomBannerView *addBannerView;

- (IBAction)favoritesViewContr:(id)sender;

@end

@implementation SPVideoListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoritesViewContrll:) name:@"getLocalLcheduleNotification" object:nil];

    
    self.headerObject.btnShare.hidden = YES;
    self.headerObject.iconSeperator.hidden = YES;
    self.headerObject.headerLabel.text = [[SPSingletonClass sharedInstance] appCategoryName];

    self.fevBtn.hidden = YES;
    [self.view bringSubviewToFront:self.fevBtn];
    
    
    [self.videoTableView registerNib:[UINib nibWithNibName:kVideoListCellNib bundle:nil] forCellReuseIdentifier:kVideoListCell];
    self.videoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    AppDelegate *del = [AppDelegate shareddelegate];
    int localRecodingEnable =  del.appReference.islocalRecordingEnabled.intValue;
    if (localRecodingEnable == 1){
        self.swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
        [self.swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self.videoTableView addGestureRecognizer:self.swipeLeft];
        
             self.swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
             [self.swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
             [self.videoTableView addGestureRecognizer:self.swipeRight];
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    self.swipedIndex = [NSIndexPath indexPathForRow:-1 inSection:1];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveVideoListsNotification:)
                                                 name:@"getVideoListNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveAddFevNotification:)
                                                 name:@"getAddFevNotification"
                                               object:nil];
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isVideoFevEnable"] isEqualToString:@"YES"]){
        self.fevBtn.hidden = NO;
        self.headerObject.iconSeperator.hidden = NO;
    }else{
        self.fevBtn.hidden = YES;
        self.headerObject.iconSeperator.hidden = YES;
    }
    
    
    [self videoListService];
    
    
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;
    
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];

    
    [super viewWillAppear:YES];
    
}
- (void)videoListService
{
    
    SDZEXVideoServiceExample *videoService = [[[SDZEXVideoServiceExample alloc] init] autorelease];
    NSString *brandTileId = [[NSUserDefaults standardUserDefaults]objectForKey:@"BrandTileId"];
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
    
    [videoService runVideoList:[brandTileId integerValue]];
}

- (void)receiveVideoListsNotification:(NSNotification *) notification
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    self.videoListArray = [[AppDelegate shareddelegate].videoListDict mutableCopy];

    
    BOOL isFevEnable;
    isFevEnable = NO;
    
    for (NSDictionary *dict in self.videoListArray) {
        NSNumber *isVideoEnable = [dict valueForKey:@"IsFavourite"];
        int isFevVideo = isVideoEnable.intValue;
        if (isFevVideo == 1){
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKeyPath:@"isVideoFevEnable"];
            isFevEnable = YES;
            break;
        }
        
    }
    if (isFevEnable== YES){
        AppDelegate *del = [AppDelegate shareddelegate];
        int localRecodingEnable =  del.appReference.islocalRecordingEnabled.intValue;
        if (localRecodingEnable == 1){
            self.fevBtn.hidden = NO;
            self.headerObject.iconSeperator.hidden = NO;
        }else{
            self.fevBtn.hidden = YES;
            self.headerObject.iconSeperator.hidden = YES;
        }
        
        
    }else{

        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isVideoFevEnable"] isEqualToString:@"YES"]){
            self.fevBtn.hidden = NO;
            self.headerObject.iconSeperator.hidden = NO;
        }else{
            self.fevBtn.hidden = YES;
            self.headerObject.iconSeperator.hidden = YES;
        }
        
        
    }
    
    
    if ([self.videoListArray count] == 0){
        
    }else{
        [self reloadTable];
    }
    
}
- (void)favoritesViewContrll:(NSNotification *) notification
{
    SPUserFavoriteVC *appUserFevVC = [[SPUserFavoriteVC alloc]initWithNibName:@"SPUserFavoriteVC" bundle:nil];
    appUserFevVC.appType = APP_VIDEO;
    appUserFevVC.boolNoListing = NO;
    [self.navigationController pushViewController:appUserFevVC animated:YES];
}

- (void)receiveAddFevNotification:(NSNotification *) notification
{
    [self videoListService];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}



- (IBAction)favoritesViewContr:(id)sender
{
    SPUserFavoriteVC *appUserFevVC = [[SPUserFavoriteVC alloc]initWithNibName:@"SPUserFavoriteVC" bundle:nil];
    appUserFevVC.appType = APP_VIDEO;
    appUserFevVC.boolNoListing = NO;
    [self.navigationController pushViewController:appUserFevVC animated:YES];
}


- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Video"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
//    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)reloadTable
{
    [self.videoTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.videoListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SPVideoListCell *videoListCell = [tableView dequeueReusableCellWithIdentifier:kVideoListCell];
    videoListCell.selectionStyle = UITableViewCellSelectionStyleNone;
    videoListCell.addToFavBtn.tag = indexPath.row;
    
    videoListCell.isVideoListVC = YES;
    //    NSLog(@"cellForRowAtIndexPath = %ld",(long)indexPath.row);
    
    //    NSLog(@"selected = %ld",(long)self.swipedIndex.row);
    
    if(self.swipedIndex.row == indexPath.row)
    {
        NSLog(@"Matched");
        
        if (isRightSwipe != YES )
        {
            
        
        [UIView animateWithDuration:0.5f
                              delay:0.1f
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             [videoListCell.addToFavBtn setFrame:CGRectMake(246, videoListCell.addToFavBtn.frame.origin.y, videoListCell.addToFavBtn.frame.size.width, videoListCell.addToFavBtn.frame.size.height)];
                            
                            
                         }
                         completion:^(BOOL finished){

                         }
         ];
        }else{
            
            [UIView animateWithDuration:0.5f
                                  delay:0.1f
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 [videoListCell.addToFavBtn setFrame:CGRectMake(320, videoListCell.addToFavBtn.frame.origin.y, videoListCell.addToFavBtn.frame.size.width, videoListCell.addToFavBtn.frame.size.height)];
                                 
                                 
                             }
                             completion:^(BOOL finished){
                                 
                             }
             ];

            
          }
        
    }
    else{
        [UIView animateWithDuration:0.5f
                              delay:0.1f
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             [videoListCell.addToFavBtn setFrame:CGRectMake(320, videoListCell.addToFavBtn.frame.origin.y, videoListCell.addToFavBtn.frame.size.width, videoListCell.addToFavBtn.frame.size.height)];
                             
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }
    
    [videoListCell bringSubviewToFront:videoListCell.addToFavBtn];
    
    
    NSDictionary *tempDict = [self.videoListArray objectAtIndex:indexPath.row];
    [videoListCell prepareView:tempDict];
    videoListCell.videoListViewDelegate = self;
    
    return videoListCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *tempDict = [self.videoListArray objectAtIndex:indexPath.row];
    AppMainVC *mainVC = [[AppMainVC alloc]init];
    mainVC.appDetailVC = [[SPVideoDetailVC alloc]initWithNibName:@"SPVideoDetailVC" bundle:nil] ;
    mainVC.appDetailVC.appType = self.appType;
    mainVC.appDetailVC.tileInfo = mainVC.widgetTileInfo;
    mainVC.appDetailVC.detailDataDictionary = tempDict;
    [self.navigationController pushViewController:mainVC.appDetailVC animated:YES];
    
    
}

- (void)addFavoriteVideo:(id)sender
{
    //    swipedIndex.row
    //self.swipedIndex = [NSIndexPath indexPathForRow:0 inSection:1];
    
    NSDictionary *tempDict = [self.videoListArray objectAtIndex:[sender tag]];
    SDZEXVideoServiceExample *videoService = [[[SDZEXVideoServiceExample alloc] init] autorelease];
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
    //    NSLog(@"swipedIndex %ld",(long)self.swipedIndex.row);
    [videoService runAddFavorite:[tempDict valueForKey:@"VideoId"]];
    
}


- (void)handleSwipeLeft:(UISwipeGestureRecognizer *)swipe
{
    
    if (swipe.state == UIGestureRecognizerStateEnded) {
        CGPoint swipeLocation = [swipe locationInView:self.videoTableView];
        NSIndexPath *swipedIndexPath = [self.videoTableView indexPathForRowAtPoint:swipeLocation];
        self.swipedIndex = swipedIndexPath;
        isRightSwipe = NO;
        [self.videoTableView reloadData];
        
    }
    
    
}
- (void)handleSwipeRight:(UISwipeGestureRecognizer *)swipe
{
    
    
    if (swipe.state == UIGestureRecognizerStateEnded) {
        CGPoint swipeLocation = [swipe locationInView:self.videoTableView];
        NSIndexPath *swipedIndexPath = [self.videoTableView indexPathForRowAtPoint:swipeLocation];
        self.swipedIndex = swipedIndexPath;
        isRightSwipe = YES;
        [self.videoTableView reloadData];
        
        
    }
    
    
}



@end
