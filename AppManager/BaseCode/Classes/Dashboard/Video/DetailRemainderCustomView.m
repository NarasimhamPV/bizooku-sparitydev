//
//  DetailRemainderCustomView.m
//  VideoWidget
//
//  Created by Sandeep on 21/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import "DetailRemainderCustomView.h"
#import "SPSingletonClass.h"

@interface DetailRemainderCustomView ()<UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UIDatePicker *remainderDatePicker;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UITextField *titleTF;
@property (strong, nonatomic) NSString *dateString;
@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSMutableDictionary *remainderDict;
@property (strong, nonatomic) IBOutlet UIButton *endRepeatBtnRef;
@property (strong, nonatomic) NSMutableDictionary *localDict;
@property (strong, nonatomic) NSMutableDictionary *selectedObject;

@property (strong, nonatomic) IBOutlet UILabel *repeatLbl;

@property (strong, nonatomic) IBOutlet UILabel *endRepeatLbl;

@property (assign) BOOL isEditRemider;


@end


@implementation DetailRemainderCustomView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self reloadInputViews];
        
    }
    return self;
}

- (void)prepareView
{
    [self getFonts];

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isEditReminder"] isEqualToString:@"YES"]){
        self.isEditRemider = YES;

    }else{
        self.isEditRemider = NO;
 
    }
    

    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    NSDate *currentDate = [NSDate date];
    [self.remainderDatePicker setDate:currentDate];
    _remainderDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
    _remainderDatePicker.minimumDate = currentDate;
    [self.remainderDatePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self bringSubviewToFront:self.remainderDatePicker];
    
    self.remainderDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    if ([[SPSingletonClass sharedInstance] repeatDateStr].length == 0) {
        [self.endRepeatBtnRef setTitle:@"Never >" forState:UIControlStateNormal];
    }else{
        [self.endRepeatBtnRef setTitle:[[SPSingletonClass sharedInstance] repeatDateStr] forState:UIControlStateNormal];
    }
    
//    [self reloadInputViews];

}

- (void)prepareView:(NSMutableDictionary *)editDict
{
    
    [self getFonts];
    

    self.isEditRemider = YES;
    self.selectedObject = editDict;
    self.dateLabel.textColor = [UIColor colorWithRed:2.0/255.0 green:131.0/255.0 blue:232.0/255.0 alpha:1.0];
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    
    self.titleTF.text = [editDict valueForKey:@"Title"];
    self.dateLabel.text = [editDict valueForKey:@"Date"];
    NSString *neverStr = [editDict valueForKey:@"Repeat"];
    if (neverStr.length == 0) {
        [self.endRepeatBtnRef setTitle:@"Never >" forState:UIControlStateNormal];
    }else{
        [self.endRepeatBtnRef setTitle:[editDict valueForKey:@"Repeat"] forState:UIControlStateNormal];
    }
    
    NSDate *currentDate = [NSDate date];
    [self.remainderDatePicker setDate:currentDate];
    [self.remainderDatePicker setMinimumDate:currentDate];
    
    _remainderDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    [self.remainderDatePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self bringSubviewToFront:self.remainderDatePicker];
//    [self reloadInputViews];
    
    

}
- (void)getFonts
{
    self.localDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"",@"Title",@"",@"Date",@"",@"Repeat",nil];

    [self.repeatLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    [self.endRepeatLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    [self.dateLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    [self.titleTF setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (component == 0){
        return 5;
    } else if (component == 1){
        return 3;
    }
    return 0;
}

-(void)datePickerChanged:(id *)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:self.remainderDatePicker.date forKey:@"SelectedDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDate *eventDate = self.remainderDatePicker.date;
    self.dateString = [self.currentDateFormatter stringFromDate:eventDate];
    self.dateLabel.text = self.dateString;
    
    [self.remainderDict setObject:self.dateString forKey:@"remainderDate"];

}

- (NSDateFormatter *)currentDateFormatter
{
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE, MM/dd/yyyy, h:mm a"];
        [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    });
    return dateFormatter;
}
- (IBAction)tapGestureRecognised:(id)sender
{
    [self.titleTF resignFirstResponder];
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    self.titleString = self.titleTF.text;
   [self.remainderDict setObject:self.titleTF.text forKey:@"title"];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.titleTF) {
        
        [self.titleTF becomeFirstResponder];
        
    }
    [textField resignFirstResponder];
	return YES;
}
- (void)doneButtonClicked:(UIButton *)sender;
{
    
    [self.titleTF resignFirstResponder];
    if (self.titleTF.text.length == 0 && self.dateLabel.text.length == 0){
        [self.endRepeatCustomViewDelegate doneBtnPressedInDetailRemainderCustomViewWithSender:sender];
        return;
    }
    if(self.titleTF.text.length == 0){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Notice!" message:@"Please enter a title." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }else if (self.dateLabel.text.length == 0){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Notice!" message:@"Please select a Date/Time in the future." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    
    else{
        
        //File Save in Documents
        NSFileManager *manger=[NSFileManager defaultManager];
        NSMutableArray *retrive = [[NSMutableArray alloc] init];
        NSString *path = [NSString stringWithFormat:@"%@/Documents/Remender.plist",NSHomeDirectory()];
        
        [self.localDict setObject:self.titleTF.text forKey:@"Title"];
        [self.localDict setObject:self.dateLabel.text forKey:@"Date"];
        [self.localDict setObject:[[SPSingletonClass sharedInstance] repeatDateStr] forKey:@"Repeat"];
        
        //If file Exists Get the contents from plist
        if([manger fileExistsAtPath:path])
        {
            //     NSLog(@"fileExistsAtPath");
            NSArray* retriveFileContents = [[NSArray alloc] initWithContentsOfFile:path];
            [retrive setArray:retriveFileContents];
            
        }
//        NSLog(@"self.selectedObject %@ self.localDict %@",self.selectedObject,self.localDict);
        if (self.isEditRemider) {
            [retrive replaceObjectAtIndex:[retrive indexOfObject:self.selectedObject] withObject:self.localDict];
           
            for (UILocalNotification *localNotification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
                if ([[localNotification.userInfo valueForKey:@"Date"] isEqualToString:[self.selectedObject valueForKey:@"Date"]]) {
                    [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ; // delete the notification from the system
                    break;
                }
            }
            
            
        }
        else
        {
            [retrive insertObject:self.localDict atIndex:0];

        }
        
        //Updating the Plist file
        [retrive writeToFile:path atomically: YES];
        
        //Local Notifications Set
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *currentDayComponents = [gregorian components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | kCFCalendarUnitWeekday fromDate:[NSDate date]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd h:mm a"];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"hh:mm a"];
        
        NSString *date = [[NSString alloc] initWithFormat:@"%ld-%ld-%ld %@",(long)[currentDayComponents year],(long)[currentDayComponents month],(long)[currentDayComponents day],[timeFormatter stringFromDate:[[NSUserDefaults standardUserDefaults] valueForKey:@"SelectedDate"]]];
        [timeFormatter release];
        NSLog(@"date %@",date);
        
        NSDate *alarmDate = [dateFormatter dateFromString:date];
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        if (localNotification == nil)
            return;
        localNotification.fireDate = alarmDate;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        
        localNotification.repeatInterval = kCFCalendarUnitWeekday;
        localNotification.alertBody = self.titleTF.text;
        localNotification.alertAction = @"Snooze";
        localNotification.userInfo = self.localDict;
        NSLog(@"localNotification %@",localNotification);

        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        self.titleTF.text = @"";
        self.dateLabel.text = @"";
        [self.endRepeatBtnRef setTitle:@"" forState:UIControlStateNormal];
        [self.endRepeatCustomViewDelegate doneBtnPressedInDetailRemainderCustomViewWithSender:sender];


    }
    
    
    
}


- (IBAction)endRepeatBtnClicked:(id)sender
{
    [self.titleTF resignFirstResponder];
    [self.endRepeatCustomViewDelegate gotoEndRepeatCustomView:sender];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    switch (textField.tag) {
            
        case 1: //Username Text field
        {
            
            //return YES;
            return ((newLength <= 30) ? YES : NO);
            
        }
            break;
            
       
            
            
        default:
            
            return YES;
            
            break;
    }
    
    
}


@end
