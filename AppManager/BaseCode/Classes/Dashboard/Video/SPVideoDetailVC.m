//
//  SPVideoDetailVC.m
//  Exceeding
//
//  Created by Veeru on 11/11/14.
//
//

#import "SPVideoDetailVC.h"
#import "SPRecordingViewController.h"
#import "AppMainVC.h"
#import "UILabel+Exceedings.h"
#import "AppDelegate.h"
#import "SDZEXVideoServiceExample.h"
#import "SPSingletonClass.h"
#import "SPUserFavoriteVC.h"
#import "CustomBannerView.h"
#import "WebViewController.h"


#define ButtonWidth 40

@interface SPVideoDetailVC ()
{
    BOOL isFromRecording;
    int deleteIndex;
    BOOL isFirstTimeLoadData;
    
}

@property (nonatomic, strong) IBOutlet UIImageView *videoImageView;
@property (nonatomic, strong) IBOutlet UILabel *videoTileLbl;
@property (nonatomic, strong) IBOutlet UIButton *playBtn;
@property (nonatomic, strong) IBOutlet UIButton *recordVideoBtn;
@property (nonatomic, strong) IBOutlet UITextView *desTF;
@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, strong) IBOutlet UIScrollView *videoDetailScrollView;
@property (nonatomic, strong) UIButton *recSelectedBtn;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) UIView * videoView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (nonatomic,strong) AppDelegate* appDelegate;
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic,strong) CustomBannerView *addBannerView;

@end

@implementation SPVideoDetailVC
@synthesize isSingleVideo,videoID;
@synthesize tileInfo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [AppDelegate shareddelegate].selectedTileInfo = tileInfo;
    self.appDelegate.isOrientation = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveVideoDetailNotification:) name:@"getVideoDetailNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoritesViewContrll:) name:@"getLocalLcheduleNotification" object:nil];
    
    
    self.headerObject.headerLabel.text = [[SPSingletonClass sharedInstance] appCategoryName];
    
    _moviePlayer =  [[MPMoviePlayerController alloc] initWithContentURL:nil];
    [self.view addSubview:self.moviePlayer.view];
    
    _moviePlayer.controlStyle = MPMovieControlStyleDefault;
    _moviePlayer.shouldAutoplay = NO;
    [_moviePlayer setFullscreen:YES animated:YES];
    
    _moviePlayer.scalingMode = YES;
    _moviePlayer.view.frame = CGRectMake(0, 44, 320, 240);
    
    [self.moviePlayer setAllowsAirPlay:YES];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.frame = CGRectMake(128, 90,60, 60);
    self.activityIndicator.hidesWhenStopped = YES;
    [self.moviePlayer.view addSubview:self.activityIndicator];
    
    
    isFirstTimeLoadData = YES;
    
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getExitWindowPlayerStop:)
                                                 name:@"exitWindowVideoDetailPlayerStop"
                                               object:nil];
    
    
    self.playBtn.hidden = YES;
    self.headerObject.btnShare.hidden = YES;
    self.headerObject.iconSeperator.hidden = YES;
    
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;
    
    
    
}


- (void)getExitWindowPlayerStop:(NSNotification*) notif
{
    [self.moviePlayer stop];
    //    [self.moviePlayer.view removeFromSuperview];
    //   self.moviePlayer = nil;
}

- (void)receiveVideoDetailNotification:(NSNotification *)notification
{
    [[SPSingletonClass sharedInstance] setIsViewDetailLoaded:NO];
    self.detailDataDictionary =  [[AppDelegate shareddelegate].videoDetailDict mutableCopy];
    [self plistData];
    
    [self loadData];
    [self recordedVideos];
    
    /*
     BOOL localRecodingVideoEnable = [self localRecordingBtnEnable];
     if (localRecodingVideoEnable == YES){
     }*/
    
}



- (void)plistData

{
    
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
    NSString *documentsDirectory = [sysPaths objectAtIndex:0];
    NSURL *filePath =  [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"RecordedVideos.plist"]];
    
    NSURL* plistPath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"RecordedVideos" ofType:@"plist"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:[filePath path]]) {
        [[NSFileManager defaultManager] copyItemAtPath:[plistPath path] toPath:[filePath path] error:nil];
        
    }
    
    
    NSData *data = [NSData dataWithContentsOfFile:[filePath path]];
    NSDictionary *jsonObject = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    self.recordedVideosArray = [jsonObject objectForKey:self.detailDataDictionary[@"Title"]];
    
    if (!self.recordedVideosArray) {
        self.recordedVideosArray = [NSMutableArray array];
    }
    
    
    if ([[self.detailDataDictionary valueForKey:@"FilePath"] isEqualToString:@""]){
        self.headerObject.btnShare.hidden = YES;
        self.headerObject.iconSeperator.hidden = YES;
        self.playBtn.hidden = YES;
    }else{
        self.headerObject.btnShare.hidden = NO;
        self.headerObject.iconSeperator.hidden = NO;
        self.playBtn.hidden = NO;
        
    }
    
    
}
-(BOOL)localRecordingBtnEnable
{
    AppDelegate *del = [AppDelegate shareddelegate];
    int localRecodingEnable =  del.appReference.islocalRecordingEnabled.intValue;
    
    if (localRecodingEnable == 1){
        
        return YES;
    }else{
        return NO;
        
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    
    if([[SPSingletonClass sharedInstance] isViewDetailLoaded]){
        videoID = [self.detailDataDictionary valueForKey:@"ItemId"];
        
    }
    if ([isSingleVideo isEqualToString:@"YES"]||[[SPSingletonClass sharedInstance] isViewDetailLoaded]){
        
        if (isFirstTimeLoadData == YES){
            SDZEXVideoServiceExample *videoService = [[[SDZEXVideoServiceExample alloc] init] autorelease];
            [videoService runVideoDetail:videoID];
            isFirstTimeLoadData = NO;
            
        }else{
            [self plistData];
            [self recordedVideos];
            /*
             BOOL localRecodingVideoEnable = [self localRecordingBtnEnable];
             if (localRecodingVideoEnable == YES){
             }*/
        }
        
    }else{
        [self plistData];
        [self recordedVideos];
        /*
         BOOL localRecodingVideoEnable = [self localRecordingBtnEnable];
         if (localRecodingVideoEnable == YES){
         }
         */
        if (isFirstTimeLoadData == YES){
            [self loadData];
            isFirstTimeLoadData = NO;
        }
        
        
    }
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerDidEnterFullscreen:) name:MPMoviePlayerDidEnterFullscreenNotification object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moviePlayexitFullscreen:) name:MPMoviePlayerWillExitFullscreenNotification object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackStateDidChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.moviePlayer];
    
    
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
    
    
}
#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Video"]) {
            self.aBannerDict = dict;
            NSLog(@"self.aboutBannerDict %@",self.aBannerDict);
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;
            
        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    //NSLog(@"self.bannerUrlsArray %@",self.bannerURLS);
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //        [self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
    
    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}
-(void)entryAction
{
    
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    //    [example1 runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID intValue]  deviceType:@"iOS"];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        NSLog(@"UIInterfaceOrientationLandscapeLeft ");
        // [self resizeForLandscape];
    } else {
        NSLog(@"resizeForPortrait ");
        
        //[self resizeForPortrait];
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"getVideoDetailNotification"
                                                  object:nil];
    [super viewWillDisappear:animated];
    
    
}

- (void)loadData
{
    
    self.videoTileLbl.text = [self.detailDataDictionary valueForKey:@"Title"];
    
    if (self.videoTileLbl.text.length > 32) {
        
        self.videoTileLbl.frame = CGRectMake(self.videoTileLbl.frame.origin.x, self.videoTileLbl.frame.origin.y, self.videoTileLbl.frame.size.width, 60);
        self.videoTileLbl.numberOfLines =2;
        
        self.playBtn.frame = CGRectMake(CGRectGetMinX(self.playBtn.frame), CGRectGetMaxY(self.videoTileLbl.frame)+4, CGRectGetWidth(self.playBtn.frame), CGRectGetHeight(self.playBtn.frame));
        
        self.desTF.frame = CGRectMake(CGRectGetMinX(self.desTF.frame), CGRectGetMaxY(self.playBtn.frame), CGRectGetWidth(self.desTF.frame), CGRectGetHeight(self.desTF.frame));
        
        [self recordedVideos];
    }
    
    [self.videoTileLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    
    
    
    self.desTF.text = [self.detailDataDictionary valueForKey:@"Description"];
    self.desTF.font = [UIFont fontWithName:@"Helvetica-Condensed" size:17];
    
    [self.desTF sizeToFit];
    
    
    self.videoImageView.hidden = NO;
    [self.playBtn setSelected:NO];
    
    [self.videoDetailScrollView setContentSize:CGSizeMake(320, CGRectGetMaxY(self.desTF.frame))];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[self.detailDataDictionary objectForKey:@"Image"]];
    if ([urlString length]!=0)
    {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
            NSURLResponse* response = nil;
            NSError* error = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                // update the image and sent notification on the main thread
                
                if ([[self.detailDataDictionary valueForKey:@"Image"] length])
                {
                    self.videoImageView.image = [UIImage imageWithData:data];
                    // newsListCell.listImage.frame = CGRectMake(0, 2, 65, 52);
                    
                }else{
                    self.videoImageView.image = [UIImage imageNamed:@"no-image.png"];
                    //newsListCell.listImage.frame = CGRectMake(0, 2, 65, 52);
                    
                }
                
            });
        });
        
        
    }else{
        self.videoImageView.image = [UIImage imageNamed:@"no-image.png"];
        
    }
    //create a player
    
    _moviePlayer.view.hidden = YES;
    [self.view bringSubviewToFront:self.videoImageView];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)moviePlaybackStateDidChange:(NSNotification *)note
{
    
    switch (self.moviePlayer.playbackState) {
        case MPMoviePlaybackStatePlaying:
            
            if (isFromRecording) {
                self.recSelectedBtn.selected = YES;
            }else{
                self.playBtn.selected = YES;
            }
            [self.activityIndicator stopAnimating];
            
        case MPMoviePlaybackStateSeekingBackward:
        case MPMoviePlaybackStateSeekingForward:
            //  self.state = ALMoviePlayerControlsStateReady;
            break;
        case MPMoviePlaybackStateInterrupted:
            //   self.state = ALMoviePlayerControlsStateLoading;
            break;
        case MPMoviePlaybackStatePaused:
            
            if (isFromRecording) {
                self.recSelectedBtn.selected = NO;
            }else{
                self.playBtn.selected = NO;
            }
            [self.activityIndicator stopAnimating];
            
            break;
        case MPMoviePlaybackStateStopped:
            
            
            if (isFromRecording) {
                self.recSelectedBtn.selected = NO;
            }else{
                self.playBtn.selected = NO;
            }
            [self.activityIndicator stopAnimating];
            
            NSURL *url = self.moviePlayer.contentURL;
            [self.moviePlayer setContentURL:url];
            [self.moviePlayer prepareToPlay];
            break;
        default:
            break;
    }
}
- (void)moviePlayBackDidFinish:(NSNotification*) notif
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayer];
    
    
    NSNumber* reason = [[notif userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    switch ([reason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            NSLog(@"Playback Ended");
            if (isFromRecording) {
                self.recSelectedBtn.selected = NO;
            }else{
                self.playBtn.selected = NO;
            }
            
            break;
        case MPMovieFinishReasonPlaybackError:
            if (isFromRecording) {
                self.recSelectedBtn.selected = NO;
            }else{
                self.playBtn.selected = NO;
            }
            NSLog(@"Playback Error");
            [self performSelector:@selector(corruptVideoAlertView) withObject:nil afterDelay:1.0];
            break;
        case MPMovieFinishReasonUserExited:
            if (isFromRecording) {
                self.recSelectedBtn.selected = NO;
            }else{
                self.playBtn.selected = NO;
            }
            NSLog(@"User Exited");
            break;
        default:
            break;
    }
    
}
-(void) moviePlayexitFullscreen:(NSNotification*) notif
{
    //MPMoviePlayerController *aMoviePlayer = [notif object];
    
    self.appDelegate.isOrientation = NO;
    if([UIDevice currentDevice].orientation == UIInterfaceOrientationLandscapeLeft || [UIDevice currentDevice].orientation ==UIInterfaceOrientationLandscapeRight){
        objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIInterfaceOrientationPortrait);
    }
    /*
     [[NSNotificationCenter defaultCenter] removeObserver:self
     name:MPMoviePlayerWillExitFullscreenNotification
     object:aMoviePlayer];*/
    
}


- (void)playerDidEnterFullscreen:(NSNotification*) notif
{
    //MPMoviePlayerController *aMoviePlayer = [notif object];
    
    self.appDelegate.isOrientation = YES;
    
    
    UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    self.videoView = [[window subviews] lastObject];
    NSLog(@"subViews%@",[window subviews]);
    
    _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPHONE5){
        _deleteButton.frame = CGRectMake (15,440, 65, 44);
        
    }else{
        _deleteButton.frame = CGRectMake (15,367, 65, 44);
        
    }
    
    [_deleteButton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    [_deleteButton.titleLabel setFont:[UIFont systemFontOfSize:20.f]];
    _deleteButton.tag = 100;
    [_deleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [_deleteButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    if (isFromRecording == YES) {
        [self.videoView addSubview:_deleteButton];
    }else{
        [self.deleteButton setHidden:YES];
    }
    self.deleteButton.autoresizingMask = ( UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    
    [self performSelector:@selector(targetMethod) withObject:nil afterDelay:20.0];
    
}

- (void)targetMethod
{
    
    for (UIView *view in [self.videoView subviews]) {
        if (view.tag == 100) {
            [view removeFromSuperview];
        }
    }
    
}

- (void)corruptVideoAlertView
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information!" message:@"The current video cannot be played at this time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void) favoritesViewContrll:(NSNotification *) notification
{
    SPUserFavoriteVC *appUserFevVC = [[SPUserFavoriteVC alloc]initWithNibName:@"SPUserFavoriteVC" bundle:nil];
    appUserFevVC.appType = APP_VIDEO;
    appUserFevVC.boolNoListing = YES;
    [self.navigationController pushViewController:appUserFevVC animated:YES];
}


- (void)recordedVideos
{
    
    AppDelegate *del = [AppDelegate shareddelegate];
    int localRecodingEnable =  del.appReference.islocalRecordingEnabled.intValue;
    
    if (localRecodingEnable == 0){
        
        return;
    }
    
    
    NSUInteger j= 0;
    
    
    if (self.recordedVideosArray.count == 6) {
        j = self.recordedVideosArray.count;
    }
    else
    {
        j = self.recordedVideosArray.count + 1; // 1 is for record btn
    }
    
    for (UIView *view in self.videoDetailScrollView.subviews) {
        
        if (view.tag >= 100) {
            [view removeFromSuperview];
        }
        
    }
    
    Float32 space= (self.view.frame.size.width - (ButtonWidth * ((self.playBtn.isHidden)? j:j+1)))/((self.playBtn.isHidden)? j+1:j+2) ; // 1 is for create btn
    Float32 x = space;
    
    CGRect createBtnNewFrame = self.playBtn.frame;
    createBtnNewFrame.origin.x = x;
    self.playBtn.frame = createBtnNewFrame;
    
    
    for(int buttonIndex=(self.playBtn.isHidden)?1:0; buttonIndex <= j ;buttonIndex++)
    {
        
        _recSelectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (j == buttonIndex && ((self.recordedVideosArray.count == (j - 1) )|| buttonIndex != 6)) {
            [_recSelectedBtn setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
            [_recSelectedBtn addTarget:self action:@selector(videoRecordBtn:) forControlEvents:UIControlEventTouchUpInside];
        }else{
            
            [_recSelectedBtn setImage:[UIImage imageNamed:@"recPlay.png"] forState:UIControlStateNormal];
            [_recSelectedBtn setImage:[UIImage imageNamed:@"recPause.png"] forState:UIControlStateSelected];
            [_recSelectedBtn addTarget:self action:@selector(playButtonClicked:saveVideosToDiskForVideo:) forControlEvents:UIControlEventTouchUpInside];
        }
        //Set offset in Y axis
        
        if (j == buttonIndex == 1 || buttonIndex != 0) {
            _recSelectedBtn.frame = CGRectMake(x  , self.playBtn.frame.origin.y , ButtonWidth, ButtonWidth);
            //Set Tag for future identification
            [_recSelectedBtn setTag:buttonIndex + 100];
            [self.videoDetailScrollView addSubview:_recSelectedBtn];
        }
        
        x = x + space + ButtonWidth;
        
    }
    
}

- (void)videoRecordBtn:(UIButton *)sender
{
    
    [self.moviePlayer stop];
    SPRecordingViewController *recordingVC = [[SPRecordingViewController alloc]initWithNibName:@"SPRecordingViewController" bundle:nil];
    recordingVC.videoDetailDictionary = self.detailDataDictionary;
    recordingVC.recordedVideosArray = self.recordedVideosArray;
    [self.navigationController pushViewController:recordingVC animated:YES];
    
}


// Server Video's Player
- (IBAction)videoPlayBtn:(UIButton *)sender
{
    self.headerObject.btnShare.hidden = NO;
    self.headerObject.iconSeperator.hidden = NO;
    _recSelectedBtn.selected = NO;
    isFromRecording = NO;
    
    [self.moviePlayer.view setHidden:NO];
    self.videoImageView.hidden =YES;
    if (![sender isSelected]) {
        [sender setSelected:YES];
        self.playBtn.selected = YES;
        [self.activityIndicator startAnimating];
        
        NSString *urlString = [NSString stringWithFormat:@"http://%@",[self.detailDataDictionary valueForKey:@"FilePath"]];
        
        if (self.moviePlayer.playbackState == MPMoviePlaybackStateStopped){
            [self.moviePlayer setContentURL:nil];
            [self.moviePlayer setContentURL:[NSURL URLWithString:urlString]];
            
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused){
            if ( [self.moviePlayer.contentURL isEqual:[NSURL URLWithString:urlString]]) {
                [self.moviePlayer play];
                return;
                
            }else{
                
                [self.moviePlayer setContentURL:[NSURL URLWithString:urlString]];
                
            }
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying){
            [self.moviePlayer setContentURL:nil];
            [self.moviePlayer setContentURL:[NSURL URLWithString:urlString]];
        }
        [self.moviePlayer play];
        
    }
    else {
        
        [sender setSelected:NO];
        [self.moviePlayer pause];
        return;
    }
    
}


//Local Video's Player
- (void)playButtonClicked:(UIButton *)button saveVideosToDiskForVideo:(NSString *)videoName
{
    self.headerObject.btnShare.hidden = YES;
    self.headerObject.iconSeperator.hidden = YES;
    [self.moviePlayer.view setHidden:NO];
    self.videoImageView.hidden =YES;
    _recSelectedBtn = button;
    self.playBtn.selected = NO;
    isFromRecording = YES;
    [self.activityIndicator startAnimating];
    for (UIButton *recPlayBtn in self.videoDetailScrollView.subviews) {
        
        if (recPlayBtn.tag >= 100 && recPlayBtn!= button) {
            [recPlayBtn setSelected:NO];
        }
        
    }
    
    
    if (![button isSelected]) {
        [button setSelected:YES];
        
        deleteIndex = NULL;
        deleteIndex = button.tag - 101;
        
        //        deleteIndex = buttonIndex;
        //        self.appDelegate.isOrientation = YES;
        
        NSURL *fileURL = [self.recordedVideosArray objectAtIndex:deleteIndex];
        
        
        if (self.moviePlayer.playbackState == MPMoviePlaybackStateStopped){
            
            [self.moviePlayer setContentURL:nil];
            
            [self.moviePlayer setContentURL:fileURL];
            
            
            
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused){
            
            if ( [self.moviePlayer.contentURL isEqual:fileURL]) {
                [self.moviePlayer play];
                return;
                
            }else{
                [self.moviePlayer setContentURL:fileURL];
                
                
            }
            
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying){
            [self.moviePlayer setContentURL:nil];
            [self.moviePlayer setContentURL:fileURL];
            
            
        }
        
        
        //    [self configureViewForOrientation:[UIApplication sharedApplication].statusBarOrientation];
        [self.moviePlayer play];
        
        
    } else {
        [button setSelected:NO];
        
        [self.moviePlayer pause];
    }
    
}

- (void)deleteButtonPressed:(UIButton *)deleteButton
{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this video?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1){
        NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
        NSString *documentsDirectory = [sysPaths objectAtIndex:0];
        NSURL *filePath =  [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"RecordedVideos.plist"]];
        
        NSLog(@"File Path: %@", filePath);
        
        NSMutableDictionary *plistDict; // needs to be mutable
        if ([[NSFileManager defaultManager] fileExistsAtPath:[filePath path]]) {
            NSData *data = [NSData dataWithContentsOfFile:[filePath path]];
            plistDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        } else {
            // Doesn't exist, start with an empty dictionary
            plistDict = [[NSMutableDictionary alloc] init];
            
        }
        
        NSData *data = [NSData dataWithContentsOfFile:[filePath path]];
        NSMutableDictionary *savedStock = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSLog(@"savedStock %@",savedStock);
        self.recordedVideosArray = [savedStock objectForKey:self.detailDataDictionary[@"Title"]];
        [self.recordedVideosArray removeObjectAtIndex:deleteIndex];
        
        [plistDict setObject:self.recordedVideosArray forKey:self.detailDataDictionary[@"Title"]];
        
        NSData *data1 = [NSKeyedArchiver archivedDataWithRootObject:plistDict];
        BOOL didWriteToFile = [data1 writeToFile:[filePath path] atomically:YES];
        if (didWriteToFile) {
            NSLog(@"Write to file a SUCCESS!");
        } else {
            NSLog(@"Write to file a FAILURE!");
            
        }
        
        [self plistData];
        [self.moviePlayer stop];
        self.videoImageView.hidden = NO;
        [self.moviePlayer setFullscreen:NO animated:YES];
        self.moviePlayer.view.hidden = YES;
    }
}
@end
