//
//  DetailRemainderCustomView.h
//  VideoWidget
//
//  Created by Sandeep on 21/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserFavoriteVC.h"

@interface DetailRemainderCustomView : UIView

@property (strong, nonatomic) id <userFevVideoDelegate> endRepeatCustomViewDelegate;
- (void) prepareView;
- (void)prepareView:(NSMutableDictionary *)editDict;


- (void)doneButtonClicked:(UIButton *)sender;
-(void)datePickerChanged:(id *)sender;
@end
