//
//  VideoCollectionViewCell.m
//  VideoWidget
//
//  Created by Sandeep on 16/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import "SPFavoritesCollectionCell.h"

#import "AppDelegate.h"
#define ButtonWidth 40



@interface SPFavoritesCollectionCell ()
{
    BOOL isFromRecording;
    int deleteIndex;
}

@property (nonatomic) CGRect defaultFrame;
@property (strong, nonatomic) IBOutlet UITextView *descriptTextView;
@property (nonatomic, assign) BOOL *isSelected;
@property (nonatomic,strong) IBOutlet UILabel *titleLbl;
@property (nonatomic,strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSString *videoUrlStr;
@property (nonatomic, strong) NSMutableDictionary *savedStock;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UIView * videoView;

@property (nonatomic, strong) UIButton *deleteButton;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation SPFavoritesCollectionCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackStateDidChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish2:) name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerDidEnterFullscreen:) name:MPMoviePlayerDidEnterFullscreenNotification object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moviePlayexitFullscreen:) name:MPMoviePlayerWillExitFullscreenNotification object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getExitWindowPlayerStop1:)
                                                 name:@"exitWindowFevPlayerStop"
                                               object:nil];
    self.videoPlayBtnRef.hidden = YES;

    
    self.detailsArray = [[AppDelegate shareddelegate].userFevVideoDict mutableCopy];
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self plistData];
    
}


- (void)plistData
{
    NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
    NSString *documentsDirectory = [sysPaths objectAtIndex:0];
    NSURL *filePath =  [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"RecordedVideos.plist"]];
    
    NSURL* plistPath = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"RecordedVideos" ofType:@"plist"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:[filePath path]]) {
        [[NSFileManager defaultManager] copyItemAtPath:[plistPath path] toPath:[filePath path] error:nil];
        
    }
    
    
    NSData *data = [NSData dataWithContentsOfFile:[filePath path]];
    self.savedStock = [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)getExitWindowPlayerStop1:(NSNotification*) notif
{
    [self.moviePlayer stop];
//    [self.moviePlayer.view removeFromSuperview];
    self.moviePlayer = nil;
}


-(void) moviePlayexitFullscreen:(NSNotification*) notif
{
    
    self.appDelegate.isOrientation = NO;
    
    SPFavoritesDetailVC *favoriteDetailVC = (SPFavoritesDetailVC *)self.detailVideoViewDelegate ;
    favoriteDetailVC.isVideoFullScreenExited = YES;
    
    
}


- (void)moviePlaybackStateDidChange:(NSNotification *)note
{
    switch (self.moviePlayer.playbackState) {
        case MPMoviePlaybackStatePlaying:

            if (isFromRecording == YES) {
                self.button.selected = YES;
            }else{
                self.videoPlayBtnRef.selected = YES;
            }
            [self.activityIndicator stopAnimating];
            
        case MPMoviePlaybackStateSeekingBackward:
        case MPMoviePlaybackStateSeekingForward:
            //  self.state = ALMoviePlayerControlsStateReady;
            break;
        case MPMoviePlaybackStateInterrupted:
            //   self.state = ALMoviePlayerControlsStateLoading;
            break;
        case MPMoviePlaybackStatePaused:

            if (isFromRecording == YES) {
                self.button.selected = NO;
            }else{
                self.videoPlayBtnRef.selected = NO;
            }
            [self.activityIndicator stopAnimating];

            break;
            
        case MPMoviePlaybackStateStopped:

            if (isFromRecording == YES) {
                self.button.selected = NO;
            }else{
                self.videoPlayBtnRef.selected = NO;
            }
            [self.activityIndicator stopAnimating];

            NSURL *url = self.moviePlayer.contentURL;
            [self.moviePlayer setContentURL:url];
            [self.moviePlayer prepareToPlay];

            break;
            
        default:
            break;
    }
}


- (void)videoPlayerInitializationWithIndexPath:(NSIndexPath *)indexPath
{
    [self plistData];
    [self.moviePlayer stop];
    
    self.videoPlayBtnRef.selected = NO;
    self.button.selected = NO;
    _detailDictionary = [self.detailsArray objectAtIndex:indexPath.row];
    
    self.recordedVideosArray = [self.savedStock objectForKey:_detailDictionary[@"Title"]];
    
    self.titleLbl.text = [_detailDictionary valueForKey:@"Title"];
    [self.titleLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    
    self.descriptTextView.text = [_detailDictionary valueForKey:@"Description"];
    // [self.descriptTextView sizeToFit];
    
    [self.descriptTextView setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:17]];
    
    if (self.titleLbl.text.length > 32) {
        
        self.titleLbl.frame = CGRectMake(self.titleLbl.frame.origin.x, self.titleLbl.frame.origin.y, self.titleLbl.frame.size.width, 60);
        self.titleLbl.numberOfLines =2;
        
        self.videoPlayBtnRef.frame = CGRectMake(CGRectGetMinX(self.videoPlayBtnRef.frame), CGRectGetMaxY(self.titleLbl.frame)+4, CGRectGetWidth(self.videoPlayBtnRef.frame), CGRectGetHeight(self.videoPlayBtnRef.frame));
        
        self.descriptTextView.frame = CGRectMake(CGRectGetMinX(self.descriptTextView.frame), CGRectGetMaxY(self.videoPlayBtnRef.frame), CGRectGetWidth(self.descriptTextView.frame), CGRectGetHeight(self.descriptTextView.frame)-6);
        
    }
    
    
    AppDelegate *del = [AppDelegate shareddelegate];
    int localRecodingEnable =  del.appReference.islocalRecordingEnabled.intValue;
    if (localRecodingEnable == 1){
        [self recordedVideos];
        
    }
    
    if (!self.recordedVideosArray) {
        self.recordedVideosArray = [NSMutableArray array];
    }
    
    
    [self bringSubviewToFront:self.displayImageView];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[[self.detailsArray objectAtIndex:indexPath.row] valueForKey:@"Image"]];
    if ([[[self.detailsArray objectAtIndex:indexPath.row] valueForKey:@"Image"] length]!= 0) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
            NSURLResponse* response = nil;
            NSError* error = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                // update the image and sent notification on the main thread
                
                if ([[_detailDictionary valueForKey:@"Image"] length]) {
                    self.displayImageView.image = [UIImage imageWithData:data];
                    
                }
//                else{
//                    self.displayImageView.image = [UIImage imageNamed:@"no-image.png"];
//                    
//                }
                
            });
        });
        
        
    }else{
        self.displayImageView.image = [UIImage imageNamed:@"no-image.png"];
        
    }
    
    self.videoUrlStr = [NSString stringWithFormat:@"http://%@",[_detailDictionary valueForKey:@"FilePath"]];
    
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.frame = CGRectMake(128, 90,60, 60);
    self.activityIndicator.hidesWhenStopped = YES;
    [self.moviePlayer.view addSubview:self.activityIndicator];
    
    [self addSubview:self.moviePlayer.view];
    self.moviePlayer.view.hidden = YES;
    [self.videoPlayBtnRef setSelected:NO];
    
    
}
-(void)playerDidEnterFullscreen:(NSNotification *) notif
{
    //MPMoviePlayerController *aMoviePlayer = [notif object];
    
    self.appDelegate.isOrientation = YES;
    
    UIWindow *window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    _videoView = [[window subviews] lastObject];
    
    _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IS_IPHONE5){
        _deleteButton.frame = CGRectMake (15,440, 65, 44);
        
    }else{
        _deleteButton.frame = CGRectMake (15,367, 65, 44);
        
    }
    
    [_deleteButton setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    [_deleteButton.titleLabel setFont:[UIFont systemFontOfSize:20.f]];
    _deleteButton.tag = 100;
    [_deleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [_deleteButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.deleteButton.autoresizingMask = ( UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    if (isFromRecording == YES) {
        [_videoView addSubview:_deleteButton];
    }
    [self performSelector:@selector(targetMethod) withObject:nil afterDelay:20.0];
    
}

- (void) targetMethod
{
    
    for (UIView *view in [self.videoView subviews])
    {
        if (view.tag == 100) {
            [view removeFromSuperview];
        }
    }

    
}


- (void)recordedVideos
{
    
    if ([[_detailDictionary valueForKey:@"FilePath"] isEqualToString:@""]){
        self.videoPlayBtnRef.hidden = YES;
    }else{
        self.videoPlayBtnRef.hidden = NO;
        
    }

    
    NSUInteger j= 0;
    
    
    if (self.recordedVideosArray.count == 6) {
        j = self.recordedVideosArray.count;
    }
    else
    {
        j = self.recordedVideosArray.count + 1; // 1 is for record btn
    }
    
    for (UIView *view in self.subviews) {
        
        if (view.tag >= 100) {
            [view removeFromSuperview];
        }
        
    }
    
    Float32 space= (self.frame.size.width - (ButtonWidth * ((self.videoPlayBtnRef.isHidden)? j:j+1)))/((self.videoPlayBtnRef.isHidden)? j+1:j+2) ;
    Float32 x = space;
    
    CGRect createBtnNewFrame = self.videoPlayBtnRef.frame;
    createBtnNewFrame.origin.x = x;
    self.videoPlayBtnRef.frame = createBtnNewFrame;
    
    
    for(int buttonIndex=(self.videoPlayBtnRef.isHidden)?1:0; buttonIndex <= j ;buttonIndex++){
        
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        
        if (j == buttonIndex && ((self.recordedVideosArray.count == (j - 1) )|| buttonIndex != 6)) {
            [self.button setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
            [self.button addTarget:self action:@selector(videoRecordBtn:) forControlEvents:UIControlEventTouchUpInside];
        }else{
            
            
            [self.button setImage:[UIImage imageNamed:@"recPlay.png"] forState:UIControlStateNormal];
            [self.button setImage:[UIImage imageNamed:@"recPause.png"] forState:UIControlStateSelected];
            [self.button addTarget:self action:@selector(playButtonClicked:saveVideosToDiskForVideo:) forControlEvents:UIControlEventTouchUpInside];
        }
        //Set offset in Y axis
        
        if (j == buttonIndex == 1 || buttonIndex != 0) {
            //       self.createBtn.frame = CGRectMake(x , 200 , 40.0, 40.0);
            self.button.frame = CGRectMake(x  , CGRectGetMinY(self.videoPlayBtnRef.frame) , ButtonWidth, ButtonWidth);
            //Set Tag for future identification
            [self.button setTag:buttonIndex + 100];
            [self addSubview:self.button];
        }
        
        x = x + space + ButtonWidth;
        
    }
}



- (void)moviePlayBackDidFinish2:(NSNotification *) notif
{
    
    
    NSNumber* reason = [[notif userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    switch ([reason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            NSLog(@"Playback Ended");
            self.displayImageView.hidden = NO;
            //            [self.moviePlayer stop];
            if (isFromRecording) {
                self.button.selected = NO;
            }else{
                self.videoPlayBtnRef.selected = NO;
            }
            
            break;
        case MPMovieFinishReasonPlaybackError:
            if (isFromRecording) {
                self.button.selected = NO;
            }else{
                self.videoPlayBtnRef.selected = NO;
            }
            NSLog(@"Playback Error");
//            [self performSelector:@selector(corruptVideoAlertView) withObject:nil afterDelay:1.0];
            break;
        case MPMovieFinishReasonUserExited:
            if (isFromRecording) {
                self.button.selected = NO;
            }else{
                self.videoPlayBtnRef.selected = NO;
            }
            NSLog(@"User Exited");
            break;
        default:
            break;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:self.moviePlayer];
    
}

/*
- (void)corruptVideoAlertView
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information!" message:@"The video is currently unavailable" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
*/
//Server Player
- (IBAction)videoPlayBtn:(UIButton *)sender
{
    
    isFromRecording = NO;
    self.button.selected = NO;
    self.displayImageView.hidden =YES;
    self.moviePlayer.view.hidden = NO ;
    if (![sender isSelected]) {
        [sender setSelected:YES];
        self.videoPlayBtnRef.selected = YES;
        [self.activityIndicator startAnimating];
        [sender setSelected:YES];
        self.videoPlayBtnRef.selected = YES;
        
        if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying){
            
            [self.moviePlayer setContentURL:nil];
            [self.moviePlayer setContentURL:[NSURL URLWithString:self.videoUrlStr]];
            
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused){
            
            if ( [self.moviePlayer.contentURL isEqual:[NSURL URLWithString:self.videoUrlStr]]) {
                [self.moviePlayer play];
                return;
                
            }else{

                [self.moviePlayer setContentURL:[NSURL URLWithString:self.videoUrlStr]];
                
            }
            
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStateStopped){
            [self.moviePlayer setContentURL:nil];
            [self.moviePlayer setContentURL:[NSURL URLWithString:self.videoUrlStr]];
            
        }
        [self.moviePlayer play];
        
        
    } else {
      
        [sender setSelected:NO];
        [self.moviePlayer pause];
    }
    
    
}

- (void)videoRecordBtn:(UIButton *)sender
{
    
    self.videoPlayBtnRef.selected = NO;
    
    [self.detailVideoViewDelegate gotoRecordingView];
    
}

//
- (void)playButtonClicked:(UIButton *)button saveVideosToDiskForVideo:(NSString *)videoName
{
    
    self.displayImageView.hidden = YES;
    self.moviePlayer.view.hidden = NO;
    self.button = button;
    [self.activityIndicator startAnimating];
    self.videoPlayBtnRef.selected = NO;
    isFromRecording = YES;
    
    for (UIButton *recPlayBtn in self.subviews) {
        
        if (recPlayBtn.tag >= 100 && recPlayBtn!= button) {
            [recPlayBtn setSelected:NO];
        }
        
    }
    
    
    if (![button isSelected]) {
        [button setSelected:YES];
        deleteIndex = NULL;
        
        deleteIndex = button.tag - 101;
         NSURL *fileURL = [self.recordedVideosArray objectAtIndex:deleteIndex];
        
        if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying){
            [self.moviePlayer setContentURL:nil];

            [self.moviePlayer setContentURL:fileURL];
            
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused){
            
            if ( [self.moviePlayer.contentURL isEqual:fileURL]) {
                [self.moviePlayer play];
                return;
                
            }else{
                
                [self.moviePlayer setContentURL:fileURL];
                
            }
            
        }else if (self.moviePlayer.playbackState == MPMoviePlaybackStateStopped){
            [self.moviePlayer setContentURL:nil];

            [self.moviePlayer setContentURL:fileURL];
            
        }
        
        
        //    [self configureViewForOrientation:[UIApplication sharedApplication].statusBarOrientation];
        [self.moviePlayer play];
        
        
    } else {
       
        [button setSelected:NO];
        [self.moviePlayer pause];
    }
    
}

- (void)deleteButtonPressed:(UIButton *)deleteButton
{
    
    
    
    
    //Are you sure you want to delete this video?
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this video?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
    [alertView show];
    
    
    //[self.detailVideoViewDelegate goPopView];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1){
        
        NSArray *sysPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory ,NSUserDomainMask, YES);
        NSString *documentsDirectory = [sysPaths objectAtIndex:0];
        NSURL *filePath =  [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"RecordedVideos.plist"]];
        
        NSLog(@"File Path: %@", filePath);
        
        NSMutableDictionary *plistDict; // needs to be mutable
        if ([[NSFileManager defaultManager] fileExistsAtPath:[filePath path]]) {
            NSData *data = [NSData dataWithContentsOfFile:[filePath path]];
            plistDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        } else {
            // Doesn't exist, start with an empty dictionary
            plistDict = [[NSMutableDictionary alloc] init];
            
        }
        
        NSData *data = [NSData dataWithContentsOfFile:[filePath path]];
        NSMutableDictionary *savedStock = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSLog(@"savedStock %@",savedStock);
        self.recordedVideosArray = [savedStock objectForKey:self.detailDictionary[@"Title"]];
        [self.recordedVideosArray removeObjectAtIndex:deleteIndex];
        
        [plistDict setObject:self.recordedVideosArray forKey:self.detailDictionary[@"Title"]];
        
        NSData *data1 = [NSKeyedArchiver archivedDataWithRootObject:plistDict];
        BOOL didWriteToFile = [data1 writeToFile:[filePath path] atomically:YES];
        if (didWriteToFile) {
            NSLog(@"Write to file a SUCCESS!");
        } else {
            NSLog(@"Write to file a FAILURE!");
            
        }

        [self plistData];
        [self.moviePlayer stop];
        [self.moviePlayer setFullscreen:NO animated:YES];
        SPFavoritesDetailVC *favoriteDetailVC = (SPFavoritesDetailVC *)self.detailVideoViewDelegate ;
        favoriteDetailVC.isVideoFullScreenExited = NO;
        self.moviePlayer.view.hidden = YES;
        self.displayImageView.hidden = NO;

    }
}

@end
