//
//  VideoCollectionViewCell.h
//  VideoWidget
//
//  Created by Sandeep on 16/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SPFavoritesCollectionCell.h"
#import "SPFavoritesDetailVC.h"

@interface SPFavoritesCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *videoPlayBtnRef;
@property (strong, nonatomic) IBOutlet UIButton *videoRecordBtnRef;
@property (strong, nonatomic) IBOutlet UIImageView *displayImageView;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) SPFavoritesCollectionCell *videoCollectionCell;

@property (strong, nonatomic) id <detailVideoViewProtocol> detailVideoViewDelegate;

@property (nonatomic,strong) NSMutableArray *detailsArray;
@property (nonatomic, strong) NSMutableArray *recordedVideosArray;
@property (nonatomic, strong) NSDictionary *detailDictionary;
@property (assign, nonatomic) BOOL isVideoFullScreenExited;

- (void)videoPlayerInitializationWithIndexPath:(NSIndexPath *)indexPath;

@end


