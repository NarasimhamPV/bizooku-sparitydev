//
//  SPVideoDetailVC.h
//  Exceeding
//
//  Created by Veeru on 11/11/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DBTilesInfo.h"
#import "BigBanners.h"


@class SPRecordingViewController;
@protocol  videosRecordingProtocol<NSObject>
@required
-(void)recordedVideos;
@end


@interface SPVideoDetailVC : AppMainDetailVC<videosRecordingProtocol,BigBannersDelegate>

@property (strong, nonatomic) NSMutableArray *recordedVideosArray;
@property (strong,nonatomic) NSString *isSingleVideo;
@property (strong,nonatomic) NSString *videoID;

@property (strong, nonatomic) DBTilesInfo *tileInfo;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
