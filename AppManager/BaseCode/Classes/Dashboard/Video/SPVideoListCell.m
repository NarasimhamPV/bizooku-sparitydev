//
//  SPVideoListCell.m
//  Exceeding
//
//  Created by Veeru on 11/11/14.
//
//

#import "SPVideoListCell.h"
#import "UILabel+Exceedings.h"

@implementation SPVideoListCell
@synthesize isVideoListVC;


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



- (void)prepareView:(NSDictionary *)listVideoDic
{
    
    self.titleLabel.text = [listVideoDic valueForKey:@"Title"];
    [self.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    [self.titleLabel setNumberOfLines:1];

    self.descriptionLbl.text = [listVideoDic valueForKey:@"Description"];
    [self.descriptionLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18]];
    [self.descriptionLbl setNumberOfLines:2];
    
    UILabel *buttonLabel = [[UILabel alloc] initWithFrame:self.addToFavBtn.bounds];
    buttonLabel.numberOfLines = 3;
    buttonLabel.textAlignment = NSTextAlignmentCenter;
    [buttonLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:14]];
    [buttonLabel setTextColor:[UIColor whiteColor]];
    [self.addToFavBtn addSubview:buttonLabel];
    [self.addToFavBtn bringSubviewToFront:buttonLabel];

    NSNumber *isVideoEnable = [listVideoDic valueForKey:@"IsFavourite"];
    int isFevVideo = isVideoEnable.intValue;

    if (self.isVideoListVC){

        if (isFevVideo == 1){
            self.fevImageView.hidden = NO;
            [self.fevImageView setImage:[UIImage imageNamed:@"favourite.png"]];
            [self.addToFavBtn removeTarget:self action:@selector(addFavorite:) forControlEvents:UIControlEventTouchUpInside];
            [self.addToFavBtn removeTarget:self action:@selector(deleteFavorite:) forControlEvents:UIControlEventTouchUpInside];

            self.addToFavBtn.hidden = YES;

        }else{
            buttonLabel.text = @"Add \n to \n Favorites";
            self.fevImageView.hidden = YES;
            self.addToFavBtn.hidden = NO;
            [self.addToFavBtn removeTarget:self action:@selector(deleteFavorite:) forControlEvents:UIControlEventTouchUpInside];
            [self.addToFavBtn addTarget:self action:@selector(addFavorite:) forControlEvents:UIControlEventTouchUpInside];
        }


    }else{
        [self.fevImageView setHidden:YES];
        //[self.fevImageView setImage:[UIImage imageNamed:@"favourite.png"]];
        [self.addToFavBtn removeTarget:self action:@selector(addFavorite:) forControlEvents:UIControlEventTouchUpInside];
        buttonLabel.text = @"Delete";
        [self.addToFavBtn setBackgroundColor:[UIColor redColor]];
        [self.addToFavBtn addTarget:self action:@selector(deleteFavorite:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kWebServiceURL,[listVideoDic objectForKey:@"Image"]];
    if ([[listVideoDic objectForKey:@"Image"] length]!=0) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
            NSURLResponse* response = nil;
            NSError* error = nil;
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                // update the image and sent notification on the main thread
                
                if ([[listVideoDic valueForKey:@"Image"] length]) {
                    self.videoImage.image = [UIImage imageWithData:data];
                }/*
                else{
                    self.videoImage.image = [UIImage imageNamed:@"no-image.png"];
                }*/
                
            });
        });
        
        
    }else{
        self.videoImage.image = [UIImage imageNamed:@"no-image.png"];
        
    }
    
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing: editing animated: YES];
    
    if (editing)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
        {
            
            for(UIView* view in self.subviews)
            {
                
                if([[[view class] description] isEqualToString:@"UITableViewCellScrollView"])
                {
                    for(UIView* view1 in view.subviews)
                    {
                        if([[[view1 class] description] isEqualToString:@"UITableViewCellReorderControl"])
                        {
                            for(UIImageView* imgView in view1.subviews)
                            {
                                
                                ((UIImageView *)imgView).frame = CGRectMake(8, 41, 36, 28);
                                ((UIImageView *)imgView).image = [UIImage imageNamed: @"GrayIcon.png"];
                            }
                            
                        }
                    }
                }
            }
            
            
        }else{
            
            for (UIView *view in self.subviews)
            {
                if ([NSStringFromClass([view class]) rangeOfString: @"Reorder"].location != NSNotFound)
                {
                    for (UIView * subview in view.subviews)
                    {
                        if ([subview isKindOfClass: [UIImageView class]])
                        {
                            ((UIImageView *)subview).frame = CGRectMake(8, 41, 36, 28);
                            ((UIImageView *)subview).image = [UIImage imageNamed: @"GrayIcon.png"];
                            
                        }
                        
                    }
                }
            }
        }
    }
}




#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)addFavoriteAlert:(id)sender
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information!" message:@"To Delete Favorite go to Favorites Widget" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)addFavorite:(id)sender
{
    [UIView animateWithDuration:0.5f
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.addToFavBtn setFrame:CGRectMake(320, self.addToFavBtn.frame.origin.y, self.addToFavBtn.frame.size.width, self.addToFavBtn.frame.size.height)];
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                     }
     ];

    [self.videoListViewDelegate addFavoriteVideo:sender];
    
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}


-(void)deleteFavorite:(id)sender
{
    [UIView animateWithDuration:0.5f
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.addToFavBtn setFrame:CGRectMake(246, self.addToFavBtn.frame.origin.y, self.addToFavBtn.frame.size.width, self.addToFavBtn.frame.size.height)];
                         
                                 }
                     completion:^(BOOL finished){
                         
                                 }];

    [self.userFevVideoListViewDelegate deleteFavoriteVideo:sender];
}


@end
