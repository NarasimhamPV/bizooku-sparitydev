//
//  EndRepeatCustomView.m
//  VideoWidget
//
//  Created by Sandeep on 24/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import "EndRepeatCustomView.h"
#import "SPSingletonClass.h"
@implementation EndRepeatCustomView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)prepareView
{
    [self.repeatLabel setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:20]];
    [self.pickerDisableView setHidden:YES];


    self.checkBoxBtn.selected = NO;
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    self.pickerDisableView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];

    [self.endRepeatPicker setMinimumDate:[NSDate date]];

    [self.endRepeatPicker setDate:[NSDate date]];
    
    self.endRepeatPicker.datePickerMode = UIDatePickerModeDate;
    
    [self.endRepeatPicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self bringSubviewToFront:self.endRepeatPicker];
    [self bringSubviewToFront:self.checkBoxBtn];

}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
    
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
        
    if (component == 0){
        return 5;
    } else if (component == 1){
        return 3;
    }
    return 0;
}
    
-(void)datePickerChanged:(id *)sender
{
        
    NSDate *eventDate = self.endRepeatPicker.date;
    [self.currentDateFormatter setDateFormat:@"MM/dd/yyyy"];
    self.dateString = [self.currentDateFormatter stringFromDate:eventDate];
    
}
- (IBAction)checkBox:(id)sender
{
    NSLog(@"Veeru");
    if (self.checkBoxBtn.selected == YES){
        [self.pickerDisableView setHidden:YES];

        self.checkBoxBtn.selected = NO;


    }else{
        [self.pickerDisableView setHidden:NO];

        self.checkBoxBtn.selected = YES;
        
    }
    
}
- (NSDateFormatter *)currentDateFormatter
    {
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
         dateFormatter = [[NSDateFormatter alloc] init];
         [dateFormatter setDateFormat:@"MMM dd, yyyy"];
         [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
         [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    });
       return dateFormatter;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (void)doneButtonClicked:(UIButton *)sender
{
    if (self.checkBoxBtn.selected == YES){
        [[SPSingletonClass sharedInstance] setRepeatDateStr:@"Never >"];

//        if (self.dateString.length == 0){
//            [[SPSingletonClass sharedInstance] setRepeatDateStr:@"Never >"];
//
//        }else{
//        [[SPSingletonClass sharedInstance] setRepeatDateStr:self.dateString];
//        }

    }else{
        if (self.dateString.length == 0){
            [[SPSingletonClass sharedInstance] setRepeatDateStr:@"Never >"];

        }else{
            [[SPSingletonClass sharedInstance] setRepeatDateStr:self.dateString];

        }
        

        //[[SPSingletonClass sharedInstance] setRepeatDateStr:@"Never >"];

    }
    
    [self.endRepeatCustomViewDelegate doneBtnPressedInEndRepeatCustomViewWithSender:sender];

}


@end
