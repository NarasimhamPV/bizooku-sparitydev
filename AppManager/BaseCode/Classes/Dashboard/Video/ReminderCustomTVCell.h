//
//  ReminderCustomTVCell.h
//  VideoWidget
//
//  Created by Sandeep on 21/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemaindersCustomView.h"
#import "SPUserFavoriteVC.h"


@interface ReminderCustomTVCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;



@property (strong, nonatomic) id <remainderCustomViewProtocol> remainderCustomViewDelegate;
@property (strong, nonatomic) id <userFevVideoDelegate> remaindersViewDelegate;
@end
