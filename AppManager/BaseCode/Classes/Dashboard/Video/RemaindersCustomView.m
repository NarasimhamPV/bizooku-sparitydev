//
//  RemaindersCustomView.m
//  VideoWidget
//
//  Created by Sandeep on 21/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import "RemaindersCustomView.h"
#import "ReminderCustomTVCell.h"
#import "SPSingletonClass.h"
@implementation RemaindersCustomView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        [self reloadInputViews];
    }
    return self;
}
- (void)awakeFromNib
{
    [self.titleLbl setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:22]];

}
- (void)prepareView
{
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];

    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [self.remainderTableView registerNib:[UINib nibWithNibName:@"ReminderCustomTVCell" bundle:nil ] forCellReuseIdentifier:@"RemainderCustomCell"];
    self.remainderTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    NSString *path = [NSString stringWithFormat:@"%@/Documents/Remender.plist",NSHomeDirectory()];
    self.remainderArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
    [[SPSingletonClass sharedInstance] setRepeatDateStr:@""];
    [self.remainderTableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.remainderArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"RemainderCustomCell";
    ReminderCustomTVCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = [self.remainderArray objectAtIndex:indexPath.row];
    // cell.remaindersViewDelegate= self.remaindersViewDelegate;
    // Display notification info
    
    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(deleteRemiderPlist:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.infoBtn.tag = indexPath.row;
    [cell.infoBtn addTarget:self action:@selector(editRemiderPlist:) forControlEvents:UIControlEventTouchUpInside];
    

    
    [cell.titleLbl setText:[[dict valueForKey:@"Title"] capitalizedString]];
    [cell.dateLbl setText:[dict valueForKey:@"Date"]];
    
    return cell;
}

- (void)deleteRemiderPlist:(id)sender
{
    deleteIndex = [sender tag];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Delete Reminder?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
    [alertView show];
    
}
- (void)editRemiderPlist:(id)sender
{
    deleteIndex = [sender tag];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isEditReminder"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.remaindersViewDelegate gotoDetailRemainderWithInfo:sender withDict:[self.remainderArray objectAtIndex:deleteIndex]];

}

- (IBAction)addRemainderBtn:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isEditReminder"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self.remaindersViewDelegate gotoDetailRemainder:sender];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        // Fast enumerate to pick out the local notification with the correct Date
        for (UILocalNotification *localNotification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
            if ([[localNotification.userInfo valueForKey:@"Date"] isEqualToString: [[self.remainderArray objectAtIndex:deleteIndex] valueForKey:@"Date"]]) {
                [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ; // delete the notification from the system
                break;
            }
        }
        
        [self.remainderArray removeObjectAtIndex:deleteIndex];
        NSString* path = [NSString stringWithFormat:@"%@/Documents/Remender.plist",NSHomeDirectory()];
        [self.remainderArray writeToFile:path atomically: YES];
        [self.remainderTableView reloadData];
    }
    
}

@end
