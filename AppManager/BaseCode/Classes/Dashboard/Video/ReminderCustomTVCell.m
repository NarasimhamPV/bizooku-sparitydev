//
//  ReminderCustomTVCell.m
//  VideoWidget
//
//  Created by Sandeep on 21/10/14.
//  Copyright (c) 2014 com.sparity. All rights reserved.
//

#import "ReminderCustomTVCell.h"

@implementation ReminderCustomTVCell
@synthesize deleteBtn;

- (void)awakeFromNib
{
    // Initialization code
    self.dateLbl.adjustsFontSizeToFitWidth = YES;
    self.titleLbl.adjustsFontSizeToFitWidth = YES;
    [self.titleLbl setFont:[UIFont fontWithName:@"HelveticaNeueLTCom-MdCn" size:16]];
    [self.dateLbl setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:15]];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteBtnClicked:(id)sender
{
    
    [self.remainderCustomViewDelegate removeCellWithSender:sender];
}


@end
