//
//  PopUpView.h
//  NamasteTelengana
//
//  Created by Sanjit shaw on 16/01/13.
//  Copyright (c) 2013 Sanjit shaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>   
#import "ConstantsEDM.h"
#import "DBTilesInfo.h"
#import "DBSponsorWidget.h"
#import "DBProductWidget.h"
#import "DBCouponsWidget.h"



@protocol PopUpViewDelegate <NSObject>

- (void)closePopUpView;

- (void)selectedImageInThePopupView:(id)object;


@end


@interface PopUpView : UIView <UIWebViewDelegate>
{
    
}

@property (nonatomic, retain) UIScrollView *scrlView;
@property (nonatomic, retain) UIImageView *imgvLarge;
@property (nonatomic, retain) UIView *contentView;
@property (nonatomic, retain) UIImageView *imgHeader;
@property (nonatomic, retain) id<PopUpViewDelegate> delegate;
@property (nonatomic, retain) DBTilesInfo *tileInfo;
@property (nonatomic, retain) NSArray *arrSponsors;
@property (nonatomic, retain) DBSponsorWidget *selectedSponsor;
@property (nonatomic, retain) DBProductWidget *selectedProduct;
@property (nonatomic, retain) DBCouponsWidget *selectedCoupon;

@property (nonatomic, readwrite) BOOL isFirstTime;

@property (nonatomic, readwrite) AppType appType;






@property (nonatomic, assign) TileType tileType;

-(void)setWebViewwithframe:(CGRect)frame URL:(NSString*)strURL;

-(void)setImageViewWithImagePath:(NSString*)imagePath title:(NSString*)title;

- (void)setContentViewWithFrame:(CGRect)frame;

-(void)setScrollViewwithFrame:(CGRect)frame withTile:(DBTilesInfo *)tileInfo withSelectedIndex:(int)selectedIndex isClickable:(BOOL)isClickable targetForClickableSelector:(id)target selectorOnClick:(SEL)selectorToCalled;

@end
