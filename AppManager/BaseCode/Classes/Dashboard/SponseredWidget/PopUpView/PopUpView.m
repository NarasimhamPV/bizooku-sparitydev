//
//  PopUpView.m
//  NamasteTelengana
//
//  Created by Sanjit shaw on 16/01/13.
//  Copyright (c) 2013 Sanjit shaw. All rights reserved.
//

#import "PopUpView.h"
#import "UIImageView+WebCache.h"
#import "UILabel+Exceedings.h"
#import "AboutUsVC.h"
#import "ViewController.h"
#import "DBTilesInfo.h"
#import "DBTileSize.h"
#import "DBApp.h"

#import "AppDelegate.h"




int padding = 10;


@implementation PopUpView

@synthesize tileType = _tileType;
@synthesize delegate = _delegate;
@synthesize arrSponsors = _arrSponsors;
@synthesize tileInfo = _tileInfo;
@synthesize selectedSponsor = _selectedSponsor;
@synthesize isFirstTime = _isFirstTime;
@synthesize selectedCoupon;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
}
*/




- (void)setContentViewWithFrame:(CGRect)frame
{
    self.contentView = [[[UIView alloc] initWithFrame:frame] autorelease];
    
    _contentView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
    _contentView.autoresizesSubviews = YES;
    _contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    _contentView.layer.cornerRadius = 5.0f;
    _contentView.backgroundColor = [UIColor blackColor];
    
    _contentView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _contentView.layer.borderWidth = 1.0f;
    
    /// Image header.....
    
    self.imgHeader = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 40)] autorelease];
    _imgHeader.image = [UIImage imageNamed:@"topHeaderStrip.png"];
    [_contentView addSubview:_imgHeader];
    
    UIImageView *imgView = [[[UIImageView alloc] initWithFrame:self.contentView.frame] autorelease];
    imgView.image = [UIImage imageNamed:@"topHeaderStrip.png"];
    [_contentView addSubview:imgView];
    
    
    
    UILabel *lbl = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 40)] autorelease];
    lbl.textAlignment = NSTextAlignmentCenter;
    if(_appType == APP_PRODUCTS)
    {
        lbl.text = @"Our Sponsors";
    }
    else if(_appType == APP_PRODUCTS)
    {
        lbl.text = @"Our Products";
    }
    [lbl sizeLabelForSubTitle];
    [lbl setTextColor:[UIColor whiteColor]];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [_contentView addSubview:lbl];
    
    //// Cross button item....
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(300-30.0, 5.0, 30.0, 30.0);
    btn.backgroundColor = [UIColor clearColor];
    [btn setImage:[UIImage imageNamed:@"close_popup.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(crossButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    btn.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:btn];
    self.imgvLarge = [[[UIImageView alloc] init] autorelease];
    
    // ImageView Tap Action....
    UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] autorelease];
    tapRecognizer.numberOfTouchesRequired = 1.0;
    tapRecognizer.numberOfTapsRequired = 1.0;
    _imgvLarge.userInteractionEnabled = YES;
    [_imgvLarge addGestureRecognizer:tapRecognizer];
    _imgvLarge.autoresizesSubviews = NO;
    [_contentView addSubview:_imgvLarge];
    
    UIScrollView *scrl = [[[UIScrollView alloc] init] autorelease];
    scrl.scrollEnabled = YES;
    self.scrlView = scrl;
    
    [self configureAllSubViewFrames];
    UIImageView *subView = [[UIImageView alloc] initWithFrame:CGRectMake(_scrlView.frame.origin.x-5, _scrlView.frame.origin.y, _scrlView.frame.size.width + 10, _scrlView.frame.size.height)];
    subView.image = [UIImage imageNamed: @"scroll_bg.png"];
    [self.contentView addSubview:subView];
    [subView release];
    _scrlView.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_scrlView];
    [self addSubview:_contentView];
}

- (void)imageTapped:(id)sender
{
    if(_selectedSponsor)
    {
        [self.delegate selectedImageInThePopupView:_selectedSponsor];
    }
    else if(_selectedProduct)
    {
        [self.delegate selectedImageInThePopupView:_selectedProduct];
    }
    else if(selectedCoupon)
    {
        [self.delegate selectedImageInThePopupView:selectedCoupon];
    }
}

- (void)configureAllSubViewFrames
{
    switch (_tileType) {
        case TILE_TYPE_145X115:
        {
            _imgvLarge.frame = CGRectMake((300-145)/2,  _imgHeader.frame.size.height + padding, 145, 115);
             _scrlView.frame = CGRectMake(20, self.imgHeader.frame.size.height+padding+self.imgvLarge.frame.size.height+2*padding, self.imgHeader.frame.size.width-40, [self innerImageHeight] + padding);
        }
            break;
        case TILE_TYPE_145X240:
        {
            _imgvLarge.frame = CGRectMake((300-145)/2, _imgHeader.frame.size.height + padding, 145, 240);
            _scrlView.frame = CGRectMake(20, self.imgHeader.frame.size.height+padding+self.imgvLarge.frame.size.height+1.5*padding, self.imgHeader.frame.size.width-40, [self innerImageHeight] + padding);
            
        }
            break;
        case TILE_TYPE_300X115:
        {
            _imgvLarge.frame = CGRectMake(0,  _imgHeader.frame.size.height + padding, 300, 115);
            _scrlView.frame = CGRectMake(20, self.imgHeader.frame.size.height+padding+self.imgvLarge.frame.size.height+1.5*padding, self.imgHeader.frame.size.width-40, [self innerImageHeight] + padding);
            
        }
            break;
        case TILE_TYPE_300X240:
        {
            _imgvLarge.frame = CGRectMake(0,  _imgHeader.frame.size.height + padding, 300, 240);
            _scrlView.frame = CGRectMake(20, self.imgHeader.frame.size.height+padding+self.imgvLarge.frame.size.height+2*padding, self.imgHeader.frame.size.width-40, [self innerImageHeight] + padding);
        }
            break;
            
        default:
            break;
    }
}


- (void)crossButtonClicked:(id)sender
{
    if(_contentView)
    {
        for(UIButton *btn in [_contentView subviews])
        {
            btn.hidden = YES;
        }
    }
    [self.delegate closePopUpView];
}


-(void)setWebViewwithframe:(CGRect)frame URL:(NSString*)strURL
{
    UIWebView *webview = [[[UIWebView alloc] initWithFrame:frame] autorelease];
    webview.delegate = self;
    
    NSURLRequest *urlRequest = [[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:strURL]] autorelease];
    
    webview.layer.cornerRadius = 4.0;
    webview.clipsToBounds = YES;
    webview.layer.borderWidth = 1.0;
    webview.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [webview loadRequest:urlRequest];
    webview.scalesPageToFit = YES;
    [_contentView addSubview:webview];
}

-(void)setImageViewWithImagePath:(NSString*)imagePath title:(NSString*)title
{
    if(_imgvLarge)
    {
        _imgvLarge.autoresizesSubviews = NO;
        
        if ([imagePath length] > 0)
        {
            _imgvLarge.image = [UIImage imageWithContentsOfFile:imagePath];
        }
        
        _imgvLarge.contentMode = UIViewContentModeScaleToFill;
    }
    //[_contentView addSubview:imgView];
}

- (CGFloat) innerImageWidth
{
    switch (_tileType) {
        case TILE_TYPE_145X115:
        {
            return 145.0/2.0;
        }
            break;
        case TILE_TYPE_145X240:
        {
            return 145.0/3.0;
        }
            break;
        case TILE_TYPE_300X115:
        {
            return 300.0/2;
        }
            break;
        case TILE_TYPE_300X240:
        {
            return 300.0/4.0;
        }
            break;
            
        default:
            break;
    }
}


- (CGFloat) innerImageHeight
{
    switch (_tileType) {
        case TILE_TYPE_145X115:
        {
            return 120.0/2.0;
        }
            break;
        case TILE_TYPE_145X240:
        {
            return 240.0/3.0;
        }
            break;
        case TILE_TYPE_300X115:
        {
            return 120.0/2;
        }
            break;
        case TILE_TYPE_300X240:
        {
            return 240.0/4.0;
        }
            break;
            
        default:
            break;
    }
}


- (void)refreshScrollWithSelectedIndex:(int)selectedIndex WithTile:(DBTilesInfo *)tile click:(BOOL)isClickable withAction:(SEL)selectorToCalled
{
    CGFloat x = 0.0;
    
    CGFloat grid_height = [self innerImageHeight];
    CGFloat grid_width = [self innerImageWidth];
    
    CGFloat diff = 15.0;
    
    
    int checkIndex = 0;
    
    for (id object in /*tile.widget.sponsor*/ _arrSponsors)
    {
        checkIndex++;
        CGRect imageViewFrame = CGRectMake(x + diff, padding/2.0, grid_width, grid_height);
        
        UIImageView *imgView = (UIImageView *)[_scrlView viewWithTag:checkIndex+5.0];
        
        if(imgView == nil)
        {
            imgView = [[[UIImageView alloc] initWithFrame:imageViewFrame] autorelease];
            imgView.tag = checkIndex + 5.0;
            if (isClickable)
            {
                UITapGestureRecognizer *tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self action:selectorToCalled] autorelease];
                tapGesture.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tapGesture];
            }
            
            [_scrlView addSubview:imgView];
        }
        
        
        
        
        NSString *strURL = nil;
        if([object isKindOfClass:[DBSponsorWidget class]])
        {
            strURL = [self imgSponsorPath:tile withSponsor:object];
        }
        else if([object isKindOfClass:[DBProductWidget class]])
        {
            strURL = [self imgProductsPath:tile withProducts:object];
        }
        
        // [imgView setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:nil];
        imgView.image = [UIImage imageWithContentsOfFile:strURL];
        
        if(selectedIndex == checkIndex-1)
        {
            imgView.layer.borderWidth = 2.0;
            imgView.layer.borderColor = [UIColor redColor].CGColor;
            NSLog(@"The selected index is : %d", selectedIndex);
        }
        else{
            imgView.layer.borderWidth = 2.0;
            imgView.layer.borderColor = [UIColor blackColor].CGColor;
        }
        
        //  imgView.image = [UIImage imageNamed:@"Icon.png"];
        // imgView.contentMode = UIViewContentModeScaleAspectFit;
        x = x + grid_width + diff;
        
    }
    
    if(_isFirstTime && _scrlView)
    {
        _scrlView.contentSize = CGSizeMake(x+diff, 0.0);
        [_contentView addSubview:_scrlView];
        self.isFirstTime = NO;
//        [_scrlView setContentOffset:CGPointMake((selectedIndex-1)*(grid_width+diff/2) + grid_width, 0.0) animated:YES];
       
        CGFloat contentXVal = (selectedIndex-1)*(grid_width+diff/2) - (grid_width/2);
         NSLog(@"The selected index value is :%d ---> offset :%f", selectedIndex,contentXVal);
        if(contentXVal < 0)
        {
            contentXVal = 0.0;
        }
        [_scrlView setContentOffset:CGPointMake(contentXVal, 0.0) animated:YES];
        
    }
    
}


- (NSString*)imgSponsorPath:(DBTilesInfo*)tileInfo withSponsor:(DBSponsorWidget *)sponsor{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"sponsor%dX%dX%@_%@",[tileInfo.tileSize.width integerValue],[tileInfo.tileSize.height integerValue], sponsor.sponsorId, [sponsor.tileImage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    
   // NSLog(@"Path for sponsor images %@: %@", tileInfo.brandTileId, path);
    return path;
}


- (NSString*)imgProductsPath:(DBTilesInfo*)tileInfo withProducts:(DBProductWidget *)product{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"product%dX%dX%@_%@",[tileInfo.tileSize.width integerValue],[tileInfo.tileSize.height integerValue], product.productId, [product.productImage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    
    //  NSLog(@"Path for sponsor images %@: %@", tileInfo.brandTileId, path);
    return path;
}



-(void)setScrollViewwithFrame:(CGRect)frame withTile:(DBTilesInfo *)tileInfo withSelectedIndex:(int)selectedIndex isClickable:(BOOL)isClickable targetForClickableSelector:(id)target selectorOnClick:(SEL)selectorToCalled
{
    
    [self refreshScrollWithSelectedIndex:selectedIndex WithTile:tileInfo click:isClickable withAction:selectorToCalled];
    
    CGFloat btnYposition = _scrlView.frame.origin.y;
    
    // generate the left & right arrow options....
    
    UIButton *btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.backgroundColor = [UIColor clearColor];
    [btnLeft setImage:[UIImage imageNamed:@"left_move.png"] forState:UIControlStateNormal];
    [btnLeft setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnLeft addTarget:self action:@selector(moveRight:) forControlEvents:UIControlEventTouchUpInside];
    btnLeft.frame = CGRectMake(0, btnYposition, 20.0, _scrlView.frame.size.height);
    [_contentView addSubview:btnLeft];
    
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setBackgroundImage:[UIImage imageNamed:@"right_move.png"] forState:UIControlStateNormal];
    btnRight.backgroundColor = [UIColor clearColor];
    [btnRight setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnRight addTarget:self action:@selector(moveLeft:) forControlEvents:UIControlEventTouchUpInside];
    btnRight.frame = CGRectMake(self.contentView.frame.size.width - 20.0, btnYposition, 20.0, _scrlView.frame.size.height);
    [_contentView addSubview:btnRight];
    
    // Swipe action to the scroll view.....
    
  /*  UISwipeGestureRecognizer *recognizerLeft = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeft:)] autorelease];
    recognizerLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    recognizerLeft.numberOfTouchesRequired = 1.0;
    [_scrlView addGestureRecognizer:recognizerLeft];
    
    UISwipeGestureRecognizer *recognizerRight = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRight:)] autorelease];
    recognizerRight.direction = UISwipeGestureRecognizerDirectionRight;
    recognizerRight.numberOfTouchesRequired = 1.0;
    [_scrlView addGestureRecognizer:recognizerRight];
   */
}



-(void)navigatetoLargerImage:(id)sender
{
    NSLog(@"Called....");
    
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer*)sender;
    UIImageView *holderImageview = (UIImageView*)[tapGesture view];
    NSLog(@"image data %@ --- the index value is : %d", holderImageview.image, holderImageview.tag-5);
    
    if([_tileInfo.widget.widgetName isEqualToString:kTypeSponsor])
    {
        self.selectedSponsor = [_arrSponsors objectAtIndex:holderImageview.tag - 6];
        [self setImageViewWithImagePath:[self imgSponsorPath:_tileInfo withSponsor:_selectedSponsor] title:_selectedSponsor.title];
    }
    else if([_tileInfo.widget.widgetName isEqualToString:kTypeProducts])
    {
        self.selectedProduct = [_arrSponsors objectAtIndex:holderImageview.tag - 6];
        [self setImageViewWithImagePath:[self imgProductsPath:_tileInfo withProducts:_selectedProduct] title:_selectedProduct.productName];
    }
    else if([_tileInfo.widget.widgetName isEqualToString:kTypeCouponesNew])
    {
        self.selectedProduct = [_arrSponsors objectAtIndex:holderImageview.tag - 6];
        [self setImageViewWithImagePath:[self imgProductsPath:_tileInfo withProducts:_selectedProduct] title:_selectedProduct.productName];
    }
  
    
    [self refreshScrollWithSelectedIndex:holderImageview.tag-6 WithTile:_tileInfo click:YES withAction:@selector(navigatetoLargerImage:)];
    
}


-(void)moveLeft:(id)sender
{    

    
   /* CGFloat grid_width = [self innerImageWidth];
    float diff = 24.0;
   
    if ((_scrlView.contentSize.width - (_scrlView.contentOffset.x + grid_width) - 65.0 - diff) > 0.0)
    {
        CGPoint offsetPoint = _scrlView.contentOffset;
        offsetPoint.x += grid_width;
        [_scrlView setContentOffset:offsetPoint animated:YES];
        [self updateLargeImageViewforOffsetpoint:offsetPoint];
    }
    */
    NSLog(@"moveLeft : %f & content size : %f", _scrlView.contentOffset.x, _scrlView.contentSize.width);
    
    CGFloat grid_width = [self innerImageWidth];
    
    if(_scrlView.contentOffset.x < (_scrlView.contentSize.width - 280.0))
    {
        CGPoint offsetPoint = _scrlView.contentOffset;
        offsetPoint.x = (_scrlView.contentSize.width - 270.0) - _scrlView.contentOffset.x;
        [_scrlView setContentOffset:offsetPoint animated:YES];
        [self updateLargeImageViewforOffsetpoint:offsetPoint];
    }
    
    /*float diff = 15.0;
    
    if ((_scrlView.contentSize.width - (_scrlView.contentOffset.x + grid_width) - diff + 65.0) > 0.0 + _scrlView.frame.size.width)
    {
        CGPoint offsetPoint = _scrlView.contentOffset;
        offsetPoint.x += (grid_width+diff);
        [_scrlView setContentOffset:offsetPoint animated:YES];
        [self updateLargeImageViewforOffsetpoint:offsetPoint];
    }
     */
}

-(void)moveRight:(id)sender
{
    NSLog(@"moveRight : %f && content size : %f",_scrlView.contentOffset.x, _scrlView.contentSize.width);
    
    float diff = 15.0;
   /* if (_scrlView.contentOffset.x > 0.0)
    {
        CGPoint offsetPoint = _scrlView.contentOffset;
        offsetPoint.x -= ([self innerImageWidth] +diff);
        [_scrlView setContentOffset:offsetPoint animated:YES];
        [self updateLargeImageViewforOffsetpoint:offsetPoint];
    }
    */
    
    if (_scrlView.contentOffset.x > 0.0)
    {
        CGPoint offsetPoint = _scrlView.contentOffset;
        offsetPoint.x = 0.0;
        [_scrlView setContentOffset:offsetPoint animated:YES];
        [self updateLargeImageViewforOffsetpoint:offsetPoint];
    }
    
}

-(void)updateLargeImageViewforOffsetpoint :(CGPoint)offsetPoint
{
    for (UIView *view in [_scrlView subviews])
    {
        if (view.frame.origin.x - 15.0 == offsetPoint.x)
        {
            UIImageView *tempImgView = (UIImageView*)view;
            
            UIImage *image = tempImgView.image;
            _imgvLarge.image = image;
            break;
        }
    }
}


@end
