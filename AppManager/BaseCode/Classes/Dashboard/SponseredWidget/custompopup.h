//
//  custompopup.h
//  ImageListViewer
//
//  Created by Sanjit shaw on 31/01/13.
//  Copyright (c) 2013 Sanjit shaw. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol customPopUpDelegate <NSObject>
-(void)handlePopUpAction;
@end

@interface custompopup : NSObject
{
    id<customPopUpDelegate>delegate;
    UIButton *First_Btn;
}

@property(retain)id delegate;

@end
