//
//  custompopup.m
//  ImageListViewer
//
//  Created by Sanjit shaw on 31/01/13.
//  Copyright (c) 2013 Sanjit shaw. All rights reserved.
//

#import "custompopup.h"

@implementation custompopup

@synthesize delegate;

-(UIView *)initwithTitleText:(NSString *)titleText  withdelegate:(id)del
//returns uiview   to viewcontroller
{
    self.delegate =del;
    UIView *customView = [[UIView alloc] init];
    customView.frame = CGRectMake(10, 10, 100,200);
    
    First_Btn =[UIButton buttonWithType:UIButtonTypeRoundedRect];
    First_Btn.frame=CGRectMake(20, 30,125,45);
    [First_Btn setTitle:@"title" forState:UIControlStateNormal];
    [First_Btn addTarget:self  action:@selector(popUpAction) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:First_Btn];
    
    return customView;
}

-(void)popUpAction
{
    [[self delegate] handlePopUpAction];
}

@end
