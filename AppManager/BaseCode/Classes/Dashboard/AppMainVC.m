//
//  AppMainVC.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppMainVC.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Utils.h"
#import "TableObject.h"
#import "NSString+Exceedings.h"
#import "PhoneNumberFormatter.h"
#import "NSDateFormatter+Exceedings.h"


@interface NSArray (Exceedings)
-(NSArray*)tableObjects;
-(NSArray*)sortedArrayFromLocation:(CLLocation*)location ascending:(BOOL)ascending;
@end


@implementation NSArray (Exceedings)




-(NSArray*)tableObjects{
    
    NSMutableArray *mutableArray = [NSMutableArray array];
    for (NSDictionary *dic in self) {
        
        NSLog(@"dtableObjectsic %@",dic);
        TableObject *obj = [[[TableObject alloc] init] autorelease];
        obj.name = [dic valueForKey:@"Name"];
        obj.locationName = [dic valueForKey:@"LocationName"];
        obj.desc = [dic valueForKey:@"Description"];
        obj.zip = [dic valueForKey:@"Zip"];
        obj.state = [dic valueForKey:@"State"];
        obj.eventCategoryName = [dic valueForKey:@"CategoryName"];//
        obj.eventCategoryId = [dic valueForKey:@"CategoryId"];
        obj.titleText = [dic valueForKey:@"Title"];
        obj.newsID = [NSString stringWithFormat:@"%@", [dic valueForKey:@"NewsId"]] ;
        obj.eventID = [NSString stringWithFormat:@"%@", [dic valueForKey:@"EventId"]] ;
        // rajesh..
        obj.locId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"LocationId"]];
        obj.productId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"ProductId"]];
        NSDateFormatter *format = [NSDateFormatter exceedingEventsPullTimeFormatter];
        
        obj.startTime = [format dateFromString:[dic valueForKey:@"StartTime"]];
        
        NSLog(@"The current date is: %@", [format stringFromDate:[NSDate date]]);
        
        obj.endTime = [format dateFromString:[dic valueForKey:@"EndTime"]];
        
        obj.startTimeStr = [[[[dic valueForKey:@"StartTime"] 
                            stringByReplacingOccurrencesOfString:@" PM" withString:@"pm"] 
                            stringByReplacingOccurrencesOfString:@" AM" withString:@"am"]
                             stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        obj.endTimeStr = [[[[dic valueForKey:@"EndTime"] 
                              stringByReplacingOccurrencesOfString:@" PM" withString:@"pm"] 
                             stringByReplacingOccurrencesOfString:@" AM" withString:@"am"]
                            stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        obj.fbPostID = [dic valueForKey:@"FBPostId"];
        
        NSString *image = [dic valueForKey:@"Image"];
        
        if (image && image.length) {
            obj.image = [NSString stringWithFormat:@"%@%@", kWebServiceURL, [dic valueForKey:@"Image"]];
        }

        obj.location =  [[[CLLocation alloc] initWithLatitude:[[dic valueForKey:@"Lattitude"] doubleValue] longitude:[[dic valueForKey:@"Longitude"] doubleValue]] autorelease];
        obj.lattitude = [dic valueForKey:@"Lattitude"];
        obj.longitude = [dic valueForKey:@"Longitude"];
        
        obj.address = [dic valueForKey:@"Address"];
        
        obj.city = [dic valueForKey:@"City"];
        obj.fax = [dic valueForKey:@"Fax"];
        obj.primaryEmail = [dic valueForKey:@"PrimaryEmailAddress"];
        
        PhoneNumberFormatter *phoneNumberFormatter = [[[PhoneNumberFormatter alloc] init] autorelease];
        obj.phone = /*[dic valueForKey:@"Phone"]; */ [phoneNumberFormatter format:[dic valueForKey:@"Phone"] withLocale:@"us"];
        obj.additonalInfo = [dic valueForKey:@"AdditonalInfo"];
        obj.website = [dic valueForKey:@"Website"];
        
        
        NSString *startDate = [dic valueForKey:@"StartDate"];
        obj.startDate = [startDate exceedingsDateFromServerNewsString];
        
        NSString *endDate = [dic valueForKey:@"EndDate"];
        obj.endDate = [endDate exceedingsDateFromServerNewsString];
        
        NSString *date = [dic valueForKey:@"CreatedDate"];
        if(date)
        {
            obj.date = [date exceedingsDateFromServerNewsString];
        }
        [mutableArray addObject:obj];
    }
    
    return [NSArray arrayWithArray:mutableArray];
}

-(NSArray*)sortedArrayFromLocation:(CLLocation*)location ascending:(BOOL)ascending{
    //NSMutableArray *newArray = [NSMutableArray arrayWithArray:self];
    
    for (TableObject *obj in self) {
        obj.distance = [obj.location distanceFromLocation:location];
     
    }
    
    NSSortDescriptor * sortByFrequency = [[[NSSortDescriptor alloc] initWithKey:@"distance" ascending:ascending] autorelease];
    return [self sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortByFrequency]];
    return [NSArray arrayWithArray:self] ;
}

-(NSArray*)sortedArrayForEventsFromLocation:(CLLocation*)location ascending:(BOOL)ascending{
    //NSMutableArray *newArray = [NSMutableArray arrayWithArray:self];
    
    for (TableObject *obj in self) {
        CLLocation *locEvent = [[CLLocation alloc] initWithLatitude:[obj.lattitude doubleValue] longitude:[obj.longitude doubleValue]];
        obj.distance = [locEvent distanceFromLocation:location];
        
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
    NSSortDescriptor *sortTimeDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES];
    
    NSSortDescriptor * sortByFrequency = [[[NSSortDescriptor alloc] initWithKey:@"distance" ascending:ascending] autorelease];
    
    
    NSArray *arrSorted = [self sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortByFrequency]];
    
    //  self = [NSArray arrayWithArray:arrSorted];
    NSArray *arrTemp = [arrSorted sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor, sortTimeDescriptor,nil]];
    

    return arrTemp;
    
    
    
    return [NSArray arrayWithArray:self] ;
}



@end

@interface AppMainVC ()

@end

@implementation AppMainVC

@synthesize headerObject = _headerObject;
@synthesize dataSourceDic, dataSourceArray;
@synthesize appType;
@synthesize tabType;
@synthesize subTabOptions;
@synthesize selectedTab = _selectedTab;
@synthesize subTabBtnArray = _subTabBtnArray;
@synthesize appDetailVC = _appDetailVC;
@synthesize socialASheet = _socialASheet;
@synthesize searchDataSourceArray = _searchDataSourceArray;
@synthesize itemId = _itemId;
@synthesize widgetTileInfo = _widgetTileInfo;
@synthesize widgetConstantType = _widgetConstantType;


#pragma mark - App Header Delegate

- (IBAction)headerSecondBtnAction:(UIButton *)sender
{
    if (sender == self.headerObject.btnShare) {
        self.socialASheet = [[[SocialActionSheet alloc] initWithDelegate:self] autorelease];
        _socialASheet.appType = appType;
        _socialASheet.widgetInfo = _widgetTileInfo;
        
        if(appType == APP_SPONSOR)
        {
            _socialASheet.itemId = _itemId;
        }
        else{
            _socialASheet.itemId = [NSNumber numberWithInt:0];
        }
        
        
        if(self.appType == APP_SPONSOR && self.dataSourceDic)
        {
            _socialASheet.detailDictionary = self.dataSourceDic;
        }
        [_socialASheet presentSocialActionSheet:YES];
    }
}
-(IBAction)headerBtnAction:(UIButton*)sender{
    
    
    if (sender == self.headerObject.btnShare) {
        self.socialASheet = [[[SocialActionSheet alloc] initWithDelegate:self] autorelease];
        _socialASheet.appType = appType;
        _socialASheet.widgetInfo = _widgetTileInfo;
        
        if(appType == APP_SPONSOR)
        {
            _socialASheet.itemId = _itemId;
        }
        else{
            _socialASheet.itemId = [NSNumber numberWithInt:0];
        }
        
        
        if(self.appType == APP_SPONSOR && self.dataSourceDic)
        {
            _socialASheet.detailDictionary = self.dataSourceDic;
        }
        [_socialASheet presentSocialActionSheet:YES];
    }
    else if(sender == self.headerObject.btnLocation)
    {
       /* if(_isLocationSortEnabled)
        {
            self.isLocationSortEnabled = NO;
            [self.headerObject.btnLocation setSelected:NO];
            
        }
        else{
            self.isLocationSortEnabled = YES;
            [self.headerObject.btnLocation setSelected:YES];
        }*/
    }
    
    
   /* if (self.appType == APP_EVENTS && _isLocationSortEnabled) {
        self.dataSourceArray = [self.dataSourceArray sortedArrayForEventsFromLocation:[AppDelegate shareddelegate].currentLocation ascending:YES];
    }
    else if (self.appType == APP_EVENTS && !_isLocationSortEnabled) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:NO];
        self.dataSourceArray = [self.dataSourceArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    }*/
    
    


    
}

-(void)socialActionSheetDidDismiss:(SocialActionSheet*)actionSheet{
    [self.socialASheet removeFromSuperview];
    self.socialASheet = nil;
}

-(AppType)appTypeForView:(id)sender{
    return appType;
}

#pragma mark - Custom

-(void)setDataSourceDic:(NSDictionary *)newdataSourceDic{
    if (self.dataSourceDic) {
        [self.dataSourceDic release];
    }
//    NSMutableArray *tempArray = [[NSMutableArray alloc] init];

    dataSourceDic = [newdataSourceDic retain];
        
    if (self.dataSourceDic && [self.dataSourceDic isKindOfClass:[NSDictionary class]]) {
        self.dataSourceArray = [self.dataSourceDic allValues];
    }
    else if (self.dataSourceDic && [self.dataSourceDic isKindOfClass:[NSArray class]]) {
        self.dataSourceArray = (NSArray*)self.dataSourceDic;
        //self.dataSourceArray = (NSArray*)self.dataSourceDic;
//        [tempArray addObjectsFromArray:(NSMutableArray*)self.dataSourceDic];
//        NSLog(@"tempArray %d",[tempArray count]);
//        [self.searchDataSourceArray setArray:tempArray];
        NSLog(@"dataSourceArray %d",[self.dataSourceArray count]);

        //[self.dataSourceArray addObjectsFromArray:(NSMutableArray*)self.dataSourceDic];

    }
    else {
        self.dataSourceArray = [NSArray array];
    }
    
    //Locations obj
    if ((self.appType == APP_LOCATIONS || self.appType == APP_EVENTS) && !_isLocationSortEnabled) {
        self.dataSourceArray = [dataSourceArray tableObjects];
        
        if (self.appType == APP_LOCATIONS) {
            self.dataSourceArray = [self.dataSourceArray sortedArrayFromLocation:[AppDelegate shareddelegate].currentLocation ascending:YES];
            NSLog(@"sorted Array %@",self.dataSourceArray );
        }
        else if (self.appType == APP_NEWS || self.appType == APP_PRODUCTS) {
            NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:NO];
            self.dataSourceArray = [self.dataSourceArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            NSLog(@"AppMain Vc %@",self.dataSourceArray);
        }
        else if(self.appType == APP_EVENTS)
        {
            // NSLog(@"The sorted array is : %@", self.dataSourceArray);
            if(_isLocationSortEnabled)
            {
            NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
            NSSortDescriptor *sortTimeDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES];
            self.dataSourceArray = [self.dataSourceArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortTimeDescriptor,nil]];
            }
            else{
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES];
                NSSortDescriptor *sortTimeDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES];
                self.dataSourceArray = [self.dataSourceArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor,sortTimeDescriptor,nil]];
                NSLog(@"self.dataSourceArray %@",self.dataSourceArray);
            }
        }
    }
    NSLog(@"self.dataSourceArray %@",self.dataSourceArray);
    [self.searchDataSourceArray addObjectsFromArray:(NSMutableArray*)self.dataSourceArray];
    //self.searchDataSourceArray = [NSMutableArray arrayWithArray:self.dataSourceArray];


    NSLog(@"self.searchDataSourceArray %d",[self.searchDataSourceArray count]);
}


#pragma mark - View Life
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
   // [GoogleAnalyticsManager logWidgetVisit:self.appType];
}

- (NSMutableArray *)subTabOptionsArray
{
    NSMutableArray *arrSubTabs = [NSMutableArray array];
    
    if(([AppDelegate shareddelegate].appReference.podcYouTubeChannelName) && (![[AppDelegate shareddelegate].appReference.podcYouTubeChannelName isEqualToString:@""]))
    {
        [arrSubTabs addObject:@"YouTube"];
        
    }
    
    
    
    if(([AppDelegate shareddelegate].appReference.podcVideoURL) && (![[AppDelegate shareddelegate].appReference.podcVideoURL isEqualToString:@""]))
    {
        [arrSubTabs addObject:@"Video"];
    }
    
    
    if(([AppDelegate shareddelegate].appReference.podcAudioURL) && (![[AppDelegate shareddelegate].appReference.podcAudioURL isEqualToString:@""]))
    {
        [arrSubTabs addObject:@"Audio"];
    }

    
    if(arrSubTabs && [arrSubTabs count] == 0)
    {
        [arrSubTabs addObject:kAppNoContent];
    }
    return arrSubTabs;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.

    self.navigationController.navigationBarHidden = YES;
    
    //Load Header View
    
    self.headerObject = [[AppHeaderBar alloc] initWithDelegate:self appType:appType];
    /*
    if(appType == APP_PRODUCTS)
    {
        self.headerObject.headerLabel.text = _pageTitle;
    }
    */
    [self.view addSubview:_headerObject.mainHeader];
    
    _headerObject.subTabsView.frame = CGRectMake(0, kBarHeight, _headerObject.subTabsView.frame.size.width, kSubBarHeight);
    
    NSInteger adjustFac = 2;//Call it 2 words
    
    NSInteger charCount = 0, charUsed = 0;
    
    //Set subTabs
     if (!subTabOptions) {
         switch (appType) {
             case APP_MEDIA:
             {
                 //self.subTabOptions = [NSArray arrayWithObjects:@"Video", @"Audio", @"Youtube",nil];
                 
                self.subTabOptions = [NSArray arrayWithArray:[self subTabOptionsArray]];
                 //[NSArray arrayWithObjects:@"", @"", @"", nil];
             }
                 break;
             case APP_SOCIAL:
             {
                 self.subTabOptions = [NSArray arrayWithObjects:@"Facebook", @"Twitter", nil];
             }
                 break;
                 
             default:
                 break;
         }
     }
   
    if(subTabOptions){
        for (NSString *str in subTabOptions) {
            charCount += ([str length] + adjustFac);
        }
        
        self.subTabBtnArray = [NSMutableArray array];
        
        for (NSInteger i=0; i< [subTabOptions count]; i++ ) {
            NSString *optStr = [subTabOptions objectAtIndex:i];
            NSInteger optCharCount = ([optStr length] + adjustFac);
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            //btn.tag = i;
            if([[subTabOptions objectAtIndex:i] isEqualToString:@"Audio"])
            {
                btn.tag = 2;
            }
            else if([[subTabOptions objectAtIndex:i] isEqualToString:@"Video"])
            {
                btn.tag = 1;
            }
            else if([[subTabOptions objectAtIndex:i] isEqualToString:@"YouTube"])
            {
                btn.tag = 0;
            }
            else if([[subTabOptions objectAtIndex:i] isEqualToString:@"Facebook"])
            {
                btn.tag = 0;
            }
            else if([[subTabOptions objectAtIndex:i] isEqualToString:@"Twitter"])
            {
                btn.tag = 1;
            }
            [btn addTarget:self action:@selector(subTabOptionClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [btn setBackgroundImage:[UIImage imageNamed:@"subTabBG_normal"] forState:UIControlStateNormal];
            if (appType == APP_SOCIAL) {
                [btn setBackgroundImage:[UIImage imageNamed:@"subTabBG_sel"] forState:UIControlStateSelected];
            }
            else{
                [btn setBackgroundImage:[UIImage imageNamed:@"subTabBG2_sel"] forState:UIControlStateSelected];
            }
            
            //Appraoch 1: Division based on chars
            //[btn setFrame:CGRectMake((charUsed*1.0/charCount) *_headerObject.subTabsView.frame.size.width, 0, (optCharCount*1.0/charCount) *_headerObject.subTabsView.frame.size.width, kSubBarHeight)];
            
            //Approach 2: Equal division of subtab options
            [btn setFrame:CGRectMake((i*1.0/[subTabOptions count]) *_headerObject.subTabsView.frame.size.width, 0, (1.0/[subTabOptions count]) *_headerObject.subTabsView.frame.size.width, kSubBarHeight)];
            
            {
                //Button Label custom
                UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, btn.frame.size.width, btn.frame.size.height)] autorelease];
                [label setFont:[UIFont fontWithName:kDefaultFontName size:18]];
                label.textAlignment = NSTextAlignmentCenter;
                label.textColor = [UIColor whiteColor];
                label.text = optStr;
                label.backgroundColor = [UIColor clearColor];
                [btn addSubview:label];
                
            }
            [_headerObject.subTabsView addSubview:btn];
            
            charUsed += optCharCount;
            
            [_subTabBtnArray addObject:btn];
            
        }
        
        [self.view addSubview:_headerObject.subTabsView];
        
    }
}

-(IBAction)subTabOptionClicked:(UIButton*)sender{
    
    //Deselect old Tab
    [_selectedTab setSelected:NO];
    
    //Select New Tab
    self.selectedTab = sender;
    [_selectedTab setSelected:YES];

}


- (void)viewDidUnload
{
    self.socialASheet = nil;
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)leftBtnAction:(id)sender {
    NSLog(@"Navigation Views %@",[self.navigationController viewControllers]);

    int widgetId;
    
    if(!_widgetTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_widgetTileInfo.widget.widgetID intValue];
    }
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    if(appType == APP_SPONSOR)
    {
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId integerValue] widgetType:self.widgetConstantType deviceType:@"iOS"];
    }

    if(appType == APP_AUDIO)
    {
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId integerValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    else{
//        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:0 widgetType:@"Listing" deviceType:@"iOS"];
    }
    if(appType == APP_COUPONS)
    {
        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_itemId integerValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
//    else{
//        [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:0 widgetType:@"Listing" deviceType:@"iOS"];
//    }
//    [self.navigationController popViewControllerAnimated:YES];

    
    [[AppDelegate shareddelegate].viewController dismissPresentedModal:self];
}

- (void)dealloc {
    [super dealloc];
    //self.socialASheet = nil;
}
@end
