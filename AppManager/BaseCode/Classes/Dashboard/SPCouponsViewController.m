
//
//  SPCouponsViewController.m
//  Exceeding
//
//  Created by SREELAKSHMI on 19/02/14.
//
//

#import "SPCouponsViewController.h"
#import "SPCoupnsCell.h"
#import "SDZEXWidgetServiceExample.h"
#import "AppDelegate.h"
#import "SPCouponDetailsViewController.h"
#import "SPCouponsListViewController.h"
#import "SPSingletonClass.h"
#import "CustomBannerView.h"
#import "WebViewController.h"



static NSString *const kcustomCouponsCell = @"couponsCellIdentifier";
static NSString *const kcouponsCellNib = @"SPCoupnsCell";

@interface SPCouponsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *couponesTableView;

@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aboutBannerDict;
@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation SPCouponsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.headerObject.btnSearch.hidden = YES;
    
    [self.couponesTableView registerNib:[UINib nibWithNibName:kcouponsCellNib bundle:nil] forCellReuseIdentifier:kcustomCouponsCell];
    self.couponesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSString *tempStr = [NSString stringWithFormat:@"%@", self.widgetTileInfo.brandid];
    NSString *brandTileID = [NSString stringWithFormat:@"%@", self.widgetTileInfo.brandTileId];
    [[NSUserDefaults standardUserDefaults] setObject:tempStr forKey:@"BrandId"];
    [[NSUserDefaults standardUserDefaults] setObject:brandTileID forKey:@"brandTileId"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveCouponsNotification:)
                                                 name:@"getCouponsNotification"
                                               object:nil];
    
    
    
    self.couponDict = [[NSDictionary alloc] init];
    self.couponsArray = [[NSMutableArray alloc] init];
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runCoupons];
    
    [self entryActionAnalytics];
    
    
    
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.bigBanner = [[BigBanners alloc] init];
    self.bigBanner.bigBannerDelegate = self;

    [self.view bringSubviewToFront:self.searchBtn];
    //Custom Banner View code here
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);
    [self.view addSubview:self.addBannerView];
    
    [self.view bringSubviewToFront:self.addBannerView];
    
    [self addBannerData];
    
    [self reloadTable];
}

- (IBAction)searchBtnAction:(id)sender
{
    
    [self.headerObject.mainHeader addSubview:self.couponsSearchBar];
    [self.view bringSubviewToFront:self.headerObject.mainHeader];
    [self.couponsSearchBar becomeFirstResponder];
    
    
}
- (void)viewWillDisappear:(BOOL)animated // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
{
    [self.addBannerView removeFromSuperview];
}



- (void)receiveCouponsNotification:(NSNotification *) notification
{
    self.couponsArray = [[AppDelegate shareddelegate].couponsdict mutableCopy];
    
    NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"CategoryName" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor,nil];
    
    self.couponsArray = (NSMutableArray *)[self.couponsArray sortedArrayUsingDescriptors:sortDescriptors];
    
    
    
    
    [self reloadTable];
    
    
}
- (void)reloadTable
{
    [self.couponesTableView reloadData];
}

#pragma mark - DataSources

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.couponsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SPCoupnsCell *customCoupnsCell = [tableView dequeueReusableCellWithIdentifier:kcustomCouponsCell];
    

    customCoupnsCell.couponDescriptionLabel.text = [[[self.couponsArray objectAtIndex:indexPath.row] objectForKey:@"CategoryName"] capitalizedString];
    
    customCoupnsCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    return customCoupnsCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 60;
    
}
#pragma mark --
#pragma mark -- Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.couponsSearchBar resignFirstResponder];
    [self.couponsSearchBar removeFromSuperview];
    
    if ([self.couponsArray count] > 0) {
        
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [tempDict setDictionary:[self.couponsArray objectAtIndex:indexPath.row]];
        SPCouponsListViewController *couponListViewCntr = [[SPCouponsListViewController alloc] initWithNibName:@"SPCouponsListViewController" bundle:nil];
        couponListViewCntr.couponListDict = tempDict;
        couponListViewCntr.appType = self.appType;
        [[SPSingletonClass sharedInstance] setCouponTitleStr:[tempDict valueForKey:@"CategoryName"]];
        
        NSString *tempStr = [tempDict valueForKey:@"CategoryId"];
        
        [[NSUserDefaults standardUserDefaults] setObject:tempStr forKey:@"CategoryId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController pushViewController:couponListViewCntr animated:YES];
    }
    
}
#pragma mark --
#pragma mark -- SearchBar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    @synchronized(self){
        
        NSMutableArray *filteredArray = [NSMutableArray array];
        NSString *text = searchText;
        for (NSDictionary *dictionary in self.couponsArray) {
            if ([dictionary isKindOfClass:[NSDictionary class]]) {
                NSString *name = [dictionary valueForKey:@"CategoryName"];
                if (name && ([name rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)) {
                    [filteredArray addObject:dictionary];
                }
            }
        }
        
        self.couponsArray = filteredArray;
        if ([searchText length] == 0) {
            self.couponsArray = [[AppDelegate shareddelegate].couponsdict mutableCopy];
            
            [self reloadTable];
        }else{
            [self.couponesTableView reloadData];
            
        }
        
        
        
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view bringSubviewToFront:self.searchBtn];
    self.couponsArray = [[AppDelegate shareddelegate].couponsdict mutableCopy];
    
    
    [self.couponsSearchBar resignFirstResponder];
    [self.couponsSearchBar removeFromSuperview];
    
    
    [self reloadTable];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [_bannerImg release];
    [_bannerView release];
    [super dealloc];
}
#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *localTempArry = [NSMutableArray array];
    
    
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Coupons"]) {
            self.aboutBannerDict = dict;
            [self.bannerUrlsArray addObjectsFromArray:[[self.aboutBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aboutBannerDict objectForKey:@"BannersList"]];
            break;

        }
        
    }
    //UIViewAnimationOptionAllowUserInteraction
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aboutBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];
            
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aboutBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        //[self.addBannerView startShowingBanners:self.bannerUrlsArray];
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Listing"];
        
    }
}


- (void)customBannerCallbackMethod:(id)withObject
{
    //    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){
        [self.selectedBannerDict setDictionary:withObject];
        
        [self.bigBanner getBannerDictionary:withObject];
        
    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryAction];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    [self.navigationController pushViewController:selectedCntrl animated:YES];
    
}


-(void)entryActionAnalytics
{
    
    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    
    if(!self.widgetTileInfo.widget)
    {
        NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    else{
        
        [example1 runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.widgetTileInfo.widget.widgetID  integerValue] widgetItemId:[self.itemId intValue] widgetType:@"Listing" deviceType:@"iOS"];
    }
    
}

-(void)entryAction
{
    
    NSUserDefaults *ustd = [NSUserDefaults standardUserDefaults];
    
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[[ustd objectForKey:kWidgetId] integerValue] widgetItemId:[itemID integerValue] widgetType:@"Listing" deviceType:@"iOS"];
    
}



@end
