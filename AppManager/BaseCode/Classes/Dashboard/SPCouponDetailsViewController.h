//
//  SPCouponDetailsViewController.h
//  Exceeding
//
//  Created by SREELAKSHMI on 21/02/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "AppMainVC.h"
#import "TableObject.h"
#import "UILabel+Exceedings.h"
#import "InfoCollectorVC.h"
#import <MessageUI/MessageUI.h>
#import "TableObject.h"
#import "SPCouponsListViewController.h"
#import "BigBanners.h"
@interface SPCouponDetailsViewController : AppMainDetailVC <UIActionSheetDelegate, MFMailComposeViewControllerDelegate,redeemImageChange,BigBannersDelegate>
{
}
@property (assign,nonatomic) id <redeemImageChange> couponListDelegate;
@property (retain, nonatomic) IBOutlet UIImageView *redeemImageView;

@property (nonatomic, strong) NSMutableArray *bannerURLS;
@property (strong, nonatomic) NSDictionary *detailbannesDict;
@property (strong, nonatomic) NSString *descriptionStr;
@property (retain, nonatomic) IBOutlet UILabel *clickReedemLbl;
@property (retain, nonatomic) IBOutlet UILabel *barcodeClickReedemLbl;
@property (retain, nonatomic) IBOutlet UIImageView *barcodeImg;


@property (retain, nonatomic) IBOutlet UIImageView *couponDetailImage;
@property (retain, nonatomic) IBOutlet UILabel *couponDetailTitleLabel;
@property (retain, nonatomic) IBOutlet UITextView *couponDetailText;
@property (retain, nonatomic) IBOutlet UIButton *bannerCancelBtn;
@property (retain, nonatomic) IBOutlet UIImageView *bannerImg;
@property (retain, nonatomic) IBOutlet UIImageView *qrlineImg;
@property (retain, nonatomic) IBOutlet UIImageView *barcodeLineImg;
@property (retain, nonatomic) IBOutlet UIButton *redeemBtn;

@property (nonatomic ,strong) UIImage *imageData;

@property (strong, nonatomic) NSDictionary *couponsDetailsDict;
@property (strong, nonatomic) NSArray *couponsDetailsArray;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;
@property (nonatomic, strong) BigBanners *bigBanner;

@end
