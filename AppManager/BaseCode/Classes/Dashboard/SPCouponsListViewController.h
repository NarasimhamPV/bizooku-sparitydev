//
//  SPCouponsListViewController.h
//  Exceeding
//
//  Created by VENKATALAKSHMI on 24/04/14.
//
//

#import <UIKit/UIKit.h>
#import "AppMainDetailVC.h"
#import "SPCouponsViewController.h"
#import "AppMainVC.h"
#import "BigBanners.h"

@protocol redeemImageChange <NSObject>    //sandy

@optional

- (void)changeRedeemImage;

@end

@interface SPCouponsListViewController : AppMainDetailVC<BigBannersDelegate>
{
    BOOL adsremoved;
}
@property (strong,nonatomic) NSArray *couponsListArray;
@property (strong,nonatomic) NSString *tileStr;
@property (strong,nonatomic) NSString *headingString;
@property (strong,nonatomic) NSDictionary *couponListDict;
@property (retain,nonatomic) IBOutlet UIButton *backClickBtn;
@property (nonatomic, strong) BigBanners *bigBanner;


@property (nonatomic, strong) NSMutableArray *bannerURLS;
- (IBAction)backBtn:(id)sender;

@end
