//
//  AppMainVC.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantsEDM.h"
#import "AppHeaderBar.h"
#import "SocialActionSheet.h"
#import "DBTilesInfo.h"
#import "DBWidget.h"


@interface AppMainDetailVC : UIViewController <AppHeaderDelegate>
{
}

@property (nonatomic, retain) IBOutlet AppHeaderBar *headerObject;

//@property (nonatomic, retain) NSArray *dataSourceArray;
@property (nonatomic, retain) NSMutableArray *subTabBtnArray;
@property (nonatomic, retain) NSArray *subTabOptions;//Sub tab option must be set before [super viewDidLoad] call is made to AppMainVC from base class otherwise default options will appear

@property (nonatomic, readwrite) AppType appType;

@property (retain, nonatomic) UIButton *selectedTab;

@property (nonatomic, retain) NSDictionary *detailDataDictionary;
@property (nonatomic, retain) id detailDataObject;

@property (retain, nonatomic) SocialActionSheet *socialASheet;
@property (nonatomic, retain) DBTilesInfo *tileInfo;
@property (retain, nonatomic) NSNumber *itemId;
@property (nonatomic, readwrite) BOOL boolNoListing;


@property (nonatomic,readwrite) BOOL isBackGrdView;

- (IBAction)leftBtnAction:(id)sender;
- (void)reloadUsingWebService;
@end
