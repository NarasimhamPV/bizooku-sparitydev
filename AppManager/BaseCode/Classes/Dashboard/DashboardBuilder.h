//
//  DashboardBuilder.h
//  jefftest
//
//  Created by Rajesh Kumar Yandamuri on 12/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDZEXLayoutServiceExample.h"

@interface DashboardBuilder : NSObject
{
    NSDictionary *appPrefDic;
}


@property (nonatomic, retain) SDZEXLayoutServiceExample *wsdlService;
@property (nonatomic, assign) NSString *refreshMode;

//Init
+(DashboardBuilder*)sharedDelegate;

//Utils
-(CGSize)appSizeForSizeID:(NSString*)sizeID;
-(void)updateUsingWebService;

//fetching all AppTypes....

-(NSString *)appTitle:(NSString *)type; /// return app title
-(NSString *)appURL:(NSString *)type; // retuirn app URL
-(NSString *)appDescription:(NSString *)type; // return app Description


-(NSArray *)fetchAllAppItems; // returns all items information
-(CGSize)fetchIconSizes:(NSString *)sizeType;
-(NSString *)fetchURLForAppType:(NSString *)type;
-(void)updateCoreData:(NSDictionary*)dictionary;


@end
