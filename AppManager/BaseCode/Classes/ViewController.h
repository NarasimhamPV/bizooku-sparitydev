//
//  ViewController.h
//  jefftest
//
//  Created by Rajesh Kumar Yandamuri on 07/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "AppTableVC.h"
#import "AboutUsVC.h"
#import "ContributeVC.h"
#import "AppWebVC.h"
#import "SponsorVC.h"
#import "SPCouponsViewController.h"

#import "DBTilesInfo.h"
#import "SocialActionSheet.h"
#import "InfoCollectorVC.h"
#import "PullToRefreshScrollView.h"
#import "custompopup.h"
#import "PopUpView.h"
#import "AppSubWebVC.h"
#import "ConstantsEDM.h"
#import "ProductsVC.h"

#import "AppMainVC.h"

//AUDIO
#import "FreeAudiosListViewController.h"
#import "AudioDetailViewController.h"
#import "AudioPurchaseViewController.h"
#import "ProductsListVC.h"
#import "SPCouponDetailsViewController.h"
#import "BigBanners.h"

@interface ViewController : UIViewController <UIWebViewDelegate, UIScrollViewDelegate, UIAlertViewDelegate, SocialActionDelegate, InfoCollectorDelegate, PullToRefreshScrollViewDelegate, customPopUpDelegate, PopUpViewDelegate,BigBannersDelegate>
{
    UIView *viPopUp;
    BOOL didCallForPushRegister;
    custompopup *obj_custompopup;
    BOOL boolUpdate;
    
    int indexAnimate1,indexAnimate2,indexAnimate3,indexAnimate4,indexAnimate5,indexAnimate6,indexAnimate7,indexAnimate8,indexAnimate9,indexAnimate10;
}

@property (readwrite, nonatomic) AppType appType;

@property (retain, nonatomic) IBOutlet UIImageView *splashImg;
@property (retain, nonatomic) IBOutlet UILabel *headerTitle;
@property (retain, nonatomic) IBOutlet UIImageView *logoImage;

// For removing the pull down remove PullToRefreshScrollView place UISCrollView

@property (nonatomic, strong) NSArray *bannersArray;


@property (retain, nonatomic) IBOutlet PullToRefreshScrollView *scrollTile;
@property (retain, nonatomic) IBOutlet UIWebView *facebookWeb;
@property (retain, nonatomic) IBOutlet UIView *headerView;
@property (retain, nonatomic) SPCouponsViewController *appCouponsVC;
@property (retain, nonatomic) AppTableVC    *appTableVC;
@property (retain, nonatomic) AboutUsVC     *appAboutUs;
@property (retain, nonatomic) ProductsVC     *appProduct;
@property (retain, nonatomic) SponsorVC     *sponsorVC;
@property (retain, nonatomic) ContributeVC  *appContribute;
@property (retain, nonatomic) AppWebVC      *appWeb;
@property (retain, nonatomic) AppSubWebVC    *appSubWeb;
@property (retain, nonatomic) DBTilesInfo    *loadedTileInfo;

@property (nonatomic, strong) FreeAudiosListViewController *audioListVC;
@property (nonatomic, strong) AudioDetailViewController *audioDetailVC;
@property (nonatomic, strong) AudioPurchaseViewController *audioPurchaseVC;

@property (retain, nonatomic) UINavigationController  *tileBodyNavigation;
@property (assign, nonatomic) UIViewController  *tileBodyVC;


// remove...


@property (nonatomic, retain) DBTilesInfo *sponsorTile;
@property (nonatomic, retain) NSMutableArray *arrSponsoredImagelist;
@property (retain, nonatomic) NSMutableData  *sponseredImageData;
// @property (assign, nonatomic) int indexCount;
@property (nonatomic, readwrite) BOOL isFirstTime;
@property (retain, nonatomic) PopUpView *viewPopUp;
@property (nonatomic, readwrite) BOOL isSponsorStarted;
@property (nonatomic, readwrite) BOOL isProductsStarted;
@property (nonatomic, readwrite) BOOL isCouponsStarted;


@property (nonatomic, readwrite) BOOL isSelectedSponsor;
@property (nonatomic, readwrite) BOOL isSelectedProducts;
@property (nonatomic, readwrite) BOOL isSelectedCoupons;

@property (retain, nonatomic) ProductsListVC *appProductList;
@property (retain, nonatomic) SPCouponDetailsViewController *appCouponDetailVC;

@property (assign, nonatomic) TileType *tileType;
@property (nonatomic, strong) BigBanners *bigBanner;

// My location

//@property (nonatomic, retain) MyCLController *clController;



- (IBAction)actRefresh:(id)sender;

-(void)stopRfreshOfScrollView;

-(IBAction)dismissPresentedModal:(id)sender;

-(IBAction)showPopUp:(id)sender;
-(void)updateAllDashboardGraphics;

-(void)loadDashboardUsingCoreDataEntries;
-(void)downlaodImagesInBG;

-(void)giveupUpdate;


@end
