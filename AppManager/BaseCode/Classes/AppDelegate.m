//
//  AppDelegate.m
//  jefftest
//
//  Created by Rajesh Kumar Yandamuri on 07/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//



#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

#import "ViewController.h"
#import "DashboardBuilder.h"

#import "NSManagedObjectContext-EasyFetch.h"
#import "DBLayoutInfo.h"
#import "Soap.h"
#import "SPSingletonClass.h"


#import "NSString+SBJSON.h"
#import "SDZEXWidgetServiceExample.h"

#import "EventDetailVC.h"

#import "NSFileManagerDoNotBackup.h"
#import "LoginVC.h"
#import "IAPHelper.h"
#import "SPLoginCodeVC.h"



//#import <FacebookSDK/FacebookSDK.h>


#import "FBIbee.h"

#define kAlertiOS5                  @"This functinality requires iOS Version 5.0 or higher"

#define kMailError @"Your device is not currently configured to send mail."
#define kMailBody @""
#define kSMS_NotAvailable @"SMS functionality not available"


@interface AppDelegate ()

@property (nonatomic, assign) UIViewController *socialVCTemp;

@end
@implementation AppDelegate


@synthesize isFirstTime = _isFirstTime;
@synthesize deviceID = _deviceID;
@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize socialVCTemp = _socialVCTemp;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

@synthesize appTitle = _appTitle;
@synthesize appBGImage = _appBGImage;
@synthesize appReference = _appReference;
@synthesize currentLocation = _currentLocation;


@synthesize selectedItemId = _selectedItemId;
@synthesize selectedTileInfo = _selectedTileInfo;

@synthesize locationController = _locationController;
@synthesize bannersDict,productDict,newsListDict,eventsListDict,eventsDetailsDict,eventsCalendarDict;
@synthesize navController;

#pragma mark - Image Paths


 + (UITextView *)resizeTheTextView:(UITextView *)txtView
{
    [txtView setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]];
    [txtView setEditable:NO];
    [txtView setScrollEnabled:NO];
    
    txtView.dataDetectorTypes = UIDataDetectorTypeAddress | UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
    
    CGSize size = [txtView.text sizeWithFont:[UIFont fontWithName:@"Helvetica-Condensed" size:16]
                       constrainedToSize:CGSizeMake(txtView.frame.size.width, CGFLOAT_MAX)
                           lineBreakMode:NSLineBreakByWordWrapping];
    
   // [txtView setFrame:CGRectMake(25,100,200,size.height)];
    CGRect frame = txtView.frame;
    
    frame.size.height = size.height+40;
    txtView.frame = frame;
    
    return txtView;
}

+ (UITextView *)resizeCalenderTextView:(UITextView *)txtView
{
    [txtView setFont:[UIFont fontWithName:@"Avenir-Light" size:16]];
    [txtView setEditable:NO];
    [txtView setScrollEnabled:NO];
    
    txtView.dataDetectorTypes = UIDataDetectorTypeAddress | UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
    
    CGSize size = [txtView.text sizeWithFont:[UIFont fontWithName:@"Avenir-Light" size:17]
                           constrainedToSize:CGSizeMake(txtView.frame.size.width, CGFLOAT_MAX)
                               lineBreakMode:NSLineBreakByWordWrapping];
    
    // [txtView setFrame:CGRectMake(25,100,200,size.height)];
    CGRect frame = txtView.frame;
    
    frame.size.height = size.height+40;
    txtView.frame = frame;
    
    return txtView;
}




-(void)setupAppRef{
    if (!_appReference) {
        NSArray *array = [self.managedObjectContext fetchObjectsForEntityName:kDBApp];
        if ([array count]) {
            self.appReference = [array objectAtIndex:0];
        }
    }
}

-(NSString*)imageBGPath{
    [self setupAppRef];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"appbg_%@", [_appReference.backgroundimage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];;
}

#pragma mark - generic


+ (void) showAlert:(NSString *)msg withTitle:(NSString *)title
{
    UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
}


+ (AppDelegate*)shareddelegate{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

-(void)showAlert:(NSString *)msg
{
    UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
}

+(void)showAlert:(NSString *)msg
{
    UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
}

+ (void)showiOSLimitAlert
{
    UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:kAlertiOS5 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alrt show];
    [alrt release];
}

/*+(void)showErrorAlert:(NSError *)error
{
    [[AppDelegate shareddelegate] performSelectorOnMainThread:@selector(showAlert:) withObject:[NSString stringWithFormat:@"Error %d: %@", [error code], [error localizedDescription]] waitUntilDone:NO];
}*/


+ (void)showNotificationAlert:(NSString *)msg button:(NSString *)btnTitle
{
    UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:msg delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:btnTitle,nil] autorelease];
    [alert show];
}



- (void)showOfflineConnectionAlert
{
    UIAlertView *alrt = [[UIAlertView alloc]initWithTitle:kOfflineErrorTitle message:kOfflineError delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alrt show];
    [alrt release];
}



+(void)showErrorAlert:(NSError *)error
{
    if([[error localizedDescription] isEqualToString:kOfflineError])
    {
        [[AppDelegate shareddelegate] performSelectorOnMainThread:@selector(showOfflineConnectionAlert) withObject:self waitUntilDone:NO];
    
    }
    else{
        [[AppDelegate shareddelegate] performSelectorOnMainThread:@selector(showAlert:) withObject:[NSString stringWithFormat:@"%@",  @"This option is currently not available."] waitUntilDone:NO];
    }
}

// checking response for all the things

/*+(BOOL) validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}*/


/*
+(BOOL) validateEmail: (NSString *) inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot = aRange.location;
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
//            else {
//             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
//             }
 
        }
    }
    return FALSE;
}
*/
// Veeru
+ (BOOL)validateEmail:(NSString *)inputText
{
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:inputText];
}


+(NSDictionary*)isValidSoapResponse:(id)value showAlert:(BOOL)showAlert{
    
    
	// Handle errors
	if([value isKindOfClass:[NSError class]]) {
        NSError *error = (NSError*)value;
        if (showAlert) {
            
            [AppDelegate  showErrorAlert:error];
        }
        
		NSLog(@"Soap Returned Error: %@", value);
		return nil;
	}
    
	// Handle faults
	if([value isKindOfClass:[SoapFault class]]) {
        SoapFault *error = (SoapFault*)value;
        
        if (showAlert) {
            [[AppDelegate shareddelegate] performSelectorOnMainThread:@selector(showAlert:) withObject:[NSString stringWithFormat:@"Soap Error %@: %@", [error faultCode], [error faultString]] waitUntilDone:NO];
        }
       
		NSLog(@"Soap Returned SoapFault: %@", value);
		return nil;
	}
    
    // Do something with the NSString* result
    
    NSString* result = (NSString*)value;
    
    NSDictionary *dic = [result JSONValue];
    if(dic)
    {
        NSString *status = [dic valueForKey:@"Success"];
        
        if ([status isKindOfClass:[NSString class]]) {
            if ([status isEqualToString:@"false"]) {
                if (showAlert) {
                    [AppDelegate showAlert:[NSString stringWithFormat:@"Error: %@", [dic valueForKey:@"ErrorMessage"]]];
                }
                return nil;
            }
        }
        
        if (![dic isKindOfClass:[NSDictionary class]] ) {
            if([value isKindOfClass:[NSString class]]) {
                if ([value isEqualToString:@"Success"]) {
                    return value;
                }
            }
        }
        return dic;
    }
    else{
        if([result isEqualToString:@"Success"])
        {
            if(showAlert)
            {
                 [AppDelegate showAlert:[NSString stringWithFormat:@"%@", @"Thank You for volunteering"]];
            }
            return nil;
        }
    }
    return nil;
   
}

-(void)setAppTitle:(NSString*)string{
    
    NSString *oldValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"appTitle"];
    
    if (_appTitle) {
        [_appTitle release];
    }
    
    if (string != nil) {
        _appTitle = [string retain];
    }
    else if(oldValue){
        _appTitle = [oldValue retain];
    }
    else{
        _appTitle = [@"Exceedings" retain];
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:_appTitle forKey:@"appTitle"];
}


#pragma mark - Activity

- (IBAction)updateHUDActivity:(NSString*)hudText show:(BOOL)show{
    
    @synchronized(self){
        _viewController.splashImg.hidden = !forceShowSplash;
        
        if (forceShowSplash) {
            _viewController.splashImg.hidden = NO;
        }
        
        _viewController.splashImg.userInteractionEnabled = show;
        
        if(show){
            if (!_appReference.appName || forceShowSplash) {
                [_viewController.view bringSubviewToFront:_viewController.splashImg];
            }
            
            if(!HUD){
                // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
                HUD = [[MBProgressHUD alloc] initWithView:self.window];
                [HUD setBackgroundColor:[UIColor clearColor]];
                [self.window addSubview:HUD];
                
                // Regiser for HUD callbacks so we can remove it from the window at the right time
                HUD.delegate = self;
                
                //Label text
              //  HUD.labelText = hudText;
                //Rajesh fixed loading issue...
                
                if(!_appReference.appName)
                    {
                        UILabel *lb  = [[[UILabel alloc] initWithFrame:CGRectMake(60, 145, 200, 60)] autorelease];
                        lb.text = @"Completing Setup...";
                        [lb setFont:[UIFont boldSystemFontOfSize:18.0f]];
                        lb.shadowOffset = CGSizeMake(0, 1.5);
                        lb.shadowColor = [UIColor blackColor];
                        [lb setTextColor:[UIColor whiteColor]];
                        [lb setBackgroundColor:[UIColor darkGrayColor]];
                        [lb setTextAlignment:NSTextAlignmentCenter];
                        lb.layer.borderWidth = 2.0;

                        lb.layer.cornerRadius = 10.0f;
                        lb.layer.borderColor = [[UIColor whiteColor] CGColor];
                                          
                        [HUD addSubview:lb];
                    }
                
                // Show the HUD while the provided method executes in a new thread
                [HUD show:NO];
            }
            else if(hudText){
               // HUD.labelText = hudText;
            }
            [self.window bringSubviewToFront:HUD];
            forceShowSplash = NO;
        }
        else{
            [HUD hide:YES];
        }
    }
}



- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    [HUD release];
	HUD = nil;
}

#pragma mark - App life

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (void)updateGraphics{
    
    UIImage *image = [UIImage imageWithContentsOfFile:[self imageBGPath]];
    if (!_appBGImage) {
        self.appBGImage = [[[UIImageView alloc] initWithImage:image] autorelease];
        [self.window addSubview:_appBGImage];
        [self.window sendSubviewToBack:_appBGImage];
    }
    
    
    _appBGImage.image = image;
    _appBGImage.frame = self.window.frame;
    
    
}

-(void)createUniqueDeviceID{
    
    self.deviceID = [AppDelegate getDeviceToken]; //[[NSUserDefaults standardUserDefaults] objectForKey:@"ExUUID"];
    
//    if (!_deviceID) {
//        
//// issue fix for Track Application.......
//        /*
//        // Create universally unique identifier (object)
//        CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
//        
//        // Get the string representation of CFUUID object.
//        self.deviceID = (NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidObject);
//        CFRelease(uuidObject);
//        
//        [[NSUserDefaults standardUserDefaults] setObject:_deviceID forKey:@"ExUUID"];
//         */
//        self.deviceID = [[UIDevice currentDevice] uniqueIdentifier];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:_deviceID forKey:@"ExUUID"];
//        
//        NSLog(@"#################### +SV+ Created Unique Devce ID Exists: %@", _deviceID);
//    }
//    else{
//        NSLog(@"-SV- Unique Devce ID Exists: %@", _deviceID);
//    }

    
}

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

+ (NSString*) getDeviceToken {
    NSString *udidString = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strValue = [defaults objectForKey:@"DonateGoldUDID"];
    
//#warning Simulator......
    
   // return @"simulator";
    
    if(!strValue || [strValue isEqualToString:@""])
    {
        if (SYSTEM_VERSION_LESS_THAN(@"6.0")) {
            
            udidString = [defaults objectForKey:@"DonateGoldUDID"];
            if (!udidString) {
                CFUUIDRef identifierObject = CFUUIDCreate(kCFAllocatorDefault);
                // Convert the CFUUID to a string
                udidString = (NSString *)CFUUIDCreateString(kCFAllocatorDefault, identifierObject);
                [defaults setObject:udidString forKey:@"DonateGoldUDID"];
                [defaults synchronize];
                CFRelease((CFTypeRef) identifierObject);
                return udidString;
            }
            
        } else {
            id appIdObject = [NSUUID UUID];
            NSString *appIdString = [appIdObject UUIDString];
            [defaults setObject:appIdString forKey:@"DonateGoldUDID"];
            [defaults synchronize];
            return appIdString;
        }
    }
    
    return strValue;
}


+ (NSString *) getAccesskey
{

    
    if([kIsBizookuAppManager isEqualToString:@"YES"])
    {
        id object = [[NSUserDefaults standardUserDefaults] valueForKey:kBizookuAccessKey];
        if(object && [object isKindOfClass:[NSString class]])
        {
            NSString *str = (NSString *)object;
            return str;
        }
        else{
            return kAccessKey;
        }
    }
    else{
        AppDelegate *del = [AppDelegate shareddelegate];
        int privateApp =del.appReference.isPrivateAppEnabled.intValue;
        if(privateApp == 1)
        {
            id object = [[NSUserDefaults standardUserDefaults] valueForKey:kBizookuAccessKey];
            if(object && [object isKindOfClass:[NSString class]])
            {
                NSString *str = (NSString *)object;
                return str;
            }
            else{
                return kAccessKey;
            }
        }
        else{
            return kAccessKey;
        }
    }

    
    
    
    
    
}

- (NSString *)tempApplicationDocumentsDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    //NSLog(@"Base path: %@", basePath);
    
    return basePath;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];

    
    //-- Set Notification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    }

    
    
    
    [[AVAudioSession sharedInstance] setDelegate:self];
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    NSError *activationError = nil;
    [[AVAudioSession sharedInstance] setActive:YES error:&activationError];

    
    //NSInteger badgeNumber = [application applicationIconBadgeNumber];// Take the current badge number
    //badgeNumber--;    // decrement by one
    [application setApplicationIconBadgeNumber:[[[launchOptions valueForKey:@"aps"]valueForKey:@"badge"]integerValue]];  // set ne badge number
    

    NSDate *currentDate = [NSDate date];
    
    //[[SPSingletonClass sharedInstance]inAppPurchaseRequest];
    self.couponsArray = [[NSArray alloc]init];
    self.couponsdict = [[NSDictionary alloc] init];
    
    self.couponsListArray = [[NSArray alloc]init];
    self.couponsListDict = [[NSDictionary alloc]init];
    
    self.couponsDetailsArray = [[NSArray alloc]init];
    self.couponsDetailDict = [[NSDictionary alloc]init];
    
    self.audioListDictionary = [NSDictionary dictionary];
    self.audioDetailDictionary = [NSDictionary dictionary];
    
    self.dashboardAudiosArray = [NSMutableArray array];
    
    self.eventsListDict = [[NSMutableDictionary alloc] init];
    self.eventsDetailsDict = [[NSMutableDictionary alloc] init];
    
    self.eventsCalendarDict =[[NSMutableDictionary alloc] init];

    self.volunterDict = [[NSMutableDictionary alloc] init];
    
    self.videoDetailDict = [[NSMutableDictionary alloc]init];

    self.videoListDict = [[NSMutableDictionary alloc] init];
    self.userFevVideoDict = [[NSMutableDictionary alloc] init];

    
    [[NSUserDefaults standardUserDefaults] setValue:currentDate forKey:@"startSession"];

    [[NSUserDefaults standardUserDefaults] synchronize];
    


    [[NSFileManager defaultManager] addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:[self tempApplicationDocumentsDirectory]]];
    
    
    
     [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    [self createUniqueDeviceID];
    forceShowSplash = YES;
    
    // Override point for customization after application launch.
    
   
    //Setup Alerts title
    [self setAppTitle:nil];
    
    [self setupAppRef];
//    [AppDelegate shareddelegate].appReference.brandid = nil;
    NSLog(@"brand id %@",[AppDelegate shareddelegate].appReference.brandid);
    if(![AppDelegate shareddelegate].appReference.brandid)
    {
        self.isFirstTime = YES;
    }
    else{

        self.isFirstTime = NO;
    }
    

    //// Checking first time / second time
    boolUpdate = NO;
    self.locationController = [[MyCLController alloc] init];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [_locationController.locationManager requestWhenInUseAuthorization];
    }
    
    if ([_locationController.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationController.locationManager requestAlwaysAuthorization];
    }
    
    
    _locationController.delegate = self;
    [_locationController.locationManager startUpdatingLocation];
    
    /*
    if([kIsBizookuAppManager isEqualToString:@"YES"])
    {
        UINavigationController *navController;
        if ([kIsBizookuSevenDigitAppManager isEqualToString:@"YES"]){

            if (self.isFirstTimeSevenDigitCode == YES){
                SPLoginCodeVC *loginCodeVC = [[[SPLoginCodeVC alloc] initWithNibName:@"SPLoginCodeVC" bundle:nil] autorelease];
                navController  = [[[UINavigationController alloc]initWithRootViewController:loginCodeVC] autorelease];
                self.window.rootViewController = navController;
                [self.window makeKeyAndVisible];

            }else{
                [self dashBoardView];

            }
            
            
        }else{
            LoginVC *login = [[[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil] autorelease];
            navController = [[[UINavigationController alloc]initWithRootViewController:login] autorelease];
            self.window.rootViewController = navController;
            [self.window makeKeyAndVisible];
            
        }
        
        
        
        
    }
    else{
        [self dashBoardView];
        
    }
     */
    if([kIsBizookuAppManager isEqualToString:@"YES"])
    {
        LoginVC *login = [[[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil] autorelease];
        navController = [[[UINavigationController alloc]initWithRootViewController:login] autorelease];
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
        
        
        
    }
    else{
        self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
        navController = [[[UINavigationController alloc]initWithRootViewController:self.viewController] autorelease];
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
        
        SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
        [example1 runDownloadInfoAddition];
        
    }

    
    
    
    IAPHelper *iapHelper = [IAPHelper sharedInstance];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:iapHelper];
    
    return YES;
}
/*
-(NSString*)UniqueAppId
{
    NSString *Appname = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    NSString *strApplicationUUID = [SSKeychain passwordForService:Appname account:@"Bizooku"];
    if (strApplicationUUID == nil)
    {
        strApplicationUUID  = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:Appname account:@"Bizooku"];
    }
    return strApplicationUUID;
}
 */
/*
- (void)dashBoardView
{
    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    UINavigationController *navController = [[[UINavigationController alloc]initWithRootViewController:self.viewController] autorelease];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
    
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runDownloadInfoAddition];
}
*/

- (void)applicationWillResignActive:(UIApplication *)application
{
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    NSDate *startDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"startSession"];
    NSDate *stopDate = [NSDate date];
    if(startDate && stopDate)
    {
        NSTimeInterval timeInterval = [stopDate timeIntervalSinceDate:startDate];
        [self startTask:timeInterval];
    }

}

- (void) startTask: (NSTimeInterval) sessionTime
{
    UIBackgroundTaskIdentifier bgTask = [[UIApplication sharedApplication]
                                         beginBackgroundTaskWithExpirationHandler:^(void){
                                         }];
    [self startSessionClose:sessionTime];
    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
}


- (void)startSessionClose:(NSTimeInterval) session
{
    SDZEXAnalyticsServiceExample *Exmp = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    [Exmp runAddSession:[[AppDelegate shareddelegate].appReference.brandid integerValue] timeInSec:session deviceType:@"iOS"];
}



- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
   //  [GoogleAnalyticsManager logAppForegroundEntry];
    
    //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    
    BOOL isLoginViewCntr =[[NSUserDefaults standardUserDefaults] boolForKey:@"LoginViewCntr"];
    
    BOOL isSevenDigitViewCntr =[[NSUserDefaults standardUserDefaults] boolForKey:@"SevenDigitViewCntr"];
    
    if (isLoginViewCntr == YES|| isSevenDigitViewCntr == YES){
        NSLog(@"login view cntr");
    }else{
        NSLog(@"no login view cntr");
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"startSession"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        forceShowSplash = NO;
        
        //// Checking first time / second time
        [[DashboardBuilder sharedDelegate] updateUsingWebService];
        
        //Inform about download
        SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
        [example1 runDownloadInfoAddition];
        
    }}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //application.applicationIconBadgeNumber = 0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    //    [GoogleAnalyticsManager logAppResignActive];
}


#pragma mark - Apple Push


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    
    NSString *str = [[[[NSString stringWithFormat:@"%@", deviceToken] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"Device token %@",str);
    
    
    [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"PushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // NSString *yourAlias = [UIDevice currentDevice].name;
    
    // Updates the device token and registers the token with UA
    //  [[UAirship shared] registerDeviceToken:deviceToken withAlias:yourAlias];
    // @"28adf6152ed23b72e835d4fada0aee1d03874df6ac2efb28404a3adb7fdf6822"
    SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    [example1 runPushNotificationTokenUpdate:str];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    //[self showAlertString:[NSString stringWithFormat:@"%@", error]];
    
    //SDZEXWidgetServiceExample* example1 = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    //[example1 runPushNotificationTokenUpdate:@"tests"];
    [[NSUserDefaults standardUserDefaults] setObject:@"jdhfgs-dsada-234dsa-asdda" forKey:@"PushToken"];

    NSLog(@"Device token error %@",error);
}
/*
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    NSLog(@"%s userInfo %@", __FUNCTION__, userInfo);
    
    NSInteger badgeNumber = [application applicationIconBadgeNumber];
    [application setApplicationIconBadgeNumber:++badgeNumber];
    
}
*/

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    NSLog(@"userInfo %@",userInfo);
    
    
    
    if (application.applicationState == UIApplicationStateActive )
    {
        //Your Code here
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSLog(@"UIApplicationStateActive");

    }
    else if (application.applicationState == UIApplicationStateBackground || application.applicationState == UIApplicationStateInactive)
    {
        //Your code here
        NSLog(@"UIApplicationStateBackground");

        int num = [[UIApplication sharedApplication]applicationIconBadgeNumber];
        
        if (num == 0) {
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:1];
        }
        
        else if (num >= 1) {
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:num + 1];
        }
    }
    
    /*
    if ([application applicationState] == UIApplicationStateInactive){
        
        NSLog(@"UIApplicationStateInactive");
        int num = [[UIApplication sharedApplication]applicationIconBadgeNumber];
        if (num == 0) {
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:1];
        }
        
        else if (num >= 1) {
            [[UIApplication sharedApplication]setApplicationIconBadgeNumber:num + 1];
            
            
        }else{
            NSLog(@"UIApplicationStateactive");
            
            
            
            
        }
    }
    */
    /*
    
     UIApplicationState state = [application applicationState];
     if (state == UIApplicationStateActive) {
     // do stuff when app is active
     //[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
     
     
     
     
     }else{
     // do stuff when app is in background
     
     
     int num = [[UIApplication sharedApplication]applicationIconBadgeNumber];
     if (num == 0) {
     [[UIApplication sharedApplication]setApplicationIconBadgeNumber:1];
     }
     
     else if (num >= 1) {
     [[UIApplication sharedApplication]setApplicationIconBadgeNumber:num + 1];
     }
     
     
     
     }
     
     
     */
     NSDictionary *apsMsg = [userInfo valueForKey:@"aps"];
     
     
     
     if(apsMsg && [[apsMsg allKeys] containsObject:@"alert"])
     {
         if([[apsMsg objectForKey:@"alert"] isKindOfClass:[NSString class]])
         {
             NSString *pushMsg = [apsMsg objectForKey:@"alert"];
             
             [self performSelectorOnMainThread:@selector(showAlert:) withObject:pushMsg waitUntilDone:NO];
         }
         else if([[apsMsg objectForKey:@"alert"] isKindOfClass:[NSDictionary class]])
        {
             NSDictionary *dic = [apsMsg objectForKey:@"alert"];
             NSString *pushMsg = [dic objectForKey:@"body"];
             
             //[self performSelectorOnMainThread:@selector(showAlert:) withObject:pushMsg waitUntilDone:NO];
             // [self performSelector:@selector(showNotificationAlert:button:) withObject:pushMsg withObject:@"hi"];
             if(dic && [[dic allKeys] containsObject:@"action-loc-key"])
             {
                 [AppDelegate showNotificationAlert:pushMsg button:[dic objectForKey:@"action-loc-key"]];
             }
             
        }
     }
    
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if ([application applicationState] == UIApplicationStateInactive)
    {
        NSLog(@"InActive");
    }else{
        NSLog(@"Active");
        
    }
    
    [[SPSingletonClass sharedInstance] setAudioPlayerStart];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[notification.userInfo valueForKey:@"Title"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    alertView.tag = 100;
    
    [alertView show];
}

/*
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void(^)())completionHandler
{
    NSLog(@"notification %@ identifier %@",notification,identifier);
    
    
}*/
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    //    NSLog(@"deviceO %@",window);
    NSUInteger orientations = UIInterfaceOrientationMaskPortrait;
    if(self.window.rootViewController){
        UIViewController *presentedViewController = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
        //        NSLog(@"isOrientation %d",self.isOrientation);
        if (self.isOrientation) {
            orientations = [presentedViewController supportedInterfaceOrientations];
        }
    }
    return orientations;
    
}




#pragma mark - Core Data stack


// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

-(void)crashTheApp{
    
    exit(0);
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
//#warning Delete the existing DB and exit the app if store doesnt match
        //App will exit in 3 seconds for refreshing the DB version.
        //
        [AppDelegate showAlert:@"Confirming Setup..."];
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        [self performSelector:@selector(crashTheApp) withObject:nil afterDelay:3];
        //[self performSelectorOnMainThread:@selector(crashTheApp) withObject:nil waitUntilDone:NO];
         
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        //     NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }    
    
    return __persistentStoreCoordinator;
}


#pragma mark Location delegate methods

#pragma mark Current Location Delegates

- (void)locationError:(NSError *)error {
    NSLog(@"locationError :::  description is: %@",error.localizedDescription);
   
}

- (void)locationUpdate:(CLLocation *)location {
    
    
    if(!boolUpdate)
    {
        if(location)
        {
            self.currentLocation = location;
            boolUpdate = YES;
            [_locationController.locationManager stopUpdatingLocation];
        }
    }
    
}



#pragma mark FaceBook Open URL 


- (BOOL)application:(UIApplication *)application 
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication 
         annotation:(id)annotation 
{
    return YES;
    
    //return [FBSession.activeSession handleOpenURL:url]; 
}


#pragma mark Share Options

- (void)postFB:(UIViewController*)vControl
{
    //self.socialVCTemp = vControl;
}



#pragma mark - Push

#pragma Twitter Composing


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    {
        if (alertView.tag == 100){
            [[SPSingletonClass sharedInstance]  setAudioPlayerStop];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getLocalLcheduleNotification" object:self];
            
        }
        
        if(buttonIndex == 1)
        {
            EventDetailVC *eventVC = [[[EventDetailVC alloc]initWithNibName:@"EventDetailVC" bundle:nil] autorelease];
            //        [self.viewController presentModalViewController:eventVC animated:YES];
            [self.viewController presentViewController:eventVC animated:YES completion:nil];
            
        }
    }
}
- (void)postTweet:(UIViewController*)vControl msgBody:(NSString *)str imagePath:(NSString *)imgPath link:(NSString *)link
{
    self.socialVCTemp = vControl;
    
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    Class TWTweetComposeViewControllerClass = NSClassFromString(@"TWTweetComposeViewController");
    
    int widgetId;
    
    if(!_selectedTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_selectedTileInfo.widget.widgetID intValue];
    }
    
    
    if (TWTweetComposeViewControllerClass != nil)
    {
        TWTweetComposeViewController *twitter = [[[TWTweetComposeViewController alloc] init] autorelease];
        
        [twitter setInitialText:[NSString stringWithFormat:str, _appReference.appName]];
        
        // NSData *dataTemp = [[[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:imgPath]] autorelease];
       // UIImage *imgData = [[[UIImage alloc]initWithData:dataTemp] autorelease];
       // [twitter addImage:imgData];
        
        if(link)
        {
            [twitter addURL:[NSURL URLWithString:link]];
        }
        else{
            if(_appReference.appstoreURL)
            {
                [twitter addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_appReference.appstoreURL]]];
            }
            else{
                [twitter addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_appReference.markettingURL]]];
            }
        }
        
        
        //[service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Twitter" action:@"Entry" status:@"" deviceType:@"iOS"];
        
        [_socialVCTemp presentViewController:twitter animated:YES completion:nil];
        
        twitter.completionHandler = ^(TWTweetComposeViewControllerResult res)
        {
            if(!_selectedItemId)
            {
                self.selectedItemId = [NSNumber numberWithInt:0];
            }
            if(_selectedTileInfo && _selectedItemId)
            {
                
                
                if(res == TWTweetComposeViewControllerResultDone)
                {
                    [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Twitter" action:@"" status:@"Shared" deviceType:@"iOS"];
                    // Message tweeted.....
                }
                if(res == TWTweetComposeViewControllerResultCancelled)
                {
                    // Message not tweeted......
                    [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Twitter" action:@"" status:@"Canceled" deviceType:@"iOS"];
                }
                if(_selectedItemId){ self.selectedItemId = nil;}
                if(_selectedTileInfo) { self.selectedTileInfo = nil;}
            }
            [_socialVCTemp dismissViewControllerAnimated:YES completion:nil];
            self.socialVCTemp = nil;
        };  
    }
    else
    {
        if([UIDevice version] < 5.0)
        {
            [AppDelegate showiOSLimitAlert];
            
        }
    }
}

#pragma mark Mail composing 

- (void)postMail:(UIViewController*)vControl subject:(NSString *)subj msgBody:(NSString *)body
{
    
    self.socialVCTemp = vControl;
    
    if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mfViewController = [[MFMailComposeViewController alloc] init];
		mfViewController.mailComposeDelegate = self;
        [[mfViewController navigationBar] setTintColor:[UIColor blackColor]];
        [mfViewController setSubject:[NSString stringWithFormat:subj, _appReference.appName]];
        [mfViewController setMessageBody:@"" isHTML:NO];
        
        [mfViewController setMessageBody:[NSString stringWithFormat:@"%@",body] isHTML:YES];
		mfViewController.navigationItem.title = @"Mail";
//		[_socialVCTemp presentModalViewController:mfViewController animated:YES];
        [_socialVCTemp presentViewController:mfViewController animated:YES completion:nil];
		[mfViewController release];
	}else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:kMailError delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
		
		[alert show];
		[alert release];
	}
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
	//UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message Status" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    int widgetId;
    
    if(!_selectedTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_selectedTileInfo.widget.widgetID intValue];
    }
    
    
    if(!_selectedItemId)
    {
        self.selectedItemId = [NSNumber numberWithInt:0];
    }
	if(_selectedItemId && _selectedTileInfo)
    {
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        
        switch (result) {
            case MFMailComposeResultCancelled:{
                //alert.message = @"Canceled";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultSaved:
            {
                //alert.message = @"Saved";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultSent:
            {
                //alert.message = @"Sent";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Email" action:@"" status:@"Shared" deviceType:@"iOS"];
            }
                break;
            case MFMailComposeResultFailed:
            {
                //alert.message = @"Message Failed";
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"Email" action:@"" status:@"Canceled" deviceType:@"iOS"];
            }
                break;
            default:
                //alert.message = @"Message Not Sent";
                break;	
        }
    }
    
    if(_selectedItemId) { self.selectedItemId = nil;}
    if(_selectedTileInfo) { self.selectedTileInfo = nil; }
    
	//[_socialVCTemp dismissModalViewControllerAnimated:YES];
    [_socialVCTemp dismissViewControllerAnimated:YES completion:^{
        
    }];
    
    
	self.socialVCTemp = nil;
    
	//[alert release];
}


#pragma mark Open SMS Composing

-(void)postSMS:(UIViewController*)vControl smsContent:(NSString *)msg{
    self.socialVCTemp = vControl;
    
    if([MFMessageComposeViewController canSendText]){
        MFMessageComposeViewController *msgController = [[[MFMessageComposeViewController alloc] init] autorelease];
        [msgController setBody:[NSString stringWithFormat:msg, _appReference.appName]];
        msgController.messageComposeDelegate = self;
        // [[AppDelegate sharedDelegate].mainViewController presentModalViewController:msgController animated:YES];
      //  [_socialVCTemp presentModalViewController:msgController animated:YES];
        [_socialVCTemp presentViewController:msgController animated:YES completion:^{
            
        }];
        
    }
    else{
        [AppDelegate showAlert:kSMS_NotAvailable];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
	
    int widgetId;
    
    if(!_selectedTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_selectedTileInfo.widget.widgetID intValue];
    }
    
    
    if(!_selectedItemId)
    {
        self.selectedItemId = [NSNumber numberWithInt:0];
    }
    if(_selectedTileInfo)
    {
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        switch (result) {
            case MessageComposeResultCancelled:
            {
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"SMS" action:@"" status:@"Canceled" deviceType:@"iOS"];
                
            }
                break;
            case MessageComposeResultFailed:
            {
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"SMS" action:@"" status:@"Failed" deviceType:@"iOS"];
            }
                break;
            case MessageComposeResultSent:
            {
                [service runAddShareAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:[_selectedItemId integerValue] actionType:@"SMS" action:@"" status:@"Shared" deviceType:@"iOS"];
            }
                break;
                
            default:
                break;
        }
    }
    if(_selectedItemId){ self.selectedItemId = nil;}
    if(_selectedTileInfo) { self.selectedTileInfo = nil;}
    
    [_socialVCTemp dismissViewControllerAnimated:YES completion:nil];
    self.socialVCTemp = nil;
}




@end
