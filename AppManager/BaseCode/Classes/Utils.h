//
//  Utils.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConstantsEDM.h"

@interface Utils : NSObject

+(NSString*)detailTitleForAppType:(AppType)appType;
+(NSString*)titleForAppType:(AppType)appType;
+(void)loadFontInView:(UIView*)aMajorView;

@end
