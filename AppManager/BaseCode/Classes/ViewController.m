//
//  ViewController.m
//  jefftest
//
//  Created by Rajesh Kumar Yandamuri on 07/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//



#import "ViewController.h"
#import "DashboardBuilder.h"

#import "AppTableVC.h"
#import "ContributeVC.h"
#import "AboutUsVC.h"
#import "AppDelegate.h"
#import "SPCouponDetailsViewController.h"
#import "SPCouponsViewController.h"
#import "SPCouponsListViewController.h"
#import "DBApp.h"
#import "DBLayoutInfo.h"
#import "DBLayoutTypes.h"
#import "DBTilesInfo.h"
#import "DBTileSize.h"
#import "DBWidget.h"
#import "DBSponsorWidget.h"

#import "SDZEXWidgetServiceExample.h"

#import "NSManagedObjectContext-EasyFetch.h"
#import "UILabel+Exceedings.h"
#import "UIColor+HexString.h"
#import "UIImageView+WebCache.h"

#import "ConstantsEDM.h"

#import "InfoCollectorVC.h"
#import <QuartzCore/QuartzCore.h>

#import "SDZEXWidgetServiceExample.h"
#import "StartUpGuideVC.h"

#import "DBProductWidget.h"
#import "DBCouponsWidget.h"

#import "custompopup.h"

#import "SponsorVC.h"
#import "CustomBannerView.h"
#import "WebViewController.h"
#import "SPSingletonClass.h"
#import "ProductsListVC.h"
#import "DBNewsWidget.h"
#import "NewListVC.h"
#import "DBEventsWidget.h"

#import "EventsListVC.h"

#import "SPEventCalendarVC.h"
#import "SPLoginCodeVC.h"
#import "SPSingletonClass.h"

#import "DBVideoWidget.h"

#import "SPVideoListVC.h"
#import "SPUserFavoriteVC.h"
#import "SPVideoDetailVC.h"


@interface ViewController ()
@property (retain, nonatomic) SocialActionSheet *socialASheet;
@property (nonatomic, retain) InfoCollectorVC *infoCollVC;

//Banner
@property (nonatomic, strong) NSMutableArray *bannerUrlsArray;
@property (nonatomic,strong) CustomBannerView *addBannerView;
@property (nonatomic, strong) NSMutableDictionary *aBannerDict;
@property (nonatomic,strong) UIImageView *backGroundStaticImage;

@property (nonatomic, strong) NSMutableDictionary *selectedBannerDict;


@end

@implementation ViewController

@synthesize loadedTileInfo = _loadedTileInfo;
@synthesize socialASheet = _socialASheet;

@synthesize splashImg;
@synthesize headerTitle;
@synthesize logoImage;
@synthesize scrollTile = _scrollTile;
@synthesize facebookWeb;
@synthesize headerView;

@synthesize appWeb = _appWeb;
@synthesize appTableVC = _appTableVC;
@synthesize appAboutUs = _appAboutUs;
@synthesize sponsorVC = _sponsorVC;
@synthesize appContribute = _appContribute;
@synthesize tileBodyNavigation = _tileBodyNavigation;
@synthesize tileBodyVC = _tileBodyVC;
@synthesize appType = _appType;
@synthesize infoCollVC = _infoCollVC;
@synthesize appSubWeb = _appSubWeb;
@synthesize isSponsorStarted = _isSponsorStarted;
@synthesize appCouponsVC = _appCouponsVC;

@synthesize audioListVC = _audioListVC;
@synthesize audioDetailVC = _audioDetailVC;
@synthesize audioPurchaseVC = _audioPurchaseVC;

/// Sponsered...

@synthesize arrSponsoredImagelist = _arrSponsoredImagelist;
@synthesize sponseredImageData = _sponseredImageData;
//@synthesize indexCount = _indexCount;
@synthesize viewPopUp = _viewPopUp;
@synthesize sponsorTile = _sponsorTile;
@synthesize tileType = _tileType;
@synthesize isSelectedSponsor = _isSelectedSponsor;
@synthesize isFirstTime = _isFirstTime;

-(void)viewDidAppear:(BOOL)animated{
    _scrollTile.delegate1 = self;
    //[[DashboardBuilder sharedDelegate] updateUsingWebService];

    
    [_scrollTile scrollRectToVisible:CGRectMake(0, 0, 320, 480) animated:NO];
}

- (void) viewDidDisappear:(BOOL)animated
{
    self.isFirstTime = NO;
    [self.backGroundStaticImage setHidden:YES];
    [self.addBannerView removeFromSuperview];
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedBannerDict = [[NSMutableDictionary alloc] init];
    
    if ([kIsBizookuAppManager isEqualToString:@"YES"])
    {
        self.backGroundStaticImage.hidden = YES;

    }else{
        if([AppDelegate shareddelegate].isFirstTime)
        {
            
            self.backGroundStaticImage = [[UIImageView alloc] init];
            self.backGroundStaticImage.frame = CGRectMake(0, 0, 320, 568);
            self.backGroundStaticImage.image = [UIImage imageWithContentsOfFile:[self splashImagePath]];
            
            [self.backGroundStaticImage setBackgroundColor:[UIColor blackColor]];
            [self.view addSubview:self.backGroundStaticImage];
            self.backGroundStaticImage.hidden = NO;
            [self.view bringSubviewToFront:self.backGroundStaticImage];
            
        }
    }
    
    self.isFirstTime = YES;
	[self updateAllDashboardGraphics];
    [DashboardBuilder sharedDelegate].refreshMode = @"New";
    [[DashboardBuilder sharedDelegate] updateUsingWebService];
    // Do any additional setup after loading the view, typically from a nib.

    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveBannersNotification:)
                                                 name:@"getBannersNotification"
                                               object:nil];
    
    

    
    [self addPopUp];
    
}

#pragma mark---
#pragma ADD Banner Methods

- (void)addBannerData
{
    self.bannerUrlsArray = [NSMutableArray arrayWithCapacity:0];
    self.addBannerView.frame = CGRectMake(0,578, 320, 70);

    NSMutableArray *localTempArry = [NSMutableArray array];
    for (NSMutableDictionary *dict in [AppDelegate shareddelegate].bannersDict) {
        if ([[dict objectForKey:@"WidgetName"] isEqualToString:@"Main Menu"]) {
            self.aBannerDict = dict;
            
            [self.bannerUrlsArray addObjectsFromArray:[[self.aBannerDict objectForKey:@"BannersList"] valueForKey:@"BannerImage"]];
            [localTempArry addObjectsFromArray:[self.aBannerDict objectForKey:@"BannersList"]];
            break;
        }
        
    }
    
    if ([self.bannerUrlsArray count]){
        if ([[[[self.aBannerDict objectForKey:@"BannersList"] objectAtIndex:0]valueForKey:@"BannerType"] isEqualToString:@"Large"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLargeBanner"];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLargeBanner"];

        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.addBannerView.bannersDict = self.aBannerDict;
        self.addBannerView.totalBannersArray = localTempArry;
        self.addBannerView.delegate = self;
        
        [self.addBannerView startShowingBanners:self.bannerUrlsArray screenDetails:@"Detail"];
    }
}

- (void)customBannerCallbackMethod:(id)withObject
{
//    self.selectedBannerDict = withObject;
    
    NSString *linkType = [withObject valueForKey:@"LinkType"];
    if ([linkType isEqualToString:@"Link to App Content"]){

        [self.selectedBannerDict setDictionary:withObject];
        [self.bigBanner getBannerDictionary:withObject];

    }else{
        WebViewController *webVc = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
        webVc.bannerDict = withObject;
        [self.navigationController pushViewController:webVc animated:YES];
    }
    
    [self entryActionBanners];
}
- (void)didChangeViewCntr:(BigBanners *)popOverView selectedCntrl:(UIViewController *)selectedCntrl;
{
    [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];

    self.appType = [[SPSingletonClass sharedInstance] getAppType];
    
    /*
    BOOL isTaskPresentVC = NO;

    if ([[self.selectedBannerDict valueForKey:@"WidgName"]isEqualToString:@"Volunteer"]||[[self.selectedBannerDict valueForKey:@"WidgName"]isEqualToString:@"Fundraising"]){
        isTaskPresentVC = [self createNewAppTableType];
    }else{
        [self.navigationController pushViewController:selectedCntrl animated:YES];

    }

    //Perform VC animation
    if (isTaskPresentVC) {
        //Create navigation
        self.tileBodyNavigation = [[UINavigationController alloc] initWithRootViewController:_tileBodyVC];
        _tileBodyNavigation.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        //Present it finally
        [self presentViewController:_tileBodyNavigation animated:YES completion:^{
            
        }];
        
    }*/
    [self.navigationController pushViewController:selectedCntrl animated:YES];


    
    
}


-(void)entryActionBanners
{
    // NSLog(@"self.addBannerView.totalBannersArray %@",self.addBannerView.totalBannersArray);
    //self.addBannerView.totalBannersArray
    NSString * itemID  = [[self.addBannerView.totalBannersArray objectAtIndex:0]valueForKey:@"BannerId"];
    NSString *widgetId = @"18";
    
    SDZEXAnalyticsServiceExample* service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid longValue] widgetId:[widgetId integerValue] widgetItemId:[itemID integerValue] widgetType:@"Detail" deviceType:@"iOS"];
    
}


- (void)startUpScreen
{
    /*
    StartUpGuideVC *vc = [[[StartUpGuideVC alloc] initWithNibName:@"StartUpGuideVC" bundle:nil] autorelease];
    [self presentViewController:vc animated:YES completion:^{
        
    }];
    
    [[AppDelegate shareddelegate].window bringSubviewToFront:vc.view];*/
    
    [self.addBannerView removeFromSuperview];
    self.addBannerView = nil;
    
    SPLoginCodeVC *vc = [[[SPLoginCodeVC alloc] initWithNibName:@"SPLoginCodeVC" bundle:nil] autorelease];
    [self.navigationController pushViewController:vc animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.addBannerView removeFromSuperview];
    self.addBannerView = nil;
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    if(viPopUp)
    {
        viPopUp.hidden = YES;
    }
    self.navigationController.navigationBar.hidden = YES;
    
    /// checking sponsor animations...
    if(!_isFirstTime)
    {
        [self startAnimationAgain];
        [self startProductAnimationAgain];
        [self startCouponesAnimationAgain];
    }
    
    
    
    [self adsBannerApi];
    
    
    
    
    
    
}

- (void)adsBannerApi
{
    SDZEXWidgetServiceExample* banners = [[[SDZEXWidgetServiceExample alloc] init] autorelease];
    if(_isFirstTime == NO){
        [[AppDelegate shareddelegate] updateHUDActivity:@"" show:YES];
        
    }
    [banners runBanners];
}
#pragma mark - Pull to refresh scroll Delegate

-(void)stopRfreshOfScrollView{
    
    [self.scrollTile stopLoading]; // Comment for no pull down...
}
-(void)refreshScrollView{
    [self.addBannerView removeFromSuperview];
    self.addBannerView = nil;
    [self adsBannerApi];
    
    [[[AppDelegate shareddelegate] dashboardAudiosArray] removeAllObjects];
    [AppDelegate shareddelegate].dashboardAudiosArray = nil;
    [AppDelegate shareddelegate].dashboardAudiosArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    [DashboardBuilder sharedDelegate].refreshMode = @"Pulldown";
    [[DashboardBuilder sharedDelegate] updateUsingWebService];
}

#pragma mark - File Paths

-(NSString*)imageLogoPath{
    DBApp* _appReference = [AppDelegate shareddelegate].appReference;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logoPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"logo_%@", [_appReference.logoContainerImg stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    // NSLog(@"The logo path is %@", logoPath);
    return logoPath;
}

-(NSString*)splashImagePath{
    DBApp* _appReference = [AppDelegate shareddelegate].appReference;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *splashPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"splash_%@", [_appReference.splashimage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    // NSLog(@"splashPath: %@", splashPath);
    return splashPath;
}


-(NSString*)imgTilePath:(DBTilesInfo*)tileInfo{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"tile%@_%@", tileInfo.brandTileId, [tileInfo.bgimage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    
    // NSLog(@"Path for %@: %@", tileInfo.brandTileId, path);
    return path;
}

- (NSString*)imgSponsorPath:(DBTilesInfo*)tileInfo withSponsor:(DBSponsorWidget *)sponsor{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"sponsor%dX%dX%@_%@",[tileInfo.tileSize.width integerValue],[tileInfo.tileSize.height integerValue], sponsor.sponsorId, [sponsor.tileImage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    
    //  NSLog(@"Path for sponsor images %@: %@", tileInfo.brandTileId, path);
    return path;
}


//Sparity Banners

- (void)receiveBannersNotification:(NSNotification *) notification
{
    
    if(_isFirstTime == NO){
        [[AppDelegate shareddelegate] updateHUDActivity:@"" show:NO];
        
    }

    //Custom Banner View code here
    self.addBannerView.frame = CGRectZero;
    if (!self.addBannerView){
        self.bigBanner = [[BigBanners alloc] init];
        self.bigBanner.bigBannerDelegate = self;
        self.addBannerView =[[[NSBundle mainBundle] loadNibNamed:@"CustomBannerView" owner:self options:nil]objectAtIndex:0];
        self.addBannerView.userInteractionEnabled = YES;
        [self.view addSubview:self.addBannerView];
        [self.view bringSubviewToFront:self.backGroundStaticImage];
        [self.backGroundStaticImage sendSubviewToBack:self.addBannerView];
    }

    [self addBannerData];
    
    
    
}



- (NSString*)imgProductsPath:(DBTilesInfo*)tileInfo withProducts:(DBProductWidget *)product{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"product%dX%dX%@_%@",[tileInfo.tileSize.width integerValue],[tileInfo.tileSize.height integerValue], product.productId, [product.productImage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    
    //  NSLog(@"Path for sponsor images %@: %@", tileInfo.brandTileId, path);
    return path;
}
//Veeru
- (NSString*)imgCouponsPath:(DBTilesInfo*)tileInfo withCoupons:(DBCouponsWidget *)coupon{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"coupon%dX%dX%@_%@",[tileInfo.tileSize.width integerValue],[tileInfo.tileSize.height integerValue], coupon.couponId, [coupon.couponImage stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
    
    return path;
}

#pragma mark - Graphics Downloads

-(void)downlaodImagesInBG
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSLog(@"################################ Start ###");
    NSMutableDictionary *imagesDictionary = nil;
    
    NSManagedObjectContext *context = [AppDelegate shareddelegate].managedObjectContext;
    NSArray *appArray = [context fetchObjectsForEntityName:kDBApp];
    
    if ([appArray count]) {
        DBApp *app = [appArray objectAtIndex:0];
        imagesDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                            app.backgroundimage, @"appbg",
                            app.logoContainerImg, @"logo",
                            app.splashimage, @"splash",
                            nil];
        
        for (DBTilesInfo *tile in app.layoutInfo.tiles) {
            if (tile.bgimage) {
                [imagesDictionary addEntriesFromDictionary:[NSDictionary dictionaryWithObject:tile.bgimage forKey:[NSString stringWithFormat:@"tile%@", tile.brandTileId]]];
                
                /// **************** for sponsors...... *********
                
                if(tile.widget.sponsor && [tile.widget.sponsor count]>0)
                {
                    for(DBSponsorWidget *sponsor in tile.widget.sponsor)
                    {
                        //  NSLog(@"THe total sponsors in the brand are: %d", [tile.widget.sponsor count]);
                        
                        if(sponsor.tileImage && ![sponsor.tileImage isEqualToString:@""])
                        {
                            [imagesDictionary addEntriesFromDictionary:[NSDictionary dictionaryWithObject:sponsor.tileImage forKey:[NSString stringWithFormat:@"sponsor%dX%dX%@", [tile.tileSize.width integerValue], [tile.tileSize.height integerValue],sponsor.sponsorId]]];
                        }
                    }
                }
                
                if(tile.widget.products && [tile.widget.products count]>0)
                {
                    for(DBProductWidget *product in tile.widget.products)
                    {
                        //  NSLog(@"THe total sponsors in the brand are: %d", [tile.widget.sponsor count]);
                        
                        if(product.productImage && ![product.productImage isEqualToString:@""])
                        {
                            [imagesDictionary addEntriesFromDictionary:[NSDictionary dictionaryWithObject:product.productImage forKey:[NSString stringWithFormat:@"product%dX%dX%@", [tile.tileSize.width integerValue], [tile.tileSize.height integerValue],product.productId]]];
                        }
                    }
                }
                
                
                //Sparity
                if(tile.widget.coupons && [tile.widget.coupons count]>0)
                {
                    for(DBCouponsWidget *coupon in tile.widget.coupons)
                    {
                        
                        if(coupon.couponImage && ![coupon.couponImage isEqualToString:@""])
                        {
                            [imagesDictionary addEntriesFromDictionary:[NSDictionary dictionaryWithObject:coupon.couponImage forKey:[NSString stringWithFormat:@"coupon%dX%dX%@", [tile.tileSize.width integerValue], [tile.tileSize.height integerValue],coupon.couponId]]];
                        }
                    }
                }
                
                
                
                
                /// ***************  The end ********************
            }
        }
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    NSArray *dicKeys = [imagesDictionary allKeys];
    
    for (NSInteger i = 0; i< [dicKeys count]; i++) {
        
        NSAutoreleasePool *subpool = [[NSAutoreleasePool alloc] init];
        
        NSString *key = [dicKeys objectAtIndex:i];
        NSString *value = [imagesDictionary valueForKey:key];
        
        NSString *localFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@", key, [value stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:localFilePath]) {
            
            NSString *imgPath = [NSString stringWithFormat:@"%@%@",kWebServiceURL ,value];
            
            
            
            if (value && [value length]) {
                NSURL *imageURL = [NSURL URLWithString:imgPath];
                
                NSURLRequest *request = [NSURLRequest requestWithURL:imageURL];
                [NSURLConnection connectionWithRequest:request delegate:self];
                
                NSData *thedata = [NSData dataWithContentsOfURL:imageURL];
                [thedata writeToFile:localFilePath atomically:YES];
                
                NSLog(@"localFilePath written: %@", localFilePath);
            }
            else {
                NSLog(@"Not dwnloading image because path not found");
            }
        }
        [subpool release];
    }
    
    NSLog(@"################################ Finished Downloading ###");
    [self performSelectorOnMainThread:@selector(updateAllDashboardGraphics) withObject:nil waitUntilDone:NO];
    
    [self giveupUpdate];
    
    NSLog(@"################################ Update Graphics Finished ###");
    [pool release];
    
}

-(void)giveupUpdate
{
    
    if ([kIsBizookuAppManager isEqualToString:@"NO"]){
        AppDelegate *del = [AppDelegate shareddelegate];
        int privateApp = del.appReference.isPrivateAppEnabled.intValue;
        
        
        if([AppDelegate shareddelegate].isFirstTime)
        {
            [AppDelegate shareddelegate].isFirstTime = NO;
            
            if (privateApp == 1 ){
                [[SPSingletonClass sharedInstance] setIsFirstTimeSevenDigitCode:NO];
                
                [self performSelector:@selector(startUpScreen) withObject:nil afterDelay:0];
                
            }else {
                [[SPSingletonClass sharedInstance] setIsFirstTimeSevenDigitCode:YES];
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"FirstTimeSevenDigitCode"];
                
                self.backGroundStaticImage.hidden = YES;
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }else
        {
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"FirstTimeSevenDigitCode"] isEqualToString:@"YES"]){
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"FirstTimeSevenDigitCode"];
                
                [AppDelegate shareddelegate].isFirstTime = NO;
                if (privateApp == 1 ){
                    [self performSelector:@selector(startUpScreen) withObject:nil afterDelay:0];
                    
                }else{
                    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"FirstTimeSevenDigitCode"];
                    
                    self.backGroundStaticImage.hidden = YES;
                    
                }
                
            }else{
                self.backGroundStaticImage.hidden = YES;
                
            }
        }
        
    }

    [[AppDelegate shareddelegate] updateHUDActivity:nil show:NO];
    [self.view sendSubviewToBack:splashImg];
}

#pragma mark - CoreData Helpers

-(DBTilesInfo*)cdTileInfoAtPosition:(NSInteger)position{
    NSArray *array = [[AppDelegate shareddelegate].managedObjectContext fetchObjectsForEntityName:kDBTilesInfo predicateWithFormat:@"position = %d", position];
    if (array && [array count]) {
        return [array objectAtIndex:0];
    }
    return nil;
}

#pragma mark - Actions

-(IBAction)animateBtnSelected:(UIButton*)sender{
    // NSLog(@"sender.tag = %d", sender.tag);
    
    CALayer *viewLayer = sender.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:1.1],
                             [NSNumber numberWithFloat:1.2],
                             [NSNumber numberWithFloat:.9],
                             [NSNumber numberWithFloat:1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.4],
                               [NSNumber numberWithFloat:0.7],
                               [NSNumber numberWithFloat:1.0],
                               nil];
    popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"transform.scale"];
    
}

-(void)dismissPresentedModal:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

#pragma mark - App Detail Creator
-(BOOL)createNewAppTableType{
    
    
    self.appTableVC = [[[AppTableVC alloc]initWithNibName:@"AppTableVC" bundle:nil] autorelease];
    self.tileBodyVC = _appTableVC;
    
    _appTableVC.widgetConstantType = @"Listing";
    _appTableVC.widgetTileInfo = _loadedTileInfo;
    _appTableVC.appType = _appType;
    _appTableVC.noDataAvailableLbl.hidden = YES;
    return YES;
}

-(BOOL)createNewAppBannersType{
    
    //    self.appAboutUs = [[[AboutUsVC alloc]initWithNibName:@"AboutUsVC" bundle:nil] autorelease];
    _appAboutUs.widgetConstantType = @"Detail";
    _appAboutUs.widgetTileInfo = _loadedTileInfo;
    //    self.tileBodyVC = _appAboutUs;
    //    _appAboutUs.appType = _appType;
    return YES;
}


#pragma mark - App Table Detail Creator

-(BOOL)createNewAppTableTypeByPageTitle:(NSString *)pageTitle
{
    self.appTableVC = [[[AppTableVC alloc]initWithNibName:@"AppTableVC" bundle:nil] autorelease];
    self.tileBodyVC = _appTableVC;
    _appTableVC.widgetConstantType = @"Listing";
    _appTableVC.widgetTileInfo = _loadedTileInfo;
    _appTableVC.appType = _appType;
    //DBTilesInfo
    
    _appTableVC.pageTitle = pageTitle;
    _appTableVC.noDataAvailableLbl.hidden = YES;
    
    return YES;
}

-(BOOL)createNewAppAboutUsType{
    
    self.appAboutUs = [[[AboutUsVC alloc]initWithNibName:@"AboutUsVC" bundle:nil] autorelease];
    _appAboutUs.widgetConstantType = @"Detail";
    _appAboutUs.widgetTileInfo = _loadedTileInfo;
    self.tileBodyVC = _appAboutUs;
    _appAboutUs.appType = _appType;
    return YES;
}

//AUDIO
-(BOOL)createNewAppAudioType
{
    
    self.audioListVC = [[[FreeAudiosListViewController alloc]initWithNibName:@"FreeAudiosListViewController" bundle:nil] autorelease];
    _audioListVC.widgetConstantType = @"Detail";
    _audioListVC.widgetTileInfo = _loadedTileInfo;
    self.tileBodyVC = _audioListVC;
    _audioListVC.appType = _appType;
    return YES;
}

-(BOOL)createNewAppAudioDetailType:(NSDictionary *)detailDict
{
    
    self.audioDetailVC = [[[AudioDetailViewController alloc]initWithNibName:@"AudioDetailViewController" bundle:nil] autorelease];
    _audioDetailVC.audioDetailDictionary = detailDict;
    self.audioDetailVC.tileInfo = self.loadedTileInfo; //self.widgetTileInfo;
    //appCouponsDetailsVC.detailDataObject = _loadedTileInfo;
    
    _audioDetailVC.boolNoListing = YES;
    //    _audioListVC.widgetConstantType = @"Detail";
    //    _audioListVC.widgetTileInfo = _loadedTileInfo;
    self.tileBodyVC = _audioDetailVC;
    _audioDetailVC.appType = APP_AUDIO_DETAIL;
    return YES;
}

-(BOOL)createNewAppAudioPurchaseType:(NSDictionary *)detailDict
{
    
    self.audioPurchaseVC = [[[AudioPurchaseViewController alloc]initWithNibName:@"AudioPurchaseViewController" bundle:nil] autorelease];
    _audioPurchaseVC.purchasingAudioDetailDictionary = detailDict;
    _audioPurchaseVC.boolNoListing = YES;
    _audioPurchaseVC.isBackgroundPurchasedAudio = NO;
    
    self.tileBodyVC = _audioPurchaseVC;
    _audioPurchaseVC.appType = APP_AUDIO_PURCHASE;
    return YES;
}

-(BOOL)createNewAppWeblinkType{
    
    self.appSubWeb = [[[AppSubWebVC alloc]initWithNibName:@"AppSubWebVC" bundle:nil] autorelease];
    //_appSubWeb.widgetConstantType = @"Detail";
    _appSubWeb.appType = _appType;
    self.appSubWeb.tileInfo = _loadedTileInfo;
    [self.appSubWeb setTileInfo:_loadedTileInfo];
    self.tileBodyVC = _appSubWeb;
    
    
    return YES;
}


-(BOOL)createNewAppProductsType:(DBProductWidget *)product
{
    
    ProductsListVC *productsList = [[ProductsListVC alloc] initWithNibName:@"ProductsListVC" bundle:nil];
    productsList.boolNoListing = YES;
    

    
    self.appProductList = productsList;
    self.appProductList.appType = _appType;
    self.tileBodyVC = _appProductList;
    _appProductList.tileInfo = _loadedTileInfo;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if([product isKindOfClass:[DBProductWidget class]])
    {
        [dic setValue:product.productEmail forKey:@"Email"];
        [dic setValue:product.productId forKey:@"ProductID"];
        [dic setValue:product.productImage forKey:@"Image"];
        [dic setValue:product.productName forKey:@"Title"];
        [dic setValue:product.productPhone forKey:@"Phone"];
        [dic setValue:product.productPrice forKey:@"Price"];
        [dic setValue:product.productPageTitle forKey:@"PageTitle"];
        [dic setValue:product.categoryId forKey:@"CategoryId"];
        [dic setValue:product.categoryName forKey:@"CategoryName"];
        
        
    }
    [[NSUserDefaults standardUserDefaults] setValue:[dic valueForKey:@"CategoryId"] forKey:@"ProductionCategoryId"];
    
    self.appProductList.detailDataDictionary = dic;
    return YES;
}

- (BOOL)createNewAppCouponsType:(DBCouponsWidget *)coupon{
    
    
    SPCouponsListViewController *appCouponsListVC = [[[SPCouponsListViewController alloc]initWithNibName:@"SPCouponsListViewController" bundle:nil]autorelease];
    appCouponsListVC.boolNoListing = YES;
    
    self.tileBodyVC = appCouponsListVC;

    appCouponsListVC.appType = _appType;
    
    if([coupon isKindOfClass:[DBCouponsWidget class]])
    {
        NSString *tempStr = coupon.couponCategoryId;
        
        [[SPSingletonClass sharedInstance] setCouponTitleStr:coupon.couponCategoryName];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:tempStr forKey:@"CategoryId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    return YES;
    
}


-(BOOL)createNewAppWebURLType{
    self.appWeb = [[[AppWebVC alloc]initWithNibName:@"AppWebVC" bundle:nil] autorelease];
    self.tileBodyVC = _appWeb;
    _appWeb.widgetConstantType = @"Detail";
    _appWeb.widgetTileInfo = _loadedTileInfo;
    _appWeb.appType = _appType;
    return YES;
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    BOOL isTaskPresentVC = NO;
    
    if (!_loadedTileInfo) {
        //Donate widget proceed action
        [_infoCollVC.view removeFromSuperview];
        self.infoCollVC = nil;
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeLocations]) {
        _appType = APP_LOCATIONS;
        isTaskPresentVC = [self createNewAppTableType];
        
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeAbout]) {
        _appType = APP_ABOUT;
        isTaskPresentVC = [self createNewAppAboutUsType];
        
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeNews]) {
        
        NSMutableArray *arrNew = [self fetchNewListByTile:_loadedTileInfo];
        
        NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:[arrNew valueForKey:@"categoryID"]];
        
        if([set count] ==  1)
        {
            _appType = APP_NEWS;
            isTaskPresentVC = [self createNewAppNewsType:[arrNew objectAtIndex:0]];
        }
        else if([set count] > 0)
        {
            _appType = APP_NEWS;
            NSString *str = [self getNewsTitleByArrayItems:arrNew];
            isTaskPresentVC = [self createNewAppTableTypeByPageTitle:str];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"%@ not available", kTypeNews]];
        }
        
        
        
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeEvents]) {
        _appType = APP_EVENTS;
        
        
        [[SPSingletonClass sharedInstance] setEventViewTypeStr:_loadedTileInfo.eventViewType];
        
        NSMutableArray *arrEvents = [self fetchEventsListByTile:_loadedTileInfo];
        

        
        NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:[arrEvents valueForKey:@"categeoryId"]];
        
        if([set count] ==  1)
        {
            isTaskPresentVC = [self createNewAppEventListType:[arrEvents objectAtIndex:0]];
        }
        else if([set count] > 0)
        {
            NSString *str = [self getEventTitleByArrayItems:arrEvents];
            isTaskPresentVC = [self createNewAppTableTypeByPageTitle:str];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"%@ not available", kTypeEvents]];
        }
        
        
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeAudio]) {
        
        BOOL isDetail = NO;
        
        id detailObj = nil;
        
        for (NSDictionary *obj in [AppDelegate shareddelegate].dashboardAudiosArray) {
            if ([[obj objectForKey:@"BrandTileId"] isEqual:_loadedTileInfo.brandTileId]) {
                if (![[[obj objectForKey:@"AudioID"] stringValue] isEqualToString:@"All"]) {
                    isDetail = YES;
                    detailObj = obj;
                }
            }
            
        }
        
        if (isDetail) {
            _appType = APP_AUDIO;
            //IphoneProductID
            if ([[detailObj objectForKey:@"Fee"] isEqualToString:@"Free"]) {
                isTaskPresentVC = [self createNewAppAudioDetailType:detailObj];
            }
            else {
                NSString *productIDStr = [detailObj valueForKey:@"IphoneProductID"];
                if ([[NSUserDefaults standardUserDefaults] boolForKey:productIDStr] == 1){
                    isTaskPresentVC = [self createNewAppAudioDetailType:detailObj];
                    
                }else{
                    isTaskPresentVC = [self createNewAppAudioPurchaseType:detailObj];
                    
                }
                
            }
            
        }
        else {
            NSLog(@"Directly navigate to List Screen");
            _appType = APP_AUDIO;
            isTaskPresentVC = [self createNewAppAudioType];
        }
        
        
    }
    
    //Video
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeVideo]) {
        _appType = APP_VIDEO;
        //[AppDelegate shareddelegate].selectedTileInfo = _loadedTileInfo;
        
        NSMutableArray *arrVideo = [self fetchVideoByTile:_loadedTileInfo];
        NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:[arrVideo valueForKey:@"videoCategoryId"]];
        int privateApp = _loadedTileInfo.isUserFavorite.intValue;
        if (privateApp == 1){
            
            AppDelegate *del = [AppDelegate shareddelegate];
            int localRecodingEnable =  del.appReference.islocalRecordingEnabled.intValue;
            if (localRecodingEnable == 1){
                isTaskPresentVC =[self createUserVideoFevorates];
            }else{
                [AppDelegate showAlert:[NSString stringWithFormat:@"Favorites %@ not available", kTypeVideo]];
            }
            
        }else{
            if([set count] ==  1)  //AppTable View
            {
                [[SPSingletonClass sharedInstance] setAppCategoryName:[[arrVideo objectAtIndex:0] valueForKey:@"videoCategoryName"]];
                
                if ([self.loadedTileInfo.singleVideoIdStr isEqualToString:@"All"]){
                    isTaskPresentVC = [self createVideoList:[arrVideo objectAtIndex:0]];
                    
                }else{
                    if (self.loadedTileInfo.singleVideoIdStr.length == 0){
                        [AppDelegate showAlert:[NSString stringWithFormat:@"There is no video"]];
                        
                        
                    }else{
                        
                        isTaskPresentVC = [self createVideoDetailType:self.loadedTileInfo.singleVideoIdStr];
                        
                    }
                }
                
            }
            else if([set count] > 0)
            {
                NSString *str = [self getVideoTitleByArrayItems:arrVideo];
                isTaskPresentVC = [self createNewAppTableTypeByPageTitle:str];
            }
            else{
                [AppDelegate showAlert:[NSString stringWithFormat:@"%@ not available", kTypeVideo]];
            }
        }
        
        
    }
    
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeSponsor]) {
        
        self.isSelectedSponsor = YES;
        self.appType = APP_SPONSOR;
        [self imageSelected:_loadedTileInfo];
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeProducts]) {
        
        NSMutableArray *arrProducts = [self fetchProductsListByTile:_loadedTileInfo];
        NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:[arrProducts valueForKey:@"categoryId"]];
        if([set count] ==  1)
        {
            _appType = APP_PRODUCTS;
            isTaskPresentVC = [self createNewAppProductsType:[arrProducts objectAtIndex:0]];
        }
        else if([set count] > 0)
        {
            _appType = APP_PRODUCTS;
            NSString *str = [self getProductTitleByArrayItems:arrProducts];
            isTaskPresentVC = [self createNewAppTableTypeByPageTitle:str];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"%@ not available", kTypeProducts]];
        }
    }
    
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeMedia]) {
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Listing" deviceType:@"iOS"];
        
        _appType = APP_MEDIA;
        isTaskPresentVC = [self createNewAppTableType];
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeSocial]) {
        _appType = APP_SOCIAL;
        isTaskPresentVC = [self createNewAppWebURLType];
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeShare]) {
        _appType = APP_SHARE;
        self.socialASheet = [[[SocialActionSheet alloc] initWithDelegate:self] autorelease];
        _socialASheet.appType = _appType;
        _socialASheet.widgetInfo = _loadedTileInfo;
        [_socialASheet presentSocialActionSheet:YES];
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeFundraising]) {
        _appType = APP_FUNDRAISING;
        isTaskPresentVC = [self createNewAppTableType];
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeVolunteer]) {
        _appType = APP_VOLUNTEER;
        isTaskPresentVC = [self createNewAppTableType];
    }
    
    // Sparity
    
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeCoupons])
    {
        //_appType = APP_COUPONS;
        
        NSMutableArray *arrCoupones = [self fetchCouponesListByTile:_loadedTileInfo];
        
        NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:[arrCoupones valueForKey:@"couponCategoryId"]];
        
        if([set count] == 1)
        {
            
            
            _appType = APP_COUPONSLIST;
            isTaskPresentVC = [self createNewAppCouponsType:[arrCoupones objectAtIndex:0]];
        }
        else if([set count] > 0)
        {
            _appType = APP_COUPONS;
            isTaskPresentVC = [self createNewAppCouponType];
        }
        else{
            [AppDelegate showAlert:[NSString stringWithFormat:@"%@ not available", kTypeProducts]];
        }
        
    }
    
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeDonate]) {
        // self.loadedTileInfo = nil;
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Donate-Dialog" deviceType:@"iOS"];
        
        if([AppDelegate shareddelegate].appReference.isDonationsEnabled)
        {
            UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:@"This Will Take You Out Of The App And Forward You To Our Contribution Website!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil] autorelease];
            alert.tag = 122;
            [alert show];
        }
        else{
            [AppDelegate showAlert:@"Donations not available."];
        }
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeWebLink]) {
        // self.loadedTileInfo = nil;
        SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
        
        [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Weblink-Dialog" deviceType:@"iOS"];
        
        _appType = APP_WEBLINK;
        
        if([_loadedTileInfo.webLinkInApp isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            isTaskPresentVC = [self createNewAppWeblinkType];
        }
        else{
            if(self.loadedTileInfo.webLinkURL && ![self.loadedTileInfo.webLinkURL isKindOfClass:[NSNull class]] && ![self.loadedTileInfo.webLinkURL isEqualToString:@""])
            {
                
                SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
                [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Weblink-Ok" deviceType:@"iOS"];
                [self visitWeblinkURL];
            }
            else{
                [AppDelegate showAlert:@"Weblink not available."];
            }
        }
        
    }
    else if ([_loadedTileInfo.widget.widgetName isEqualToString:kTypeContribute]){
        if([AppDelegate shareddelegate].appReference.isDonationsEnabled)
        {
            UIAlertView *alert = [[[UIAlertView alloc]initWithTitle:[AppDelegate shareddelegate].appReference.brandname message:@"This Will Take You Out Of The App And Forward You To Our Contribution Website!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil] autorelease];
            alert.tag = 122;
            [alert show];
        }
        else{
            [AppDelegate showAlert:@"Donations not available."];
        }
    }

    
    else if([_loadedTileInfo.widget.widgetName isEqualToString:kTypeNoConfiguration]){
        [AppDelegate showAlert:[NSString stringWithFormat:@"Tile not configured!"] withTitle:[AppDelegate shareddelegate].appReference.brandname];
        
    }
    else{
        [AppDelegate showAlert:[NSString stringWithFormat:@"Tile not configured!"] withTitle:[AppDelegate shareddelegate].appReference.brandname];
    }
    
    if(_loadedTileInfo)
    {
        NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
        [standardDef setObject:self.loadedTileInfo.widget.widgetID forKey:kWidgetId];
        [standardDef synchronize];
    }
    
    
    //Perform VC animation
    if (isTaskPresentVC) {
        //Create navigation
        self.tileBodyNavigation = [[UINavigationController alloc] initWithRootViewController:_tileBodyVC];
        _tileBodyNavigation.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        //Present it finally
        [self presentViewController:_tileBodyNavigation animated:YES completion:^{
            
        }];
        
    }
    
    
    return;
}

#pragma mark Page Title:

- (NSString *)getProductTitleByArrayItems:(NSArray *)arr
{
    if(arr && [arr count] > 0)
    {
        DBProductWidget *prod = [arr objectAtIndex:0];
        return prod.productPageTitle;
    }
    return nil;
}
- (NSString *)getCouponsTitleByArrayItems:(NSArray *)arr
{
    
    if(arr && [arr count] > 0)
    {
        DBCouponsWidget *coupons = [arr objectAtIndex:0];
        return coupons.couponTitle;
    }
    return nil;
}

- (NSString *)getNewsTitleByArrayItems:(NSArray *)arr
{
    if(arr && [arr count] > 0)
    {
        DBNewsWidget *news = [arr objectAtIndex:0];
        return news.newsTitle;
    }
    return nil;
}
- (NSString *)getEventTitleByArrayItems:(NSArray *)arr
{
    if(arr && [arr count] > 0)
    {
        DBEventsWidget *events = [arr objectAtIndex:0];
        return events.eventTitle;
    }
    return nil;
}
//Vedio
- (NSString *)getVideoTitleByArrayItems:(NSArray *)arr
{
    if(arr && [arr count] > 0)
    {
        DBVideoWidget *video = [arr objectAtIndex:0];
        return video.videoCategoryName;
    }
    return nil;
}


#pragma mark - Social Action Sheet

-(void)socialActionSheetDidDismiss:(SocialActionSheet*)actionSheet{
    [self.socialASheet removeFromSuperview];
    self.socialASheet = nil;
}

-(void)socialActionSheet:(SocialActionSheet*)actionSheet cancelAction:(id)sender{
    
}

-(IBAction)dashboardIconAction:(UIButton*)sender{
    
    // NSLog(@"dashboardIconAction: %d", sender.tag);
    
    DBTilesInfo *tileInfo = [self cdTileInfoAtPosition:sender.tag];
    self.loadedTileInfo = tileInfo;
    
    [self animateBtnSelected:sender];
    
    // NSLog(@"tileInfo: %@ %@", tileInfo.tilename, tileInfo.position);
}


-(void)updateGraphics{

    
    logoImage.image = [UIImage imageWithContentsOfFile:[self imageLogoPath]];
    splashImg.image = [UIImage imageWithContentsOfFile:[self splashImagePath]];
    
    //  NSLog(@"THe SPLASH IMAGE : %f, %f, %f, %f", splashImg.frame.origin.x, splashImg.frame.origin.y, splashImg.frame.size.width, splashImg.frame.size.height);
    
}

-(void)updateAllDashboardGraphics{
    @synchronized(self){
        [self updateGraphics];
        [[AppDelegate shareddelegate] updateGraphics];
        [self loadDashboardUsingCoreDataEntries];
    }
}




#pragma mark THE ANIMATIONS..

- (void) startNewAnimationsForTile:(DBTilesInfo *)tile
{
    int animateIndex = [self fetchAnimatedIndexByTile:tile];
    NSMutableArray *arr = nil;
    //fetchCouponesListByTile
    if([tile.widget.widgetName isEqualToString:kTypeSponsor])
    {
        [self fetchSponsorsListByTile:tile];
    }
    else if([tile.widget.widgetName isEqualToString:kTypeProducts])
    {
        [self fetchProductsListByTile:tile];
    }else if([tile.widget.widgetName isEqualToString:kTypeCouponesNew])
    {
        [self fetchCouponesListByTile:tile];
    }
    
    if (animateIndex < [arr count]-1)
    {
        animateIndex++;
        
        [self performSelector:@selector(setImageforIndex:) withObject:[NSNumber numberWithInt:animateIndex] afterDelay:(1.0/2)];
        [self animateViewForValue:-2 byTile:tile];
        [self setAnimatedIndexWithIndex:animateIndex inTile:tile];
        
    }
    
}

-(void)animateViewForValue:(signed int)value byTile:(DBTilesInfo *)tile
{
    UIImageView *imgView = [self imgViewByTile:tile];
    [UIView animateWithDuration:0.4 animations:^(void){
        imgView.transform = CGAffineTransformMakeScale(1.0, 0.0);
    } completion:^(BOOL finished){
        [UIView animateWithDuration:0.4 animations:^(void){
            imgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        } completion:^(BOOL finished){
            [self performSelector:@selector(repeatFlipBlockinTile:) withObject:tile afterDelay:4.0];
            //   [self  animateViewForValue:-2 byTile:tile];
        }];
    }];
    
}

- (void)repeatFlipBlockinTile:(DBTilesInfo *)tile
{
    
    if([tile.widget.widgetName isEqualToString:kTypeSponsor]){
        int counter = [self fetchAnimatedIndexByTile:tile];
        NSMutableArray *arrImages = [self fetchSponsorsListByTile:tile];
        if(arrImages && [arrImages count] > 0 && (counter < [arrImages count]-1))
        {
            DBSponsorWidget *sponsr = [arrImages objectAtIndex:counter];
            if(sponsr)
            {
                UIImageView *imgView = [self imgViewByTile:tile];
                imgView.image = [UIImage imageWithContentsOfFile:[self imgTilePath:tile]];
                
            }
            [self  animateViewForValue:-2 byTile:tile];
        }
    }
    else if([tile.widget.widgetName isEqualToString:kTypeProducts]){
        int counter = [self fetchAnimatedIndexByTile:tile];
        NSMutableArray *arrImages = [self fetchProductsListByTile:tile];
        if(arrImages && [arrImages count] > 0 && (counter < [arrImages count]-1))
        {
            DBProductWidget *sponsr = [arrImages objectAtIndex:counter];
            if(sponsr)
            {
                UIImageView *imgView = [self imgViewByTile:tile];
                imgView.image = [UIImage imageWithContentsOfFile:[self imgTilePath:tile]];
                
            }
            [self  animateViewForValue:-2 byTile:tile];
        }
    }
    
    else if([tile.widget.widgetName isEqualToString:kTypeCouponesNew]){
        int counter = [self fetchAnimatedIndexByTile:tile];
        NSMutableArray *arrImages = [self fetchCouponesListByTile:tile];
        if(arrImages && [arrImages count] > 0 && (counter < [arrImages count]-1))
        {
            DBSponsorWidget *coupons = [arrImages objectAtIndex:counter];
            if(coupons)
            {
                UIImageView *imgView = [self imgViewByTile:tile];
                imgView.image = [UIImage imageWithContentsOfFile:[self imgTilePath:tile]];
                
            }
            [self  animateViewForValue:-2 byTile:tile];
        }
    }
    
}
-(void)setImageforIndex:(NSNumber*)index
{
    // UIImageView *imgView =
    // _imgvSlide.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[_arrImages objectAtIndex:index.intValue]]];
}

#pragma mark THE SPONSORED WIDGET STUFF.......


- (void) startAnimationAgain
{
    NSManagedObjectContext *context = [AppDelegate shareddelegate].managedObjectContext;
    NSArray *array =[context fetchObjectsForEntityName:kDBTilesInfo sortByKey:@"position" ascending:YES] ;
    for (DBTilesInfo *tile in array)
    {
        if([tile.widget.widgetName isEqualToString:kTypeSponsor])
        {
            [self startShowingImages:tile];
            [self setAnimatedIndexWithIndex:([[self fetchSponsorsListByTile:tile] count]-1) inTile:tile];
        }
    }
}

- (void) startProductAnimationAgain
{
    NSManagedObjectContext *context = [AppDelegate shareddelegate].managedObjectContext;
    NSArray *array =[context fetchObjectsForEntityName:kDBTilesInfo sortByKey:@"position" ascending:YES] ;
    for (DBTilesInfo *tile in array)
    {
        if([tile.widget.widgetName isEqualToString:kTypeProducts])
        {
            [self startShowingImages:tile];
            [self setAnimatedIndexWithIndex:([[self fetchProductsListByTile:tile] count]-1) inTile:tile];
        }
    }
}
//Veeru
- (void)startCouponesAnimationAgain
{
    NSManagedObjectContext *context = [AppDelegate shareddelegate].managedObjectContext;
    NSArray *array =[context fetchObjectsForEntityName:kDBTilesInfo sortByKey:@"position" ascending:YES] ;
    for (DBTilesInfo *tile in array)
    {
        
        if([tile.widget.widgetName isEqualToString:kTypeCouponesNew])
        {
            [self startShowingImages:tile];
            [self setAnimatedIndexWithIndex:([[self fetchCouponesListByTile:tile] count]-1) inTile:tile];
        }
    }
}


- (void) setAnimatedIndexWithIndex:(int)index inTile:(DBTilesInfo *)tile
{
    int tilePos = [tile.position intValue];
    switch (tilePos) {
        case 1:
            indexAnimate1 = index;
            break;
        case 2:
            indexAnimate2 = index;
            break;
        case 3:
            indexAnimate3 = index;
            break;
        case 4:
            indexAnimate4 = index;
            break;
        case 5:
            indexAnimate5 = index;
            break;
        case 6:
            indexAnimate6 = index;
            break;
        case 7:
            indexAnimate7 = index;
            break;
        case 8:
            indexAnimate8 = index;
            break;
        case 9:
            indexAnimate9 = index;
            break;
        case 10:
            indexAnimate10 = index;
            break;
        default:
            break;
    }
}

- (int) fetchAnimatedIndexByTile:(DBTilesInfo *)tile
{
    int tilePos = [tile.position intValue];
    switch (tilePos) {
        case 1:
            return indexAnimate1;
            break;
        case 2:
            return indexAnimate2;
            break;
        case 3:
            return indexAnimate3;
            break;
        case 4:
            return indexAnimate4;
            break;
        case 5:
            return indexAnimate5;
            break;
        case 6:
            return indexAnimate6;
            break;
        case 7:
            return indexAnimate7;
            break;
        case 8:
            return indexAnimate8;
            break;
        case 9:
            return indexAnimate9;
            break;
        case 10:
            return indexAnimate10;
            break;
        default:
            break;
    }
    return 0;
}
- (void)configureSponseredWidget:(UIImageView *)imgView withTile:(DBTilesInfo *)tile
{
    obj_custompopup = [[custompopup alloc] init];
    [obj_custompopup setDelegate:self];
    //  _indexCount = 0;
    [self setAnimatedIndexWithIndex:([[self fetchSponsorsListByTile:tile] count]-1) inTile:tile];
    self.sponsorTile = tile;
    // self.sponseredImageView = imgView;
    [self startShowingImages:tile];
    
}


- (void)configureProductsWidget:(UIImageView *)imgView withTile:(DBTilesInfo *)tile
{
    obj_custompopup = [[custompopup alloc] init];
    [obj_custompopup setDelegate:self];
    //  _indexCount = 0;
    [self setAnimatedIndexWithIndex:([[self fetchProductsListByTile:tile] count]-1) inTile:tile];
    //  self.sponsorTile = tile;
    // self.sponseredImageView = imgView;
    [self startShowingImages:tile];
    
}
//Veeru
- (void)configureCouponsWidget:(UIImageView *)imgView withTile:(DBTilesInfo *)tile
{
    obj_custompopup = [[custompopup alloc] init];
    [obj_custompopup setDelegate:self];
    //  _indexCount = 0;
    [self setAnimatedIndexWithIndex:([[self fetchCouponesListByTile:tile] count]-1) inTile:tile];
    //  self.sponsorTile = tile;
    // self.sponseredImageView = imgView;
    [self startShowingImages:tile];
    
}


- (CGRect)configurePopUpViewScrollFrameForAnimateTile:(DBTileSize *)size inView:(UIView *)superView
{
    int yValue = [size.height integerValue];
    int xVal = [size.width integerValue];
    int padding = 10;
    
    switch (yValue) {
        case 115:
        {
            switch (xVal) {
                case 145:
                {
                    return CGRectMake(20, _viewPopUp.imgHeader.frame.size.height+padding+_viewPopUp.imgvLarge.frame.size.height+padding, _viewPopUp.imgHeader.frame.size.width-40, yValue/1.5);
                }
                    break;
                case 300:
                {
                    return CGRectMake(20, _viewPopUp.imgHeader.frame.size.height+padding+_viewPopUp.imgvLarge.frame.size.height+padding, _viewPopUp.imgHeader.frame.size.width-40, yValue/1.5);
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 240:
        {
            switch (xVal) {
                case 145:
                {
                    return CGRectMake(20, _viewPopUp.imgHeader.frame.size.height+padding+_viewPopUp.imgvLarge.frame.size.height+padding, _viewPopUp.imgHeader.frame.size.width-40, yValue/3);
                    
                }
                    break;
                case 300:
                {
                    return CGRectMake(20, _viewPopUp.imgHeader.frame.size.height+padding+_viewPopUp.imgvLarge.frame.size.height+padding, _viewPopUp.imgHeader.frame.size.width-40, yValue/3);
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return CGRectNull;
}




- (TileType )configureTileType:(DBTileSize *)size
{
    int yValue = [size.height integerValue];
    int xVal = [size.width integerValue];
    
    switch (yValue) {
        case 115:
        {
            switch (xVal) {
                case 145:
                {
                    return TILE_TYPE_145X115;
                }
                    break;
                case 300:
                {
                    return TILE_TYPE_300X115;
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 240:
        {
            switch (xVal) {
                case 145:
                {
                    return TILE_TYPE_145X240;
                    
                }
                    break;
                case 300:
                {
                    return TILE_TYPE_300X240;
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    return TILE_TYPE_145X115;
    
}


-(void)productImageSelected:(DBTilesInfo *)tileInfo
{
    NSMutableArray *arrSpnsr = nil;
    
    if([tileInfo.widget.widgetName isEqualToString:kTypeProducts])
    {
        arrSpnsr = [self fetchProductsListByTile:tileInfo];
    }
    
    if(arrSpnsr && [arrSpnsr count] > 1)
    {
        if(_isSelectedProducts)
        {
            if (_viewPopUp != nil)
            {
                [_viewPopUp removeFromSuperview], self.viewPopUp = nil;
            }
            
            CGFloat deduction = 0.0;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                if ([UIScreen mainScreen].bounds.size.height > 480.0)
                {
                    deduction = 40.0;
                }
                else
                {
                    deduction = 20.0;
                }
            }
            
            _viewPopUp = [[[PopUpView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
            _viewPopUp.tileType = [self configureTileType:_loadedTileInfo.tileSize];
            _viewPopUp.tileInfo = _loadedTileInfo;
            _viewPopUp.arrSponsors = arrSpnsr; //[NSArray arrayWithArray:[_loadedTileInfo.widget.sponsor allObjects]];
            _viewPopUp.isFirstTime = YES;
            [_viewPopUp setContentViewWithFrame:CGRectMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0, 0.0, 0.0 )];
            _viewPopUp.contentView.hidden = YES;
            _viewPopUp.delegate = self;
            _viewPopUp.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
            _viewPopUp.backgroundColor = [UIColor clearColor];
            
            _viewPopUp.autoresizesSubviews = YES;
            _viewPopUp.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            [self.view addSubview:_viewPopUp];
            
            UIView *shieldView = [[[UIView alloc] initWithFrame:_viewPopUp.bounds] autorelease];
            shieldView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7];
            shieldView.tag = 1004;
            [_viewPopUp.superview insertSubview:shieldView belowSubview:_viewPopUp];
            
            
            _viewPopUp.contentView.hidden = NO;
            [UIView animateWithDuration:0.2 animations:^(void){
                _viewPopUp.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
                
                _viewPopUp.contentView.frame = CGRectMake(10,(_viewPopUp.frame.size.height-(_viewPopUp.imgHeader.frame.size.height + _viewPopUp.imgvLarge.frame.size.height + _viewPopUp.scrlView.frame.size.height + 3*10))/2, 300, _viewPopUp.imgHeader.frame.size.height + _viewPopUp.imgvLarge.frame.size.height + _viewPopUp.scrlView.frame.size.height + 3*10);
                
                
            } completion:^(BOOL finished)
             {
                 //   [_viewPopUp.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[AppDelegate shareddelegate] imageBGPath]]]] ;
                 
                 _viewPopUp.layer.cornerRadius = 12.0;
                 
                 
                 int index = 0;
                 
                 int animatedIndex = [self fetchAnimatedIndexByTile:tileInfo];
                 if(animatedIndex == 0)
                 {
                     index = [arrSpnsr count] - 1;
                 }
                 else{
                     index = animatedIndex - 1;
                 }
                 
                 if(_isProductsStarted)
                 {
                     index++;
                     if(index > [arrSpnsr count]-1)
                     {
                         index = 0;
                     }
                 }
                 
                 
                 
                 
                 DBProductWidget *prod = [arrSpnsr objectAtIndex:index];
                 _viewPopUp.selectedProduct = prod;
                 [_viewPopUp setImageViewWithImagePath:[self imgProductsPath:tileInfo withProducts:prod] title:prod.productName];
                 
                 CGRect frameForScrollView = [self configurePopUpViewScrollFrameForAnimateTile:_loadedTileInfo.tileSize inView:_viewPopUp];
                 
                 [_viewPopUp setScrollViewwithFrame:frameForScrollView withTile:tileInfo withSelectedIndex:index isClickable:YES targetForClickableSelector:self selectorOnClick:@selector(navigatetoLargerImage:)];
             }];
        }
    }
    else if(arrSpnsr && [arrSpnsr count] == 1){
        [self selectedImageInThePopupView:[arrSpnsr objectAtIndex:0]];
    }
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[tileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Sponsor-Popup" deviceType:@"iOS"];
}


//Sparity
-(BOOL)createNewAppCouponType
{
    self.appCouponsVC = [[[SPCouponsViewController alloc] initWithNibName:@"SPCouponsViewController" bundle:nil] autorelease];
    _appCouponsVC.widgetConstantType = @"Coupons";
    _appCouponsVC.widgetTileInfo = _loadedTileInfo;
    self.tileBodyVC = _appCouponsVC;
    
    _appCouponsVC.appType = _appType;
    return YES;
}
-(BOOL)createNewAppCouponsListType
{
    SPCouponsListViewController *appCouponsListVC = [[[SPCouponsListViewController alloc]initWithNibName:@"SPCouponsListViewController" bundle:nil]autorelease];
    
    //    appCouponsListVC.widgetConstantType = @"CouponsList";
    //    appCouponsListVC.widgetTileInfo = _loadedTileInfo;
    self.tileBodyVC = appCouponsListVC;
    appCouponsListVC.appType = _appType;
    return YES;
}

-(BOOL)createNewAppCouponsDetailsType
{
    SPCouponDetailsViewController *appCouponsDetailsVC = [[SPCouponDetailsViewController alloc]initWithNibName:@"SPCouponDetailsViewController" bundle:nil];
    appCouponsDetailsVC.boolNoListing = YES;
    
    self.appCouponDetailVC = appCouponsDetailsVC;
    self.appCouponDetailVC.appType = _appType;
    self.tileBodyVC = _appCouponDetailVC;
    _appCouponDetailVC.tileInfo = _loadedTileInfo;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    self.appProductList.detailDataDictionary = dic;
    return YES;
    
}
//Veeru

-(void)couponsImageSelected:(DBTilesInfo *)tileInfo
{
    NSMutableArray *arrCoupones = nil;
    
    if([tileInfo.widget.widgetName isEqualToString:kTypeCouponesNew])
    {
        arrCoupones = [self fetchCouponesListByTile:tileInfo];
    }
    
    if(arrCoupones && [arrCoupones count] > 1)
    {
        if(_isSelectedProducts)
        {
            if (_viewPopUp != nil)
            {
                [_viewPopUp removeFromSuperview], self.viewPopUp = nil;
            }
            
            CGFloat deduction = 0.0;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                if ([UIScreen mainScreen].bounds.size.height > 480.0)
                {
                    deduction = 40.0;
                }
                else
                {
                    deduction = 20.0;
                }
            }
            
            _viewPopUp = [[[PopUpView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
            _viewPopUp.tileType = [self configureTileType:_loadedTileInfo.tileSize];
            _viewPopUp.tileInfo = _loadedTileInfo;
            _viewPopUp.arrSponsors = arrCoupones; //[NSArray arrayWithArray:[_loadedTileInfo.widget.sponsor allObjects]];
            _viewPopUp.isFirstTime = YES;
            [_viewPopUp setContentViewWithFrame:CGRectMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0, 0.0, 0.0 )];
            _viewPopUp.contentView.hidden = YES;
            _viewPopUp.delegate = self;
            _viewPopUp.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
            _viewPopUp.backgroundColor = [UIColor clearColor];
            
            _viewPopUp.autoresizesSubviews = YES;
            _viewPopUp.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            [self.view addSubview:_viewPopUp];
            
            UIView *shieldView = [[[UIView alloc] initWithFrame:_viewPopUp.bounds] autorelease];
            shieldView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7];
            shieldView.tag = 1004;
            [_viewPopUp.superview insertSubview:shieldView belowSubview:_viewPopUp];
            
            
            _viewPopUp.contentView.hidden = NO;
            [UIView animateWithDuration:0.2 animations:^(void){
                _viewPopUp.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
                
                _viewPopUp.contentView.frame = CGRectMake(10,(_viewPopUp.frame.size.height-(_viewPopUp.imgHeader.frame.size.height + _viewPopUp.imgvLarge.frame.size.height + _viewPopUp.scrlView.frame.size.height + 3*10))/2, 300, _viewPopUp.imgHeader.frame.size.height + _viewPopUp.imgvLarge.frame.size.height + _viewPopUp.scrlView.frame.size.height + 3*10);
                
                
            } completion:^(BOOL finished)
             {
                 //   [_viewPopUp.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[AppDelegate shareddelegate] imageBGPath]]]] ;
                 
                 _viewPopUp.layer.cornerRadius = 12.0;
                 
                 
                 int index = 0;
                 
                 int animatedIndex = [self fetchAnimatedIndexByTile:tileInfo];
                 if(animatedIndex == 0)
                 {
                     index = [arrCoupones count] - 1;
                 }
                 else{
                     index = animatedIndex - 1;
                 }
                 
                 if(_isProductsStarted)
                 {
                     index++;
                     if(index > [arrCoupones count]-1)
                     {
                         index = 0;
                     }
                 }
                 
                 DBCouponsWidget *coupons =[arrCoupones objectAtIndex:index];
                 _viewPopUp.selectedCoupon = coupons;
                 [_viewPopUp setImageViewWithImagePath:[self imgCouponsPath:tileInfo withCoupons:coupons] title:coupons.couponCategoryName];
                 
                 CGRect frameForScrollView = [self configurePopUpViewScrollFrameForAnimateTile:_loadedTileInfo.tileSize inView:_viewPopUp];
                 [_viewPopUp setScrollViewwithFrame:frameForScrollView withTile:tileInfo withSelectedIndex:index isClickable:YES targetForClickableSelector:self selectorOnClick:@selector(navigatetoLargerImage:)];
                 
             }];
        }
    }
    else if(arrCoupones && [arrCoupones count] == 1){
        [self selectedImageInThePopupView:[arrCoupones objectAtIndex:0]];
    }
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[tileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Sponsor-Popup" deviceType:@"iOS"];
}



-(void)imageSelected:(DBTilesInfo *)tileInfo
{
    NSMutableArray *arrSpnsr = nil;
    
    if([tileInfo.widget.widgetName isEqualToString:kTypeSponsor])
    {
        arrSpnsr = [self fetchSponsorsListByTile:tileInfo];
    }
    
    if(arrSpnsr && [arrSpnsr count] > 1)
    {
        if(_isSelectedSponsor)
        {
            if (_viewPopUp != nil)
            {
                [_viewPopUp removeFromSuperview], self.viewPopUp = nil;
            }
            
            CGFloat deduction = 0.0;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                if ([UIScreen mainScreen].bounds.size.height > 480.0)
                {
                    deduction = 40.0;
                }
                else
                {
                    deduction = 20.0;
                }
            }
            
            _viewPopUp = [[[PopUpView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
            _viewPopUp.tileType = [self configureTileType:_loadedTileInfo.tileSize];
            _viewPopUp.tileInfo = _loadedTileInfo;
            _viewPopUp.arrSponsors = arrSpnsr; //[NSArray arrayWithArray:[_loadedTileInfo.widget.sponsor allObjects]];
            _viewPopUp.isFirstTime = YES;
            [_viewPopUp setContentViewWithFrame:CGRectMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0, 0.0, 0.0 )];
            _viewPopUp.contentView.hidden = YES;
            _viewPopUp.delegate = self;
            _viewPopUp.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.9];
            _viewPopUp.backgroundColor = [UIColor clearColor];
            
            _viewPopUp.autoresizesSubviews = YES;
            _viewPopUp.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            [self.view addSubview:_viewPopUp];
            
            UIView *shieldView = [[[UIView alloc] initWithFrame:_viewPopUp.bounds] autorelease];
            shieldView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7];
            shieldView.tag = 1004;
            [_viewPopUp.superview insertSubview:shieldView belowSubview:_viewPopUp];
            
            
            _viewPopUp.contentView.hidden = NO;
            [UIView animateWithDuration:0.2 animations:^(void){
                _viewPopUp.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
                
                _viewPopUp.contentView.frame = CGRectMake(10,(_viewPopUp.frame.size.height-(_viewPopUp.imgHeader.frame.size.height + _viewPopUp.imgvLarge.frame.size.height + _viewPopUp.scrlView.frame.size.height + 3*10))/2, 300, _viewPopUp.imgHeader.frame.size.height + _viewPopUp.imgvLarge.frame.size.height + _viewPopUp.scrlView.frame.size.height + 3*10);
                
                
            } completion:^(BOOL finished)
             {
                 //   [_viewPopUp.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[AppDelegate shareddelegate] imageBGPath]]]] ;
                 
                 _viewPopUp.layer.cornerRadius = 12.0;
                 
                 
                 int index = 0;
                 
                 int animatedIndex = [self fetchAnimatedIndexByTile:tileInfo];
                 if(animatedIndex == 0)
                 {
                     index = [arrSpnsr count] - 1;
                 }
                 else{
                     index = animatedIndex - 1;
                 }
                 
                 if(_isSponsorStarted)
                 {
                     index++;
                     if(index > [arrSpnsr count]-1)
                     {
                         index = 0;
                     }
                 }
                 
                 
                 
                 
                 DBSponsorWidget *sponsor = [arrSpnsr objectAtIndex:index];
                 _viewPopUp.selectedSponsor = sponsor;
                 [_viewPopUp setImageViewWithImagePath:[self imgSponsorPath:tileInfo withSponsor:sponsor] title:sponsor.title];
                 
                 CGRect frameForScrollView = [self configurePopUpViewScrollFrameForAnimateTile:_loadedTileInfo.tileSize inView:_viewPopUp];
                 
                 [_viewPopUp setScrollViewwithFrame:frameForScrollView withTile:tileInfo withSelectedIndex:index isClickable:YES targetForClickableSelector:self selectorOnClick:@selector(navigatetoLargerImage:)];
             }];
        }
    }
    else if(arrSpnsr && [arrSpnsr count] == 1){
        [self selectedImageInThePopupView:[arrSpnsr objectAtIndex:0]];
    }
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[tileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Sponsor-Popup" deviceType:@"iOS"];
}


-(void)navigatetoLargerImage:(id)sender
{
    NSLog(@"Called....");
    
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer*)sender;
    UIImageView *holderImageview = (UIImageView*)[tapGesture view];
}


- (void)closePopUpView
{
    [self removeEpaperView:nil];
}

- (void)selectedImageInThePopupView:(id)object
{
    if(_sponsorVC)
    {
        self.sponsorVC = nil;
    }
    self.sponsorVC = [[[SponsorVC alloc] initWithNibName:@"SponsorVC" bundle:nil] autorelease];
    
    
    _sponsorVC.widgetConstantType = @"Detail";
    _sponsorVC.widgetTileInfo = _loadedTileInfo;
    
    self.tileBodyVC = _sponsorVC;
    
    self.tileBodyNavigation = [[UINavigationController alloc] initWithRootViewController:_tileBodyVC];
    _tileBodyNavigation.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _tileBodyNavigation.navigationBar.hidden = YES;
    if([object isKindOfClass:[DBSponsorWidget class]])
    {
        DBSponsorWidget *sponsor = (DBSponsorWidget *)object;
        
        _sponsorVC.appType = APP_SPONSOR;
        //        NSLog(@"The sponsered content is : %@ & sponsor id is : %@", sponsor,sponsor.sponsorId );
        
        _sponsorVC.itemId = sponsor.sponsorId;
    }
    else if([object isKindOfClass:[DBProductWidget class]])
    {
        DBProductWidget *prod = (DBProductWidget *)object;
        _sponsorVC.appType = APP_PRODUCTS;
        //        NSLog(@"The products content is : %@ & sponsor id is : %@", prod,prod.productId );
        
        _sponsorVC.itemId = prod.productId;
    }
    //Sparity
    else if([object isKindOfClass:[DBCouponsWidget class]])
    {
        DBCouponsWidget *coupons = (DBCouponsWidget *)object;
        _sponsorVC.appType = APP_COUPONS;
        //        NSLog(@"The Coupons content is : %@ & coupon id is : %@", coupons,coupons.couponId );
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber * myNumber = [f numberFromString:coupons.couponId];
        
        _sponsorVC.itemId = myNumber;//coupons.couponId;//coupons.couponId;
    }
    
    //  SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    // [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:@"Sponsor-Detail" deviceType:@"iOS"];
    
    //Present it finally
    [self presentViewController:_tileBodyNavigation animated:YES completion:^{
        
    }];
    
    
    //  _appAboutUs.appType = APP_ABOUT;
    [self removeEpaperView:nil];
    
    
    
}

-(void)removeEpaperView:(UIButton*)sender
{
    //sender.hidden = YES;
    self.isSelectedSponsor = NO;
    self.isSelectedProducts = NO;
    self.isSelectedCoupons = NO;
    [self removeEpaper];
}

-(void)removeEpaper
{
    if (_viewPopUp != nil)
    {
        [UIView animateWithDuration:0.2 animations:^(void){
            
            for (UIView *view in [_viewPopUp.contentView subviews])
            {
                if (![view isKindOfClass:[UIButton class]])
                {
                    [view removeFromSuperview];
                }
            }
            
            _viewPopUp.contentView.frame = CGRectMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0, 0.0, 0.0 );
        } completion:^(BOOL finished)
         {
             [_viewPopUp removeFromSuperview], _viewPopUp = nil;
             
             UIView *viShieldView = (UIView *)[self.view viewWithTag:1004];
             if(viShieldView)
             {
                 [viShieldView removeFromSuperview];
             }
         }];
    }
    SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    
    int widgetId;
    
    if(!_loadedTileInfo.widget)
    {
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
        widgetId = [[std objectForKey:kWidgetId] intValue];
    }
    else{
        widgetId = [_loadedTileInfo.widget.widgetID intValue];
    }
    
    
    [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:widgetId widgetItemId:0 widgetType:@"Sponsor-Popup" deviceType:@"iOS"];
}


-(void)handlePopUpAction
{
    NSLog(@"Inside handlePopUpAction");
}


#pragma Products dynamic stuff.....


- (NSMutableArray *)fetchProductsListByTile:(DBTilesInfo *)tile
{
    [[NSUserDefaults standardUserDefaults] setValue:tile.categoryListTitle forKey:@"CategoryListTitle"];
    [[NSUserDefaults standardUserDefaults] setValue:tile.brandTileId forKey:@"ProductBrandTileId"];

    //brandTileId
    
    NSMutableArray *arrProducts = [NSMutableArray arrayWithArray:[tile.widget.products allObjects]];
    
    //   NSLog(@"*********** The sorted array content :%@", arrSponsors);
    
    NSSortDescriptor *aSortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"ProductId" ascending:NO] autorelease];
    [arrProducts sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    return arrProducts;
}

#pragma News dynamic stuff.....

- (NSMutableArray *)fetchNewListByTile:(DBTilesInfo *)tile
{
    
    
    NSMutableArray *arrNews = [NSMutableArray arrayWithArray:[tile.widget.news allObjects]];
    //   NSLog(@"*********** The sorted array content :%@", arrSponsors);
    
    NSSortDescriptor *aSortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"newsID" ascending:NO] autorelease];
    [arrNews sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    return arrNews;
}

- (BOOL)createNewAppNewsType:(DBNewsWidget *)news
{
    
    NewListVC *appNewsListVC = [[NewListVC alloc]initWithNibName:@"NewListVC" bundle:nil];
    appNewsListVC.boolNoListing = YES;

    self.tileBodyVC = appNewsListVC;
    appNewsListVC.appType = _appType;
    
    if([news isKindOfClass:[DBNewsWidget class]])
    {
        NSString *tempStr = [NSString stringWithFormat:@"%@",news.categoryID];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:tempStr forKey:@"NewsCategeoryId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
    return YES;
    
}
#pragma events dynamic stuff.....

- (NSMutableArray *)fetchEventsListByTile:(DBTilesInfo *)tile
{
    
    NSMutableArray *arrEvents = [NSMutableArray arrayWithArray:[tile.widget.events allObjects]];
    [[NSUserDefaults standardUserDefaults] setValue:tile.brandTileId forKey:@"ProductBrandTileId"];

    
    
    NSSortDescriptor *aSortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"eventsId" ascending:NO] autorelease];
    [arrEvents sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    return arrEvents;
}


#pragma sponsors dynamic stuff.....


- (NSMutableArray *)fetchSponsorsListByTile:(DBTilesInfo *)tile
{
    NSMutableArray *arrSponsors = [NSMutableArray arrayWithArray:[tile.widget.sponsor allObjects]];
    
    //   NSLog(@"*********** The sorted array content :%@", arrSponsors);
    
    NSSortDescriptor *aSortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"sponsorId" ascending:NO] autorelease];
    [arrSponsors sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    return arrSponsors;
}
//Veeru
#pragma coupones dynamic stuff.....


- (NSMutableArray *)fetchCouponesListByTile:(DBTilesInfo *)tile
{
    
    NSMutableArray *arrCoupons = [NSMutableArray arrayWithArray:[tile.widget.coupons allObjects]];
    
    //   NSLog(@"*********** The sorted array content :%@", arrSponsors);
    //couponsSet
    NSSortDescriptor *aSortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"couponId" ascending:NO] autorelease];
    [arrCoupons sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    return arrCoupons;
}

- (BOOL)createNewAppEventListType:(DBEventsWidget *)event{
    
    NSString *eventType = [[SPSingletonClass sharedInstance] eventViewTypeStr];
    if ([eventType isEqualToString:@"Calender"]){
        SPEventCalendarVC *eventCalendarVC = [[SPEventCalendarVC alloc] initWithNibName:@"SPEventCalendarVC" bundle:nil];
        eventCalendarVC.headerTitle = [event valueForKey:@"categeoryName"];

        [[SPSingletonClass sharedInstance] setEventDcHaderTitle:[event valueForKey:@"categeoryName"]];

        eventCalendarVC.boolNoListing = YES;
        self.tileBodyVC = eventCalendarVC;
        eventCalendarVC.appType = _appType;
        
    }else{
        EventsListVC *appNewsListVC = [[EventsListVC alloc]initWithNibName:@"EventsListVC" bundle:nil];
        appNewsListVC.boolNoListing = YES;
        self.tileBodyVC = appNewsListVC;
        appNewsListVC.appType = _appType;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:event.categeoryId forKey:@"EventsCategoryId"];
    
    return YES;
    
}
- (NSMutableArray *)fetchVideoByTile:(DBTilesInfo *)tile
{
    
    [[NSUserDefaults standardUserDefaults] setValue:tile.categoryListTitle forKey:@"CategoryListTitle"];
    
    NSMutableArray *arrEvents = [NSMutableArray arrayWithArray:[tile.widget.videos allObjects]];
    
    
    NSSortDescriptor *aSortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"videoCategoryId" ascending:NO] autorelease];
    [arrEvents sortUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    return arrEvents;
}

//Video

- (BOOL)createVideoList:(DBVideoWidget *)video
{
    
    SPVideoListVC *appNewsListVC = [[SPVideoListVC alloc]initWithNibName:@"SPVideoListVC" bundle:nil];
    appNewsListVC.boolNoListing = YES;
    self.tileBodyVC = appNewsListVC;
    appNewsListVC.appType = _appType;
    [[NSUserDefaults standardUserDefaults] setValue:video.videoCategoryId forKey:@"VideoCategoryId"];
    return YES;
    
}
// Video Details Page
- (BOOL)createVideoDetailType:(NSString *)videoID
{
    NSLog(@"videoID %@",videoID);
    SPVideoDetailVC *videoDetailVC = [[[SPVideoDetailVC alloc]initWithNibName:@"SPVideoDetailVC" bundle:nil] autorelease];
    videoDetailVC.boolNoListing = YES;
    self.tileBodyVC = videoDetailVC;
    videoDetailVC.videoID = videoID;

    videoDetailVC.isSingleVideo = @"YES";
    videoDetailVC.tileInfo = _loadedTileInfo;
    
    videoDetailVC.appType = _appType;
    //    [[NSUserDefaults standardUserDefaults] setValue:video.videoCategoryId forKey:@"VideoCategoryId"];
    return YES;
}

// UserVideo Fevorates
- (BOOL)createUserVideoFevorates
{
    //    NSLog(@"videoCategoryId %@",video.videoCategoryId);
    SPUserFavoriteVC *appUserFevVC = [[SPUserFavoriteVC alloc]initWithNibName:@"SPUserFavoriteVC" bundle:nil];
    appUserFevVC.tileInfo = _loadedTileInfo;
    
    appUserFevVC.boolNoListing = YES;
    self.tileBodyVC = appUserFevVC;
    appUserFevVC.appType = _appType;
    return YES;
}


- (UIImageView *)imgViewByTile:(DBTilesInfo *)tile
{
    if(_scrollTile)
    {
        UIButton *btn = (UIButton *)[_scrollTile viewWithTag:[tile.position integerValue]];
        if(btn)
        {
            UIImageView *imgView = (UIImageView *)[btn viewWithTag:[tile.position integerValue]+100];
            if(imgView)
            {
                return imgView;
            }
        }
        
    }
    return nil;
}

- (void)startShowingImages:(DBTilesInfo *)tile
{
    
    
    if([tile.widget.widgetName isEqualToString:kTypeSponsor])
    {
        NSMutableArray *arrSponsors = [self fetchSponsorsListByTile:tile];
        int indexVal = [self fetchAnimatedIndexByTile:tile];
        if (indexVal == ([arrSponsors count] - 1))
        {
            indexVal = 0;
        }
        else
        {
            indexVal++;
        }
        
//        NSLog(@"Index Val : %d", indexVal);
        
        if(arrSponsors && [arrSponsors count] > 1)
        {
            DBSponsorWidget *sponsor = [arrSponsors objectAtIndex:indexVal];
            [self showImage:[self imgSponsorPath:tile withSponsor:sponsor] inTile:tile];
            /*  if([tile.position intValue] % 2 == 0)
             {
             [self showImage:[self imgSponsorPath:tile withSponsor:sponsor] inTile:tile];
             }
             else{
             [self startNewAnimationsForTile:tile];
             }*/
            
            
        }
        
        [self setAnimatedIndexWithIndex:indexVal inTile:tile];
    }
    else if([tile.widget.widgetName isEqualToString:kTypeProducts])
    {
        if ([tile.imageType isEqualToString:@"Image"]){
            
        }
        else{
            NSMutableArray *arrSponsors = [self fetchProductsListByTile:tile];
            int indexVal = [self fetchAnimatedIndexByTile:tile];
            if (indexVal == ([arrSponsors count] - 1))
            {
                indexVal = 0;
            }
            else
            {
                indexVal++;
            }
            
//            NSLog(@"Index Val : %d", indexVal);
            
            if(arrSponsors && [arrSponsors count] > 1)
            {
                DBProductWidget *prod = [arrSponsors objectAtIndex:indexVal];
                [self showImage:[self imgProductsPath:tile withProducts:prod] inTile:tile];
                /*  if([tile.position intValue] % 2 == 0)
                 {
                 [self showImage:[self imgSponsorPath:tile withSponsor:sponsor] inTile:tile];
                 }
                 else{
                 [self startNewAnimationsForTile:tile];
                 }*/
                
                
            }
            
            [self setAnimatedIndexWithIndex:indexVal inTile:tile];
        }
    }
    //Veeru
    else if([tile.widget.widgetName isEqualToString:kTypeCouponesNew])
    {
        if ([tile.imageType isEqualToString:@"Image"]){
            
        }else{
            NSMutableArray *arrCoupons = [self fetchCouponesListByTile:tile];
            //NSLog(@"Coupons Array%@",arrCoupons);
            
            int indexVal = [self fetchAnimatedIndexByTile:tile];
            if (indexVal == ([arrCoupons count] - 1))
            {
                indexVal = 0;
            }
            else
            {
                indexVal++;
            }
            
            
            if(arrCoupons && [arrCoupons count] > 1)
            {
                DBCouponsWidget *coupon = [arrCoupons objectAtIndex:indexVal];
                [self showImage:[self imgCouponsPath:tile withCoupons:coupon] inTile:tile];
                
                
                
            }
            
            [self setAnimatedIndexWithIndex:indexVal inTile:tile];
            
        }
        
    }
    
}

- (int)randomNumbersFrom:(int) lowerBound to:(int) upperBound
{
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    return rndValue;
}


-(void)showImage:(NSString *)strURL inTile:(DBTilesInfo *)tile
{
    
    UIImageView *imgView = [self imgViewByTile:tile];
    
    CGFloat animat = [self randomNumbersFrom:4 to:6];
    
    
    /*
     if(([tile.position integerValue]) % 2 == 0)
     {
     animat = 4.0;
     }
     else{
     animat = 2.0;
     }
     */
    UIViewAnimationOptions animation = UIViewAnimationOptionTransitionCrossDissolve;
    
    [UIView animateWithDuration:1.0 delay:animat options:animation animations:^(void){
        imgView.alpha = 0.1;
    } completion:^(BOOL finished){
        
        if(finished)
        {
            if([tile.widget.widgetName isEqualToString:kTypeSponsor])
            {
                self.isSponsorStarted = YES;
            }else if([tile.widget.widgetName isEqualToString:kTypeProducts])
            {
                self.isProductsStarted = YES;
            }
            else if([tile.widget.widgetName isEqualToString:kTypeCouponesNew])
            {
                self.isCouponsStarted = YES;
            }
            imgView.image = [UIImage imageWithContentsOfFile:strURL];
            
            [UIView animateWithDuration:1.0 delay:0 options:animation animations:^(void){
                imgView.alpha = 1.0;
            } completion:^(BOOL finished){
                
                if(finished)
                {
                    [self startShowingImages:tile];
                    if([tile.widget.widgetName isEqualToString:kTypeSponsor])
                    {
                        self.isSponsorStarted = NO;
                    }else if([tile.widget.widgetName isEqualToString:kTypeProducts])
                    {
                        self.isProductsStarted = NO;
                    }
                    else if([tile.widget.widgetName isEqualToString:kTypeCouponesNew])
                    {
                        self.isCouponsStarted = NO;
                    }
                }
                
                
            }];
        }
        
        
    }];
}

- (void)pauseAnimation:(DBTilesInfo *)tile
{
    [self startShowingImages:tile];
}


-(void)loadDashboardUsingCoreDataEntries
{
    //[self updateAllDashboardGraphics];
    
    //[self performSelectorOnMainThread:@selector(updateAllDashboardGraphics) withObject:nil waitUntilDone:NO];
    
    //Remove old content
    
    NSArray *scrollView = _scrollTile.subviews;
    for (UIView *view in scrollView) {
        if (view.tag != 9005) {//Do not do anything to pull down to refresh view
            [view removeFromSuperview];
        }
    }
    
    NSManagedObjectContext *context = [AppDelegate shareddelegate].managedObjectContext;
    NSArray *array =[context fetchObjectsForEntityName:kDBTilesInfo sortByKey:@"position" ascending:YES] ;
    
    //CGFloat spacing = 10.0;
    
    NSInteger scrollHeight = 0;
    
    DBApp *_appRef = [AppDelegate shareddelegate].appReference;
    [[NSUserDefaults standardUserDefaults] setObject:_appRef.brandid forKey:@"BrandId"];

    
    
    headerTitle.text = @"";
    //headerTitle.text =  _appRef.brandname;
    
    if ([array count] && !didCallForPushRegister) {
        //Register for push notification
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        didCallForPushRegister = YES;
    }
    
    for(DBTilesInfo *tile in array){
        // [tile printMe];
        
        CGRect tileRect = CGRectMake([tile.x integerValue], [tile.y integerValue], [tile.tileSize.width integerValue], [tile.tileSize.height integerValue]);
        
        //Making view
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn addTarget:self action:@selector(dashboardIconAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.frame = tileRect;
        btn.tag = [tile.position integerValue];
        
        UIView *viEvent = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tileRect.size.width, tileRect.size.height)];
        viEvent.backgroundColor = [UIColor clearColor];
        viEvent.userInteractionEnabled = NO;
        
        
        if((!tile.bgcolor || [tile.bgcolor isEqualToString:@""]) && (tile.widget && ![tile.widget isKindOfClass:[NSNull class]]))
        {
            [viEvent setBackgroundColor:[UIColor clearColor]];
        }
        else{
            
            [viEvent setBackgroundColor:[UIColor colorWithHexString:tile.bgcolor defaultColor:[UIColor whiteColor]]];
            
            
            
        }
        
        
        //[viEvent setBackgroundColor:[UIColor clearColor]];
        
        //Background Image
        UIImageView *imgView = [[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[self imgTilePath:tile]]] autorelease];
        imgView.frame = CGRectMake(0, 0, viEvent.frame.size.width, viEvent.frame.size.height);
        if ([tile.widget.widgetName isEqualToString:kTypeNoConfiguration]){
            
            [imgView setBackgroundColor:[UIColor clearColor]];
            
        }else{
            [viEvent addSubview:imgView];
            
        }
        ///********** STARTING SPOSORED WIDGET CHANGES  *************///////
        
        
        if([tile.widget.widgetName isEqualToString:kTypeSponsor])
        {
            //  imgView.image = [UIImage imageNamed:@"Icon.png"];
            
            self.isSponsorStarted = YES;
            
            if([tile.widget.sponsor count] >0)
            {
                NSMutableArray *arrSpnsr = [self fetchSponsorsListByTile:tile];
                //   [imgView setImageWithURL:[NSURL URLWithString:[self imgSponsorPath:tile withSponsor:[_arrSponsoredImagelist objectAtIndex:0]]] placeholderImage:nil];
                
                
                {
                    imgView.image = [UIImage imageWithContentsOfFile:[self imgSponsorPath:tile withSponsor:[arrSpnsr objectAtIndex:0]]];
                }
                imgView.tag = [tile.position integerValue]+100;
                
                
                [self configureSponseredWidget:imgView withTile:tile];
            }
        }
        
        if([tile.widget.widgetName isEqualToString:kTypeProducts])
        {
            if ([tile.imageType isEqualToString:@"Image"]){
                
            }else{
                self.isProductsStarted = YES;
                
                if([tile.widget.products count] >0)
                {
                    NSMutableArray *arrSpnsr = [self fetchProductsListByTile:tile];
                    //  [imgView setImageWithURL:[NSURL URLWithString:[self imgSponsorPath:tile withSponsor:[_arrSponsoredImagelist objectAtIndex:0]]] placeholderImage:nil];
                    
                    
                    {
                        imgView.image = [UIImage imageWithContentsOfFile:[self imgProductsPath:tile withProducts:[arrSpnsr objectAtIndex:0]]];
                    }
                    imgView.tag = [tile.position integerValue]+100;
                    
                    
                    [self configureProductsWidget:imgView withTile:tile];
                }
                
                
            }
        }
        
        //Veeru
        if([tile.widget.widgetName isEqualToString:kTypeCouponesNew])
        {
            //tile.imageType = @"marq";
            
            if ([tile.imageType isEqualToString:@"Image"]){
                
            }else{
                //Marque feature
                self.isCouponsStarted = YES;//couponsSet
                if([tile.widget.coupons count] >0)
                {
                    NSMutableArray *arrSpnsr = [self fetchCouponesListByTile:tile];
                    [imgView setImageWithURL:[NSURL URLWithString:[self imgCouponsPath:tile withCoupons:[_arrSponsoredImagelist objectAtIndex:0]]] placeholderImage:nil];
                    
                    
                    {
                        imgView.image = [UIImage imageWithContentsOfFile:[self imgCouponsPath:tile withCoupons:[arrSpnsr objectAtIndex:0]]];
                    }
                    imgView.tag = [tile.position integerValue]+100;
                    
                    
                    [self configureCouponsWidget:imgView withTile:tile];
                }
            }
            
        }
        
        
        ////*****  THE END ***********///////
        
        
        //Label
        
        UILabel *label = [[[UILabel alloc] init] autorelease];
        label.numberOfLines = 1;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithHexString:tile.titleColor defaultColor:[UIColor blackColor]];
        
        (tile.tilename)?(label.text = tile.tilename):(label.text = [NSString stringWithFormat:@"Tile%@", tile.titlePosition]);
        
        CGFloat labelFont = 20.0f;
        (tile.titleFontSize)?(labelFont = [tile.titleFontSize floatValue]):16;
        [label setFont:[UIFont fontWithName:tile.titleFont size:labelFont]];
        
        NSInteger labelOffset = 3;
        
        CGRect labelFrame = CGRectMake(labelOffset, labelOffset, viEvent.frame.size.width - 2*labelOffset, viEvent.frame.size.height-2*labelOffset);
        
        
        
        label.frame = labelFrame;
        
        
        if ([tile.titlePosition isEqualToString:@"topLeftTxt"]) {
            
            // [label alignTop];
            [label realignTop:tile.titleFont];
            label.textAlignment = NSTextAlignmentLeft;
        }
        else if ([tile.titlePosition isEqualToString:@"topRightTxt"]) {
            
            // [label alignTop];
            [label realignTop:tile.titleFont];
            label.textAlignment = NSTextAlignmentRight;
        }
        else if ([tile.titlePosition isEqualToString:@"bottomLeftTxt"]) {
            // [label alignBottom];
            [label realignBottom:tile.titleFont];
            // [label realignBottom:tile.titleFont rect:tileRect];
            label.textAlignment = NSTextAlignmentLeft;
            
        }
        else{//Bottom Right default
            
            // [label alignBottom];
            //  [label realignBottom:tile.titleFont rect:tileRect];
            label.textAlignment = NSTextAlignmentRight;
            [label realignBottom:tile.titleFont];
        }
        
        
        //Live content
        if ([tile.tiletype isEqualToString:@"Live Content"]) {
            UILabel *liveContent = [[[UILabel alloc] init] autorelease];
            liveContent.numberOfLines = 2;
            liveContent.minimumScaleFactor = (tile.titleFontSize)?[tile.titleFontSize integerValue]:16;
            liveContent.backgroundColor = [UIColor clearColor];
            liveContent.textColor = [UIColor colorWithHexString:tile.titleColor defaultColor:[UIColor blackColor]];
            
            liveContent.text = tile.widget.liveTileInfo;
            liveContent.font = [UIFont fontWithName:tile.titleFont size:(tile.titleFontSize)?[tile.titleFontSize integerValue]:14];
            
            liveContent.frame = labelFrame;
            [liveContent alignTop];
            [viEvent addSubview:liveContent];
        }
        
        if ([tile.widget.widgetName isEqualToString:@"Media"] ) {
            if (!tile.bgimage || [tile.bgimage length] <= 0) {
                //imgView.image = [UIImage imageNamed:@"videoThumb"];
            }
        }
        //Hide tile if not required
        if(!tile.widget.widgetName){
            //  btn.hidden = YES;
            [viEvent setBackgroundColor:[UIColor colorWithHexString:tile.defaultBgColor defaultColor:[UIColor colorWithRed:0.502 green:0.799 blue:1.000 alpha:1.000]]];
            [label setFont:[UIFont fontWithName:@"Helvetica-Condensed" size:18.0f]];
            NSInteger offSet = 6.0;
            label.frame = CGRectMake(offSet, offSet, viEvent.frame.size.width - 2*offSet, 18*label.numberOfLines);
            label.text = [NSString stringWithFormat:@"Tile%@", tile.position];
            [label resizeToFit];
            
            [label setTextColor:[UIColor colorWithHexString:tile.defaultTitleColor defaultColor:[UIColor blackColor]]];
            label.textAlignment = NSTextAlignmentRight;
            
        }
        [viEvent addSubview:label];
        
        //Add it
        
        [btn addSubview:viEvent];
        [_scrollTile addSubview:btn];
        [viEvent release];
        
        //   NSLog(@"custom tile height : %d -- scrollHeight: %d ", [tile.y integerValue]+ [tile.tileSize.height integerValue] + 10 ,scrollHeight);
        
        scrollHeight = MAX(scrollHeight, [tile.y integerValue]+ [tile.tileSize.height integerValue] + 10);
        
        //     NSLog(@"scrollHeight: %@: %d ", tile.position ,scrollHeight);
        
    }
    
    //Scroll View frame
    
    CGRect withoutLogoContainer = CGRectMake(0, 0, 320, 480); //fix : 170
    CGRect withLogoContainer = CGRectMake(0, 75, 320, 405);
    
    if([AppDelegate shareddelegate].window.frame.size.height == 568)
    {
        withoutLogoContainer = CGRectMake(0, 0, 320, 568); //fix : 170
        withLogoContainer = CGRectMake(0, 75, 320, 493);
    }
    if ([_appRef.layoutInfo.hasLogoContainer boolValue] && kEnableDashboardHeader) {
        _scrollTile.frame = withLogoContainer;
        headerView.hidden = NO;
    }
    else{
        _scrollTile.frame = withoutLogoContainer;
        headerView.hidden = YES;
    }
    
    //Set scrolling
    _scrollTile.contentSize = CGSizeMake(300, scrollHeight + 20);
    
    //[_scrollTile scrollRectToVisible:CGRectMake(0, _scrollTile.contentSize.height - 100, _scrollTile.contentSize.width, 100) animated:NO];
    
    //[_scrollTile scrollRectToVisible:CGRectMake(0, 580, 320, 480) animated:YES];
    
    // NSLog(@"_scrollTile.contentSize: %@", NSStringFromCGSize(_scrollTile.contentSize));
    
}



-(IBAction)showPopUp:(id)sender
{
    
    if(viPopUp.hidden)
    {
        viPopUp.hidden = NO;
    }
    else {
        viPopUp.hidden = YES;
    }
}
-(void)addPopUp
{
    viPopUp = [[UIView alloc]initWithFrame:CGRectMake(123, 40, 185, 100)];
    [viPopUp setBackgroundColor:[UIColor redColor]];
    
    UIImageView *imgView = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pop-up-layer.png"]] autorelease];
    imgView.frame = CGRectMake(0, 0, 185, 90);
    [viPopUp addSubview:imgView];
    viPopUp.hidden = YES;
    
    ////// FaceBook Button
    
    UIImage* imgBtn = [UIImage imageNamed:@"pop-up-button.png"];
    CGRect frameBtn = CGRectMake(7, 20, imgBtn.size.width, imgBtn.size.height);
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"pop-up-button.png"] forState:UIControlStateNormal];
    [btn setTitle:@"FaceBook" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    btn.frame = frameBtn;
    [btn addTarget:self action:@selector(showAlert:) forControlEvents:UIControlEventTouchUpInside];
    [btn setShowsTouchWhenHighlighted:YES];
    [viPopUp addSubview:btn];
    
    ///// Twitter Button
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setBackgroundImage:[UIImage imageNamed:@"pop-up-button.png"] forState:UIControlStateNormal];
    btn2.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [btn2 addTarget:self action:@selector(showAlert:) forControlEvents:UIControlEventTouchUpInside];
    [btn2 setTitle:@"FaceBook" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    CGRect frameBtn2 = CGRectMake(7, 55 , imgBtn.size.width, imgBtn.size.height);
    btn2.frame = frameBtn2;
    [btn2 setShowsTouchWhenHighlighted:YES];
    [viPopUp addSubview:btn2];
    
    [self.view addSubview:viPopUp];
}


-(void)showAlert:(id)sender
{
    [AppDelegate showAlert:@"Functionality not implemented"];
    [self showPopUp:nil];
}


#pragma mark - InfoCollector Delegate

-(void)insertInfoVCAnimated{
    self.infoCollVC = [[[InfoCollectorVC alloc] initWithNibName:@"InfoCollectorVC" bundle:nil] autorelease];
    _infoCollVC.delegate = self;
    [self.view addSubview:_infoCollVC.view];
    
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:0.2],
                             [NSNumber numberWithFloat:1.2],
                             [NSNumber numberWithFloat:.9],
                             [NSNumber numberWithFloat:1],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.5],
                               [NSNumber numberWithFloat:0.7],
                               [NSNumber numberWithFloat:1.0],
                               nil];
    //popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"addTestVC"];
}

-(void)removeInfoVCAnimated{
    CALayer *viewLayer = _infoCollVC.view.layer;
    CAKeyframeAnimation* popInAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    popInAnimation.duration = 0.3;
    popInAnimation.values = [NSArray arrayWithObjects:
                             [NSNumber numberWithFloat:1.0],
                             [NSNumber numberWithFloat:0.0],
                             nil];
    popInAnimation.keyTimes = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:1.0],
                               nil];
    popInAnimation.delegate = self;
    
    [viewLayer addAnimation:popInAnimation forKey:@"removeTestVC"];
}

-(void)infoCancel:(id)sender{
    
    //    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    //    [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[[AppDelegate shareddelegate].selectedTileInfo.widget.widgetID widgetItemId:[self.itemId intValue] action:@"Cancelled" deviceType:@"iOS"];
    
    [self removeInfoVCAnimated];
}

-(void)infoProceed:(id)sender firstName:(NSString*)firstName lastName:(NSString*)lastName gender:(NSString*)gender mobile:(NSString*)mobile email:(NSString*)email{
    
    //    SDZEXAnalyticsServiceExample* example1 = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
    //    [example1 runAddInfoAction:[[AppDelegate shareddelegate].appReference.brandid intValue] widgetId:[self.tileInfo.widget.widgetID intValue] widgetItemId:[self.itemId intValue] action:@"Cancelled" deviceType:@"iOS"];
    
    [self removeInfoVCAnimated];
    [self performSelector:@selector(visitDonateSite) withObject:nil afterDelay:0.4];
}

#pragma mark Alert View Delegate
-(void)visitDonateSite{
    
    NSString *urlString = nil;
    
    NSArray *arrDonateURL = [[AppDelegate shareddelegate].appReference.donateURL componentsSeparatedByString:@"exceedings"];
    if([AppDelegate shareddelegate].appReference.donateURL && ![[AppDelegate shareddelegate].appReference.donateURL isEqualToString:@""] && [arrDonateURL count] >= 2)
    {
        urlString = [NSString stringWithFormat:kURLDonation, [AppDelegate shareddelegate].appReference.donateURL,[AppDelegate shareddelegate].appReference.brandid,[AppDelegate shareddelegate].appReference.brandname, @"Donation", @"0"];
    }
    else{
        urlString = [AppDelegate shareddelegate].appReference.donateURL;
    }
    
    NSString *url = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    // NSString *urlString = [[DashboardBuilder sharedDelegate] fetchURLForAppType:kTypeContribute];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

#pragma mark Alert View Delegate
-(void)visitWeblinkURL{
    
//    NSString *urlString = nil;
    
    
    //NSString *url = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    // NSString *urlString = [[DashboardBuilder sharedDelegate] fetchURLForAppType:kTypeContribute];
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.loadedTileInfo.webLinkURL]];
    
}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 122) {
        NSString *str = nil;
        if (buttonIndex == 1) {
            str = @"Donate-Ok";
            SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:str deviceType:@"iOS"];
            [self visitDonateSite];
        }
        else if(buttonIndex == 0)
        {
            str = @"Donate-Canceled";
            SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:str deviceType:@"iOS"];
        }
        if(buttonIndex == 1)
        {
            [self visitDonateSite];
        }
        
    }
    else if(alertView.tag == 123)
    {
        NSString *str = nil;
        if (buttonIndex == 1) {
            str = @"Weblink-Ok";
            SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            [service runAddEntryAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:str deviceType:@"iOS"];
            [self visitWeblinkURL];
        }
        else if(buttonIndex == 0)
        {
            str = @"Weblink-Canceled";
            SDZEXAnalyticsServiceExample *service = [[[SDZEXAnalyticsServiceExample alloc] init] autorelease];
            [service runAddExitAction:[[AppDelegate shareddelegate].appReference.brandid integerValue] widgetId:[_loadedTileInfo.widget.widgetID integerValue] widgetItemId:0 widgetType:str deviceType:@"iOS"];
        }
        
    }
    
}




- (void)viewDidUnload
{
    [self setFacebookWeb:nil];
    [self setScrollTile:nil];
    [viPopUp release];
    [self setLogoImage:nil];
    [self setHeaderTitle:nil];
    [self setHeaderView:nil];
    [self setSplashImg:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    //webView.backgroundColor = [UIColor clearColor];
    //webView.opaque = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}

/*
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
 {
 return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
 }
 */

- (void)dealloc {
    [facebookWeb release];
    if(viPopUp)
    {
        [viPopUp release];
    }
    self.scrollTile = nil;
    [logoImage release];
    [headerTitle release];
    [headerView release];
    [splashImg release];
    [super dealloc];
}
- (IBAction)actRefresh:(id)sender {
    [DashboardBuilder sharedDelegate].refreshMode = @"Pulldown";
    [[DashboardBuilder sharedDelegate] updateUsingWebService];
}



@end
