//
//  ConstantsShare.h
//  Exceeding
//
//  Created by Betrand Yella on 27/07/12.
//  Copyright (c) 2012 IBEE SAolutions. All rights reserved.
//

#ifndef Exceeding_ConstantsShare_h
#define Exceeding_ConstantsShare_h

    // Application Constants...

#define kApplicationMailSubj @"Check out this amazing App from %@"
#define kApplicationMailContent @"<div> <p>%@</p></div><div>You can get it here:</div>"


#define kApplicationFBAppName @"App Title"
#define kApplicationFBAppLink @"AppLink"
#define kApplicationFBAppDesc @"App description runs here"
#define kApplicationFBAppBody @"App Body"
#define kApplicationFBAppImage @"App image URL"
#define kApplicationFBAppCaption @"App image URL"
#define kCouponsTextContent @" "  
#define kCouponsDetailsTextContent @""
#define kCouponsListContent @""


#define kTitleLinkURL @"http://exceedings.com"

//#define kTitleLinkURL @"http://exceedings.ibeesolutions.com"


#define kApplicationTextContent @"I just found this amazing app from %@"

#define kSMSDefaultText @"I just found this amazing app from %@. iPhone: %@ Android: %@"
#define kApplicationTweetContent @"I just found this amazing app from %@. %@"


#define kSocialMailSubj @"Check this out from '%@'!"
#define kSocialMailContent @"<div><strong>Absolutely amazing!</strong></div><br/><br/> <div><a href=\"%@\">Facebook</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"%@\">Twitter</a></div><br/>"



#define kFBFanPageURL @"http://www.facebook.com/%@"

#define kFBEventPageURL @"http://www.facebook.com/events/%@"


//#define kFBFanPageURL @"%@"

#define kTwitterFanPage @"http://www.twitter.com/%@"

//#define kTwitterFanPage @"%@"

    //  News Constants...

#define kNewsMailSubj @"A must read from %@!"

#define kNewsMailContent @"<div><p>%@</p></div><br/>"



#define kSponsorMailSubj @"Check this out from '%@'!"
#define kSponsorSMSSubj @"Check this out from '%@'! %@"
#define kSponsorMailContent @"<div><strong> Absolutely amazing! </strong></div><br/>"


#define kMediaMailSubj @"Check this out from '%@'!"
#define kMediaSMSSubj @"Check this out from '%@'! %@"
#define kMediaMailContent @"<div><strong> Absolutely amazing! </strong></div><br/>"



#define kNewsText @""

#define kNewsFBAppName @"App Title"
#define kNewsFBAppLink @"AppLink"
#define kNewsFBAppDesc @"App description runs here"
#define kNewsFBAppBody @"App Body"
#define kNewsFBAppImage @"App image URL"
#define kNewsFBAppCaption @"Caption"

#define kNewsiPhoneAppURL @""
#define kNewsAndroidAppURL @""

#define kNewsTwitterContent @"A must read; %@"


#define kNewsTextContent @"A must read; %@. %@"


// Events Constants....

#define kEventsMailSubj @"I'll be attending '%@' hosted by %@"

#define kEventsMailContent @"<div>Check this out from %@!</div><div><strong>Event Date:</strong> %@</div><div><strong>Event Start Time:</strong> %@</div><div><strong>Event End Time:</strong> %@</div><div><strong>Location:</strong> %@</div><br/><div><p><strong>Description:</strong> %@</p></div>"

#define kEventsMailRegisterFBURL @""
#define kEventsmailDownloadText @"Download their mobile"

#define kEventsFBAppName @"App Title"
#define kEventsFBAppLink @"AppLink"
#define kEventsFBAppDesc @"App description runs here"
#define kEventsFBAppBody @"App Body"
#define kEventsFBAppImage @"App image URL"
#define kEventsFBAppCAption @"Caption"


#define kEventsiPhoneAppURL @""
#define kEventsAndroidAppURL @""

#define kEventsTwitterContent @"I will be attending this event! '%@'"

#define kEventsTextContent @"I'll be attending this event and thought you might be interested in attending too. %@"




    // Fundraising Constants....

#define kFundraisingEmailSubj @"I'm supporting '%@'!"
#define kFundraisingEmailContent @"<div>I am supporting %@ by contributing to '%@' and thought you might be interested in learning more. Here are the Details: </div><br/><div><strong>Start Date:</strong> %@</div><div><strong>End Date:</strong> %@</div><div><strong> Goal:</strong> %@</div><div><strong> Raised:</strong> %@</div> <div><strong>Description:</strong><p>%@</p></div>"

#define kFundraisingText @"I'm supporting '%@'! %@"

#define kFundraisingTweet @"I'm supporting '%@' !"

#define kFundraisingFBAppName @"App Title"
#define kFundraisingFBAppLink @"AppLink"
#define kFundraisingFBAppDesc @"App description runs here"
#define kFundraisingFBAppBody @"App Body"
#define kFundraisingFBAppImage @"App image URL"
#define kFundraisingFBAppCAption @"Caption"


    // Volunteer Constants...

#define kVolunteerEmailSubj @"I wanted to share something with you."
#define kVolunteerEmailContent @"<div>I'm supporting %@ by volunteering for '%@' and thought you might be interested in learning more. Here are the Details: </div><div><strong>Start Date:</strong> %@</div><div><strong>End Date:</strong> %@</div><div><strong>Needed:</strong> %@</div><div><strong>Joined:</strong> %@</div><br/><div><strong>Description:</strong><p>%@</p></div><br/>"

#define kVolunteerText @"I'm volunteering for %@. %@"
#define kVolunteerTweet @"I'm volunteering for %@"

#define kVolunteerFBAppName @"App Title"
#define kVolunteerFBAppLink @"AppLink"
#define kVolunteerFBAppDesc @"App description runs here"
#define kVolunteerFBAppBody @"App Body"
#define kVolunteerFBAppImage @"App image URL"
#define kVolunteerFBAppCaption @"Caption"

// Audio Constants.........
// Check out "Audio Title" from "App Name"
#define kAudioEmailSubj @"Check out"
#define kAudioEmailContent @"<div>I'm supporting %@ by volunteering for '%@' and thought you might be interested in learning more. Here are the Details: </div><strong>Needed:</strong> %@</div><div><strong>Joined:</strong> %@</div><br/><div><strong>Description:</strong><p>%@</p></div><br/>"

#define kAudioText @"I'm audio for %@. %@"
#define kAudioTweet @"I'm audio for %@"

#define kAudioFBAppName @"App Title"
#define kAudioFBAppLink @"AppLink"
#define kAudioFBAppDesc @"App description runs here"
#define kAudioFBAppBody @"App Body"
#define kAudioFBAppImage @"App image URL"
#define kAudioFBAppCaption @"Caption"




    // Media Constants.....

#define kMediaEmailSubj @"Check this out from %@"
#define kMediaEmailContent @"Absolutely amazing! URL to Media Download their mobile app and see how they are making a difference"

#define kMediaTwitter @"Check this out from %@"
#define kMediaText @"Check this out from %@"

#define kMediaFBAppName @"App Title"
#define kMediaFBAppLink @"AppLink"
#define kMediaFBAppDesc @"App description runs here"
#define kMediaFBAppBody @"App Body"
#define kMediaFBAppImage @"App image URL"
#define kMediaFBAppCaption @"App Caption"



#endif
