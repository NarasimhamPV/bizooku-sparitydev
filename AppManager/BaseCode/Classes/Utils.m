//
//  Utils.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 14/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSString*)titleForAppType:(AppType)appType{
    switch (appType) {
        case APP_NONE:          return @"None";
        case APP_NEWS:          return @"News";
        case APP_ABOUT:         return @"About";
        case APP_CONTRIBUTE:    return @"Contribute";
        case APP_EVENTS:        return @"Events";
        case APP_MEDIA:         return @"Media";
        case APP_LOCATIONS:     return @"Locations";
        case APP_SOCIAL:        return @"Social";
        case APP_SHARE:         return @"Share";
        case APP_FUNDRAISING:   return @"Fundraising";
        case APP_VOLUNTEER:     return @"Volunteer";
        case APP_SPONSOR:     return @"Sponsor";
        case APP_PRODUCTS:     return @"Product";
        case APP_COUPONS:       return @"Coupons";
        case APP_COUPONSLIST: return @"CouponName";
        case APP_COUPONSDETAILS: return @"CouponId";  //SL
        case APP_AUDIO:         return @"Audios";
        case APP_FREE_AUDIO_List: return @"Free Audios";
        case APP_AUDIO_DETAIL:    return @"Audio Detail";
        case APP_AUDIO_PURCHASE:  return @"Audio Purchase";
        case APP_VIDEO:           return @"Videos";
  
        default:                return nil;
    }
}

+(NSString*)detailTitleForAppType:(AppType)appType{
    switch (appType) {
        case APP_NONE:          return @"None";
        case APP_NEWS:          return @"News";
        case APP_ABOUT:         return @"";
        case APP_CONTRIBUTE:    return @"Contribute";
        case APP_EVENTS:        return @"Event";
        case APP_MEDIA:         return @"Media";
        case APP_LOCATIONS:     return @"Location";
        case APP_SOCIAL:        return @"Social";
        case APP_SHARE:         return @"Share";
        case APP_FUNDRAISING:   return @"Fund Campaign";
        case APP_VOLUNTEER:     return @"Volunteer";
        case APP_SPONSOR:     return @"Sponsor";
        case APP_COUPONS:   return @"Coupons"; //SL
        case APP_AUDIO:         return @"Audios";
        case APP_FREE_AUDIO_List: return @"Free Audios";
        case APP_AUDIO_DETAIL:    return @"Audio Detail";
        case APP_AUDIO_PURCHASE:   return @"Audio Purchase";
        case APP_VIDEO:           return @"Videos";

        default:                return nil;
    }
}

+(void)loadFontInView:(UIView*)aMajorView{
	
	for (UIView *aView in aMajorView.subviews) {
		if([aView isKindOfClass:[UILabel class]]){
			UILabel *aLabel = (UILabel*)aView;
			[aLabel setFont:[UIFont fontWithName:kDefaultFontName size:aLabel.font.pointSize]];
		}
		else if([aView isKindOfClass:[UIButton class]]){
			UIButton *aBtn = (UIButton*)aView;
			[aBtn.titleLabel setFont:[UIFont fontWithName:kDefaultFontName size:28]];
		}
		else{
			[Utils loadFontInView:aView];
		}
	}
}


@end
