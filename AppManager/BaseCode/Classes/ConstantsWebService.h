//
//  ConstantsWebService.h
//  Exceeding
//
//  Created by VENKATALAKSHMI on 18/06/14.
//
//

#ifndef Exceeding_ConstantsWebService_h
#define Exceeding_ConstantsWebService_h

// News API

#define getNews               @"http://tempuri.org/GetNews"
#define getNewsByNewsId       @"http://tempuri.org/GetNewsByNewsId"

//Coupond API

#define  getCouponsCategeoriesByBrandTileId @"http://tempuri.org/GetCouponsCategoriesByBrandTileId"
#define getCouponsByCategeoryId             @"http://tempuri.org/GetCouponsByCategoryId"
#define getCouponsByCouponId                @"http://tempuri.org/GetCouponsByCouponId"
#define redeemCoupon                        @"http://tempuri.org/RedeemCoupon"
//Event calendar
#define getEventDatesByCategoryId                        @"http://tempuri.org/GetEventDatesByCategoryId"
#define getGetEventsByDate                        @"http://tempuri.org/GetEventsByDate"

// Background API

#define backgroundByBrandId @"http://tempuri.org/GetBackgroundsByBrandId"

// Banners API
#define getBannerByBrand    @"http://tempuri.org/GetBannersByBrand"

// Events API

#define getEvents          @"http://tempuri.org/GetEvents"
#define getEventsByEventId @"http://tempuri.org/GetEventByEventId"

//Location API

//#define getLocation             @"http://tempuri.org/GetLocations"
#define getNearestLocations             @"http://tempuri.org/GetNearestLocations"


#define getLocationByLocationId @"http://tempuri.org/GetLocationByLocationId"

// About API

#define aboutWidgetByBrand @"http://tempuri.org/GetAboutWidgetByBrand"

// Fundraising API

#define fundrasingInfoByBrand         @"http://tempuri.org/GetFundraisingInfoByBrand"
#define getFundraingInfoByCampaingId  @"http://tempuri.org/GetFundraisingInfoByCampaignId"
#define addFundRaiser                 @"http://tempuri.org/AddFundraiser"

// Volunteer API

#define getVolunteerInfoByBrand      @"http://tempuri.org/GetVolunteerInfoByBrand"
#define getVolunteerInfoByCampaingId @"http://tempuri.org/GetVolunteerInfoByCampaignId"
#define addVolunteer                 @"http://tempuri.org/AddVolunteer"

// Sponsor API

#define getSponsorBySponsorsId @"http://tempuri.org/GetSponsorsBySponsorsId"

//Product API

#define getProduct           @"http://tempuri.org/GetProducts"
#define getProdutByProductId @"http://tempuri.org/GetProductByProductId"

// DownloadInfo API

#define addDownLoadInfo @"http://tempuri.org/AddDownloadInfo"

// Update Device Token API

#define updateDeviceToken @"http://tempuri.org/UpdateDeviceToken"

// Audio API

#define getAudioByAudioId @"http://tempuri.org/GetAudioByAudioId"
#define getDudioByBrandId @"http://tempuri.org/GetAudiosByBrandId"


// Analytics

#define addSession      @"http://tempuri.org/AddSession"

#define addEntryActiion @"http://tempuri.org/AddEntryAction"

#define addExitAction   @"http://tempuri.org/AddExitAction"

#define addInfoAction   @"http://tempuri.org/AddInfoAction"

#define addMediaAction  @"http://tempuri.org/AddMediaAction"

#define addShareAction  @"http://tempuri.org/AddShareAction"

#define addVolunteerRegisterAction @"http://tempuri.org/AddVolunteerRegisterAction"

#define bannerClickedCount         @"http://tempuri.org/BannerClickCount"

//SevenDigitCodeAccessKey
#define getSevenDigitCodeAccessKey       @"http://tempuri.org/CheckSevenDigitCode"
#define getNineDigitCodeAccessKey       @"http://tempuri.org/CheckNineDigitCode"


// Layout SerVices

#define getAccessKey       @"http://tempuri.org/GetAccessKey"

#define getBrandLayoutInfo @"http://tempuri.org/GetBrandLayoutInfo"

#define brandLayoutInfo    @"http://tempuri.org/GetBrandLayoutInfo"

//New Lists for Categories API

#define getProductCategeory @"http://tempuri.org/GetProductCategoriesByBrandTileId"

#define getProductList @"http://tempuri.org/GetProductByPrdtCatId"
// News
#define getNewsCategeoriesByBrandTileId @"http://tempuri.org/GetNewsCategoriesByBrandTileId"
#define getNewsList           @ "http://tempuri.org/GetNewsByNewsCatId"
//Events
#define getEventsCategeory          @"http://tempuri.org/GetEventsCategoriesByBrandTileId"
#define getEventsByCategeoryId      @"http://tempuri.org/GetEventsByCategoryId"


//Video Weiget API

#define getCategoriesByBrand @"http://tempuri.org/GetCategoriesByBrand"
#define getVideosByCategoryId @"http://tempuri.org/GetVideosByCategoryId"
#define getVideoById @"http://tempuri.org/GetVideoById"




#define getUserFavorites @"http://tempuri.org/GetUserFavorites"
#define addFavorite @"http://tempuri.org/AddFavorite"
#define updateVideoFavouritesOrder @"http://tempuri.org/UpdateVideoFavouritesOrder"


#define deleteFavorite @"http://tempuri.org/DeleteFavorite"




#endif

