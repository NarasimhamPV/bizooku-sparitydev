//
//  SPSingletonClass.m
//  Exceeding
//
//  Created by Veeru on 19/05/14.
//
//

#import "SPSingletonClass.h"

@implementation SPSingletonClass
#define kMyNewProductIdentifier @"Audiotrack1"
#define kMyNewProductIdentifier1 @"Audiotrack2"


@synthesize couponTitleStr;
@synthesize isBackgroundAudioButtonPressed,screenNameStr,isAudioDetailsView;
@synthesize myProductArry;
@synthesize backGroundAudioScreen,eventDcHaderTitle,isMainViewCntr,eventViewTypeStr;
@synthesize seletedDate;
@synthesize isFirstTimeSevenDigitCode;
@synthesize getAppType;
@synthesize isViewDetailLoaded;
@synthesize repeatDateStr,fevVideoIndexPath,appCategoryName;

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}



//In-App-Purchase start here
- (void)inAppPurchaseRequest
{
    if([SKPaymentQueue canMakePayments])
    {
        [self requestProductData];
    }
    else {
        //IN-APP:can't make payments
        
    }
}
//
- (void)requestProductData
{
    //to send product identifiers request
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kMyNewProductIdentifier]];
    request.delegate = self;
    [request start];
}
//received product details from itunes.
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    myProductArry = [[NSArray alloc]initWithArray:response.products];
    if ([myProductArry count]>0)
    {
        SKProduct *selectedProduct = [myProductArry objectAtIndex:0];
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:selectedProduct.priceLocale];
        NSString *currencyString = [formatter stringFromNumber:selectedProduct.price];
        
        //store users currency
        [[NSUserDefaults standardUserDefaults]setObject:currencyString forKey:@"currencyString"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        selectedProduct = nil;
    }
    //myProduct = nil;
    
}

- (void)setAudioPlayerStart
{
    //NSString *ringToneName = [[NSUserDefaults standardUserDefaults]objectForKey:@"SelectedAlermTone"];
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"circles" ofType:@"m4r"]];
    NSError *error;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    self.audioPlayer.numberOfLoops = 0;
    if (self.audioPlayer == nil)
        NSLog(@"Error in audio player:%@",[error localizedDescription]);
    else
        [self.audioPlayer play];
    
}
- (void)setAudioPlayerStop
{
    [self.audioPlayer stop];
    [self.audioPlayer release];
    
}

@end
