//
//  InfoCollectorVC.h
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 25/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCRoundSwitch.h"
#import "ConstantsEDM.h"

@protocol InfoCollectorDelegate <NSObject>
-(void)infoCancel:(id)sender;
-(void)infoProceed:(id)sender firstName:(NSString*)firstName lastName:(NSString*)lastName gender:(NSString*)gender mobile:(NSString*)mobile email:(NSString*)email;
@end

@interface InfoCollectorVC : UIViewController<UITextFieldDelegate>
@property (retain, nonatomic) IBOutlet UITextField *tfFirst;
@property (retain, nonatomic) IBOutlet UITextField *tfLast;
@property (retain, nonatomic) IBOutlet DCRoundSwitch *dcSwitch;
@property (retain, nonatomic) IBOutlet UITextField *tfMobile;
@property (retain, nonatomic) IBOutlet UITextField *tfEmail;
@property (retain, nonatomic) id<InfoCollectorDelegate> delegate;
@property (readwrite, nonatomic) AppType appType;
@property (retain, nonatomic) IBOutlet UIView *formView;

- (IBAction)actProceed:(id)sender;
- (IBAction)actCancel:(id)sender;

@end
