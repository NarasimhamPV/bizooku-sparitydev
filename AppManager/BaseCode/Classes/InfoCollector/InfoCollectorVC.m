//
//  InfoCollectorVC.m
//  Exceeding
//
//  Created by Rajesh Kumar Yandamuri on 25/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InfoCollectorVC.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"


@interface InfoCollectorVC ()

@end

@implementation InfoCollectorVC
@synthesize tfFirst;
@synthesize tfLast;
@synthesize dcSwitch;
@synthesize tfMobile;
@synthesize tfEmail;
@synthesize appType = _appType;
@synthesize formView;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.dcSwitch.onText = @"Male";
	self.dcSwitch.offText = @"Female";
    
    formView.layer.cornerRadius = 10.0;
    
    [tfFirst becomeFirstResponder];
}

- (void)viewDidUnload
{
    [self setTfFirst:nil];
    [self setTfLast:nil];
    [self setDcSwitch:nil];
    [self setTfMobile:nil];
    [self setFormView:nil];
    [self setTfEmail:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [tfFirst release];
    [tfLast release];
    [dcSwitch release];
    [tfMobile release];
    [formView release];
    [tfEmail release];
    [super dealloc];
}

#pragma mark Textfield delegate methods...

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField == tfEmail)
    {
        if([AppDelegate validateEmail:tfEmail.text])
        {
            [tfEmail setBackgroundColor:[UIColor whiteColor]];
            
        }
        else{
            [tfEmail setBackgroundColor:[UIColor yellowColor]];
            
        }
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField ==tfMobile)
    {
        //[tfMobile setTextColor:[UIColor blackColor]];
         [tfMobile setBackgroundColor:[UIColor whiteColor]];
        if([self isNumber:string])
        {
            /*
            if([textField.text length] < 10 )
            {
                
                if([textField.text length] == 3 || [textField.text length] == 7)
                {
                    NSMutableString *string = [NSMutableString string];
                    [string appendString:tfMobile.text];
                    
                    [string appendString:@"-"];
                    textField.text = string;
                }

                return YES;
            }
            else{
                
                return NO;
            }*/
        }
        else{
            return YES;
        }
    }
    else if(textField ==tfEmail)
    {
         [tfEmail setBackgroundColor:[UIColor whiteColor]];
        //[tfEmail setTextColor:[UIColor blackColor]];
    }
    else if(textField == tfFirst)
    {
        [tfFirst setBackgroundColor:[UIColor whiteColor]];
    }
    else if(textField == tfLast)
    {
        [tfLast setBackgroundColor:[UIColor whiteColor]];
    }
    
    
    
    return YES;
    
}

- (BOOL)isNumber:(NSString *)str
{
    for (int i=1; i<=9; i++) {
        if([str intValue] == i || [str isEqualToString:@"0"])
        {
            return YES;
        }
    }
    return NO;
}

- (BOOL) validatePhoneWithString:(NSString *)phone
{
    NSString *str = @"(([+]{1}|[0]{2}){0,1}+[1]{1}){0,1}+[ ]{0,1}+(?:[-( ]{0,1}[0-9]{3}[-) ]{0,1}){0,1}+[ ]{0,1}+[0-9]{2,3}+[0-9- ]{4,8}";
    NSPredicate *no = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
    if([no evaluateWithObject:phone])
    {
        if([tfMobile.text length] == 12)
        {
            return YES;
        }
        return NO;
    }
    else{
        return NO;
    }
    return [no evaluateWithObject:phone];
}


- (IBAction)actProceed:(id)sender {
    if(tfFirst.text.length != 0)
    {
        if(tfLast.text.length != 0)
        {
            if([AppDelegate validateEmail:tfEmail.text])
            {
                if (tfMobile.text.length  > 0){
                    [_delegate infoProceed:self firstName:tfFirst.text lastName:tfLast.text gender:(dcSwitch.on)?@"M":@"F" mobile:tfMobile.text email:tfEmail.text];

                }else{
                    [tfMobile setBackgroundColor:[UIColor yellowColor]];
                    
                    [tfEmail setBackgroundColor:[UIColor whiteColor]];
                    [tfLast setBackgroundColor:[UIColor whiteColor]];
                    [tfFirst setBackgroundColor:[UIColor whiteColor]];
                    [tfMobile becomeFirstResponder];
                }
                // Phone no Validation removed
                /*
                if([self validatePhoneWithString:tfMobile.text])
                {
                    [_delegate infoProceed:self firstName:tfFirst.text lastName:tfLast.text gender:(dcSwitch.on)?@"M":@"F" mobile:tfMobile.text email:tfEmail.text];
                }
                else{
                    //[tfMobile setTextColor:[UIColor redColor]];
                    [tfMobile setBackgroundColor:[UIColor yellowColor]];
                    
                    [tfEmail setBackgroundColor:[UIColor whiteColor]];
                    [tfLast setBackgroundColor:[UIColor whiteColor]];
                    [tfFirst setBackgroundColor:[UIColor whiteColor]];
                    [tfMobile becomeFirstResponder];
                }*/
            }
            else{
                //[tfEmail setTextColor:[UIColor redColor]];
                [tfEmail setBackgroundColor:[UIColor yellowColor]];
                [tfLast setBackgroundColor:[UIColor whiteColor]];
                [tfFirst setBackgroundColor:[UIColor whiteColor]];
                [tfMobile setBackgroundColor:[UIColor whiteColor]];
                [tfEmail becomeFirstResponder];
            }
        }
        else{
            [tfLast setBackgroundColor:[UIColor yellowColor]];
            [tfFirst setBackgroundColor:[UIColor whiteColor]];
             [tfMobile setBackgroundColor:[UIColor whiteColor]];
             [tfEmail setBackgroundColor:[UIColor whiteColor]];
             [tfLast becomeFirstResponder];
        }
    }
    else{
        [tfFirst setBackgroundColor:[UIColor yellowColor]];
        [tfLast setBackgroundColor:[UIColor whiteColor]];
        [tfMobile setBackgroundColor:[UIColor whiteColor]];
        [tfEmail setBackgroundColor:[UIColor whiteColor]];
        [tfFirst becomeFirstResponder];
    }
}

- (IBAction)actCancel:(id)sender {
    
    
    [_delegate infoCancel:self];
}
@end
